/*
SQLyog Community v12.5.0 (64 bit)
MySQL - 5.7.12-log : Database - appson_master
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`appson_master` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `appson_master`;

/*Table structure for table `againstpayment` */

DROP TABLE IF EXISTS `againstpayment`;

CREATE TABLE `againstpayment` (
  `Bill_no` varchar(255) DEFAULT NULL,
  `Ref_Bill_no` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `Payer_name` varchar(255) DEFAULT NULL,
  `Amount` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `cheque_no` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `againstpayment` */

/*Table structure for table `app_setting` */

DROP TABLE IF EXISTS `app_setting`;

CREATE TABLE `app_setting` (
  `Menu` varchar(45) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `app_setting` */

/*Table structure for table `approval` */

DROP TABLE IF EXISTS `approval`;

CREATE TABLE `approval` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` int(10) unsigned NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `vsn` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`vsn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `approval` */

/*Table structure for table `appsondayend` */

DROP TABLE IF EXISTS `appsondayend`;

CREATE TABLE `appsondayend` (
  `Ledger_name` varchar(255) DEFAULT NULL,
  `Open_bal` varchar(255) DEFAULT NULL,
  `openbal_type` varchar(255) DEFAULT NULL,
  `curr_bal` varchar(255) DEFAULT NULL,
  `curr_baltype` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `appsondayend` */

/*Table structure for table `article_no` */

DROP TABLE IF EXISTS `article_no`;

CREATE TABLE `article_no` (
  `articleno` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`articleno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `article_no` */

/*Table structure for table `bankpayment` */

DROP TABLE IF EXISTS `bankpayment`;

CREATE TABLE `bankpayment` (
  `sno` varchar(45) NOT NULL,
  `da_te` varchar(45) DEFAULT NULL,
  `by_ledger` longtext,
  `to_ledger` longtext,
  `amo_unt` varchar(45) DEFAULT NULL,
  `narra_tion` longtext,
  `grand_total` varchar(45) DEFAULT NULL,
  `vsn` int(11) unsigned DEFAULT NULL,
  `branchid` varchar(45) NOT NULL,
  `cheque` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bankpayment` */

/*Table structure for table `bankreceipt` */

DROP TABLE IF EXISTS `bankreceipt`;

CREATE TABLE `bankreceipt` (
  `s_no` varchar(45) NOT NULL,
  `da_te` varchar(45) DEFAULT NULL,
  `by_ledger` longtext,
  `to_ledger` longtext,
  `amo_unt` varchar(45) DEFAULT NULL,
  `narra_tion` longtext,
  `grand_total` varchar(45) DEFAULT NULL,
  `vsn` int(10) unsigned DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `cheque` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bankreceipt` */

/*Table structure for table `bill_cheque` */

DROP TABLE IF EXISTS `bill_cheque`;

CREATE TABLE `bill_cheque` (
  `bill_no` varchar(45) DEFAULT NULL,
  `bill_date` varchar(45) DEFAULT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Cheque_no` varchar(45) DEFAULT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `cheque_date` varchar(45) DEFAULT NULL,
  `payeebank` varchar(45) DEFAULT NULL,
  `Tobank` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bill_cheque` */

/*Table structure for table `billing` */

DROP TABLE IF EXISTS `billing`;

CREATE TABLE `billing` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `vatamnt` varchar(45) DEFAULT NULL,
  `vataccount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) DEFAULT NULL,
  `Sgstamnt` varchar(45) DEFAULT NULL,
  `Sgstaccount` varchar(45) DEFAULT NULL,
  `Cgstamnt` varchar(45) DEFAULT NULL,
  `Cgstaccount` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_taxamnt` varchar(45) DEFAULT NULL,
  `total_sgst_taxamnt` varchar(45) DEFAULT NULL,
  `total_cgst_taxamnt` varchar(45) DEFAULT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `cashamount` varchar(255) DEFAULT NULL,
  `cashaccount` varchar(45) DEFAULT NULL,
  `sundrydebtorsamount` varchar(255) DEFAULT NULL,
  `sundrydebtorsaccount` varchar(255) DEFAULT NULL,
  `cardamount` varchar(255) DEFAULT NULL,
  `cardaccount` varchar(255) DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `SR_Ref` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `billing` */

/*Table structure for table `billing1` */

DROP TABLE IF EXISTS `billing1`;

CREATE TABLE `billing1` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `vatamnt` varchar(45) DEFAULT NULL,
  `vataccount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) DEFAULT NULL,
  `Sgstamnt` varchar(45) DEFAULT NULL,
  `Sgstaccount` varchar(45) DEFAULT NULL,
  `Cgstamnt` varchar(45) DEFAULT NULL,
  `Cgstaccount` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_taxamnt` varchar(45) DEFAULT NULL,
  `total_sgst_taxamnt` varchar(45) DEFAULT NULL,
  `total_cgst_taxamnt` varchar(45) DEFAULT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `cashamount` varchar(255) DEFAULT NULL,
  `cashaccount` varchar(45) DEFAULT NULL,
  `sundrydebtorsamount` varchar(255) DEFAULT NULL,
  `sundrydebtorsaccount` varchar(255) DEFAULT NULL,
  `cardamount` varchar(255) DEFAULT NULL,
  `cardaccount` varchar(255) DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `SR_Ref` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `billing1` */

/*Table structure for table `billing_customer` */

DROP TABLE IF EXISTS `billing_customer`;

CREATE TABLE `billing_customer` (
  `Customer_Name` varchar(45) DEFAULT NULL,
  `Is_Married` tinyint(1) DEFAULT NULL,
  `Spouse_name` varchar(45) DEFAULT NULL,
  `Anniversary` varchar(45) DEFAULT NULL,
  `Mobile_No` varchar(45) DEFAULT NULL,
  `Whatsapp_No` varchar(45) DEFAULT NULL,
  `Email_Id` varchar(45) DEFAULT NULL,
  `DOB` varchar(45) DEFAULT NULL,
  `Address1` varchar(45) DEFAULT NULL,
  `Address2` varchar(45) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `billing_customer` */

/*Table structure for table `billing_old` */

DROP TABLE IF EXISTS `billing_old`;

CREATE TABLE `billing_old` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `vatamnt` varchar(45) DEFAULT NULL,
  `vataccount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) DEFAULT NULL,
  `Sgstamnt` varchar(45) DEFAULT NULL,
  `Sgstaccount` varchar(45) DEFAULT NULL,
  `Cgstamnt` varchar(45) DEFAULT NULL,
  `Cgstaccount` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_taxamnt` varchar(45) DEFAULT NULL,
  `total_sgst_taxamnt` varchar(45) DEFAULT NULL,
  `total_cgst_taxamnt` varchar(45) DEFAULT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `cashamount` varchar(255) DEFAULT NULL,
  `cashaccount` varchar(45) DEFAULT NULL,
  `sundrydebtorsamount` varchar(255) DEFAULT NULL,
  `sundrydebtorsaccount` varchar(255) DEFAULT NULL,
  `cardamount` varchar(255) DEFAULT NULL,
  `cardaccount` varchar(255) DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `SR_Ref` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `billing_old` */

/*Table structure for table `branch` */

DROP TABLE IF EXISTS `branch`;

CREATE TABLE `branch` (
  `Branch_no` varchar(45) NOT NULL,
  `Branch_name` varchar(45) DEFAULT NULL,
  `Branch_address` varchar(45) DEFAULT NULL,
  `Branch_city` varchar(45) DEFAULT NULL,
  `Branch_State` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Branch_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `branch` */

/*Table structure for table `branchdata1` */

DROP TABLE IF EXISTS `branchdata1`;

CREATE TABLE `branchdata1` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `branchdata1` */

/*Table structure for table `branchdata2` */

DROP TABLE IF EXISTS `branchdata2`;

CREATE TABLE `branchdata2` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `branchdata2` */

/*Table structure for table `branchdata3` */

DROP TABLE IF EXISTS `branchdata3`;

CREATE TABLE `branchdata3` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `branchdata3` */

/*Table structure for table `branchdata4` */

DROP TABLE IF EXISTS `branchdata4`;

CREATE TABLE `branchdata4` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `branchdata4` */

/*Table structure for table `branchdata5` */

DROP TABLE IF EXISTS `branchdata5`;

CREATE TABLE `branchdata5` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `branchdata5` */

/*Table structure for table `bsnl_billing_gst` */

DROP TABLE IF EXISTS `bsnl_billing_gst`;

CREATE TABLE `bsnl_billing_gst` (
  `Bill_Refrence_No` varchar(45) DEFAULT NULL,
  `Bill_Date` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Customer_Name` varchar(45) DEFAULT NULL,
  `Customer_Account` varchar(45) DEFAULT NULL,
  `Mobile_no` varchar(45) DEFAULT NULL,
  `Sno` varchar(45) DEFAULT NULL,
  `Product` varchar(45) DEFAULT NULL,
  `Qnty` varchar(45) DEFAULT NULL,
  `Gross_Amount` varchar(45) DEFAULT NULL,
  `Basic_Amount` varchar(45) DEFAULT NULL,
  `Net_Amount` varchar(45) DEFAULT NULL,
  `Total_Quntity` varchar(45) DEFAULT NULL,
  `Total_Gross_Amount` varchar(45) DEFAULT NULL,
  `Total_Basic_Amount` varchar(45) DEFAULT NULL,
  `Total_Net_Amount` varchar(45) DEFAULT NULL,
  `Igst_Account` varchar(45) DEFAULT NULL,
  `Igst_Amount` varchar(45) DEFAULT NULL,
  `Sgst_Account` varchar(45) DEFAULT NULL,
  `Sgst_Amount` varchar(45) DEFAULT NULL,
  `Cgst_Account` varchar(45) DEFAULT NULL,
  `Cgst_Amount` varchar(45) DEFAULT NULL,
  `Total_Igst_Amount` varchar(45) DEFAULT NULL,
  `Total_Sgst_Amount` varchar(45) DEFAULT NULL,
  `Total_Cgst_Amount` varchar(45) DEFAULT NULL,
  `Round_Off` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `Branch_id` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bsnl_billing_gst` */

/*Table structure for table `bsnl_purchase` */

DROP TABLE IF EXISTS `bsnl_purchase`;

CREATE TABLE `bsnl_purchase` (
  `Bill_Refrence_No` varchar(45) NOT NULL,
  `Supplier_Id` varchar(45) DEFAULT NULL,
  `Supplier_Name` varchar(45) DEFAULT NULL,
  `Invoice_Date` varchar(45) DEFAULT NULL,
  `Purchase_Type` varchar(45) DEFAULT NULL,
  `Party_Purchase_Date` varchar(45) DEFAULT NULL,
  `Purchase_Refrence_No` varchar(45) DEFAULT NULL,
  `Sno` varchar(45) DEFAULT NULL,
  `Qnty` varchar(45) DEFAULT NULL,
  `Product` varchar(45) DEFAULT NULL,
  `Gross_Amount` varchar(45) DEFAULT NULL,
  `Basic_Price` varchar(45) DEFAULT NULL,
  `Discount` varchar(45) DEFAULT NULL,
  `Amnt_After_Disc` varchar(45) DEFAULT NULL,
  `Total_Gross_Amount` varchar(45) DEFAULT NULL,
  `Total_Basic_Amount` varchar(45) DEFAULT NULL,
  `Total_Qnty` varchar(45) DEFAULT NULL,
  `Total_Discount` varchar(45) DEFAULT NULL,
  `Total_Amnt_After_Disc` varchar(45) DEFAULT NULL,
  `Other_Charges` varchar(45) DEFAULT NULL,
  `Igst_Account` varchar(45) DEFAULT NULL,
  `Igst_Amount` varchar(45) DEFAULT NULL,
  `Sgst_Account` varchar(45) DEFAULT NULL,
  `Sgst_Amount` varchar(45) DEFAULT NULL,
  `Cgst_Account` varchar(45) DEFAULT NULL,
  `Cgst_Amount` varchar(45) DEFAULT NULL,
  `Total_Igst_Amount` varchar(45) DEFAULT NULL,
  `Total_Sgst_Amount` varchar(45) DEFAULT NULL,
  `Total_Cgst_Amount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `Branch_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bsnl_purchase` */

/*Table structure for table `bsnlbilling` */

DROP TABLE IF EXISTS `bsnlbilling`;

CREATE TABLE `bsnlbilling` (
  `s_no` varchar(45) NOT NULL,
  `date` varchar(45) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `amnt` varchar(45) NOT NULL,
  `disc_amnt` varchar(45) NOT NULL,
  `net_amnt` varchar(45) NOT NULL,
  `to_ledger` varchar(45) NOT NULL,
  `by_ledger` varchar(45) NOT NULL,
  `disc_ledger` varchar(45) NOT NULL,
  `narration` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`s_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bsnlbilling` */

/*Table structure for table `byforgation` */

DROP TABLE IF EXISTS `byforgation`;

CREATE TABLE `byforgation` (
  `color` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`color`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `byforgation` */

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `city` varchar(45) DEFAULT NULL,
  `city_code` varchar(45) NOT NULL,
  `state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`city_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `city` */

insert  into `city`(`city`,`city_code`,`state`) values 
('AHEMDABAD','1','GUJARAT'),
('BAREILY','10','UTTAR PRADESH'),
('MORADABAD','11','UTTAR PRADESH'),
('MUMBAI','12','MAHARASHTRA'),
('BANGLORE','13','KARNATAKA'),
('COIMBATORE','14','TAMILNADU'),
('CHIRALA','15','ANDHRA PRADESH'),
('BELGAUM','16','KARNATAKA'),
('RAMPUR','17','RAJASTHAN'),
('JAMNAGAR','18','GUJARAT'),
('ERODE','19','MADHYA PRADESH'),
('SURAT','2','GUJARAT'),
('JETPUR','20','RAJASTHAN'),
('BHUJ-KUTCH','22','GUJARAT'),
('KOTA','23','RAJASTHAN'),
('ULHASNAGAR','25','MAHARASHTRA'),
('BIKANER','26','RAJASTHAN'),
('CHANDERI','27','RAJASTHAN'),
('CHENNAI','28','TAMILNADU'),
('DADAR ','29','MAHARASHTRA'),
('JAIPUR','3','RAJASTHAN'),
('ICHALKARANJI','30','TAMILNADU'),
('JODHPUR','31','RAJASTHAN'),
('GURGAON','32','UTTAR PRADESH'),
('KOLKATA','4','WEST BENGAL'),
('DELHI','5','DELHI'),
('INDORE','6','MADHYA PRADESH'),
('RATLAM','7','MADHYA PRADESH'),
('VARANASI','8','UTTAR PRADESH'),
('LUCKNOW','9','UTTAR PRADESH');

/*Table structure for table `city_sales` */

DROP TABLE IF EXISTS `city_sales`;

CREATE TABLE `city_sales` (
  `city` varchar(45) DEFAULT NULL,
  `city_code` varchar(45) NOT NULL,
  `state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`city_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `city_sales` */

/*Table structure for table `closingstock` */

DROP TABLE IF EXISTS `closingstock`;

CREATE TABLE `closingstock` (
  `Name` varchar(255) NOT NULL,
  `Amount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `closingstock` */

/*Table structure for table `cmp_detail` */

DROP TABLE IF EXISTS `cmp_detail`;

CREATE TABLE `cmp_detail` (
  `cmp_name` varchar(255) NOT NULL,
  `sub_name` varchar(255) NOT NULL,
  `add1` varchar(255) NOT NULL,
  `add2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `tin_no` varchar(255) NOT NULL,
  `pan_no` varchar(255) NOT NULL,
  `tan_no` varchar(255) NOT NULL,
  `gst_no` varchar(255) NOT NULL,
  `mobile_no2` varchar(255) NOT NULL,
  `pincode` varchar(45) NOT NULL,
  PRIMARY KEY (`cmp_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cmp_detail` */

insert  into `cmp_detail`(`cmp_name`,`sub_name`,`add1`,`add2`,`city`,`state`,`phone_no`,`mobile_no`,`tin_no`,`pan_no`,`tan_no`,`gst_no`,`mobile_no2`,`pincode`) values 
('ANUPAM','','36 DALU MODI BAZAR','','RATLAM','M.P.','07412-233051','','23203400045','AAZFA6385K','','','','');

/*Table structure for table `contra` */

DROP TABLE IF EXISTS `contra`;

CREATE TABLE `contra` (
  `s_no` varchar(45) NOT NULL,
  `v_date` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `amo_unt` varchar(45) DEFAULT NULL,
  `narra_tion` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`s_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `contra` */

/*Table structure for table `customer_details` */

DROP TABLE IF EXISTS `customer_details`;

CREATE TABLE `customer_details` (
  `Customer_Name` varchar(45) DEFAULT NULL,
  `Mobile_no` varchar(45) NOT NULL,
  PRIMARY KEY (`Mobile_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer_details` */

/*Table structure for table `databranch1` */

DROP TABLE IF EXISTS `databranch1`;

CREATE TABLE `databranch1` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `databranch1` */

/*Table structure for table `databranch2` */

DROP TABLE IF EXISTS `databranch2`;

CREATE TABLE `databranch2` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `databranch2` */

/*Table structure for table `databranch3` */

DROP TABLE IF EXISTS `databranch3`;

CREATE TABLE `databranch3` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `databranch3` */

/*Table structure for table `databranch4` */

DROP TABLE IF EXISTS `databranch4`;

CREATE TABLE `databranch4` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `databranch4` */

/*Table structure for table `databranch5` */

DROP TABLE IF EXISTS `databranch5`;

CREATE TABLE `databranch5` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `databranch5` */

/*Table structure for table `defecteditem` */

DROP TABLE IF EXISTS `defecteditem`;

CREATE TABLE `defecteditem` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `qnt` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `defecteditem` */

/*Table structure for table `discount` */

DROP TABLE IF EXISTS `discount`;

CREATE TABLE `discount` (
  `dis_code` varchar(45) DEFAULT NULL,
  `S_no` varchar(45) DEFAULT NULL,
  `des_cription` varchar(45) DEFAULT NULL,
  `pur_code` varchar(45) DEFAULT NULL,
  `disc_type` varchar(45) DEFAULT NULL,
  `dis_on_series_from` varchar(45) DEFAULT NULL,
  `dis_on_series_to` varchar(45) DEFAULT NULL,
  `validity_from` varchar(45) DEFAULT NULL,
  `validity_to` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `discount` */

/*Table structure for table `file_import` */

DROP TABLE IF EXISTS `file_import`;

CREATE TABLE `file_import` (
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `file_import` */

/*Table structure for table `group_create` */

DROP TABLE IF EXISTS `group_create`;

CREATE TABLE `group_create` (
  `Name` varchar(45) NOT NULL DEFAULT '',
  `Under` varchar(45) DEFAULT NULL,
  `Sub_Legder` varchar(45) DEFAULT NULL,
  `Bal_Reporting` varchar(45) DEFAULT NULL,
  `Calculations` varchar(45) DEFAULT NULL,
  `Method` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `group_create` */

/*Table structure for table `interestentry` */

DROP TABLE IF EXISTS `interestentry`;

CREATE TABLE `interestentry` (
  `s_no` varchar(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `Party_name` varchar(255) DEFAULT NULL,
  `amnt` varchar(255) DEFAULT NULL,
  `to_ledger` varchar(255) DEFAULT NULL,
  `by_ledger` varchar(255) DEFAULT NULL,
  `narration` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `interestentry` */

/*Table structure for table `journal` */

DROP TABLE IF EXISTS `journal`;

CREATE TABLE `journal` (
  `vsn` int(10) unsigned NOT NULL,
  `s_no` varchar(45) NOT NULL,
  `da_te` varchar(45) DEFAULT NULL,
  `by_ledger` longtext,
  `to_ledger` longtext,
  `narra_tion` longtext,
  `debit_amt` varchar(45) DEFAULT NULL,
  `credit_amt` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `journal` */

/*Table structure for table `ledger` */

DROP TABLE IF EXISTS `ledger`;

CREATE TABLE `ledger` (
  `ledger_name` varchar(255) NOT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `groups` varchar(45) DEFAULT NULL,
  `inv_affected` varchar(45) DEFAULT NULL,
  `recon_date` varchar(45) DEFAULT NULL,
  `percent` varchar(45) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `open_bal` int(10) unsigned DEFAULT NULL,
  `bal_type` varchar(45) DEFAULT NULL,
  `curr_bal` varchar(45) DEFAULT NULL,
  `currbal_type` varchar(45) DEFAULT NULL,
  `type_oftax` varchar(45) DEFAULT NULL,
  `kth_code` varchar(45) DEFAULT NULL,
  `Gst_Applicable` tinyint(1) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Change_Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ledger_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ledger` */

insert  into `ledger`(`ledger_name`,`alias`,`groups`,`inv_affected`,`recon_date`,`percent`,`method`,`open_bal`,`bal_type`,`curr_bal`,`currbal_type`,`type_oftax`,`kth_code`,`Gst_Applicable`,`Type`,`Change_Status`) values 
('CARD PAYMENT','Card payment','BANK ACCOUNTS','','','','',0,'CR','0.00','CR','',NULL,NULL,NULL,0),
('CASH','Cash','CASH IN HAND','','','','',0,'DR','0.00','DR','',NULL,NULL,NULL,0),
('CGST 2.5%','','DUTIES AND TAXES','','','2.5','ADDITIONAL DUTY',0,'CR','0.00','DR','null','',1,'CGST',0),
('CGST 6%','','DUTIES AND TAXES','','','6','ADDITIONAL DUTY',0,'CR','0.00','DR','null','',1,'CGST',0),
('Discount Account','','INDIRECT INCOMES','','','','',0,'CR','0.0','DR','','',0,'',0),
('Freight Charges','','INDIRECT EXPENSES','','','','',0,'CR','0.00','CR','','',0,'',0),
('IGST 12%','','DUTIES AND TAXES','','','12','ADDITIONAL DUTY',0,'CR','0.00','DR','OTHER','',1,'IGST',0),
('IGST 5%','','DUTIES AND TAXES','','','5','ADDITIONAL DUTY',0,'CR','0.00','DR','null','',1,'IGST',0),
('INTREST A/C','','INDIRECT EXPENSES','','','','',0,'CR','0.00','CR','',NULL,NULL,NULL,0),
('PURCHASE 12%','','PURCHASE ACCOUNTS','','','12','',0,'CR','0.00','CR','','',0,'',0),
('Purchase 5%','','PURCHASE ACCOUNTS','','','5','',0,'CR','0.00','DR','','',0,'',0),
('Round Off','','DUTIES AND TAXES','NO','','0','ADDITIONAL DUTY',0,'DR','0.00','DR','OTHER','',0,'null',0),
('Sales 12%','','SALES ACCOUNTS','','','12',NULL,0,'CR','0.00','DR','','',0,'',0),
('Sales 5%','','SALES ACCOUNTS','','','5','',0,'CR','0.00','CR','','',0,'',0),
('SGST 2.5%','','DUTIES AND TAXES','','','2.5','ADDITIONAL DUTY',0,'CR','0.00','DR','OTHER','',1,'SGST',0),
('SGST 6%','','DUTIES AND TAXES','','','6','ADDITIONAL DUTY',0,'CR','0.00','DR','null','',1,'SGST',0);

/*Table structure for table `ledger_details` */

DROP TABLE IF EXISTS `ledger_details`;

CREATE TABLE `ledger_details` (
  `ledger_name` varchar(255) NOT NULL,
  `account` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `pin` varchar(45) DEFAULT NULL,
  `pan` varchar(45) DEFAULT NULL,
  `tax` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ledger_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ledger_details` */

/*Table structure for table `ledger_opening` */

DROP TABLE IF EXISTS `ledger_opening`;

CREATE TABLE `ledger_opening` (
  `ledger_name` varchar(255) NOT NULL,
  `groups` varchar(45) DEFAULT NULL,
  `open_bal` varchar(45) DEFAULT NULL,
  `bal_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ledger_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ledger_opening` */

/*Table structure for table `ledgerfinal` */

DROP TABLE IF EXISTS `ledgerfinal`;

CREATE TABLE `ledgerfinal` (
  `S_no` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Curr_Date` varchar(45) DEFAULT NULL,
  `ledger_name` varchar(45) DEFAULT NULL,
  `groups` varchar(45) DEFAULT NULL,
  `curr_bal` varchar(45) DEFAULT NULL,
  `currbal_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`S_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ledgerfinal` */

/*Table structure for table `openingstock` */

DROP TABLE IF EXISTS `openingstock`;

CREATE TABLE `openingstock` (
  `Name` varchar(255) NOT NULL,
  `Amount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `openingstock` */

/*Table structure for table `party` */

DROP TABLE IF EXISTS `party`;

CREATE TABLE `party` (
  `party_code` varchar(45) NOT NULL,
  `party_name` varchar(45) NOT NULL,
  `mobile_no` varchar(45) DEFAULT NULL,
  `address1` varchar(45) DEFAULT NULL,
  `address2` varchar(45) DEFAULT NULL,
  `address3` varchar(45) DEFAULT NULL,
  `address4` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `mark_up` varchar(45) DEFAULT NULL,
  `igst` tinyint(1) NOT NULL,
  `pan_no` varchar(45) DEFAULT NULL,
  `gstin_no` varchar(45) DEFAULT NULL,
  `whatsapp_no` varchar(45) DEFAULT NULL,
  `bank_account1` varchar(45) DEFAULT NULL,
  `bank_name1` varchar(45) DEFAULT NULL,
  `bank_ifsc1` varchar(45) DEFAULT NULL,
  `bank_account2` varchar(45) DEFAULT NULL,
  `bank_name2` varchar(45) DEFAULT NULL,
  `bank_ifsc2` varchar(45) DEFAULT NULL,
  `opening_bal` varchar(45) DEFAULT NULL,
  `bal_type` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `branch_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`party_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `party` */

/*Table structure for table `party1` */

DROP TABLE IF EXISTS `party1`;

CREATE TABLE `party1` (
  `party_code` varchar(45) NOT NULL DEFAULT '',
  `party_name` longtext,
  `address` longtext,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `phone_no` varchar(45) DEFAULT NULL,
  `email_no` varchar(45) DEFAULT NULL,
  `pan_no` varchar(45) DEFAULT NULL,
  `tin_no` varchar(45) DEFAULT NULL,
  `opening_bal` varchar(45) DEFAULT NULL,
  `bal_type` varchar(45) DEFAULT NULL,
  `web_sit` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `mobileno` varchar(45) DEFAULT NULL,
  `whatsapp no` varchar(45) DEFAULT NULL,
  `mark_up` varchar(45) DEFAULT NULL,
  `purchase_account` varchar(255) DEFAULT NULL,
  `IGST` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`party_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `party1` */

insert  into `party1`(`party_code`,`party_name`,`address`,`city`,`state`,`phone_no`,`email_no`,`pan_no`,`tin_no`,`opening_bal`,`bal_type`,`web_sit`,`branchid`,`mobileno`,`whatsapp no`,`mark_up`,`purchase_account`,`IGST`) values 
('NA','UNKNOWN','NA','NA','NA',NULL,NULL,NULL,NULL,'0','CR','NA','0',NULL,NULL,NULL,NULL,0),
('P/1/10','R.AVINASH','149,1st FLOR B.B.C.CLOTH MRKETVICTORIA PANCHKUVA','AHEMDABAD','GUJARAT','','AH250',NULL,NULL,'0','CR','P/AHM/10','0',NULL,NULL,'0',NULL,0),
('P/1/100','GARIMA FASHION','B-80,(FF)SUMEL BUSINESS PARK(SAFAL),B/H.N.C.M.SARANGPUR','AHEMDABAD','GUJARAT','07929299843,22151044','AH136',NULL,NULL,'0','CR','P/AHM/100','0',NULL,NULL,'0',NULL,0),
('P/1/101','JAI MATADI DRESSES','SHOP NO.3,PANCHKUVA SINDHI CLOTH MARKET','AHEMDABAD','GUJARAT','9879220313,22154329','AH102',NULL,NULL,'0','CR','P/AHM/101','0',NULL,NULL,'0',NULL,0),
('P/1/102','ASHOKKUMAR LILARAM','SHOP.NO.A/22,JUMANI BAZAR,SINDHI MARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','22172431,9825441953(LILAR','AH134',NULL,NULL,'0','CR','P/AHM/102','0',NULL,NULL,'0',NULL,0),
('P/1/103','LAJWANTI ENTERPRRISE','135,1ST FLOOR,SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','9377440188,30239219','AH137',NULL,NULL,'0','CR','P/AHM/103','0',NULL,NULL,'0',NULL,0),
('P/1/104','SHREE ADESHWAR TEXTILE','1609,ZATKAN,S POLE,KAPDIWADRAIPUR','AHEMDABAD','GUJARAT','22140698,26645069','AH138',NULL,NULL,'0','CR','P/AHM/104','0',NULL,NULL,'0',NULL,0),
('P/1/105','SAI CREATION','87,GROUND FLOOR,SUGNOMAL MARKTNR.HARIOM(A.C)MRKT REVDI BAZAR','AHEMDABAD','GUJARAT','30430510,9428433169','AH139',NULL,NULL,'0','CR','P/AHM/105','0',NULL,NULL,'0',NULL,0),
('P/1/106','SHREE LAXMI CREATION','137,1st FLOOR,SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','30249818','AH143',NULL,NULL,'0','CR','P/AHM/106','0',NULL,NULL,'0',NULL,0),
('P/1/107','KHUSHI ENTERPRISE','50,GROUND FLOOR,B.B.C.MARKETPANCHKUVA','AHEMDABAD','GUJARAT','30222846,9825699560','AH152',NULL,NULL,'0','CR','P/AHM/107','0',NULL,NULL,'0',NULL,0),
('P/1/108','CHAWLA TRADERS','103-104,AATISH NEARTEKRA','AHEMDABAD','GUJARAT','079-26440433,9825197667','AH140','','24ANSPS4915K1ZD','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/1/109','PRINCY DRESSES(PRINCESS)','260,2nd FLOOR,SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','079-30434757','AH144',NULL,NULL,'0','CR','P/AHM/109','0',NULL,NULL,'0',NULL,0),
('P/1/11','KASHISH CREATION','87,G/F,SUGNOMAL MARKET NR.HARIOM(A.C)MARKET REVDI BAZAR','AHEMDABAD','GUJARAT','079-30430510,9375252211','AH192',NULL,NULL,'0','CR','P/AHM/11','0',NULL,NULL,'0',NULL,0),
('P/1/110','R.S.FASHION','1 & 201,V.P.SILK MARKETPANCHKUVA','AHEMDABAD','GUJARAT','079-22110457,9328078171','AH145',NULL,NULL,'0','CR','P/AHM/110','0',NULL,NULL,'0',NULL,0),
('P/1/111','LADHARAM JETHANAND & CO.','4,JUNI DEVPLEPMENT BANK BUILDIN BANK,REVDI BAZAR','AHEMDABAD','GUJARAT','22171158,9825143833','AH146',NULL,NULL,'0','CR','P/AHM/111','0',NULL,NULL,'0',NULL,0),
('P/1/112','MAYURI EMPORIUM','118,Ist FLOOR,V.P.SILK MARKETB/H,PANCHKUVA SINDHI MARKET','AHEMDABAD','GUJARAT','30228005,22140011','AH157',NULL,NULL,'0','CR','P/AHM/112','0',NULL,NULL,'0',NULL,0),
('P/1/113','RADHE CORPORATION','L.K.TRUST BUILDING,G/F,OPP.DHANLAXMI MRK,PANCHKUVA REVDI','AHEMDABAD','GUJARAT','','AH164',NULL,NULL,'0','CR','P/AHM/113','0',NULL,NULL,'0',NULL,0),
('P/1/114','RIDDHI SELECTION','2386,RANI-NO- HAZIRO,MANEK CHOWK','AHEMDABAD','GUJARAT','22110471,9824445690','AH191',NULL,NULL,'0','CR','P/AHM/114','0',NULL,NULL,'0',NULL,0),
('P/1/115','KALAWANTI','S.NO.529/1,OLD BHAGWATI CLOTHMARKET,NR.BBC MARKET,PANCHKUVA','AHEMDABAD','GUJARAT','079-22140006,9824541469','AH147',NULL,NULL,'0','CR','P/AHM/115','0',NULL,NULL,'0',NULL,0),
('P/1/116','S.M.CORPORATION','G/F,HARIOM MARKER-2PANCHKUVA','AHEMDABAD','GUJARAT','','AH150',NULL,NULL,'0','CR','P/AHM/116','0',NULL,NULL,'0',NULL,0),
('P/1/117','RESHAM CREATION','S.NO.91,G/F SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','30244668,8000202232','AH167',NULL,NULL,'0','CR','P/AHM/117','0',NULL,NULL,'0',NULL,0),
('P/1/118','SHREE SHASWAT CREATION','22,MAIZEMINE,NEW CLOTH MARKET','AHEMDABAD','GUJARAT','221717333,9376105642','AH168',NULL,NULL,'0','CR','P/AHM/118','0',NULL,NULL,'0',NULL,0),
('P/1/119','SHREE ANJANA ENTERPRISE','144,ANAND SHOPING  CANPOLE','AHEMDABAD','GUJARAT','079-25352606, 30022606','AH169','','24AASPD6062F1ZS','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/12','COTTON SELECTION {TILAT}','5,VRUNDAVAN','AHEMDABAD','GUJARAT','','AH223',NULL,NULL,'0','CR','P/AHM/12','0',NULL,NULL,'0',NULL,0),
('P/1/120','MINESH ENTERPRISES','FATEHBHAWNI HAWELI,RATANPOLE','AHEMDABAD','GUJARAT','8140736034','AH170',NULL,NULL,'0','CR','P/AHM/120','0',NULL,NULL,'0',NULL,0),
('P/1/121','BHADRA TEXTILES','443,B.B.C.MARKET,4th FLOORPANCHKUVA','AHEMDABAD','GUJARAT','9879096965','AH206',NULL,NULL,'0','CR','P/AHM/121','0',NULL,NULL,'0',NULL,0),
('P/1/122','SELECTION ART','BLOCK-C-27/28,G/F SUMEL BUSINEPARK-3 OPP.N.C.M SARANGPUR','AHEMDABAD','GUJARAT','079-66170014,9824366456','AH171',NULL,NULL,'0','CR','P/AHM/122','0',NULL,NULL,'0',NULL,0),
('P/1/123','SELECTION POINT','250,NEW CLOTH MARKET,O/SSARANGPUR GATE','AHEMDABAD','GUJARAT','22172239,9824366456','AH172',NULL,NULL,'0','CR','P/AHM/123','0',NULL,NULL,'0',NULL,0),
('P/1/124','HITESHKUMAR BABULAL SHETH(HUF)','58,ANAND SHOPING CENTRE,GOLWADRATNAPOLE','AHEMDABAD','GUJARAT','079-25354575,9374607694','AH173',NULL,NULL,'0','CR','P/AHM/124','0',NULL,NULL,'0',NULL,0),
('P/1/125','KAARYA ETHNIC CLOTHING PVT(HRA','HASEJAA HOUSE,OPP.MASAKTI MARKET,SAKAR BAZAR,KALUPUR','AHEMDABAD','GUJARAT','22130777','AH175',NULL,NULL,'0','CR','P/AHM/125','0',NULL,NULL,'0',NULL,0),
('P/1/126','PRACHI ENTERPRISE','S.NO.707/17,G/F,BANK OF BARODABUILDING,SAKAR BAZAR','AHEMDABAD','GUJARAT','9327005160,9377519598','AH228',NULL,NULL,'0','CR','P/AHM/126','0',NULL,NULL,'0',NULL,0),
('P/1/127','KESAR SALWAR SUITS','469,SAKAR BAZAR,OPP.MASKATIMARKET,KALUPUR','AHEMDABAD','GUJARAT','8866874874','AH229',NULL,NULL,'0','CR','P/AHM/127','0',NULL,NULL,'0',NULL,0),
('P/1/128','JAGDISH KALA','E-316,3rd FLOOR SUMEL BUSINESSPARK3 OPP.NEW CLOTH MRK,RAIPUR','AHEMDABAD','GUJARAT','796617277','AH176',NULL,NULL,'0','CR','P/AHM/128','0',NULL,NULL,'0',NULL,0),
('P/1/129','KAARYA ETHENIC CLOTING PVT(KBC','','AHEMDABAD','GUJARAT','','AH230',NULL,NULL,'0','CR','P/AHM/129','0',NULL,NULL,'0',NULL,0),
('P/1/13','ESHA COLLECTION','312REVDI BAZAR','AHEMDABAD','GUJARAT','','AH289',NULL,NULL,'0','CR','P/AHM/13','0',NULL,NULL,'0',NULL,0),
('P/1/130','MAHESHKUMAR & BROS. CLOTH MRCH','B-3,KOMAL CHAMBERS,NEAR PANCHKUVA GATE STATION RAOD','AHEMDABAD','GUJARAT','22141796,22142063','AH231',NULL,NULL,'0','CR','P/AHM/130','0',NULL,NULL,'0',NULL,0),
('P/1/131','K.MANISHKUMAR & CO.','G-1 TO 4,SUMEL BUSINESS PARK-3NEW CLOTH MARKET','AHEMDABAD','GUJARAT','22162657','AH232',NULL,NULL,'0','CR','P/AHM/131','0',NULL,NULL,'0',NULL,0),
('P/1/132','M.K.SALWAR KAMEEZ PVT.LTD.','219,220,B.B.C.MARKETPANCHKUVA','AHEMDABAD','GUJARAT','22140639,9327029068','AH233',NULL,NULL,'0','CR','P/AHM/132','0',NULL,NULL,'0',NULL,0),
('P/1/133','JULLY TEXTILES','234,2nd FLOOR,B.B.C.MARKETVICTORIA JUBILEE COMPOND PANCH','AHEMDABAD','GUJARAT','30244660','AH234',NULL,NULL,'0','CR','P/AHM/133','0',NULL,NULL,'0',NULL,0),
('P/1/134','BHOOMI SELECTION','G-4,L.K.TRUST BUILDING OPP.B.B.C MARKET PANCHKUVA REID ROAD','AHEMDABAD','GUJARAT','079-22154244,8000833215','AH182',NULL,NULL,'0','CR','P/AHM/134','0',NULL,NULL,'0',NULL,0),
('P/1/135','SHRI DANSINGH & SONS','239,2nd FLOOR SHRI RAM CLOTHMARKT CROS LANE RAILWAYPURA','AHEMDABAD','GUJARAT','079-22113991','AH207',NULL,NULL,'0','CR','P/AHM/135','0',NULL,NULL,'0',NULL,0),
('P/1/136','KARISHMA /SAI CREATION','87,G/F SUGNOMAL MARKET NR,HARIOM(AC)MARKET REVDI BAZAR','AHEMDABAD','GUJARAT','079-30430510,9375252211','AH235',NULL,NULL,'0','CR','P/AHM/136','0',NULL,NULL,'0',NULL,0),
('P/1/137','SONA FASHION','KOMAL CHAMBERS,PANCHKUVA','AHEMDABAD','GUJARAT','','AH236',NULL,NULL,'0','CR','P/AHM/137','0',NULL,NULL,'0',NULL,0),
('P/1/138','MOTWANI NX','I-5 SHRI GHANTAKARNA MAHAVIRCOMM.MRK.NR.N.C.M SARANGPUR','AHEMDABAD','GUJARAT','22179494,9228890407','AH237',NULL,NULL,'0','CR','P/AHM/138','0',NULL,NULL,'0',NULL,0),
('P/1/139','JESSICA FASHIONS','C-14.G/F GHANTAKARNA MAHAVIRCOMM.MRK NR.N.C.M. SARANGPUR','AHEMDABAD','GUJARAT','22179255,9099757385','AH238',NULL,NULL,'0','CR','P/AHM/139','0',NULL,NULL,'0',NULL,0),
('P/1/14','JAY TRADING COMPANY','676/62,DHANLAXMI MARBAZAR,','AHEMDABAD','GUJARAT','22141625,09724501043','AH101','','24AAZPA8426F1ZK','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/140','K.B COTTONS','J/4,SHREE GHANTAKARAN,MAHAVIRCOMM.MARKET,SARANPUR','AHEMDABAD','GUJARAT','9327017956','AH239',NULL,NULL,'0','CR','P/AHM/140','0',NULL,NULL,'0',NULL,0),
('P/1/141','R.S.FABRICS','1 & 201 VP.SILK MARKETPANCH KUVA','AHEMDABAD','GUJARAT','','AH240',NULL,NULL,'0','CR','P/AHM/141','0',NULL,NULL,'0',NULL,0),
('P/1/142','NEELAM H.RAMCHANDANI','5,KABIR RATAN MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','','AH241',NULL,NULL,'0','CR','P/AHM/142','0',NULL,NULL,'0',NULL,0),
('P/1/143','SHREE BALAJI COMFORTS','45,ANAND SHOPING CENTRERATANPOLE','AHEMDABAD','GUJARAT','30221562,9825735742','AH242','','24AAUPD7755R1ZR','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/144','K.J.CORPORATION','529/15/1,UNDER BHAGWATI MARKETOPP.B.B.C MRKT,PANCHKUVA','AHEMDABAD','GUJARAT','9601037843,9974865679','AH243',NULL,NULL,'0','CR','P/AHM/144','0',NULL,NULL,'0',NULL,0),
('P/1/145','M.VANECHAND & CO.','335,SHRIRAM CLOTH MARKETCROSS LANE RAILWAYPURA','AHEMDABAD','GUJARAT','22144114','AH244',NULL,NULL,'0','CR','P/AHM/145','0',NULL,NULL,'0',NULL,0),
('P/1/146','K.DHARAMCHAND & CO.(LEEVAS)','237,N.C.M,O/S RAIPURGATERAIPUR','AHEMDABAD','GUJARAT','22174383','AH186',NULL,NULL,'0','CR','P/AHM/146','0',NULL,NULL,'0',NULL,0),
('P/1/147','KMB DESIGNS PVT.LTD(M.VANECHAN','GHEE BAZAR KALUPUR','AHEMDABAD','GUJARAT','22132227','AH247',NULL,NULL,'0','CR','P/AHM/147','0',NULL,NULL,'0',NULL,0),
('P/1/148','SHREE AMBICA TEXTILES','2015,G/F HARIOM MARKET-2 REIDRAOD PANCHKUVA','AHEMDABAD','GUJARAT','9327013876','AH248',NULL,NULL,'0','CR','P/AHM/148','0',NULL,NULL,'0',NULL,0),
('P/1/149','R.V.ENTERPRISE','1sT FLOR S.NO.155 B.B.C.CLOTHMARKETVICTORIA PANKUVA','AHEMDABAD','GUJARAT','','AH249',NULL,NULL,'0','CR','P/AHM/149','0',NULL,NULL,'0',NULL,0),
('P/1/15','TILAT TRADERS','OPP.ORIENTAL BUILDINGRELIEF ROAD','AHEMDABAD','GUJARAT','079-22138813','AH103',NULL,NULL,'0','CR','P/AHM/15','0',NULL,NULL,'0',NULL,0),
('P/1/150','M.D','J/3 CELLE SHREE GHANTAKARANCOMMERCIAL MARKET,SARANGPUR','AHEMDABAD','GUJARAT','','AH194',NULL,NULL,'0','CR','P/AHM/150','0',NULL,NULL,'0',NULL,0),
('P/1/151','KOLORS','G-10,G/F,SUMEL BUSINOTH MARKET RAIPUR','AHEMDABAD','GUJARAT','','AH253','','24AFSPS6623L1ZR','0','CR','','0','9016330600',NULL,'0','PURCHASE O MP',1),
('P/1/152','H.R.CRAETION','S.NO.E-7,8,9 SHRI GHANTAKARNACOMM. MRKET SARANGPUR','AHEMDABAD','GUJARAT','009909287068,9099958068','AH195',NULL,NULL,'0','CR','P/AHM/152','0',NULL,NULL,'0',NULL,0),
('P/1/153','AH255','188-189,SUGNOAL MARKET,NEARHARIOM A/C MRKET REVDI BAZAR','AHEMDABAD','GUJARAT','','AH255',NULL,NULL,'0','CR','P/AHM/153','0',NULL,NULL,'0',NULL,0),
('P/1/154','JASMIN TEXTILES','ZEVERI BHAWAN OPP.SHAMLA POLERANGATI BAZAR,ASTODIA','AHEMDABAD','GUJARAT','22141777,9825393799','AH256',NULL,NULL,'0','CR','P/AHM/154','0',NULL,NULL,'0',NULL,0),
('P/1/155','SHAH DIPAKKUMAR LALBHAI(H.U.F.','RANI-NO-HAJIRO MANEK CHOWK','AHEMDABAD','GUJARAT','22113508','AH257',NULL,NULL,'0','CR','P/AHM/155','0',NULL,NULL,'0',NULL,0),
('P/1/156','MITI ENTERPRISE','BLOCK E-448,SAFAL-3AHMEDABAD-2','AHEMDABAD','GUJARAT','','AH258',NULL,NULL,'0','CR','P/AHM/156','0',NULL,NULL,'0',NULL,0),
('P/1/157','KANHA FAB TEX','D-2 G/F SUMEL BUSINESS PARK-3OPP.NCM RAIPUR','AHEMDABAD','GUJARAT','079-66173009,9638949587','SU314',NULL,NULL,'0','CR','P/AHM/157','0',NULL,NULL,'0',NULL,0),
('P/1/158','KANHA FAB TEX','D-2 G/F SUMEL BUSINESS PARK-3OPP.NCM,RAIPUR','AHEMDABAD','GUJARAT','079-66173009,9638949587','AH259',NULL,NULL,'0','CR','P/AHM/158','0',NULL,NULL,'0',NULL,0),
('P/1/159','R-ONE FASHION','2nd CELLER HARIOM MAPANCHKUVA','AHEMDABAD','GUJARAT','','AH260','','24ARGPR0353L1ZO','0','CR','','0','9825360600',NULL,'0','PURCHASE 12%',1),
('P/1/16','SHREE PARKAR TEXTILE','25-26,ANNAND SHOPPING CENTERRATANPOLE','AHEMDABAD','GUJARAT','25321562,9825459400','AH104','','24ABMPD4747N1ZB','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/160','HIVA','','AHEMDABAD','GUJARAT','','AH261',NULL,NULL,'0','CR','P/AHM/160','0',NULL,NULL,'0',NULL,0),
('P/1/161','SHANKAR ENTERPRISES','ATTRWALA BUILDING 2nd FLOORKALUPUR','AHEMDABAD','GUJARAT','','AH262',NULL,NULL,'0','CR','P/AHM/161','0',NULL,NULL,'0',NULL,0),
('P/1/162','SHIV SHANKAR ENTERPRISES','ATTARWALA BUILDING 1st FLORKALUPUR','AHEMDABAD','GUJARAT','','AH263',NULL,NULL,'0','CR','P/AHM/162','0',NULL,NULL,'0',NULL,0),
('P/1/163','KRISHNA ENTERPRISES','KALUPUR KOTNI RANG','AHEMDABAD','GUJARAT','','AH264',NULL,NULL,'0','CR','P/AHM/163','0',NULL,NULL,'0',NULL,0),
('P/1/164','SHYAM CREATION','312,REID ROAD,PANCHKUVA','AHEMDABAD','GUJARAT','','AH265',NULL,NULL,'0','CR','P/AHM/164','0',NULL,NULL,'0',NULL,0),
('P/1/165','RADHEY CREATION','165,REVDI BAZAR','AHEMDABAD','GUJARAT','','AH266',NULL,NULL,'0','CR','P/AHM/165','0',NULL,NULL,'0',NULL,0),
('P/1/166','R.D.ENTERPRISRS','SUMEL BUSINESS PARK SARNPUR','AHEMDABAD','GUJARAT','','AH267',NULL,NULL,'0','CR','P/AHM/166','0',NULL,NULL,'0',NULL,0),
('P/1/167','ARCHANA ENTERPRISES','BLOCK B-12/13 G/F SUMEL BUSINESS,PARK-3 OPP.NCM','AHEMDABAD','GUJARAT','079-66171501','AH268',NULL,NULL,'0','CR','P/AHM/167','0',NULL,NULL,'0',NULL,0),
('P/1/168','MAHETA SYNTHETICS','80,81 ANAND SHOPING CENTERGOLWAD RATANPOLE','AHEMDABAD','GUJARAT','25351831','AH269',NULL,NULL,'0','CR','P/AHM/168','0',NULL,NULL,'0',NULL,0),
('P/1/169','MOHAN ENTERPRISE','15 RATANPOL GOLWAD','AHEMDABAD','GUJARAT','','DE220',NULL,NULL,'0','CR','P/AHM/169','0',NULL,NULL,'0',NULL,0),
('P/1/17','K.ZAVER & CO.','2001,HARIOM MARKETREID ROAD PANCHKUVA','AHEMDABAD','GUJARAT','22149825','AH105','','24AAAFK9797Q1ZD','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/170','MAYURI EMPORIUM','118,V.P.SILK SINDHI MARKET','AHEMDABAD','GUJARAT','','AH271',NULL,NULL,'0','CR','P/AHM/170','0',NULL,NULL,'0',NULL,0),
('P/1/171','ASHISH CREATION','2050,V.P.SILK SINDHI MARKET','AHEMDABAD','GUJARAT','','AH272',NULL,NULL,'0','CR','P/AHM/171','0',NULL,NULL,'0',NULL,0),
('P/1/172','SIMRAN EMPORIUM','2020,V.P SILK SINDHI MARKERT','AHEMDABAD','GUJARAT','','AH273',NULL,NULL,'0','CR','P/AHM/172','0',NULL,NULL,'0',NULL,0),
('P/1/173','NAVRANG CREATION','2020,ATTAR MANSION JAWAHAR MARG','AHEMDABAD','GUJARAT','','AH274',NULL,NULL,'0','CR','P/AHM/173','0',NULL,NULL,'0',NULL,0),
('P/1/174','VIJAY ENTERPRISES','256,NEHRU GALI GANDHI NAGAR','AHEMDABAD','GUJARAT','','AH276',NULL,NULL,'0','CR','P/AHM/174','0',NULL,NULL,'0',NULL,0),
('P/1/175','MADHAVI CREATION','2565NEHRU GALI GANDHI NAGAR','AHEMDABAD','GUJARAT','','AH277',NULL,NULL,'0','CR','P/AHM/175','0',NULL,NULL,'0',NULL,0),
('P/1/176','MANYA COLLECTION','SFAEL PARK-4','AHEMDABAD','GUJARAT','','AH278',NULL,NULL,'0','CR','P/AHM/176','0',NULL,NULL,'0',NULL,0),
('P/1/177','RISHAB COLLECTION','SAFEL PARK-3','AHEMDABAD','GUJARAT','','AH279',NULL,NULL,'0','CR','P/AHM/177','0',NULL,NULL,'0',NULL,0),
('P/1/178','HARIHAR COLLETION','1250 SAFEL-3','AHEMDABAD','GUJARAT','','AH281',NULL,NULL,'0','CR','P/AHM/178','0',NULL,NULL,'0',NULL,0),
('P/1/179','SANJAY CREATION','2nd FLOOR KHAJURI NO KHAMCHOKALUPUR','AHEMDABAD','GUJARAT','','AH282',NULL,NULL,'0','CR','P/AHM/179','0',NULL,NULL,'0',NULL,0),
('P/1/18','D.D.CORPORATION','G/F-20,VRUNDAVAN SHOPPINGCENTER RATANPOLE','AHEMDABAD','GUJARAT','07925353745','AH106',NULL,NULL,'0','CR','P/AHM/18','0',NULL,NULL,'0',NULL,0),
('P/1/180','MAHESH CREATION','25 HARIOM MARKET','AHEMDABAD','GUJARAT','','AH283',NULL,NULL,'0','CR','P/AHM/180','0',NULL,NULL,'0',NULL,0),
('P/1/181','JAGDISH CREATION','36,N.T.M KALUPUR','AHEMDABAD','GUJARAT','','AH284',NULL,NULL,'0','CR','P/AHM/181','0',NULL,NULL,'0',NULL,0),
('P/1/182','JANVI NX','236,KALUPUR','AHEMDABAD','GUJARAT','','AH286',NULL,NULL,'0','CR','P/AHM/182','0',NULL,NULL,'0',NULL,0),
('P/1/183','KIRAN CREATION','S.NO.45,2nd FLORKALUPUR KOTNI RANG','AHEMDABAD','GUJARAT','','AH287',NULL,NULL,'0','CR','P/AHM/183','0',NULL,NULL,'0',NULL,0),
('P/1/184','CHITRA NX','1st FLLOR KALUPUR','AHEMDABAD','GUJARAT','','AH290',NULL,NULL,'0','CR','P/AHM/184','0',NULL,NULL,'0',NULL,0),
('P/1/185','MADHAVI NX','25 SUMEL BUSINESS','AHEMDABAD','GUJARAT','','AH291',NULL,NULL,'0','CR','P/AHM/185','0',NULL,NULL,'0',NULL,0),
('P/1/186','MAHADEV TEXTILES','S.NO.E-227 3rd FLOR SUMEL BUSINESS PARK-3 RAIPUR','AHEMDABAD','GUJARAT','','AH292',NULL,NULL,'0','CR','P/AHM/186','0',NULL,NULL,'0',NULL,0),
('P/1/187','MOHINI ENTERPRISES','ATTARWALA BUILDING 1 st FLOORKULUPUR','AHEMDABAD','GUJARAT','','AH293',NULL,NULL,'0','CR','P/AHM/187','0',NULL,NULL,'0',NULL,0),
('P/1/188','SUNIL FASHION','256,MANEK CHOWK','AHEMDABAD','GUJARAT','','AH294',NULL,NULL,'0','CR','P/AHM/188','0',NULL,NULL,'0',NULL,0),
('P/1/189','GULAB CHAND FAKIR CHAND','2056,MANEK CHOWK','AHEMDABAD','GUJARAT','','AH296',NULL,NULL,'0','CR','P/AHM/189','0',NULL,NULL,'0',NULL,0),
('P/1/19','K.SURENDRAKUMAR','11,SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','07922165216','AH107','','24AMNPK4507F1Z7','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/190','ART CREATION','SUGNOMAL MARKET REVDI BAZAR','AHEMDABAD','GUJARAT','','AH297',NULL,NULL,'0','CR','P/AHM/190','0',NULL,NULL,'0',NULL,0),
('P/1/191','SATRANGI FASHION','111,BBC MARKETPANCHKUA','AHEMDABAD','GUJARAT','9998477235','AH185',NULL,NULL,'0','CR','P/AHM/191','0',NULL,NULL,'0',NULL,0),
('P/1/192','VIJAY ENTERPRIES','256 RATANPLOE GOLWAD','AHEMDABAD','GUJARAT','','DE221',NULL,NULL,'0','CR','P/AHM/192','0',NULL,NULL,'0',NULL,0),
('P/1/193','SHIVSHAKTI HANDICRAFT','E-51,2nd FLOR SUMEL BUSINESS PARK(SAFAL-1)NR.NCM.SARANGPUR','AHEMDABAD','GUJARAT','9227200539,9327015143','AH193',NULL,NULL,'0','CR','P/AHM/193','0',NULL,NULL,'0',NULL,0),
('P/1/194','MUNAVVA HUSAIN GULAM RASUL','2433,RANI,S HAJIRA MANEK CHOWKSARFRAZ SHAIKH MUNAVAR HUSAIN','AHEMDABAD','GUJARAT','9427027973','AH252',NULL,NULL,'0','CR','P/AHM/194','0',NULL,NULL,'0',NULL,0),
('P/1/195','K.RAJKUMAR','B/416,SUMMEL BUSINESS PARK-3OPP.NCM RAIPUR','AHEMDABAD','GUJARAT','9726806920','AH246',NULL,NULL,'0','CR','P/AHM/195','0',NULL,NULL,'0',NULL,0),
('P/1/196','TALREJA BROTHERS','E-15,16,17,18,GHANTAKARNA NAHVIR MARKET,SARANGPUR','AHEMDABAD','GUJARAT','079-22122426,9879918447','AH254',NULL,NULL,'0','CR','P/AHM/196','0',NULL,NULL,'0',NULL,0),
('P/1/197','AAAA','SSSSSSFFFFFFFFFF','AHEMDABAD','GUJARAT','','','','','0','CR','','0','','','','PURCHASE IN MP',1),
('P/1/198','OPENING SUTING','','AHEMDABAD','GUJARAT','','','','','0','CR','','0','','','','PURCHASE O MP',1),
('P/1/199','OPENING SHiRTING','','AHEMDABAD','GUJARAT','','','','','0','CR','','0','','','','PURCHASE O MP',1),
('P/1/2','MOHAN ENTERPRISRS','PANCKUWA','AHEMDABAD','GUJARAT','','AH280',NULL,NULL,'0','CR','P/AHM/2','0',NULL,NULL,'0',NULL,0),
('P/1/20','R.KHUSHAL','39,SUGNOMAL MARKET','AHEMDABAD','GUJARAT','30222881','AH109',NULL,NULL,'0','CR','P/AHM/20','0',NULL,NULL,'0',NULL,0),
('P/1/200','OPENING DRESS MATERIEL','','AHEMDABAD','GUJARAT','','','','','0','CR','','0','','','','PURCHASE O MP',1),
('P/1/201','MEHTA SYNTHETICS','80/81 ANAND SHOPING CENTRERATANPOLE','AHEMDABAD','GUJARAT','','','','24AEXPM18889C1Z3','0','CR','','0','9825093049','','','PURCHASE O MP',1),
('P/1/202','SHIVALI BOUTIQUE','E-408 4th FLOORSUMELIPUR','AHEMDABAD','GUJARAT','079-67123456','','','24AAIHD8565R1Z1','0','CR','','0','80000043210','','','PURCHASE 12%',1),
('P/1/203','COTTON PLUS','7 VRUNDAVAN SHOPINGCENTERRATANPLOE','AHEMDABAD','GUJARAT','079-25350352','pravinsheth99@gmail.com','AAEFC1642G','24AAEFC1642G1ZY','0','CR','','0','9825019371','','','PURCHASE 5%',1),
('P/1/204','S.B.ENTERPRISES','72,ANAND SHOPING CENTERRATNPOLE','AHEMDABAD','GUJARAT','079-25354575','','','24AFQPS1728A1ZK','0','CR','','0','937467694,9825878312','','','PURCHASE 12%',1),
('P/1/205','SHIVAM FASHION','E-239O E-246 2nd FLOK-3OPP.NEW CLOTH MARKERTRAIPUR','AHEMDABAD','GUJARAT','','','','24AACHG3172R1ZO','0','CR','','0','','','','PURCHASE 12%',1),
('P/1/206','VATS ENTERPRISES','138,ANAND SHOPING CENTER 1st FLOORGOLWAD RATANPLOE','AHEMDABAD','GUJARAT','079-25352606','','','24AWMPD5260D1ZV','0','CR','','0','9427626376','','','PURCHASE 5%',1),
('P/1/207','SHIVALI CREATION','E-348,SUMEL BUSINESS PARK-3OPP.NEW CLOTH MRKETRAIPUR','AHEMDABAD','GUJARAT','','accounts@shivalifashion.com','','24ABKPG1568L1ZH','0','CR','','0','','','','PURCHASE 12%',1),
('P/1/208','GOSSIP THE FASHION HUB','','AHEMDABAD','GUJARAT','','','','24AAMFG9810G1ZC','0','CR','','0','','','','PURCHASE 5%',1),
('P/1/21','DEEPAKKUMAR TULSIDAS','52,SUGNOMAL MARKET','AHEMDABAD','GUJARAT','21139689,9879658111','AH110',NULL,NULL,'0','CR','P/AHM/21','0',NULL,NULL,'0',NULL,0),
('P/1/22','S.B.TEXTILE','66,ANAND SHOPPING CEE','AHEMDABAD','GUJARAT','25354575','AH113','','24AAFHS3945D1Z9','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/1/24','K.MAHENDRAKUMAR','ASHOK MANSIONKALUPURKOTNI RANG','AHEMDABAD','GUJARAT','22136307','AH116','','24ABBPJ6377R1Z2','0','CR','','0','',NULL,'0','null',1),
('P/1/25','K.P.TEXTILE','34,G/F,HARIOM A/C MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','55240251','AH117',NULL,NULL,'0','CR','P/AHM/25','0',NULL,NULL,'0',NULL,0),
('P/1/26','MINESH CUTPIECE CENTRE','1, SUGNOMAL CLOTH MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','25357435,9924979987','AH119','','24ACPPS1538J1Z8','0','CR','','0','',NULL,'0','PURCHASE 5%',1),
('P/1/27','LALCHAND THAKURDAS','100,JUMANI MARKET REVDI BAZAR','AHEMDABAD','GUJARAT','22132448','AH121',NULL,NULL,'0','CR','P/AHM/27','0',NULL,NULL,'0',NULL,0),
('P/1/28','D.R.FABRICS','563,REID ROAD,OUTSIDE PANCHKUVA GATE,','AHEMDABAD','GUJARAT','22141199','AH122',NULL,NULL,'0','CR','P/AHM/28','0',NULL,NULL,'0',NULL,0),
('P/1/29','AKAYLONE SYNTEX','9,PARVATI PART-1,NAGORI POLERATANPOLE','AHEMDABAD','GUJARAT','30221139','AH123',NULL,NULL,'0','CR','P/AHM/29','0',NULL,NULL,'0',NULL,0),
('P/1/3','NANCY','C-10,YOGESHWAR STATEOAD,NIOL KATHWADA','AHEMDABAD','GUJARAT','29292795','AH177','','24AOZPS4775F1Z5','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/30','D.J.TEXTILE','163-64,SINDHI MARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','09374825920','AH126',NULL,NULL,'0','CR','P/AHM/30','0',NULL,NULL,'0',NULL,0),
('P/1/31','GLORY','316,VANIJYA BHAWAN,D.B.ROADKANKARIA','AHEMDABAD','GUJARAT','9426767241','AH132',NULL,NULL,'0','CR','P/AHM/31','0',NULL,NULL,'0',NULL,0),
('P/1/32','KENZER','47,G/F,SUGNOMAL MARKET','AHEMDABAD','GUJARAT','22141786','AH133',NULL,NULL,'0','CR','P/AHM/32','0',NULL,NULL,'0',NULL,0),
('P/1/33','K.NARESHKUMAR N X','521/1,OPP.PANCHKUVA KAPADMAHAJAN BUILDING','AHEMDABAD','GUJARAT','22143450','AH135',NULL,NULL,'0','CR','P/AHM/33','0',NULL,NULL,'0',NULL,0),
('P/1/34','SHREE GAYATRI HANDICRAFT','120,1st FLOORSUGNOMAL MARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','30222886,9374234334','AH141',NULL,NULL,'0','CR','P/AHM/34','0',NULL,NULL,'0',NULL,0),
('P/1/35','R.J.TEXTILE','486,CELLER,SUGNOMAL MARKET','AHEMDABAD','GUJARAT','','AH142',NULL,NULL,'0','CR','P/AHM/35','0',NULL,NULL,'0',NULL,0),
('P/1/36','RAHUL FABRICS (BANJARA)','A/88,JUMANI BAZAR,SINDHI MARKEREVDI BAZAR,','AHEMDABAD','GUJARAT','9898398525,9824080201','AH148',NULL,NULL,'0','CR','P/AHM/36','0',NULL,NULL,'0',NULL,0),
('P/1/37','A.KETAN CORPORATION (VIMAL)','469/19,PARSI CHAWL,SAKAR BAZARKALUPUR','AHEMDABAD','GUJARAT','22177314','AH149',NULL,NULL,'0','CR','P/AHM/37','0',NULL,NULL,'0',NULL,0),
('P/1/38','R.S.TEXTILE','1& 201,V.P. SILK MARKETPANCHKUVA','AHEMDABAD','GUJARAT','9328078171','AH153',NULL,NULL,'0','CR','P/AHM/38','0',NULL,NULL,'0',NULL,0),
('P/1/39','G.S.TRADERS','2487/3,BADSHAHS HAJIRAMANEK CHOWK','AHEMDABAD','GUJARAT','2113449','AH154','','24ABDPG4781E1ZY','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/1/4','RAM SHANKAR NX','456,SAFEL-3','AHEMDABAD','GUJARAT','','AH288',NULL,NULL,'0','CR','P/AHM/4','0',NULL,NULL,'0',NULL,0),
('P/1/40','SAHELI SELECTIOIN','2433,RANI HAJIRA,SMANEK CHOWK','AHEMDABAD','GUJARAT','9427027973','AH155',NULL,NULL,'0','CR','P/AHM/40','0',NULL,NULL,'0',NULL,0),
('P/1/41','SWASTIK CORPORATION','529/15/1,OPP.VICTORIA JUBILEEHOSPITAL,PANCHKUVA','AHEMDABAD','GUJARAT','07922149162/22143666','AH158',NULL,NULL,'0','CR','P/AHM/41','0',NULL,NULL,'0',NULL,0),
('P/1/42','RESHAM TEXTILES','HARIOM MARKET 3,rd FLOOR','AHEMDABAD','GUJARAT','','AH161',NULL,NULL,'0','CR','P/AHM/42','0',NULL,NULL,'0',NULL,0),
('P/1/43','ANAND TRADING CO.','239,2nd FLOOR,SHRI RAM CLOTHMARKET CROSS LANE','AHEMDABAD','GUJARAT','9426759921(M) 22113991','AH159',NULL,NULL,'0','CR','P/AHM/43','0',NULL,NULL,'0',NULL,0),
('P/1/44','ASHISH SYNTHETICS','24,ANAND SHOPPING CENTERGOLWAD,RATANPOLE','AHEMDABAD','GUJARAT','','AH160',NULL,NULL,'0','CR','P/AHM/44','0',NULL,NULL,'0',NULL,0),
('P/1/45','SHA TARACHAND CHUNILAL','NAYA MADUPURA ROADDARIYAPUR DARWAJA','AHEMDABAD','GUJARAT','','AH162',NULL,NULL,'0','CR','P/AHM/45','0',NULL,NULL,'0',NULL,0),
('P/1/46','K.MAHESHKUMAR INDUS.P.L(HARRA)','HASEJAA HOUSE,OPP.MASKATIMARKET,SAKAR BAZAR','AHEMDABAD','GUJARAT','079-22130666,09328230500','AH165',NULL,NULL,'0','CR','P/AHM/46','0',NULL,NULL,'0',NULL,0),
('P/1/47','SHEEBA INTERNATIONAL','G/F,KHUSHIRAM BHAVANKALUPUR KOTNI RANG','AHEMDABAD','GUJARAT','','AH166',NULL,NULL,'0','CR','P/AHM/47','0',NULL,NULL,'0',NULL,0),
('P/1/48','K.RAJSHREE','A-17&102,jumani bazarSINDHI MARKET','AHEMDABAD','GUJARAT','22132422,9328213699','AH174',NULL,NULL,'0','CR','P/AHM/48','0',NULL,NULL,'0',NULL,0),
('P/1/49','K.BHUMIKA','241,2,ND FLOOR,B.B.C. MARKET','AHEMDABAD','GUJARAT','','AH178',NULL,NULL,'0','CR','P/AHM/49','0',NULL,NULL,'0',NULL,0),
('P/1/5','K.HARISH KUMAR','S.NO.1260,PANCHKUVA','AHEMDABAD','GUJARAT','','AH295',NULL,NULL,'0','CR','P/AHM/5','0',NULL,NULL,'0',NULL,0),
('P/1/50','KARISHMA INTERNATIONAL(K.RAJSR','A-17&102,JUMANI BAZARREVDI BAZAR','AHEMDABAD','GUJARAT','','AH179',NULL,NULL,'0','CR','P/AHM/50','0',NULL,NULL,'0',NULL,0),
('P/1/51','DAHYABHAI VANMALIDAS LAVSI','OUTSIDE PANCHKUVA GATE','AHEMDABAD','GUJARAT','22174607','AH180',NULL,NULL,'0','CR','P/AHM/51','0',NULL,NULL,'0',NULL,0),
('P/1/52','SANJIT TEXTILE','OUTSIDE PANCHKUVA GATE','AHEMDABAD','GUJARAT','','AH181',NULL,NULL,'0','CR','P/AHM/52','0',NULL,NULL,'0',NULL,0),
('P/1/53','PRASANG COLLECTION','188-189,SUGNOMAL MARARKETREVDI BAZAR','AHEMDABAD','GUJARAT','22159922,30249922,66079922','AH183','','24ABBPA3969Q1ZD','0','CR','','0','9879175800,9426636063',NULL,'0','PURCHASE O MP',1),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','RANI KA HAJIRAMANEKCHOWK','AHEMDABAD','GUJARAT','','AH184','','24AINPS8959B1ZR','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/1/55','JAI AMBE CREATION','56,G/F,SUGNOMAL MARKET','AHEMDABAD','GUJARAT','','AH187',NULL,NULL,'0','CR','P/AHM/55','0',NULL,NULL,'0',NULL,0),
('P/1/56','K.B.COLLECTION','B-19,4th FLOOR,SUMEL BUSINESSPARK,B/H N.C.M.,RAIPUR','AHEMDABAD','GUJARAT','079-22163893,9879353471','AH188',NULL,NULL,'0','CR','P/AHM/56','0',NULL,NULL,'0',NULL,0),
('P/1/57','MANOJKUMAR TULSIDAS','52,SUGNOMAL MARKET','AHEMDABAD','GUJARAT','','AH190',NULL,NULL,'0','CR','P/AHM/57','0',NULL,NULL,'0',NULL,0),
('P/1/58','SHREE GANPATI TRADERS(SHE GIRL','G.F.NETAJI CLOTH MARKETKALUPUR KOTNI RANG','AHEMDABAD','GUJARAT','','AH196',NULL,NULL,'0','CR','P/AHM/58','0',NULL,NULL,'0',NULL,0),
('P/1/59','YES COLLECTION (R.J.TEX.)','486,CELLER,SUGNOMAL','AHEMDABAD','GUJARAT','','AH197',NULL,NULL,'0','CR','P/AHM/59','0',NULL,NULL,'0',NULL,0),
('P/1/6','PATEL GIRISHCHAND DAHYABHAI','2436,RANI;S HAZIROMANEK CHOWK','AHEMDABAD','GUJARAT','9586746176,9825578761','AH251',NULL,NULL,'0','CR','P/AHM/6','0',NULL,NULL,'0',NULL,0),
('P/1/60','V.V.FABRICS','70,ANAND SHOPPING CENTERGOLWAD,RATANPOLE','AHEMDABAD','GUJARAT','09879693661(ASHOKJI)','AH198',NULL,NULL,'0','CR','P/AHM/60','0',NULL,NULL,'0',NULL,0),
('P/1/61','AMIT CREATION','D-1,JUMANI BAZAR,SINHI MARKET','AHEMDABAD','GUJARAT','','AH200',NULL,NULL,'0','CR','P/AHM/61','0',NULL,NULL,'0',NULL,0),
('P/1/62','PAGHDIWALA G.M.&CO.','2432/1,RANI,S HAJIRA,MANEK CHOWK','AHEMDABAD','GUJARAT','07922141913','AH201',NULL,NULL,'0','CR','P/AHM/62','0',NULL,NULL,'0',NULL,0),
('P/1/63','SHRI AMBAJI DRESSES','SHOP NO.34, PANCHKUVASINDHI MARKET','AHEMDABAD','GUJARAT','07930424865','AH202',NULL,NULL,'0','CR','P/AHM/63','0',NULL,NULL,'0',NULL,0),
('P/1/64','SALIM ABDUL KARIM HOKABAJ','MOTI VOHARWAD,KAZI,S DHABA,ASTODIA,','AHEMDABAD','GUJARAT','','AH203',NULL,NULL,'0','CR','P/AHM/64','0',NULL,NULL,'0',NULL,0),
('P/1/65','R.RAMANLAL & CO. (S.B.)','71,ANNAND SHOPPING CENTERRATANPOLE','AHEMDABAD','GUJARAT','','AH204',NULL,NULL,'0','CR','P/AHM/65','0',NULL,NULL,'0',NULL,0),
('P/1/66','SAMYAK TRADING CO.(CHIRAG BHAI','RANI NI HAJIRO,MANHEKCHOWK','AHEMDABAD','GUJARAT','','AH205',NULL,NULL,'0','CR','P/AHM/66','0',NULL,NULL,'0',NULL,0),
('P/1/67','MALVIKA EMPORIUM','106-108,PANCHKUVA SINDHIMARKET','AHEMDABAD','GUJARAT','','AH208',NULL,NULL,'0','CR','P/AHM/67','0',NULL,NULL,'0',NULL,0),
('P/1/68','MANISH TRADERS','M.AYUSH OPP.REVDI BAZAR POST OOFF.CORNER WALA','AHEMDABAD','GUJARAT','','AH209',NULL,NULL,'0','CR','P/AHM/68','0',NULL,NULL,'0',NULL,0),
('P/1/69','K.MANTHAN','SHOP NO.227,2ND FLOOR','AHEMDABAD','GUJARAT','','AH210',NULL,NULL,'0','CR','P/AHM/69','0',NULL,NULL,'0',NULL,0),
('P/1/7','MAHADEV ENTERPRISES','880,SAFEL-3','AHEMDABAD','GUJARAT','','AH275',NULL,NULL,'0','CR','P/AHM/7','0',NULL,NULL,'0',NULL,0),
('P/1/70','AMBIKA TEXTILES','','AHEMDABAD','GUJARAT','','AH211',NULL,NULL,'0','CR','P/AHM/70','0',NULL,NULL,'0',NULL,0),
('P/1/71','K.MAHESHKUMAR INDUS.P.L.(KBC)','HASEJAA HOUSE,OPP.MASKATI MARKET,SAKAR BAZAR','AHEMDABAD','GUJARAT','079-22130666','AH212',NULL,NULL,'0','CR','P/AHM/71','0',NULL,NULL,'0',NULL,0),
('P/1/72','M.T.INTERNATIONAL','157-158,1 st FLOOR,SUGNOMALMARKET,REVDI BAZAR,','AHEMDABAD','GUJARAT','30249906,9879175442','AH213',NULL,NULL,'0','CR','P/AHM/72','0',NULL,NULL,'0',NULL,0),
('P/1/73','K.KHUSHI(KHUSHBOO)','REVDI BAZAR,RELEIF ROAD,','AHEMDABAD','GUJARAT','9909182302','AH215',NULL,NULL,'0','CR','P/AHM/73','0',NULL,NULL,'0',NULL,0),
('P/1/74','G.A.TEXTILE(KASBA)','SHOP NO.42,3rd FLOOR,HIRABHAIMARKET,DIWAN BALLUBHAI MRG.,','AHEMDABAD','GUJARAT','079-25467429,9328260492','AH216',NULL,NULL,'0','CR','P/AHM/74','0',NULL,NULL,'0',NULL,0),
('P/1/75','MAITRI','C-83,84 SUMEL BUSINE.SCHOL ROD KANKARIA','AHEMDABAD','GUJARAT','079-25467150,9377730064','AH217','','24AAGCM4427L2ZB','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/1/76','CRAZY WAVES FABRICS','9,PARWATI PART-1,NAGORIPOLERATANPOLE','AHEMDABAD','GUJARAT','','AH218',NULL,NULL,'0','CR','P/AHM/76','0',NULL,NULL,'0',NULL,0),
('P/1/77','DEEPALI DRISHTI CLOTH MARCHANT','52,GROUND FLOOR,SUGNOMAL MARKET,REVDI BAZAR,','AHEMDABAD','GUJARAT','22113968,9879658111','AH219',NULL,NULL,'0','CR','P/AHM/77','0',NULL,NULL,'0',NULL,0),
('P/1/78','PARSHWANATH CORPORATION','529/15/1,OPP.VICTORIA JUBILEEHOSPITAL,PANCHKUWA,','AHEMDABAD','GUJARAT','22149162,22143666','AH220',NULL,NULL,'0','CR','P/AHM/78','0',NULL,NULL,'0',NULL,0),
('P/1/79','SNEHA SYNTHETICS','676/62,DHANLAXMI MARKET,CROSS LANE,REVDI BAZAR,','AHEMDABAD','GUJARAT','22141625','AH127',NULL,NULL,'0','CR','P/AHM/79','0',NULL,NULL,'0',NULL,0),
('P/1/8','GAYATRI TEXTILES','236,HARI OM A/C MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','','AH270',NULL,NULL,'0','CR','P/AHM/8','0',NULL,NULL,'0',NULL,0),
('P/1/80','LAVANYA SUITS','D-1-JAMUNI BAZAR,SINDHI MARKETKALUPUR,','AHEMDABAD','GUJARAT','22162382,9909428030','AH221',NULL,NULL,'0','CR','P/AHM/80','0',NULL,NULL,'0',NULL,0),
('P/1/81','LILARAM THADHARAM','A/22,JUMANI BAZAR,SINDHIMARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','09374159153ASHOK BHAI','AH222',NULL,NULL,'0','CR','P/AHM/81','0',NULL,NULL,'0',NULL,0),
('P/1/82','PARESH ENTERPRISE','529/13,BHAGWATI CLOTH MARKET,PANCHKUWA','AHEMDABAD','GUJARAT','22148738','AH156',NULL,NULL,'0','CR','P/AHM/82','0',NULL,NULL,'0',NULL,0),
('P/1/83','K.SHAAN CREATION','137,1st FLOOR,SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','30244663,9913666644','AH224',NULL,NULL,'0','CR','P/AHM/83','0',NULL,NULL,'0',NULL,0),
('P/1/84','K.K.FABRICS','54,GROUND FLOOR,SUGNOMAL MARKEREVDI BAZAR','AHEMDABAD','GUJARAT','22111238,9879353471','AH225',NULL,NULL,'0','CR','P/AHM/84','0',NULL,NULL,'0',NULL,0),
('P/1/85','SATYAM CORPORATION','2024,HARIOM MARKET,NO.2REID ROAD,PANCHKUWA','AHEMDABAD','GUJARAT','22112178,22113211','AH226',NULL,NULL,'0','CR','P/AHM/85','0',NULL,NULL,'0',NULL,0),
('P/1/86','HARI-OM PRIYANKA FASHION','14,GROUND FLOOR,SUNGNOMAL MARKNR.HARIOM MARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','22170185','AH227',NULL,NULL,'0','CR','P/AHM/86','0',NULL,NULL,'0',NULL,0),
('P/1/87','SHRIPAL TEXTILE','','AHEMDABAD','GUJARAT','','AH151',NULL,NULL,'0','CR','P/AHM/87','0',NULL,NULL,'0',NULL,0),
('P/1/88','KANKU FASHIONS','BLOCK C,4th FLOOR,SO.NO.F-2,SUMEL BUSINESS PARK,SARANPUR,','AHEMDABAD','GUJARAT','079-30261026,09327055444','AH108',NULL,NULL,'0','CR','P/AHM/88','0',NULL,NULL,'0',NULL,0),
('P/1/89','RADHIKA CREATION','162,1st FLOOR,SUGNOMAL MARKET,NR.HARIOM(AC)MARKT,REVDI BAZAR','AHEMDABAD','GUJARAT','30244760,9374768876','AH111',NULL,NULL,'0','CR','P/AHM/89','0',NULL,NULL,'0',NULL,0),
('P/1/9','FASHION PALACE','2439,RANINO HAJIRO ,MANAK CHOCK(BOB A/C NO.03280100017949','AHEMDABAD','GUJARAT','','AH199',NULL,NULL,'0','CR','P/AHM/9','0',NULL,NULL,'0',NULL,0),
('P/1/90','SURENDRAKUMAR KUNDANMAL','40,SUGNOMAL MARKET,NR.HARIOMMARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','22165216,9537866020','AH112',NULL,NULL,'0','CR','P/AHM/90','0',NULL,NULL,'0',NULL,0),
('P/1/91','K.P.RAKESHKUMAR','178/179,SINDHI MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','079-22121120','AH115',NULL,NULL,'0','CR','P/AHM/91','0',NULL,NULL,'0',NULL,0),
('P/1/92','PRINCESS DRESSES','260,2nd FLOOR,SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','30434757,9924188180','AH118',NULL,NULL,'0','CR','P/AHM/92','0',NULL,NULL,'0',NULL,0),
('P/1/93','K.S.TEXTILE','54,GROUND FLOOR,SUGNOMAL MARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','30221238,9879353471','AH120',NULL,NULL,'0','CR','P/AHM/93','0',NULL,NULL,'0',NULL,0),
('P/1/94','S.VASDEV','103,JUMANI BAZARSINDHI MARKET','AHEMDABAD','GUJARAT','079-22159168,09327692977','AH124',NULL,NULL,'0','CR','P/AHM/94','0',NULL,NULL,'0',NULL,0),
('P/1/95','SAINATH CREATION','132,(GALI)1st FLOOR,SUGNOMALMARKET,REVDI BAZAR','AHEMDABAD','GUJARAT','9879367121,9825745282','AH125',NULL,NULL,'0','CR','P/AHM/95','0',NULL,NULL,'0',NULL,0),
('P/1/96','OM SAI CREATION','362/1,3rd FLOOR,SUGNOMALMARKET REVDI BAZAR','AHEMDABAD','GUJARAT','9327732752','AH128',NULL,NULL,'0','CR','P/AHM/96','0',NULL,NULL,'0',NULL,0),
('P/1/97','ASHIRWAD CREATION','286,2nD FLOOR SUGNOMAL MARKETREVDI BAZAR','AHEMDABAD','GUJARAT','30244298,9662228270','AH129',NULL,NULL,'0','CR','P/AHM/97','0',NULL,NULL,'0',NULL,0),
('P/1/98','R.SONS','SNO.8 G/F SUGNOMAL MARKET,NR.HARIOM(AC) MARKET REVDI BAZAR','AHEMDABAD','GUJARAT','9328357560,9468601844','AH130',NULL,NULL,'0','CR','P/AHM/98','0',NULL,NULL,'0',NULL,0),
('P/1/99','KASBA PARIDHAN PVT.LTD.','41,42,3rd FLOOR HIRABHAI MRK.DIWAN BALLU BHAI MARG,KANKARIA','AHEMDABAD','GUJARAT','07925467429','AH131',NULL,NULL,'0','CR','P/AHM/99','0',NULL,NULL,'0',NULL,0),
('P/10/8','GULATI ARTS','NEAR KASHI NATH SETH JEWELLERS1st fLOOR,MEHRA MRK.BARA BAZAR','BAREILY','UTTAR PRADESH','0581-3294356,9837006125','BR105',NULL,NULL,'0','CR','P/BAR/8','0',NULL,NULL,'0',NULL,0),
('P/11/1','KARTAR FABRICS','25,KUNWAR COMPLEXSTATION ROAD','MORADABAD','UTTAR PRADESH','9927071981,9837293001','MO101',NULL,NULL,'0','CR','P/MBD/1','0',NULL,NULL,'0',NULL,0),
('P/12/1','DA111','36.M.GHANDHI MARKET,KING,S CIRCLE,','MUMBAI','MAHARASHTRA','','DA111',NULL,NULL,'0','CR','P/MUM/1','0',NULL,NULL,'0',NULL,0),
('P/12/10','SUPER TEXTILES','30,KAKAD A/C MARKET,','MUMBAI','MAHARASHTRA','2205219','MU107',NULL,NULL,'0','CR','P/MUM/10','0',NULL,NULL,'0',NULL,0),
('P/12/11','MEERA SILK STORE','SO.NO.128,M.GANDHI MARKET,KING,S CIRCLE','MUMBAI','MAHARASHTRA','24024412','MU108',NULL,NULL,'0','CR','P/MUM/11','0',NULL,NULL,'0',NULL,0),
('P/12/12','H.HITENDRA & CO.','622,DWARKESH GULLY,M.J.MARKET,','MUMBAI','MAHARASHTRA','22428829','MU109',NULL,NULL,'0','CR','P/MUM/12','0',NULL,NULL,'0',NULL,0),
('P/12/13','HASSANAND DESIGNER STUDIO','133/134,GHADIAL GALLY,M.J.MARKET','MUMBAI','MAHARASHTRA','22401782','MU110',NULL,NULL,'0','CR','P/MUM/13','0',NULL,NULL,'0',NULL,0),
('P/12/14','DINFAB VAKHARIA PRINTS','76/86,LAXMI BHUVAN,OLD HANUMANLANE,KALBADEVI','MUMBAI','MAHARASHTRA','9324471639,9987059639','MU111',NULL,NULL,'0','CR','P/MUM/14','0',NULL,NULL,'0',NULL,0),
('P/12/15','SONALI PRINTS','64/A,JASRA BHAVAN,R.NO.3 IstFLOR OLD HANUMAN LANE','MUMBAI','MAHARASHTRA','9322235911','MU112',NULL,NULL,'0','CR','P/MUM/15','0',NULL,NULL,'0',NULL,0),
('P/12/16','AMAR ENTERPRISE','52/54,PARVATI BHAVAN ROOM NO.14,1st FLOR OLD HANUMAN LANE','MUMBAI','MAHARASHTRA','22408206,9820255877','MU113',NULL,NULL,'0','CR','P/MUM/16','0',NULL,NULL,'0',NULL,0),
('P/12/17','ANMOL COLLECTION','426,GAUMUKH GALLI,M,J,MARKET','MUMBAI','MAHARASHTRA','22414669,','MU114',NULL,NULL,'0','CR','P/MUM/17','0',NULL,NULL,'0',NULL,0),
('P/12/18','SEIKQ. SAREES','131/133,OLD HANUMAN LANE,1stFLOOR,KALBADEVI RAOD','MUMBAI','MAHARASHTRA','22050914,09892460854','MU115',NULL,NULL,'0','CR','P/MUM/18','0',NULL,NULL,'0',NULL,0),
('P/12/19','ARBUDA TEXTILES','MUNISUVRAT COMPOUND,PHASE-3,DWING 2nd FLOR GALA NO.211,RENL','MUMBAI','MAHARASHTRA','09699611125,09413649562','MU116',NULL,NULL,'0','CR','P/MUM/19','0',NULL,NULL,'0',NULL,0),
('P/12/2','BHAWANI CREATIONS','80,M.GHANDHI MARKETKING,S CIRCLE','MUMBAI','MAHARASHTRA','','DA112',NULL,NULL,'0','CR','P/MUM/2','0',NULL,NULL,'0',NULL,0),
('P/12/20','NEETA FASHION','155-B,RANGARI CHAWL,SNO.7 DADASAHEB PHALKE ROAD DADAR','MUMBAI','MAHARASHTRA','9920601085','MU117',NULL,NULL,'0','CR','P/MUM/20','0',NULL,NULL,'0',NULL,0),
('P/12/21','MATULYA TEXTILES','1/5,RAJDACHAWL 2nd FLOR ROOM NO.12 OLD HANUMAN KALBADEVI','MUMBAI','MAHARASHTRA','9323871808,9930410474','MU118',NULL,NULL,'0','CR','P/MUM/21','0',NULL,NULL,'0',NULL,0),
('P/12/3','DEEPAK TEXTILES (KARACHIWALA)','MADINA BLDG,R.NO.27,1,ST FLR,C, BLOCK,MUSAFHIRKHANA ROAD','MUMBAI','MAHARASHTRA','','MU101',NULL,NULL,'0','CR','P/MUM/3','0',NULL,NULL,'0',NULL,0),
('P/12/4','ASHIKA SAREES LTD.','117/119,OLD HANUMAN LANE','MUMBAI','MAHARASHTRA','','MU102',NULL,NULL,'0','CR','P/MUM/4','0',NULL,NULL,'0',NULL,0),
('P/12/5','NARBHERAMMOTILALSILKS PVT.LTD.','201/F,SWADESHI MKT.BLDG.113,CAVEL STREET','MUMBAI','MAHARASHTRA','9820158786,9820202900','MU103',NULL,NULL,'0','CR','P/MUM/5','0',NULL,NULL,'0',NULL,0),
('P/12/6','ISHWAR EMBROIDERS','146.M.G.MARKET','MUMBAI','MAHARASHTRA','','DA116',NULL,NULL,'0','CR','P/MUM/6','0',NULL,NULL,'0',NULL,0),
('P/12/7','BHARAT VASTRALAYA (KARISHMA)','33/34,SHAMMI GALLY,SWADESHIMARKET,MAZENINE FLOOR','MUMBAI','MAHARASHTRA','','MU104',NULL,NULL,'0','CR','P/MUM/7','0',NULL,NULL,'0',NULL,0),
('P/12/8','GITESH TEXTILES (KARISHMA)','33/34,SHAMMI GALLY,SWADESHI MARKET','MUMBAI','MAHARASHTRA','','MU105',NULL,NULL,'0','CR','P/MUM/8','0',NULL,NULL,'0',NULL,0),
('P/12/9','DINTEX FABRICS','76/86,LAXMI BHUVAN,OLD HANUMANLANE,KALABADEVI,','MUMBAI','MAHARASHTRA','','MU106',NULL,NULL,'0','CR','P/MUM/9','0',NULL,NULL,'0',NULL,0),
('P/15/1','CHIRALA HANDLOOMS (P) LTD','RATNA NAGAR,RAMAKRISHNAPURAM','CHIRALA','TAMILNADU','08594-232325','CH101',NULL,NULL,'0','CR','P/CHI/1','0',NULL,NULL,'0',NULL,0),
('P/15/2','K.SURESH HANDLOOMS','5,SRI VYSHNAVI TOWERS,POLIMERA ROAD','CHIRALA','TAMILNADU','9346712327','CH102',NULL,NULL,'0','CR','P/CHI/2','0',NULL,NULL,'0',NULL,0),
('P/15/3','SRI UNADEVI HANDLOOMS','POLIMERA ROAD','CHIRALA','TAMILNADU','08594234435','CH103',NULL,NULL,'0','CR','P/CHI/3','0',NULL,NULL,'0',NULL,0),
('P/15/4','SRIDEVI HANDLOOMS','BEHIND B.V.S. MARKET ,NEAR M.G.C.MARKET,','CHIRALA','TAMILNADU','08594232938','CH104',NULL,NULL,'0','CR','P/CHI/4','0',NULL,NULL,'0',NULL,0),
('P/15/5','KRISHNA HANDLOOMS','D.NO.14-19-136,JALEDI COMPLEX((CELLAR) R.R.ROAD','CHIRALA','TAMILNADU','08594-232968,9346466811','CH105',NULL,NULL,'0','CR','P/CHI/5','0',NULL,NULL,'0',NULL,0),
('P/16/1','SATYANARAYANRAJGOPAL CHINDAK','618-20,VITHALDEV GALLISHAHAPUR','BELGAUM','KARNATAKA','0831-2488318','BE101',NULL,NULL,'0','CR','P/BEL/1','0',NULL,NULL,'0',NULL,0),
('P/16/2','M/S.LAXMINARAYAN & SONS','74,KHADE BAZARSHAHAPUR','BELGAUM','KARNATAKA','2425324','BE102',NULL,NULL,'0','CR','P/BEL/2','0',NULL,NULL,'0',NULL,0),
('P/16/3','SHREE SHYAM SAREES','BHARATNAGAR 3,rd CROSS,SHSHSPUR,','BELGAUM','KARNATAKA','2496559','BE103',NULL,NULL,'0','CR','P/BEL/3','0',NULL,NULL,'0',NULL,0),
('P/17/1','HANDICRAFT HOUSE','OPP.RAMPUR CLUB RAH E RAZA','RAMPUR','RAJASTHAN','9359304254.,9897530288','RP001',NULL,NULL,'0','CR','P/RAM/1','0',NULL,NULL,'0',NULL,0),
('P/18/1','SUTARIA SONS','C-3-29,NEW SUPER MARKET','JAMNAGAR','GUJARAT','2542727','JM101',NULL,NULL,'0','CR','P/JAM/1','0',NULL,NULL,'0',NULL,0),
('P/19/1','MEHTA TEXTILES','68,KRISHNA TALKIES ROAD','ERODE','','0424-2212087','ER101',NULL,NULL,'0','CR','P/ERO/1','0',NULL,NULL,'0',NULL,0),
('P/19/2','MANISH MILLS','SUKI TOWERS,261,EASWARAN KOVIL STREET','ERODE','','04242250352,9443025635','ER102',NULL,NULL,'0','CR','P/ERO/2','0',NULL,NULL,'0',NULL,0),
('P/2/1','GARDEN SILK MILLS LTD.','M-1270-71,GROUND FLOORSURAT TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','2337912','SU101',NULL,NULL,'0','CR','P/SUR/1','0',NULL,NULL,'0',NULL,0),
('P/2/10','HEENA TEX FAB PVT.LTD.','A-1012-13,R.K.T.MARKET,RING ROAD','SURAT','GUJARAT','0261-2328616','SU122',NULL,NULL,'0','CR','P/SUR/10','0',NULL,NULL,'0',NULL,0),
('P/2/100','KING FAB','511,U/G,N.T.M.RING ROAD','SURAT','GUJARAT','2341765','SU319',NULL,NULL,'0','CR','P/SUR/100','0',NULL,NULL,'0',NULL,0),
('P/2/101','ROOP REKHA DESIGNER','640-641,ABHISHEK MARKET,RING ROAD,','SURAT','GUJARAT','02612202838','SU320',NULL,NULL,'0','CR','P/SUR/101','0',NULL,NULL,'0',NULL,0),
('P/2/102','TANVI SAREES (VARDHAN)','632,UPPER GROUND,ABHISHEKMARKET,RING ROAD,','SURAT','GUJARAT','02613003461','SU321',NULL,NULL,'0','CR','P/SUR/102','0',NULL,NULL,'0',NULL,0),
('P/2/103','AKHAND-DEEP FASHION','686,UPPER GROUND,ABHISHEKMARKET,RING ROAD,','SURAT','GUJARAT','02612331606','SU323',NULL,NULL,'0','CR','P/SUR/103','0',NULL,NULL,'0',NULL,0),
('P/2/104','VAMA SAREES(NARAYANI SAREES)','507,UPPER GROUND,SWADESHITEXTILES MARKET,RING ROAD','SURAT','GUJARAT','9427169615','SU325',NULL,NULL,'0','CR','P/SUR/104','0',NULL,NULL,'0',NULL,0),
('P/2/105','MANILA FASHION(NAVAL)','C-2028,SURAT TEXTILE MARKET,RING ROAD,','SURAT','GUJARAT','','SU326',NULL,NULL,'0','CR','P/SUR/105','0',NULL,NULL,'0',NULL,0),
('P/2/106','NITESH TEXTILES','ROYAL MARKETRING ROAD','SURAT','GUJARAT','','SU327',NULL,NULL,'0','CR','P/SUR/106','0',NULL,NULL,'0',NULL,0),
('P/2/107','ROHIT TEXTILE','R.K.T TEXTILE ARKETRING ROAD','SURAT','GUJARAT','','SU328',NULL,NULL,'0','CR','P/SUR/107','0',NULL,NULL,'0',NULL,0),
('P/2/108','ANANDI TEXTILE','UPPER GROUND ,KOHINOOR MARKET RING ROAD','SURAT','GUJARAT','','SU329',NULL,NULL,'0','CR','P/SUR/108','0',NULL,NULL,'0',NULL,0),
('P/2/109','TARIKA SILK MILLS','202,TRADE HOUSERING ROAD','SURAT','GUJARAT','','SU330',NULL,NULL,'0','CR','P/SUR/109','0',NULL,NULL,'0',NULL,0),
('P/2/11','KALA SHREE SYNTEX PVT.LTD.','G-5/6,SHANKAR MARKET,MOTI BEGUMWADI','SURAT','GUJARAT','2312149','SU134',NULL,NULL,'0','CR','P/SUR/11','0',NULL,NULL,'0',NULL,0),
('P/2/110','MANOJ FASHION','R.K.T RING ROAD','SURAT','GUJARAT','','SU331',NULL,NULL,'0','CR','P/SUR/110','0',NULL,NULL,'0',NULL,0),
('P/2/111','POONAM RAYONS','K.T.M,RING ROAD','SURAT','GUJARAT','','SU332',NULL,NULL,'0','CR','P/SUR/111','0',NULL,NULL,'0',NULL,0),
('P/2/112','NANDINI CREATION','ROHIT TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','','SU333',NULL,NULL,'0','CR','P/SUR/112','0',NULL,NULL,'0',NULL,0),
('P/2/113','ARCHIS TEXTILE','101,PASHUPATI TEXTILE MARKETRING ROAD','SURAT','GUJARAT','','SU334',NULL,NULL,'0','CR','P/SUR/113','0',NULL,NULL,'0',NULL,0),
('P/2/114','SIDDHI CREATION','1013,SHREE MAHAVIR TEXTILEMARKET RING ROAD','SURAT','GUJARAT','','SU335',NULL,NULL,'0','CR','P/SUR/114','0',NULL,NULL,'0',NULL,0),
('P/2/115','LAXMI FABRICS','2\"ND FLOOR, ROYAL MARKETRING ROAD','SURAT','GUJARAT','','SU336',NULL,NULL,'0','CR','P/SUR/115','0',NULL,NULL,'0',NULL,0),
('P/2/116','RADHIKA FABRICS','AJANTA SHOPING CENTRERING ROAD','SURAT','GUJARAT','','SU337',NULL,NULL,'0','CR','P/SUR/116','0',NULL,NULL,'0',NULL,0),
('P/2/117','NOVA FABRICS','N.T.M MARKETRING ROAD','SURAT','GUJARAT','','SU338',NULL,NULL,'0','CR','P/SUR/117','0',NULL,NULL,'0',NULL,0),
('P/2/118','SONA TEXTILES','NEW TEXTILE MARKETRING RAOD','SURAT','GUJARAT','','SU339',NULL,NULL,'0','CR','P/SUR/118','0',NULL,NULL,'0',NULL,0),
('P/2/119','GANESH ART','3\"RD FLOOR ,A/C TEXTILE MARKETRING ROAD','SURAT','GUJARAT','','SU340',NULL,NULL,'0','CR','P/SUR/119','0',NULL,NULL,'0',NULL,0),
('P/2/12','M.AGARWAL CREATIONS PVT.LTD.','104,SHANKAR MARKET,','SURAT','GUJARAT','2320220','SU135',NULL,NULL,'0','CR','P/SUR/12','0',NULL,NULL,'0',NULL,0),
('P/2/120','DURGA TEXTILES','87,ROYAL MARKET,U.M.ROAD','SURAT','GUJARAT','','SU341',NULL,NULL,'0','CR','P/SUR/120','0',NULL,NULL,'0',NULL,0),
('P/2/121','AMBICA ART CREATION','ROYAL CHAMBERS,OPP.RANATAN CINEMA, SALABATPURA','SURAT','GUJARAT','','SU342',NULL,NULL,'0','CR','P/SUR/121','0',NULL,NULL,'0',NULL,0),
('P/2/122','GOPAL FAB','1008,SANKAR MARKET-2RING ROAD','SURAT','GUJARAT','','SU344',NULL,NULL,'0','CR','P/SUR/122','0',NULL,NULL,'0',NULL,0),
('P/2/123','SAINATH ART','2002,ROHIT TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','','SU345',NULL,NULL,'0','CR','P/SUR/123','0',NULL,NULL,'0',NULL,0),
('P/2/124','VINIT FAB','LOWER GROUND,GOLDAN PLAZAMARKET,RING ROAD,','SURAT','GUJARAT','','SU346',NULL,NULL,'0','CR','P/SUR/124','0',NULL,NULL,'0',NULL,0),
('P/2/125','AKSHAY PRINTS','2050,SHIV SHAKTI TEXTILE MARKET, RING ROAD,','SURAT','GUJARAT','02612202103','SU347',NULL,NULL,'0','CR','P/SUR/125','0',NULL,NULL,'0',NULL,0),
('P/2/126','SAIBABA TEXTILES','3087,SHREE MAHAVIR TETILE MARKET,RING ROAD ,','SURAT','GUJARAT','02613054785','SU348',NULL,NULL,'0','CR','P/SUR/126','0',NULL,NULL,'0',NULL,0),
('P/2/127','AADESH TEXTILES','1033,GROUND FLOOR, MAHAVIR,MARKET, RING ROAD,','SURAT','GUJARAT','02612354573','SU349',NULL,NULL,'0','CR','P/SUR/127','0',NULL,NULL,'0',NULL,0),
('P/2/128','LAXMI POOJA SAREES','3087/MAHAVIR MARKET,RING ROAD,','SURAT','GUJARAT','02612345685','SU350',NULL,NULL,'0','CR','P/SUR/128','0',NULL,NULL,'0',NULL,0),
('P/2/129','EKTA FASHION PVT.LTD.','518 TO 521,SHREE MAHALAXMI MARKET ,RING ROAD,','SURAT','GUJARAT','2310751','SU351',NULL,NULL,'0','CR','P/SUR/129','0',NULL,NULL,'0',NULL,0),
('P/2/13','ROOP SANGAM FASHION','G-17,SHANKAR MARKETMOTI BEGUMWADI','SURAT','GUJARAT','2355372','SU136',NULL,NULL,'0','CR','P/SUR/13','0',NULL,NULL,'0',NULL,0),
('P/2/130','KARISHMA FASHION','105,ALISHAN MARKET,CHOWK BAZAR,','SURAT','GUJARAT','3231222,9328020775','SU352',NULL,NULL,'0','CR','P/SUR/130','0',NULL,NULL,'0',NULL,0),
('P/2/131','AMAAN FASHION','SHOP.NO.G-11,SILK SAGARMARKET,CHOWK BAZAR','SURAT','GUJARAT','9825757073,9925197949','SU353',NULL,NULL,'0','CR','P/SUR/131','0',NULL,NULL,'0',NULL,0),
('P/2/132','MUSKAN FASHION','G-13,SILK DOM MARKET,CHOWK BAZAR','SURAT','GUJARAT','2438141,9824015113','SU354',NULL,NULL,'0','CR','P/SUR/132','0',NULL,NULL,'0',NULL,0),
('P/2/133','KOMAL TEX (BALAJI TERINE)','2055-56ADARSH MARKET-1,RING ROAD','SURAT','GUJARAT','02613925414,3925401','SU355',NULL,NULL,'0','CR','P/SUR/133','0',NULL,NULL,'0',NULL,0),
('P/2/134','MAHAVEER SAREE(MANMANDIR SAREE','1053,SHRI MAHAVIR TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','0261-2339920','SU357',NULL,NULL,'0','CR','P/SUR/134','0',NULL,NULL,'0',NULL,0),
('P/2/135','ADITYA PRINTS PVT.LTD.','40005,4th FLOOR,ASHOKA TOWERRING ROAD,','SURAT','GUJARAT','02616611770','SU358',NULL,NULL,'0','CR','P/SUR/135','0',NULL,NULL,'0',NULL,0),
('P/2/136','AAYUSI CREATION','1015,1st FLOOR,ASHOKA TOWARRING ROAD','SURAT','GUJARAT','9662028334,3925553','SU359',NULL,NULL,'0','CR','P/SUR/136','0',NULL,NULL,'0',NULL,0),
('P/2/137','WEST INDIA PRINTS','520,UPPER GROUND,SWADESHI TEX.MARKET,RING ROAD,','SURAT','GUJARAT','2329134,9825094016','SU360',NULL,NULL,'0','CR','P/SUR/137','0',NULL,NULL,'0',NULL,0),
('P/2/138','VAISHNAVI SAREES','S-546,J.J.A/C,MARKET,RING ROAD','SURAT','GUJARAT','9725353501,9374821841','SU361',NULL,NULL,'0','CR','P/SUR/138','0',NULL,NULL,'0',NULL,0),
('P/2/139','SASHI CREATION','E-599,NEW TEXTILE MARKET(N.T.M0,RING ROAD','SURAT','GUJARAT','02613042748,9825610117','SU362',NULL,NULL,'0','CR','P/SUR/139','0',NULL,NULL,'0',NULL,0),
('P/2/14','RITA FASHION PVT.LTD.','F-3130,N.T.M RING ROAD','SURAT','GUJARAT','2353053','SU138',NULL,NULL,'0','CR','P/SUR/14','0',NULL,NULL,'0',NULL,0),
('P/2/140','RAKHI ENTERPRISE','P-1098-99,GROUND FLOOR ,SURAT TEXTILE MAR.RING ROAD','SURAT','GUJARAT','2335309','SU363',NULL,NULL,'0','CR','P/SUR/140','0',NULL,NULL,'0',NULL,0),
('P/2/141','RAJ DESIGNER','1053,SHREEMAHAVIR TEXTILE MAR,RING ROAD','SURAT','GUJARAT','0261-2344816','SU364',NULL,NULL,'0','CR','P/SUR/141','0',NULL,NULL,'0',NULL,0),
('P/2/142','VISHAL THREADSS','PF-1,CORPORATE OFFICE BUILDINGBOMBAY MARKET,UMARWADA','SURAT','GUJARAT','','SU365',NULL,NULL,'0','CR','P/SUR/142','0',NULL,NULL,'0',NULL,0),
('P/2/143','NIHARIKA SAREES','ASHOKA TOWERRING ROAD','SURAT','GUJARAT','','SU366',NULL,NULL,'0','CR','P/SUR/143','0',NULL,NULL,'0',NULL,0),
('P/2/144','SANGINI SAREE(VIDHIPATI SAREE)','S-547-48,J.J.A.C.MARKET,RING ROAD','SURAT','GUJARAT','02612320768','SU367',NULL,NULL,'0','CR','P/SUR/144','0',NULL,NULL,'0',NULL,0),
('P/2/145','SAARTHI FABRICS(SUNSHINE)','555,UPPER GROUND ADARSHMARKET-2,RING ROAD','SURAT','GUJARAT','02613926555/9898968111','SU368',NULL,NULL,'0','CR','P/SUR/145','0',NULL,NULL,'0',NULL,0),
('P/2/146','MAVIYA FABRICS','SHOP.NO.G-1,LALSHA MARKET,VADACHAUTA,CHOWK BAZAR,','SURAT','GUJARAT','9979226405','SU369',NULL,NULL,'0','CR','P/SUR/146','0',NULL,NULL,'0',NULL,0),
('P/2/147','ABHISHEK PRINTS','3064,SHREE MAHVEER TEXTILE MARKET RING ROAD','SURAT','GUJARAT','2337814','SU372',NULL,NULL,'0','CR','P/SUR/147','0',NULL,NULL,'0',NULL,0),
('P/2/148','NANDLAL SILK MILLES','S.NO.U-1220,GROUND FLOORSURAT TEXTILE MARKET','SURAT','GUJARAT','2336421','SU373',NULL,NULL,'0','CR','P/SUR/148','0',NULL,NULL,'0',NULL,0),
('P/2/149','BINDIYA SAREES (VIDHIPATI)','S-547-48,J.J.A/C MARKETRING ROAD','SURAT','GUJARAT','2320768,987930259','SU374',NULL,NULL,'0','CR','P/SUR/149','0',NULL,NULL,'0',NULL,0),
('P/2/15','MILAN SILK MILLS','2045,SHIV SHAKTI MARKETRING ROAD,','SURAT','GUJARAT','2346567','SU142',NULL,NULL,'0','CR','P/SUR/15','0',NULL,NULL,'0',NULL,0),
('P/2/150','JAIN SYNTH FAB PVT.LTD','638-39,UPPER GROUND ABHISHEKMARKET,RING ROAD','SURAT','GUJARAT','2364960','SU375',NULL,NULL,'0','CR','P/SUR/150','0',NULL,NULL,'0',NULL,0),
('P/2/151','SHREE SHYAM TEXTILES','3068,2ND FLOR SHREE MAHAVIRTEXTILE MARKET,RING ROAD','SURAT','GUJARAT','2342724','SU376',NULL,NULL,'0','CR','P/SUR/151','0',NULL,NULL,'0',NULL,0),
('P/2/152','SHREE BHAWANI TEXTILE','G-7,OM SHANKAR MARKETRING ROAD','SURAT','GUJARAT','02612368965','SU377',NULL,NULL,'0','CR','P/SUR/152','0',NULL,NULL,'0',NULL,0),
('P/2/153','VAISHNAVI CREATION','226-27,LOWER GROUND,ADRSHMARKET-2,RING ROAD','SURAT','GUJARAT','3203251','SU378',NULL,NULL,'0','CR','P/SUR/153','0',NULL,NULL,'0',NULL,0),
('P/2/154','VIJAY PRINTS','54,BASEMAENT,ABHISHEK MARKETRING ROAD','SURAT','GUJARAT','3927609','SU379',NULL,NULL,'0','CR','P/SUR/154','0',NULL,NULL,'0',NULL,0),
('P/2/155','RASHMI FASHION','B-21 32-33,1sT FLOOR,RADHA KRISHNA TEXTILE MARKET RING ROAD','SURAT','GUJARAT','2312367,9327707690','SU200',NULL,NULL,'0','CR','P/SUR/155','0',NULL,NULL,'0',NULL,0),
('P/2/156','TANISHA SAREES','4070,MAHAVIR TEXTILE MARKETRING ROAD','SURAT','GUJARAT','','SU104',NULL,NULL,'0','CR','P/SUR/156','0',NULL,NULL,'0',NULL,0),
('P/2/157','JAIN SYNTHETICS','638-39,UPPER GROUNDABHISHEK MARKET','SURAT','GUJARAT','','SU108',NULL,NULL,'0','CR','P/SUR/157','0',NULL,NULL,'0',NULL,0),
('P/2/158','KHUSHI THE DESIGNER','U-30ABHINANDAN A.C.MARKETGHOD-DOAD ROAD,','SURAT','GUJARAT','','SU109',NULL,NULL,'0','CR','P/SUR/158','0',NULL,NULL,'0',NULL,0),
('P/2/159','DEEPIKA FASHION','L-1257-58,1st FLOOR,N.T.MRING ROAD','SURAT','GUJARAT','0261-3934427,9375498599','SU110',NULL,NULL,'0','CR','P/SUR/159','0',NULL,NULL,'0',NULL,0),
('P/2/16','MINA SAREES PVT.LTD.','J-102-3,LOWER GROUND,J.J.A.C. MARKET','SURAT','GUJARAT','','SU145',NULL,NULL,'0','CR','P/SUR/16','0',NULL,NULL,'0',NULL,0),
('P/2/160','B.L.FASHION','540,UPPER GROUND,JAY SHREERAMMARKET,RING ROAD','SURAT','GUJARAT','9327627960,9327514121','SU112',NULL,NULL,'0','CR','P/SUR/160','0',NULL,NULL,'0',NULL,0),
('P/2/161','SHREENATH COLLECTION','G-6,OM SHANKAR MARKET,MOTIBEUMWADI,RING ROAD','SURAT','GUJARAT','9879308669,9909325056','SU113',NULL,NULL,'0','CR','P/SUR/161','0',NULL,NULL,'0',NULL,0),
('P/2/162','SHREE SARASWATI FASHION','SHUBHAM,147,LOWER GROUND(N.T.MRING ROAD','SURAT','GUJARAT','2353388,9898385303','SU116',NULL,NULL,'0','CR','P/SUR/162','0',NULL,NULL,'0',NULL,0),
('P/2/163','SRIJAN SILK MILLS','648,UPPER GROUND,ABHISHEK MRKRING ROAD','SURAT','GUJARAT','02613058044','SU119',NULL,NULL,'0','CR','P/SUR/163','0',NULL,NULL,'0',NULL,0),
('P/2/164','DHANLAXMI SAREE','644,UPPER GROUND ABHISHEK TEXTILE MRK,RING ROAD','SURAT','GUJARAT','0261-3018042','SU120',NULL,NULL,'0','CR','P/SUR/164','0',NULL,NULL,'0',NULL,0),
('P/2/165','BINDAL SAREES','310,ABHISHEK TEXTILE MARKETRING ROAD','SURAT','GUJARAT','0261-3045310','SU121',NULL,NULL,'0','CR','P/SUR/165','0',NULL,NULL,'0',NULL,0),
('P/2/166','AVADHI FASHIONS','W-1209,10,S.T.M.RING ROAD','SURAT','GUJARAT','02612332555,09974712555','SU123',NULL,NULL,'0','CR','P/SUR/166','0',NULL,NULL,'0',NULL,0),
('P/2/167','POOJA SAREE','3065,SHREE MAHVIR TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','02613013356,9377636909','SU124',NULL,NULL,'0','CR','P/SUR/167','0',NULL,NULL,'0',NULL,0),
('P/2/168','AMIRAT FASHION','B-1030,N.T.M.','SURAT','GUJARAT','2312188,3006032','SU128',NULL,NULL,'0','CR','P/SUR/168','0',NULL,NULL,'0',NULL,0),
('P/2/169','BHARTI SILK MILLS','Q-1249,SURAT TEXTILES MARKET,RING ROAD','SURAT','GUJARAT','09099600468,0261-6611049','SU239',NULL,NULL,'0','CR','P/SUR/169','0',NULL,NULL,'0',NULL,0),
('P/2/17','SIMRAN TEXTILE','1041-42 L/G,SHIV SHAKTITEXTILE MARKET','SURAT','GUJARAT','2202452','SU150',NULL,NULL,'0','CR','P/SUR/17','0',NULL,NULL,'0',NULL,0),
('P/2/170','KUSUM SAREE (CREATION)','531/532,ABHINANDAN MARKET,RINGRAOD','SURAT','GUJARAT','9998498471,02613928551','SU129',NULL,NULL,'0','CR','P/SUR/170','0',NULL,NULL,'0',NULL,0),
('P/2/171','JAY SILK MILLS','','SURAT','GUJARAT','','SU158',NULL,NULL,'0','CR','P/SUR/171','0',NULL,NULL,'0',NULL,0),
('P/2/172','NANDANI SAREES(NIDHI SAREE)','ASHOKA TOWER,RING ROAD','SURAT','GUJARAT','','SU130',NULL,NULL,'0','CR','P/SUR/172','0',NULL,NULL,'0',NULL,0),
('P/2/173','KARISHMA COTTEN HOUSE','','SURAT','GUJARAT','','SU131',NULL,NULL,'0','CR','P/SUR/173','0',NULL,NULL,'0',NULL,0),
('P/2/174','VIBHAVRI SAREE','2016,SWADESHI TEX.MRK.2nD FLOOR,RING ROAD','SURAT','GUJARAT','','SU132',NULL,NULL,'0','CR','P/SUR/174','0',NULL,NULL,'0',NULL,0),
('P/2/175','S.S.FABRICS','SHOP.NO.B-1-2,ALISHAN MARKET ,VADA CHUTTA CHOWK BAZAR','SURAT','GUJARAT','9979226405,02612491047','SU139',NULL,NULL,'0','CR','P/SUR/175','0',NULL,NULL,'0',NULL,0),
('P/2/176','KALAKIRTI FASHION','D-WING,SHRI KUBERTEX.MRK.OPP.R.K.T.M PARKING GATE NAWABWADI','SURAT','GUJARAT','02612411555','SU140',NULL,NULL,'0','CR','P/SUR/176','0',NULL,NULL,'0',NULL,0),
('P/2/177','BELA FASHION','1079,80 ADARSH MARKETRING ROAD','SURAT','GUJARAT','','SU133',NULL,NULL,'0','CR','P/SUR/177','0',NULL,NULL,'0',NULL,0),
('P/2/178','ABHI CREATION','2025,U/G,JAY LAXMI MARKETMOTI BEUMWADI,RING ROAD','SURAT','GUJARAT','7878768848,09173355696','SU146',NULL,NULL,'0','CR','P/SUR/178','0',NULL,NULL,'0',NULL,0),
('P/2/179','SHREE PUJIYA SAREE','M-1716-17,G/F,MILLENNIUMMARKET','SURAT','GUJARAT','','SU147',NULL,NULL,'0','CR','P/SUR/179','0',NULL,NULL,'0',NULL,0),
('P/2/18','AVADHI ENTERPRISES','K-1001 1st FLOOR NEW T.T. MARKET,RING ROAD','SURAT','GUJARAT','02612336372,09376720766','SU152',NULL,NULL,'0','CR','P/SUR/18','0',NULL,NULL,'0',NULL,0),
('P/2/180','SHREE LAXMI SILK','J-101,J.J.AC','SURAT','GUJARAT','','SU149',NULL,NULL,'0','CR','P/SUR/180','0',NULL,NULL,'0',NULL,0),
('P/2/181','SUMATHINATH(ABHISHEK)','3064,SHREE MAHVIR TEXTILEMARKET,RING ROAD','SURAT','GUJARAT','3012055,2337814','SU115',NULL,NULL,'0','CR','P/SUR/181','0',NULL,NULL,'0',NULL,0),
('P/2/182','VENAVI CREATION(VAMAL)','3050,ABHISHEK TEXTILE MARKETRING ROAD','SURAT','GUJARAT','0261-2202103,9374505885','SU125',NULL,NULL,'0','CR','P/SUR/182','0',NULL,NULL,'0',NULL,0),
('P/2/183','M.M.FABRICS','SHOP.NO.T-8065-66,2nd FLOORR.K.T.MARKET,RING ROAD','SURAT','GUJARAT','0261-2366800','SU126',NULL,NULL,'0','CR','P/SUR/183','0',NULL,NULL,'0',NULL,0),
('P/2/184','SILVER TEXTILES','G-2 B,SILK DOM MARKETCHOWK BAZAR','SURAT','GUJARAT','9898008666','SU137',NULL,NULL,'0','CR','P/SUR/184','0',NULL,NULL,'0',NULL,0),
('P/2/185','FINE FASHION','G/13,SILK DOM MARKETCHOWK BAZAR','SURAT','GUJARAT','9824138791,98254015113','SU143',NULL,NULL,'0','CR','P/SUR/185','0',NULL,NULL,'0',NULL,0),
('P/2/186','MAKKI FABRICS','','SURAT','GUJARAT','','SU193',NULL,NULL,'0','CR','P/SUR/186','0',NULL,NULL,'0',NULL,0),
('P/2/187','MAN MANDIR SAREE','2028, MAHAVIR  MARKET,RING ROAD','SURAT','GUJARAT','0261-2339920','SU148',NULL,NULL,'0','CR','P/SUR/187','0',NULL,NULL,'0',NULL,0),
('P/2/188','VISHAL SILK MILLS','R-1108-09,GROUND FLOOR,SURATTEXTILE MRKET RING ROAD','SURAT','GUJARAT','','SU127',NULL,NULL,'0','CR','P/SUR/188','0',NULL,NULL,'0',NULL,0),
('P/2/189','NISHA FASHION','1099-,NEW PASHUPATI MARKETMOTI BEGUMWADI,RING ROAD','SURAT','GUJARAT','02613100457,9328868625','SU151',NULL,NULL,'0','CR','P/SUR/189','0',NULL,NULL,'0',NULL,0),
('P/2/19','VAIBHAV SAREES','615,U/G, ABHISHEK MARKET,','SURAT','GUJARAT','2202793','SU159',NULL,NULL,'0','CR','P/SUR/19','0',NULL,NULL,'0',NULL,0),
('P/2/190','S.K.TEXTILE','213,2nd FLOOR,MANOJ MARKETRING ROAD','SURAT','GUJARAT','02612340956,3019719','SU153',NULL,NULL,'0','CR','P/SUR/190','0',NULL,NULL,'0',NULL,0),
('P/2/191','RINKY FASHION PVT.LTD.','HOUSE NO.27,RATHI PALACERING ROAD','SURAT','GUJARAT','0261-3251425','SU154',NULL,NULL,'0','CR','P/SUR/191','0',NULL,NULL,'0',NULL,0),
('P/2/192','KAVYA SAREE','J-620-21,J.J.A/C TEXTILEMARKET,RING ROAD','SURAT','GUJARAT','2320768','SU155',NULL,NULL,'0','CR','P/SUR/192','0',NULL,NULL,'0',NULL,0),
('P/2/193','VIVAANA DESIGNERS PVT.(AADITYA','B-2058-59-60,MILLENIUM TEXTILEMARKET ,RING ROAD','SURAT','GUJARAT','02616557521','SU156',NULL,NULL,'0','CR','P/SUR/193','0',NULL,NULL,'0',NULL,0),
('P/2/194','SURYAVANSI CREATION PVT.LTD.','SALES CUM H.O.,104,SHANKARMARKET,RING ROAD','SURAT','GUJARAT','0261-2320220','SU161',NULL,NULL,'0','CR','P/SUR/194','0',NULL,NULL,'0',NULL,0),
('P/2/195','ALFA FABRICS','E-2266,MILLENIUM TEXTILE MARKET,KAMELA DARWAJA,RING ROA','SURAT','GUJARAT','0261-3195900,9979195506','SU157',NULL,NULL,'0','CR','P/SUR/195','0',NULL,NULL,'0',NULL,0),
('P/2/196','VISHWA BHARTI TEXTILES','O-1261-62,GROUND FLOOR,SURAT TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','0261-2327556,9898614799','SU163',NULL,NULL,'0','CR','P/SUR/196','0',NULL,NULL,'0',NULL,0),
('P/2/197','HAREKRISHNA TEXTILE','1037-38,LOWER GROUND,TIRUPATIMARKET,RING ROAD','SURAT','GUJARAT','2333153,9825113209','SU257',NULL,NULL,'0','CR','P/SUR/197','0',NULL,NULL,'0',NULL,0),
('P/2/198','RAJLAXMI FASHIONS','415,LOWER GROUND,ABHISHEK TEXTILE MAREKET,RING ROAD','SURAT','GUJARAT','0261-3043489,9375969871','SU164',NULL,NULL,'0','CR','P/SUR/198','0',NULL,NULL,'0',NULL,0),
('P/2/2','PARAM SILK MILLS (PINAL FASHIO','501,UPPER GROUND,NEW TEXTILEMARKET,','SURAT','GUJARAT','0261-3065470','SU102',NULL,NULL,'0','CR','P/SUR/2','0',NULL,NULL,'0',NULL,0),
('P/2/20','GIRITEX SILK MILLS','1037-38,G/F,SHREE MAHAVIRTEXTILE MARKET','SURAT','GUJARAT','3094625','SU160',NULL,NULL,'0','CR','P/SUR/20','0',NULL,NULL,'0',NULL,0),
('P/2/200','RAJLAXMI CREATION','415,LOWER GROUND ,ABHISHEK TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','0261-3043489,9375969871','SU169',NULL,NULL,'0','CR','P/SUR/200','0',NULL,NULL,'0',NULL,0),
('P/2/201','KRISHNA INTERNATIONAL','R.K.T.MARKET BHARAT BHAI','SURAT','GUJARAT','','SU171',NULL,NULL,'0','CR','P/SUR/201','0',NULL,NULL,'0',NULL,0),
('P/2/202','DHIRAJ SYNTHETICS','3033,2ND FLOORSHREE MAHAVIR TEXTILE MARKET','SURAT','GUJARAT','2334094,6615214','SU173',NULL,NULL,'0','CR','P/SUR/202','0',NULL,NULL,'0',NULL,0),
('P/2/203','SYNTH & BLEND','W-1204,GROUND FLOOR,SURAT TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','2338856,9825113560','SU166',NULL,NULL,'0','CR','P/SUR/203','0',NULL,NULL,'0',NULL,0),
('P/2/204','MAHAVIR SAREES','3044,2ND FLOOR,SHREE MAHAVIR TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','9824172151,9099139400','SU174',NULL,NULL,'0','CR','P/SUR/204','0',NULL,NULL,'0',NULL,0),
('P/2/205','VIDHI SHREE (VIDHIPATI)','J-620,21 J.J.A/C MARKETRING ROAD','SURAT','GUJARAT','','SU182',NULL,NULL,'0','CR','P/SUR/205','0',NULL,NULL,'0',NULL,0),
('P/2/206','LAVIN CREATION','6,UPPER GROUND RADHS KRISHNA TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','0261-3006751','SU175',NULL,NULL,'0','CR','P/SUR/206','0',NULL,NULL,'0',NULL,0),
('P/2/207','ANKITA TEX','C-1162,RADHA KRISHANA TEXTILERING ROAD','SURAT','GUJARAT','9825474341,9408926685','SU183',NULL,NULL,'0','CR','P/SUR/207','0',NULL,NULL,'0',NULL,0),
('P/2/208','SONI CREATION','U-7109,1st FLOOR,R.K.TRING ROAD','SURAT','GUJARAT','8141608711','SU184',NULL,NULL,'0','CR','P/SUR/208','0',NULL,NULL,'0',NULL,0),
('P/2/209','MAHEK CREATION','A-216,GAJJAR CHAMBER,RING ROAD','SURAT','GUJARAT','0261-3016985,9374513306','SU185',NULL,NULL,'0','CR','P/SUR/209','0',NULL,NULL,'0',NULL,0),
('P/2/21','MANISH CREATION','1080,SHIV SHAKTI MARKET','SURAT','GUJARAT','2343840','SU162',NULL,NULL,'0','CR','P/SUR/21','0',NULL,NULL,'0',NULL,0),
('P/2/210','YASH INTERNATIONAL','Z-1166,GROUND FLOOR,SURAT TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','2336516/9925108085','SU215',NULL,NULL,'0','CR','P/SUR/210','0',NULL,NULL,'0',NULL,0),
('P/2/211','HARI OM SILK MILLS','L-338.NEW TEXTILE MARKETRING ROAD','SURAT','GUJARAT','9879354731','SU186',NULL,NULL,'0','CR','P/SUR/211','0',NULL,NULL,'0',NULL,0),
('P/2/212','KISHIKA EMBROIDERY','T-6049 TO 51,RADHA KRISHNAMARKET,RING ROAD','SURAT','GUJARAT','0261-2806665','SU179',NULL,NULL,'0','CR','P/SUR/212','0',NULL,NULL,'0',NULL,0),
('P/2/213','KSM FASHION','7 th FLOOR AMBAJI MARKETRING ROAD','SURAT','GUJARAT','','SU241',NULL,NULL,'0','CR','P/SUR/213','0',NULL,NULL,'0',NULL,0),
('P/2/214','KEVAL SAREES','N-1084,G/F,SURAT TEXTILE MARKTRING ROAD','SURAT','GUJARAT','2324085','SU190',NULL,NULL,'0','CR','P/SUR/214','0',NULL,NULL,'0',NULL,0),
('P/2/215','RADHEY RADHEY CREATION(VIJAY P','54,BASEMENT,ABHISHEK MARKETRING ROAD','SURAT','GUJARAT','3927609','SU194',NULL,NULL,'0','CR','P/SUR/215','0',NULL,NULL,'0',NULL,0),
('P/2/216','MALLINATH TEXTILES','3064,SHREE MAHVIR TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','3012055','SU195',NULL,NULL,'0','CR','P/SUR/216','0',NULL,NULL,'0',NULL,0),
('P/2/217','MILAN FASHION','3070,MAHAVIR TEXTILE MARKET,RING ROAD','SURAT','GUJARAT','','SU208',NULL,NULL,'0','CR','P/SUR/217','0',NULL,NULL,'0',NULL,0),
('P/2/218','VIJAY SHREE FASHIONS','599,UPPER GROUND,ABHINANDANTEXTILE MARKET,RING ROAD','SURAT','GUJARAT','0261-3914780,9825557814','SU197',NULL,NULL,'0','CR','P/SUR/218','0',NULL,NULL,'0',NULL,0),
('P/2/219','M.KIRAN','N-1084,GROUND FLOOR,RIONG ROAD','SURAT','GUJARAT','2324085,9879043889','SU202','','','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/2/22','SHREE SILK','S-2104,J.J.A/C.MARKET','SURAT','GUJARAT','9879271973','SU167',NULL,NULL,'0','CR','P/SUR/22','0',NULL,NULL,'0',NULL,0),
('P/2/220','OM CREATION','C-1153,UPPER GROUND RADHAKRISHAN TEXTILE MARKET RING RO','SURAT','GUJARAT','0261-3006629,9978881281','SU206',NULL,NULL,'0','CR','P/SUR/220','0',NULL,NULL,'0',NULL,0),
('P/2/221','TANNU FASHION','L-583,SHREE SHYAM MARKETRING ROAD','SURAT','GUJARAT','9825289006,9376208116','SU198',NULL,NULL,'0','CR','P/SUR/221','0',NULL,NULL,'0',NULL,0),
('P/2/222','VINEE BANDHEJ','2004-5,T.T.TOWERRING ROAD','SURAT','GUJARAT','2202259/9328812259','SU199',NULL,NULL,'0','CR','P/SUR/222','0',NULL,NULL,'0',NULL,0),
('P/2/223','RAM AVTAR SONS','M-1277,GROUND FLOOR SURAT TEXTILE MARKET RING ROAD','SURAT','GUJARAT','0261-2321331,3262348','SU203',NULL,NULL,'0','CR','P/SUR/223','0',NULL,NULL,'0',NULL,0),
('P/2/224','LAXMI SAREES','2065-66,TEXTILE TOWERRING ROAD','SURAT','GUJARAT','2202034,9879191461','SU280',NULL,NULL,'0','CR','P/SUR/224','0',NULL,NULL,'0',NULL,0),
('P/2/225','SANGEETA TEXTILES','302,SURANA INTERNATI,RING ROAD','SURAT','GUJARAT','','SU204','','24AAZPY0748C1Z9','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/2/226','AZAZ TEXTILES','G-4,SILK SAGAR MARKETCHOWK BAZAR','SURAT','GUJARAT','9825282909','SU220',NULL,NULL,'0','CR','P/SUR/226','0',NULL,NULL,'0',NULL,0),
('P/2/227','DADISA DESIGNER','I-2484,UPPER GROUND MILLENNIUMTEXTILE MARKET RING ROAD','SURAT','GUJARAT','0261--6516724','SU221',NULL,NULL,'0','CR','P/SUR/227','0',NULL,NULL,'0',NULL,0),
('P/2/228','SYMORE PRINTS PVT.LTD.','102,SURANA INTERNATIONAL NEARJASH MARKET,RING ROAD','SURAT','GUJARAT','0261-2325485','SU209',NULL,NULL,'0','CR','P/SUR/228','0',NULL,NULL,'0',NULL,0),
('P/2/229','V.R.SAREE','210,UPPER GROUND,ABHINANDANMARKET,RING ROAD','SURAT','GUJARAT','6506639,9898226639','SU210',NULL,NULL,'0','CR','P/SUR/229','0',NULL,NULL,'0',NULL,0),
('P/2/23','ROHAN FABRICS','607,ABHINANDAN MARKET','SURAT','GUJARAT','2342496','SU168',NULL,NULL,'0','CR','P/SUR/23','0',NULL,NULL,'0',NULL,0),
('P/2/230','DHULEWA CREATION','S.NO.2017,JAY LAXMIRING ROAD','SURAT','GUJARAT','0261-2335705,9427113504-3','SU211','','24ADIPD9074E1ZO','0','CR','','0','',NULL,'0','PURCHASE 5%',1),
('P/2/231','VARSHITAP DESIGNER','204,ABHINANDAN MARKETRING ROAD','SURAT','GUJARAT','3131500,9328086676','SU212',NULL,NULL,'0','CR','P/SUR/231','0',NULL,NULL,'0',NULL,0),
('P/2/232','M.S.CREATION(MAHAVEER)','1071,SHRI MAHAVIR TEXTILESMARKET,RING ROAD','SURAT','GUJARAT','0261-3050333,09428870583','SU218',NULL,NULL,'0','CR','P/SUR/232','0',NULL,NULL,'0',NULL,0),
('P/2/233','VIRAT CREATION','222-223,L/G,ABHINANDAN MARKETRING ROAD','SURAT','GUJARAT','0261-6459495,9427146580','SU217',NULL,NULL,'0','CR','P/SUR/233','0',NULL,NULL,'0',NULL,0),
('P/2/234','VERONICA DESIGNERS','286,ABHINANDAN TEXTILES MARKETRING ROAD','SURAT','GUJARAT','0261-2365007,9898755007','SU222',NULL,NULL,'0','CR','P/SUR/234','0',NULL,NULL,'0',NULL,0),
('P/2/235','KALI CREATION C/O DADISA DESIG','I-2479-80,MILLENNIUM TEXTILEMARKET,RING ROAD','SURAT','GUJARAT','0261-6535337','SU224',NULL,NULL,'0','CR','P/SUR/235','0',NULL,NULL,'0',NULL,0),
('P/2/236','TANTIA TRADING COMPANY PVT.LTD','KHATA NO.107 TO 110 SHIV ASHISIND.','SURAT','GUJARAT','9825118122','SU242',NULL,NULL,'0','CR','P/SUR/236','0',NULL,NULL,'0',NULL,0),
('P/2/237','RASNA CREATION','NEW TEXTILES MARKETRING ROAD','SURAT','GUJARAT','0261-3006969','SU371',NULL,NULL,'0','CR','P/SUR/237','0',NULL,NULL,'0',NULL,0),
('P/2/238','DATARI FASHION','G-9,SILK DOM MARKETCHOWK BAZAR','SURAT','GUJARAT','9925807967,9825379647','SU226',NULL,NULL,'0','CR','P/SUR/238','0',NULL,NULL,'0',NULL,0),
('P/2/239','RANG TEXTILES','681,N.T.M,RING ROAD','SURAT','GUJARAT','0261-3006969','SU356',NULL,NULL,'0','CR','P/SUR/239','0',NULL,NULL,'0',NULL,0),
('P/2/24','KALA SAGAR','577,U/GABHINANDAN MARKET','SURAT','GUJARAT','2202077','SU170',NULL,NULL,'0','CR','P/SUR/24','0',NULL,NULL,'0',NULL,0),
('P/2/240','JAY SHREE NATHJI DRESSES','2021,JAY LAXMI MARKETUPPER GROUND,RING ROAD','SURAT','GUJARAT','8000821646,9328221646','SU380',NULL,NULL,'0','CR','P/SUR/240','0',NULL,NULL,'0',NULL,0),
('P/2/241','KHUSHI FAB','C-1156,57,UPPER GROUND,RADHAKRISHNA TEXTLE RING ROAD','SURAT','GUJARAT','3111208,9979210564','SU232',NULL,NULL,'0','CR','P/SUR/241','0',NULL,NULL,'0',NULL,0),
('P/2/242','KEY MART COTTON COLORS','702,TWIN TOWER,7th FLOORSAHARA DARWAJA JUNCTION,RNG RO','SURAT','GUJARAT','0261-2312883,9327336262','SU227',NULL,NULL,'0','CR','P/SUR/242','0',NULL,NULL,'0',NULL,0),
('P/2/243','VASCO CREATION','290,ABHINANDAN TEXTILES MARKERRING ROAD','SURAT','GUJARAT','0261-311339,9427519342','SU243',NULL,NULL,'0','CR','P/SUR/243','0',NULL,NULL,'0',NULL,0),
('P/2/244','SANGINI INTERNATIONAL','291,LOWER GROUND,ABHINANADANMRKT,RING ROAD SURAT','SURAT','GUJARAT','0261-3006787','SU247',NULL,NULL,'0','CR','P/SUR/244','0',NULL,NULL,'0',NULL,0),
('P/2/245','VINEE SILK MILLES','I-2460-61,MILLNIUM MARKETRING ROAD','SURAT','GUJARAT','0261-3049460,9328812259','SU248',NULL,NULL,'0','CR','P/SUR/245','0',NULL,NULL,'0',NULL,0),
('P/2/246','SONERI CREATION','1041,TEXTILE TOWER(T.T.MARKET)RING ROAD','SURAT','GUJARAT','0261-3057333,9913058878','SU238',NULL,NULL,'0','CR','P/SUR/246','0',NULL,NULL,'0',NULL,0),
('P/2/247','RAJAT CREATION','K-2574-75-76,MILLENNIUM TEXTILMARKET,RING ROAD','SURAT','GUJARAT','6516393','SU297',NULL,NULL,'0','CR','P/SUR/247','0',NULL,NULL,'0',NULL,0),
('P/2/248','SAKHI SAHELI','539,ABHINANDAN TXTILES MARKETRING ROAD','SURAT','GUJARAT','02613015012','SU322',NULL,NULL,'0','CR','P/SUR/248','0',NULL,NULL,'0',NULL,0),
('P/2/249','SHAGUN CREATION','B-1017,KASHI MARKETRING ROAD','SURAT','GUJARAT','8000707711','SU249',NULL,NULL,'0','CR','P/SUR/249','0',NULL,NULL,'0',NULL,0),
('P/2/25','SUBH LAXMI SYNTHETICS','2128-29,N.T.M.','SURAT','GUJARAT','','SU172',NULL,NULL,'0','CR','P/SUR/25','0',NULL,NULL,'0',NULL,0),
('P/2/250','SHREE NAGNESHA FASHION','581,UPPER GROUNDADARSH MARKET-1','SURAT','GUJARAT','9712384888/07041136688','SU250',NULL,NULL,'0','CR','P/SUR/250','0',NULL,NULL,'0',NULL,0),
('P/2/251','SHREE GANESH TEXTILES','D-1288 TO 90,UPPER GROUND,RADHA KRISHNA MARTKET RING ROAD','SURAT','GUJARAT','','SU230',NULL,NULL,'0','CR','P/SUR/251','0',NULL,NULL,'0',NULL,0),
('P/2/252','ARHAM CLOTH(DHULEWA)','2018,MEZZANINE FL.JAY LAXMI MARKET MOTI BEGUMWADI RING ROAD','SURAT','GUJARAT','0261-2335705,9427113504-3','SU251',NULL,NULL,'0','CR','P/SUR/252','0',NULL,NULL,'0',NULL,0),
('P/2/253','CHOCOLATE FAB(ONLY)','A-1070-71,RADHA KRISHNA TEXTILES MARKET RING ROAD','SURAT','GUJARAT','0261-3921321,9925921321','SU252',NULL,NULL,'0','CR','P/SUR/253','0',NULL,NULL,'0',NULL,0),
('P/2/254','TORAN G.FASHION(AMRIT FASHION)','B-1029,NEW TEXTILES MARKETRING ROAD','SURAT','GUJARAT','3006032','SU254',NULL,NULL,'0','CR','P/SUR/254','0',NULL,NULL,'0',NULL,0),
('P/2/255','K-3 DESIGNER','539,ABHINANDAN TEXTILES MARKETRING ROAD','SURAT','GUJARAT','9725291012,9724229406','SU260',NULL,NULL,'0','CR','P/SUR/255','0',NULL,NULL,'0',NULL,0),
('P/2/256','G.T.CREATION (AMRIT CREATION)','B-1029,30 1st FLOOR NEW TESXTILES MARKET RING ROAD','SURAT','GUJARAT','','SU261',NULL,NULL,'0','CR','P/SUR/256','0',NULL,NULL,'0',NULL,0),
('P/2/257','GUDDU PRINTS (YASH)','Z-1166,G/F,S.T.MRING ROAD','SURAT','GUJARAT','','SU267',NULL,NULL,'0','CR','P/SUR/257','0',NULL,NULL,'0',NULL,0),
('P/2/258','SIDDHARTH TEXTILES','M-1279,SURAT TEXTILE MARKET RING ROAD,SURAT','SURAT','GUJARAT','2321223,2332449,989006311','SU234',NULL,NULL,'0','CR','P/SUR/258','0',NULL,NULL,'0',NULL,0),
('P/2/259','VAISHALI FASHIONS','401,4th FLOOR,TWIN TOWER,SAHARA DARWAJA RING ROAD','SURAT','GUJARAT','0261-2310003,9904799043','SU271',NULL,NULL,'0','CR','P/SUR/259','0',NULL,NULL,'0',NULL,0),
('P/2/26','JAGDAMBA TEXTILES','1024-25,KASHI MARKET','SURAT','GUJARAT','0261-2337277','SU177',NULL,NULL,'0','CR','P/SUR/26','0',NULL,NULL,'0',NULL,0),
('P/2/260','RAJ INTERNATIONAL','2051-52,ABHISHEK TEXTILESMARKET RING ROAD','SURAT','GUJARAT','0261-2311269,9998564871','SU295',NULL,NULL,'0','CR','P/SUR/260','0',NULL,NULL,'0',NULL,0),
('P/2/261','SHREE SHYAM TEX (TANTIYA)','S.NO.708,UPPER GROUND,JAGDAMBAMARKET,RING ROAD','SURAT','GUJARAT','9327363070','SU283',NULL,NULL,'0','CR','P/SUR/261','0',NULL,NULL,'0',NULL,0),
('P/2/262','ASHTAPAD NX','197,G/F,VANKAR TEXTILE MARKET(VTM),RING ROAD','SURAT','GUJARAT','','SU274',NULL,NULL,'0','CR','P/SUR/262','0',NULL,NULL,'0',NULL,0),
('P/2/263','FABULASTIC FAB','641,VANKAR TEXTILE MARKETRING ROAD','SURAT','GUJARAT','9712384888,7041136688','SU289',NULL,NULL,'0','CR','P/SUR/263','0',NULL,NULL,'0',NULL,0),
('P/2/264','KAYA FASHION','S.NO.D-1271,72,RADHA KRISHNA TXTILE MRKET RING ROAD','SURAT','GUJARAT','0261-3921321,9925921321','SU298',NULL,NULL,'0','CR','P/SUR/264','0',NULL,NULL,'0',NULL,0),
('P/2/265','K9 CREATIONS','D-569/70,U/G NEW TEXTILE MRKETRING ROAD','SURAT','GUJARAT','0261-3063155,9377757706','SU235',NULL,NULL,'0','CR','P/SUR/265','0',NULL,NULL,'0',NULL,0),
('P/2/266','RUDRA COTTON HOUSE PVT.LTD','D-1288 TO 90,U/G RADHA KRISHNAMARKET,RING ROAD]','SURAT','GUJARAT','0261-3001290,9825750674','SU285',NULL,NULL,'0','CR','P/SUR/266','0',NULL,NULL,'0',NULL,0),
('P/2/267','SHITAL SELECTION','G-2,SHANKAR TEXTUILES MARKETMOTI BEGUMWADI','SURAT','GUJARAT','9898977820','SU307',NULL,NULL,'0','CR','P/SUR/267','0',NULL,NULL,'0',NULL,0),
('P/2/268','KAPLON FABRICS','U-1226 G/F SURAT TEXTIE MARKETRING ROAD','SURAT','GUJARAT','0261-2312883,9327336262','SU144',NULL,NULL,'0','CR','P/SUR/268','0',NULL,NULL,'0',NULL,0),
('P/2/269','AYUSHI CREATION','2021,UPPER GROUND JAY LAXMIMARKET RING ROAD','SURAT','GUJARAT','09328221646,08000821646','SU308',NULL,NULL,'0','CR','P/SUR/269','0',NULL,NULL,'0',NULL,0),
('P/2/27','SAKSHI FASHION C/O VIJAY PRINT','54,ABHISHEK MAREKET','SURAT','GUJARAT','','SU178',NULL,NULL,'0','CR','P/SUR/27','0',NULL,NULL,'0',NULL,0),
('P/2/270','PRAYOSHA SAREE','S-550,J.J.A/C MARKETU/GRING ROAD','SURAT','GUJARAT','0261-3049676','SU343','','24AAPFP3367E1Z4','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/2/271','HARI OM PRINTS','1009,LOWE GROUND TIRUPATIMARKET RING ROAD','SURAT','GUJARAT','2333153,9825113209','SU312',NULL,NULL,'0','CR','P/SUR/271','0',NULL,NULL,'0',NULL,0),
('P/2/272','PINAL TEX FAB','501,UPPER GROUND N.T.MRING ROAD','SURAT','GUJARAT','2365470','SU317',NULL,NULL,'0','CR','P/SUR/272','0',NULL,NULL,'0',NULL,0),
('P/2/273','RADHIKA FASHION','2024,U/G JAY LAXMI MARKETMOTIBEGUMWADI RING ROAD','SURAT','GUJARAT','9913779371,8690802864','SU381',NULL,NULL,'0','CR','P/SUR/273','0',NULL,NULL,'0',NULL,0),
('P/2/274','SHREE BHAGWATI CREATION3','1024,U/G JAI LAXMI MARKETRING ROAD','SURAT','GUJARAT','9974374610,9377933725','SU382',NULL,NULL,'0','CR','P/SUR/274','0',NULL,NULL,'0',NULL,0),
('P/2/275','MEHUL FASHION','1st FLOORRING ROAD','SURAT','GUJARAT','','SU383',NULL,NULL,'0','CR','P/SUR/275','0',NULL,NULL,'0',NULL,0),
('P/2/276','GAURAV SILK MILLS','A-10014-15,UPPER GROUNDRKT MKTRING ROAD','SURAT','GUJARAT','02612324556','','','24AYLPV0826C2ZH','0','CR','','0','9825141926','','','PURCHASE O MP',1),
('P/2/277','SASYA DESIGNER','A/4,RADHA KRISHNA LOGISTIK PARKBHARAT CANCER HOSPITALSAROLI','SURAT','GUJARAT','','','','24APZPV5572L1ZQ','0','CR','','0','9429838639,9879535559','','','PURCHASE O MP',1),
('P/2/278','RIDHAM FASHIONS PVT.LTD.','.N-1740-41 G/F   MILLENIUM TEXTILES MARKETRING ROAD','SURAT','GUJARAT','0261-2356482','','','24AAFCR8541K1Z3','0','CR','','0','9376019203','9376019202','','PURCHASE O MP',1),
('P/2/279','VEDANTA FABRICS','M-1685,86 GROUND FLOOEMILLENIUM TEXTILES MARKETRING ROAD','SURAT','GUJARAT','2341920','','','24AKHPJ8636Q1ZH','0','CR','','0','9825240158,9979842290','','','PURCHASE O MP',1),
('P/2/28','NUTAN PRINTS','2091,MAHAVIR MARKET,','SURAT','GUJARAT','','SU180',NULL,NULL,'0','CR','P/SUR/28','0',NULL,NULL,'0',NULL,0),
('P/2/280','KAJREE FASHION','A/4065-66 LIFT NO.12L.PB/H BHARAT CANCER HOSTITAL','SURAT','GUJARAT','','','','24AMJPB2698G1Z2','0','CR','','0','9825964293','','','PURCHASE 12%',0),
('P/2/281','SUBHAM FACTORY OUTLET','S.NO.1,2,3 JAY SHREE RAM MARKETRING ROAD ','SURAT','GUJARAT','','','','24CZMPK7868R1Z5','0','CR','','0','9377840625,9913587129','','','PURCHASE 5%',1),
('P/2/282','VANYA DESIGNER','P/2823-25,MILLENIUM TEXTILES MRKETRING ROAD','SURAT','GUJARAT','0261-234469','vanyadesigner@gmail.com','','24AAJFV9224E1Z4','0','CR','','0','7878014469','','','PURCHASE 5%',1),
('P/2/29','RUPANA SILK MILLS','3038,2nd FLOORMAHAVEER TEXTILE MARKET','SURAT','GUJARAT','','SU181',NULL,NULL,'0','CR','P/SUR/29','0',NULL,NULL,'0',NULL,0),
('P/2/3','HARE KRISHNA SILK MILLS','764-65,UPPER GROUND,ABHISHEKMARKET,RING ROAD','SURAT','GUJARAT','2349703','SU103',NULL,NULL,'0','CR','P/SUR/3','0',NULL,NULL,'0',NULL,0),
('P/2/30','SARASWATI FASHION','612,SAI ASHARAM MARKET','SURAT','GUJARAT','','SU187',NULL,NULL,'0','CR','P/SUR/30','0',NULL,NULL,'0',NULL,0),
('P/2/31','T-TEX','S-2376,2,nd,FLOOR,N.T.M.','SURAT','GUJARAT','','SU188',NULL,NULL,'0','CR','P/SUR/31','0',NULL,NULL,'0',NULL,0),
('P/2/32','FEMINA FASHION','G-2/3,LALSHAH MARKET,OPP.ALISHAN MARKET,CHOWK BAZAR','SURAT','GUJARAT','','SU189',NULL,NULL,'0','CR','P/SUR/32','0',NULL,NULL,'0',NULL,0),
('P/2/33','MAKKI TEXTILE','101,ALISHAN MARKET,CHOWK BAZAR','SURAT','GUJARAT','','SU191',NULL,NULL,'0','CR','P/SUR/33','0',NULL,NULL,'0',NULL,0),
('P/2/34','SUIT WORLD','J-684-85,N.T.M.','SURAT','GUJARAT','','SU192',NULL,NULL,'0','CR','P/SUR/34','0',NULL,NULL,'0',NULL,0),
('P/2/35','BALAJI TERINE','204,ADARSH MARKET-2,','SURAT','GUJARAT','','SU201',NULL,NULL,'0','CR','P/SUR/35','0',NULL,NULL,'0',NULL,0),
('P/2/36','PARSURAM FAB {NAVAL}','C-2028,1,ST FLOOR,S.T.M.  RING ROAD','SURAT','GUJARAT','2344871,2322647','SU205',NULL,NULL,'0','CR','P/SUR/36','0',NULL,NULL,'0',NULL,0),
('P/2/37','SHWETA SYNTHETICS','J/131,LOWAR GROUND,J.J.A.C.MARKET.RING ROAD,','SURAT','GUJARAT','0261-2334131','SU207',NULL,NULL,'0','CR','P/SUR/37','0',NULL,NULL,'0',NULL,0),
('P/2/38','SHREE DUTT KUSHAL','1008,SWADESHI MARKETRING ROAD,','SURAT','GUJARAT','2352153,9427140903','SU213',NULL,NULL,'0','CR','P/SUR/38','0',NULL,NULL,'0',NULL,0),
('P/2/39','PARTH CREATION','509,SWADESHI MARKET','SURAT','GUJARAT','','SU214',NULL,NULL,'0','CR','P/SUR/39','0',NULL,NULL,'0',NULL,0),
('P/2/4','SALONI SAREES','612,ABHISHEK MARKET,RING ROAD','SURAT','GUJARAT','0261-2326710, 9374735262','SU105',NULL,NULL,'0','CR','P/SUR/4','0',NULL,NULL,'0',NULL,0),
('P/2/40','MAHALAXMI FABRICS','S.S.FASHION,3079-80,ROAD,','SURAT','GUJARAT','02613006033,9374374333','SU216','','','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/2/41','SABA FABRICS','VADA CHAUTA,CHOWK BAZAR,','SURAT','GUJARAT','','SU219',NULL,NULL,'0','CR','P/SUR/41','0',NULL,NULL,'0',NULL,0),
('P/2/42','SATGURU TEXTILE','502,N.T.M','SURAT','GUJARAT','','SU223',NULL,NULL,'0','CR','P/SUR/42','0',NULL,NULL,'0',NULL,0),
('P/2/43','FLORAL CREATION','A-1010-11,R.K.T.MARKET','SURAT','GUJARAT','2333456,3216707','SU225',NULL,NULL,'0','CR','P/SUR/43','0',NULL,NULL,'0',NULL,0),
('P/2/44','MANOJKUMAR JAIN (H.U.F.)','3091,2,ND FLOORSHRI MAHAVIR TEXTILE MARKET','SURAT','GUJARAT','','SU228',NULL,NULL,'0','CR','P/SUR/44','0',NULL,NULL,'0',NULL,0),
('P/2/45','GOLDEN CORPORATION','S-1240-41,G/F,S.T.M.','SURAT','GUJARAT','','SU229',NULL,NULL,'0','CR','P/SUR/45','0',NULL,NULL,'0',NULL,0),
('P/2/46','ASHAPURI SILK MILLES','294,ABHINADAN MARKETRING ROAD','SURAT','GUJARAT','2312197,9714134558','SU231',NULL,NULL,'0','CR','P/SUR/46','0',NULL,NULL,'0',NULL,0),
('P/2/47','VARMAN SILK MILLS','2007,2,ND FLOOR,SWADESHI MARKET','SURAT','GUJARAT','','SU233',NULL,NULL,'0','CR','P/SUR/47','0',NULL,NULL,'0',NULL,0),
('P/2/48','VIDHATA FABRICS (SALONI)','612,ABHISHEK MARKET','SURAT','GUJARAT','','SU236',NULL,NULL,'0','CR','P/SUR/48','0',NULL,NULL,'0',NULL,0),
('P/2/49','GANESH SILK MILLS (SIGMA SAREE','603,U/G,ABHISHEK MARKET','SURAT','GUJARAT','','SU237',NULL,NULL,'0','CR','P/SUR/49','0',NULL,NULL,'0',NULL,0),
('P/2/5','MANBHARI PRINTS','M-1277,S.T.M,RING ROAD,','SURAT','GUJARAT','0261-2321330','SU106','','','0','CR','','0','',NULL,'0','null',1),
('P/2/50','KAYAKALP TEXTILES','U-1226,G/F,S.T.M.RING ROAD','SURAT','GUJARAT','2312883,9327336262','SU240',NULL,NULL,'0','CR','P/SUR/50','0',NULL,NULL,'0',NULL,0),
('P/2/51','SANJAY ENTERPRISE (JAIN SYN.)','638,ABHISHEK MARKETRING ROAD','SURAT','GUJARAT','02612364960','SU244',NULL,NULL,'0','CR','P/SUR/51','0',NULL,NULL,'0',NULL,0),
('P/2/52','JAGDISH PRINTS','613,U/G,ABHISHEK MARKETRING ROAD','SURAT','GUJARAT','2352144,9375957582','SU245',NULL,NULL,'0','CR','P/SUR/52','0',NULL,NULL,'0',NULL,0),
('P/2/53','WESTERN SAREES','1017,1stFLOOR,SWADESHI TEXTILE MARKET','SURAT','GUJARAT','','SU246',NULL,NULL,'0','CR','P/SUR/53','0',NULL,NULL,'0',NULL,0),
('P/2/54','HETVI TEXTILE','501,N.T.M.','SURAT','GUJARAT','','SU253',NULL,NULL,'0','CR','P/SUR/54','0',NULL,NULL,'0',NULL,0),
('P/2/55','DHARTI PRINTS','1030-31,L/G,TIRUPATI MARKET','SURAT','GUJARAT','','SU255',NULL,NULL,'0','CR','P/SUR/55','0',NULL,NULL,'0',NULL,0),
('P/2/56','AGARWAL ENTERPRISES','1051,N.T.M.','SURAT','GUJARAT','','SU256',NULL,NULL,'0','CR','P/SUR/56','0',NULL,NULL,'0',NULL,0),
('P/2/57','BALAJI TEX FAB','118,1,ST, FLOORSHANKAR MARKET RING ROAD','SURAT','GUJARAT','0261-2323677,9377655776','SU258',NULL,NULL,'0','CR','P/SUR/57','0',NULL,NULL,'0',NULL,0),
('P/2/58','JAGADAMBA FASHIONS','613,U/G,ABHISHEK TEXTILEMARKET','SURAT','GUJARAT','','SU259',NULL,NULL,'0','CR','P/SUR/58','0',NULL,NULL,'0',NULL,0),
('P/2/59','SHREEJEE INTERNATIONAL','S-119,L/G,J.J.A.C.MARKET','SURAT','GUJARAT','','SU262',NULL,NULL,'0','CR','P/SUR/59','0',NULL,NULL,'0',NULL,0),
('P/2/6','RUCHIKA PRINTS','509,UPPER GROUND,SWADESHIMARKET,RING ROAD','SURAT','GUJARAT','0261635582','SU107',NULL,NULL,'0','CR','P/SUR/6','0',NULL,NULL,'0',NULL,0),
('P/2/60','SHREE HAJARIMAL PRINTS','S-109,L/G,J.J.A.C.KARKET','SURAT','GUJARAT','','SU263',NULL,NULL,'0','CR','P/SUR/60','0',NULL,NULL,'0',NULL,0),
('P/2/61','VISHAKHA SILK MILLS','1033,G/F,MAHAVIR MARKET','SURAT','GUJARAT','02612354573,9825630317','SU264',NULL,NULL,'0','CR','P/SUR/61','0',NULL,NULL,'0',NULL,0),
('P/2/62','RONAK FASHION','1014,G/F,MAHAVIR MARKET','SURAT','GUJARAT','','SU265',NULL,NULL,'0','CR','P/SUR/62','0',NULL,NULL,'0',NULL,0),
('P/2/63','KANHAIYA PRINTS','3076,2,ND,FLOORSHREE MAHAVIR TEXTILE MARKET','SURAT','GUJARAT','','SU266',NULL,NULL,'0','CR','P/SUR/63','0',NULL,NULL,'0',NULL,0),
('P/2/64','SAHIL FASHION','G/13,SILK DOM MARKETCHOWK BAZAR','SURAT','GUJARAT','','SU268',NULL,NULL,'0','CR','P/SUR/64','0',NULL,NULL,'0',NULL,0),
('P/2/65','RINKESH FASHION','ADARSH MARKETRING ROAD','SURAT','GUJARAT','','SU270',NULL,NULL,'0','CR','P/SUR/65','0',NULL,NULL,'0',NULL,0),
('P/2/66','VEERPRABHU SILK MILLS(VAIBAV)','615,ABHISHEK TEXTILE MARKET,RING ROAD,','SURAT','GUJARAT','2202793','SU196',NULL,NULL,'0','CR','P/SUR/66','0',NULL,NULL,'0',NULL,0),
('P/2/67','ICON SAREE(KAYAKALP)','U-1226,GROUND FLOOR,SURAT TEXTILE MARKET RING ROAD','SURAT','GUJARAT','2312883,5511226','SU272',NULL,NULL,'0','CR','P/SUR/67','0',NULL,NULL,'0',NULL,0),
('P/2/68','LIRIL FASHION(KALA KUNJ)','668, ABHISHEK TEXTILE MARKETRING ROAD,','SURAT','GUJARAT','2202220','SU273',NULL,NULL,'0','CR','P/SUR/68','0',NULL,NULL,'0',NULL,0),
('P/2/69','VIVEL FASHION','693, ABHISHEK TEXTILE MARKETRING ROAD','SURAT','GUJARAT','2202031','SU275',NULL,NULL,'0','CR','P/SUR/69','0',NULL,NULL,'0',NULL,0),
('P/2/7','NEHA SILK MILLS','502-3,ASHOKA TOWER,','SURAT','GUJARAT','0261-2334945','SU114',NULL,NULL,'0','CR','P/SUR/7','0',NULL,NULL,'0',NULL,0),
('P/2/70','ADESH INTERNATIONAL(OSCAR)','211,2ND FLOOR,451 TEXTILESMARKET,RING ROAD','SURAT','GUJARAT','2324644,2324645','SU276',NULL,NULL,'0','CR','P/SUR/70','0',NULL,NULL,'0',NULL,0),
('P/2/71','B.B.SYNTHETICS','3070,SHREE MAHAVIR TEXTILES MARKET,RING ROAD,','SURAT','GUJARAT','9879227050','SU277',NULL,NULL,'0','CR','P/SUR/71','0',NULL,NULL,'0',NULL,0),
('P/2/72','RAJAT FASHION(RONAK)','1014,GROUND FLOOR,MAHAVIRMARKET,RING ROAD,','SURAT','GUJARAT','9998689351','SU278',NULL,NULL,'0','CR','P/SUR/72','0',NULL,NULL,'0',NULL,0),
('P/2/73','BAJAJ CREATION','B-2055-56-57,MILLENIUM TEXTILEMARKET','SURAT','GUJARAT','','SU279',NULL,NULL,'0','CR','P/SUR/73','0',NULL,NULL,'0',NULL,0),
('P/2/74','BALAJI SILK MILLS','3001, SHREE MAHAVIR MARKET','SURAT','GUJARAT','','SU281',NULL,NULL,'0','CR','P/SUR/74','0',NULL,NULL,'0',NULL,0),
('P/2/75','GAZI TEXTILES','B-28 SILK PALACE  MARKET,CHOWK BAZAR,','SURAT','GUJARAT','9879060961','SU282',NULL,NULL,'0','CR','P/SUR/75','0',NULL,NULL,'0',NULL,0),
('P/2/76','SANJARI FASHION','CHOWK BAZAR','SURAT','GUJARAT','','SU284',NULL,NULL,'0','CR','P/SUR/76','0',NULL,NULL,'0',NULL,0),
('P/2/77','SABRI TEX','G-102,ALISHAN MARKET,CHOWK BAZAR,','SURAT','GUJARAT','9879412307','SU286',NULL,NULL,'0','CR','P/SUR/77','0',NULL,NULL,'0',NULL,0),
('P/2/78','SIDDHI VINAYAK FASHION','2110,UPPER GROUND,SHIV SHAKTITEXTILE MARKET,RING ROAD,','SURAT','GUJARAT','02613918555','SU287',NULL,NULL,'0','CR','P/SUR/78','0',NULL,NULL,'0',NULL,0),
('P/2/79','VIMLA FASHION','3050,ABHISHEK TEXTILE MARKETRING ROAD,','SURAT','GUJARAT','02612202103','SU288',NULL,NULL,'0','CR','P/SUR/79','0',NULL,NULL,'0',NULL,0),
('P/2/8','EAST INDIA SAREES','1001-2,SWADESHI MARKET','SURAT','GUJARAT','0261-2338221','SU117',NULL,NULL,'0','CR','P/SUR/8','0',NULL,NULL,'0',NULL,0),
('P/2/80','VAMA CREATION','1005,SWADESHI MARKET,RING ROAD,','SURAT','GUJARAT','02612202699','SU292',NULL,NULL,'0','CR','P/SUR/80','0',NULL,NULL,'0',NULL,0),
('P/2/81','VIDHI PATI SAREES(VIDDHI SAREE','2011,2nd FLOOR,SWADESHI MARKETRING ROAD,','SURAT','GUJARAT','2330768','SU293',NULL,NULL,'0','CR','P/SUR/81','0',NULL,NULL,'0',NULL,0),
('P/2/82','D.K.CREATION(AGGARAWAL)','1160,NEW TEXTILES MARKET,RING ROAD,','SURAT','GUJARAT','9377605921','SU294',NULL,NULL,'0','CR','P/SUR/82','0',NULL,NULL,'0',NULL,0),
('P/2/83','SIDDHI VINAYAK KNOTS&PRINTSP.L','S-538,U/G,FLOOR,J.J. A/ MARKET,','SURAT','GUJARAT','0261-2368755','SU291',NULL,NULL,'0','CR','P/SUR/83','0',NULL,NULL,'0',NULL,0),
('P/2/84','ROOHANI TEXTILES','G-5/6,SILK SAGAR MARKET,CHOWK BAZAR','SURAT','GUJARAT','','SU296',NULL,NULL,'0','CR','P/SUR/84','0',NULL,NULL,'0',NULL,0),
('P/2/85','INTERNATIONAL CREATIONS PVT.','561,UPPER GROUND FLOOR,ADARSH MARKET ,RING ROAD,','SURAT','GUJARAT','2342574','SU290',NULL,NULL,'0','CR','P/SUR/85','0',NULL,NULL,'0',NULL,0),
('P/2/86','ROHIT FASHION','1073-74,TIRUPATI MARKET,RING ROAD,','SURAT','GUJARAT','9998620233','SU299',NULL,NULL,'0','CR','P/SUR/86','0',NULL,NULL,'0',NULL,0),
('P/2/87','SUNSHINE','557-58,UPPER GROUND ADARSHMARKET,RING ROAD,','SURAT','GUJARAT','0261-2320048,3003437','SU300',NULL,NULL,'0','CR','P/SUR/87','0',NULL,NULL,'0',NULL,0),
('P/2/88','KAVITA FASHION','546-47,ADARSH MARKET,RING ROAD,','SURAT','GUJARAT','2326052,3916052','SU301',NULL,NULL,'0','CR','P/SUR/88','0',NULL,NULL,'0',NULL,0),
('P/2/89','VISHWAM FABRICS (INTEL)','SHOP NO.105,NEW TEXTILESMARKET,RING ROAD,','SURAT','GUJARAT','2331348,3927852','SU302',NULL,NULL,'0','CR','P/SUR/89','0',NULL,NULL,'0',NULL,0),
('P/2/9','AVYA TEXTILES (VARDHMAN)','Z-1199,GROUND FLOOR,S.T.M.','SURAT','GUJARAT','2363155','SU118',NULL,NULL,'0','CR','P/SUR/9','0',NULL,NULL,'0',NULL,0),
('P/2/90','MARUDHAR KESHRI CREATION','N-2746-47,UPPER GROUND,MILLENNIUM,TEXTILES MARKET RING ROAD','SURAT','GUJARAT','02613111220','SU303',NULL,NULL,'0','CR','P/SUR/90','0',NULL,NULL,'0',NULL,0),
('P/2/91','RUCHI SAREES','G-1315,SURAT TEXTILES MARKETRING ROAD','SURAT','GUJARAT','2321676,3206676','SU304',NULL,NULL,'0','CR','P/SUR/91','0',NULL,NULL,'0',NULL,0),
('P/2/92','UMESH SILK FASHION','U-1224,SURAT TEXTILES MARKET,RING ROAD','SURAT','GUJARAT','2322206,6611159','SU305',NULL,NULL,'0','CR','P/SUR/92','0',NULL,NULL,'0',NULL,0),
('P/2/93','NAVKAR DESIGNER (KALA DESIGNER','S-225,LOWER GROUN,J.J.A.C.MARKET,RING ROAD','SURAT','GUJARAT','2353197','SU306',NULL,NULL,'0','CR','P/SUR/93','0',NULL,NULL,'0',NULL,0),
('P/2/94','NARAYANI SAREE','507,UPPER GROUND,SWADESHI TEX.MARKET ,RING ROAD,','SURAT','GUJARAT','2334255','SU309',NULL,NULL,'0','CR','P/SUR/94','0',NULL,NULL,'0',NULL,0),
('P/2/95','ARCHANA FABRICS','1086,G/F,HARIOM MARKETRING ROAD','SURAT','GUJARAT','3014459','SU311',NULL,NULL,'0','CR','P/SUR/95','0',NULL,NULL,'0',NULL,0),
('P/2/96','ARTI CREATION','C-2129/30,U/G,MILLENIUM MARKET','SURAT','GUJARAT','','SU313',NULL,NULL,'0','CR','P/SUR/96','0',NULL,NULL,'0',NULL,0),
('P/2/97','VISHAL FASHIONS PVT.LTD.','\"VISHAL HOUSE\",102,WORLD TRADECENTER,RING ROAD,','SURAT','GUJARAT','3918239-40','SU315',NULL,NULL,'0','CR','P/SUR/97','0',NULL,NULL,'0',NULL,0),
('P/2/98','B.S.AGARWAL','1159,N.T.M.RING ROAD','SURAT','GUJARAT','9377605921,3267664','SU316',NULL,NULL,'0','CR','P/SUR/98','0',NULL,NULL,'0',NULL,0),
('P/2/99','S.A.FABRICS','G-106,AALISHAN MAR.CHOWK BAZAR,','SURAT','GUJARAT','9727144789','SU318',NULL,NULL,'0','CR','P/SUR/99','0',NULL,NULL,'0',NULL,0),
('P/20/1','MAHADEV BANDEJ & PRINT','KHODPARA,JIN PLOT','JETPUR','','9624632500','JE101',NULL,NULL,'0','CR','P/JET/1','0',NULL,NULL,'0',NULL,0),
('P/20/2','EMPIRE CREATION','MAIN ROAD NAVAGADH','JETPUR','','02823-220582','JE102',NULL,NULL,'0','CR','P/JET/2','0',NULL,NULL,'0',NULL,0),
('P/22/1','PRAMUKH TARDING','SHROFF BAZAR OCHI STREET NR.SONI BAZAR','BHUJ-KUTCH','GUJARAT','9825327917,9925147698','KU101',NULL,NULL,'0','CR','P/KUT/1','0',NULL,NULL,'0',NULL,0),
('P/23/1','ANKUR','114,VALLABH BARI','KOTA','RAJASTHAN','2380418','KOT01',NULL,NULL,'0','CR','P/KOT/1','0',NULL,NULL,'0',NULL,0),
('P/25/2','SUNSHINE TEXTILES','471-A,ROOM NO.5,BEHIND GOVINDPALACE,KRISHNA GALLI,BHAJI MKT','ULHASNAGAR','UTTAR PRADESH','','UL102',NULL,NULL,'0','CR','P/ULH/2','0',NULL,NULL,'0',NULL,0),
('P/25/3','KRISHNA','21,P.R.TEXTILE MARKET,HANUMAN GALLI,','ULHASNAGAR','UTTAR PRADESH','','UL103',NULL,NULL,'0','CR','P/ULH/3','0',NULL,NULL,'0',NULL,0),
('P/25/4','FEELINGS','3,SHAHENSHA ARCADEJAPANI BAZAR','ULHASNAGAR','UTTAR PRADESH','','UL104',NULL,NULL,'0','CR','P/ULH/4','0',NULL,NULL,'0',NULL,0),
('P/26/1','JINDAT SAREE HOUSE','BEGANI CHOWK,','BIKANER','RAJASTHAN','09414143867,25244667','BI101',NULL,NULL,'0','CR','P/BIK/1','0',NULL,NULL,'0',NULL,0),
('P/26/2','KRISHNA SAREE CENTER','MOHTA CHOWK,','BIKANER','RAJASTHAN','','BI102',NULL,NULL,'0','CR','P/BIK/2','0',NULL,NULL,'0',NULL,0),
('P/26/3','BINNANI BANDHU','MOHTA CHOWK','BIKANER','RAJASTHAN','01512201481','BI103',NULL,NULL,'0','CR','P/BIK/3','0',NULL,NULL,'0',NULL,0),
('P/26/4','BINNANI COLLECTION','MOHTA CHOWK','BIKANER','RAJASTHAN','','BI104',NULL,NULL,'0','CR','P/BIK/4','0',NULL,NULL,'0',NULL,0),
('P/27N/1','MOTANAL & SONS','MOTAMAL GALICHANDERI,DIST ASHOK NAGAR','CHANDERI','MADHYA PRADESH','09406509178','CAN01',NULL,NULL,'0','CR','P/CHAN/1','0',NULL,NULL,'0',NULL,0),
('P/28/1','NAVRATHAN HANDLOOMS','366,MINT STREET, SOWCARPET,','CHENNAI','TAMILNADU','25290902','CHE01',NULL,NULL,'0','CR','P/CHE/1','0',NULL,NULL,'0',NULL,0),
('P/29/1','DA107','151,M.GANDHI MARKETKING,S CIRCLE','DADAR','MAHARASHTRA','','DA107',NULL,NULL,'0','CR','P/DAD/1','0',NULL,NULL,'0',NULL,0),
('P/29/10','BHAGWATI SAREE CENTER','17-18,NEW CLOTH SECTIONM.G.MARKET,KING,S CIRCLE,','DADAR','MAHARASHTRA','022-24094642','DA104',NULL,NULL,'0','CR','P/DAD/10','0',NULL,NULL,'0',NULL,0),
('P/29/11','DA106','107,M.GANDHI MARKETKING,S CIRCLE','DADAR','MAHARASHTRA','022-66624153','DA106',NULL,NULL,'0','CR','P/DAD/11','0',NULL,NULL,'0',NULL,0),
('P/29/12','SHREYA FASHION','16/B,KRISHNA NIWAS,OPP.MUNCIPLSCHOOL,DR. A.B.ROAD HINDMATA','DADAR','MAHARASHTRA','022-24158213','DA102',NULL,NULL,'0','CR','P/DAD/12','0',NULL,NULL,'0',NULL,0),
('P/29/13','GURU KRIPA','149,M.GANDHI MARKET,KING,S CIRCLE,','DADAR','MAHARASHTRA','24018181','DA103',NULL,NULL,'0','CR','P/DAD/13','0',NULL,NULL,'0',NULL,0),
('P/29/14','PARAS DUPATTA','101,M.GANDHI MARKET,KING,S CIRCLE','DADAR','MAHARASHTRA','66624224,9820322127(GIRIS','DA105',NULL,NULL,'0','CR','P/DAD/14','0',NULL,NULL,'0',NULL,0),
('P/29/2','MEERA SILK STORE','128,M.GANDHIMARKET','DADAR','MAHARASHTRA','','DA108',NULL,NULL,'0','CR','P/DAD/2','0',NULL,NULL,'0',NULL,0),
('P/29/3','JANVI','124,M.GANDHI MARKET','DADAR','MAHARASHTRA','','DA109',NULL,NULL,'0','CR','P/DAD/3','0',NULL,NULL,'0',NULL,0),
('P/29/4','PRETTY WOMEN','145,M.GHANDHI MARKET','DADAR','MAHARASHTRA','','DA110',NULL,NULL,'0','CR','P/DAD/4','0',NULL,NULL,'0',NULL,0),
('P/29/5','JAI AMBE','1,2&17,NEW CLOTH SECTIONM.GHANDHI MARKET','DADAR','MAHARASHTRA','','DA113',NULL,NULL,'0','CR','P/DAD/5','0',NULL,NULL,'0',NULL,0),
('P/29/6','VISHAL CREATION','163.M.G.MARKETKING,S CIRCLE','DADAR','MAHARASHTRA','','DA114',NULL,NULL,'0','CR','P/DAD/6','0',NULL,NULL,'0',NULL,0),
('P/29/7','MORDEN MATCHING CENTRE','11/12,NEW CLOTH SECTIONM.GHANDHI MARKET','DADAR','MAHARASHTRA','','DA115',NULL,NULL,'0','CR','P/DAD/7','0',NULL,NULL,'0',NULL,0),
('P/29/8','RIDDHI FASHION','13,NEW CLOTH SECTIONM.G.MARKET','DADAR','MAHARASHTRA','','DA117',NULL,NULL,'0','CR','P/DAD/8','0',NULL,NULL,'0',NULL,0),
('P/29/9','DEEP COLLECTION PVT.LTD.','24,HINDMATA CLOTH MARKET,BELOW HOTEL SHANTIDOOT,','DADAR','MAHARASHTRA','022-24151135','DA101',NULL,NULL,'0','CR','P/DAD/9','0',NULL,NULL,'0',NULL,0),
('P/3/1','SAGAR FASHION','4279,\"SHIV BHAWAN\"2nd FLOOR,SANJAY BAZAR,NR.SANGANERI GATE','JAIPUR','RAJASTHAN','0141-2573240','JA101',NULL,NULL,'0','CR','P/JAI/1','0',NULL,NULL,'0',NULL,0),
('P/3/10','GANESHLAL SAGARMAL JAIN','233,JOHARI BAZAR,','JAIPUR','RAJASTHAN','2571214','JA110',NULL,NULL,'0','CR','P/JAI/10','0',NULL,NULL,'0',NULL,0),
('P/3/11','LAXSHMI SAREES','239,MAYA BHAWAN,1,st FLOORPUROHITJI KA KATLA','JAIPUR','RAJASTHAN','2578372','JA111',NULL,NULL,'0','CR','P/JAI/11','0',NULL,NULL,'0',NULL,0),
('P/3/12','SAREE HOUSE','SARVESHWAR MANSION,NR.MALPANICOMPLEX,KATLA PUTOHITJI KA','JAIPUR','RAJASTHAN','2572162','JA112',NULL,NULL,'0','CR','P/JAI/12','0',NULL,NULL,'0',NULL,0),
('P/3/13','JAI BAWANI COLLECTION','2345,DHULA HOUSE ROADNEAR HANUMAN MANDIR','JAIPUR','RAJASTHAN','0141-3224028,9828330845','JA113',NULL,NULL,'0','CR','P/JAI/13','0',NULL,NULL,'0',NULL,0),
('P/3/14','THE RAJASTHAN PRINT (PINKI)','15,BAPU BAZAR','JAIPUR','RAJASTHAN','2576499','JA114',NULL,NULL,'0','CR','P/JAI/14','0',NULL,NULL,'0',NULL,0),
('P/3/15','GOPAL TEXTILES (MAMA PRINTS)','76,BAPU BAZAR','JAIPUR','RAJASTHAN','','JA115',NULL,NULL,'0','CR','P/JAI/15','0',NULL,NULL,'0',NULL,0),
('P/3/16','JYOTI PRINTS','B-5,LORDI HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','2578752','JA116',NULL,NULL,'0','CR','P/JAI/16','0',NULL,NULL,'0',NULL,0),
('P/3/17','KARISHMA','3,AGARWAL BUILDINGDHULA HOUSE,BAPU BAZAR','JAIPUR','RAJASTHAN','9414045636','JA117',NULL,NULL,'0','CR','P/JAI/17','0',NULL,NULL,'0',NULL,0),
('P/3/18','RAMGOPAL RAMBALLABH','201,JOHARI BAZAR','JAIPUR','RAJASTHAN','01412571437','JA118',NULL,NULL,'0','CR','P/JAI/18','0',NULL,NULL,'0',NULL,0),
('P/3/19','JAGDISH SONS','SHIV KRIPA MANSION,1,st FLOORDHULA HOUSE BAPU BAZAR','JAIPUR','RAJASTHAN','0141-3106340','JA119',NULL,NULL,'0','CR','P/JAI/19','0',NULL,NULL,'0',NULL,0),
('P/3/2','MEERA PRINTS (SONA)','22,SAI MANSION,DHULA HOUSE,BAPU BAZAR,','JAIPUR','RAJASTHAN','0141-2574237','JA102',NULL,NULL,'0','CR','P/JAI/2','0',NULL,NULL,'0',NULL,0),
('P/3/20','KING CITY BANDHANI','KHANDELWAL COMPLEX,DHULA HOUSE BAPU BAZAR','JAIPUR','RAJASTHAN','9414064169','JA120',NULL,NULL,'0','CR','P/JAI/20','0',NULL,NULL,'0',NULL,0),
('P/3/21','AKASH DEEP TEXTILES','21,SAI MANSION,DHULA HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','2573582','JA121',NULL,NULL,'0','CR','P/JAI/21','0',NULL,NULL,'0',NULL,0),
('P/3/22','HERRY FASHION','DHULA HOUSE,(CHOWK)BAPU BAZAR','JAIPUR','RAJASTHAN','9351486633','JA122',NULL,NULL,'0','CR','P/JAI/22','0',NULL,NULL,'0',NULL,0),
('P/3/23','M.S.BANDHANI','752,BANWALON KA GATE,CHAURA RASTA','JAIPUR','RAJASTHAN','2578923','JA123',NULL,NULL,'0','CR','P/JAI/23','0',NULL,NULL,'0',NULL,0),
('P/3/24','SHIVAM HANDLOOM (RASHTRIYA)','31,HULA HOUSE ROADBAPU BAZAR','JAIPUR','RAJASTHAN','','JA124',NULL,NULL,'0','CR','P/JAI/24','0',NULL,NULL,'0',NULL,0),
('P/3/25','NAKSHATRA FASHION (DARSHNA)','AGARWAL CHAMBER,DHULA HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','','JA125',NULL,NULL,'0','CR','P/JAI/25','0',NULL,NULL,'0',NULL,0),
('P/3/26','HARE RAMA TEXTILES (MAMA PRINT','76,BAPU BAZAR','JAIPUR','RAJASTHAN','','JA126',NULL,NULL,'0','CR','P/JAI/26','0',NULL,NULL,'0',NULL,0),
('P/3/27','ASHOK TRADERS (JYOTI)','9-10-11,BAPU BAZAR','JAIPUR','RAJASTHAN','','JA127',NULL,NULL,'0','CR','P/JAI/27','0',NULL,NULL,'0',NULL,0),
('P/3/28','OM CREATION','G-1,AKASHDEEP COMPLEX-2DHULA HOUSE,BAPU BAZAR','JAIPUR','RAJASTHAN','','JA128',NULL,NULL,'0','CR','P/JAI/28','0',NULL,NULL,'0',NULL,0),
('P/3/29','SONA PRINTS','16,SAI MANSION,DHULA HOUSE BAPU BAZAR','JAIPUR','RAJASTHAN','0141-6537720,9413339815','JA129',NULL,NULL,'0','CR','P/JAI/29','0',NULL,NULL,'0',NULL,0),
('P/3/3','KOMAL TEXTILES','F-1,AKASH DEEP COMPLEX,DHULA HOUSE BAPU BAZAR','JAIPUR','RAJASTHAN','094140-48572','JA103',NULL,NULL,'0','CR','P/JAI/3','0',NULL,NULL,'0',NULL,0),
('P/3/30','KALA SAGAR','1-2,SUNNY ARCADE,HELIPADA,RAMLAL JI KA RASTA','JAIPUR','RAJASTHAN','','JA130',NULL,NULL,'0','CR','P/JAI/30','0',NULL,NULL,'0',NULL,0),
('P/3/31','PAHNAVA','G-7-11,1,st AKASHDEEP COMPLEXDHULA HOUSE','JAIPUR','RAJASTHAN','','JA131',NULL,NULL,'0','CR','P/JAI/31','0',NULL,NULL,'0',NULL,0),
('P/3/32','VOLGA APPEARLS','37-38,DHULA HOSE ROADBAPU BAZAR','JAIPUR','RAJASTHAN','','JA132',NULL,NULL,'0','CR','P/JAI/32','0',NULL,NULL,'0',NULL,0),
('P/3/33','SUKRITI','4-5,G/F,SHIV KRIPA MANSION,DHUKA HOUSE','JAIPUR','RAJASTHAN','','JA133',NULL,NULL,'0','CR','P/JAI/33','0',NULL,NULL,'0',NULL,0),
('P/3/34','KAMDHENU TEXTILES','DHULA HOUSE BAPU BAZAR','JAIPUR','RAJASTHAN','','JA134',NULL,NULL,'0','CR','P/JAI/34','0',NULL,NULL,'0',NULL,0),
('P/3/35','LAXMI TEXTILE PRINTS','32,DHULA HOUSE ROADBAPU BAZAR','JAIPUR','RAJASTHAN','','JA135',NULL,NULL,'0','CR','P/JAI/35','0',NULL,NULL,'0',NULL,0),
('P/3/36','SHREE OM TEX PRINTS (SONA)','16,SAI MANSION,DHULA HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','','JA136',NULL,NULL,'0','CR','P/JAI/36','0',NULL,NULL,'0',NULL,0),
('P/3/37','VAISHNAVI TEXTILES (MAMA)','BAPU BAZAR','JAIPUR','RAJASTHAN','','JA137',NULL,NULL,'0','CR','P/JAI/37','0',NULL,NULL,'0',NULL,0),
('P/3/38','KAMYA KOLLECTIONS','118.1,st FLOOR,SUNNY ARCADERAM LALLA JI KA RASTA','JAIPUR','RAJASTHAN','','JA138',NULL,NULL,'0','CR','P/JAI/38','0',NULL,NULL,'0',NULL,0),
('P/3/39','S.K.SAREE PALACE(S.K.BHANDHANI','3,rd FLOOR PUROHIT JI KA NAYAKATLA','JAIPUR','RAJASTHAN','','JA139',NULL,NULL,'0','CR','P/JAI/39','0',NULL,NULL,'0',NULL,0),
('P/3/4','KALA NIKETAN','SAI MANSION DHULA HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','094140-78833','JA104',NULL,NULL,'0','CR','P/JAI/4','0',NULL,NULL,'0',NULL,0),
('P/3/40','MAHA GANPATI BANDHANI HOUSE','G-2-3,AKASHDEEP COMPLEX(FIRST)DHULA HOUSE,BAPU BAZAR','JAIPUR','RAJASTHAN','0141-4014851,9829115413','JA140',NULL,NULL,'0','CR','P/JAI/40','0',NULL,NULL,'0',NULL,0),
('P/3/41','RIDDHI SIDDHI SAREES','8,KAMLA MARKET,NR.MANGALAMDHULA HOUSE,BAPU BAZAR','JAIPUR','RAJASTHAN','9928312642,9414229475','JA141',NULL,NULL,'0','CR','P/JAI/41','0',NULL,NULL,'0',NULL,0),
('P/3/42','SHIV PARVATI SAREES','3,RD FLOOR,SHIV KRIPA MANSIONDHULA HOUSE ,BAPU BAZAR','JAIPUR','RAJASTHAN','','JA142',NULL,NULL,'0','CR','P/JAI/42','0',NULL,NULL,'0',NULL,0),
('P/3/43','JAIPUR TEXTILES(MAMA PRINTS)','75,BAPU BAZAR','JAIPUR','RAJASTHAN','0141-2573251,3265711','JA143',NULL,NULL,'0','CR','P/JAI/43','0',NULL,NULL,'0',NULL,0),
('P/3/44','GURU NANAK TEXTILES','DHULA HOUSE,NR.BACHHAWAT SAREEBAPU BAZAR','JAIPUR','RAJASTHAN','','JA144',NULL,NULL,'0','CR','P/JAI/44','0',NULL,NULL,'0',NULL,0),
('P/3/45','AMAR-SONS','25,26,27,JHULELAL CLOTH MARKETBAPU BAZAR','JAIPUR','RAJASTHAN','','JA145',NULL,NULL,'0','CR','P/JAI/45','0',NULL,NULL,'0',NULL,0),
('P/3/46','R.R.FASHION','153,HALDIYON KA RASTSKAMLA NEHRU SCHOOL,JOHARI BAZA','JAIPUR','RAJASTHAN','','JA146',NULL,NULL,'0','CR','P/JAI/46','0',NULL,NULL,'0',NULL,0),
('P/3/47','MANSI TEXTILES(AKASH DEEP)','S.NO.21,SAI MANSION DHULAHOUSE,BAPU BAZAR,','JAIPUR','RAJASTHAN','01412573934','JA147',NULL,NULL,'0','CR','P/JAI/47','0',NULL,NULL,'0',NULL,0),
('P/3/48','JINDAL SAREES','2,nd FLOOR,ABOVE UCO BANK,JOHARI BAZAR,','JAIPUR','RAJASTHAN','01402567940','JA148',NULL,NULL,'0','CR','P/JAI/48','0',NULL,NULL,'0',NULL,0),
('P/3/49','GULAB CHAND LAXMI NARAIAN','21,KHANDA HAWAMAHAL,NEARGANESH TEMPLE,','JAIPUR','RAJASTHAN','2609460//0141//2604355','JA149',NULL,NULL,'0','CR','P/JAI/49','0',NULL,NULL,'0',NULL,0),
('P/3/5','RAJA VEER COLLECTION','151-52,BAPU BAZAR','JAIPUR','RAJASTHAN','567991','JA105',NULL,NULL,'0','CR','P/JAI/5','0',NULL,NULL,'0',NULL,0),
('P/3/50','MANGLAM FASHIONS','MANGLAM HOUSE,DHULA HOUSE,MAINROAD BAPU BAZAR','JAIPUR','RAJASTHAN','01412578461,09828161164','JA150',NULL,NULL,'0','CR','P/JAI/50','0',NULL,NULL,'0',NULL,0),
('P/3/51','DIAMAND SAREES','17,SUNNY ARCADE,RAM LALAJIKA RASTA,JOHRI BAZAR,','JAIPUR','RAJASTHAN','9929796639,9314501629','JA151',NULL,NULL,'0','CR','P/JAI/51','0',NULL,NULL,'0',NULL,0),
('P/3/52','SAVITA PRINTS','18,SAI MANSION,DHULA HOUSE,BAPU BAZAR,','JAIPUR','RAJASTHAN','01412577077,9314887243','JA152',NULL,NULL,'0','CR','P/JAI/52','0',NULL,NULL,'0',NULL,0),
('P/3/53','DOGRI COLLECTION','F(12-13),KARTARPURADAM','JAIPUR','RAJASTHAN','9928505859,09314010030','JA153','','08AANFD7055MIZU','0','CR','','0','',NULL,'0','null',1),
('P/3/54','KURTI','17,SHIVAJI MARG,','JAIPUR','RAJASTHAN','','JA154',NULL,NULL,'0','CR','P/JAI/54','0',NULL,NULL,'0',NULL,0),
('P/3/55','SONA TEXTILES','11,KHANDILWAL COMPLEX(GROUND FLOR) DHULA HOUSE,BAPU BAZAR','JAIPUR','RAJASTHAN','9414060793','JA155',NULL,NULL,'0','CR','P/JAI/55','0',NULL,NULL,'0',NULL,0),
('P/3/56','AANCHAL FABTEX INDIA PVT.LTD.','F-1,KARTARPURA INDUSTRIAL AREA22,GODWON','JAIPUR','RAJASTHAN','01412211753,2211754','JA156',NULL,NULL,'0','CR','P/JAI/56','0',NULL,NULL,'0',NULL,0),
('P/3/57','GULAB CHAND PRINTS PVT.LTD.','12-14,CITY PULSE MALL,NARAINSINGH CIRCLE,','JAIPUR','RAJASTHAN','2576300','JA157',NULL,NULL,'0','CR','P/JAI/57','0',NULL,NULL,'0',NULL,0),
('P/3/58','MANISH DESIGNER PVT.LTD.','\"AASHIRWAD COMPLEX\"7EMA,SANGANERI GATE','JAIPUR','RAJASTHAN','01413215861,9829061224','JA158','','08AAFCM0647M1Z7','0','CR','','0','',NULL,'0','PURCHASE 5%',1),
('P/3/59','MAHARANI','150,1ST.FL.HALDIYA H.JOHARI BAZAZR','JAIPUR','RAJASTHAN','1412574447,9461054259','JA159','','08AYZPK9058R1ZW','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/3/6','SHRI GOVIND KRIPA','1,KHANDELWAL COMPLEX,DHULA HOUSE BAPU BAZAR','JAIPUR','RAJASTHAN','2566423','JA106',NULL,NULL,'0','CR','P/JAI/6','0',NULL,NULL,'0',NULL,0),
('P/3/60','MAHARANI EXPORTS','1 st FLOOR C-24,OPP.ST.XAVIERSCHOOL,BHAGWEN DAS ROAD','JAIPUR','RAJASTHAN','0091-2369660','JA160',NULL,NULL,'0','CR','P/JAI/60','0',NULL,NULL,'0',NULL,0),
('P/3/61','K.K.SAREE','KHANDA MANDIR BAIJI,MANAK CHOWK','JAIPUR','RAJASTHAN','0141-4014909','JA161',NULL,NULL,'0','CR','P/JAI/61','0',NULL,NULL,'0',NULL,0),
('P/3/62','ROOP NIKHAR FASHION','4279,\"SHIV BHAWAN\"SANJAY BAZARNEAR SANGNERI GATE','JAIPUR','RAJASTHAN','01412577432','JA162',NULL,NULL,'0','CR','P/JAI/62','0',NULL,NULL,'0',NULL,0),
('P/3/63','LUCKY FASHIONS','254,4TH,FLOOR,PUROHIT BHAWANPUROHIT JI KA KATLA,JOHRI BAZA','JAIPUR','RAJASTHAN','9314501065,2571068,257788','JA163',NULL,NULL,'0','CR','P/JAI/63','0',NULL,NULL,'0',NULL,0),
('P/3/64','PRINCE INTERNATIONAL(SAHIBA)','1st & IInd FLOOR,78BAPU BAZR','JAIPUR','RAJASTHAN','2578968,9875298104','JA164',NULL,NULL,'0','CR','P/JAI/64','0',NULL,NULL,'0',NULL,0),
('P/3/65','ROOP DARSH FASHIONS(MANISH)','708-709,ASHIRWAD COMPLEX,SANGANERI GATE','JAIPUR','RAJASTHAN','','JA165',NULL,NULL,'0','CR','P/JAI/65','0',NULL,NULL,'0',NULL,0),
('P/3/66','M.K.TAILORING HOUSE','P.NO.12A,RADHA GOVIND NAGAR,NRPANNA DHAY CIRCLE,SHEOPUR','JAIPUR','RAJASTHAN','','JA166',NULL,NULL,'0','CR','P/JAI/66','0',NULL,NULL,'0',NULL,0),
('P/3/67','GAYATRI RATNA','','JAIPUR','RAJASTHAN','','JA167',NULL,NULL,'0','CR','P/JAI/67','0',NULL,NULL,'0',NULL,0),
('P/3/68','MEGHA FASHION','','JAIPUR','RAJASTHAN','','JA168',NULL,NULL,'0','CR','P/JAI/68','0',NULL,NULL,'0',NULL,0),
('P/3/69','COTTON MUSEUM(RASHTRIYA)','27,DHULA HOUSE ROADBAPU BAZAR','JAIPUR','RAJASTHAN','0141-2560104,9829917828','JA169',NULL,NULL,'0','CR','P/JAI/69','0',NULL,NULL,'0',NULL,0),
('P/3/7','PARIDHAN TEXTILES (SAHIBAA)','78,BAPU BAZAR','JAIPUR','RAJASTHAN','0141-2578968','JA107',NULL,NULL,'0','CR','P/JAI/7','0',NULL,NULL,'0',NULL,0),
('P/3/70','RAMA ENTERPRISES','SHOP.155,1st FLOOR,SARAOGIMANSION M.I.RAOD','JAIPUR','RAJASTHAN','141-2792541,9314078703','JA170',NULL,NULL,'0','CR','P/JAI/70','0',NULL,NULL,'0',NULL,0),
('P/3/71','RANJANA TEXTILE AGENCY','46,KATLA PUROHIT JI KA','JAIPUR','RAJASTHAN','569373,','JA171',NULL,NULL,'0','CR','P/JAI/71','0',NULL,NULL,'0',NULL,0),
('P/3/72','REKHA TEXTILES','MADAN MANSION,BAPU BAZAR','JAIPUR','RAJASTHAN','0141-2578487,9414047112','JA172','','08ABLPR1804P1Z4','0','CR','','0','',NULL,'0','null',1),
('P/3/73','BANDHANI MART','224,2nd FLOOR,SUNNY ARCADE,RAMLALA JI KA RASTA JOHRI BAZAR','JAIPUR','RAJASTHAN','9414362009,9887410001','JA173',NULL,NULL,'0','CR','P/JAI/73','0',NULL,NULL,'0',NULL,0),
('P/3/74','RICH COTTON MUSEUM (RASHTRIYA)','S-53,RADHA MARG,BEHIND RAJ MANDIR CENEMA','JAIPUR','RAJASTHAN','8529538734','JA174','','08AAFCR6285G1Z1','0','CR','','0','9829917828',NULL,'0','PURCHASE O MP',1),
('P/3/75','DUA CREATIONS','1st FLOOR SHOP NO.4,DHULA HOUSE,BAPU NAGAR','JAIPUR','RAJASTHAN','08755348696,09001459999','JA175',NULL,NULL,'0','CR','P/JAI/75','0',NULL,NULL,'0',NULL,0),
('P/3/76','MAHA GANPATI BANDHANI -2','SHOP NO.2 KAMLA MARKET,DHULA HOUSE,BAPU BAZAR','JAIPUR','RAJASTHAN','0141-4014854,9829115413','JA176',NULL,NULL,'0','CR','P/JAI/76','0',NULL,NULL,'0',NULL,0),
('P/3/77','SHREE SHYAM CREATION','F-8-9,IsT FLOR AKSHGANGA COMPXSABJI MANDI ROAD JOHRI BAZAR','JAIPUR','RAJASTHAN','8094043250,9001906511','JA177',NULL,NULL,'0','CR','P/JAI/77','0',NULL,NULL,'0',NULL,0),
('P/3/78','LAXMI EXTENSION','DHULA HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','9460871085','JA178',NULL,NULL,'0','CR','P/JAI/78','0',NULL,NULL,'0',NULL,0),
('P/3/79','REKHA TEXTILES-2','MADAN MANSION,DHULA HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','0141-2578487,9414047112','JA179',NULL,NULL,'0','CR','P/JAI/79','0',NULL,NULL,'0',NULL,0),
('P/3/8','BHAGWATI FASHIONS (RASHTRIYA)','S-53,B RADHA MARG BEA','JAIPUR','RAJASTHAN','0141-2577010','JA108','','08AACCB6305J1ZS','0','CR','','0','9829130929,9829766064',NULL,'0','PURCHASE O MP',1),
('P/3/80','SHREE JANKI CREATIONS PVT.LTD.','3-4,NEAR RATHI TEMPLE DHULAHOUSE,MAIN ROAD BAPU BAZAR','JAIPUR','RAJASTHAN','0141-2578460','JA180',NULL,NULL,'0','CR','P/JAI/80','0',NULL,NULL,'0',NULL,0),
('P/3/81','BHARAT TEXTILES(REKHA TEXTILES','1 sT FLLOR MADNA MANSIONBAPU BAZAR','JAIPUR','RAJASTHAN','0141-2578487,9414047112','JA181',NULL,NULL,'0','CR','P/JAI/81','0',NULL,NULL,'0',NULL,0),
('P/3/82','ARJUNDAS RANCHHORDAS','LALANIYO KA CHOWK DEPOAL KA RASTA JOHRI','JAIPUR','RAJASTHAN','','JA182','','08AAOFA7138H1Z7','0','CR','','0','',NULL,'0','PURCHASE 12%',0),
('P/3/83','M/S MADHURI FAB TEX','102,1st FLOOR VISHNU PRIYA COMPLEX,DHULA HOUSE BAPU BAZAR','JAIPUR','RAJASTHAN','2568637,9950833998','JA184',NULL,NULL,'0','CR','P/JAI/83','0',NULL,NULL,'0',NULL,0),
('P/3/84','RANJANA FASHION','3 1st FLOOR NEAR SITAPURA HOUSHALDIYON KA RASTA JOHRI BAZAR','JAIPUR','RAJASTHAN','0141-2570360,9950985301','JA185',NULL,NULL,'0','CR','P/JAI/84','0',NULL,NULL,'0',NULL,0),
('P/3/85','AARTI COLLECTION','1 sT FLOOR NEAR BACHE BAPU BAZAR','JAIPUR','RAJASTHAN','9214681336','JA186','','08BVEPK0567N1Z6','0','CR','','0','',NULL,'0','null',1),
('P/3/86','PRATHAM  FASHION DESIGNERS PVT.LTD..','S-53,BRADHA MARG,BEHIND RAJ MANDIR CINEMA,M.I. ROAD','JAIPUR','RAJASTHAN','01414916058/59/60','JA187','','08AAJCP1992L1ZS','0','CR','','0','9166464100',NULL,'0','PURCHASE O MP',1),
('P/3/87','ISHWAR DAS NARESH KUMAR','2nd FLOOR,AKASHDEEP-5 BEHINDRAJDEEP HOTAL BAPU BAZAR','JAIPUR','RAJASTHAN','01412578131,9414076379','JA188',NULL,NULL,'0','CR','P/JAI/87','0',NULL,NULL,'0',NULL,0),
('P/3/88','MAHESH TEXTILES','1st FLOOR MADAN MANSIONBAPU BAZAR','JAIPUR','RAJASTHAN','0141-2578437,9414047112','JA189','','08AMCPR4523R1ZG','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/3/89','ARIHANT','344-45 3rd FLOOR SARAOGI MANSION M.I.ROAD','JAIPUR','RAJASTHAN','9414245672','JA190',NULL,NULL,'0','CR','P/JAI/89','0',NULL,NULL,'0',NULL,0),
('P/3/9','S.K.BANDHANI','PUROHITJI KA NAYA KATLA','JAIPUR','RAJASTHAN','2570644','JA109',NULL,NULL,'0','CR','P/JAI/9','0',NULL,NULL,'0',NULL,0),
('P/3/90','GAURAV EMPIRE','','JAIPUR','RAJASTHAN','','JA191',NULL,NULL,'0','CR','P/JAI/90','0',NULL,NULL,'0',NULL,0),
('P/3/91','G & G SAREE EMPORIUM','UG-23,APPER FLOOR NAVJEEVAN PLAZA,SARAOGI MASNSION M.I.RAOD','JAIPUR','RAJASTHAN','8003670001','JA192',NULL,NULL,'0','CR','P/JAI/91','0',NULL,NULL,'0',NULL,0),
('P/3/92','MAAHIRA','ASHIRWAD COMPLEX 708,09 Ist FLNEAR MINARVA CINEMA SANGANERIG','JAIPUR','RAJASTHAN','9829020459','JA193',NULL,NULL,'0','CR','P/JAI/92','0',NULL,NULL,'0',NULL,0),
('P/3/93','LAXMI NX','2385,DHULA HOUSEBAPU BAZAR','JAIPUR','RAJASTHAN','1414011085','','','08ALOPP4933A1Z1','0','CR','','0','9509879093,9460871085','','','PURCHASE O MP',1),
('P/3/94','ORCHID FABRICS','UG-40 NAVJEEVAN PLAZA NEAR SARAOGI MANSION M.I. ROAD','JAIPUR','RAJASTHAN','','','','08AAEF01460C1ZO','0','CR','','0','9928540777','9828828983','','PURCHASE 5%',1),
('P/30/1','SHUBHLAXMI FABRICS','7/185,KAPAD MARKET,DIST-KOLAPUR(MAHARASHTRA)','ICHALKARANJI','MAHARASHTRA','0230-2433385,9422580449','IC001',NULL,NULL,'0','CR','P/ICH/1','0',NULL,NULL,'0',NULL,0),
('P/31/1','PARALIYA SYNTHETICS','104/105,1,ST FLOOR RAI BHADURBAZAR,M.G.H.ROAD,','JODHPUR','RAJASTHAN','','JO101',NULL,NULL,'0','CR','P/JOD/1','0',NULL,NULL,'0',NULL,0),
('P/31/2','VARDHMAN INDUSTRIES','E.222 ROAD NO.62ND PHASE BASNI','JODHPUR','RAJASTHAN','','JO102',NULL,NULL,'0','CR','P/JOD/2','0',NULL,NULL,'0',NULL,0),
('P/31/3','JAGDISH SAREE EMPORIUM','OPP.TELIYO KI MASJID,TRIPOLIYABAZAR','JODHPUR','RAJASTHAN','02912612393,9414134393','JO103',NULL,NULL,'0','CR','P/JOD/3','0',NULL,NULL,'0',NULL,0),
('P/31/4','MOHD.ASLAM & CO.','KHANDHA FALSA,KUMARIYA KHUWASTREET','JODHPUR','RAJASTHAN','0291-2649018/9829547484','JO104',NULL,NULL,'0','CR','P/JOD/4','0',NULL,NULL,'0',NULL,0),
('P/31/5','M.HUZAIFA BROTHERS','KHANDA FALSA CHADWA STREET','JODHPUR','RAJASTHAN','9314284184,9352367395','JO105',NULL,NULL,'0','CR','P/JOD/5','0',NULL,NULL,'0',NULL,0),
('P/31/6','JAGDISH FAB TEX','JAGMOHINI HOUSE,830,10th,CHOPASNI ROAD,NEAR RANVER BHAWAN','JODHPUR','RAJASTHAN','9828336236,9983881834','JO106',NULL,NULL,'0','CR','P/JOD/6','0',NULL,NULL,'0',NULL,0),
('P/31/7','NAHEED FABS','KHANDA FALSA,CHADWA STREET,','JODHPUR','RAJASTHAN','0291-2626457,9829025786','JO107',NULL,NULL,'0','CR','P/JOD/7','0',NULL,NULL,'0',NULL,0),
('P/4/1','SHREE BALAJI SAREES (P)LTD','145-B,COTTON STREET,','KOLKATA','WEST BENGAL','22686631','KO101',NULL,NULL,'0','CR','P/KOL/1','0',NULL,NULL,'0',NULL,0),
('P/4/10','NEW SHAREE KUTHIR','203/1,MAHATMA GANDHI ROAD,(PORAKKOTHI),1sT FLOOR,ROOM191','KOLKATA','WEST BENGAL','22707247,9232471154','KO110',NULL,NULL,'0','CR','P/KOL/10','0',NULL,NULL,'0',NULL,0),
('P/4/11','J.C.BASAK &CO.','46/1A,STRAND ROAD,','KOLKATA','WEST BENGAL','','KO111',NULL,NULL,'0','CR','P/KOL/11','0',NULL,NULL,'0',NULL,0),
('P/4/12','ARCHIES CREATION','P.S.ARCADE,11,SUDDERM NO.T-29','KOLKATA','WEST BENGAL','329434479830168556','KO112','','19ABAFA2122L1ZP','0','CR','','0','',NULL,'0','PURCHASE 12%',0),
('P/4/13','SPARSH','26A,PARK LANE 1st FLOOR','KOLKATA','WEST BENGAL','','KO113',NULL,NULL,'0','CR','P/KOL/13','0',NULL,NULL,'0',NULL,0),
('P/4/14','HITESHI CREATION PVT.LTD.','26A,PARK LANE,3rd FLOOR','KOLKATA','WEST BENGAL','033-22275254','KO114',NULL,NULL,'0','CR','P/KOL/14','0',NULL,NULL,'0',NULL,0),
('P/4/15','GARIMA FASHIONS','','KOLKATA','WEST BENGAL','','KO115',NULL,NULL,'0','CR','P/KOL/15','0',NULL,NULL,'0',NULL,0),
('P/4/16','SPARSH SAREE','5B,RAWDON STREET,2nd FLOOR','KOLKATA','WEST BENGAL','033-44064986','KO116',NULL,NULL,'0','CR','P/KOL/16','0',NULL,NULL,'0',NULL,0),
('P/4/17','SUJATA SAREES PVT.LTD.','103,PARK STREET,\"RAVI AUTOHOUSE\",2nd FLOOR','KOLKATA','WEST BENGAL','2227-7797,22277798','KO117',NULL,NULL,'0','CR','P/KOL/17','0',NULL,NULL,'0',NULL,0),
('P/4/18','GOLCHHA SAREES','107/1,PARK STREET 5 TH FLOORNEAR KHOINOOR BUILDING','KOLKATA','WEST BENGAL','65001474','KO118',NULL,NULL,'0','CR','P/KOL/18','0',NULL,NULL,'0',NULL,0),
('P/4/19','D.P.INDUS.','','KOLKATA','WEST BENGAL','','KO119',NULL,NULL,'0','CR','P/KOL/19','0',NULL,NULL,'0',NULL,0),
('P/4/2','SHREE MARUDHAR KESHARI TEXTILE','SHREE ARCADE,2,JOGENDARAKAVIRAJ ROW,1,stFLOOR','KOLKATA','WEST BENGAL','22598504','KO102',NULL,NULL,'0','CR','P/KOL/2','0',NULL,NULL,'0',NULL,0),
('P/4/20','SAROJ CREATION','P.S.ARCADE ,11 SUDDER STREETS.NO.T-37 3 rd FLOOR','KOLKATA','WEST BENGAL','9331000720','KO120',NULL,NULL,'0','CR','P/KOL/20','0',NULL,NULL,'0',NULL,0),
('P/4/21','MEENAL SHAH','24A,RABINDRA SARANI OPP.MAYURCINMA MADANMANSION3rD FLR R.98','KOLKATA','WEST BENGAL','9830973592,9331906055','KO121',NULL,NULL,'0','CR','P/KOL/21','0',NULL,NULL,'0',NULL,0),
('P/4/22','KHETAN FASHIONS','95A PARK STREETBASEMENT','KOLKATA','WEST BENGAL','033-30235640,9831036011','KO122','','19ANUPK6720E1ZO','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/4/23','SHREE GOVIND STORES','13/A,SHIV THAKUR LANE3rd FLOOR','KOLKATA','WEST BENGAL','22744763,32015390','KO123',NULL,NULL,'0','CR','P/KOL/23','0',NULL,NULL,'0',NULL,0),
('P/4/24','RADHA KRISHNA CREATION','19 NO.BYSACK STREET5th FLOOR','KOLKATA','WEST BENGAL','65254470','KO124','','19ADKPV0513C1ZP','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/4/25','RAJSHREE CREATION PVT.LTD.','65,SIR HARIRAM GOENKA STREET2nd FLOOR BANGUR ARCADE','KOLKATA','WEST BENGAL','033-22745247,8420556555','KO125',NULL,NULL,'0','CR','P/KOL/25','0',NULL,NULL,'0',NULL,0),
('P/4/26','RUPANJALI','22,BURTALLA STREET,2nd FLOORROOM NO.36','KOLKATA','WEST BENGAL','22735517,9830559417','KO126',NULL,NULL,'0','CR','P/KOL/26','0',NULL,NULL,'0',NULL,0),
('P/4/27','RANGELLII FASHION','3 WOOD STREET,NR-VARDHAN MARKET,','KOLKATA','WEST BENGAL','9831344277','KO127',NULL,NULL,'0','CR','P/KOL/27','0',NULL,NULL,'0',NULL,0),
('P/4/28','FASHION INSIGHT','DIAMOND CHABERS,4 CHOWRINGHEELANE,BLOCK3,3rdFLOR ROOM NO.3F','KOLKATA','WEST BENGAL','03332015455,9831022565','KO128',NULL,NULL,'0','CR','P/KOL/28','0',NULL,NULL,'0',NULL,0),
('P/4/29','SHREE SHIV STORES','24,KALAKAR STREET(KALKAR MRKETGROUND FLOOR','KOLKATA','WEST BENGAL','9883270314,9831100143','KO129',NULL,NULL,'0','CR','P/KOL/29','0',NULL,NULL,'0',NULL,0),
('P/4/3','RANG REZ FASHIONS','BALAJI TOWER,135,COTTON STREET4,thFLOOR,','KOLKATA','WEST BENGAL','22681438','KO103',NULL,NULL,'0','CR','P/KOL/3','0',NULL,NULL,'0',NULL,0),
('P/4/30','ANUPAM SAREE PVT.LTD.','6,SHIB THAKUR LANE 4th FLOOR(DHAKA PATTI)','KOLKATA','WEST BENGAL','033-22744173,8697223611','KO130',NULL,NULL,'0','CR','P/KOL/30','0',NULL,NULL,'0',NULL,0),
('P/4/31','K.K. FASHIONS','7,GANPAT BAGLA ROAD,2ndFLOR ROM.NO.203','KOLKATA','WEST BENGAL','9830529934','KO131','','19AFUPK6072K1ZN','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/4/32','BAJRANG SONTHALIA(AGENT)','107/1,PARK STRET,3rD FLOOR','KOLKATA','WEST BENGAL','93318769543,9330011222','KO132',NULL,NULL,'0','CR','P/KOL/32','0',NULL,NULL,'0',NULL,0),
('P/4/33','R.K.STUDIO','P-1,KALAKAR STREET 1sT FLOOR','KOLKATA','WEST BENGAL','','KO133',NULL,NULL,'0','CR','P/KOL/33','0',NULL,NULL,'0',NULL,0),
('P/4/34','VR2','13,NARAYAN PRASAD BABU LANE2nd','KOLKATA','WEST BENGAL','9831582244','KO134',NULL,NULL,'0','CR','P/KOL/34','0',NULL,NULL,'0',NULL,0),
('P/4/35','RICHES','571,N-BLOCKNEW ALIPORE','KOLKATA','WEST BENGAL','9903010384','KO135','','URD','0','CR','','0','',NULL,'0','PURCHASE URD',1),
('P/4/36','AKSHARA','58,SIR HARIRAM GOENKA STREETGROUND FOOR','KOLKATA','WEST BENGAL','9830134447,9831273909','KO136','','19ABFFA9510P1ZY','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/4/37','RANGVARSHA','47,SIR HARI RAM GOENKA STREET2nd FLOR','KOLKATA','WEST BENGAL','8759104742','KO137',NULL,NULL,'0','CR','P/KOL/37','0',NULL,NULL,'0',NULL,0),
('P/4/38','SHUBHAM TEXTILES','255,RABINDRA SARANI,BAJORIA KATRA,1st FLOR,ROOM NO.4','KOLKATA','WEST BENGAL','9330987272','KO138',NULL,NULL,'0','CR','P/KOL/38','0',NULL,NULL,'0',NULL,0),
('P/4/39','AKSHI','','KOLKATA','WEST BENGAL','','KO139',NULL,NULL,'0','CR','P/KOL/39','0',NULL,NULL,'0',NULL,0),
('P/4/4','DAGA BANDHU','2/1B,CHAITANYA SETT STREET,1stFLOOR','KOLKATA','WEST BENGAL','22681390','KO104',NULL,NULL,'0','CR','P/KOL/4','0',NULL,NULL,'0',NULL,0),
('P/4/40','SRINGAR CREATION','6 th FLOR RAMPURIA KATRA,147COTTON STREET BURRA BAZAR','KOLKATA','WEST BENGAL','8879709331,9748725549','KO140',NULL,NULL,'0','CR','P/KOL/40','0',NULL,NULL,'0',NULL,0),
('P/4/41','MANBHARI','P-3 MADAN CHATERJEE LANE','KOLKATA','WEST BENGAL','8961415249,8981042358','KO141','','19BNYPS0434A1ZS','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/4/42','PRABHA FASHIONS PVT.LTD.','P-6,KALAKAR STREET 1st FLOR','KOLKATA','WEST BENGAL','9038110110','KO142','','19AAGCP2757G1Z6','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/4/43','SAMMANITA SATTVA LLP','SIDDHA POINT,5th FLOR,IOI PARKSTREET','KOLKATA','WEST BENGAL','334006,','KO143','','19ADCFS9215N1ZH','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/4/44','SHIVANGI ARTS','65C,SHAKESPEARSARANI','KOLKATA','WEST BENGAL','8442839960','KO144',NULL,NULL,'0','CR','P/KOL/44','0',NULL,NULL,'0',NULL,0),
('P/4/45','JYOTI KHAITAN','P-84,LAKE TOWN BLOCK-B 3rD FLRLANE BESIDE GANGURAM SWEETS','KOLKATA','WEST BENGAL','3251-6623,9331101164','KO145',NULL,NULL,'0','CR','P/KOL/45','0',NULL,NULL,'0',NULL,0),
('P/4/46','VASUDEV FABRICS','','KOLKATA','WEST BENGAL','','KO146',NULL,NULL,'0','CR','P/KOL/46','0',NULL,NULL,'0',NULL,0),
('P/4/47','AP FOR WOMEN','48A,PARK STREET 2nd FLOOR','KOLKATA','WEST BENGAL','9836910267,9830414710','KO147',NULL,NULL,'0','CR','P/KOL/47','0',NULL,NULL,'0',NULL,0),
('P/4/48','SHRI SAA STUDIO LLP','P.S.ARCADE ROOM NO.T-392,SUDDER STREET','KOLKATA','WEST BENGAL','9830525774','KO148',NULL,NULL,'0','CR','P/KOL/48','0',NULL,NULL,'0',NULL,0),
('P/4/49','NARI','42A RAFI AHAMED KIDWAI ROAD6A,6th FOLLR','KOLKATA','WEST BENGAL','033-2229 0202,46012292','','','19AANFN9803H1ZS','0','CR','','0','','','','PURCHASE O MP',1),
('P/4/5','PRIYANSHI SAREES PVT.LTD.','2,JOGENDARA KAVIRAJ ROW,SHREE ARCADE 2,nd FLOOR','KOLKATA','WEST BENGAL','22597950','KO105',NULL,NULL,'0','CR','P/KOL/5','0',NULL,NULL,'0',NULL,0),
('P/4/50','GORAKSH CREATION','6,SHIV THAKUR LANE 3rd FLOOR ','KOLKATA','WEST BENGAL','','','','19BPPPS4977F1ZZ','0','CR','','0','9748461126','','','PURCHASE O MP',1),
('P/4/51','FASHIONOVA','36,KALI KRISHNA TAGORE STREET ,4th FLORABOVE ALLHABAD BANK NERA MALAPADAD CROSSING','KOLKATA','WEST BENGAL','','','','19BVSPM6054J1ZT','0','CR','','0','','','','PURCHASE O MP',1),
('P/4/52','KOTHARI LIFE STYLE','','KOLKATA','WEST BENGAL','','','','19AAOFK2935Q1ZG','0','CR','','0','','','','PURCHASE O MP',1),
('P/4/53','NAV DURGA','36, KALI KRISHNA TAGORE STREET3 rd FLOOR,MALAPURA CROSSING','KOLKATA','WEST BENGAL','033-22590328','','','19AALFN1724B1ZI','0','CR','','0','','','','PURCHASE O MP',1),
('P/4/54','KHUSH APPAREL WORLD PVT.LTD.','96D SATIN SEN SARANI ','KOLKATA','WEST BENGAL','03323359583','','','19AAFCM4281M1ZY','0','CR','','0','9830941026','','','PURCHASE 12%',1),
('P/4/6','SATKAAR','113,PARK STREET,(PODDAR POINT)2,ND,FLOOR,','KOLKATA','WEST BENGAL','22498248','KO106',NULL,NULL,'0','CR','P/KOL/6','0',NULL,NULL,'0',NULL,0),
('P/4/7','JHALAK CREATION','4A,CHAITAN SETH STREET,SIKARIATOWERS,UTI BANK BUILDING','KOLKATA','WEST BENGAL','9830899428','KO107',NULL,NULL,'0','CR','P/KOL/7','0',NULL,NULL,'0',NULL,0),
('P/4/8','S.S.SALWAR SUITS PVT.LTD.','19,COLOOTOLA STREET, 1st FLOOR','KOLKATA','WEST BENGAL','22257012','KO108',NULL,NULL,'0','CR','P/KOL/8','0',NULL,NULL,'0',NULL,0),
('P/4/9','MANDAKANI SAREES','137,COTTON STREET','KOLKATA','WEST BENGAL','22730943','KO109','','19ACIPJ0445J1ZJ','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/5/10','K.OMPRAKASH&CO.','1374/8,KATRA LEHASWAN MAHLAXMIMKT.CHANDANI CHOWK','DELHI','DEHLI','23980349','DE105',NULL,NULL,'0','CR','P/DEL/10','0',NULL,NULL,'0',NULL,0),
('P/5/100','TEJOO FASHIONS PVT.LTD.','713.STATION ROAD,NEAI CHOWK','DELHI','DELHI','011-41300000','','','07AADCTOO28P1Z7','0','CR','','0','',NULL,'0','PURCHASE 12%',1),
('P/5/101','MANYA COLLECTION','IX/6490,NEHRU GALIGANDHI NAGAR','DELHI','DEHLI','9911953905,7838409037','DE203',NULL,NULL,'0','CR','P/DEL/101','0',NULL,NULL,'0',NULL,0),
('P/5/102','TOPUP TRADING CO.(GRADUTE)','1016,SUBHASH ROAD GANDHI NAGAR','DELHI','DEHLI','','DE204',NULL,NULL,'0','CR','P/DEL/102','0',NULL,NULL,'0',NULL,0),
('P/5/103','GAUTAM TRADING CO.','X/1108,GALI NO.3,TAGORE GALIGANDHI NAGAR','DELHI','DEHLI','39454022','DE205',NULL,NULL,'0','CR','P/DEL/103','0',NULL,NULL,'0',NULL,0),
('P/5/104','SURAJ SELECTION(SETHI JEE)','X/546,GALI NO.7,RA NAGARGANDHI NAGAR','DELHI','DEHLI','','DE206',NULL,NULL,'0','CR','P/DEL/104','0',NULL,NULL,'0',NULL,0),
('P/5/105','P.M.P CREATION(MANYA)','IX/4541,GALI NO.10,RAM NAGARGANDHI NAGAR','DELHI','DEHLI','','DE207',NULL,NULL,'0','CR','P/DEL/105','0',NULL,NULL,'0',NULL,0),
('P/5/106','B.P. TARDING CO,','3282,SUBBHASH ROADGANDHI NAGAR','DELHI','DEHLI','','DE208',NULL,NULL,'0','CR','P/DEL/106','0',NULL,NULL,'0',NULL,0),
('P/5/107','K.D.S. TRADING CO.','9 GANDHI NAGAR','DELHI','DEHLI','','DE209',NULL,NULL,'0','CR','P/DEL/107','0',NULL,NULL,'0',NULL,0),
('P/5/108','SUMBHAV CREATION','584/5,G/F GALI GHANTESHAWARKATRA NEEL,CHANDNI CHOUWK','DELHI','DEHLI','011-23965211,9818518868','DE210',NULL,NULL,'0','CR','P/DEL/108','0',NULL,NULL,'0',NULL,0),
('P/5/109','THE BETTER CHOICE         LGP','17-A RING ROAD LALPAT NAGAR','DELHI','DEHLI','','DE211',NULL,NULL,'0','CR','P/DEL/109','0',NULL,NULL,'0',NULL,0),
('P/5/11','SANJAY SALES CORPORATION','1374,1,stFLOOR,KATRA LEHASWANMAHALAXMI MKT.CH. CHOWK','DELHI','DEHLI','23970068','DE106',NULL,NULL,'0','CR','P/DEL/11','0',NULL,NULL,'0',NULL,0),
('P/5/110','SMART BEAUTY & HEALTH CARE PVT','C-57 SHOP NO.2& C-63 OLD DOUBLSTORY LAJPAT NAGAR-IV','DELHI','DEHLI','011-41771232','DE212',NULL,NULL,'0','CR','P/DEL/110','0',NULL,NULL,'0',NULL,0),
('P/5/111','KETAN FABRICS','SANJEEV AGENCY','DELHI','DEHLI','','DE213',NULL,NULL,'0','CR','P/DEL/111','0',NULL,NULL,'0',NULL,0),
('P/5/112','WELCOME FABRICS PVT.LTD.AISHI','','DELHI','DEHLI','','DE214',NULL,NULL,'0','CR','P/DEL/112','0',NULL,NULL,'0',NULL,0),
('P/5/113','DIVYA LADIES SUITS','SANGEEV AGENCY','DELHI','DEHLI','','DE215',NULL,NULL,'0','CR','P/DEL/113','0',NULL,NULL,'0',NULL,0),
('P/5/114','SANJEEV AGENCY','4291/5,2nd FLOOR BRIJ MARKETJOGIWARA,NAI SARAK','DELHI','DEHLI','9311102171,9871102171','DE216',NULL,NULL,'0','CR','P/DEL/114','0',NULL,NULL,'0',NULL,0),
('P/5/115','VARDHMAN CLOTHSTORE PVT.LTD.','4015,MAIN ROIAD SHANTI MOHALLAOPP.JAIN MRKET GANDHI NAGAR','DELHI','DEHLI','1','DE217',NULL,NULL,'0','CR','P/DEL/115','0',NULL,NULL,'0',NULL,0),
('P/5/116','R J ENTERPRISES IND.PVT.','5514,1st FLOOR KATRANI CHOWK','DELHI','GUJARAT','011-23954108','DE218','','07AAHCR7383M1ZN','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/5/117','RISHABH COLLECTION','1540,NEHRU GALIGANDHI NAGAR','DELHI','DEHLI','','DE219',NULL,NULL,'0','CR','P/DEL/117','0',NULL,NULL,'0',NULL,0),
('P/5/118','MADHAVI CREATION','2565,NEHRU GALIGANDHJI NAGAR','DELHI','DEHLI','','DE222',NULL,NULL,'0','CR','P/DEL/118','0',NULL,NULL,'0',NULL,0),
('P/5/119','MAHAVEER COLLECTION','1250 NEHRU NAGAR','DELHI','DEHLI','','DE223',NULL,NULL,'0','CR','P/DEL/119','0',NULL,NULL,'0',NULL,0),
('P/5/12','SHYAMLAL & COMPANY','1397,KATRA NAGINCHANH. CHOWK','DELHI','GUJARAT','9999751075,9899650757','DE107','','','0','CR','','0','',NULL,'0','PURCHASE 5%',1),
('P/5/120','SHREE SAI CREATION','650NEHRU GALI','DELHI','DEHLI','','DE224',NULL,NULL,'0','CR','P/DEL/120','0',NULL,NULL,'0',NULL,0),
('P/5/121','ARCHID TRADING CO.','KATRA LEHSWAN CORNER MAI','DELHI','DEHLI','','DE136',NULL,NULL,'0','CR','P/DEL/121','0',NULL,NULL,'0',NULL,0),
('P/5/123','SSUNDRI COLLECTION','35/9,STREET NO.11,TUGLAKABAD EXTENSION','DELHI','DELHI','011-29993268','DE185',NULL,NULL,'0','CR','P/NDEL/1','0',NULL,NULL,'0',NULL,0),
('P/5/124','OPENING COTTSWOOL','','DELHI','UTTAR PRADESH','','','','','0','CR','','0','','','','PURCHASE O MP',1),
('P/5/125','GRADUTE MENS WEAR','9/6650,NEHRU GALI,GANDHI NAGAR','DELHI','DELHI','011-22083898','','','07AMJPK6473P1ZW','0','CR','','0','9891447601','','','PURCHASE O MP',1),
('P/5/126','LAKHMI CHAND TEJOO MAL','692,BAGH DEEWAR STATION ROADCHANDNI CHOWK','DELHI','UTTAR PRADESH','41300000','','','07AAAFL3353P2Z1','0','CR','','0','','','','PURCHASE 5%',1),
('P/5/127','TANYA CREATION','10/124,GEETA COLONY,OPP.GANDHI NAGARPO;ICE STATION ','DELHI','UTTAR PRADESH','011-22073068','','','07AAOPG4616Q2Z5','0','CR','','0','','','','PURCHASE 12%',1),
('P/5/128','CRYSTAL DRESSES','9/6129 GURU HARIKISHAN GALIGANDHI NAGAR','DELHI','UTTAR PRADESH','011-22074166','','','07ALZPD2820M1ZP','0','CR','','0','','','','PURCHASE 12%',1),
('P/5/13','POONAM ARTS','650/1,GALLIGHANTESHWAR,KATRANEEL, CHANDANI CHOWK','DELHI','DEHLI','23936685','DE108',NULL,NULL,'0','CR','P/DEL/13','0',NULL,NULL,'0',NULL,0),
('P/5/14','SHREEJEE CREATION','826/A,KATRA NEELCHANDANI CHOWK','DELHI','DEHLI','23987942,','DE109',NULL,NULL,'0','CR','P/DEL/14','0',NULL,NULL,'0',NULL,0),
('P/5/15','GURUNANAK EMPORIUM','600,GALIGHANTEHWAR,KATRA NEELCHANDANI CHOWK','DELHI','DEHLI','23988521','DE110',NULL,NULL,'0','CR','P/DEL/15','0',NULL,NULL,'0',NULL,0),
('P/5/16','R.K.& SONS','585,1,st FLOOR,GALI GHANTESHWAKATRA NEEL,CH.CHOWK','DELHI','DEHLI','23982953','DE111',NULL,NULL,'0','CR','P/DEL/16','0',NULL,NULL,'0',NULL,0),
('P/5/17','AAR ESS FABRICS PVT.LTD.','612-614,GALI GHANTESHARKATRA NEEL,CH. CHOWK','DELHI','DEHLI','23922118','DE112',NULL,NULL,'0','CR','P/DEL/17','0',NULL,NULL,'0',NULL,0),
('P/5/18','RADHESHWARI SUITS','816,SANGAM MARKET,KATRA NEELCHANDANI CHOWK','DELHI','DEHLI','9312284344','DE113',NULL,NULL,'0','CR','P/DEL/18','0',NULL,NULL,'0',NULL,0),
('P/5/19','SREE CHAITANYA TEXTILES','54,KRISHNA MARKET,CHANDANI CHOWK','DELHI','DEHLI','23978524','DE114',NULL,NULL,'0','CR','P/DEL/19','0',NULL,NULL,'0',NULL,0),
('P/5/20','P.K.TEXTILES','819/9,SANGAM MARKETKATRA NEEL','DELHI','DELHI','23924251','DE115','','','0','CR','','0','',NULL,'0','null',1),
('P/5/21','MURTY TEXTILES','495-501,KRISHNA GALI,KATRA NEEL,CHANDANI CHOWK,','DELHI','DEHLI','23975773','DE116',NULL,NULL,'0','CR','P/DEL/21','0',NULL,NULL,'0',NULL,0),
('P/5/22','INDRA FASHION','808-11,KATRA NEELCHANDANI CHOWK','DELHI','DEHLI','2394787','DE117',NULL,NULL,'0','CR','P/DEL/22','0',NULL,NULL,'0',NULL,0),
('P/5/23','R.K.TEXTILES','815/12,SANGAM MARKETKATRA NEEL','DELHI','DEHLI','23994153','DE118',NULL,NULL,'0','CR','P/DEL/23','0',NULL,NULL,'0',NULL,0),
('P/5/24','POOJA FABRICS','599/02,GALI GANTESHWAR,KATRA NEEL,CHANDANI CHOWK','DELHI','DEHLI','23937469','DE119',NULL,NULL,'0','CR','P/DEL/24','0',NULL,NULL,'0',NULL,0),
('P/5/25','SHREE GANGA FASHIONS','815/8,SANGAM MARKET,KATRA NEEL','DELHI','DEHLI','9873453694','DE120',NULL,NULL,'0','CR','P/DEL/25','0',NULL,NULL,'0',NULL,0),
('P/5/26','NEELKANTH CREATIONS','650/1,GALI GHANTESHWAR,KATRA NEEL,','DELHI','DEHLI','9818084233','DE121',NULL,NULL,'0','CR','P/DEL/26','0',NULL,NULL,'0',NULL,0),
('P/5/27','RISHABH TEXTILE','1397,KATRA NAGIN CHANDCHANDANI CHOWK','DELHI','DEHLI','9899555707','DE122',NULL,NULL,'0','CR','P/DEL/27','0',NULL,NULL,'0',NULL,0),
('P/5/28','FAQUIR CHAND TIRLOK CHAND JAIN','1590,OLD MARWARI KATRA,NAI SARAK,','DELHI','DEHLI','23261750','DE123',NULL,NULL,'0','CR','P/DEL/28','0',NULL,NULL,'0',NULL,0),
('P/5/29','SAPNA CREATION','666/4,GALI GHANTESHWAR,KATRANEEL,CHANDANI CHOWK','DELHI','DEHLI','23964447','DE124',NULL,NULL,'0','CR','P/DEL/29','0',NULL,NULL,'0',NULL,0),
('P/5/30','SAHIDA FASHION','901/2,KUCHA KABIL ATTAR,CHANDANI CHOWK,','DELHI','DEHLI','9810929248','DE125',NULL,NULL,'0','CR','P/DEL/30','0',NULL,NULL,'0',NULL,0),
('P/5/31','MUSKAN SILK EMPORIUM','GHANTASHWAR GALI CORNER560,KATRA NEEL','DELHI','DEHLI','9818025630','DE126',NULL,NULL,'0','CR','P/DEL/31','0',NULL,NULL,'0',NULL,0),
('P/5/32','Z.CREATION','599/1,GALI GHANTESHWAR,KATRA NEEL CH.CHOWK','DELHI','DEHLI','','DE127',NULL,NULL,'0','CR','P/DEL/32','0',NULL,NULL,'0',NULL,0),
('P/5/33','LAKASHMI FASHION','586,GALI GANTESHWAR,KATRA NEEL','DELHI','DEHLI','','DE128',NULL,NULL,'0','CR','P/DEL/33','0',NULL,NULL,'0',NULL,0),
('P/5/34','BHATIA COLLECTION','814/1,KUCHA KABIL ATTARCH.CHOWK','DELHI','DEHLI','','DE129',NULL,NULL,'0','CR','P/DEL/34','0',NULL,NULL,'0',NULL,0),
('P/5/35','SUREKA COLLECTION','584,2nd FLOOR GALI GHANTESHWARKATRA NEEL CHADNI CHOWK','DELHI','DEHLI','','DE130',NULL,NULL,'0','CR','P/DEL/35','0',NULL,NULL,'0',NULL,0),
('P/5/36','HI-FI FASHION','827/17,KATRA NEEL','DELHI','DEHLI','','DE131',NULL,NULL,'0','CR','P/DEL/36','0',NULL,NULL,'0',NULL,0),
('P/5/37','J.H.TEXTILE','768,KATRA NEEL','DELHI','DEHLI','','DE132',NULL,NULL,'0','CR','P/DEL/37','0',NULL,NULL,'0',NULL,0),
('P/5/38','MAHAVIR TEXTILE','688/2,KATRA NEEL','DELHI','DEHLI','','DE133',NULL,NULL,'0','CR','P/DEL/38','0',NULL,NULL,'0',NULL,0),
('P/5/39','HONEY FASHION','1397,KATRA NAGINCHANDMETROWALA','DELHI','DEHLI','','DE134',NULL,NULL,'0','CR','P/DEL/39','0',NULL,NULL,'0',NULL,0),
('P/5/40','ANKUR TEXTILES','KATRA JHAJHAR,NR.GOYAL{MICKY}','DELHI','DEHLI','','DE135',NULL,NULL,'0','CR','P/DEL/40','0',NULL,NULL,'0',NULL,0),
('P/5/41','A.P.TEXTILES','KATRA LEHSWANCORNER MAI SARDAR','DELHI','DEHLI','','DE137',NULL,NULL,'0','CR','P/DEL/41','0',NULL,NULL,'0',NULL,0),
('P/5/42','RAJENDRAKUMAR ANSHULKUMAR JAIN','MHALAXMI MARKET,KATRA LEHSWANUPSTAIRS,NR.K.OMPRAKASH','DELHI','DEHLI','','DE138',NULL,NULL,'0','CR','P/DEL/42','0',NULL,NULL,'0',NULL,0),
('P/5/43','PARMESHWARIDAS PUNAMCHANDGUPTA','1376-C,KATRA LESHWAN,CHANDNI CHOWK','DELHI','DEHLI','09818980194-RAJIV J GUPTA','DE139',NULL,NULL,'0','CR','P/DEL/43','0',NULL,NULL,'0',NULL,0),
('P/5/44','GURU KIRPA TEXTILES','895/11,KUCHA KABIL ATTARCHANDANI CHOWK','DELHI','DEHLI','','DE140',NULL,NULL,'0','CR','P/DEL/44','0',NULL,NULL,'0',NULL,0),
('P/5/45','AAKRITI','816/2,SANGAM MARKET,KATRA NEEL','DELHI','DEHLI','','DE141',NULL,NULL,'0','CR','P/DEL/45','0',NULL,NULL,'0',NULL,0),
('P/5/46','DHRUV TEXTILES','816/6,SANGAM MARKETKATRA NEEL','DELHI','DEHLI','','DE142',NULL,NULL,'0','CR','P/DEL/46','0',NULL,NULL,'0',NULL,0),
('P/5/47','SANT TEXTILE','867/5,KUCHAKABIL ATTAR,CHANDNI CHOWK','DELHI','DEHLI','','DE143',NULL,NULL,'0','CR','P/DEL/47','0',NULL,NULL,'0',NULL,0),
('P/5/48','M.K.TRADING CO.','758,KATRA NEELCH,CHOWK','DELHI','DEHLI','','DE144',NULL,NULL,'0','CR','P/DEL/48','0',NULL,NULL,'0',NULL,0),
('P/5/49','VAIBHAVCOLLECTION','20-21,SANGAM CLOTH MARKET1,ST FLOOR 816,KATRANEEL','DELHI','DEHLI','','DE145',NULL,NULL,'0','CR','P/DEL/49','0',NULL,NULL,'0',NULL,0),
('P/5/50','PREETI SELECTION','500,KRISHNA GALI,KATRA NEEL','DELHI','DEHLI','9971508101,23964447','DE146',NULL,NULL,'0','CR','P/DEL/50','0',NULL,NULL,'0',NULL,0),
('P/5/51','S.D.TEXTILES','16/9,2,ND FL.SANGAM MARKETATRA NEEL','DELHI','DEHLI','','DE147',NULL,NULL,'0','CR','P/DEL/51','0',NULL,NULL,'0',NULL,0),
('P/5/52','HIRDAYA NARAIN SAROOP NARAIN','1651,LD MARWARI KATRANAISARAK','DELHI','DEHLI','','DE148',NULL,NULL,'0','CR','P/DEL/52','0',NULL,NULL,'0',NULL,0),
('P/5/53','AHUJA FASHION SAREES','4280,JOGIWARA,BHAIRON WALI GALLI,SHIV MARKET','DELHI','DEHLI','','DE149',NULL,NULL,'0','CR','P/DEL/53','0',NULL,NULL,'0',NULL,0),
('P/5/54','GAURAV SAREES CENTRE','4226/4,NANAK MARKET,JOGIWARANAI SARAK','DELHI','DEHLI','','DE150',NULL,NULL,'0','CR','P/DEL/54','0',NULL,NULL,'0',NULL,0),
('P/5/55','SANJAY TEXTILE','GF-816/11A,SANGAM MARKETKATRA NEEL','DELHI','DEHLI','','DE151',NULL,NULL,'0','CR','P/DEL/55','0',NULL,NULL,'0',NULL,0),
('P/5/56','UTKARSH SAREE','5850/2,JOGIWARANAI SARAK','DELHI','DEHLI','','DE152',NULL,NULL,'0','CR','P/DEL/56','0',NULL,NULL,'0',NULL,0),
('P/5/57','SUMIT SAREE CENTRE','4206,JOGIWARANAI SARAK','DELHI','DEHLI','','DE153',NULL,NULL,'0','CR','P/DEL/57','0',NULL,NULL,'0',NULL,0),
('P/5/58','AASHISH COLLECTIONS','KATRA NEEL,','DELHI','DEHLI','9811125933','DE154',NULL,NULL,'0','CR','P/DEL/58','0',NULL,NULL,'0',NULL,0),
('P/5/59','BHAGMAL SATYAPAL','409,KATRA CHOBAN,CH.CH.','DELHI','DELHI','9818568353','DE155','','07AAFPM9430P1ZS','0','CR','','0','9873336213',NULL,'0','PURCHASE 5%',1),
('P/5/6','KAMTAPRASAD PRABHAT KR.JAIN','1219,1,st FLOOR,NO.1YAN,CHANDANI CHOK','DELHI','GUJARAT','23922720,9811011483','DE101','','07AAHP3347G1ZN','0','CR','','0','',NULL,'0','PURCHASE 5%',1),
('P/5/60','ARORA BROS.','NR.POOJA,GALI GNTESHWARKATRANEEL','DELHI','DEHLI','','DE156',NULL,NULL,'0','CR','P/DEL/60','0',NULL,NULL,'0',NULL,0),
('P/5/61','GOGIA TEXTILES','GALIGANTESHWAR,KATRA NEELOPP.POOJA','DELHI','DEHLI','','DE157',NULL,NULL,'0','CR','P/DEL/61','0',NULL,NULL,'0',NULL,0),
('P/5/62','SHOURYA SIDDHII CREATIONS','KAMTA PRASAD','DELHI','DEHLI','','DE158',NULL,NULL,'0','CR','P/DEL/62','0',NULL,NULL,'0',NULL,0),
('P/5/63','PARAS FABRICS PVT.LTD.','1360/3,KATRA LEHSWANCHANDANI CHOWK','DELHI','DEHLI','','DE159',NULL,NULL,'0','CR','P/DEL/63','0',NULL,NULL,'0',NULL,0),
('P/5/64','RINKU & COMPANY','SHYAMLAL &CO.','DELHI','DEHLI','','DE160',NULL,NULL,'0','CR','P/DEL/64','0',NULL,NULL,'0',NULL,0),
('P/5/65','RAMESH CHANDRA ANKUR KUMARJAIN','1408/1 KATRA ZHANZHARNR. MICKY','DELHI','DEHLI','','DE161',NULL,NULL,'0','CR','P/DEL/65','0',NULL,NULL,'0',NULL,0),
('P/5/66','CHARAN JIT SINGH & CO.','815/10,SANGAM MARKET,KATRA NEEL','DELHI','DEHLI','','DE162',NULL,NULL,'0','CR','P/DEL/66','0',NULL,NULL,'0',NULL,0),
('P/5/67','SHREE RADHEY PRINTS','650/11,GALI GHANTESHWAR,KATRANEEL,CHANDNI CHOWK,','DELHI','DEHLI','011-20219257','DE163',NULL,NULL,'0','CR','P/DEL/67','0',NULL,NULL,'0',NULL,0),
('P/5/68','MANGALDEEP CREATION','611,GALI GHANTESHWAR,KATRANEEL,CHANDNI CHOWK,','DELHI','DEHLI','9910270290','DE164',NULL,NULL,'0','CR','P/DEL/68','0',NULL,NULL,'0',NULL,0),
('P/5/69','SAHIB EMPORIUM','816/4,SANGAM MARKET,NEW KRISHNA,MARKET,CH,CHOWK','DELHI','DEHLI','23988523,9911319000','DE166',NULL,NULL,'0','CR','P/DEL/69','0',NULL,NULL,'0',NULL,0),
('P/5/7','SHRI HARI CREATIONS','888/2,KUCHA KABIL ATTAR,CHANDANI CHOWK','DELHI','DEHLI','9213109557,9911523788','DE102',NULL,NULL,'0','CR','P/DEL/7','0',NULL,NULL,'0',NULL,0),
('P/5/70','A.J.TEXTILE  (GOYAL)','1397,KATRA NAGIN CHAND,CH.CHOWK','DELHI','DEHLI','','DE167',NULL,NULL,'0','CR','P/DEL/70','0',NULL,NULL,'0',NULL,0),
('P/5/71','SHIVENDRA FABRICS (INDRA)','808,KATRA NEELCHANDNI CHOWK','DELHI','DEHLI','23974787,9810754280','DE168',NULL,NULL,'0','CR','P/DEL/71','0',NULL,NULL,'0',NULL,0),
('P/5/72','KANAK CREATION','815/19,SANGAM MARKET,KATRA NEEL,CHANDNI CHOWK','DELHI','DEHLI','23942743,9868059502','DE169',NULL,NULL,'0','CR','P/DEL/72','0',NULL,NULL,'0',NULL,0),
('P/5/73','RADHIKA DUPATTA CENTRE','905/2,KUCHA KABIL ATTAR,CHANDNI CHOUWK','DELHI','DEHLI','23988293','DE170',NULL,NULL,'0','CR','P/DEL/73','0',NULL,NULL,'0',NULL,0),
('P/5/74','S.FASHION','KUCHA KABIL ATTAR,CHANDNI CHOWK','DELHI','DEHLI','','DE171',NULL,NULL,'0','CR','P/DEL/74','0',NULL,NULL,'0',NULL,0),
('P/5/75','BANWARI LAL SATISH CHAND','1343,KATRA LEHSWANCHANDNI CHOWK','DELHI','DEHLI','23964838,9811383303','DE173',NULL,NULL,'0','CR','P/DEL/75','0',NULL,NULL,'0',NULL,0),
('P/5/76','MONU TEXTILE','CHANDNI CHOWKDELHI','DELHI','DEHLI','23987854,9810019519','DE177',NULL,NULL,'0','CR','P/DEL/76','0',NULL,NULL,'0',NULL,0),
('P/5/77','MOHAN LAL BHUSHAN KUMAR & CO.','1397 KATRA NAGIN CHANDCHANDNI CHOWK','DELHI','DEHLI','9350172124','DE178',NULL,NULL,'0','CR','P/DEL/77','0',NULL,NULL,'0',NULL,0),
('P/5/78','LAJ TEXTILES','40-B,KRISHNA CLOTH MARKET,CHANDNI CHOWK','DELHI','DEHLI','011-23933679','DE179',NULL,NULL,'0','CR','P/DEL/78','0',NULL,NULL,'0',NULL,0),
('P/5/79','SRI SAI COLLECTIONS','816/3,SANGAM MARKET,KATRA NEELCHANDNI CHOWK','DELHI','DEHLI','22416413,9811065407','DE180',NULL,NULL,'0','CR','P/DEL/79','0',NULL,NULL,'0',NULL,0),
('P/5/8','P.R.FABRICS (P)LTD.','550-51,KATARA NEEL,CHANDANI CHOWK','DELHI','DEHLI','32448600','DE103',NULL,NULL,'0','CR','P/DEL/8','0',NULL,NULL,'0',NULL,0),
('P/5/80','SARA FASHIONS','SH.NO.650/11,G/FGALI GHANTESHWAR,KATRA NEEL,CHANDNI CHOWK','DELHI','DEHLI','9717960024,9971469615','DE181',NULL,NULL,'0','CR','P/DEL/80','0',NULL,NULL,'0',NULL,0),
('P/5/81','SARDAR TEXTILEX','868/2,KUCHA KABIL ATTARCHANDANI CHOWK','DELHI','DEHLI','','DE182',NULL,NULL,'0','CR','P/DEL/81','0',NULL,NULL,'0',NULL,0),
('P/5/82','SHRI SAI KIRPA (HARI CREATION)','903/3,KUCHA KABIL ATTAR,NEAR.K.SON,S DUPPATTA,CHANDANICHOWK','DELHI','DEHLI','09911523788','DE183',NULL,NULL,'0','CR','P/DEL/82','0',NULL,NULL,'0',NULL,0),
('P/5/83','MOHIT FASHIONS','','DELHI','DEHLI','','DE184',NULL,NULL,'0','CR','P/DEL/83','0',NULL,NULL,'0',NULL,0),
('P/5/84','RAJAT & CO.','KATRA CHOBANCHANDNICHOWK','DELHI','GUJARAT','23268400,23263455','DE186','','07AAJPK8743R1ZT','0','CR','','0','',NULL,'0','null',1),
('P/5/85','SARANGA','502,KRISHANA GALI,KATRA NEELCHANDANI CHOWK','DELHI','DEHLI','01123928988','DE187',NULL,NULL,'0','CR','P/DEL/85','0',NULL,NULL,'0',NULL,0),
('P/5/86','MAHALAKSHMI TEXTILES TRADERS P','889/11,KUCHA KABIL ATTARCHANDNI CHOWK','DELHI','DEHLI','23942449,9810872005','DE188',NULL,NULL,'0','CR','P/DEL/86','0',NULL,NULL,'0',NULL,0),
('P/5/87','R.TEX','1376,C KATRA LESHWANCHANDNI CHOWK','DELHI','DEHLI','9891399750,9818980194','DE189',NULL,NULL,'0','CR','P/DEL/87','0',NULL,NULL,'0',NULL,0),
('P/5/88','SUMBHAV IMPEX','479,1sT FLOOR GALI MEHENDIKATRA NEEL CHANDNI CHOWK','DELHI','DEHLI','011-23965211','DE190',NULL,NULL,'0','CR','P/DEL/88','0',NULL,NULL,'0',NULL,0),
('P/5/89','SHAHI COLLECTION','717/2,NAI BASTI,KATRA NEELCHANDNI CHOWK','DELHI','DEHLI','011-23988652','DE191',NULL,NULL,'0','CR','P/DEL/89','0',NULL,NULL,'0',NULL,0),
('P/5/9','GOYAL TEXTILES','1397,KATARA NAGINCHAND,CHANDANI CHOWK','DELHI','DEHLI','23973557','DE104',NULL,NULL,'0','CR','P/DEL/9','0',NULL,NULL,'0',NULL,0),
('P/5/90','SETHI JEE HANDLOOM','IX/6492,NEHRU GALI,GANDHI NAGAR','DELHI','DEHLI','9810248462,9899232210','DE193',NULL,NULL,'0','CR','P/DEL/90','0',NULL,NULL,'0',NULL,0),
('P/5/91','B.K.FASHION','JANTA GALI GANDHI NAGAR','DELHI','DEHLI','9268788182,22071428','DE192',NULL,NULL,'0','CR','P/DEL/91','0',NULL,NULL,'0',NULL,0),
('P/5/92','CHANDAN ENTERPRISES','XI/6645,NEHRU GALIGANDHI NAGAR','DELHI','DEHLI','9136175693,8882023845','DE194',NULL,NULL,'0','CR','P/DEL/92','0',NULL,NULL,'0',NULL,0),
('P/5/93','LAKSHMI FASHIONWEAR','IX/6639,NEHRU STREET,OPP.JAINGUEST HOUSE,GANDHI NAGAR','DELHI','DEHLI','9810720960,9136450501','DE195',NULL,NULL,'0','CR','P/DEL/93','0',NULL,NULL,'0',NULL,0),
('P/5/94','R.K.KHANNA & CO.','411,KATRA CHOBANCHANDNI CHOWK','DELHI','DEHLI','','DE196',NULL,NULL,'0','CR','P/DEL/94','0',NULL,NULL,'0',NULL,0),
('P/5/95','NEW MALHOTRA GARMENTS','IX/6488,NEHRU GALIGANDHI NAGAR','DELHI','DELHI','9868918672,9650227754','DE198','','07AVOPM113G1ZL','0','CR','P/DEL/95','0','',NULL,'0','PURCHASE O MP',1),
('P/5/96','PANACHE INTERNATIONAL','787,GALI TALLAIYA G.F.KATRANEEL ,CHANDNI CHOWK','DELHI','DEHLI','9811674646','DE199',NULL,NULL,'0','CR','P/DEL/96','0',NULL,NULL,'0',NULL,0),
('P/5/97','EMBRYOS','IX-6487,NEHRU GALIGANDHI NAGAR','DELHI','GUJARAT','9999858887','DE200','','07AHJPN6748C1Z6','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/5/98','MUNJAL COLLECTION','9/6488,NEHRU GALIGANDHI NAGAR','DELHI','DEHLI','9213061590','DE201',NULL,NULL,'0','CR','P/DEL/98','0',NULL,NULL,'0',NULL,0),
('P/5/99','GRADUATE LADIES WEAR','9/6650,NEHRU GALIGANDHI NAGAR','DELHI','DEHLI','9891447601','DE197',NULL,NULL,'0','CR','P/DEL/99','0',NULL,NULL,'0',NULL,0),
('P/6/10','KOTA SAREE CENTER','','INDORE','MADHYA PRADESH','','IN109',NULL,NULL,'0','CR','P/IND/10','0',NULL,NULL,'0',NULL,0),
('P/6/11','SWAPNA SUNDARI(PURCHASE)','','INDORE','MADHYA PRADESH','33220400000001','IN110','','23APPPS6767J1Z2 ','0','CR','','0','',NULL,'0','PURCHASE IN MP',0),
('P/6/13','M/S DILIPKUMAR SANTOSHKUMAR','79/11,SITLAMATA BAZAR,NEEMAMARKET,2ND,FLOOR,','INDORE','MADHYA PRADESH','2454680,9893059597','IN112',NULL,NULL,'0','CR','P/IND/13','0',NULL,NULL,'0',NULL,0),
('P/6/16','SURBHI SAREE','43/46,M.T.CLOTH MARKETMAHAVEER CHOWK','INDORE','MADHYA PRADESH','2459753','IN113','','23AAQFS6914B1Z9','0','CR','','0','',NULL,'0','null',0),
('P/6/17','ANKUR SAREE EMPORIUM','122,M.T.CLOTH MARKETMHAVEER CHOWK','INDORE','MADHYA PRADESH','2459752','IN114','','23AAFFA4673E1ZS','0','CR','','0','',NULL,'0','null',0),
('P/6/18','SHREE SIDHIKA','80,NEEMA MARKET,2 ND FLOORSITLAMATA BAZAAR','INDORE','MADHYA PRADESH','9826810185,9926275401','IN115','','23AOIPB2015E1ZU','0','CR','','0','',NULL,'0','null',0),
('P/6/19','SAHELI SUITS','42,ITWARIA BAZAR KANCH MANDIRRAOD','INDORE','MADHYA PRADESH','0731-24500027,9827200666','IN116',NULL,NULL,'0','CR','P/IND/19','0',NULL,NULL,'0',NULL,0),
('P/6/20','MAHENDRA TRADERS 2016-17','28,M.T.CLOTH MARKET','INDORE','MADHYA PRADESH','','IN119',NULL,NULL,'0','CR','P/IND/20','0',NULL,NULL,'0',NULL,0),
('P/6/21','KHUSHI FASHION','149-150 SHANTI CHAMBER 2nd FLROPP.COMETEE HALL) M.T.CLOT MRK','INDORE','MADHYA PRADESH','9926523644,2452644','IN120',NULL,NULL,'0','CR','P/IND/21','0',NULL,NULL,'0',NULL,0),
('P/6/22','NIRANKARI TEXTILES','157,M.T.CLOTH MARKET','INDORE','MADHYA PRADESH','0731-2450571,9303239218','IN121',NULL,NULL,'0','CR','P/IND/22','0',NULL,NULL,'0',NULL,0),
('P/6/24','SHRUTI SUITS LLP','275,M.T.COTH MARKET','INDORE','MADHYA PRADESH','07312459126, 2456126 (A/C)','IN122','','23ACTFS4651C1Z2','0','CR','','0','8989503971',NULL,'0','PURCHASE IN MP',0),
('P/6/25','RJ CREATION','176,2nd FLOOR M.T.COTH MAREKETSINDHI PATTI','INDORE','MADHYA PRADESH','9425055210','IN123',NULL,NULL,'0','CR','P/IND/25','0',NULL,NULL,'0',NULL,0),
('P/6/26','M.M.FASHIONS','216,M.T.CLOTH MARKET 1sT FLOORNEAR SARITA TAX','INDORE','MADHYA PRADESH','9893433047,9893518090','IN124',NULL,NULL,'0','CR','P/IND/26','0',NULL,NULL,'0',NULL,0),
('P/6/27','HUNNY TEXTILES','77,FREEGANJ M.T. CLOTH MARKET','INDORE','MADHYA PRADESH','0731-2450571,9977010073','IN125',NULL,NULL,'0','CR','P/IND/27','0',NULL,NULL,'0',NULL,0),
('P/6/28','MYRA CREATION','116,1st FLOOR TRADE CENTER18,SOUTH TUKOGUNJ','INDORE','MADHYA PRADESH','0731-4263503,9669197777','IN126',NULL,NULL,'0','CR','P/IND/28','0',NULL,NULL,'0',NULL,0),
('P/6/29','SAVITA JI SOLANKI C/O DIKKU BH','','INDORE','MADHYA PRADESH','','C0585',NULL,NULL,'0','CR','P/IND/29','0',NULL,NULL,'0',NULL,0),
('P/6/3','PARUL BAHAN JI INDORE','','INDORE','MADHYA PRADESH','','C0323',NULL,NULL,'0','CR','P/IND/3','0',NULL,NULL,'0',NULL,0),
('P/6/30','CJ FASHION','1 st FLOR M.T.CLOTH MARKETGOVERDHAN CHOWK','INDORE','MADHYA PRADESH','0731-2459747,9826037079','IN128',NULL,NULL,'0','CR','P/IND/30','0',NULL,NULL,'0',NULL,0),
('P/6/31','ANKITA FASHION','31-B,SIR HUKAMCHAND MARGITWARIYABAZAR','INDORE','MADHYA PRADESH','0731-2450496,9893310615','IN129','','23AAMPW7446G1Z9','0','CR','','0','',NULL,'0','null',0),
('P/6/32','MAHENDRA TRADERS 2017-18','28,M.T.CLOTH MARKET','INDORE','MADHYA PRADESH','073`1-2459997  -2459850','IN107','','23ACHPG3395H1ZZ','0','CR','','0','9926044390',NULL,'0','PURCHASE IN MP',0),
('P/6/33','P.A.MARKETING 2017-18','28,M.T.CLOTH MARKET','INDORE','MADHYA PRADESH','','IN108','','23AAGHP2325F1ZL ','0','CR','','0','',NULL,'0','PURCHASE IN MP',0),
('P/6/34','SHREE TIRUPATI SAREE','220,VITHLESWARAL MARKETM.T.CLOTH MARKET','INDORE','MADHYA PRADESH','','IN130',NULL,NULL,'0','CR','P/IND/34','0',NULL,NULL,'0',NULL,0),
('P/6/35','FATEHCHAND DARBARILAL','77,FRERGANJ MARKETINSIDE M T CLOTH MARKET','INDORE','MADHYA PRADESH','','','','23BBLPM5720J1ZL','0','CR','','0','9893030100,7805030100','','','PURCHASE IN MP',0),
('P/6/36','SWAPNA SUNDARI NX','104,105 AASHIRWAD COMPLEX KANADIA ROAD','INDORE','MADHYA PRADESH','0731-2595170','','','','0','CR','','0','9425900072','','','PURCHASE 5%',0),
('P/6/37','B.H.CREATION','231,VITHLESHWAR MARKETM.T.CLOTH MARKET','INDORE','MADHYA PRADESH','','','','23AFOPT3922P1ZS','0','CR','','0','9926811795,9926219795','','','PURCHASE 5%',0),
('P/6/38','SALONI','48,SITLAMATA BAZARNEAR PUJAB NATIONAL BANK','INDORE','MADHYA PRADESH','0731-2538767','','','23AATFS1048M1ZS','0','CR','','0','9826550099,9826311099','','','PURCHASE 5%',0),
('P/6/4','SURENDRAKUMAR NARENDRAKUMAR(SH','275, M.T. CLOTH MARKET','INDORE','MADHYA PRADESH','2459126','IN101',NULL,NULL,'0','CR','P/IND/4','0',NULL,NULL,'0',NULL,0),
('P/6/5','ANIL KUMAR DINESH KUMAR','31,HUKUMCHAND MARG,IITWARIA BAZAR','INDORE','MADHYA PRADESH','9893310615, 9425350519(A/C','IN102','','23AAOPW2409D1S','0','CR','','0','',NULL,'0','PURCHASE IN MP',0),
('P/6/6','SWAPNA SUNDARI(SALES)','S.NO.7,AASHIRWAD COMPLEX,KANADIA ROAD','INDORE','MADHYA PRADESH','07312595170','IN103','','23APPPS6767J1Z2 ','0','CR','','0','',NULL,'0','PURCHASE IN MP',0),
('P/6/7','SWAPNA SUNDARI DESIGNER','SHOP NO.7,ASHIRWAD C','INDORE','MADHYA PRADESH','0731-2595170,9425900072','IN104','','23APPPS6767J1Z2','0','CR','','0','',NULL,'0','null',0),
('P/6/8','KARAN KUMAR BHARAT KUMAR','SITLAMATA BAZAR,42/4(BOHRA BAKHAL)','INDORE','MADHYA PRADESH','2546886','IN105','','23ACRPM6762A1ZL','0','CR','','0','',NULL,'0','PURCHASE IN MP',0),
('P/6/9','SUNMOON HANDLOOM','133-134,M.T.CLOTH MARKET,MAHAVEER CHOUWK,MUCHHAL CHAMBER 1S','INDORE','MADHYA PRADESH','9826312365','IN106',NULL,NULL,'0','CR','P/IND/9','0',NULL,NULL,'0',NULL,0),
('P/7/192','SEYAS','41, BACK SIDE OF RAM MANDIRKHATI PURA','RATLAM','MADHYA PRADESH','9179789699','SEYAS',NULL,NULL,'0','CR','P/RTM/192','0',NULL,NULL,'0',NULL,0),
('P/7/197','P.K.TEXTILES','84,NOLAIPURA','RATLAM','MADHYA PRADESH','','RA001',NULL,NULL,'0','CR','P/RTM/197','0',NULL,NULL,'0',NULL,0),
('P/7/200','SIDHACHAL SYNTHETICS','32,NEW CLOTH MARKET,','RATLAM','MADHYA PRADESH','239739,9425103139,','RA002','','23AFNPK7160H1ZD','0','CR','','0','',NULL,'0','null',0),
('P/7/217','KOTHARI SUTING & SHARTING','28,BHARAVA KI KUI','RATLAM','MADHYA PRADESH','266403,9827079388','RA004',NULL,NULL,'0','CR','P/RTM/217','0',NULL,NULL,'0',NULL,0),
('P/7/231','GANDHI FABRICS','DIGAMBAR JAIN TEMPLE,TOPKHANA','RATLAM','MADHYA PRADESH','237136','RA005',NULL,NULL,'0','CR','P/RTM/231','0',NULL,NULL,'0',NULL,0),
('P/7/232','J.D.INTERPRISES','78,BAJAJ KHANA','RATLAM','MADHYA PRADESH','236565','RA006',NULL,NULL,'0','CR','P/RTM/232','0',NULL,NULL,'0',NULL,0),
('P/7/253','NAKHRAALI','5 NOLAIPURA','RATLAM','MADHYA PRADESH','9425103426,9827202123','RA007',NULL,NULL,'0','CR','P/RTM/253','0',NULL,NULL,'0',NULL,0),
('P/7/254','S.R.TEXTILE','57,BHARAWA KI KUI','RATLAM','MADHYA PRADESH','234621,9425195221','RA008',NULL,NULL,'0','CR','P/RTM/254','0',NULL,NULL,'0',NULL,0),
('P/7/281','SHRADHA COLLECTION','','RATLAM','MADHYA PRADESH','9827233003/9827252033','RA009',NULL,NULL,'0','CR','P/RTM/281','0',NULL,NULL,'0',NULL,0),
('P/7/314','BARMECHA CLOTH STORE','18, OPP.OZHAKAHLI MASJIDPORWADO KA WAAS','RATLAM','MADHYA PRADESH','07412-231849','RA010',NULL,NULL,'0','CR','P/RTM/314','0',NULL,NULL,'0',NULL,0),
('P/7/319','VIVAH SAREES','58/2,JAIN GIRLS STREET,SAKHLECHA MARKET,CHOMUKHIPUL','RATLAM','MADHYA PRADESH','9302061065,9406626628','RA011',NULL,NULL,'0','CR','P/RTM/319','0',NULL,NULL,'0',NULL,0),
('P/7/325','VIPIN JI PAL C/O.RAJKUMAR JI','C/O.RAJKUMAR JI VINODIYA1543/B NEW RLY.COLCNY','RATLAM','MADHYA PRADESH','9907253750','C0088',NULL,NULL,'0','CR','P/RTM/325','0',NULL,NULL,'0',NULL,0),
('P/7/332','ARIHANT TEXTILES','32,BAJAJ KHANA','RATLAM','MADHYA PRADESH','241124 9424529058','RA012',NULL,NULL,'0','CR','P/RTM/332','0',NULL,NULL,'0',NULL,0),
('P/7/340','SAMRATHMAL RAJENDRA KUMAR','57,BHARWA KI KUI','RATLAM','MADHYA PRADESH','07412-234621,9425195221','RA013',NULL,NULL,'0','CR','P/RTM/340','0',NULL,NULL,'0',NULL,0),
('P/7/357','SHUBHAM CORPORATION','52,BHARAWA KI KUI','RATLAM','MADHYA PRADESH','07412-241582,9977604444','RA015',NULL,NULL,'0','CR','P/RTM/357','0',NULL,NULL,'0',NULL,0),
('P/7/399','PEEHAR SAREES','67,RUBY TOWAR CHANDNICHOWK','RATLAM','MADHYA PRADESH','07412231563','RA016','','23ARDPC5431B1Z0','0','CR','','0','',NULL,'0','PURCHASE IN MP',0),
('P/7/419','GOURAV SAREE CENTER','68 CHANDNI CHOWK','RATLAM','MADHYA PRADESH','','RA017','','23AAVPC6915H1ZM','0','CR','','0','',NULL,'0','PURCHASE 5%',0),
('P/7/433','SUDARSHAN KUMAR SHANTILAL JAIN','MANAK CHOWK','RATLAM ','MADHYA PRADESH','9827233003,9827252033','RA003',NULL,NULL,'0','CR','P/RTM/433','0',NULL,NULL,'0',NULL,0),
('P/7/439','JAMBOO STORE','','RATLAM','MADHYA PRADESH','','','','23AIZPG1128NIZX ','0','CR','','0','','','','PURCHASE 5%',0),
('P/7/66','MANNU BHAI PATERIYA','L/23, JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','C0223',NULL,NULL,'0','CR','P/RTM/66','0',NULL,NULL,'0',NULL,0),
('P/8/1','KOTHARI SILK STORES','CK.20/10,AYUSHMAN KATRA 2,ndFLOOR,THATHERI BAZAR(CHOWK)','VARANASI','UTTAR PRADESH','2420441','VA101',NULL,NULL,'0','CR','P/VAR/1','0',NULL,NULL,'0',NULL,0),
('P/8/10','MINHAJ CREATION','3,MADANI MARKET UNDER JAMMU& KASHMIR BANK MADANPURA','VARANASI','UTTAR PRADESH','0542-2452546','VA111',NULL,NULL,'0','CR','P/VAR/10','0',NULL,NULL,'0',NULL,0),
('P/8/11','TANZEB FASHIONS','D-28/61 .PANDAY HAVELI ROAD','VARANASI','UTTAR PRADESH','0542-2455900 2454600','VA112','','09AALPW35591J','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/8/12','SSD HANDLOOM','58/53,1st FLOOR NEAR ASHBHAIRO MANDIR CHOWK','VARANASI','UTTAR PRADESH','0542-2404240,9839915104','VA113',NULL,NULL,'0','CR','P/VAR/12','0',NULL,NULL,'0',NULL,0),
('P/8/13','LOYAL FAB','D.31/336 A.337-A,MADANPURA ROADMADANPURA','VARANASI','UTTAR PRADESH','05422451567','','','09ABVPA7460J1Z5','0','CR','','0','','','','PURCHASE O MP',1),
('P/8/2','TALIB FABRICS','3,MADANI MARKET,MADAN PURA,','VARANASI','UTTAR PRADESH','2452546,2454806','VA102','','09AAEPZ6839L1ZQ','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/8/3','NISA SSS','D.35/70,CITY CENTRE,MADAN PURA','VARANASI','UTTAR PRADESH','2451972,2450556','VA103','','09AGKPR3901R1ZN','0','CR','','0','',NULL,'0','PURCHASE 5%',1),
('P/8/4','HAKIMCO','D-28/61,PANDAY HAVELI ROAD,','VARANASI','UTTAR PRADESH','05422455900','VA104',NULL,NULL,'0','CR','P/VAR/4','0',NULL,NULL,'0',NULL,0),
('P/8/5','R.K.SAREES','KRISHNA SUNDARY','VARANASI','UTTAR PRADESH','','VA106',NULL,NULL,'0','CR','P/VAR/5','0',NULL,NULL,'0',NULL,0),
('P/8/6','KRISHNA SUNDRY COTTAGE INDSTRI','CK.58/55,AAS BHAIROCHOWK','VARANASI','UTTAR PRADESH','9415221343,9453006074','VA107',NULL,NULL,'0','CR','P/VAR/6','0',NULL,NULL,'0',NULL,0),
('P/8/7','LOYAL EXCLUSIVE PVT.LTD.','D.31/336A-337A,MADANPURA RAOD','VARANASI','UTTAR PRADESH','0542-2451567','VA108','','09AAGFL6976Q1Z7','0','CR','','0','9935530070,7800678008',NULL,'0','PURCHASE O MP',1),
('P/8/8','JAMAL COLLECTION','B-16/67-1,PANDEY HAWELIMADANPURA','VARANASI','UTTAR PRADESH','0542-2450608,9336911749','VA109',NULL,NULL,'0','CR','P/VAR/8','0',NULL,NULL,'0',NULL,0),
('P/8/9','FANCY FAIR','B 18/187,A-1 MALTI BAGHREORI TALAB','VARANASI','UTTAR PRADESH','2455177,08726606040','VA110',NULL,NULL,'0','CR','P/VAR/9','0',NULL,NULL,'0',NULL,0),
('P/9/1','SARASWATI CREATIONS','SHOP NO.32,33,BHOLA  NATH DHARAMSHALA.GOLE DARWAZACHOWK,','LUCKNOW','UTTAR PRADESH','0522-2255626,9621634498','LU101','AHMPK8537D','09AHMPK8537D1Z1','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/9/2','PVs CHIKAN CREATION','GOLE DARWAZA,CHOWK SARAFA','LUCKNOW','UTTAR PRADESH','2255755,3058154','LU102','','09AHNPS7347A1Z0','0','CR','','0','',NULL,'0','PURCHASE O MP',1),
('P/9/3','SHREE GANESH CREATION','','LUCKNOW','UTTAR PRADESH','','LU104',NULL,NULL,'0','CR','P/LUC/3','0',NULL,NULL,'0',NULL,0),
('P/9/4','AYUB KHAN','H.NO.17HUSNA PALACE,RAM NAGARBALAGANJ','LUCKNOW','UTTAR PRADESH','7607868478','LU105',NULL,NULL,'0','CR','P/LUC/4','0',NULL,NULL,'0',NULL,0),
('P/9/5','SHRI GANESH CREATION','SHOP.NO.A-9 BASEMENT KUSUDEEPBUILDIND, CHOWK','LUCKNOW','UTTAR PRADESH','9936750498','LU106',NULL,NULL,'0','CR','P/LUC/5','0',NULL,NULL,'0',NULL,0),
('P/9/6','KARISHMA CHIKAN STORE','CHOK SARAFA','LUCKNOW','UTTAR PRADESH','','LU107',NULL,NULL,'0','CR','P/LUC/6','0',NULL,NULL,'0',NULL,0),
('P/BAN/1','PAL SAREES','7,1,st FLOOR, PATHI COMPLEXBETTAPPA LANE,NAGARTHPET CROSS','BANGALORE','KARNATAKA','41325161','BA101',NULL,NULL,'0','CR','P/BAN/1','0',NULL,NULL,'0',NULL,0),
('P/BAN/2','LISHA SAREES','27,1ST FLOOR BALAJI BLD.BETTAPPA LANESRMT LORRY CT STREET CR','BANGALORE','KARNATAKA','221055059','BA104',NULL,NULL,'0','CR','P/BAN/2','0',NULL,NULL,'0',NULL,0),
('P/BAN/3','HITENDRA SILK & SAREES','55,ANJENAYA TEMPLE STREETNARAYAN SHETTY PETH','BANGALORE','KARNATAKA','41475965','BA105',NULL,NULL,'0','CR','P/BAN/3','0',NULL,NULL,'0',NULL,0),
('P/BAN/4','HIMMAT SILKS','1,SRIPAL MANSION 1st FLOORAPPAJI RAO LANE 1st CROSS','BANGALORE','KARNATAKA','22290673','BA106',NULL,NULL,'0','CR','P/BAN/4','0',NULL,NULL,'0',NULL,0),
('P/BAN/5','KAJOL SAREES','7,1,st FLOOR PATHI COMPLEXBETTAPPA LANE NAGARTHPET CROSS','BANGALORE','KARNATAKA','9844509772','BA102',NULL,NULL,'0','CR','P/BAN/5','0',NULL,NULL,'0',NULL,0),
('P/BAN/6','OM SHANTI SAREES','# 33,PILLAPPA LANE 1,st FLOORNAGARTHPET CROSS','BANGALORE','KARNATAKA','22210074','BA103',NULL,NULL,'0','CR','P/BAN/6','0',NULL,NULL,'0',NULL,0),
('P/BAR/1','DUA SILK STORE','OPP.DEEPAK SWEETS,BARA BAZAR','BAREILLY','UTTAR PRADESH','05813242360,09412288700','BR101',NULL,NULL,'0','CR','P/BAR/1','0',NULL,NULL,'0',NULL,0),
('P/BAR/2','ANIL SELECTION','4-5,PASHUPATI MARKET,BARA BAZAR,','BAREILLY','UTTAR PRADESH','05813292902','BR102',NULL,NULL,'0','CR','P/BAR/2','0',NULL,NULL,'0',NULL,0),
('P/BAR/3','MAHIMA ARTS{GULATI}','NR.KASHINATH SETH JEWELLERS,1ST FLOOR,MEHRA MARKET,BARABZR','BAREILLY','UTTAR PRADESH','05813294356','BR103',NULL,NULL,'0','CR','P/BAR/3','0',NULL,NULL,'0',NULL,0),
('P/BAR/4','PURAN CHAND CREATION\"S','GANDHI KATRA,BARA BAZAR','BAREILLY','UTTAR PRADESH','2471714,9412292201','BR104',NULL,NULL,'0','CR','P/BAR/4','0',NULL,NULL,'0',NULL,0),
('P/BAR/5','ONKAR NATH & BROS','BARA BAZAR,DARZI CHOWK','BAREILLY','UTTAR PRADESH','3257298,9897047408','BA107',NULL,NULL,'0','CR','P/BAR/5','0',NULL,NULL,'0',NULL,0),
('P/BAR/6','DUA FASHIONS','122-A,OPP.DUSHERA MELA GROUNDMODEL TOWN','BAREILLY','UTTAR PRADESH','9319148696,9219531802','BR106',NULL,NULL,'0','CR','P/BAR/6','0',NULL,NULL,'0',NULL,0),
('P/BAR/7','SAREE SANSAR MODERN','SANSAR HOUSE,BARA BAZAR','BAREILLY','UTTAR PRADESH','','BR107',NULL,NULL,'0','CR','P/BAR/7','0',NULL,NULL,'0',NULL,0),
('P/BEL/4','SHREE SARVESHWAR TRADING CO.','OPP.SHIVAJI GARDENDR.S.P.M.ROAD,SHAHAPUR,','BELGAUM 2405130','KARNATAKA','0831-2424307,9448479567','BE104',NULL,NULL,'0','CR','P/BEL/4','0',NULL,NULL,'0',NULL,0),
('P/COI/1','RAJASTHAN HANDLOOM TEXTILES','187, RANGAI GOWDER STREET','COIMBATORE','TAMILNADU','473694','CO101',NULL,NULL,'0','CR','P/COI/1','0',NULL,NULL,'0',NULL,0),
('P/COI/2','KIRTHI CREATIONS','1078,RANGAI GOWDER STREET,','COIMBATORE','TAMILNADU','2470720','CO102',NULL,NULL,'0','CR','P/COI/2','0',NULL,NULL,'0',NULL,0),
('P/COI/3','PRAVIN HANDLOOM TEXTILES','1197,SUKRAWARPET STREET','COIMBATORE','TAMILNADU','0422-2470825','CO103',NULL,NULL,'0','CR','P/COI/3','0',NULL,NULL,'0',NULL,0),
('P/COI/4','SHA NAGRAJ CHANDANMAL','1180,SUKRAWARPET,','COIMBATORE','TAMILNADU','04222472531','CO104',NULL,NULL,'0','CR','P/COI/4','0',NULL,NULL,'0',NULL,0),
('P/DEL/1','TARA SYNTEX(PVT.)LTD.','831-832,MAIN ROAD,CHANDNI CHOWK,','DEHLI','DEHLI','32593122,32593123','DE165',NULL,NULL,'0','CR','P/DEL/1','0',NULL,NULL,'0',NULL,0),
('P/DEL/2','NAGPAL TEXTILES','552,KATRA NEELCHANDNI CHOWK','DEHLI','DEHLI','23933556','DE172',NULL,NULL,'0','CR','P/DEL/2','0',NULL,NULL,'0',NULL,0),
('P/DEL/3','RAM LAL & SONS(ANOKHI KARACHI','9,NEW MEENA BAZARJAMA MASJID','DEHLI','DEHLI','9971533801,9818113009','DE174',NULL,NULL,'0','CR','P/DEL/3','0',NULL,NULL,'0',NULL,0),
('P/DEL/4','SANYA HANDLOOMS','815/9,SANGAM MARKET,KATRA NEEL,CHANDNI CHOWK','DEHLI','DEHLI','23831494,9891708701','DE175',NULL,NULL,'0','CR','P/DEL/4','0',NULL,NULL,'0',NULL,0),
('P/DEL/5','PEE ESS TEXTILES','1397/KATRA NAGIN CHANDCHANDNI CHOWK','DEHLI','DEHLI','2512032','DE176',NULL,NULL,'0','CR','P/DEL/5','0',NULL,NULL,'0',NULL,0),
('P/DHA/1','MAHAMMED BILAL KHATRI','151/2,MAHAKALESHWAR MARGBAGH','DHAR','DEHLI','07297267163','DH101',NULL,NULL,'0','CR','P/DHA/1','0',NULL,NULL,'0',NULL,0),
('P/DHA/2','JARINA KHAN W/O ANIS JI KHAN','9,RASMANDHAL','DHAR','DEHLI','9669172510','C0373',NULL,NULL,'0','CR','P/DHA/2','0',NULL,NULL,'0',NULL,0),
('P/GUR/1','RAGHAV ENTERPRISES','S.NO.129,DEEP PLAZA BUILDINGOPP. DISTT. COURT','GURGAON','UTTAR PRADESH','011-23266283,9891420089','GU101',NULL,NULL,'0','CR','P/GUR/1','0',NULL,NULL,'0',NULL,0),
('P/JAI/93','LAXMI WHOLESALE CLOTH MARCHANT','1,DHULA HOUSE BAPU BAZAR','JAIPUR-','RAJASTHAN','9314515576,9460871085','JA183',NULL,NULL,'0','CR','P/JAI/93','0',NULL,NULL,'0',NULL,0),
('P/SAI/3','SANGEETA CHAREL MADAM,SAILANA','','SAILANA','MADHYA PRADESH','9425195455','C0387',NULL,NULL,'0','CR','P/SAI/3','0',NULL,NULL,'0',NULL,0);

/*Table structure for table `party_sales` */

DROP TABLE IF EXISTS `party_sales`;

CREATE TABLE `party_sales` (
  `party_code` varchar(45) NOT NULL,
  `party_name` varchar(45) NOT NULL,
  `mobile_no` varchar(45) DEFAULT NULL,
  `address1` varchar(45) DEFAULT NULL,
  `address2` varchar(45) DEFAULT NULL,
  `address3` varchar(45) DEFAULT NULL,
  `address4` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `mark_down` varchar(45) DEFAULT NULL,
  `igst` tinyint(1) NOT NULL,
  `pan_no` varchar(45) DEFAULT NULL,
  `gstin_no` varchar(45) DEFAULT NULL,
  `whatsapp_no` varchar(45) DEFAULT NULL,
  `bank_account1` varchar(45) DEFAULT NULL,
  `bank_name1` varchar(45) DEFAULT NULL,
  `bank_ifsc1` varchar(45) DEFAULT NULL,
  `bank_account2` varchar(45) DEFAULT NULL,
  `bank_name2` varchar(45) DEFAULT NULL,
  `bank_ifsc2` varchar(45) DEFAULT NULL,
  `opening_bal` varchar(45) DEFAULT NULL,
  `bal_type` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `branch_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`party_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `party_sales` */

/*Table structure for table `party_sales1` */

DROP TABLE IF EXISTS `party_sales1`;

CREATE TABLE `party_sales1` (
  `party_code` varchar(45) NOT NULL DEFAULT '',
  `party_name` varchar(45) DEFAULT NULL,
  `address` longtext,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `phone_no` varchar(45) DEFAULT NULL,
  `email_no` varchar(45) DEFAULT NULL,
  `pan_no` varchar(45) DEFAULT NULL,
  `tin_no` varchar(45) DEFAULT NULL,
  `opening_bal` varchar(45) DEFAULT NULL,
  `bal_type` varchar(45) DEFAULT NULL,
  `web_sit` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL,
  `mobileno` varchar(45) DEFAULT NULL,
  `whatsapp_no` varchar(45) DEFAULT NULL,
  `IGST` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`party_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `party_sales1` */

insert  into `party_sales1`(`party_code`,`party_name`,`address`,`city`,`state`,`phone_no`,`email_no`,`pan_no`,`tin_no`,`opening_bal`,`bal_type`,`web_sit`,`branchid`,`mobileno`,`whatsapp_no`,`IGST`) values 
('S/1','SAVITA JI SOLANKI C/O DIKKU BHAI','','INDORE','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/10','SONU JI MESSI (CONVENT SCHOOL)','5,RITAYARD COLONY,THOMAS KICHAL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9907326456','',0),
('S/100','LALAN SINGH JI THAKUR','HOUSE NO.20 INDRALOK NAGAROPP.PANCHMUKI MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424020187','',0),
('S/101','LELE JI( INDORE)','SURESH JI LELE,HOUSE NO.18,TILAK PATH,DHONE PATEL KI GALI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9302135124','',0),
('S/102','GEORGE MADAM','INDIRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9993800514','',0),
('S/103','K.LALLI MADAM','C-39,JAVAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425990263,07803806978','',0),
('S/104','MOHIN BHAI  A.C. WALE','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/105','RAJENDRA JI BAARIYA','SBICOLLECTORATE BRANCH','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/106','MANISH JI JOSHI','110,JOSHI BUILDING STATION ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9406635100,9329311322','',0),
('S/107','L.THOMAS','329,GHANDHI NAGAR,NR.GAYATRI K KIRANASTORE,NR. GARDEN','RATLAM','MADHYA PRADESH','2604831','','','','0','CR','','0','','',0),
('S/109','MRS. MARY THOMAS','DR.DEVI SINGH COLONYN.R. NEW COART ','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9691206960,9770202805','',0),
('S/11','ALKA JI SHRIVASTAV','GUJRATI SCHOOLNEW ROAD','RATLAM','MADHYA PRADESH','234035','','','','0','CR','','0','','',0),
('S/110','PARWATI JI MISHRA','1530;A  RAILWAY NURSE COLONYROAD NO.3','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827092311','',0),
('S/111','MANNU BHAI PATERIYA','L/23, JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/113','MEENA JI PUNJABI','SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/114','SAROJ JOY WILSON C/O L.DENIYAL','1/46 B INDIRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/115','TERRY MADAM','GANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425354997','',0),
('S/116','VINITA JI JOHN','47,M.B.NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9300630609','',0),
('S/117','YASMIN JI SHERANI','18/36,SHAIRANI PURA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9826634258,9893441625','',0),
('S/118','MAMTA JI SHARMA','149/B JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','07412 221165','','','','0','CR','','0','9669914014','8435014014',0),
('S/119','MAHESH CHANDRA JI SARLA','806/C, JAORA ROADGHATLA COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9109917162','',0),
('S/12','HEMLATA JI JAIN','D/20,ANAND COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425329800','',0),
('S/120','A.P.PANDEY','M/39 DEVRA DEV NARAYAN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9098644557','',0),
('S/121','LALCHAND JI DUBEY','Q.NO.1622,ROAD NO.7OLD RAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9752231192','',0),
('S/122','MEGHNA JI PETHE','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/123','MANJEET JI KAUR','SINDHII BUILDING','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/124','C.J.SINGH','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424528113','',0),
('S/125','SHAILENDRA JI NIGAM','2,GOVERMENT BUILDINGOPP.MAHTMA GANDHI SCHOOLJAORA','RATLAM','MADHYA PRADESH','222755','','','','0','CR','','0','9407107975','',0),
('S/126','SURENDRA SINGH GAUTAM','93/D RAILWAY MANDI COLONYNAGDA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9977943336','7000324836',0),
('S/127','MUKESH JI C/O DR.SAIYYAAD','','RATLAM','MADHYA PRADESH','','','','','169','DR','','0','','',0),
('S/128','BHANWAR SINGH PANWAR','EX-106,JAWAHAR NAGARC/O KANHYALAL JI GOYAL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9301438182','',0),
('S/129','ZENUL KHAN','C/O ABDUL AZIZ KHAN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8103386305,9009444455','',0),
('S/13','SHASHIKALA JI GAUR','25,GATE NO.1GLOBAL TOWNSHIP','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425438233','',0),
('S/130','MANJU BAHAN JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/131','L.DENIYAL','LAURILA VILLA1/46-B INDIRA NAGAR','RATLAM','MADHYA PRADESH','07412-261478','','','','0','CR','','0','8109773047','',0),
('S/132','CONNIE MADAM(CONVENT SCHOOL)','MITRA NIWAS COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9907478028','',0),
('S/133','KAHNYALAL JI PATEL','COPRATIVE BANKA-13 ALAJI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425355528','',0),
('S/134','MADHU JI DAVE','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/135','DR.RATHOR SAHAB','JAORA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425926967','',0),
('S/136','KIRAN JI RATHORE','BALAJI NAMKEEN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/137','YOGESH JI PATIL','GULMOHAR COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9752492967','',0),
('S/138','M.NAVRATAN CLOTH EMPORIUM','NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/139','MEENU JI POWER HOUSE ROAD','SAILANA BUS STATNDOPP.MANDI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/14','AKILA BANO JI','444,INDRA NAGARNEAR SAI BABA TEMPLE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/140','K.K.MISHRA JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/141','ASHOK KUMAR JI YATI','D/20 ANAND COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9039246894','',0),
('S/142','BHUWNESHWAR JI','589,GHANDI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9977024251','',0),
('S/143','W.WILLIAM C/O S.K.ALEXZENDER','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/144','SEEMA JI KOSHIK C/O MANNU BHAI','DRM OFFICE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/145','M.P.KRISHAN GUPTAN','HO.NO.136 PRATAP NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/146','M.PATODIYA','222,KASTURBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/147','A.BARLO','395,KASTURBA NAGAR GALI NO.5SANCHI DERI PAAS WALI GALLI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/148','M.M.SANT JI','106,DEVRA DEV NARAYAN COLONYNEAR GURUDWARA','RATLAM','MADHYA PRADESH','07412 261262','','','','0','CR','','0','','',0),
('S/149','CHANDRAWATI JI MOYAL','6,DR.DEVISINGH COLONYOPP.NEW COURT','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893586111,9179928423','',0),
('S/15','KASHI PRASAD JI YADAV','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/150','KANCHAN JI (BALAJI)','BALAJI SWEATSNEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/151','M.L.CHOUHAN','368,PAWAN SHREE INDIRA NAGAREAST NR.JEEVAN BIMA HOSPITAL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424809097','',0),
('S/152','ASHA JI TIWARI','ROTRI HAAL AJNATA TALKIZ','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/153','MUNSILLA JI','H.NO.18POLICE LINECOLONYDO BATTI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827660565','',0),
('S/154','SEEMA JI VYAS','101,SHRIMALI VAAS','RATLAM','MADHYA PRADESH','07412-232605','','','','0','CR','','0','','',0),
('S/155','NILIMA JI SHEETUT','9,BASANT VIHARKASTURBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9981545329','',0),
('S/156','ANAMIKA JI SARASWAT','GOVERMENT GILS COLLEGE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/157','NARENDRA JI GUPTA','VISHRAM 14,STATE BANK COLONYLOKENDRA BHAWAN COMPOUND','RATLAM','MADHYA PRADESH','07412 234650','','','','0','CR','','0','','',0),
('S/158','SURYHAN SINGH JI','C/O SASHIKALA JI GOUR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/159','CHANDRAKANTA JI DEVRA','W/O GAJRAJ SINGH JI DEVRAB-93 VINOBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7415102930','',0),
('S/16','HARIHAR JI RATHORE','15,POLICE LINE,DO BATTI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9981467664','',0),
('S/160','JAGDISH JI CHOBEY','JVL MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/161','ARSHAD JI C/O DR.SAIYAD','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/162','NARENDRA JI RAJGURU','SUNDRAMC/O V.K.SOMANIJAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/163','NARENDRA JI SONI','19,KATJU NAGAR BEHIND MEHRA NURSING HOMEGALI NO,.2','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9229447334','',0),
('S/164','SHRI ARVIND SOCIETY','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/165','ATUL JI BHATNAGAR( GARD SAHAB)','NEAR CONVENT SCHOOL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/166','POONAM JI ( RATI ENTERPRISES)','NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/167','SHRADHA JI SHARMA','13/2,PROFFESAR NAGARNAYAGANV RAJGARH','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9098215095,8349487771','',0),
('S/168','NIRANJAN SINGH JI PRAJAPAT','POLICE LINE DO BATI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/169','ASHA','NEAIND.R RAJ STONE POONAM KARMDI ROAD','RATLAM','MADHYA PRADESH','405328','','','','0','CR','','0','9425195238','',0),
('S/17','KAMAL JI SHAKY','ARIHANT PARISAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827338415,9630707078','',0),
('S/170','DURGESH JI SHUKLA ( GARD SAHAB)','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/171','NEMA JI STATION','24,LOKENDRA BAWANBANK COLONY','RATLAM','MADHYA PRADESH','07412  230665','','','','0','CR','','0','','',0),
('S/172','NARESH JI KOSHIK','5,SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/173','NOBEL JI JORGE','MEERA KUTI HO.NO.319GANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9826044672','',0),
('S/174','NIRMALA JI JAYANTH','ADHAR SHILA 33MITRA NIWAS ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/175','NIRMLA JI BHATNAGAR','E/12 ANAND COLONY','RATLAM','MADHYA PRADESH','07412  24118','','','','0','CR','','0','','',0),
('S/176','NAMRATA JI GEORGE','358,PNT COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/177','ANAND STEEL','NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/178','ARCHANA JI PANDEY','4/1/154,SAJJAN VIHAR COLONYROAD NO.5BARBAD ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9644532447','',0),
('S/179','N.SINGH','180,KASTURBA NAGARMAIN ROAD','RATLAM','MADHYA PRADESH','07412 263082','','','','0','CR','','0','','',0),
('S/18','BAHADUR SINGH JI','18/2,LOK KALAYAN NAGARNIRALA NAGAR,RAJGARH','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425356032','',0),
('S/180','SANGEETA JI MAALPAANI','SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425195317','',0),
('S/181','GUJRAT SWEATS','NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/182','NIKITA JI SHARMA C/O ASHA SHARMA','14/2 VIDHYA NIWAS T.I.T ROADBEHIND JAIN DHARMSHALA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827666969','',0),
('S/183','SHALINI JI JOHN C/O W.ALEXZENDAR','11,MIG INDIRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424021242','',0),
('S/184','GOPAL JI SHARMA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/185','O.P.CHOUDHRY','B-158 SUNCITY','RATLAM','MADHYA PRADESH','07412-234258','','','','0','CR','','0','9630373352','',0),
('S/186','PRAKASH PRAHLAD JI','6/7,AASHIRWAD MAHALAXMI NAGARNEAR KALIKA MATA TEMPLE','RATLAM','MADHYA PRADESH','07412-221149','','','','0','CR','','0','','',0),
('S/187','B.S.MALVIYA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827209306','',0),
('S/188','MAYA JI YADAV','POLICE LINE DO BATI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/189','PANDEY MADAM','61,RAJSWA COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/19','JAVED MADAM,CONVENT','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/190','PITAMBAR JI ZHA','31,RAJ BHAWANSHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9303277971','',0),
('S/191','SONIYA PACHECO','CONVENT SCHOOLPRIMERY SECTION','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/192','PADMA JI RAO MANGILAL JI RAO','47,NEW BANK COLONYDO BATTIGANESH BHAWAN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9826670603  ( ANUP JI','',0),
('S/193','S.S. BHATI C/O MANNU BHAIYA','DRTM OFFICE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/194','ANJALI JI GLOBAL','C/O SAROJ JI GLOBAL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/195','PRATIMA JI JOSHI','504,MAHESHWARI COMPEXPOWESE  HOUSE ROAD','RATLAM','MADHYA PRADESH','07412 405295','','','','0','CR','','0','9926010701,9977263649','',0),
('S/196','PURNIMA JI FHEGDHE','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/197','JYOTI JAIN BOISAR','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/198','ANKITA SINGH C/O CONNI MADAM','52,T.I.TLAXAMANPURA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7879177175','',0),
('S/199','SHIKHA JI  KHANDELWAL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/2','GAYATRI JI NAGAR C/O SEEMA JI','GURU KRIPAINDIRA NAGAR','RATLAM','MADHYA PRADESH','9893312724','','','','0','CR','','0','9893312724','',0),
('S/20','DR.VINEETA JI MALVIYA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/200','DEV N SAQRKAR','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/201','PRAKASH CHANDRAWATN C/O RAJESH MATHUR','871/A ROAD NO.12','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8085439442,9907609442','',0),
('S/202','ANUBHAV JI UPADHAYA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/203','IMDAD BHAI','29/4 ASIYANA LOKENDRA COMPOUNDBANK COLONY','RATLAM','MADHYA PRADESH','07412 231672','','','','0','CR','','0','','',0),
('S/204','SHRIVASTAV MADAM','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9826088535','',0),
('S/205','PRAKASH JI DUBEY','MADHURAM 8/7  SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/206','ST.JOSEPHS CONVENT SCHOOL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/207','PANTH MADAM BANK COLONY','','RATLAM','MADHYA PRADESH','07412 222778','','','','0','CR','','0','','',0),
('S/208','PUSHPA JI KAPOOR','78,SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/209','YUNUS BHAI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9691266633','',0),
('S/21','H.K. HOLKAR','SHRI MANTION BUILDING,CIRCUIT HOUSE,POWER HOUSE ROAD','RATLAM','MADHYA PRADESH','507254','','','','0','CR','','0','','',0),
('S/210','TRILOTMA JI SHARMA','114,VASANT VIHARKASTURBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/211','G.L.PRAJAPAT','30,MAHALAXMI NAGAR','RATLAM','MADHYA PRADESH','07412 236761','','','','0','CR','','0','','',0),
('S/212','PRAKASH JI PANT SHIVPUR HOUSE','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/213','GAYATRI JI PANT','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/214','SUNIL JI PARADKAR','24 JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827616200','',0),
('S/215','PANDIT MADAM','10,SUYOG BHAWAN NR.RAJPUT BORDINGSHASTRI  NAGAR','RATLAM','MADHYA PRADESH','07412 243178','','','','0','CR','','0','9827030178,8989527327','',0),
('S/216','DEEPA JI PANWAR(DRM OFFICE)','865/A NEW RAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9993338207','',0),
('S/217','DEEPAK JI C/O KIRAN JI PRABHA','55,SAHAKARI BANK COLONY SUNDAR VANOPP.SUMAGAL GARDEN','RATLAM','MADHYA PRADESH','07412 263474','','','','0','CR','','0','9893108595','',0),
('S/218','P.S.GUPTE','M-57,JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9617636309','',0),
('S/219','P.S.YADAV','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/22','MERCHANT AUNTY','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/220','ANJU JI BHURIYA','C/O VIMLA JI KATAREY22,M.B.NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9685358678','',0),
('S/221','PREM NAYARN JI CAR PENTER','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425355394','',0),
('S/222','RAMESH JI CHOUHAN','138,GANDHI NAGAR','RATLAM','MADHYA PRADESH','07412 261118','','','','0','CR','','0','9752492614','',0),
('S/223','K.R.GOPI','B-54 VINOBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7566134044','',0),
('S/224','ASHA JI PANDEY','108,RAMDAWRA BUILDINGSTATION ROAD','RATLAM','MADHYA PRADESH','07412 239017','','','','0','CR','','0','','',0),
('S/225','ANJU JI VERMA','381,VERMA SADANPNT COLONY,BEHIND KALALI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/226','KALPNA JI SHASTRI','B/17,VINOBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7879405473','',0),
('S/227','KIRTI JI SHARMA','C/O TRILOTMA JI DESAI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/23','RAJESH UMAPRASAD JI','28/A,INDIRANAGAR,OPP. MAHADEV MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9098205742/9685383057','',0),
('S/230','BHAVNA JI SINGH','520,KASTURBA NAGAR','RATLAM','MADHYA PRADESH','07412 264333','','','','0','CR','','0','','',0),
('S/231','RACHANA JI CHANDRAWAT','GAFFAR BHAI DUDH WALE KE PAASSHAKTI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827533848','',0),
('S/232','RAMACHEWAR JI YADAV','Q.NO.G/3,CHAMBAL COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827664022','',0),
('S/233','ARTI JI BHATNAGAR','C/O NIRMALA JI BHATNAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/234','JACLIN MADAM','CONVENT SCHOOLC/O NASREEN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/235','RAJKUMAR JI VINODIYA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/236','ANMOL BHAI DRESS WALE','CHAMBER OF COMMERCE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/237','HARSH JI PANT','18,MAHESH NAGARRAJGARH','RATLAM','MADHYA PRADESH','07412 278178','','','','0','CR','','0','9300390529','',0),
('S/238','PAVECHA JI','C/O RAJESJ JI VYAS ','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/239','TARA JI PANT SHIVPUR HOUSE','DO BATTI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/24','POONAM JI NAGAWAT','NEAR CIRCUIT HOUSEPOWE HOUSE ROAD','RATLAM','MADHYA PRADESH','231311','','','','0','CR','','0','8959608320','',0),
('S/240','SANDHYA JI YADAV C/O VANDANA JI HERRY','52,MAHAVEER NAGAR','RATLAM','MADHYA PRADESH','07412 225564','','','','0','CR','','0','9098669810','',0),
('S/241','RAJESH JI YADAV','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/242','RAKESH JI DHAWAN','135,PRATAP NAGAR CENTRAL EXCICE QWT.','RATLAM','MADHYA PRADESH','07412 267002','','','','0','CR','','0','','',0),
('S/243','RAJESH JI RATHI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9422909684','',0),
('S/244','RAMSHARAN JI PAL','BLOCK NO.2622 MOHAN NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/245','SANJAY JI RAMGOPAL JI RAWAT','HO.NO.87 P.N.T COLONYROAD NO.3 LAXMANPURA','RATLAM','MADHYA PRADESH','07412 223672','','','','0','CR','','0','','',0),
('S/246','RAIES JI C/O DR.SAIYYAD JI','62 NEW COLONYKAAJI PURA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9977965096','',0),
('S/247','PRAKASH JI SUBEDAR','SHRI GURU KRIPA 115,SHASTRI NAGARNEAR HARSH APRTMENT','RATLAM','MADHYA PRADESH','07412 239479','','','','0','CR','','0','9039644752','',0),
('S/248','DARVESH KIRANA STORE','NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/249','SUMITRA JI SHARMA','A/4 POONAM VIHARNEAR INDRALOK NAGAR','RATLAM','MADHYA PRADESH','07412 236748','','','','0','CR','','0','8989527755','',0),
('S/25','NIKTEY MADAM','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/250','RAMESH JI RETREKAR','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/251','REKHA JI RATHORE','254,GANDHI NAGAR ','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/252','RAKSHA MADAM','JAORA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8982519223,9977298223','',0),
('S/253','RAMESH JI SOURASHTRY','B-85 ALKAPURI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8989705175,9926805760','',0),
('S/254','ARUN JI LAL','C/O JAGDISH JI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/255','RAJIYA JI','34,LAXMAN PURA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9977167313','',0),
('S/256','RADIO PALACE','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/258','DIPTI JI DAVE','11/9SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9993465856','',0),
('S/259','DR.MANJU SINGH','SHASTRI NAGAROPP.AJANTA PALACE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/26','ASHA CHANDWAN','SINDHI COLONY,HOUSE NO.40,COMMECE COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827013182','',0),
('S/260','LATA JI KETHWAS C/O PARWATI JI','HO.NO.93 P.N.T COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9098522938','',0),
('S/261','RFAMESH JI PATHAK','37,RAJSWAS COLONY','RATLAM','MADHYA PRADESH','07412 243653 9407428764','','','','0','CR','','0','','',0),
('S/262','USHA JI KARANDIKAR','3,JAWAHAR NAGARJANTA COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/263','RAMABU JI SHARMA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/264','MADANLAL JI DAABI','NEAR BANK OF INDIANAMLI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/265','JARINA JI KHAN','W/O ANIS JI KHAN9,RASMANDHALDHAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9669172510','',0),
('S/266','RATNA JI PARKHEY','165,INDRALOK NAGAR(WEST)','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8827437366','',0),
('S/267','R.S.MEENA JI','HO.NO.24,ANMOL NAGARGANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425195468','',0),
('S/268','RAMESH JI KARDE ( C/O RAKSHA MADAM','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9300702358','',0),
('S/269','SURJIT SUIT COLLECTION','SUBHASH MARGNEAR VORA MASJID','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9009919508','',0),
('S/27','INDU DEVI SINHA D.S.O.','208,APPARTMENT.AJANTA TALKIES ROAD','RATLAM','MADHYA PRADESH','241067','','','','0','CR','','0','','',0),
('S/270','R.BARLOW JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8989606280','',0),
('S/271','RINKOO JAIN','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/272','R.ROERTS (REBBECA JI)','INDIRA NAGARNEAR RAJPUT HEALTH  CLUB','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/273','NARENDRA JI CHOPRA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/274','PRITI JI AJMERA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/275','R.T.AKHANDEY JI','25-7/B ALAKAPURI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8461935720','',0),
('S/276','KIRAN JI YADAV C/O SARASWATI JI DHIMAN','258 MOHAN NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9575613346','',0),
('S/277','RAJENDRA JI SAXENA','24 MIG DEVRA DEV NARAYAN NAGAR','RATLAM','MADHYA PRADESH','07412 260781','','','','0','CR','','0','9977524007','',0),
('S/278','MANJU JI MAIYDA C/O SARSWATI JI  DHIMAN','B-81,DINDAYAL NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9669661498','',0),
('S/279','RAMESH JI GOYAL COLOR LAB','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/28','RANGOLI COLLECTION 2013-14','KASTURBA NAGAR CORNERRAM MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/280','MEHARBAN SINGHYADAV JI','POLICE COLONYDO BATI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9907873547','',0),
('S/281','R.L.MEENA JI (I.T)','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827503988','',0),
('S/282','SEEMA JI UPADHAYAY','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/283','HEMANT JI MISHRA','HO.NO.7 MIG INDIRA NAGAR','RATLAM','MADHYA PRADESH','07412 235777','','','','0','CR','','0','9827743150','',0),
('S/284','SAROJ JI GLOBAL','FLAT NO.16-17SHASTRI NAGARPARIJAT APARTMENT','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9300474964','',0),
('S/285','SHAKUR AHMED BHAI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/286','SATISH JI RINGE','HASAN ALI BUILDINGDAT KI PUL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9669900640','',0),
('S/287','SANTOSH DEVI PRAMODKUMAR JI','16/13,GOVT.KARAMCHARI COLONYFRUNT OF RAMDWARAKALIKA MATA MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/288','KAVITA JI GUJRATI SCHOOL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/289','SUBALA JI CHOUREY','26/06,RETIRED COLONYGANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/29','ISWARLAL JI BEDWAL','HIG C/11 VINOBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/290','SUNANDA NJI','HO.NO.12401 ROAD NO.5NEW RAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9754101206','',0),
('S/291','SMT. SURKHA RANAWAT C/O DEEPA JI','40,MEHTA JI KA VASTHAWARIYA BAZAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8435174039','',0),
('S/292','RAJ OPTICAL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/293','SATISH JI KHIMLA','HO.NO.607,KATJU NAGARNEAR JAIN GIRLS SCHOOL','RATLAM','MADHYA PRADESH','07412 233082','','','','0','CR','','0','9406686994','',0),
('S/294','DURGA JI C/O SUSHMA JI TYAGI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/295','MAYUR JI VYAS','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424592361','',0),
('S/296','SADHNA JI OZHA','7,PRATAP NAGAR COLONY','RATLAM','MADHYA PRADESH','07412 239772','','','','0','CR','','0','9826290103','',0),
('S/297','SHAITAN SINGH JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/298','ASHA JI VOHRA C/O SANTOSH DEVI','C/O SANTOSH DEVI PRAMODKUMAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/299','GAJENDRA SINGH JI C/O LALAN SINGH JI','HO.NO.13 GLOBUS TOWNSHIP','RATLAM','MADHYA PRADESH','07412 222096','','','','0','CR','','0','9685588337','',0),
('S/3','AMEETA JI BOTHRA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/30','I.D. JOSHI','226,BEHIND MIRA KUTIGANDHI NAGAR/GRAM VATPYAR,POST.LAKHANI DIS-BAGESHWAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425195448,9759290189,9917696078','',0),
('S/300','SUDHA JI PARASAR','M-8 DINDAYAL NAGAR','RATLAM','MADHYA PRADESH','07412 266201','','','','0','CR','','0','','',0),
('S/301','KIRTI JI BAGHEL C/O H.K.HOLKER','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/302','ASHOK JI DODIYA','','RATLAM','MADHYA PRADESH','','','','','17462','DR','','0','','',0),
('S/303','SAPNA JI RAJAWAT','NEW RAILWAY COLONYROAD NO.12 HO.NO.871/BD.S.OFFICE ACCOUNT','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8305272958','',0),
('S/304','SUSHILA JI GERA','SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/305','RANGOLI COLLECTION 2014-15','KASTURBA NAGAR CORNER','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9479425735,9981251888','',0),
('S/306','R.K.RATHORE T.C.','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/307','SAVITA JI THAKKAR','HO.NO.14 GANESHWARI T.I.T ROAD SHIV MANDIR KI GALI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9098333193','',0),
('S/308','DILIP JI SHARMA','MJ-44 JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','07412 244370','','','','0','CR','','0','9752495073','',0),
('S/309','BABULAL JI SHARMA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9098087137','',0),
('S/31','BACHALAL JI SHAH','88,GLOBUS TOWNSHIP','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7354360813','',0),
('S/310','ASHA JI KACHAWAH','J/7 ,JANTA COLONYJAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9907365506','',0),
('S/311','HASRH JI (GOLU)','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/312','PUSHPA JI PANDYA C/O O.P CHOUDHARY','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9406685865','',0),
('S/313','ANJALI JI JOSHI','11/4VIKRAM NAGAR,MHOW ROAD','RATLAM','MADHYA PRADESH','07412 267448','','','','0','CR','','0','','',0),
('S/314','RAMESH JI SHARMA C/O DUBEY JI','OLD RAILWAY COLONYRAOD NO.7','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/315','SEEMA JI BHIORKAR','HO.NO.68 PRABHU KUNJMAHALAXMI NAGAR','RATLAM','MADHYA PRADESH','07412 243024','','','','0','CR','','0','','',0),
('S/316','SANDHYA  JI BHORKAR','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/317','DR.SAIYYAD JI','95-A,SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893544130,9303131436','',0),
('S/318','SARVANAND BAZAR','NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/319','BALAJI NAMKEEN','NEW ROAD OPP.GUJRATI SCHOOL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/32','G.L. MEENA (RAILWAY)','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/320','RAMSINGH JI PATIL','C/O LALCHAND JI DUBEYRAOD NO.7,HO.NO.620CRAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7828428166','',0),
('S/321','MANISH ANAND','C/O ASHA JI SHARMAEIX 123 JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9300963347','',0),
('S/322','JAYDEEP SINGH GOYAL C/O KAHNYA JI','C/O KAHNYALAL JI GOYAL204/6 ROAD NO.5KASTURABA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7803810832','',0),
('S/323','SUKHDEV JI YADAV','PANCHED RUGHNATHGARH','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/324','MUKESH KUMAR JI C/O PITAMBAR JI','HO NO.31RAJ BHAVANSHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9977060841','',0),
('S/325','SURESH JI JOSHI','38 NEW ROAD','RATLAM','MADHYA PRADESH','07412 232369','','','','0','CR','','0','','',0),
('S/326','O.P.SONI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/327','SARASWATI JI DHIMAN','HO.NO.176 PNT COLONYLAXMAN PURA KE PASS','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/328','SUSHMA JI TYAGI','POLICE LINE   DO BATTI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/329','RANJANARASHMI JI','C/O AMIEETA JI TIWAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/33','JAGRATI JI SAXENA','94,ANUBHUTI BHAWAN SHRIMALIVAS','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425355157','',0),
('S/330','SAYRABANO JI','643/D ROAD NO.6OLD RAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/331','SANJAY JI RTO OFFICE','SHASTRI NAGAROPP.SAI MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/332','KIRAN BHAI','STANDAR CYCAL SARVICE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/333','K.L.KORI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/334','SHANTHA KRISHANAN','D.S.OFFICE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/335','ANITA JI C/O JAVED MADAM','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/336','ASHOK JI BORGE','108,SHASTRI NAGARNEAR DR.PREM SINGH THAKUR','RATLAM','MADHYA PRADESH','07412 233090','','','','0','CR','','0','8435513388','',0),
('S/337','SHARDA DEVI BORASI','A-71,JAWAHAR NAGARCHAR BATTI CHOURAHA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9406635774','',0),
('S/338','BHARAT GLASS HOUSE','LOKENDRA TALKIZE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/339','BHAWAR LAL JI THAKKAR','NAGAR NIGAM','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/34','JAGDISH JI MEENA','SAILANA YARD,NEAR MASJID','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9752492343','',0),
('S/340','SHOBHA JI SINGH','247-B ALKAPURI','RATLAM','MADHYA PRADESH','0712 238385','','','','0','CR','','0','','',0),
('S/341','SHIVLAL JI (PAPERWALE)','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827355370','',0),
('S/342','SHAGUFTA JI KHAN','97,SAMTA NAGARANAND COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9753264614','',0),
('S/343','RAJESH JI MATHUR','124/A ROAD NO.11OLD RAICOLONYLWAY ','RATLAM','MADHYA PRADESH','07412 44513','','','','0','CR','','0','9752492448','',0),
('S/344','SHRIKANTH JI DIVE','SAHAYOG,POWER HOUSE ROADNR,CIRCUIT HOUSE','RATLAM','MADHYA PRADESH','236999','','','','0','CR','','0','9910045168','',0),
('S/345','S.YADAV (SISTER)','12/2,  GANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9300224069','',0),
('S/346','S.S. PETER','4502,RETIRED COLONYNR.MORNING STAR SCHOOL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/347','PRERNA PANDIT C/O NARENDRA JI RAJGURU','A-116 SUNDARSHAN APARTMENTJAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893049949','',0),
('S/348','S.K.ALEXZENDER JI','51, INDIRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/349','S.L.PRADHAN JI','2/11, TAKSHILA APPT.SHASTRI NAGAR ','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/35','JAMNA BAI','MIRAKUTI BEHIND KALALI215,GANESH BHAWANGANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/350','TRILLOTMA JI DESAI','M-47 VIKRAM NAGARBEHIND COSMAS HOTEL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9479659804,7898508253','',0),
('S/351','USHA JI VYAS','HO.NO.49 VEDVYAS COLONY','RATLAM','MADHYA PRADESH','07412 231610','','','','0','CR','','0','','',0),
('S/352','USHA JI SAXENA','KIRAN TALKIES ROADNEAR PURNESHWAR TEMPLE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/353','USHA JI CHOUHAN','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/354','VARSHA JI KARANDIKAR','37,VIDHA VIHARKASTURBA NAGAR','RATLAM','MADHYA PRADESH','07412 403639','','','','0','CR','','0','','',0),
('S/355','VIJAY JI CHOUHAN','GANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827623059','',0),
('S/356','VIJAYA JI CHOUHAN','GANGOTRI APARTMENTSHAHSTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827623059','',0),
('S/357','VIMLA DEVI KATAREY','B-140M.B.NAGAR','RATLAM','MADHYA PRADESH','07412 264555','','','','0','CR','','0','','',0),
('S/358','MOHAN JI BARAILE','DR.DEVISINGH COLONYOPP.COURT','RATLAM','MADHYA PRADESH','07412 220044','','','','0','CR','','0','','',0),
('S/359','VIJAY JI VYAS','120,DEVRA DEVNARAYAN COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424809543,9993201940','',0),
('S/36','H.S.VYAS','17,LOK KALAYAN NAGARRAJGARH','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9993476671','',0),
('S/360','VIJAYLAXMI JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/361','VANDANA JI HARRY','118,ESTHER VINA POWER HOUSER ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/362','SHAILENDRA SINGH RAJAWAT','1263/B RAILWAY NURSE COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','8982544579','',0),
('S/363','JAGDISH JI SHRIVASTAV','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425187044','',0),
('S/364','JEEVAN SINGH JI JHALA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7509218491','',0),
('S/365','VIMLA SISTER','NEW INDRALOK NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9755601716','',0),
('S/366','W.ALEXZENDER JI','HO.NO.11 MIG INDIRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/367','NIRMALA JI TRIVEDI','C/O SUSHILA JI NAAHAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/368','VIVNITA JI JAKHWAL C/O DUBEY JI','OLD RAILWAY COLONY ROAD 7HO.NO.51/A','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','7067499427','',0),
('S/369','SHER SINGH JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/37','RAJNATH JI YADAV (D.S. YADAV)','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/370','IRFAN KHAN','IRFAN  AHMED KHAN STENOGRAPHUR150,RAJENDRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/371','SANJAY JI RATLAM PUBLIC SCHOOL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9977004785','',0),
('S/372','SURY JI DAVE','29,PALACE ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/373','REKHA JI NIGAM','22,AJANTA TAKIES ROAD OPP.SAJJAN PRABHA ','RATLAM','MADHYA PRADESH','07412 234343','','','','0','CR','','0','9827321030','',0),
('S/374','F.THOMAS','329,GANDHI NAGAR','RATLAM','MADHYA PRADESH','07412 260483','','','','0','CR','','0','','',0),
('S/375','L.S. GUPTE JI','12,MIG, INDIRA NAGAR','RATLAM','MADHYA PRADESH','260400','','','','0','CR','','0','9424021185','',0),
('S/376','DEEPAK JI RAJ C/O. DR. BAJPAI','212,ABHILASHA APARTMENTVEDVYAS COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9009185536','',0),
('S/377','HEMLATA JI UPADHAY','DISEAL SHAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/378','RANJANA JI DIVEDI C/O. DUBEY MD','C/O PUSHPA JU DUBEY,1/13 RETIRED COLONY,NEAR CENTRAL BANK','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425989778','',0),
('S/379','RAM PRAKASH BHASKAR','POLICE COLONY,DO BATTI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/38','ABDUL AZIZ KHAN','412,GANDHI NAGAR,BEHIND MEERA KUTI','RATLAM','MADHYA PRADESH','400773','','','','0','CR','','0','9826650055,9009444455','',0),
('S/380','SALIM KHAN','BARBODNA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/381','SIRASO MADAM','A/367,INDIRA NAGAR (EAST)NR.BEEMA HOSPITAL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9752492527,9406635191','',0),
('S/382','S.L. VANA','B/5,SUNDAR VAN,NEAR TELEPHONE NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/383','DR. H.J. VYANWAHARE','VASANT VIHAR,KASTURBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/384','KUSUM JI YATI','D-20,ANAND COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/385','BABITA JI NIGAM C/O SARASWATI JI','30-A-NARBEL PALACE,MHOW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827535375','',0),
('S/386','RADHAVALLABH JI NATANI','NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/387','BHARTI JI RAWAL DSO','M-86,JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','220797','','','','0','CR','','0','','',0),
('S/388','VIJENDRA KUMAR JI SAXENA','MG.28,OPP.INDIAN OIL CORPORATIONJAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9406634969','',0),
('S/389','VAJERAM JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/390','GAYA PRASAD JI SHARMA','JAVAHAR NAGARE-39','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893643226','',0),
('S/391','DR.C.V.SINGH JI','ROAD NO. 5KASTURBA NAGAROPP. GARDEN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827312988','',0),
('S/392','SHEETAL PRASAD JI PAL','HOUSE NO.57,ROAD NO.1,KASTURBA NAGAR,','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827319909','',0),
('S/393','A.K. BALUJA','HOUSE NO.935,ROAD NO.3NEW RAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425354727,8054250383','',0),
('S/394','JYOTI RAOUT','B-15,VINOBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9926087655','',0),
('S/395','S.M. KHAN','HOUSE NO.10,RAM BAGH','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9907251451','',0),
('S/396','VIRENDRA KUMAR JI VARMA','35/17,DR.DEVI SINGHCOLONY,NEAR COURT MANDIRWALI GALI','RATLAM','MADHYA PRADESH','409945','','','','0','CR','','0','8085929080','',0),
('S/397','A.RAO C/O R.ARLOW','161,INDRALOK NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425328967','',0),
('S/398','RATAN SINGH JI KAUSHIK','141,SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9301678833','',0),
('S/399','KHAN SHAEB COOL CENTRE','97,SAMTA NAGARANAND COLONY,','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827065677,9179788569','',0),
('S/4','SANJAY JI UPADYAY','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/40','ANITA CHOUHAN','7,KANCHAN COLONYVED VYAS COLONY','RATLAM','MADHYA PRADESH','240816','','','','0','CR','','0','','',0),
('S/400','KANYALAL JI GOYAL','GALI NO.5KASTURBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9329212711','',0),
('S/401','D.R. DAHALE','HOUSE NO.44,VARDAN NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','07412260359','',0),
('S/402','SAJJAN BAI C/O MANJEET KOUR','KALU SINGH JI DAVARJAIL PARISAR,POLICE LINEJAIL ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/403','MULE BAHEN JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/404','ASHWIN JI ALEXZENDER','C/O S.K. ALEXZENDER','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425990311,9589120583','',0),
('S/405','ASHISH JI PAL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9691985121','',0),
('S/406','AKSHAY KUMAR JI OJHA','23-C,ANAND COLONY','RATLAM','MADHYA PRADESH','267520','','','','0','CR','','0','','',0),
('S/407','ABHAY KUMAR JI VOHRA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827261983','',0),
('S/408','ANUPAM','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/409','SALIL JI CHOUDHARY','1,CHANDANI CHOWK','RATLAM','MADHYA PRADESH','231778','','','','0','CR','','0','','',0),
('S/41','SHANTA DEVI THAKUR','FLAT NO.15AAVISHKAR APARTMENTT.I.T ROAD ','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9039239293','',0),
('S/410','SHUKLA MADAM','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/411','SHIVANI DEEPAK KUMARJI PANTH','BANK COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/412','MANISHA WILLIAM','27/3 KARJU NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9754045517','',0),
('S/413','S.R. DUBEY JI GUJRATI SCHOOL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/414','Y.K. MISHRA (PROFESER)','HOUSE NO.8/B,RAMBAG NEAR RITUVAN','RATLAM','MADHYA PRADESH','236032','','','','0','CR','','0','','',0),
('S/415','APPROVAL SALES SAREE 2016-17','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/416','APPROVAL CUSTOMER A/C','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/417','BHAGWANDAS JI','LIG 23JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9406634541,9424609992','',0),
('S/418','SHAILU BHAI PRO VISION','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/419','BALRAM JI MAHESHWARI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827869754','',0),
('S/42','JUBEDA BAHAN JI SHEIKH A.GANI','8931/B,NEW RAILWAY COLONYROAD NO.09','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/420','JUBEDA JI QURESHI','TIT  BUILDINGINDIRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893849944,8878019314','',0),
('S/421','BASANT KUMAR JI SHAKTAWAT','58,SHREEMALI VYASNEAR PURNESHWAR MANDIR','RATLAM','MADHYA PRADESH','07412 200630','','','','0','CR','','0','','',0),
('S/422','BABU BHAI GOPAL','KARTIK NIWAS HO.NO.3MAHESH NAGAR','RATLAM','MADHYA PRADESH','07412  222626','','','','0','CR','','0','','',0),
('S/423','DHAMANI JI','CHGANDNI CHOWK','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/424','BHARTI JI TIWARI','SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/425','SATYANARAYAN JI BAHETI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/426','USHA JI NIMBEY','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/427','P.B.BHORKAR JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/428','BRIJBALA JI VYAS','SHRIMALI JI VYASOPP.WALL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/429','BASANTI JI GUJRATI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827890455','',0),
('S/43','SHRILAL JI BORASI','HOUSE NO.104,MAHAVEERNAGAR OPP.GAS GODAM','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/430','GEETA JI SHUKLA  C/O BARLOW MADAM','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/431','RAMSAGAR JI YADAV','C/O PARWATI JI MISHRAD.S.OFFICE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/432','SHOBHA JI SEHAGUL','C/O A.K.BALUJA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/433','ROBERT MADAM(GIRLS COLLEGE)','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/434','RINKOO JI LOBO','HO NO.44VARDAN NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893738487','',0),
('S/435','KUNDI JI CHABRIYA','GUJARTI SCOOL NEW ROAD','RATLAM','MADHYA PRADESH','234035','','','','0','CR','','0','','',0),
('S/436','SUDHA JI SALEKAR','7,MANHAR HOTEL KI GALLISTATION ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9993466265,9009970229','',0),
('S/437','YARDEY MADAM','OPP.LOKENDRA CINEMA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/438','B.P.TIWARI','154,SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','243393','',0),
('S/439','B.N.UDIYA','A-75 ALKA PURI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893085802','',0),
('S/44','SONIYA JI PATWARI','C/O ANITA JI CHOUHAN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/440','CHITRA JI SISODIYA','381/1 TELEPHONE NAGARVIRIYA K HEDI ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','263163','',0),
('S/441','ASHA JI PUNJABI','SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/442','KAILASH JI PHATHAK','EX-32,JAWAHAR MARG','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425356491','',0),
('S/443','CHANDANA JI JOSHI RAJENDRA JI','34,SHAKTI SADAN SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/444','HARI KRISHNA JI BARGE','C/O. RAMESH JI CHOUHAN112/T OLD RAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','09893012827','',0),
('S/445','KAKKU BHAI','DR. SAIYED SAB','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/446','C.P. VYAS','TAPASYA MDX-19 RATANPURI COLONYSAILANA ROADPPP','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','234090,9752492940','',0),
('S/447','DINESH JI KOSHIK','5,SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/448','USHA JI TIWARI','27,LOTUS CITY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9098350001/9407108096','',0),
('S/449','DINESH JI PATEL','GURUKIPA MITRA NIWAS COLONYBEHIND CONVENT SCHOOL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/45','JAGDISH PRASAD JI BADRILAL JI','B-47,DINDAYAL NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827566515,9827644136','',0),
('S/450','MANGLESHWARI JI JOSHI','103,GULMOHAR COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9770360814','',0),
('S/451','DASHRATH JI SHOLULE','33,MAHESHNAGAR CHRISTAN COLONYNEAR ANAND VIHAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','409120','',0),
('S/452','ADWERD JI SINGH','HO NO.597/AKASTURBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/453','DILIP JI KASTURE','192,INDARLOK NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','224419','',0),
('S/454','VIPIN JI PAL C/O. RAJKUMAR JI','C/O. RAJKUMAR JI VINODIYA 1543/B NEW RAILWAY COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9907253750','',0),
('S/455','DINESH DAIRY','HOUSE NO. 39, FREEGANJ ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827733499,9827085162','',0),
('S/456','SUJJANMAL JI RATANPURA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/457','AMIT JI SHARMA C/O S.C. SHARMA','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/458','YASHPAL JI JOSHI','HOUSE NO.161,GULMOHAR COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9755368886','',0),
('S/459','B.S.SAXENA JI','945/A, ROAD NO.2NEW RAILWAY COLONY','RATLAM','MADHYA PRADESH','233973','','','','0','CR','','0','','',0),
('S/46','PRATIBHA JI OJHA','12,DR.DEVISINGH COLONYSAKET BHAWAN','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9229435539,9300715052','',0),
('S/460','SHYAMA JI CHOUHAN','L-35,JAWAHAR NAGARNR.MANSA MATA MANDIR','RATLAM','MADHYA PRADESH','224324','','','','0','CR','','0','7898206007','',0),
('S/461','LAXMAN DAS DHARAM DAS','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/462','PRABHU JI PRASAD','INDRLOK NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/463','PREM PAL SINGH JI CHOUDHARY','DO BATTI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/464','UMA JI RATHI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/465','LEELA JI DAVAR','20,M.B.NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9826999379','',0),
('S/466','D.S.YADAV','90 TIT QUARTER,LAXMANPURA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424020008','',0),
('S/467','D.R. JADHAV JI','8,KARMACHARI COLONYNR.KALIKA MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/469','R.K.TRIPATHI JI','C/O.JADISH JI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/47','WALTER SISTER','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425937266','',0),
('S/470','FELIX NINAMA','522,KASTURBA NAGARROAD NO.-5','RATLAM','MADHYA PRADESH','263833','','','','0','CR','','0','','',0),
('S/471','GOVIND LAL JI SHARMA','28A INDRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424883853','',0),
('S/472','GIRISH JI GORH','D/11 POONAM VIHAR COLONY','RATLAM','MADHYA PRADESH','240485','','','','0','CR','','0','','',0),
('S/473','GOPI CHAND JI (HEADSAAB)','KASTURBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9301132082','',0),
('S/474','PUSHPA JI DUBEY','HO.NO.168P.N.T.COLONY','AHEMDABAD','GUJARAT','','','','','0','CR','','0','9890494935','',0),
('S/475','MANOHAR JI KHABIYA','','BADNAGAR','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/476','DINESH JI TIWARI','S.NO.111,UPVAN D VYAS COLONY','BADNAGAR','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/477','PUKHRAJ JI PAREKH','','DHODHAR','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/478','KAMINI JI JOSHI','2583,E-SECTOR,SUDAMA NAGAR','INDORE','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/479','RAJNEESH JI NIGAM','','INDORE','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/48','ISHWAR LAL JI BEDWAL','HIG C/11VINOBA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/480','HARISHCHANDRA JI SHARMA','202,SCHEME NO.98SAMVAD NAGAR','INDORE','MADHYA PRADESH','2400054','','','','0','CR','','0','','',0),
('S/482','NIKHIL DESHPANDE','17,CLASICPURNIMA PARK  OPP.HOTAL REDISSIONRING ROAD','INDORE','MADHYA PRADESH','0731 2552232','','','','0','CR','','0','9993433760','',0),
('S/483','PUKHRAJ JI MALLU','','INDORE','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/484','MAHIPAL SINGH JI SISODIYA','3 IN MIDAS GOLD,BACK RIDHI SIDHI RIJENCYKHAJRANA ROAD','INDORE','MADHYA PRADESH','0731-2907896','','','','0','CR','','0','9977090336','',0),
('S/485','PARUL BAHEN JI INDORE','','INDORE','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/486','BEENA JI BOLIYA','','KHACHROD','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/487','MANJU PATIDAR KHAWASA','BHERULAL JI KHATTA JI MELAWAT','KHAWASA','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/488','FULCHAND JI','','MUMBAI','UTTAR PRADESH','','','','','0','CR','','0','','',0),
('S/489','KAILASH CHANDRA JI GWALIYARI','VAIDRAJ JI KI GALI','SAILANA','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/49','I.D.JOSHI','226,BEHIND MIRA KUTI GANDHI NAGARGRAM VATPYAR POST;LAKHNI DIS.BAGHESHWAR','RATLAM','MADHYA PRADESH','9425195448','','','','0','CR','','0','9917696078,9759290189','',0),
('S/490','SANGEETA JI CHAREL MADAM','','SAILANA','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/491','CHANDRA JI VAGHELA','HOUSE NO.487,SAILANA RAILWAY COLONY','SAILANA','MADHYA PRADESH','223786','','','','0','CR','','0','','',0),
('S/492','VARSHA MODI','JACK AND JILL RING ROAD','SURAT','GUJARAT','','','','','0','CR','','0','','',0),
('S/493','ANITA JI BHATT C/O.. A.L.DAINEL','GANESH MANDIR ,KHAJRANA ROAD','INDORE','MADHYA PRADESH','','','','','0','CR','','0','9826094442','',0),
('S/495','APPROVAL SAREE & SUITS','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/496','TIMNNA JI','','RATLAM','MADHYA PRADESH','','','','','5099','DR','','0','','',0),
('S/497','ANJALI JI DALAL','','RATLAM','MADHYA PRADESH','','','','','99','DR','','0','','',0),
('S/498','SWAPNA SUNDARI (SALES)','7,ASHIRWAD COMPLEXKANDIA ROAD','INDORE','null','2595170','','','23APPPS6767J1Z2 ','0','CR','','0','','',0),
('S/5','SEEMA JI JAFAR','FLAT NO.205,ARAVALI APARTMENT','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827445699','',0),
('S/500','CHANDA JI VERMA','C/O PARWATI JI MISHRA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9691135274','',0),
('S/501','MUKUL  JI DALAL','DALAL BROTHERS THOPKHANA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/502','KANCHAN JI DAABI','NEAR BANK OF INDIA NAMLI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','9827337587',0),
('S/503','RAJESH JI VYAS','','RATLAM','MADHYA PRADESH','','','','','19070','DR','','0','','',0),
('S/504','KAHNYALAL JI VYAS','39,SAJJAN VIHAR COLONYBADBAD ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','9755026588',0),
('S/505','MANORAMA JI GEHLOT','6/193,JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','4628','DR','','0','','9425990313',0),
('S/506','MAHESH CHANDRA JI GUPTA','B-64-A DONGE NAGAR CHOURAHA','RATLAM','MADHYA PRADESH','07412-263368','','','','905','DR','','0','','',0),
('S/507','RAMBABU JI SHARMA','DO BATI PULICE LINE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/508','RAMESH JI PATHAK','37 RAJSWAS COLONY','RATLAM','MADHYA PRADESH','243653','','','','3517','DR','','0','','9407428764',0),
('S/509','RENU JI BALCHANDANI','HO NO.72,SINDHI COLONY BIRYAS KHEDI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','7869282233',0),
('S/510','VINITA JI JAKHWAL C/O DUBEY JI','OLD RLY COLONY ROAD NO.7 HO.NO.51/A','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','7067499427',0),
('S/511','RENU JI MICHAEL','922-B,ROADNEW RAILWAY COLONY','RATLAM','MADHYA PRADESH','238964','','','','0','CR','','0','','',0),
('S/512','SANJAY JI GANG','C/O GANG PACKGING','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/513','DR.SATYAPRAKASH R. MISHRA','A/101,HILL PARK CHS LTD.JIVDANI ROADVIRAR','MUMBAI','MAHARASTRA','','','','','0','CR','','0','9860293401','',0),
('S/514','USHA JI ANAND','EX-123 JAWAHAR NAGAR NEAR BY RANJIT HANUMAN MANDIR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','9300963347',0),
('S/515','SUNITA JI CHOUHAN','','RATLAM','MADHYA PRADESH','9406686103','','','','0','CR','','0','','',0),
('S/516','SWAMI SIR','OFFICER COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/517','SUSHMA AGNIHOTRI','M/79,JAWAHAR NAGARC/O BHARTI RAWAL','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827946779','',0),
('S/518','D.S. JOSHI JI','FL.NO. 104 VASANT VIHARKASTURBA NAGAR,','RATLAM','MADHYA PRADESH','263125','','','','0','CR','','0','','',0),
('S/519','R.K.BAGHEL','C/O BABU LAL JI SHARMA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/53','JAGDISH PRASAD JI MEENA','SAILANA YARDNEAR MASJID','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9752492343','',0),
('S/57','DEEPAK JI NAGAWAT','DHANMANDI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/58','ANAND PRASAD JI','AMBER APPARTMENT OPP. DR.SAIYYAD SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425947124','1',0),
('S/59','JYOTSANA JI','362,KASTURBA NAGARROAD NO.5','RATLAM','MADHYA PRADESH','07412  263769','','','','0','CR','','0','9752492105','',0),
('S/6','MADHUKAR BHASHKAR','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/60','PREMVATI JI SHARMA','HOUSE NO.509,ROAD NO.8KASTURBA NAGAR','RATLAM','MADHYA PRADESH','263589','','','','0','CR','','0','','',0),
('S/61','GOPAL JI HANSWAL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/62','CHANDRIKA JI VORA','B/72,ALKAPURI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9993200994','',0),
('S/63','JAGDISH JI DHAMANI C/O KLNINAM','HOUSE NO.1/6,D.R.P LINE','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827531409','',0),
('S/64','S.N. CHOUHAN','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/65','SHYAMLAL JI YADAV','49,PANKAJ VIHAR,AMBIKA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424046681','',0),
('S/66','SANGEETA JI TOMAR','27,CENTRAL PLAZA KARJU NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425355810','',0),
('S/67','C.H. CANARA MADAM','11/A SHRIBHASKAR SMRITIOPPOSITE RAJPUT BOARDING SHASTRI NAGAR','RATLAM','MADHYA PRADESH','233938','','','','0','CR','','0','','',0),
('S/68','KIRAN PRABHA JI','55,SARKAI BANK COLONYSUNDARVAN OPPOSITESUMANGAL GARDEN','RATLAM','MADHYA PRADESH','263474','','','','0','CR','','0','98931085595','',0),
('S/69','RAMASHANKAR JI TRIPATHI','210,JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','242842','','','','0','CR','','0','','',0),
('S/7','GAYATRI JI SHARMA','361,EAST INDRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424048119','',0),
('S/70','KALPANA JI SINGH','MORDEN SCHOOL,SAILANA','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9753027336','',0),
('S/71','NITIN JI MULEY','211,SHRI SADANGANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827733517,9907685326','',0),
('S/72','DR.ARUN JI PUROHIT','SUNCITY SAILANA ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/73','KAMLA BAI JAYASWAL','30,AJANTA TALKIES ROAD','RATLAM','MADHYA PRADESH','07412 239576','','','','0','CR','','0','','',0),
('S/74','KAPILA MADAM','2/54JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/75','ANWAR BHAI DRM OFFICE','10,ASHIYANA APURVA COLONYNIRALA NAGARBARBAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827318824','',0),
('S/76','KANTILAL JI  JOSHI','14,NARENDRA KUTIRGUJRATI COLONYSTATION ROAD','RATLAM','MADHYA PRADESH','07412  223539','','1','','0','CR','','0','','',0),
('S/78','KANCHAN JI VERMA','HOUSE NO.76,ROAD NO.7VIDHA VIHAR (M.B.NAGAR)','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/79','KAILASH CHANDRA JI JHALA','SHIV SADAN NAYA GAOMAHESH NAGAR NEAR RATHI COAT DIPO','RATLAM','MADHYA PRADESH','243213','','','','0','CR','','0','','',0),
('S/8','VISHNU JI MEENA','PUNJAB & SINDH BANK','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/80','PARMAR SAHEB','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/81','SURESH JI PANWAR','HOUSE NO.10,SAAKET APARTMENT,SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9302282158','',0),
('S/82','MAHIPAL SINGH JI DODIYA','140,INDRA NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9424591609,9752495120','',0),
('S/83','USHA JI SHRIVASTAV','64,GURU NANAK BHAWANNEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9753168162','',0),
('S/84','KEVALRAM JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9926187737','',0),
('S/85','SAARIKA JI C/O MANJU BAHEN JI','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/86','SUNITA JI GOYAL','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/87','KAMALDAS JI BAIRAGI','HOUSE NO.51,RAIL NAGAR','RATLAM','MADHYA PRADESH','260423','','','','0','CR','','0','9424591633','',0),
('S/88','SUNIL JI PURANDAR','HOUSE NO.4,MAHAVEER NAGAR','RATLAM','MADHYA PRADESH','07412225792','','','','0','CR','','0','','',0),
('S/89','ABHAY JI VYAS','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/9','ADITI JI DEVESAR','','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/90','HEMANT JI KULKARNI','14,MDX,RATANPURI','RATLAM','MADHYA PRADESH','230002','','','','0','CR','','0','9425926888','',0),
('S/91','AMEETA JI TIWARI','SHASTRI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9425935865','',0),
('S/92','RAJESH JI GOYAL C/O MUKESH JI','208,GANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9893372093','',0),
('S/93','ASHA JI SHARMA','EX-38,JAWAHAR NAGAR','RATLAM','MADHYA PRADESH','244365','','','','0','CR','','0','','',0),
('S/94','K.L. NINAMA','POLICE LINE,DO BATTI','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','','',0),
('S/95','KAMLESH ACHARYA','HOUSE NO.6,PREM VIHARNAJAR BAAG,BANK COLONY','RATLAM','MADHYA PRADESH','223726','','','','0','CR','','0','','',0),
('S/96','DEEPAK JI KOHLI','44/G ROAD NO.5RAILWAY OLD COLONY','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9755399647','',0),
('S/97','SHAMIM SHANAVARAJ JI KHAN','14,GANDHI NAGAR','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9329059864','',0),
('S/98','DR.K..L. PANJABI','104,NEW ROAD','RATLAM','MADHYA PRADESH','','','','','0','CR','','0','9827790294','',0),
('S/99','K.R. SHALUNKEY','HOUSE NO.66,RAJENDRA NAGAR','RATLAM','MADHYA PRADESH','234161','','','','0','CR','','0','','',0);

/*Table structure for table `password` */

DROP TABLE IF EXISTS `password`;

CREATE TABLE `password` (
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `password` */

/*Table structure for table `payment` */

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `sno` varchar(45) NOT NULL,
  `da_te` varchar(45) DEFAULT NULL,
  `by_ledger` longtext,
  `to_ledger` longtext,
  `amo_unt` varchar(45) DEFAULT NULL,
  `narra_tion` longtext,
  `grand_total` varchar(45) DEFAULT NULL,
  `vsn` int(11) unsigned DEFAULT NULL,
  `branchid` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payment` */

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_code` varchar(45) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `IGST` varchar(45) DEFAULT NULL,
  `print_on` varchar(45) DEFAULT NULL,
  `SGST` varchar(45) DEFAULT NULL,
  `CGST` varchar(45) DEFAULT NULL,
  `HSL_CODE` varchar(45) DEFAULT NULL,
  `sales_account` varchar(45) DEFAULT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `iscalculationapplicable` tinyint(1) unsigned DEFAULT NULL,
  `salestopurchase` varchar(45) DEFAULT NULL,
  `isquantityapplicable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`product_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product` */

/*Table structure for table `purchasereturn` */

DROP TABLE IF EXISTS `purchasereturn`;

CREATE TABLE `purchasereturn` (
  `Supplier_id` varchar(45) NOT NULL,
  `Purchase_type` varchar(45) DEFAULT NULL,
  `Invoice_date` varchar(45) DEFAULT NULL,
  `Invoice_no` varchar(45) DEFAULT NULL,
  `Purchaseref_no` varchar(45) DEFAULT NULL,
  `Stock_no` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `qun_tity` varchar(45) DEFAULT NULL,
  `amou_nt` varchar(45) DEFAULT NULL,
  `Product_desc` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `Byforgation` varchar(45) DEFAULT NULL,
  `Produ_ct` varchar(45) DEFAULT NULL,
  `Bill_amt` varchar(45) DEFAULT NULL,
  `dis_count` varchar(45) DEFAULT NULL,
  `amtafter_discount` varchar(45) DEFAULT NULL,
  `tax` varchar(45) DEFAULT NULL,
  `taxamt` varchar(45) DEFAULT NULL,
  `Other_charges` varchar(45) DEFAULT NULL,
  `Net_amt` varchar(45) DEFAULT NULL,
  `vataccount` varchar(45) DEFAULT NULL,
  `vat` varchar(45) DEFAULT NULL,
  `vatamount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `taxaccount` varchar(45) DEFAULT NULL,
  `otherchargesaccount` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `purchasereturn` */

/*Table structure for table `purchasereturndualgst` */

DROP TABLE IF EXISTS `purchasereturndualgst`;

CREATE TABLE `purchasereturndualgst` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `igst_first` varchar(45) DEFAULT NULL,
  `igst_second` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) DEFAULT NULL,
  `sgst_first` varchar(45) DEFAULT NULL,
  `sgst_second` varchar(45) DEFAULT NULL,
  `cgst_first` varchar(45) DEFAULT NULL,
  `cgst_second` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_first_amnt` varchar(45) DEFAULT NULL,
  `total_sgst_first_amnt` varchar(45) DEFAULT NULL,
  `total_cgst_first_amnt` varchar(45) DEFAULT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `cashamount` varchar(255) DEFAULT NULL,
  `cashaccount` varchar(45) DEFAULT NULL,
  `sundrydebtorsamount` varchar(255) DEFAULT NULL,
  `sundrydebtorsaccount` varchar(255) DEFAULT NULL,
  `cardamount` varchar(255) DEFAULT NULL,
  `cardaccount` varchar(255) DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `igst_account` varchar(45) DEFAULT NULL,
  `sgst_account` varchar(45) DEFAULT NULL,
  `cgst_account` varchar(45) DEFAULT NULL,
  `total_igst_second_amnt` varchar(45) DEFAULT NULL,
  `total_sgst_second_amnt` varchar(45) DEFAULT NULL,
  `total_cgst_second_amnt` varchar(45) DEFAULT NULL,
  `freight_amount` varchar(45) DEFAULT NULL,
  `freight_account` varchar(45) DEFAULT NULL,
  `Freight_Gst` varchar(45) DEFAULT NULL,
  `Freight_Gst_Amount` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `purchasereturndualgst` */

/*Table structure for table `purchasereturndualgst1` */

DROP TABLE IF EXISTS `purchasereturndualgst1`;

CREATE TABLE `purchasereturndualgst1` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `igst_first` varchar(45) DEFAULT NULL,
  `igst_second` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) DEFAULT NULL,
  `sgst_first` varchar(45) DEFAULT NULL,
  `sgst_second` varchar(45) DEFAULT NULL,
  `cgst_first` varchar(45) DEFAULT NULL,
  `cgst_second` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_first_amnt` varchar(45) DEFAULT NULL,
  `total_sgst_first_amnt` varchar(45) DEFAULT NULL,
  `total_cgst_first_amnt` varchar(45) DEFAULT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `cashamount` varchar(255) DEFAULT NULL,
  `cashaccount` varchar(45) DEFAULT NULL,
  `sundrydebtorsamount` varchar(255) DEFAULT NULL,
  `sundrydebtorsaccount` varchar(255) DEFAULT NULL,
  `cardamount` varchar(255) DEFAULT NULL,
  `cardaccount` varchar(255) DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `igst_account` varchar(45) DEFAULT NULL,
  `sgst_account` varchar(45) DEFAULT NULL,
  `cgst_account` varchar(45) DEFAULT NULL,
  `total_igst_second_amnt` varchar(45) DEFAULT NULL,
  `total_sgst_second_amnt` varchar(45) DEFAULT NULL,
  `total_cgst_second_amnt` varchar(45) DEFAULT NULL,
  `freight_amount` varchar(45) DEFAULT NULL,
  `freight_account` varchar(45) DEFAULT NULL,
  `Freight_Gst` varchar(45) DEFAULT NULL,
  `Freight_Gst_Amount` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `purchasereturndualgst1` */

/*Table structure for table `queries` */

DROP TABLE IF EXISTS `queries`;

CREATE TABLE `queries` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `queries` */

/*Table structure for table `queries1` */

DROP TABLE IF EXISTS `queries1`;

CREATE TABLE `queries1` (
  `queries` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `queries1` */

/*Table structure for table `ratechange` */

DROP TABLE IF EXISTS `ratechange`;

CREATE TABLE `ratechange` (
  `branch` varchar(45) DEFAULT NULL,
  `stockno` varchar(45) DEFAULT NULL,
  `previousrate` varchar(45) DEFAULT NULL,
  `updatedrate` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ratechange` */

/*Table structure for table `receipt` */

DROP TABLE IF EXISTS `receipt`;

CREATE TABLE `receipt` (
  `s_no` varchar(45) NOT NULL,
  `da_te` varchar(45) DEFAULT NULL,
  `by_ledger` longtext,
  `to_ledger` longtext,
  `amo_unt` varchar(45) DEFAULT NULL,
  `narra_tion` longtext,
  `grand_total` varchar(45) DEFAULT NULL,
  `vsn` int(10) unsigned DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `receipt` */

/*Table structure for table `salesreturn` */

DROP TABLE IF EXISTS `salesreturn`;

CREATE TABLE `salesreturn` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `vatamnt` varchar(45) DEFAULT NULL,
  `vataccount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) NOT NULL,
  `Sgstamnt` varchar(45) DEFAULT NULL,
  `Sgstaccount` varchar(45) DEFAULT NULL,
  `Cgstamnt` varchar(45) DEFAULT NULL,
  `Cgstaccount` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_taxamnt` varchar(45) DEFAULT NULL,
  `total_sgst_taxamnt` varchar(45) DEFAULT NULL,
  `total_cgst_taxamnt` varchar(45) DEFAULT NULL,
  `bill_no` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `salesreturn` */

/*Table structure for table `salesreturn1` */

DROP TABLE IF EXISTS `salesreturn1`;

CREATE TABLE `salesreturn1` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `vatamnt` varchar(45) DEFAULT NULL,
  `vataccount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) NOT NULL,
  `Sgstamnt` varchar(45) DEFAULT NULL,
  `Sgstaccount` varchar(45) DEFAULT NULL,
  `Cgstamnt` varchar(45) DEFAULT NULL,
  `Cgstaccount` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_taxamnt` varchar(45) DEFAULT NULL,
  `total_sgst_taxamnt` varchar(45) DEFAULT NULL,
  `total_cgst_taxamnt` varchar(45) DEFAULT NULL,
  `bill_no` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `salesreturn1` */

/*Table structure for table `salesreturn_old` */

DROP TABLE IF EXISTS `salesreturn_old`;

CREATE TABLE `salesreturn_old` (
  `bill_refrence` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `customer_account` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `stock_no` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `rate` varchar(45) DEFAULT NULL,
  `qnty` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `disc_code` varchar(45) DEFAULT NULL,
  `disc%` varchar(45) DEFAULT NULL,
  `disc_amnt` varchar(45) DEFAULT NULL,
  `total` varchar(45) DEFAULT NULL,
  `staff` varchar(45) DEFAULT NULL,
  `total_amnt` varchar(45) DEFAULT NULL,
  `total_qnty` varchar(45) DEFAULT NULL,
  `total_disc_amnt` varchar(45) DEFAULT NULL,
  `total_amnt_after_disc` varchar(45) DEFAULT NULL,
  `total_value_before_tax` varchar(45) DEFAULT NULL,
  `total_value_after_tax` varchar(45) DEFAULT NULL,
  `vatamnt` varchar(45) DEFAULT NULL,
  `vataccount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `party_code` varchar(45) NOT NULL,
  `Sgstamnt` varchar(45) DEFAULT NULL,
  `Sgstaccount` varchar(45) DEFAULT NULL,
  `Cgstamnt` varchar(45) DEFAULT NULL,
  `Cgstaccount` varchar(45) DEFAULT NULL,
  `Round_Off_Value` varchar(45) DEFAULT NULL,
  `total_igst_taxamnt` varchar(45) DEFAULT NULL,
  `total_sgst_taxamnt` varchar(45) DEFAULT NULL,
  `total_cgst_taxamnt` varchar(45) DEFAULT NULL,
  `bill_no` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `salesreturn_old` */

/*Table structure for table `setting` */

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `Purchase_Party` varchar(7) DEFAULT NULL,
  `Sales_Party` varchar(7) DEFAULT NULL,
  `Ledger` varchar(7) DEFAULT NULL,
  `Staff` varchar(7) DEFAULT NULL,
  `Group_Party` varchar(7) DEFAULT NULL,
  `Product` varchar(7) DEFAULT NULL,
  `Byforgation` varchar(7) DEFAULT NULL,
  `Article_No` varchar(7) DEFAULT NULL,
  `City` varchar(7) DEFAULT NULL,
  `Sales_Billing` varchar(7) DEFAULT NULL,
  `Update_Sales_Billing` varchar(7) DEFAULT NULL,
  `Sales_Return` varchar(7) DEFAULT NULL,
  `Purchase` varchar(7) DEFAULT NULL,
  `Purchase_Return` varchar(7) DEFAULT NULL,
  `Rate_Change` varchar(7) DEFAULT NULL,
  `Daily_Sales_Report` varchar(7) DEFAULT NULL,
  `Daily_Purchase_Report` varchar(7) DEFAULT NULL,
  `Tag_Detail` varchar(7) DEFAULT NULL,
  `Stock_Analysis_Report` varchar(7) DEFAULT NULL,
  `Stock_Report` varchar(7) DEFAULT NULL,
  `Tag_Printing` varchar(7) DEFAULT NULL,
  `Contra` varchar(7) DEFAULT NULL,
  `Journal` varchar(7) DEFAULT NULL,
  `Cash_Payment` varchar(7) DEFAULT NULL,
  `Cash_Reciept` varchar(7) DEFAULT NULL,
  `Bank_Payment` varchar(7) DEFAULT NULL,
  `Bank_Reciept` varchar(7) DEFAULT NULL,
  `Day_Book` varchar(7) DEFAULT NULL,
  `Trial_Balance` varchar(7) DEFAULT NULL,
  `Trading_Account` varchar(7) DEFAULT NULL,
  `Balance_Sheet` varchar(7) DEFAULT NULL,
  `Profit_and_Loss` varchar(7) DEFAULT NULL,
  `Taxation_Report` varchar(7) DEFAULT NULL,
  `Party_Outstanding_Report` varchar(7) DEFAULT NULL,
  `Party_Against_Payment_Report` varchar(7) DEFAULT NULL,
  `Backup` varchar(7) DEFAULT NULL,
  `Restore` varchar(7) DEFAULT NULL,
  `Setting` varchar(7) DEFAULT NULL,
  `Stock_Transfer` varchar(7) DEFAULT NULL,
  `Export_To_Branch` varchar(7) DEFAULT NULL,
  `Import_From_Branch` varchar(7) DEFAULT NULL,
  `BSNL_Billing` varchar(7) DEFAULT NULL,
  `BSNL_Purchase` varchar(7) DEFAULT NULL,
  `Barcode` varchar(7) DEFAULT NULL,
  `Rate` varchar(7) DEFAULT NULL,
  `Sales_Return_update` varchar(7) DEFAULT NULL,
  `Purchase_Update` varchar(7) DEFAULT NULL,
  `Purchase_Return_update` varchar(7) DEFAULT NULL,
  `Interest_Entry` varchar(7) DEFAULT NULL,
  `Stock_Transfer_Report` varchar(7) DEFAULT NULL,
  `Daily_Report` varbinary(7) DEFAULT NULL,
  `Customer_Report` varchar(7) DEFAULT NULL,
  `Company` varchar(45) DEFAULT NULL,
  `Tax_Type` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `setting` */

insert  into `setting`(`Purchase_Party`,`Sales_Party`,`Ledger`,`Staff`,`Group_Party`,`Product`,`Byforgation`,`Article_No`,`City`,`Sales_Billing`,`Update_Sales_Billing`,`Sales_Return`,`Purchase`,`Purchase_Return`,`Rate_Change`,`Daily_Sales_Report`,`Daily_Purchase_Report`,`Tag_Detail`,`Stock_Analysis_Report`,`Stock_Report`,`Tag_Printing`,`Contra`,`Journal`,`Cash_Payment`,`Cash_Reciept`,`Bank_Payment`,`Bank_Reciept`,`Day_Book`,`Trial_Balance`,`Trading_Account`,`Balance_Sheet`,`Profit_and_Loss`,`Taxation_Report`,`Party_Outstanding_Report`,`Party_Against_Payment_Report`,`Backup`,`Restore`,`Setting`,`Stock_Transfer`,`Export_To_Branch`,`Import_From_Branch`,`BSNL_Billing`,`BSNL_Purchase`,`Barcode`,`Rate`,`Sales_Return_update`,`Purchase_Update`,`Purchase_Return_update`,`Interest_Entry`,`Stock_Transfer_Report`,`Daily_Report`,`Customer_Report`,`Company`,`Tax_Type`) values 
('Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Enable','Disable','Enable','Enable','Enable','Disable','Disable','Enable','Disable','Enable','Enable','Enable','Disable','Disable','Disable','Enable',NULL,NULL);

/*Table structure for table `staff` */

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `Emp_Code` varchar(45) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `phone_no` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `aadhar_no` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Emp_Code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `staff` */

insert  into `staff`(`Emp_Code`,`Name`,`address`,`city`,`state`,`phone_no`,`email_id`,`aadhar_no`) values 
('ST/1','SHYAM RAO','','RATLAM','MADHYA PRADESH','','',NULL),
('ST/2','NASHREEN','','RATLAM','MADHYA PRADESH','','',NULL),
('ST/3','PREETI','','RATLAM','MADHYA PRADESH','','',NULL),
('ST/4','SHABNAM','','RATLAM','MADHYA PRADESH','','',NULL),
('ST/5','YESHWANTH','','RATLAM','MADHYA PRADESH','','',NULL),
('ST/6','SELF','','RATLAM','MADHYA PRADESH','','',NULL),
('ST/7','OTHER','','RATLAM','MADHYA PRADESH','','',NULL),
('ST/8','KAVITA','','RATLAM','MADHYA PRADESH','','',NULL);

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `Supplier_id` varchar(45) DEFAULT NULL,
  `Supplier_name` varchar(45) DEFAULT NULL,
  `Invoice_date` varchar(45) DEFAULT NULL,
  `Invoice_number` varchar(45) DEFAULT NULL,
  `Purchase_type` varchar(45) DEFAULT NULL,
  `Refrence_number` varchar(45) DEFAULT NULL,
  `Sno` varchar(45) DEFAULT NULL,
  `Qty` varchar(45) DEFAULT NULL,
  `Rate` varchar(45) DEFAULT NULL,
  `Amount` varchar(45) DEFAULT NULL,
  `Pdesc` varchar(45) DEFAULT NULL,
  `Retail` varchar(45) DEFAULT NULL,
  `Desc` varchar(45) DEFAULT NULL,
  `Byforgation` varchar(45) DEFAULT NULL,
  `Product` varchar(45) DEFAULT NULL,
  `Bill_amount` varchar(45) DEFAULT NULL,
  `discount` varchar(45) DEFAULT NULL,
  `amount_after_discount` varchar(45) DEFAULT NULL,
  `Cgst_tax` varchar(45) DEFAULT NULL,
  `Other_charges` varchar(45) DEFAULT NULL,
  `Net_amount` varchar(45) DEFAULT NULL,
  `Sgst_tax` varchar(45) DEFAULT NULL,
  `sgstamount` varchar(45) DEFAULT NULL,
  `cgsttaxamount` varchar(45) DEFAULT NULL,
  `sgstaccount` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `Partypurchase_date` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `totalquantity` varchar(45) DEFAULT NULL,
  `samebarcode` varchar(45) DEFAULT NULL,
  `cgsttaxaccount` varchar(45) DEFAULT NULL,
  `otherchargesaccount` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `IGST_Amount` varchar(45) DEFAULT NULL,
  `IGST_Tax_Account` varchar(45) DEFAULT NULL,
  `round_off` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stock` */

insert  into `stock`(`Supplier_id`,`Supplier_name`,`Invoice_date`,`Invoice_number`,`Purchase_type`,`Refrence_number`,`Sno`,`Qty`,`Rate`,`Amount`,`Pdesc`,`Retail`,`Desc`,`Byforgation`,`Product`,`Bill_amount`,`discount`,`amount_after_discount`,`Cgst_tax`,`Other_charges`,`Net_amount`,`Sgst_tax`,`sgstamount`,`cgsttaxamount`,`sgstaccount`,`to_ledger`,`by_ledger`,`Partypurchase_date`,`articleno`,`totalquantity`,`samebarcode`,`cgsttaxaccount`,`otherchargesaccount`,`branchid`,`IGST_Amount`,`IGST_Tax_Account`,`round_off`) values 
('P/1/203','COTTON PLUS','2017-09-14','PR/1718/104','Credit','97','5','21','422','8862.00','399.0','520','','null','SHPRM 5208','37002.00','3700.20','33301.8','0','0.11','34967.0','0','0.0','0.0','null','COTTON PLUS','PURCHASE 5%','2017-09-14','null','72.0','NO','null','null','0','1665.09','IGST 5%','-0.00'),
('P/1/203','COTTON PLUS','2017-09-14','PR/1718/104','Credit','97','6','3','480','1440.00','454.0','590','','null','SHPRM 5208','37002.00','3700.20','33301.8','0','0.11','34967.0','0','0.0','0.0','null','COTTON PLUS','PURCHASE 5%','2017-09-14','null','72.0','NO','null','null','0','1665.09','IGST 5%','-0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-01','PR/1718/105','Credit','704','1','10','815','8150.00','856.0','1099','SHAWL-401','null','SRSYP 5407','8150.00','244.50','7905.5','2.5','-.78','8300.00','2.5','197.64','197.64','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-01','null','10.0','NO','CGST 2.5%','null','0','0.00','null','0.00'),
('P/2/279','VEDANTA FABRICS','2017-09-20','PR/1718/106','Credit','5503','1','17','360','6120.00','378.0','550','5D','null','SRSYP 5407','21600.00','0.00','21600.0','0','0','22680.0','0','0.0','0.0','null','VEDANTA FABRICS','PURCHASE 5%','2017-09-20','null','60.0','NO','null','null','0','1080.0','IGST 5%','0.00'),
('P/2/279','VEDANTA FABRICS','2017-09-20','PR/1718/106','Credit','5503','2','43','360','15480.00','378.0','599','5D','null','SRSYP 5407','21600.00','0.00','21600.0','0','0','22680.0','0','0.0','0.0','null','VEDANTA FABRICS','PURCHASE 5%','2017-09-20','null','60.0','NO','null','null','0','1080.0','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','1','4','725','2900.00','761.0','730','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','2','2','630','1260.00','662.0','890','','P2','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','3','4','495','1980.00','520.0','650','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','4','2','615','1230.00','646.0','870','','P2','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','5','3','550','1650.00','578.0','790','','P3','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','6','4','530','2120.00','557.0','760','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','7','4','645','2580.00','677.0','880','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','8','4','595','2380.00','625.0','799','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','9','4','640','2560.00','672.0','899','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','10','4','530','2120.00','557.0','750','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','11','4','575','2300.00','604.0','820','','P4','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','12','4','625','2500.00','656.0','899','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','13','4','625','2500.00','656.0','899','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','14','4','525','2100.00','551.0','760','','P4','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','15','3','815','2445.00','856.0','1160','','P3','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','16','3','425','1275.00','446.0','670','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','17','4','550','2200.00','578.0','830','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','18','3','530','1590.00','557.0','830','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','19','4','595','2380.00','625.0','899','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','20','4','675','2700.00','709.0','999','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','21','4','395','1580.00','415.0','620','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','22','3','395','1185.00','415.0','620','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','23','3','405','1215.00','425.0','630','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','24','3','525','1575.00','551.0','799','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','1','16','699','11184.00','697.0','1150','PNT / XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','2','12','425','5100.00','424.0','690','XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','3','8','649','5192.00','647.0','1050','XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','4','10','599','5990.00','598.0','960','XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','5','10','599','5990.00','598.0','960','XL 42','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','1','1','3190','3190.00','3350.0','4190','VTC-001','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','2','1','3190','3190.00','3350.0','4190','VTC-002','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','3','1','2850','2850.00','2993.0','3750','VTC-003','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','4','1','2850','2850.00','2993.0','3750','VTC-004','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','5','1','3050','3050.00','3203.0','3999','VTC-005','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','6','1','3050','3050.00','3203.0','3999','VTC-006','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','7','1','2930','2930.00','3077.0','3850','VTC-007','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','8','1','3050','3050.00','3203.0','3999','VTC-008','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','9','1','2970','2970.00','3119.0','3899','VTC-009','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','10','1','3025','3025.00','3176.0','3980','VTC-010','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','1','28','425','11900.00','446.0','580','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','2','14','595','8330.00','625.0','820','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','3','3','650','1950.00','683.0','890','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','4','4','850','3400.00','893.0','1199','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','5','12','910','10920.00','956.0','1250','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','1','7','415','2905.00','436.0','560','','null','SSCOT 5208','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','2','18','450','8100.00','473.0','620','','null','SSCOT 5208','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','3','38','395','15010.00','415.0','540','','null','SSSLK 5210','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','4','3','450','1350.00','473.0','599','','null','SSSLK 5210','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','5','4','550','2200.00','578.0','750','','null','SSSLK 5210','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','1','12','381','4572.00','400.0','500','','P6','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','2','16','345','5520.00','362.0','470','2.25 SAL','P8','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','3','4','391','1564.00','411.0','540','2.50 SAL','P4','SSCOT 5208','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','4','4','511','2044.00','537.0','699','2.50 SAL','P4','SSCOT 5208','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','5','4','431','1724.00','453.0','599','','null','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','6','7','811','5677.00','852.0','1150','2.50 SAL','null','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','7','3','771','2313.00','810.0','1070','','P3','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','8','4','621','2484.00','652.0','890','','P4','SSSLK 6307','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','9','2','751','1502.00','789.0','1070','','P2','SSSLK 6307','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','10','4','821','3284.00','862.0','1170','','P4','SSSLK 6307','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','1','8','585','4680.00','614.0','830','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','2','3','525','1575.00','551.0','750','','P3','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','3','8','425','3400.00','446.0','599','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','4','4','440','1760.00','462.0','620','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','5','12','450','5400.00','473.0','650','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','6','4','550','2200.00','578.0','780','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','7','8','525','4200.00','551.0','750','','P8','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','8','4','535','2140.00','562.0','770','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','9','2','400','800.00','420.0','799','','P2','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','10','4','495','1980.00','520.0','799','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','11','4','525','2100.00','551.0','750','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','12','4','435','1740.00','457.0','630','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','1','16','365','5840.00','383.0','520','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','2','4','375','1500.00','394.0','530','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','3','12','385','4620.00','404.0','550','','P6','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','4','8','425','3400.00','446.0','599','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','5','11','395','4345.00','415.0','570','P11','null','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','6','8','485','3880.00','509.0','699','','P8','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','7','4','510','2040.00','536.0','730','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','8','4','500','2000.00','525.0','720','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','9','4','510','2040.00','536.0','730','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','1','2','1395','2790.00','1465.0','2199','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','2','1','1595','1595.00','1675.0','2499','','null','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','3','2','1595','3190.00','1675.0','2499','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','4','2','1595','3190.00','1675.0','2499','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','5','2','1195','2390.00','1255.0','1850','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','6','2','1495','2990.00','1570.0','2390','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','7','3','1295','3885.00','1360.0','2050','','P3','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/116','Credit','MT-1995','1','2','895','1790.00','940.0','1399','PNT','P2','SSKUR 6307','1790.00','0.00','1790.0','0','0.50','1880.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','2.0','NO','null','null','0','89.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','1','2','1395','2790.00','1465.0','2090','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','2','2','1295','2590.00','1360.0','1950','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','3','3','1595','4785.00','1675.0','2399','DUP / SAL','P3','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','4','2','1395','2790.00','1465.0','2090','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','5','2','1395','2790.00','1465.0','2090','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','6','3','1895','5685.00','1990.0','2850','DUP / SAL','P3','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','7','2','1295','2590.00','1360.0','1950','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','8','2','1695','3390.00','1780.0','2499','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','9','2','1595','3190.00','1675.0','2399','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','10','3','1595','4785.00','1675.0','2399','DUP / SAL','P3','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','11','2','1195','2390.00','1255.0','1850','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','1','2','1995','3990.00','2095.0','3199','INR','P2','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','2','4','2295','9180.00','2410.0','3650','JAC','P4','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','3','1','2295','2295.00','2410.0','3650','INR','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','4','2','1695','3390.00','1780.0','2699','INR','P2','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','5','1','1695','1695.00','1780.0','2699','PNT','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','6','3','1695','5085.00','1780.0','2799','PNT / DUP','P3','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','7','1','2095','2095.00','2200.0','3350','DUP','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','8','1','1195','1195.00','1255.0','1880','PNT','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','9','1','1095','1095.00','1150.0','1750','INR','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','1','2','2095','4190.00','2200.0','3350','DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','2','3','1595','4785.00','1675.0','2499','DUP','P3','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','3','1','1195','1195.00','1255.0','1899','PNT','null','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','4','2','1595','3190.00','1675.0','2560','DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','5','2','1495','2990.00','1570.0','2399','PNT / DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','6','2','1695','3390.00','1780.0','2699','DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','7','2','1195','2390.00','1255.0','1899','PNT','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','8','2','1995','3990.00','2095.0','3199','INR','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','9','2','1495','2990.00','1570.0','2499','PNT / DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','10','2','2795','5590.00','2935.0','4499','INR','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','11','6','1895','11370.00','1990.0','2999','','P6','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/120','Credit','10766','1','2','895','1790.00','940.0','1399','','P2','SSKUR 6211','1790.00','0.00','1790.0','0','0.50','1880.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 5%','2017-09-01','null','2.0','NO','null','null','0','89.5','IGST 5%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','1','2','1095','2190.00','1150.0','1699','DUP','P2','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','2','2','2215','4430.00','2326.0','3399','DUP','P2','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','3','2','1995','3990.00','2095.0','3150','DUP','P2','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','4','1','1625','1625.00','1706.0','2560','DUP','null','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','5','1','1995','1995.00','2095.0','3150','DUP','null','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','1','1','1095','1095.00','1150.0','1730','','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','2','2','1595','3190.00','1675.0','2520','','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','3','1','2095','2095.00','2200.0','3299','DUP','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','4','3','2195','6585.00','2305.0','3499','','P3','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','5','1','2595','2595.00','2725.0','3799','INR','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','6','1','2895','2895.00','3040.0','4550','','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','1','2','1445','2890.00','1517.0','2399','DUP / SAL','P2','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','2','1','2395','2395.00','2515.0','3999','SKRT / DUP','null','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','3','2','1795','3590.00','1885.0','2950','DUP / SAL','P2','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','4','1','1595','1595.00','1675.0','2699','DUP / SAL','null','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','5','1','1645','1645.00','1727.0','2770','DUP / SAL','null','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','6','4','1395','5580.00','1465.0','2299','DUP /  SAL','P4','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','7','2','1295','2590.00','1360.0','2180','PNT','P2','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','8','1','1095','1095.00','1150.0','1850','PNT','null','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','9','1','1195','1195.00','1255.0','1890','','null','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','10','1','1995','1995.00','2095.0','3299','DUP','null','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','11','2','1495','2990.00','1570.0','2299','DUP','P2','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','12','4','1295','5180.00','1360.0','2099','','P4','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','13','3','1795','5385.00','1885.0','2850','INR','P3','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','14','2','2895','5790.00','3040.0','4599','DUP','P2','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','15','4','1595','6380.00','1675.0','2499','','P4','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','1','2','1245','2490.00','1307.0','1960','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','2','2','1395','2790.00','1465.0','2099','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','3','2','1295','2590.00','1360.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','4','2','1295','2590.00','1360.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','5','3','1095','3285.00','1150.0','1630','SAL / DUP','P3','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','6','2','1095','2190.00','1150.0','1650','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','7','1','1045','1045.00','1097.0','1650','SAL / DUP','null','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','8','2','1295','2590.00','1360.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','9','2','1345','2690.00','1412.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','10','3','1395','4185.00','1465.0','2099','SAL / DUP','P3','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','11','1','1295','1295.00','1360.0','1990','SAL / DUP','null','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','12','2','895','1790.00','940.0','1299','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','1','4','695','2780.00','730.0','1030','','P4','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','2','3','799','2397.00','839.0','1150','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','3','5','625','3125.00','656.0','899','','P5','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','4','3','1175','3525.00','1234.0','1660','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','5','2','875','1750.00','919.0','1290','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','6','4','575','2300.00','604.0','820','','P4','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','7','3','550','1650.00','578.0','799','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','8','2','975','1950.00','1024.0','1430','','P2','SSKUR 6104','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','9','2','775','1550.00','814.0','1094','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','10','3','895','2685.00','940.0','1299','','P3','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','11','2','975','1950.00','1024.0','1430','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','12','4','695','2780.00','730.0','999','','P4','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','13','2','1050','2100.00','1103.0','1550','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','14','4','1175','4700.00','1234.0','1670','','P4','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','15','2','1050','2100.00','1103.0','1660','','P2','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','16','3','475','1425.00','499.0','660','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','17','2','575','1150.00','604.0','899','','P2','SSKUR 6204','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','18','2','750','1500.00','788.0','1190','','P2','SSKUR 6204','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','19','3','575','1725.00','604.0','920','','P3','SSKUR 6204','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','1','2','550','1100.00','578.0','799','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','2','2','550','1100.00','578.0','799','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','3','3','875','2625.00','919.0','1290','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','4','3','895','2685.00','940.0','1299','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','5','3','799','2397.00','839.0','1150','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','6','4','995','3980.00','1045.0','1470','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','7','3','799','2397.00','839.0','1150','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','8','3','795','2385.00','835.0','1130','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','9','3','550','1650.00','578.0','780','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','10','4','525','2100.00','551.0','770','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','11','2','1050','2100.00','1103.0','1550','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','12','14','1195','16730.00','1255.0','1750','','null','SSSLK 5515','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','13','4','1395','5580.00','1465.0','1999','','P4','SSSLK 5515','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','14','1','995','995.00','1045.0','1460','','null','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','15','3','1295','3885.00','1360.0','1899','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','16','4','695','2780.00','730.0','1030','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','17','2','1195','2390.00','1255.0','1750','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','18','2','1295','2590.00','1360.0','1899','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','19','3','995','2985.00','1045.0','1450','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','20','4','525','2100.00','551.0','760','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','21','3','675','2025.00','709.0','999','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','22','2','995','1990.00','1045.0','1460','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','23','1','1295','1295.00','1360.0','1899','','null','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','24','1','1095','1095.00','1150.0','1550','','null','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','25','2','995','1990.00','1045.0','1450','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','26','2','1475','2950.00','1549.0','2160','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','27','2','1295','2590.00','1360.0','1899','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','1','5','550','2750.00','578.0','850','','P5','SSDUP 6307','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','2','3','850','2550.00','893.0','1260','','P3','SSKUR 6307','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','3','2','950','1900.00','998.0','1399','','P2','SSKUR 6307','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','4','1','2250','2250.00','2363.0','3299','','null','SSSLK 5515','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','1','18','260','4680.00','273.0','399','','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','2','4','450','1800.00','473.0','650','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','3','9','280','2520.00','294.0','450','P9','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','4','8','535','4280.00','562.0','780','','P8','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','5','9','180','1620.00','189.0','280','P9','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','6','5','535','2675.00','562.0','770','','P5','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','7','4','450','1800.00','473.0','650','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','8','4','595','2380.00','625.0','860','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','9','4','510','2040.00','536.0','760','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','10','34','575','19550.00','604.0','860','P34','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','11','40','165','6600.00','173.0','230','P40','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','12','35','180','6300.00','189.0','250','P35','null','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','13','11','635','6985.00','667.0','930','P11','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','1','1','999','999.00','1049.0','1330','PLTNIM-700','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','2','1','999','999.00','1049.0','1330','PLTNIM-701','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','3','1','999','999.00','1049.0','1330','PLTNIM-702','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','4','1','999','999.00','1049.0','1330','PLTNIM-703','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','5','1','999','999.00','1049.0','1330','PLTNIM-704','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','6','1','999','999.00','1049.0','1330','PLTNIM-705','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','7','1','999','999.00','1049.0','1330','PLTNIM-706','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','8','1','999','999.00','1049.0','1330','PLTNIM-707','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','9','1','999','999.00','1049.0','1330','PLTNIM-709','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','10','1','999','999.00','1049.0','1330','PLTNIM-710','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','11','1','999','999.00','1049.0','1330','PLTNIM-711','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','12','1','999','999.00','1049.0','1330','PLTNIM-712','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','13','1','999','999.00','1049.0','1330','PLTNIM-713','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','14','1','999','999.00','1049.0','1330','PLTNIM-715','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','15','1','999','999.00','1049.0','1399','PLTNIM-716','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','16','1','999','999.00','1049.0','1330','PLTNIM-717','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','17','1','999','999.00','1049.0','1330','PLTNIM-718','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','18','1','999','999.00','1049.0','1330','PLTNIM-719','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/11','SWAPNA SUNDARI(PURCHASE)','2017-09-29','PR/1718/130','Credit','155','1','43','240','10320.00','252.0','310','2','null','LEGI 5208','10320.00','492.00','9828.0','2.5','0','10320.0','2.5','246','246','SGST 2.5%','SWAPNA SUNDARI(PURCHASE)','PURCHASE 5%','2017-09-29','TWIST','43.0','NO','CGST 2.5%','FREIGHT','0','0.0','null','0.00'),
('P/1/119','SHREE ANJANA ENTERPRISE','2017-09-23','PR/1718/131','Credit','1962','1','80.60','325','26195.00','341.0','522','','null','RA5515','26195.00','5','26190.0','0','0','27500.00','0','0.00','0.00','null','SHREE ANJANA ENTERPRISE','PURCHASE 5%','2017-09-23','null','80.6','NO','null','FREIGHT','0','1310','IGST 5%','0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','1','12','651','7812.00','684.0','899','','null','SSSLK  5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','2','12','775','9300.00','814.0','1130','','null','SSCOT 6204','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','3','6','1395','8370.00','1465.0','2050','','P6','SSSLK  5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','4','7','1340','9380.00','1407.0','1999','','null','SSSLK  5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','5','8','699','5592.00','734.0','1130','PNT','P8','SSKUR 6307','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','6','9','699','6291.00','734.0','1099','','null','SSKUR 6307','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','7','9','425','3825.00','446.0','650','','null','SSBTM 5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','8','6','625','3750.00','656.0','980','','null','SSKUR 5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-28','PR/1718/133','Credit','1034','1','12','869','10428.00','912.0','1199','NZAKAT-001','null','SRSYP 5407','10428.00','312.7999999999993','10115.2','2.5','0.04','10621.0','2.5','252.88','252.88','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-28','null','12.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','1','2','1795','3590.00','1885.0','2999','LEG / DUP','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','2','2','2995','5990.00','3145.0','4999','LEG / DUP ','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','3','1','4995','4995.00','5245.0','7999','LEG / DUP ','null','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','4','2','4095','8190.00','4300.0','6999','LEG / DUP','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','5','2','1995','3990.00','2095.0','3299','LEG / DUP ','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','6','3','1595','4785.00','1675.0','2550','LEG / DUP','P3','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','7','10','1650','16500.00','1733.0','2699','LEG / DUP','null','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','8','7','1850','12950.00','1943.0','2999','LEG / DUP ','null','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/7/439','JAMBOO STORE','2017-08-30','PR/1718/135','Credit','1359','1','2','235','470.00','247.0','290','','null','SST&B 5208','470.00','22.00','448.0','2.5','0','470.0','2.5','11','11','SGST 2.5%','JAMBOO STORE','PURCHASE 5%','2017-08-30','null','2.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/1/203','COTTON PLUS','2017-09-14','PR/1718/104','Credit','97','5','21','422','8862.00','399.0','520','','null','SHPRM 5208','37002.00','3700.20','33301.8','0','0.11','34967.0','0','0.0','0.0','null','COTTON PLUS','PURCHASE 5%','2017-09-14','null','72.0','NO','null','null','0','1665.09','IGST 5%','-0.00'),
('P/1/203','COTTON PLUS','2017-09-14','PR/1718/104','Credit','97','6','3','480','1440.00','454.0','590','','null','SHPRM 5208','37002.00','3700.20','33301.8','0','0.11','34967.0','0','0.0','0.0','null','COTTON PLUS','PURCHASE 5%','2017-09-14','null','72.0','NO','null','null','0','1665.09','IGST 5%','-0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-01','PR/1718/105','Credit','704','1','10','815','8150.00','856.0','1099','SHAWL-401','null','SRSYP 5407','8150.00','244.50','7905.5','2.5','-.78','8300.00','2.5','197.64','197.64','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-01','null','10.0','NO','CGST 2.5%','null','0','0.00','null','0.00'),
('P/2/279','VEDANTA FABRICS','2017-09-20','PR/1718/106','Credit','5503','1','17','360','6120.00','378.0','550','5D','null','SRSYP 5407','21600.00','0.00','21600.0','0','0','22680.0','0','0.0','0.0','null','VEDANTA FABRICS','PURCHASE 5%','2017-09-20','null','60.0','NO','null','null','0','1080.0','IGST 5%','0.00'),
('P/2/279','VEDANTA FABRICS','2017-09-20','PR/1718/106','Credit','5503','2','43','360','15480.00','378.0','599','5D','null','SRSYP 5407','21600.00','0.00','21600.0','0','0','22680.0','0','0.0','0.0','null','VEDANTA FABRICS','PURCHASE 5%','2017-09-20','null','60.0','NO','null','null','0','1080.0','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','1','4','725','2900.00','761.0','730','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','2','2','630','1260.00','662.0','890','','P2','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','3','4','495','1980.00','520.0','650','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','4','2','615','1230.00','646.0','870','','P2','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','5','3','550','1650.00','578.0','790','','P3','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','6','4','530','2120.00','557.0','760','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','7','4','645','2580.00','677.0','880','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','8','4','595','2380.00','625.0','799','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','9','4','640','2560.00','672.0','899','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','10','4','530','2120.00','557.0','750','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','11','4','575','2300.00','604.0','820','','P4','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','12','4','625','2500.00','656.0','899','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','13','4','625','2500.00','656.0','899','','P4','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','14','4','525','2100.00','551.0','760','','P4','SSCOT 5208','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','15','3','815','2445.00','856.0','1160','','P3','SSSLK 5210','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','16','3','425','1275.00','446.0','670','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','17','4','550','2200.00','578.0','830','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','18','3','530','1590.00','557.0','830','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','19','4','595','2380.00','625.0','899','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','20','4','675','2700.00','709.0','999','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','21','4','395','1580.00','415.0','620','','P4','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','22','3','395','1185.00','415.0','620','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','23','3','405','1215.00','425.0','630','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/1/53','PRASANG COLLECTION','2017-09-15','PR/1718/107','Credit','43','24','3','525','1575.00','551.0','799','','P3','SSKUR 6104','48325.00','1430.00','46895.0','0','0','49240.0','0','0.0','0.0','null','PRASANG COLLECTION','PURCHASE 5%','2017-09-15','null','85.0','NO','null','null','0','2345','IGST 5%','0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','1','16','699','11184.00','697.0','1150','PNT / XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','2','12','425','5100.00','424.0','690','XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','3','8','649','5192.00','647.0','1050','XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','4','10','599','5990.00','598.0','960','XXL 44','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/2/280','KAJREE FASHION','2017-09-18','PR/1718/108','Credit','2602','5','10','599','5990.00','598.0','960','XL 42','null','SSKUR 6106','33456.00','1673.19','31782.81','0','0','33372.0','0','0.0','0.0','null','KAJREE FASHION','PURCHASE 5%','2017-09-18','null','56.0','NO','null','null','0','1589.19','IGST 5%','-0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','1','1','3190','3190.00','3350.0','4190','VTC-001','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','2','1','3190','3190.00','3350.0','4190','VTC-002','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','3','1','2850','2850.00','2993.0','3750','VTC-003','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','4','1','2850','2850.00','2993.0','3750','VTC-004','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','5','1','3050','3050.00','3203.0','3999','VTC-005','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','6','1','3050','3050.00','3203.0','3999','VTC-006','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','7','1','2930','2930.00','3077.0','3850','VTC-007','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','8','1','3050','3050.00','3203.0','3999','VTC-008','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','9','1','2970','2970.00','3119.0','3899','VTC-009','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-21','PR/1718/109','Credit','935','10','1','3025','3025.00','3176.0','3980','VTC-010','null','SRSYP 5407','30155.00','904.65','29250.35','2.5','0.15','30713.0','2.5','731.25','731.25','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-21','null','10.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','1','28','425','11900.00','446.0','580','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','2','14','595','8330.00','625.0','820','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','3','3','650','1950.00','683.0','890','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','4','4','850','3400.00','893.0','1199','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/110','Credit','091','5','12','910','10920.00','956.0','1250','','null','SSSLK 5210','36500.00','305.00','36195.0','0','-04.76','38000.00','0','0.0','0.0','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','61.0','NO','null','null','0','1809.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','1','7','415','2905.00','436.0','560','','null','SSCOT 5208','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','2','18','450','8100.00','473.0','620','','null','SSCOT 5208','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','3','38','395','15010.00','415.0','540','','null','SSSLK 5210','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','4','3','450','1350.00','473.0','599','','null','SSSLK 5210','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/1/14','JAY TRADING COMPANY','2017-09-15','PR/1718/111','Credit','090','5','4','550','2200.00','578.0','750','','null','SSSLK 5210','29565.00','356.00','29209.0','0','0.24','30670.00','0','0.00','0.00','null','JAY TRADING COMPANY','PURCHASE 5%','2017-09-15','null','70.0','NO','null','null','0','1460.76','IGST 5%','0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','1','12','381','4572.00','400.0','500','','P6','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','2','16','345','5520.00','362.0','470','2.25 SAL','P8','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','3','4','391','1564.00','411.0','540','2.50 SAL','P4','SSCOT 5208','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','4','4','511','2044.00','537.0','699','2.50 SAL','P4','SSCOT 5208','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','5','4','431','1724.00','453.0','599','','null','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','6','7','811','5677.00','852.0','1150','2.50 SAL','null','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','7','3','771','2313.00','810.0','1070','','P3','SSSLK 5210','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','8','4','621','2484.00','652.0','890','','P4','SSSLK 6307','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','9','2','751','1502.00','789.0','1070','','P2','SSSLK 6307','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/35','FATEHCHAND DARBARILAL','2017-09-20','PR/1718/112','Credit','174','10','4','821','3284.00','862.0','1170','','P4','SSSLK 6307','30684.00','0.00','30684.0','2.5','-18.22','32200.00','2.5','767.11','767.11','SGST 2.5%','FATEHCHAND DARBARILAL','PURCHASE 5%','2017-09-20','null','60.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','1','8','585','4680.00','614.0','830','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','2','3','525','1575.00','551.0','750','','P3','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','3','8','425','3400.00','446.0','599','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','4','4','440','1760.00','462.0','620','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','5','12','450','5400.00','473.0','650','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','6','4','550','2200.00','578.0','780','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','7','8','525','4200.00','551.0','750','','P8','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','8','4','535','2140.00','562.0','770','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','9','2','400','800.00','420.0','799','','P2','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','10','4','495','1980.00','520.0','799','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','11','4','525','2100.00','551.0','750','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-14','PR/1718/113','Credit','153','12','4','435','1740.00','457.0','630','','P4','SSCOT 6204','31975.00','0.00','31975.0','0','0.25','33574.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-14','null','65.0','NO','null','null','0','1598.75','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','1','16','365','5840.00','383.0','520','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','2','4','375','1500.00','394.0','530','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','3','12','385','4620.00','404.0','550','','P6','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','4','8','425','3400.00','446.0','599','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','5','11','395','4345.00','415.0','570','P11','null','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','6','8','485','3880.00','509.0','699','','P8','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','7','4','510','2040.00','536.0','730','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','8','4','500','2000.00','525.0','720','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/19','K.SURENDRAKUMAR','2017-09-13','PR/1718/114','Credit','261','9','4','510','2040.00','536.0','730','','P4','SSCOT 6204','29665.00','0.25','29664.75','0','0','31148.0','0','0.0','0.0','null','K.SURENDRAKUMAR','PURCHASE 5%','2017-09-13','null','71.0','NO','null','null','0','1483.25','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','1','2','1395','2790.00','1465.0','2199','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','2','1','1595','1595.00','1675.0','2499','','null','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','3','2','1595','3190.00','1675.0','2499','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','4','2','1595','3190.00','1675.0','2499','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','5','2','1195','2390.00','1255.0','1850','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','6','2','1495','2990.00','1570.0','2390','','P2','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/115','Credit','MT-1997','7','3','1295','3885.00','1360.0','2050','','P3','SSSLK 5515','20030.00','0.00','20030.0','0','0.50','21032.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','14.0','NO','null','null','0','1001.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/116','Credit','MT-1995','1','2','895','1790.00','940.0','1399','PNT','P2','SSKUR 6307','1790.00','0.00','1790.0','0','0.50','1880.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','2.0','NO','null','null','0','89.5','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','1','2','1395','2790.00','1465.0','2090','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','2','2','1295','2590.00','1360.0','1950','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','3','3','1595','4785.00','1675.0','2399','DUP / SAL','P3','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','4','2','1395','2790.00','1465.0','2090','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','5','2','1395','2790.00','1465.0','2090','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','6','3','1895','5685.00','1990.0','2850','DUP / SAL','P3','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','7','2','1295','2590.00','1360.0','1950','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','8','2','1695','3390.00','1780.0','2499','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','9','2','1595','3190.00','1675.0','2399','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','10','3','1595','4785.00','1675.0','2399','DUP / SAL','P3','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/117','Credit','MT-1996','11','2','1195','2390.00','1255.0','1850','DUP / SAL','P2','SSSLK 5515','37775.00','0.00','37775.0','0','0.25','39664.0','0','0.0','0.0','null','MAITRI','PURCHASE 5%','2017-09-15','null','25.0','NO','null','null','0','1888.75','IGST 5%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','1','2','1995','3990.00','2095.0','3199','INR','P2','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','2','4','2295','9180.00','2410.0','3650','JAC','P4','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','3','1','2295','2295.00','2410.0','3650','INR','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','4','2','1695','3390.00','1780.0','2699','INR','P2','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','5','1','1695','1695.00','1780.0','2699','PNT','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','6','3','1695','5085.00','1780.0','2799','PNT / DUP','P3','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','7','1','2095','2095.00','2200.0','3350','DUP','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','8','1','1195','1195.00','1255.0','1880','PNT','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/118','Credit','MT-1994','9','1','1095','1095.00','1150.0','1750','INR','null','12% SSKUR 6307','30020.00','0.40','30019.6','0','0','33622.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','16.0','NO','null','null','0','3602.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','1','2','2095','4190.00','2200.0','3350','DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','2','3','1595','4785.00','1675.0','2499','DUP','P3','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','3','1','1195','1195.00','1255.0','1899','PNT','null','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','4','2','1595','3190.00','1675.0','2560','DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','5','2','1495','2990.00','1570.0','2399','PNT / DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','6','2','1695','3390.00','1780.0','2699','DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','7','2','1195','2390.00','1255.0','1899','PNT','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','8','2','1995','3990.00','2095.0','3199','INR','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','9','2','1495','2990.00','1570.0','2499','PNT / DUP','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','10','2','2795','5590.00','2935.0','4499','INR','P2','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/1/75','MAITRI','2017-09-15','PR/1718/119','Credit','MT-1993','11','6','1895','11370.00','1990.0','2999','','P6','12% SSKUR 6307','46070.00','0.40','46069.6','0','0','51598.0','0','0.0','0.0','null','MAITRI','PURCHASE 12%','2017-09-15','null','26.0','NO','null','null','0','5528.4','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/120','Credit','10766','1','2','895','1790.00','940.0','1399','','P2','SSKUR 6211','1790.00','0.00','1790.0','0','0.50','1880.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 5%','2017-09-01','null','2.0','NO','null','null','0','89.5','IGST 5%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','1','2','1095','2190.00','1150.0','1699','DUP','P2','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','2','2','2215','4430.00','2326.0','3399','DUP','P2','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','3','2','1995','3990.00','2095.0','3150','DUP','P2','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','4','1','1625','1625.00','1706.0','2560','DUP','null','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/43','SAMMANITA SATTVA LLP','2017-09-01','PR/1718/121','Credit','10766','5','1','1995','1995.00','2095.0','3150','DUP','null','12% SSKUR 6211','14230.00','0.00','14230.0','0','0.40','15938.0','0','0.0','0.0','null','SAMMANITA SATTVA LLP','PURCHASE 12%','2017-09-01','null','8.0','NO','null','null','0','1707.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','1','1','1095','1095.00','1150.0','1730','','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','2','2','1595','3190.00','1675.0','2520','','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','3','1','2095','2095.00','2200.0','3299','DUP','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','4','3','2195','6585.00','2305.0','3499','','P3','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','5','1','2595','2595.00','2725.0','3799','INR','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/4/53','NAV DURGA','2017-09-08','PR/1718/122','Credit','748','6','1','2895','2895.00','3040.0','4550','','null','12% SSKUR 6211','18455.00','0.00','18455.0','0','0.40','20670.0','0','0.0','0.0','null','NAV DURGA','PURCHASE 12%','2017-09-08','null','9.0','NO','null','null','0','2214.6','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','1','2','1445','2890.00','1517.0','2399','DUP / SAL','P2','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','2','1','2395','2395.00','2515.0','3999','SKRT / DUP','null','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','3','2','1795','3590.00','1885.0','2950','DUP / SAL','P2','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','4','1','1595','1595.00','1675.0','2699','DUP / SAL','null','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','5','1','1645','1645.00','1727.0','2770','DUP / SAL','null','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','6','4','1395','5580.00','1465.0','2299','DUP /  SAL','P4','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','7','2','1295','2590.00','1360.0','2180','PNT','P2','12% SSKUR 6307','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','8','1','1095','1095.00','1150.0','1850','PNT','null','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','9','1','1195','1195.00','1255.0','1890','','null','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','10','1','1995','1995.00','2095.0','3299','DUP','null','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','11','2','1495','2990.00','1570.0','2299','DUP','P2','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','12','4','1295','5180.00','1360.0','2099','','P4','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','13','3','1795','5385.00','1885.0','2850','INR','P3','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','14','2','2895','5790.00','3040.0','4599','DUP','P2','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/123','Credit','844','15','4','1595','6380.00','1675.0','2499','','P4','12% SSKUR 6204','50295.00','1005.90','49289.1','0','-.79','55203.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 12%','2017-09-15','null','31.0','NO','null','null','0','5914.69','IGST 12%','0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','1','2','1245','2490.00','1307.0','1960','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','2','2','1395','2790.00','1465.0','2099','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','3','2','1295','2590.00','1360.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','4','2','1295','2590.00','1360.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','5','3','1095','3285.00','1150.0','1630','SAL / DUP','P3','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','6','2','1095','2190.00','1150.0','1650','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','7','1','1045','1045.00','1097.0','1650','SAL / DUP','null','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','8','2','1295','2590.00','1360.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','9','2','1345','2690.00','1412.0','1999','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','10','3','1395','4185.00','1465.0','2099','SAL / DUP','P3','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','11','1','1295','1295.00','1360.0','1990','SAL / DUP','null','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/108','CHAWLA TRADERS','2017-09-15','PR/1718/124','Credit','843','12','2','895','1790.00','940.0','1299','SAL / DUP','P2','SSSLK 5515','29530.00','590.60','28939.4','0','-.37','30386.00','0','0.00','0.00','null','CHAWLA TRADERS','PURCHASE 5%','2017-09-15','null','24.0','NO','null','null','0','1446.97','IGST 5%','-0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','1','4','695','2780.00','730.0','1030','','P4','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','2','3','799','2397.00','839.0','1150','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','3','5','625','3125.00','656.0','899','','P5','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','4','3','1175','3525.00','1234.0','1660','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','5','2','875','1750.00','919.0','1290','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','6','4','575','2300.00','604.0','820','','P4','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','7','3','550','1650.00','578.0','799','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','8','2','975','1950.00','1024.0','1430','','P2','SSKUR 6104','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','9','2','775','1550.00','814.0','1094','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','10','3','895','2685.00','940.0','1299','','P3','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','11','2','975','1950.00','1024.0','1430','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','12','4','695','2780.00','730.0','999','','P4','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','13','2','1050','2100.00','1103.0','1550','','P2','SSSLK 5515','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','14','4','1175','4700.00','1234.0','1670','','P4','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','15','2','1050','2100.00','1103.0','1660','','P2','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','16','3','475','1425.00','499.0','660','','P3','SSCOT 5208','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','17','2','575','1150.00','604.0','899','','P2','SSKUR 6204','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','18','2','750','1500.00','788.0','1190','','P2','SSKUR 6204','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/125','Credit','316','19','3','575','1725.00','604.0','920','','P3','SSKUR 6204','43142.00','0.00','43142.0','0','-.10','45299.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','55.0','NO','null','null','0','2157.1','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','1','2','550','1100.00','578.0','799','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','2','2','550','1100.00','578.0','799','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','3','3','875','2625.00','919.0','1290','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','4','3','895','2685.00','940.0','1299','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','5','3','799','2397.00','839.0','1150','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','6','4','995','3980.00','1045.0','1470','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','7','3','799','2397.00','839.0','1150','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','8','3','795','2385.00','835.0','1130','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','9','3','550','1650.00','578.0','780','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','10','4','525','2100.00','551.0','770','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','11','2','1050','2100.00','1103.0','1550','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','12','14','1195','16730.00','1255.0','1750','','null','SSSLK 5515','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','13','4','1395','5580.00','1465.0','1999','','P4','SSSLK 5515','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','14','1','995','995.00','1045.0','1460','','null','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','15','3','1295','3885.00','1360.0','1899','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','16','4','695','2780.00','730.0','1030','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','17','2','1195','2390.00','1255.0','1750','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','18','2','1295','2590.00','1360.0','1899','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','19','3','995','2985.00','1045.0','1450','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','20','4','525','2100.00','551.0','760','','P4','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','21','3','675','2025.00','709.0','999','','P3','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','22','2','995','1990.00','1045.0','1460','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','23','1','1295','1295.00','1360.0','1899','','null','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','24','1','1095','1095.00','1150.0','1550','','null','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','25','2','995','1990.00','1045.0','1450','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','26','2','1475','2950.00','1549.0','2160','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/1/54','SHAH CHIRAGBHAI DEEPAK KUMAR','2017-09-15','PR/1718/126','Credit','315','27','2','1295','2590.00','1360.0','1899','','P2','SSCOT 5208','78489.00','0.00','78489.0','0','-.45','82413.00','0','0.0','0.0','null','SHAH CHIRAGBHAI DEEPAK KUMAR','PURCHASE 5%','2017-09-15','null','82.0','NO','null','null','0','3924.45','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','1','5','550','2750.00','578.0','850','','P5','SSDUP 6307','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','2','3','850','2550.00','893.0','1260','','P3','SSKUR 6307','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','3','2','950','1900.00','998.0','1399','','P2','SSKUR 6307','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/9/2','PVs CHIKAN CREATION','2017-09-06','PR/1718/127','Credit','B/58/17118','4','1','2250','2250.00','2363.0','3299','','null','SSSLK 5515','9450.00','0.00','9450.0','0','-3','9920.00','0','0.0','0.0','null','PVs CHIKAN CREATION','PURCHASE 5%','2017-09-06','null','11.0','NO','null','null','0','473','IGST 5%','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','1','18','260','4680.00','273.0','399','','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','2','4','450','1800.00','473.0','650','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','3','9','280','2520.00','294.0','450','P9','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','4','8','535','4280.00','562.0','780','','P8','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','5','9','180','1620.00','189.0','280','P9','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','6','5','535','2675.00','562.0','770','','P5','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','7','4','450','1800.00','473.0','650','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','8','4','595','2380.00','625.0','860','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','9','4','510','2040.00','536.0','760','','P4','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','10','34','575','19550.00','604.0','860','P34','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','11','40','165','6600.00','173.0','230','P40','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','12','35','180','6300.00','189.0','250','P35','null','SRSYP 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/7/399','PEEHAR SAREES','2017-09-27','PR/1718/128','Credit','51','13','11','635','6985.00','667.0','930','P11','null','SRASL 5407','63230.00','0.00','63230.0','2.5','0','66392.0','2.5','1581','1581','SGST 2.5%','PEEHAR SAREES','PURCHASE 5%','2017-09-27','null','185.0','NO','CGST 2.5%','null','0','0.0','null','0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','1','1','999','999.00','1049.0','1330','PLTNIM-700','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','2','1','999','999.00','1049.0','1330','PLTNIM-701','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','3','1','999','999.00','1049.0','1330','PLTNIM-702','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','4','1','999','999.00','1049.0','1330','PLTNIM-703','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','5','1','999','999.00','1049.0','1330','PLTNIM-704','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','6','1','999','999.00','1049.0','1330','PLTNIM-705','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','7','1','999','999.00','1049.0','1330','PLTNIM-706','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','8','1','999','999.00','1049.0','1330','PLTNIM-707','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','9','1','999','999.00','1049.0','1330','PLTNIM-709','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','10','1','999','999.00','1049.0','1330','PLTNIM-710','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','11','1','999','999.00','1049.0','1330','PLTNIM-711','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','12','1','999','999.00','1049.0','1330','PLTNIM-712','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','13','1','999','999.00','1049.0','1330','PLTNIM-713','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','14','1','999','999.00','1049.0','1330','PLTNIM-715','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','15','1','999','999.00','1049.0','1399','PLTNIM-716','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','16','1','999','999.00','1049.0','1330','PLTNIM-717','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','17','1','999','999.00','1049.0','1330','PLTNIM-718','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/33','P.A.MARKETING 2017-18','2017-09-23','PR/1718/129','Credit','370','18','1','999','999.00','1049.0','1330','PLTNIM-719','null','SRSYP 5407','17982.00','539.46','17442.54','2.5','0.18','18315.0','2.5','436.14','436.14','SGST 2.5%','P.A.MARKETING 2017-18','PURCHASE 5%','2017-09-23','IW','18.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/6/11','SWAPNA SUNDARI(PURCHASE)','2017-09-29','PR/1718/130','Credit','155','1','43','240','10320.00','252.0','310','2','null','LEGI 5208','10320.00','492.00','9828.0','2.5','0','10320.0','2.5','246','246','SGST 2.5%','SWAPNA SUNDARI(PURCHASE)','PURCHASE 5%','2017-09-29','TWIST','43.0','NO','CGST 2.5%','FREIGHT','0','0.0','null','0.00'),
('P/1/119','SHREE ANJANA ENTERPRISE','2017-09-23','PR/1718/131','Credit','1962','1','80.60','325','26195.00','341.0','522','','null','RA5515','26195.00','5','26190.0','0','0','27500.00','0','0.00','0.00','null','SHREE ANJANA ENTERPRISE','PURCHASE 5%','2017-09-23','null','80.6','NO','null','FREIGHT','0','1310','IGST 5%','0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','1','12','651','7812.00','684.0','899','','null','SSSLK  5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','2','12','775','9300.00','814.0','1130','','null','SSCOT 6204','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','3','6','1395','8370.00','1465.0','2050','','P6','SSSLK  5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','4','7','1340','9380.00','1407.0','1999','','null','SSSLK  5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','5','8','699','5592.00','734.0','1130','PNT','P8','SSKUR 6307','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','6','9','699','6291.00','734.0','1099','','null','SSKUR 6307','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','7','9','425','3825.00','446.0','650','','null','SSBTM 5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/2/281','SUBHAM FACTORY OUTLET','2017-09-29','PR/1718/132','Credit','1074','8','6','625','3750.00','656.0','980','','null','SSKUR 5407','54320.00','2716.20','51603.8','0','0','54184.0','0','0.0','0.0','null','SUBHAM FACTORY OUTLET','PURCHASE 5%','2017-09-29','null','69.0','NO','null','null','0','2580.2','IGST 5%','-0.00'),
('P/6/32','MAHENDRA TRADERS 2017-18','2017-09-28','PR/1718/133','Credit','1034','1','12','869','10428.00','912.0','1199','NZAKAT-001','null','SRSYP 5407','10428.00','312.7999999999993','10115.2','2.5','0.04','10621.0','2.5','252.88','252.88','SGST 2.5%','MAHENDRA TRADERS 2017-18','PURCHASE 5%','2017-09-28','null','12.0','NO','CGST 2.5%','null','0','0.0','null','-0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','1','2','1795','3590.00','1885.0','2999','LEG / DUP','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','2','2','2995','5990.00','3145.0','4999','LEG / DUP ','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','3','1','4995','4995.00','5245.0','7999','LEG / DUP ','null','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','4','2','4095','8190.00','4300.0','6999','LEG / DUP','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','5','2','1995','3990.00','2095.0','3299','LEG / DUP ','P2','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','6','3','1595','4785.00','1675.0','2550','LEG / DUP','P3','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','7','10','1650','16500.00','1733.0','2699','LEG / DUP','null','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/1/205','SHIVAM FASHION','2017-09-27','PR/1718/134','Credit','2192','8','7','1850','12950.00','1943.0','2999','LEG / DUP ','null','12% SSKUR 6204','60990.00','0.0','60990.0','0','0','68309.0','0','0.0','0.0','null','SHIVAM FASHION','PURCHASE 12%','2017-09-27','null','29.0','NO','null','null','0','7319','IGST 12%','0.00'),
('P/7/439','JAMBOO STORE','2017-08-30','PR/1718/135','Credit','1359','1','2','235','470.00','247.0','290','','null','SST&B 5208','470.00','22.00','448.0','2.5','0','470.0','2.5','11','11','SGST 2.5%','JAMBOO STORE','PURCHASE 5%','2017-08-30','null','2.0','NO','CGST 2.5%','null','0','0.0','null','0.00');

/*Table structure for table `stock1` */

DROP TABLE IF EXISTS `stock1`;

CREATE TABLE `stock1` (
  `Supplier_id` varchar(45) DEFAULT NULL,
  `Supplier_name` varchar(45) DEFAULT NULL,
  `Invoice_date` varchar(45) DEFAULT NULL,
  `Invoice_number` varchar(45) DEFAULT NULL,
  `Partypurchase_date` varchar(45) DEFAULT NULL,
  `Purchase_type` varchar(45) DEFAULT NULL,
  `Refrence_number` varchar(45) DEFAULT NULL,
  `Sno` varchar(45) DEFAULT NULL,
  `Qty` varchar(45) DEFAULT NULL,
  `Rate` varchar(45) DEFAULT NULL,
  `Amount` varchar(45) DEFAULT NULL,
  `Retail` varchar(45) DEFAULT NULL,
  `Bill_amount` varchar(45) DEFAULT NULL,
  `discount` varchar(45) DEFAULT NULL,
  `amount_after_discount` varchar(45) DEFAULT NULL,
  `otherchargesaccount` varchar(45) DEFAULT NULL,
  `Other_charges` varchar(45) DEFAULT NULL,
  `igst_account` varchar(45) DEFAULT NULL,
  `igst_amount` varchar(45) DEFAULT NULL,
  `sgst_account` varchar(45) DEFAULT NULL,
  `sgst_amount` varchar(45) DEFAULT NULL,
  `cgst_account` varchar(45) DEFAULT NULL,
  `cgst_amount` varchar(45) DEFAULT NULL,
  `Net_amount` varchar(45) DEFAULT NULL,
  `Pdesc` varchar(45) DEFAULT NULL,
  `Desc` varchar(45) DEFAULT NULL,
  `Product` varchar(45) DEFAULT NULL,
  `Byforgation` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `samebarcode` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL,
  `total_igst_amnt_5` varchar(45) DEFAULT NULL,
  `total_igst_amnt_12` varchar(45) DEFAULT NULL,
  `total_sgst_amnt_25` varchar(45) DEFAULT NULL,
  `total_sgst_amnt_6` varchar(45) DEFAULT NULL,
  `total_cgst_amnt_25` varchar(45) DEFAULT NULL,
  `total_cgst_amnt_6` varchar(45) DEFAULT NULL,
  `Freight_Gst` varchar(45) DEFAULT NULL,
  `Freight_Gst_Amount` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stock1` */

/*Table structure for table `stockchecking` */

DROP TABLE IF EXISTS `stockchecking`;

CREATE TABLE `stockchecking` (
  `stock_no` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockchecking` */

/*Table structure for table `stockid` */

DROP TABLE IF EXISTS `stockid`;

CREATE TABLE `stockid` (
  `invoice_number` varchar(45) DEFAULT NULL,
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `Party` varchar(45) DEFAULT NULL,
  `Da_te` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `qnt` double DEFAULT NULL,
  `nooftag` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid` */

/*Table structure for table `stockid2` */

DROP TABLE IF EXISTS `stockid2`;

CREATE TABLE `stockid2` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `qnt` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid2` */

/*Table structure for table `stockid21` */

DROP TABLE IF EXISTS `stockid21`;

CREATE TABLE `stockid21` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid21` */

/*Table structure for table `stockid22` */

DROP TABLE IF EXISTS `stockid22`;

CREATE TABLE `stockid22` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid22` */

/*Table structure for table `stockid23` */

DROP TABLE IF EXISTS `stockid23`;

CREATE TABLE `stockid23` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid23` */

/*Table structure for table `stockid24` */

DROP TABLE IF EXISTS `stockid24`;

CREATE TABLE `stockid24` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Stock_No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid24` */

/*Table structure for table `stockid241` */

DROP TABLE IF EXISTS `stockid241`;

CREATE TABLE `stockid241` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Stock_No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid241` */

/*Table structure for table `stockid242` */

DROP TABLE IF EXISTS `stockid242`;

CREATE TABLE `stockid242` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Stock_No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid242` */

/*Table structure for table `stockid25` */

DROP TABLE IF EXISTS `stockid25`;

CREATE TABLE `stockid25` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid25` */

/*Table structure for table `stockid26` */

DROP TABLE IF EXISTS `stockid26`;

CREATE TABLE `stockid26` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sto_date` varchar(45) DEFAULT NULL,
  `supplier_name` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockid26` */

/*Table structure for table `stockid_old` */

DROP TABLE IF EXISTS `stockid_old`;

CREATE TABLE `stockid_old` (
  `invoice_number` varchar(45) DEFAULT NULL,
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `Pro_duct` varchar(45) DEFAULT NULL,
  `Party` varchar(45) DEFAULT NULL,
  `Da_te` varchar(45) DEFAULT NULL,
  `articleno` varchar(45) DEFAULT NULL,
  `sno` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) NOT NULL,
  `ismeter` tinyint(1) DEFAULT NULL,
  `qnt` double DEFAULT NULL,
  `nooftag` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `stockid_old` */

/*Table structure for table `stockissue` */

DROP TABLE IF EXISTS `stockissue`;

CREATE TABLE `stockissue` (
  `issue_no` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockissue` */

/*Table structure for table `stockprint` */

DROP TABLE IF EXISTS `stockprint`;

CREATE TABLE `stockprint` (
  `Stock_No` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Re_tail` varchar(45) DEFAULT NULL,
  `De_sc` varchar(45) DEFAULT NULL,
  `P_Desc` varchar(45) DEFAULT NULL,
  `byforgation` varchar(45) DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `article_no` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockprint` */

/*Table structure for table `stockrecieved` */

DROP TABLE IF EXISTS `stockrecieved`;

CREATE TABLE `stockrecieved` (
  `Reference_no` varchar(45) DEFAULT NULL,
  `St_date` varchar(45) DEFAULT NULL,
  `From_branch` varchar(45) DEFAULT NULL,
  `Stock_no` varchar(45) NOT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Quant_ity` varchar(45) DEFAULT NULL,
  `amoun_t` varchar(45) DEFAULT NULL,
  `prod_desc` varchar(45) DEFAULT NULL,
  `ret_ail` varchar(45) DEFAULT NULL,
  `desc_ription` varchar(45) DEFAULT NULL,
  `Byforgation` varchar(45) DEFAULT NULL,
  `Produ_ct` varchar(45) DEFAULT NULL,
  `vsn` varchar(45) DEFAULT NULL,
  `totalqnty` varchar(45) DEFAULT NULL,
  `totalamnt` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockrecieved` */

/*Table structure for table `stocktransfer` */

DROP TABLE IF EXISTS `stocktransfer`;

CREATE TABLE `stocktransfer` (
  `Reference_no` varchar(45) DEFAULT NULL,
  `St_date` varchar(45) DEFAULT NULL,
  `To_branch` varchar(45) DEFAULT NULL,
  `Stock_no` varchar(45) DEFAULT NULL,
  `Ra_te` varchar(45) DEFAULT NULL,
  `Quant_ity` varchar(45) DEFAULT NULL,
  `amoun_t` varchar(45) DEFAULT NULL,
  `prod_desc` varchar(45) DEFAULT NULL,
  `ret_ail` varchar(45) DEFAULT NULL,
  `desc_ription` varchar(45) DEFAULT NULL,
  `Byforgation` varchar(45) DEFAULT NULL,
  `Produ_ct` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `totalqnty` varchar(45) DEFAULT NULL,
  `totalamnt` varchar(45) DEFAULT NULL,
  `Party_code` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stocktransfer` */

/*Table structure for table `tag` */

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag` (
  `Tagno` int(10) unsigned NOT NULL,
  `PRate` varchar(255) DEFAULT NULL,
  `RRate` varchar(255) DEFAULT NULL,
  `Product` varchar(255) DEFAULT NULL,
  `byforgation` varchar(255) DEFAULT NULL,
  `Article_no` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `PDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Tagno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tag` */

/*Table structure for table `tagpositions` */

DROP TABLE IF EXISTS `tagpositions`;

CREATE TABLE `tagpositions` (
  `labelname` varchar(45) DEFAULT NULL,
  `xposition` varchar(45) DEFAULT NULL,
  `yposition` varchar(45) DEFAULT NULL,
  `width` varchar(45) DEFAULT NULL,
  `height` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tagpositions` */

/*Table structure for table `tobank` */

DROP TABLE IF EXISTS `tobank`;

CREATE TABLE `tobank` (
  `s_no` varchar(45) DEFAULT '0',
  `ba_nk` varchar(45) DEFAULT NULL,
  `amo_unt` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `Da_te` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tobank` */

/*Table structure for table `toho` */

DROP TABLE IF EXISTS `toho`;

CREATE TABLE `toho` (
  `s_no` varchar(45) DEFAULT '0',
  `Cash` varchar(45) DEFAULT NULL,
  `amo_unt` varchar(45) DEFAULT NULL,
  `branchid` varchar(45) DEFAULT NULL,
  `by_ledger` varchar(45) DEFAULT NULL,
  `to_ledger` varchar(45) DEFAULT NULL,
  `Da_te` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `toho` */

/*Table structure for table `vehicle_insurance` */

DROP TABLE IF EXISTS `vehicle_insurance`;

CREATE TABLE `vehicle_insurance` (
  `type` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `insurance_no` varchar(255) DEFAULT NULL,
  `insurance_renew_date` varchar(255) DEFAULT NULL,
  `insurance_duration` varchar(255) DEFAULT NULL,
  `insurance_company` varchar(255) DEFAULT NULL,
  `insurance_amount` varchar(255) DEFAULT NULL,
  `vehicle_purch_date` varchar(255) DEFAULT NULL,
  `agent_name` varchar(255) DEFAULT NULL,
  `Agent_contact no` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vehicle_insurance` */

/*Table structure for table `year` */

DROP TABLE IF EXISTS `year`;

CREATE TABLE `year` (
  `year` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `year` */

insert  into `year`(`year`) values 
('1819');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
