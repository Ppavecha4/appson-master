package Main;

import connection.connection;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author kdkushdakh
 */
public class Exportdata1 extends javax.swing.JFrame implements ActionListener,
        PropertyChangeListener {

    Task task;
    String Stock = null;

    @Override
    public void actionPerformed(ActionEvent e) {
        jButton1.setEnabled(false);
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        //Instances of javax.swing.SwingWorker are not reusuable, so
        //we create new instances as needed.
        task = new Task();
        task.addPropertyChangeListener(this);
        task.execute();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress".equals(evt.getPropertyName())) {
            // progress = (Integer) evt.getNewValue();

            jProgressBar1.setString("data importing...");
            // System.out.println("progress : " + progress);
            /*jTextArea1.append(String.format(
                    "Completed %d%% of task.\n", task.getProgress()));*/
        }
    }

    /**
     * Creates new form Exportdata
     */
    /* start swing worker class*/
    class Task extends SwingWorker<Void, Void> {

        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
            FileOutputStream fout;
            BufferedWriter writer = null;
            Tobranch = (String) jComboBox1.getSelectedItem();
            System.out.println("D:\\Appson data\\data" + Tobranch + "\\" + "data" + Tobranch + "-" + dateNow + ".sql");
            int i2 = JOptionPane.showConfirmDialog(rootPane, "DO you want to Export the Stock");
            if (i2 == 0) {

                jProgressBar1.setVisible(true);
                jProgressBar1.setString("Stock is Transefered Please Wait...");

                try {

                    fout = new FileOutputStream("D:\\Appson data\\data" + Tobranch + "\\" + "data" + Tobranch + "-" + dateNow + ".sql");

                    writer = new BufferedWriter(new FileWriter("D:\\Appson data\\data" + Tobranch + "\\" + "data" + Tobranch + "-" + dateNow + ".sql"));

                    // writer.write("//hello i am ankush solanki");
                    //writer.newLine();
                    int rowcount = jTable1.getRowCount();
                    connection c = new connection();

                    Connection connect = c.cone();

                    Statement st = connect.createStatement();
                    Statement st1 = connect.createStatement();
                    Statement st2 = connect.createStatement();
                    Statement st3 = connect.createStatement();
                    Statement st4 = connect.createStatement();
                    Statement st5 = connect.createStatement();
                    Statement st6 = connect.createStatement();
                    Statement st7 = connect.createStatement();
                    Statement st8 = connect.createStatement();
                    for (int i = 0; i < rowcount; i++) {
                        String reference = jTable1.getValueAt(i, 0) + "";

                        ResultSet rs = st.executeQuery("select * from stocktransfer where Reference_no = '" + reference + "'");

                        while (rs.next()) {
                            Stock = rs.getString(4);
                            //   System.out.println("value of stock is " + Stock);
                            st2.executeUpdate("delete from stockid2 where Stock_no='" + Stock + "'");

                            ResultSet rs1 = st1.executeQuery("select * from stockid where Stock_No='" + Stock + "' ");
                            while (rs1.next()) {

                                String sql = "INSERT into StockId2 values('" + Stock + "','" + rs1.getString(3) + "','" + rs1.getString(4) + "','" + rs1.getString(5) + "','" + rs1.getString(6) + "','" + rs1.getString(7) + "','" + rs1.getString(8) + "','" + rs1.getString(11) + "','" + rs1.getString(10) + "','" + rs1.getString(9) + "','0')";
//         st1.executeUpdate("INSERT into StockId2"+Tobranch+" values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')");                                     
                                writer.write(sql + "/t@#");
                                if (Tobranch.equals("1")) {
                                    String sql1 = "INSERT into StockId21 values('" + Stock + "','" + rs1.getString(3) + "','" + rs1.getString(4) + "','" + rs1.getString(5) + "','" + rs1.getString(6) + "','" + rs1.getString(7) + "','" + rs1.getString(8) + "','" + rs1.getString(11) + "','" + rs1.getString(10) + "','" + rs1.getString(9) + "','0')";
                                    // writer.write(sql1 + "/t@#");
                                    st7.executeUpdate(sql1);

                                }
                                if (Tobranch.equals("2")) {
                                    String sql1 = "INSERT into StockId22 values('" + Stock + "','" + rs1.getString(3) + "','" + rs1.getString(4) + "','" + rs1.getString(5) + "','" + rs1.getString(6) + "','" + rs1.getString(7) + "','" + rs1.getString(8) + "','" + rs1.getString(11) + "','" + rs1.getString(10) + "','" + rs1.getString(9) + "','0')";
                                    // writer.write(sql1 + "/t@#");
                                    st2.executeUpdate(sql1);
                                }
                                if (Tobranch.equals("3")) {
                                    String sql1 = "INSERT into StockId23 values('" + Stock + "','" + rs1.getString(3) + "','" + rs1.getString(4) + "','" + rs1.getString(5) + "','" + rs1.getString(6) + "','" + rs1.getString(7) + "','" + rs1.getString(8) + "','" + rs1.getString(11) + "','" + rs1.getString(10) + "','" + rs1.getString(9) + "','0')";
                                    // writer.write(sql1 + "/t@#");

                                }
                                if (Tobranch.equals("4")) {
                                    String sql1 = "INSERT into StockId24 values('" + Stock + "','" + rs1.getString(3) + "','" + rs1.getString(4) + "','" + rs1.getString(5) + "','" + rs1.getString(6) + "','" + rs1.getString(7) + "','" + rs1.getString(8) + "','" + rs1.getString(11) + "','" + rs1.getString(10) + "','" + rs1.getString(9) + "','0')";

                                    // writer.write(sql1 + "/t@#");
                                    st4.executeUpdate(sql1);
                                }
                                if (Tobranch.equals("5")) {
                                    String sql1 = "INSERT into StockId25 values('" + Stock + "','" + rs1.getString(3) + "','" + rs1.getString(4) + "','" + rs1.getString(5) + "','" + rs1.getString(6) + "','" + rs1.getString(7) + "','" + rs1.getString(8) + "','" + rs1.getString(11) + "','" + rs1.getString(10) + "','" + rs1.getString(9) + "','0')";
                                    //writer.write(sql1 + "/t@#");
                                    st5.executeUpdate(sql1);

                                }

                                //   c.query(sql, Tobranch);
                                // writer.write(sql);
                                //  writer.newLine();
                            }

                            String sql2 = "INSERT into StockRecieved values('" + rs.getString(1) + "','" + dateNow1 + "','0','" + Stock + "','" + rs.getString(5) + "','" + rs.getString(6) + "','" + rs.getString(7) + "','" + rs.getString(8) + "','" + rs.getString(9) + "','" + rs.getString(10) + "','" + rs.getString(11) + "','" + rs.getString(12) + "','" + rs.getString(13) + "','" + rs.getString(14) + "','" + rs.getString(15) + "','" + rs.getString("Party_code") + "')";

                            writer.write(sql2 + "/t@#");

                        }

                        st6.executeUpdate("insert into stockissue values('" + jTable1.getValueAt(i, 0) + "" + "')");

                    }

                    ResultSet Resultbranchdata = st7.executeQuery("select * from branchdata" + Tobranch + " ");

                    while (Resultbranchdata.next()) {
                        writer.write(Resultbranchdata.getString(1) + "/t@#");
                    }

                    st8.executeUpdate("delete from branchdata" + Tobranch + " ");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    writer.flush();
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(Exportdata1.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            jProgressBar1.setIndeterminate(false);
            jProgressBar1.setValue(100);
            jProgressBar1.setString("Stock Transefered Successfully");

            JOptionPane.showMessageDialog(rootPane, "Stock Transefered successfully");

            dispose();

            return null;
        }

        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done() {
            Toolkit.getDefaultToolkit().beep();
            jButton1.setEnabled(true);
            setCursor(null); //turn off the wait cursor
            /*jTextArea1.append("Done!\n");*/
        }
    }

    /* end swing worker class*/
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH-mm");
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    String dateNow1 = formatter1.format(currentDate.getTime());

    private static Exportdata1 obj = null;

    public Exportdata1() {
        initComponents();
        this.setLocationRelativeTo(null);
        jProgressBar1.setStringPainted(true);
        jProgressBar1.setVisible(false);
        jButton1.setActionCommand("export");
        jButton1.addActionListener(this);
        try {
//            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            connection c = new connection();
            Connection connect = c.cone();

            Statement st = connect.createStatement();
            ResultSet rs2 = st.executeQuery("SELECT  * FROM Branch");
            while (rs2.next()) {
                jComboBox1.addItem(rs2.getString(1));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
    }

    public static Exportdata1 getObj() {
        if (obj == null) {
            obj = new Exportdata1();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jProgressBar1 = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Export Data");
        setLocationByPlatform(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setText("Please select Branch :-");

        jComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jComboBox1FocusLost(evt);
            }
        });

        jButton1.setText("Export");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel2.setText("Transfer Reference:");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TRANSFER REFERENCE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
        }

        jLabel3.setText("Total quantity :-");

        jLabel4.setText("Total Amount: -");

        jProgressBar1.setStringPainted(true);
        jProgressBar1.setIndeterminate(true);
        jProgressBar1.setBackground(Color.green);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(80, 80, 80))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jProgressBar1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(21, 21, 21)
                                .addComponent(jTextField1)
                                .addGap(58, 58, 58))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(5, 5, 5)
                                        .addComponent(jTextField3)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton1)
                                    .addComponent(jButton2)))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(22, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                .addGap(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        /*   int i2 = JOptionPane.showConfirmDialog(rootPane,"DO you want to Export the Stock");
if(i2==0)
{
    ExportProgress ep=new ExportProgress();
    ep.setVisible(true);
try
{
int rowcount=jTable1.getRowCount();

for(int i=0;i<rowcount;i++)
{
    String reference=jTable1.getValueAt(i, 0)+"";
connection c = new connection();
Connection connect = c.cone();
                                    
                                     Statement st =connect.createStatement();
                                     Statement st1 =connect.createStatement();
                                     Statement st2 =connect.createStatement();
                                      Statement st3 =connect.createStatement();
                                       Statement st4 =connect.createStatement();
                                        Statement st5 =connect.createStatement();
                                         Statement st6 =connect.createStatement();
                                          Statement st7 =connect.createStatement();
                                     ResultSet rs=st.executeQuery("select * from stocktransfer where Reference_no = '"+reference+"'");

                                     while(rs.next())
                                     {
                                         
                                       
                                      Stock=rs.getString(4);
                                      
                                         System.out.println("value of stock is "+Stock);
                                      st2.executeUpdate("delete from stockid2 where Stock_no='"+Stock+"'");
                                      
                                     ResultSet rs1=st1.executeQuery("select * from stockid where Stock_No='"+Stock+"' ");
                                     while(rs1.next())
                                     {
                                           
                                      String sql="INSERT into StockId2 values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')";
                             //         st1.executeUpdate("INSERT into StockId2"+Tobranch+" values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')");                                     
                                      
                                      if(Tobranch.equals("1"))
                                     {
                                     String sql1="INSERT into StockId21 values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')";    
                                     st7.executeUpdate(sql1);
                                     }
                                       if(Tobranch.equals("2"))
                                     {
                                     String sql1="INSERT into StockId22 values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')";    
                                     st2.executeUpdate(sql1);
                                     }
                                      if(Tobranch.equals("3"))
                                     {
                                     String sql1="INSERT into StockId23 values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')";    
                                     st3.executeUpdate(sql1);
                                     }
//                                      if(Tobranch.equals("4"))
//                                     {
//                                     String sql1="INSERT into StockId24 values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')";    
//                                     st4.executeUpdate(sql1);
//                                     } 
                                     if(Tobranch.equals("5"))
                                     {
                                     String sql1="INSERT into StockId25 values('"+Stock+"','"+rs1.getString(3) +"','"+rs1.getString(4) +"','"+rs1.getString(5)+"','"+rs1.getString(6) +"','"+rs1.getString(7) +"','"+rs1.getString(8)+"','"+rs1.getString(11) +"','"+rs1.getString(10) +"','"+rs1.getString(9) +"','0')";    
                                     st5.executeUpdate(sql1);
                                     }
                                     
                                      c.query(sql, Tobranch);   
                                     }
                                    
                                     String sql1="INSERT into StockRecieved values('"+rs.getString(1)+"','"+dateNow+"','0','"+Stock+"','"+rs.getString(5)+"','"+rs.getString(6)+"','"+rs.getString(7)+"','"+rs.getString(8)+"','"+rs.getString(9)+"','"+rs.getString(10)+"','"+rs.getString(11)+"','"+rs.getString(12)+"','"+rs.getString(13)+"','"+rs.getString(14)+"','"+rs.getString(15)+"')";
                                     System.out.println(""+sql1);
                                     c.query(sql1, Tobranch);
                                     }
                                         
                                     st6.executeUpdate("insert into stockissue values('"+jTable1.getValueAt(i, 0)+""+"')");
}
    
}
catch(Exception e)
{
     e.printStackTrace();
}
    
    
    
//    try {
//            // TODO add your handling code here:
//           mail m=new mail();
//           m.Tobranch=(String)jComboBox1.getSelectedItem();
//           m.send_mail("ldlovedakh@gmail.com");
//           JOptionPane.showMessageDialog(null, "Backup Sent successfully");
//           FileOutputStream fout = new FileOutputStream("data"+(String)jComboBox1.getSelectedItem()+".sql");
//            String line = "";
//            byte b[] = line.getBytes();//converting string into byte array
//            fout.write(b);
//            fout.close();
//        } catch (IOException ex) {
//            Logger.getLogger(mainMenu.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (MessagingException ex) {
//            Logger.getLogger(mainMenu.class.getName()).log(Level.SEVERE, null, ex);
//        }   
}
JOptionPane.showMessageDialog(rootPane, "Stock Transefered successfully");
dispose();
         */

    }//GEN-LAST:event_jButton1ActionPerformed
    String Tobranch = null;
    private void jComboBox1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusLost
        {
            int i1 = JOptionPane.showConfirmDialog(rootPane, "Have you selected the right Branch");
            if (i1 == 0) {
                jComboBox1.setEnabled(false);
                Tobranch = (String) jComboBox1.getSelectedItem();
            } else {
                jComboBox1.requestFocus();
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1FocusLost
    String transfereference = null;
    ArrayList Alreadytransferreference = new ArrayList();
    ArrayList Alreadybranch = new ArrayList();

    double countqty = 0;
    double sumamount = 0;
    double totalcountqty = 0;
    double totalsumamount = 0;
    boolean istransferrewfrence = false;
    boolean isalreadybranch = false;
    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            istransferrewfrence = false;
            isalreadybranch = false;
            transfereference = jTextField1.getText();
            try {

                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                //  ResultSet rs=st.executeQuery("select Stock_No from Stockid");

                Statement ts = connect.createStatement();
                ResultSet rs = ts.executeQuery("Select * from stocktransfer");
                String a1 = null;
                String matchsno4 = "test";
                while (rs.next()) {
                    String matchsno1 = rs.getString(1);
                    if (matchsno4.equals(matchsno1)) {

                    } else {
                        Alreadytransferreference.add(rs.getString(1) + "");
                        Alreadybranch.add(rs.getString(3) + "");
                        matchsno4 = matchsno1;
                    }

//                  jTextField18.setText(""+ffvat);
                }
            } catch (SQLException ex) {
                Logger.getLogger(Exportdata1.class.getName()).log(Level.SEVERE, null, ex);
            }

            int a = Alreadytransferreference.size();
            int b = Alreadybranch.size();
            for (int i = 0; i < a; i++) {
                if (transfereference.equals(Alreadytransferreference.get(i))) {
                    istransferrewfrence = true;
                }
            }

            for (int i = 0; i < b; i++) {
                if (Tobranch.equals(Alreadybranch.get(i))) {
                    isalreadybranch = true;
                }
            }
            System.out.println("already" + Alreadytransferreference);
            System.out.println("transfer" + transfereference);

            if (istransferrewfrence == false) {
                JOptionPane.showMessageDialog(rootPane, "The Stock Transfer Reference Number is not Valid please enter the Reference Number again ");
                jTextField1.setText(null);
                jTextField1.requestFocus();
            } else if (isalreadybranch == false) {
                JOptionPane.showMessageDialog(rootPane, "The Transfer to this reference number is not commited to the above selected branch ");
                jTextField1.setText(null);
                jTextField1.requestFocus();
            } else {
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                Object o[] = {transfereference};
                dtm.addRow(o);
                try {

                    connection c = new connection();
                    Connection connect = c.cone();

                    Statement st = connect.createStatement();
                    //  ResultSet rs=st.executeQuery("select Stock_No from Stockid");

                    Statement ts = connect.createStatement();
                    ResultSet rs = ts.executeQuery("select count(Stock_no),sum(Ra_te) from stocktransfer where Reference_no='" + transfereference + "'");

                    while (rs.next()) {
                        countqty = Double.parseDouble(rs.getString(1));
                        sumamount = Double.parseDouble(rs.getString(2));
                    }
                    totalcountqty = totalcountqty + countqty;
                    totalsumamount = totalsumamount + sumamount;
                    jTextField2.setText("" + totalcountqty);
                    jTextField3.setText("" + totalsumamount);
                } catch (SQLException ex) {
                    Logger.getLogger(Exportdata1.class.getName()).log(Level.SEVERE, null, ex);
                }
                jTextField1.setText(null);

            }

        }

    }//GEN-LAST:event_jTextField1KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Exportdata1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Exportdata1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Exportdata1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Exportdata1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Exportdata1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    // End of variables declaration//GEN-END:variables
}
