package Main;

import MISC.stockchecking;
import Printing.printbarcode;
import Printing.tagdetail;
import accounting.transaction.Contra;
import accounting.transaction.bankpayment1;
import accounting.transaction.bankreciept;
import accounting.transaction.cashpayment;
import accounting.transaction.cashreciept;
import accounting.transaction.interestentry1;
import accounting.transaction.journ;
import connection.connection;
import group.Creategroup;
import static java.awt.Frame.NORMAL;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import master.article.addArticle;
import master.city.Updatecity1;
import master.city.addcity;
import master.color.byforgation;
import master.color.delbyforgation;
import master.customer.Add_Purchase_Party;
import master.customer.Add_Sales_Party;
import master.customer.Update_Purchase_Party;
import master.customer.Update_Sales_Party;
import master.product.UpdateProduct;
import master.product.addProduct;
import master.staff.ViewStaff;
import master.staff.addMember;
import reporting.accounts.Alterledger;
import reporting.accounts.BalanceSheet1;
import reporting.accounts.CityWiseStockReport;
import reporting.accounts.PartyOutstandingReports;
import reporting.accounts.PartyWisePaymentReport;
import reporting.accounts.PartyWiseStock_AnalysisReport;
import reporting.accounts.ProductWiseStockReport;
import reporting.accounts.PurchaseRateWiseStockReport;
import reporting.accounts.RetailRateWiseStockReport;
import reporting.accounts.Tradingaccount1;
import reporting.accounts.Viewlledger_2;
import reporting.accounts.createLedger;
import reporting.accounts.dailyreport1;
import reporting.accounts.pals1;
import reporting.accounts.tdspayablereport;
import reporting.accounts.tdsrecievablereport;
import reporting.accounts.trailbalwithopen;
import reporting.accounts.viewlledger;
import reporting.accounts.viewlledger1;
import reporting.stock.CityWiseClosingStock;
import reporting.stock.CityWiseDailyPurchaseReport;
import reporting.stock.CityWiseDailySalesReport;
import reporting.stock.Customer_Report;
import reporting.stock.Dailyreport_;
import reporting.stock.Filter_Stcok_Rate;
import reporting.stock.Gst_BillWise_SalesReturn_Report;
import reporting.stock.Gst_BillWise_Sales_Report;
import reporting.stock.PartyWiseDailyPurchaseReport;
import reporting.stock.PartyWiseDailySalesReport;
import reporting.stock.PartyWiseclosingstock;
import reporting.stock.ProductWiseClosingStock;
import reporting.stock.ProductWiseDailyPurchaseReport;
import reporting.stock.ProductWiseDailySalesReport;
import reporting.stock.PurchaseRateWiseClosingStock;
import reporting.stock.RetailRateWiseClosingStock;
import reporting.stock.Staffwise_Sales_Report;
import reporting.stock.dailyreportcashsales;
import reporting.stock.dailyreportdebitsales;
import reporting.stock.dailyreportwithdiscount;
import reporting.stock.defecteditemlist;
import reporting.stock.purchasestockreport;
import reporting.stock.purchasestockreport_dule1;
import transaction.Bill_Update;
import transaction.BsnlPurchase_Update;
import transaction.Bsnl_Billing_GST;
import transaction.Bsnl_Purchase;
import transaction.BsnlaBilling_Update;
import transaction.Create_barcode;
import transaction.New_Dual_Billing;
import transaction.New_Dual_PurchaseReturn;
import transaction.New_Dual_SalesReturn;
import transaction.Pur_trans_dualGst;
import transaction.Purchase_Update;
import transaction.Rate_authentication;
import transaction.Sales_Return_Update;
import transaction.StockTransfer;
import transaction.adddefectedpiece;
import transaction.cashbook;
import transaction.purchasereturngstupdate;

/**
 *
 * @author Aashish
 */
public class MainMenu_1 extends javax.swing.JFrame {

    /**
     * Creates new form MainMenu
     */
    public MainMenu_1() {
        initComponents();
        menuVisibility();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    // Method for Customizing the menu option {Show or hide menu option}
    public void menuVisibility() {
        try {
            String sql = "Select * from setting";
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                String s1 = rs.getString("Purchase_Party");
                String s2 = rs.getString("Sales_Party");
                String s3 = rs.getString("Ledger");
                String s4 = rs.getString("Staff");
                String s5 = rs.getString("Group_Party");
                String s6 = rs.getString("Product");
                String s7 = rs.getString("Byforgation");
                String s8 = rs.getString("Article_No");
                String s9 = rs.getString("City");
                String s10 = rs.getString("Sales_Billing");
                String s11 = rs.getString("Update_Sales_Billing");
                String s12 = rs.getString("Sales_Return");
                String s13 = rs.getString("Purchase");
                String s14 = rs.getString("Purchase_Return");
                String s15 = rs.getString("Rate_Change");
                String s16 = rs.getString("Daily_Sales_Report");
                String s17 = rs.getString("Daily_Purchase_Report");
                String s18 = rs.getString("Tag_Detail");
                String s19 = rs.getString("Stock_Analysis_Report");
                String s20 = rs.getString("Stock_Report");
                String s21 = rs.getString("Tag_Printing");
                String s22 = rs.getString("Contra");
                String s23 = rs.getString("Journal");
                String s24 = rs.getString("Cash_Payment");
                String s25 = rs.getString("Cash_Reciept");
                String s26 = rs.getString("Bank_Payment");
                String s27 = rs.getString("Bank_Reciept");
                String s28 = rs.getString("Day_Book");
                String s29 = rs.getString("Trial_Balance");
                String s30 = rs.getString("Trading_Account");
                String s31 = rs.getString("Balance_Sheet");
                String s32 = rs.getString("Profit_and_Loss");
                String s33 = rs.getString("Taxation_Report");
                String s34 = rs.getString("Party_Outstanding_Report");
                String s35 = rs.getString("Party_Against_Payment_Report");
                String s36 = rs.getString("Backup");
                String s37 = rs.getString("Restore");
                String s38 = rs.getString("Setting");
                String s39 = rs.getString("Stock_Transfer");
                String s40 = rs.getString("Export_To_Branch");
                String s41 = rs.getString("Import_From_Branch");
                String s42 = rs.getString("Sales_Return_update");
                String s43 = rs.getString("Purchase_Update");
                String s44 = rs.getString("Purchase_Return_update");
                String s45 = rs.getString("BSNL_Billing");
                String s46 = rs.getString("BSNL_Purchase");
                String s47 = rs.getString("Interest_Entry");
                String s48 = rs.getString("Stock_Transfer_Report");
                String s49 = rs.getString("Daily_Report");
                String s50 = rs.getString("Customer_Report");

                if ("Enable".equalsIgnoreCase(s1)) {
                    jMenu10.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s1)) {
                    jMenu10.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s2)) {
                    jMenu11.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s2)) {
                    jMenu11.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s3)) {
                    jMenu12.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s3)) {
                    jMenu12.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s4)) {
                    jMenu13.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s4)) {
                    jMenu13.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s5)) {
                    jMenu14.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s5)) {
                    jMenu14.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s6)) {
                    jMenu15.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s6)) {
                    jMenu15.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s7)) {
                    jMenu16.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s7)) {
                    jMenu16.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s8)) {
                    jMenu17.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s8)) {
                    jMenu17.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s9)) {
                    jMenuItem18.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s9)) {
                    jMenuItem18.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s10)) {
                    jMenuItem20.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s10)) {
                    jMenuItem20.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s11)) {
                    jMenuItem21.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s11)) {
                    jMenuItem21.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s12)) {
                    jMenuItem22.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s12)) {
                    jMenuItem22.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s13)) {
                    jMenu21.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s13)) {
                    jMenu21.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s39)) {
                    jMenu23.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s39)) {
                    jMenu23.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s14)) {
                    jMenuItem26.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s14)) {
                    jMenuItem26.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s15)) {
                    jMenu32.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s15)) {
                    jMenu32.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s16)) {
                    jMenu25.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s16)) {
                    jMenu25.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s17)) {
                    jMenu26.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s17)) {
                    jMenu26.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s18)) {
                    jMenuItem56.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s18)) {
                    jMenuItem56.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s19)) {
                    jMenu29.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s19)) {
                    jMenu29.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s20)) {
                    jMenu27.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s20)) {
                    jMenu27.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s21)) {
                    jMenuItem59.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s21)) {
                    jMenuItem59.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s22)) {
                    jMenuItem60.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s22)) {
                    jMenuItem60.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s23)) {
                    jMenuItem61.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s23)) {
                    jMenuItem61.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s24)) {
                    jMenuItem62.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s24)) {
                    jMenuItem62.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s25)) {
                    jMenuItem63.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s5)) {
                    jMenuItem63.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s26)) {
                    jMenuItem64.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s26)) {
                    jMenuItem64.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s27)) {
                    jMenuItem65.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s27)) {
                    jMenuItem65.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s28)) {
                    jMenuItem66.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s28)) {
                    jMenuItem66.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s29)) {
                    jMenuItem67.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s29)) {
                    jMenuItem67.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s30)) {
                    jMenuItem68.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s30)) {
                    jMenuItem68.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s31)) {
                    jMenuItem69.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s31)) {
                    jMenuItem69.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s32)) {
                    jMenuItem70.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s32)) {
                    jMenuItem70.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s33)) {
                    jMenu31.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s33)) {
                    jMenu31.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s34)) {
                    jMenuItem71.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s34)) {
                    jMenuItem71.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s35)) {
                    jMenuItem72.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s35)) {
                    jMenuItem72.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s36)) {
                    jMenuItem81.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s36)) {
                    jMenuItem81.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s37)) {
                    jMenuItem82.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s37)) {
                    jMenuItem82.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s38)) {
                    jMenu9.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s38)) {
                    jMenu9.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s40)) {
                    jMenuItem79.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s40)) {
                    jMenuItem79.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s41)) {
                    jMenuItem80.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s41)) {
                    jMenuItem80.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s42)) {
                    jMenuItem23.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s42)) {
                    jMenuItem23.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s43)) {
                    jMenuItem25.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s43)) {
                    jMenuItem25.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s44)) {
                    jMenuItem27.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s44)) {
                    jMenuItem27.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s45)) {
                    jMenu32.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s45)) {
                    jMenu32.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s46)) {
                    jMenu33.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s46)) {
                    jMenu33.setVisible(false);
                }
                if ("Enable".equalsIgnoreCase(s47)) {
                    jMenuItem87.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s47)) {
                    jMenuItem87.setVisible(false);
                }
                  if ("Enable".equalsIgnoreCase(s48)) {
                    jMenu34.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s48)) {
                    jMenu34.setVisible(false);
                }
                 if ("Enable".equalsIgnoreCase(s49)) {
                    jMenu40.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s49)) {
                    jMenu40.setVisible(false);
                }
                 if ("Enable".equalsIgnoreCase(s50)) {
                    jMenuItem103.setVisible(true);
                } else if ("Disable".equalsIgnoreCase(s50)) {
                    jMenuItem103.setVisible(false);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel75 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu10 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu11 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu12 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenu13 = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenu14 = new javax.swing.JMenu();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenu15 = new javax.swing.JMenu();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenu16 = new javax.swing.JMenu();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenu17 = new javax.swing.JMenu();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenu18 = new javax.swing.JMenu();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu19 = new javax.swing.JMenu();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jMenu20 = new javax.swing.JMenu();
        jMenuItem22 = new javax.swing.JMenuItem();
        jMenuItem23 = new javax.swing.JMenuItem();
        jMenu21 = new javax.swing.JMenu();
        jMenuItem24 = new javax.swing.JMenuItem();
        jMenuItem25 = new javax.swing.JMenuItem();
        jMenu22 = new javax.swing.JMenu();
        jMenuItem26 = new javax.swing.JMenuItem();
        jMenuItem27 = new javax.swing.JMenuItem();
        jMenu32 = new javax.swing.JMenu();
        jMenuItem83 = new javax.swing.JMenuItem();
        jMenuItem84 = new javax.swing.JMenuItem();
        jMenu33 = new javax.swing.JMenu();
        jMenuItem85 = new javax.swing.JMenuItem();
        jMenuItem86 = new javax.swing.JMenuItem();
        jMenu23 = new javax.swing.JMenu();
        jMenuItem28 = new javax.swing.JMenuItem();
        jMenuItem29 = new javax.swing.JMenuItem();
        jMenu24 = new javax.swing.JMenu();
        jMenuItem30 = new javax.swing.JMenuItem();
        jMenuItem31 = new javax.swing.JMenuItem();
        jMenuItem32 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu25 = new javax.swing.JMenu();
        jMenuItem33 = new javax.swing.JMenuItem();
        jMenuItem34 = new javax.swing.JMenuItem();
        jMenuItem35 = new javax.swing.JMenuItem();
        jMenuItem36 = new javax.swing.JMenuItem();
        jMenuItem37 = new javax.swing.JMenuItem();
        jMenuItem38 = new javax.swing.JMenuItem();
        jMenuItem39 = new javax.swing.JMenuItem();
        jMenuItem40 = new javax.swing.JMenuItem();
        jMenuItem41 = new javax.swing.JMenuItem();
        jMenu26 = new javax.swing.JMenu();
        jMenuItem42 = new javax.swing.JMenuItem();
        jMenuItem43 = new javax.swing.JMenuItem();
        jMenuItem44 = new javax.swing.JMenuItem();
        jMenuItem45 = new javax.swing.JMenuItem();
        jMenu27 = new javax.swing.JMenu();
        jMenuItem46 = new javax.swing.JMenuItem();
        jMenuItem47 = new javax.swing.JMenuItem();
        jMenuItem48 = new javax.swing.JMenuItem();
        jMenu28 = new javax.swing.JMenu();
        jMenuItem49 = new javax.swing.JMenuItem();
        jMenuItem50 = new javax.swing.JMenuItem();
        jMenu29 = new javax.swing.JMenu();
        jMenuItem51 = new javax.swing.JMenuItem();
        jMenuItem52 = new javax.swing.JMenuItem();
        jMenuItem53 = new javax.swing.JMenuItem();
        jMenu30 = new javax.swing.JMenu();
        jMenuItem54 = new javax.swing.JMenuItem();
        jMenuItem55 = new javax.swing.JMenuItem();
        jMenuItem56 = new javax.swing.JMenuItem();
        jMenuItem57 = new javax.swing.JMenuItem();
        jMenuItem58 = new javax.swing.JMenuItem();
        jMenu34 = new javax.swing.JMenu();
        jMenu35 = new javax.swing.JMenu();
        jMenuItem89 = new javax.swing.JMenuItem();
        jMenuItem90 = new javax.swing.JMenuItem();
        jMenuItem91 = new javax.swing.JMenuItem();
        jMenuItem92 = new javax.swing.JMenuItem();
        jMenu36 = new javax.swing.JMenu();
        jMenu37 = new javax.swing.JMenu();
        jMenuItem93 = new javax.swing.JMenuItem();
        jMenuItem94 = new javax.swing.JMenuItem();
        jMenu38 = new javax.swing.JMenu();
        jMenuItem95 = new javax.swing.JMenuItem();
        jMenuItem96 = new javax.swing.JMenuItem();
        jMenuItem97 = new javax.swing.JMenuItem();
        jMenu39 = new javax.swing.JMenu();
        jMenuItem98 = new javax.swing.JMenuItem();
        jMenuItem99 = new javax.swing.JMenuItem();
        jMenu40 = new javax.swing.JMenu();
        jMenuItem100 = new javax.swing.JMenuItem();
        jMenuItem103 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem59 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem60 = new javax.swing.JMenuItem();
        jMenuItem61 = new javax.swing.JMenuItem();
        jMenuItem62 = new javax.swing.JMenuItem();
        jMenuItem63 = new javax.swing.JMenuItem();
        jMenuItem64 = new javax.swing.JMenuItem();
        jMenuItem65 = new javax.swing.JMenuItem();
        jMenuItem87 = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem66 = new javax.swing.JMenuItem();
        jMenuItem67 = new javax.swing.JMenuItem();
        jMenuItem68 = new javax.swing.JMenuItem();
        jMenuItem69 = new javax.swing.JMenuItem();
        jMenuItem70 = new javax.swing.JMenuItem();
        jMenuItem71 = new javax.swing.JMenuItem();
        jMenuItem72 = new javax.swing.JMenuItem();
        jMenu31 = new javax.swing.JMenu();
        jMenuItem73 = new javax.swing.JMenuItem();
        jMenuItem74 = new javax.swing.JMenuItem();
        jMenuItem75 = new javax.swing.JMenuItem();
        jMenuItem76 = new javax.swing.JMenuItem();
        jMenuItem77 = new javax.swing.JMenuItem();
        jMenuItem78 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenuItem79 = new javax.swing.JMenuItem();
        jMenuItem80 = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        jMenuItem81 = new javax.swing.JMenuItem();
        jMenuItem82 = new javax.swing.JMenuItem();
        jMenuItem101 = new javax.swing.JMenuItem();
        jMenuItem102 = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        jMenuItem88 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Main Menu");

        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel75.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Main/mainmenu.png"))); // NOI18N
        jLabel75.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel75KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel75, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 917, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel75, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 461, Short.MAX_VALUE)
        );

        jMenu1.setText("Master");

        jMenu10.setText("Purchase Party");

        jMenuItem1.setText("Add New ");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu10.add(jMenuItem1);

        jMenuItem2.setText("View / Update");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu10.add(jMenuItem2);

        jMenu1.add(jMenu10);

        jMenu11.setText("Sales Party");

        jMenuItem3.setText("Add New");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem3);

        jMenuItem4.setText("View / Update");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem4);

        jMenu1.add(jMenu11);

        jMenu12.setText("Ledger");

        jMenuItem5.setText("Create New");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem5);

        jMenuItem7.setText("Update Ledger");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem7);

        jMenuItem6.setText("View Ledger");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem6);

        jMenu1.add(jMenu12);

        jMenu13.setText("Staff");

        jMenuItem8.setText("Add New");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem8);

        jMenuItem9.setText("Update Staff");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem9);

        jMenu1.add(jMenu13);

        jMenu14.setText("Group");

        jMenuItem10.setText("Create New");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem10);

        jMenuItem11.setText("View / Update");
        jMenu14.add(jMenuItem11);

        jMenu1.add(jMenu14);

        jMenu15.setText("Product");

        jMenuItem12.setText("Add New");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu15.add(jMenuItem12);

        jMenuItem13.setText("View / Update");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu15.add(jMenuItem13);

        jMenu1.add(jMenu15);

        jMenu16.setText("By Forgation");

        jMenuItem14.setText("Add New");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu16.add(jMenuItem14);

        jMenuItem15.setText("View / Update");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu16.add(jMenuItem15);

        jMenu1.add(jMenu16);

        jMenu17.setText("Artical Number");

        jMenuItem16.setText("Add New");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu17.add(jMenuItem16);

        jMenu1.add(jMenu17);

        jMenu18.setText("City");

        jMenuItem17.setText("Add New");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenu18.add(jMenuItem17);

        jMenuItem18.setText("View");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        jMenu18.add(jMenuItem18);

        jMenu1.add(jMenu18);

        jMenuItem19.setText("Opening Stock");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem19);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Transaction");

        jMenu19.setText("Sales Billing");

        jMenuItem20.setText("New Billing");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        jMenu19.add(jMenuItem20);

        jMenuItem21.setText("Update Billing");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        jMenu19.add(jMenuItem21);

        jMenu2.add(jMenu19);

        jMenu20.setText("Sales Return");

        jMenuItem22.setText("New Sales Return");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        jMenu20.add(jMenuItem22);

        jMenuItem23.setText("Update Sales Return");
        jMenuItem23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem23ActionPerformed(evt);
            }
        });
        jMenu20.add(jMenuItem23);

        jMenu2.add(jMenu20);

        jMenu21.setText("Purchase ");

        jMenuItem24.setText("New Purchase");
        jMenuItem24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem24ActionPerformed(evt);
            }
        });
        jMenu21.add(jMenuItem24);

        jMenuItem25.setText("Update Purchase");
        jMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem25ActionPerformed(evt);
            }
        });
        jMenu21.add(jMenuItem25);

        jMenu2.add(jMenu21);

        jMenu22.setText("Purchase Return");

        jMenuItem26.setText("New Purchase Return");
        jMenuItem26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem26ActionPerformed(evt);
            }
        });
        jMenu22.add(jMenuItem26);

        jMenuItem27.setText("Update Purchase Return");
        jMenuItem27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem27ActionPerformed(evt);
            }
        });
        jMenu22.add(jMenuItem27);

        jMenu2.add(jMenu22);

        jMenu32.setText("Bsnl Billing");

        jMenuItem83.setText("New Billing");
        jMenuItem83.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem83ActionPerformed(evt);
            }
        });
        jMenu32.add(jMenuItem83);

        jMenuItem84.setText("Update Billing");
        jMenuItem84.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem84ActionPerformed(evt);
            }
        });
        jMenu32.add(jMenuItem84);

        jMenu2.add(jMenu32);

        jMenu33.setText("Bsnl Purchase");

        jMenuItem85.setText("New Purchase");
        jMenuItem85.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem85ActionPerformed(evt);
            }
        });
        jMenu33.add(jMenuItem85);

        jMenuItem86.setText("Update Purchase");
        jMenuItem86.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem86ActionPerformed(evt);
            }
        });
        jMenu33.add(jMenuItem86);

        jMenu2.add(jMenu33);

        jMenu23.setText("Stock Transfer");

        jMenuItem28.setText("New Stock Transfer");
        jMenuItem28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem28ActionPerformed(evt);
            }
        });
        jMenu23.add(jMenuItem28);

        jMenuItem29.setText("Update Stock Transfer");
        jMenuItem29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem29ActionPerformed(evt);
            }
        });
        jMenu23.add(jMenuItem29);

        jMenu2.add(jMenu23);

        jMenu24.setText("Defected Item");

        jMenuItem30.setText("Add New Item");
        jMenuItem30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem30ActionPerformed(evt);
            }
        });
        jMenu24.add(jMenuItem30);

        jMenuItem31.setText("View Item");
        jMenuItem31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem31ActionPerformed(evt);
            }
        });
        jMenu24.add(jMenuItem31);

        jMenu2.add(jMenu24);

        jMenuItem32.setText("Rate Change");
        jMenuItem32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem32ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem32);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Reporting");

        jMenu25.setText("Sales Report");

        jMenuItem33.setText("Sales Report With Discount");
        jMenuItem33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem33ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem33);

        jMenuItem34.setText("Bill Wise Sales Report");
        jMenuItem34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem34ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem34);

        jMenuItem35.setText("Tag Wise Sales Report");
        jMenuItem35.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem35ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem35);

        jMenuItem36.setText("Party Wise Sales Report");
        jMenuItem36.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem36ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem36);

        jMenuItem37.setText("City Wise Sales Report");
        jMenuItem37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem37ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem37);

        jMenuItem38.setText("Product Wise Sales Report");
        jMenuItem38.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem38ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem38);

        jMenuItem39.setText("Cash Sales Report");
        jMenuItem39.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem39ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem39);

        jMenuItem40.setText("Debit Sales Report");
        jMenuItem40.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem40ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem40);

        jMenuItem41.setText("Staff Wise Sales Report");
        jMenuItem41.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem41ActionPerformed(evt);
            }
        });
        jMenu25.add(jMenuItem41);

        jMenu3.add(jMenu25);

        jMenu26.setText("Purchase Report");

        jMenuItem42.setText("Bill Wise Purchase Report");
        jMenuItem42.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem42ActionPerformed(evt);
            }
        });
        jMenu26.add(jMenuItem42);

        jMenuItem43.setText("Party Wise Purchase Report");
        jMenuItem43.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem43ActionPerformed(evt);
            }
        });
        jMenu26.add(jMenuItem43);

        jMenuItem44.setText("Product Wise Purchase Report");
        jMenuItem44.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem44ActionPerformed(evt);
            }
        });
        jMenu26.add(jMenuItem44);

        jMenuItem45.setText("City Wise Purchase Report");
        jMenuItem45.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem45ActionPerformed(evt);
            }
        });
        jMenu26.add(jMenuItem45);

        jMenu3.add(jMenu26);

        jMenu27.setText("Stcok Report");

        jMenuItem46.setText("Party Wise Stock Report");
        jMenuItem46.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem46ActionPerformed(evt);
            }
        });
        jMenu27.add(jMenuItem46);

        jMenuItem47.setText("Product Wise Stock Report");
        jMenuItem47.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem47ActionPerformed(evt);
            }
        });
        jMenu27.add(jMenuItem47);

        jMenuItem48.setText("City Wise Stock Report");
        jMenuItem48.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem48ActionPerformed(evt);
            }
        });
        jMenu27.add(jMenuItem48);

        jMenu28.setText("Rate Wise Stock Report");

        jMenuItem49.setText("Retail Rate");
        jMenuItem49.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem49ActionPerformed(evt);
            }
        });
        jMenu28.add(jMenuItem49);

        jMenuItem50.setText("Purchase Rate");
        jMenuItem50.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem50ActionPerformed(evt);
            }
        });
        jMenu28.add(jMenuItem50);

        jMenu27.add(jMenu28);

        jMenu3.add(jMenu27);

        jMenu29.setText("Stock Analysis Report");

        jMenuItem51.setText("Party Wise Analysis");
        jMenuItem51.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem51ActionPerformed(evt);
            }
        });
        jMenu29.add(jMenuItem51);

        jMenuItem52.setText("Product Wise Analysis");
        jMenuItem52.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem52ActionPerformed(evt);
            }
        });
        jMenu29.add(jMenuItem52);

        jMenuItem53.setText("City Wise Analysis");
        jMenuItem53.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem53ActionPerformed(evt);
            }
        });
        jMenu29.add(jMenuItem53);

        jMenu30.setText("Rate Wise Analysis Report");

        jMenuItem54.setText("Retail Rate ");
        jMenuItem54.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem54ActionPerformed(evt);
            }
        });
        jMenu30.add(jMenuItem54);

        jMenuItem55.setText("Purchase Rate");
        jMenuItem55.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem55ActionPerformed(evt);
            }
        });
        jMenu30.add(jMenuItem55);

        jMenu29.add(jMenu30);

        jMenu3.add(jMenu29);

        jMenuItem56.setText("Tag Detail");
        jMenuItem56.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem56ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem56);

        jMenuItem57.setText("Rate Filter");
        jMenuItem57.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem57ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem57);

        jMenuItem58.setText("Stock Checking");
        jMenuItem58.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem58ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem58);

        jMenu34.setText("Stock Transfer Report");

        jMenu35.setText("Stock Recieved Report");

        jMenuItem89.setText("PURCHASE");
        jMenu35.add(jMenuItem89);

        jMenuItem90.setText("RECIEVED BY BRANCH ON HO");
        jMenu35.add(jMenuItem90);

        jMenuItem91.setText("ALL RECIEVED BY BRANCH");
        jMenu35.add(jMenuItem91);

        jMenuItem92.setText("PURCHASE NEW");
        jMenu35.add(jMenuItem92);

        jMenu34.add(jMenu35);

        jMenu36.setText("Stock Issue Report");

        jMenu37.setText("Direct Issue");

        jMenuItem93.setText("SALES PRICE");
        jMenu37.add(jMenuItem93);

        jMenuItem94.setText("PURCHASE PRICE");
        jMenu37.add(jMenuItem94);

        jMenu36.add(jMenu37);

        jMenu38.setText("Issue By Branch");

        jMenuItem95.setText("Sales Price");
        jMenu38.add(jMenuItem95);

        jMenuItem96.setText("Purchase Price");
        jMenu38.add(jMenuItem96);

        jMenu36.add(jMenu38);

        jMenuItem97.setText("All Issue");
        jMenu36.add(jMenuItem97);

        jMenu34.add(jMenu36);

        jMenu39.setText("Issue Panding Report");

        jMenuItem98.setText("Through HO");
        jMenu39.add(jMenuItem98);

        jMenuItem99.setText("Through Branch");
        jMenu39.add(jMenuItem99);

        jMenu34.add(jMenu39);

        jMenu3.add(jMenu34);

        jMenu40.setText("Daily Report");

        jMenuItem100.setText("Tag With Bill No");
        jMenu40.add(jMenuItem100);

        jMenu3.add(jMenu40);

        jMenuItem103.setText("Customer Report");
        jMenuItem103.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem103ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem103);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Printing");

        jMenuItem59.setText("Tag Printing");
        jMenuItem59.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem59ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem59);

        jMenuBar1.add(jMenu4);

        jMenu5.setText("Accounting");

        jMenuItem60.setText("Contra");
        jMenuItem60.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem60ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem60);

        jMenuItem61.setText("Journal");
        jMenuItem61.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem61ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem61);

        jMenuItem62.setText("Cash Payment");
        jMenuItem62.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem62ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem62);

        jMenuItem63.setText("Cash Reciept");
        jMenuItem63.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem63ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem63);

        jMenuItem64.setText("Bank Payment");
        jMenuItem64.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem64ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem64);

        jMenuItem65.setText("Bank Reciept");
        jMenuItem65.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem65ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem65);

        jMenuItem87.setText("Interest Entry");
        jMenuItem87.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem87ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem87);

        jMenuBar1.add(jMenu5);

        jMenu6.setText("Financial Reports");

        jMenuItem66.setText("Cash And Sales Book");
        jMenuItem66.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem66ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem66);

        jMenuItem67.setText("Trial Balance ");
        jMenuItem67.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem67ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem67);

        jMenuItem68.setText("Trading Account");
        jMenuItem68.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem68ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem68);

        jMenuItem69.setText("Balance Sheet");
        jMenuItem69.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem69ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem69);

        jMenuItem70.setText("Profit And Loss");
        jMenuItem70.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem70ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem70);

        jMenuItem71.setText("Party Outstanding Report");
        jMenuItem71.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem71ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem71);

        jMenuItem72.setText("Party Against Payment Report");
        jMenuItem72.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem72ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem72);

        jMenu31.setText("Taxation Report");

        jMenuItem73.setText("TDS Payable Report");
        jMenuItem73.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem73ActionPerformed(evt);
            }
        });
        jMenu31.add(jMenuItem73);

        jMenuItem74.setText("TDS Receivable Report");
        jMenuItem74.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem74ActionPerformed(evt);
            }
        });
        jMenu31.add(jMenuItem74);

        jMenuItem75.setText("GST Sales Report");
        jMenuItem75.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem75ActionPerformed(evt);
            }
        });
        jMenu31.add(jMenuItem75);

        jMenuItem76.setText("GST Sales Return Report");
        jMenuItem76.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem76ActionPerformed(evt);
            }
        });
        jMenu31.add(jMenuItem76);

        jMenuItem77.setText("GST Purchase Report");
        jMenuItem77.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem77ActionPerformed(evt);
            }
        });
        jMenu31.add(jMenuItem77);

        jMenuItem78.setText("GST Purchase Return Report");
        jMenuItem78.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem78ActionPerformed(evt);
            }
        });
        jMenu31.add(jMenuItem78);

        jMenu6.add(jMenu31);

        jMenuBar1.add(jMenu6);

        jMenu7.setText("Branch");

        jMenuItem79.setText("Export To Branch");
        jMenuItem79.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem79ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem79);

        jMenuItem80.setText("Import From Branch");
        jMenuItem80.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem80ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem80);

        jMenuBar1.add(jMenu7);

        jMenu8.setText("Data");

        jMenuItem81.setText("Backup");
        jMenuItem81.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem81ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem81);

        jMenuItem82.setText("Restore");
        jMenuItem82.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem82ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem82);

        jMenuItem101.setText("Update");
        jMenuItem101.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem101ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem101);

        jMenuItem102.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem102.setText("Update Ledger");
        jMenuItem102.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem102ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem102);

        jMenuBar1.add(jMenu8);

        jMenu9.setText("Settings");

        jMenuItem88.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem88.setText("App Setting");
        jMenuItem88.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem88ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem88);

        jMenuBar1.add(jMenu9);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel75KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel75KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel75KeyPressed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Add_Purchase_Party.getObj().setVisible(true);
        if (Add_Purchase_Party.getObj().isShowing()) {
            Add_Purchase_Party.getObj().setState(NORMAL);
            Add_Purchase_Party.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        Update_Purchase_Party.getObj().setVisible(true);
        if (Update_Purchase_Party.getObj().isShowing()) {
            Update_Purchase_Party.getObj().setState(NORMAL);
            Update_Purchase_Party.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        Add_Sales_Party.getObj().setVisible(true);
        if (Add_Sales_Party.getObj().isShowing()) {
            Add_Sales_Party.getObj().setState(NORMAL);
            Add_Sales_Party.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        Update_Sales_Party.getObj().setVisible(true);
        if (Update_Sales_Party.getObj().isShowing()) {
            Update_Sales_Party.getObj().setState(NORMAL);
            Update_Sales_Party.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        createLedger.getObj().setVisible(true);
        if (createLedger.getObj().isShowing()) {
            createLedger.getObj().setState(NORMAL);
            createLedger.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        Alterledger.getObj().setVisible(true);
        if (Alterledger.getObj().isShowing()) {
            Alterledger.getObj().setState(NORMAL);
            Alterledger.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        viewlledger.getObj().setVisible(true);
        if (viewlledger.getObj().isShowing()) {
            viewlledger.getObj().setState(NORMAL);
            viewlledger.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        addMember.getObj().setVisible(true);
        if (addMember.getObj().isShowing()) {
            addMember.getObj().setState(NORMAL);
            addMember.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        ViewStaff.getObj().setVisible(true);
        if (ViewStaff.getObj().isShowing()) {
            ViewStaff.getObj().setState(NORMAL);
            ViewStaff.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        Creategroup.getObj().setVisible(true);
        if (Creategroup.getObj().isShowing()) {
            Creategroup.getObj().setState(NORMAL);
            Creategroup.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        addProduct.getObj().setVisible(true);
        if (addProduct.getObj().isShowing()) {
            addProduct.getObj().setState(NORMAL);
            addProduct.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        UpdateProduct.getObj().setVisible(true);
        if (UpdateProduct.getObj().isShowing()) {
            UpdateProduct.getObj().setState(NORMAL);
            UpdateProduct.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        byforgation.getObj().setVisible(true);
        if (byforgation.getObj().isShowing()) {
            byforgation.getObj().setState(NORMAL);
            byforgation.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        delbyforgation.getObj().setVisible(true);
        if (delbyforgation.getObj().isShowing()) {
            delbyforgation.getObj().setState(NORMAL);
            delbyforgation.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        addArticle.getObj().setVisible(true);
        if (addArticle.getObj().isShowing()) {
            addArticle.getObj().setState(NORMAL);
            addArticle.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        addcity.getObj().setVisible(true);
        if (addcity.getObj().isShowing()) {
            addcity.getObj().setState(NORMAL);
            addcity.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        Updatecity1.getObj().setVisible(true);
        if (Updatecity1.getObj().isShowing()) {
            Updatecity1.getObj().setState(NORMAL);
            Updatecity1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        try {
            connection c1 = new connection();
            Connection connect = c1.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from stock limit 1");
            if (rs.next()) {
                JOptionPane.showMessageDialog(rootPane, "you already started purchasing now you are not able to add opening stock ");
            } else {

                Create_barcode.getObj().setVisible(true);
                if (Create_barcode.getObj().isShowing()) {
                    Create_barcode.getObj().setState(NORMAL);
                    Create_barcode.getObj().requestFocus();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
        New_Dual_Billing.getObj().setVisible(true);
        if (New_Dual_Billing.getObj().isShowing()) {
            New_Dual_Billing.getObj().setState(NORMAL);
            New_Dual_Billing.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed
        Bill_Update.getObj().setVisible(true);
        if (Bill_Update.getObj().isShowing()) {
            Bill_Update.getObj().setState(NORMAL);
            Bill_Update.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem21ActionPerformed

    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
        New_Dual_SalesReturn.getObj().setVisible(true);
        if (New_Dual_SalesReturn.getObj().isShowing()) {
            New_Dual_SalesReturn.getObj().setState(NORMAL);
            New_Dual_SalesReturn.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem22ActionPerformed

    private void jMenuItem23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem23ActionPerformed
        Sales_Return_Update.getObj().setVisible(true);
        if (Sales_Return_Update.getObj().isShowing()) {
            Sales_Return_Update.getObj().setState(NORMAL);
            Sales_Return_Update.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem23ActionPerformed

    private void jMenuItem24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem24ActionPerformed
        Pur_trans_dualGst.getObj().setVisible(true);
        if (Pur_trans_dualGst.getObj().isShowing()) {
            Pur_trans_dualGst.getObj().setState(NORMAL);
            Pur_trans_dualGst.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem24ActionPerformed

    private void jMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem25ActionPerformed
        Purchase_Update.getObj().setVisible(true);
        if (Purchase_Update.getObj().isShowing()) {
            Purchase_Update.getObj().setState(NORMAL);
            Purchase_Update.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem25ActionPerformed

    private void jMenuItem26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem26ActionPerformed
        New_Dual_PurchaseReturn.getObj().setVisible(true);
        if (New_Dual_PurchaseReturn.getObj().isShowing()) {
            New_Dual_PurchaseReturn.getObj().setState(NORMAL);
            New_Dual_PurchaseReturn.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem26ActionPerformed

    private void jMenuItem27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem27ActionPerformed
        purchasereturngstupdate.getObj().setVisible(true);
        if (purchasereturngstupdate.getObj().isShowing()) {
            purchasereturngstupdate.getObj().setState(NORMAL);
            purchasereturngstupdate.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem27ActionPerformed

    private void jMenuItem28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem28ActionPerformed
        StockTransfer.getObj().setVisible(true);
        if (StockTransfer.getObj().isShowing()) {
            StockTransfer.getObj().setState(NORMAL);
            StockTransfer.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem28ActionPerformed

    private void jMenuItem29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem29ActionPerformed
        // TODO add your handling code here:
        // Stock Transfer update code goes here
    }//GEN-LAST:event_jMenuItem29ActionPerformed

    private void jMenuItem30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem30ActionPerformed
        adddefectedpiece.getObj().setVisible(true);
        if (adddefectedpiece.getObj().isShowing()) {
            adddefectedpiece.getObj().setState(NORMAL);
            adddefectedpiece.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem30ActionPerformed

    private void jMenuItem31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem31ActionPerformed
        defecteditemlist.getObj().setVisible(true);
        if (defecteditemlist.getObj().isShowing()) {
            defecteditemlist.getObj().setState(NORMAL);
            defecteditemlist.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem31ActionPerformed

    private void jMenuItem32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem32ActionPerformed
        Rate_authentication.getObj().setVisible(true);
        if (Rate_authentication.getObj().isShowing()) {
            Rate_authentication.getObj().setState(NORMAL);
            Rate_authentication.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem32ActionPerformed

    private void jMenuItem33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem33ActionPerformed
        dailyreportwithdiscount.getObj().setVisible(true);
        if (dailyreportwithdiscount.getObj().isShowing()) {
            dailyreportwithdiscount.getObj().setState(NORMAL);
            dailyreportwithdiscount.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem33ActionPerformed

    private void jMenuItem34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem34ActionPerformed
        Dailyreport_.getObj().setVisible(true);
        if (Dailyreport_.getObj().isShowing()) {
            Dailyreport_.getObj().setState(NORMAL);
            Dailyreport_.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem34ActionPerformed

    private void jMenuItem35ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem35ActionPerformed
        dailyreport1.getObj().setVisible(true);
        if (dailyreport1.getObj().isShowing()) {
            dailyreport1.getObj().setState(NORMAL);
            dailyreport1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem35ActionPerformed

    private void jMenuItem36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem36ActionPerformed
        PartyWiseDailySalesReport.getObj().setVisible(true);
        if (PartyWiseDailySalesReport.getObj().isShowing()) {
            PartyWiseDailySalesReport.getObj().setState(NORMAL);
            PartyWiseDailySalesReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem36ActionPerformed

    private void jMenuItem37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem37ActionPerformed
        CityWiseDailySalesReport.getObj().setVisible(true);
        if (CityWiseDailySalesReport.getObj().isShowing()) {
            CityWiseDailySalesReport.getObj().setState(NORMAL);
            CityWiseDailySalesReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem37ActionPerformed

    private void jMenuItem38ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem38ActionPerformed
        ProductWiseDailySalesReport.getObj().setVisible(true);
        if (ProductWiseDailySalesReport.getObj().isShowing()) {
            ProductWiseDailySalesReport.getObj().setState(NORMAL);
            ProductWiseDailySalesReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem38ActionPerformed

    private void jMenuItem39ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem39ActionPerformed
        dailyreportcashsales.getObj().setVisible(true);
        if (dailyreportcashsales.getObj().isShowing()) {
            dailyreportcashsales.getObj().setState(NORMAL);
            dailyreportcashsales.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem39ActionPerformed

    private void jMenuItem40ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem40ActionPerformed
        dailyreportdebitsales.getObj().setVisible(true);
        if (dailyreportdebitsales.getObj().isShowing()) {
            dailyreportdebitsales.getObj().setState(NORMAL);
            dailyreportdebitsales.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem40ActionPerformed

    private void jMenuItem41ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem41ActionPerformed
        Staffwise_Sales_Report.getObj().setVisible(true);
        if (Staffwise_Sales_Report.getObj().isShowing()) {
            Staffwise_Sales_Report.getObj().setState(NORMAL);
            Staffwise_Sales_Report.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem41ActionPerformed

    private void jMenuItem42ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem42ActionPerformed
        purchasestockreport.getObj().setVisible(true);
        if (purchasestockreport.getObj().isShowing()) {
            purchasestockreport.getObj().setState(NORMAL);
            purchasestockreport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem42ActionPerformed

    private void jMenuItem43ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem43ActionPerformed
        PartyWiseDailyPurchaseReport.getObj().setVisible(true);
        if (PartyWiseDailyPurchaseReport.getObj().isShowing()) {
            PartyWiseDailyPurchaseReport.getObj().setState(NORMAL);
            PartyWiseDailyPurchaseReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem43ActionPerformed

    private void jMenuItem44ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem44ActionPerformed
        ProductWiseDailyPurchaseReport.getObj().setVisible(true);
        if (ProductWiseDailyPurchaseReport.getObj().isShowing()) {
            ProductWiseDailyPurchaseReport.getObj().setState(NORMAL);
            ProductWiseDailyPurchaseReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem44ActionPerformed

    private void jMenuItem45ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem45ActionPerformed
        CityWiseDailyPurchaseReport.getObj().setVisible(true);
        if (CityWiseDailyPurchaseReport.getObj().isShowing()) {
            CityWiseDailyPurchaseReport.getObj().setState(NORMAL);
            CityWiseDailyPurchaseReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem45ActionPerformed

    private void jMenuItem46ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem46ActionPerformed
        PartyWiseclosingstock.getObj().setVisible(true);
        if (PartyWiseclosingstock.getObj().isShowing()) {
            PartyWiseclosingstock.getObj().setState(NORMAL);
            PartyWiseclosingstock.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem46ActionPerformed

    private void jMenuItem47ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem47ActionPerformed
        ProductWiseClosingStock.getObj().setVisible(true);
        if (ProductWiseClosingStock.getObj().isShowing()) {
            ProductWiseClosingStock.getObj().setState(NORMAL);
            ProductWiseClosingStock.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem47ActionPerformed

    private void jMenuItem48ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem48ActionPerformed
        CityWiseClosingStock.getObj().setVisible(true);
        if (CityWiseClosingStock.getObj().isShowing()) {
            CityWiseClosingStock.getObj().setState(NORMAL);
            CityWiseClosingStock.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem48ActionPerformed

    private void jMenuItem49ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem49ActionPerformed
        RetailRateWiseClosingStock.getObj().setVisible(true);
        if (RetailRateWiseClosingStock.getObj().isShowing()) {
            RetailRateWiseClosingStock.getObj().setState(NORMAL);
            RetailRateWiseClosingStock.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem49ActionPerformed

    private void jMenuItem50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem50ActionPerformed
        PurchaseRateWiseClosingStock.getObj().setVisible(true);
        if (PurchaseRateWiseClosingStock.getObj().isShowing()) {
            PurchaseRateWiseClosingStock.getObj().setState(NORMAL);
            PurchaseRateWiseClosingStock.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem50ActionPerformed

    private void jMenuItem51ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem51ActionPerformed
        PartyWiseStock_AnalysisReport.getObj().setVisible(true);
        if (PartyWiseStock_AnalysisReport.getObj().isShowing()) {
            PartyWiseStock_AnalysisReport.getObj().setState(NORMAL);
            PartyWiseStock_AnalysisReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem51ActionPerformed

    private void jMenuItem52ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem52ActionPerformed
        ProductWiseStockReport.getObj().setVisible(true);
        if (ProductWiseStockReport.getObj().isShowing()) {
            ProductWiseStockReport.getObj().setState(NORMAL);
            ProductWiseStockReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem52ActionPerformed

    private void jMenuItem53ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem53ActionPerformed
        CityWiseStockReport.getObj().setVisible(true);
        if (CityWiseStockReport.getObj().isShowing()) {
            CityWiseStockReport.getObj().setState(NORMAL);
            CityWiseStockReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem53ActionPerformed

    private void jMenuItem54ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem54ActionPerformed
        RetailRateWiseStockReport.getObj().setVisible(true);
        if (RetailRateWiseStockReport.getObj().isShowing()) {
            RetailRateWiseStockReport.getObj().setState(NORMAL);
            RetailRateWiseStockReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem54ActionPerformed

    private void jMenuItem55ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem55ActionPerformed
        PurchaseRateWiseStockReport.getObj().setVisible(true);
        if (PurchaseRateWiseStockReport.getObj().isShowing()) {
            PurchaseRateWiseStockReport.getObj().setState(NORMAL);
            PurchaseRateWiseStockReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem55ActionPerformed

    private void jMenuItem56ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem56ActionPerformed
        tagdetail.getObj().setVisible(true);
        if (tagdetail.getObj().isShowing()) {
            tagdetail.getObj().setState(NORMAL);
            tagdetail.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem56ActionPerformed

    private void jMenuItem57ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem57ActionPerformed
        Filter_Stcok_Rate.getObj().setVisible(true);
        if (Filter_Stcok_Rate.getObj().isShowing()) {
            Filter_Stcok_Rate.getObj().setState(NORMAL);
            Filter_Stcok_Rate.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem57ActionPerformed

    private void jMenuItem58ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem58ActionPerformed
        stockchecking.getObj().setVisible(true);
        if (stockchecking.getObj().isShowing()) {
            stockchecking.getObj().setState(NORMAL);
            stockchecking.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem58ActionPerformed

    private void jMenuItem59ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem59ActionPerformed
        printbarcode.getObj().setVisible(true);
        if (printbarcode.getObj().isShowing()) {
            printbarcode.getObj().setState(NORMAL);
            printbarcode.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem59ActionPerformed

    private void jMenuItem60ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem60ActionPerformed
        Contra.getObj().setVisible(true);
        if (Contra.getObj().isShowing()) {
            Contra.getObj().setState(NORMAL);
            Contra.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem60ActionPerformed

    private void jMenuItem61ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem61ActionPerformed
        journ.getObj().setVisible(true);
        if (journ.getObj().isShowing()) {
            journ.getObj().setState(NORMAL);
            journ.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem61ActionPerformed

    private void jMenuItem62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem62ActionPerformed
        cashpayment.getObj().setVisible(true);
        if (cashpayment.getObj().isShowing()) {
            cashpayment.getObj().setState(NORMAL);
            cashpayment.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem62ActionPerformed

    private void jMenuItem63ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem63ActionPerformed
        cashreciept.getObj().setVisible(true);
        if (cashreciept.getObj().isShowing()) {
            cashreciept.getObj().setState(NORMAL);
            cashreciept.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem63ActionPerformed

    private void jMenuItem64ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem64ActionPerformed
        bankpayment1.getObj().setVisible(true);
        if (bankpayment1.getObj().isShowing()) {
            bankpayment1.getObj().setState(NORMAL);
            bankpayment1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem64ActionPerformed

    private void jMenuItem65ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem65ActionPerformed
        bankreciept.getObj().setVisible(true);
        if (bankreciept.getObj().isShowing()) {
            bankreciept.getObj().setState(NORMAL);
            bankreciept.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem65ActionPerformed

    private void jMenuItem66ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem66ActionPerformed
        cashbook.getObj().setVisible(true);
        if (cashbook.getObj().isShowing()) {
            cashbook.getObj().setState(NORMAL);
            cashbook.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem66ActionPerformed

    private void jMenuItem67ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem67ActionPerformed
        trailbalwithopen.getObj().setVisible(true);
        if (trailbalwithopen.getObj().isShowing()) {
            trailbalwithopen.getObj().setState(NORMAL);
            trailbalwithopen.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem67ActionPerformed

    private void jMenuItem68ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem68ActionPerformed
        Tradingaccount1.getObj().setVisible(true);
        if (Tradingaccount1.getObj().isShowing()) {
            Tradingaccount1.getObj().setState(NORMAL);
            Tradingaccount1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem68ActionPerformed

    private void jMenuItem69ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem69ActionPerformed
        BalanceSheet1.getObj().setVisible(true);
        if (BalanceSheet1.getObj().isShowing()) {
            BalanceSheet1.getObj().setState(NORMAL);
            BalanceSheet1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem69ActionPerformed

    private void jMenuItem70ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem70ActionPerformed
        pals1.getObj().setVisible(true);
        if (pals1.getObj().isShowing()) {
            pals1.getObj().setState(NORMAL);
            pals1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem70ActionPerformed

    private void jMenuItem71ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem71ActionPerformed
        PartyOutstandingReports.getObj().setVisible(true);
        if (PartyOutstandingReports.getObj().isShowing()) {
            PartyOutstandingReports.getObj().setState(NORMAL);
            PartyOutstandingReports.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem71ActionPerformed

    private void jMenuItem72ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem72ActionPerformed
        PartyWisePaymentReport.getObj().setVisible(true);
        if (PartyWisePaymentReport.getObj().isShowing()) {
            PartyWisePaymentReport.getObj().setState(NORMAL);
            PartyWisePaymentReport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem72ActionPerformed

    private void jMenuItem73ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem73ActionPerformed
        tdspayablereport.getObj().setVisible(true);
        if (tdspayablereport.getObj().isShowing()) {
            tdspayablereport.getObj().setState(NORMAL);
            tdspayablereport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem73ActionPerformed

    private void jMenuItem74ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem74ActionPerformed
        tdsrecievablereport.getObj().setVisible(true);
        if (tdsrecievablereport.getObj().isShowing()) {
            tdsrecievablereport.getObj().setState(NORMAL);
            tdsrecievablereport.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem74ActionPerformed

    private void jMenuItem75ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem75ActionPerformed
        Gst_BillWise_Sales_Report.getObj().setVisible(true);
        if (Gst_BillWise_Sales_Report.getObj().isShowing()) {
            Gst_BillWise_Sales_Report.getObj().setState(NORMAL);
            Gst_BillWise_Sales_Report.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem75ActionPerformed

    private void jMenuItem76ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem76ActionPerformed
        Gst_BillWise_SalesReturn_Report.getObj().setVisible(true);
        if (Gst_BillWise_SalesReturn_Report.getObj().isShowing()) {
            Gst_BillWise_SalesReturn_Report.getObj().setState(NORMAL);
            Gst_BillWise_SalesReturn_Report.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem76ActionPerformed

    private void jMenuItem77ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem77ActionPerformed
        purchasestockreport_dule1.getObj().setVisible(true);
        if (purchasestockreport_dule1.getObj().isShowing()) {
            purchasestockreport_dule1.getObj().setState(NORMAL);
            purchasestockreport_dule1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem77ActionPerformed

    private void jMenuItem78ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem78ActionPerformed
        // GST Purchase Return Report code goes here
    }//GEN-LAST:event_jMenuItem78ActionPerformed

    private void jMenuItem79ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem79ActionPerformed
        Exportdata1.getObj().setVisible(true);
        if (Exportdata1.getObj().isShowing()) {
            Exportdata1.getObj().setState(NORMAL);
            Exportdata1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem79ActionPerformed

    private void jMenuItem80ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem80ActionPerformed
        importdata1.createAndShowGUI();
    }//GEN-LAST:event_jMenuItem80ActionPerformed
    // Database Backup method

    public boolean backupDataWithDatabase(String dumpExePath, String host, String port, String user, String password, String database, String backupPath) {
        boolean status = false;
        try {
            Process p = null;

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String filepath = "backup(with_DB)-" + database + "-" + host + "-(" + dateFormat.format(date) + ").sql";

            String batchCommand = "";
            if (password != "") {
                //Backup with database
                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            } else {
                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            }

            Runtime runtime = Runtime.getRuntime();
            p = runtime.exec(batchCommand);
            int processComplete = p.waitFor();

            if (processComplete == 0) {
                status = true;
                JOptionPane.showMessageDialog(this, "Backup created successfully for with DB " + database + " in " + host + ":" + port);
            } else {
                status = false;
                JOptionPane.showMessageDialog(this, "Could not create the backup for with DB " + database + " in " + host + ":" + port);
            }

        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(this, ioe);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e);
        }
        return status;
    }
    private void jMenuItem81ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem81ActionPerformed
        String dumpExePath = "C:\\Program Files\\MySQL\\MySQL Server 5.1\\bin\\mysqldump";
        String host = "localhost";
        String port = "3307";
        String user = "root";
        String password = "root";
        String database = "Appson";
        String backupPath = "D:/Data//";
        backupDataWithDatabase(dumpExePath, host, port, user, password, database, backupPath);
    }//GEN-LAST:event_jMenuItem81ActionPerformed
// Databse Restore Method 
    JFileChooser fc = new JFileChooser();
    private boolean isConnected = false;

    public boolean restore(String filePath) {
        boolean status = false;
        String username = "root";
        String password = "root";
        String hostname = "localhost";
        String[] command = new String[]{"C:\\Program Files\\MySQL\\MySQL Server 5.1\\bin\\mysql", "appson", "-u" + username, "-h" + hostname, "-e", " source " + filePath};

        try {
            Process runtimeProcess = Runtime.getRuntime().exec(command);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                System.out.println("DatabaseManager.restore: Restore Successfull");
                JOptionPane.showMessageDialog(this, "DatabaseManager.restore: Restore Successfull");
                status = true;
            } else {
                JOptionPane.showMessageDialog(this, "DatabaseManager.restore: Restore Failure!");
            }
        } catch (IOException ioe) {
            System.out.println("Exception IO");
            JOptionPane.showMessageDialog(this, ioe);
            ioe.printStackTrace();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e);
            e.printStackTrace();
        }
        return status;
    }
    private void jMenuItem82ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem82ActionPerformed
        if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            String dbUserName = "root";
            String dbPassword = "root";
            String source = fc.getSelectedFile().toString();
            // String source="C:\\Users\\pc\\Documents";
            restore(source);
            System.out.println("getCurrentDirectory(): " + fc.getCurrentDirectory());
            System.out.println("getSelectedFile() : " + fc.getSelectedFile());
        } else {
            JOptionPane.showMessageDialog(rootPane, "No Selection ");
            System.out.println("No Selection ");
        }
    }//GEN-LAST:event_jMenuItem82ActionPerformed

    private void jMenuItem84ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem84ActionPerformed
        BsnlaBilling_Update.getObj().setVisible(true);
        if (BsnlaBilling_Update.getObj().isShowing()) {
            BsnlaBilling_Update.getObj().setState(NORMAL);
            BsnlaBilling_Update.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem84ActionPerformed

    private void jMenuItem83ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem83ActionPerformed
        Bsnl_Billing_GST.getObj().setVisible(true);
        if (Bsnl_Billing_GST.getObj().isShowing()) {
            Bsnl_Billing_GST.getObj().setState(NORMAL);
            Bsnl_Billing_GST.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem83ActionPerformed

    private void jMenuItem85ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem85ActionPerformed
        Bsnl_Purchase.getObj().setVisible(true);
        if (Bsnl_Purchase.getObj().isShowing()) {
            Bsnl_Purchase.getObj().setState(NORMAL);
            Bsnl_Purchase.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem85ActionPerformed

    private void jMenuItem86ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem86ActionPerformed
        BsnlPurchase_Update.getObj().setVisible(true);
        if (BsnlPurchase_Update.getObj().isShowing()) {
            BsnlPurchase_Update.getObj().setState(NORMAL);
            BsnlPurchase_Update.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem86ActionPerformed

    private void jMenuItem87ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem87ActionPerformed
        interestentry1.getObj().setVisible(true);
        if (interestentry1.getObj().isShowing()) {
            interestentry1.getObj().setState(NORMAL);
            interestentry1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem87ActionPerformed

    private void jMenuItem88ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem88ActionPerformed
        Setting.getObj().setVisible(true);
        if (Setting.getObj().isShowing()) {
            Setting.getObj().setState(NORMAL);
            Setting.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem88ActionPerformed

    private void jMenuItem101ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem101ActionPerformed
        viewlledger1.getObj().setVisible(true);
        if (viewlledger1.getObj().isShowing()) {
            viewlledger1.getObj().setState(NORMAL);
            viewlledger1.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem101ActionPerformed

    private void jMenuItem102ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem102ActionPerformed
      Viewlledger_2.getObj().setVisible(true);
        if (Viewlledger_2.getObj().isShowing()) {
            Viewlledger_2.getObj().setState(NORMAL);
            Viewlledger_2.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem102ActionPerformed

    private void jMenuItem103ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem103ActionPerformed
        Customer_Report.getObj().setVisible(true);
        if (Customer_Report.getObj().isShowing()) {
            Customer_Report.getObj().setState(NORMAL);
            Customer_Report.getObj().requestFocus();
        }
    }//GEN-LAST:event_jMenuItem103ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainMenu_1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainMenu_1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainMenu_1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainMenu_1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainMenu_1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel75;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu10;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenu jMenu12;
    private javax.swing.JMenu jMenu13;
    private javax.swing.JMenu jMenu14;
    private javax.swing.JMenu jMenu15;
    private javax.swing.JMenu jMenu16;
    private javax.swing.JMenu jMenu17;
    private javax.swing.JMenu jMenu18;
    private javax.swing.JMenu jMenu19;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu20;
    private javax.swing.JMenu jMenu21;
    private javax.swing.JMenu jMenu22;
    private javax.swing.JMenu jMenu23;
    private javax.swing.JMenu jMenu24;
    private javax.swing.JMenu jMenu25;
    private javax.swing.JMenu jMenu26;
    private javax.swing.JMenu jMenu27;
    private javax.swing.JMenu jMenu28;
    private javax.swing.JMenu jMenu29;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu30;
    private javax.swing.JMenu jMenu31;
    private javax.swing.JMenu jMenu32;
    private javax.swing.JMenu jMenu33;
    private javax.swing.JMenu jMenu34;
    private javax.swing.JMenu jMenu35;
    private javax.swing.JMenu jMenu36;
    private javax.swing.JMenu jMenu37;
    private javax.swing.JMenu jMenu38;
    private javax.swing.JMenu jMenu39;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu40;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem100;
    private javax.swing.JMenuItem jMenuItem101;
    private javax.swing.JMenuItem jMenuItem102;
    private javax.swing.JMenuItem jMenuItem103;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem23;
    private javax.swing.JMenuItem jMenuItem24;
    private javax.swing.JMenuItem jMenuItem25;
    private javax.swing.JMenuItem jMenuItem26;
    private javax.swing.JMenuItem jMenuItem27;
    private javax.swing.JMenuItem jMenuItem28;
    private javax.swing.JMenuItem jMenuItem29;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem30;
    private javax.swing.JMenuItem jMenuItem31;
    private javax.swing.JMenuItem jMenuItem32;
    private javax.swing.JMenuItem jMenuItem33;
    private javax.swing.JMenuItem jMenuItem34;
    private javax.swing.JMenuItem jMenuItem35;
    private javax.swing.JMenuItem jMenuItem36;
    private javax.swing.JMenuItem jMenuItem37;
    private javax.swing.JMenuItem jMenuItem38;
    private javax.swing.JMenuItem jMenuItem39;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem40;
    private javax.swing.JMenuItem jMenuItem41;
    private javax.swing.JMenuItem jMenuItem42;
    private javax.swing.JMenuItem jMenuItem43;
    private javax.swing.JMenuItem jMenuItem44;
    private javax.swing.JMenuItem jMenuItem45;
    private javax.swing.JMenuItem jMenuItem46;
    private javax.swing.JMenuItem jMenuItem47;
    private javax.swing.JMenuItem jMenuItem48;
    private javax.swing.JMenuItem jMenuItem49;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem50;
    private javax.swing.JMenuItem jMenuItem51;
    private javax.swing.JMenuItem jMenuItem52;
    private javax.swing.JMenuItem jMenuItem53;
    private javax.swing.JMenuItem jMenuItem54;
    private javax.swing.JMenuItem jMenuItem55;
    private javax.swing.JMenuItem jMenuItem56;
    private javax.swing.JMenuItem jMenuItem57;
    private javax.swing.JMenuItem jMenuItem58;
    private javax.swing.JMenuItem jMenuItem59;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem60;
    private javax.swing.JMenuItem jMenuItem61;
    private javax.swing.JMenuItem jMenuItem62;
    private javax.swing.JMenuItem jMenuItem63;
    private javax.swing.JMenuItem jMenuItem64;
    private javax.swing.JMenuItem jMenuItem65;
    private javax.swing.JMenuItem jMenuItem66;
    private javax.swing.JMenuItem jMenuItem67;
    private javax.swing.JMenuItem jMenuItem68;
    private javax.swing.JMenuItem jMenuItem69;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem70;
    private javax.swing.JMenuItem jMenuItem71;
    private javax.swing.JMenuItem jMenuItem72;
    private javax.swing.JMenuItem jMenuItem73;
    private javax.swing.JMenuItem jMenuItem74;
    private javax.swing.JMenuItem jMenuItem75;
    private javax.swing.JMenuItem jMenuItem76;
    private javax.swing.JMenuItem jMenuItem77;
    private javax.swing.JMenuItem jMenuItem78;
    private javax.swing.JMenuItem jMenuItem79;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem80;
    private javax.swing.JMenuItem jMenuItem81;
    private javax.swing.JMenuItem jMenuItem82;
    private javax.swing.JMenuItem jMenuItem83;
    private javax.swing.JMenuItem jMenuItem84;
    private javax.swing.JMenuItem jMenuItem85;
    private javax.swing.JMenuItem jMenuItem86;
    private javax.swing.JMenuItem jMenuItem87;
    private javax.swing.JMenuItem jMenuItem88;
    private javax.swing.JMenuItem jMenuItem89;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JMenuItem jMenuItem90;
    private javax.swing.JMenuItem jMenuItem91;
    private javax.swing.JMenuItem jMenuItem92;
    private javax.swing.JMenuItem jMenuItem93;
    private javax.swing.JMenuItem jMenuItem94;
    private javax.swing.JMenuItem jMenuItem95;
    private javax.swing.JMenuItem jMenuItem96;
    private javax.swing.JMenuItem jMenuItem97;
    private javax.swing.JMenuItem jMenuItem98;
    private javax.swing.JMenuItem jMenuItem99;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
