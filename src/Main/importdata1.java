/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import connection.connection;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 *
 * @author app
 */
public class importdata1 extends javax.swing.JPanel implements ActionListener, 
                                        PropertyChangeListener
{
static JFrame frame; 
    Task task;
   
    JFileChooser input; 
    File file;
    String fileName;
    connection c;
    Connection con;
    int progress=0;

    @Override
    public void actionPerformed(ActionEvent e) {
       jButton1.setEnabled(false);
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        //Instances of javax.swing.SwingWorker are not reusuable, so
        //we create new instances as needed.
        task = new Task();
        task.addPropertyChangeListener(this);
        task.execute(); 
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress".equals(evt.getPropertyName())) {
             progress = (Integer) evt.getNewValue();
           
            jProgressBar1.setValue(progress);
             System.out.println("progress : "+progress);
            /*jTextArea1.append(String.format(
                    "Completed %d%% of task.\n", task.getProgress()));*/
        }    }

    /**
     * Creates new form importdata1
     */
    class Task extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        
        @Override
        public Void doInBackground() {
            
            input= new JFileChooser();
            int result = input.showSaveDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                try {
                     file = input.getSelectedFile();
                    fileName = file.toString();
                    
                    String fName=file.getName();
                    c = new connection();
                     con = c.cone();
                     Statement st1=con.createStatement();
                     ResultSet rs1=st1.executeQuery("select * from file_import where file_name='"+fName+"'");
                     if(rs1.next()){
                        JOptionPane.showMessageDialog(frame,"File is Already Imported...");
                     }else
                     {
                         
                     Statement st = con.createStatement();
                    
                    String sql = "";
                    try {
                        con.setAutoCommit(false);
                        String line = "";
//                    FileInputStream fin = new FileInputStream(fileName);
                        BufferedReader fin = new BufferedReader(new FileReader(fileName));
                           
                        int i = 0;
                        int readlength=0;
                     // int totalLength= line.length();
                       // double lengthPerPercent = 100.0 / totalLength;
                        while ((i = fin.read()) != -1) {
                            line += (char) i;
                            //  System.out.println("line" +line);
                        }
                        String[] temp;
                        temp = line.split("/t@#");
                        
                            //int totalLength=temp.length;
                                 Thread.sleep(100);
                                 float length=0;
                                 float totalLength=0;
                                     System.out.println("length of line : "+line.length());
                                   System.out.println("temp.length : "+temp.length);
                                   int j=0;
                                   long fileLength=file.length();
                                   totalLength=temp.length;
                                 float  percent=(totalLength*1)/100;
                                   System.out.println("before loop : "+percent);
                                   totalLength=temp.length;
                                   int local=0;
                                   local=(int) percent;
                                   float newPercent=percent;
                           for (i = 0; i <temp.length; i++) {
                                     Thread.sleep(10);
                                     System.out.println("total length : "+totalLength);
                                     local=(int) percent;
                                     System.out.println("local is :"+local);
                                     System.out.println("value of i :"+i);
                                      if(i==local)
                                      {
                                       System.out.println("print i : "+i+"  local : "+local);
                                       progress++;
                                       System.out.println("progress : "+progress);
                                       setProgress(Math.min(progress, 100));
                                       percent=newPercent+percent;
                                      }
                            
                                      if (temp[i].contains("Update") && temp[i].contains("ledger") && temp[i].contains("ledger_name")) {
                                String strArray[] = temp[i].split("'");
                                String amount = strArray[1];
                                String type = strArray[3];
                                String ledger = strArray[5];
                                String currbal_type = "";
                                double curr_bal = 0.0;
                                Statement stmt = con.createStatement();

                                String sql1 = "select curr_bal,currbal_type from ledger where ledger_name='" + ledger + "'";
                                ResultSet rs = stmt.executeQuery(sql1);
                                while (rs.next()) {
                                    curr_bal = rs.getDouble("curr_bal");
                                    currbal_type = rs.getString("currbal_type");
                                }

                                System.out.println("ledger name is " + ledger);
                                System.out.println("current balance is" + curr_bal);
                                System.out.println("current balance type is" + currbal_type);

                                if (!currbal_type.equals(type)) {

                                    if (Double.parseDouble(amount) > curr_bal) {
                                        currbal_type = type;
                                    }

                                    curr_bal = curr_bal - Double.parseDouble(amount);
                                    System.out.println("curr BAL: is" + ledger + "   " + curr_bal);
                                    curr_bal = Math.abs(curr_bal);
                                } else if (currbal_type.equals(type)) {
                                    curr_bal = curr_bal + Double.parseDouble(amount);
//                        currbal_type=currbal_type;
                                }
                                sql = "UPDATE ledger  SET curr_bal='" + curr_bal + "',currbal_type='" + currbal_type + "' where ledger_name='" + ledger + "'";
//                      st.executeUpdate(sql);
                                System.out.println("update query is " + "UPDATE ledger  SET curr_bal='" + curr_bal + "',currbal_type='" + currbal_type + "' where ledger_name='" + ledger + "'");
                                temp[i] = sql;
                             
                                
                               
                            }
                            System.out.println("Query is " + temp[i]);
                            /*readlength += i;
                            System.out.println("read length : "+readlength);
                            progress = (readlength*10)/100;*/
                           // System.out.println("progress is : "+progress);
                           
                            
                                      
                                       st.executeUpdate(temp[i]);
      
                            //        st.addBatch(temp[i]);
                                      
                                  
                        }
                               Statement st2=con.createStatement();
                                st2.executeUpdate("insert into file_import values('"+fName+"')");
                        //  st.executeBatch();
                        con.commit();
                        st.close();
                        con.close();
                        fin.close();
//                    FileOutputStream fout = new FileOutputStream(fileName);
//                    line = "";
//                    byte b[] = line.getBytes();//converting string into byte array
//                    fout.write(b);
//                    fout.close();
                        JOptionPane.showMessageDialog(null, "data imported successfully");
                        frame.dispose();
                        System.out.println();
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                        con.rollback();
                    } catch (IOException ex) {
                        // Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(importdata1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                } catch (SQLException ex) {
                    // Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (result == JFileChooser.CANCEL_OPTION) {
                System.out.println("Cancel was selected");
            }
            return null;
        }

        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done() {
            Toolkit.getDefaultToolkit().beep();
            jButton1.setEnabled(true);
            setCursor(null); //turn off the wait cursor
            /*jTextArea1.append("Done!\n");*/
        }
    }

    public importdata1() {
        initComponents();
        jButton1.setActionCommand("file Upload");
        jButton1.addActionListener(this);
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar(0,100);
        jButton1 = new javax.swing.JButton();

        jProgressBar1.setValue(0);
        jProgressBar1.setStringPainted(true);
        jProgressBar1.setPreferredSize(new java.awt.Dimension(200, 19));

        jButton1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jButton1.setText("Import Data");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(142, 142, 142)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(146, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(jButton1)
                .addGap(29, 29, 29)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleParent(this);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
                   
// TODO add your handling code here:
       
       /*task = new Task();
        task.addPropertyChangeListener(this);
        task.execute(); */
          /*  input= new JFileChooser();
            int result = input.showSaveDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                try {
                     file = input.getSelectedFile();
                    fileName = file.toString();
                    c = new connection();
                     con = c.cone();
                    Statement st = con.createStatement();
                    String sql = "";
                    try {
                        con.setAutoCommit(false);
                        String line = "";
//                    FileInputStream fin = new FileInputStream(fileName);
                        BufferedReader fin = new BufferedReader(new FileReader(fileName));
                           
                        int i = 0;
                        /*int readlength=0;
                      int totalLength= line.length();
                        double lengthPerPercent = 100.0 / totalLength;*/
                     /*   while ((i = fin.read()) != -1) {
                            line += (char) i;
                            //  System.out.println("line" +line);
                        }
                        String[] temp;
                        temp = line.split("/t@#");
                        
                      
                        for (i = 0; i < temp.length; i++) {

                            if (temp[i].contains("Update") && temp[i].contains("ledger") && temp[i].contains("ledger_name")) {
                                String strArray[] = temp[i].split("'");
                                String amount = strArray[1];
                                String type = strArray[3];
                                String ledger = strArray[5];
                                String currbal_type = "";
                                double curr_bal = 0.0;
                                Statement stmt = con.createStatement();

                                String sql1 = "select curr_bal,currbal_type from ledger where ledger_name='" + ledger + "'";
                                ResultSet rs = stmt.executeQuery(sql1);
                                while (rs.next()) {
                                    curr_bal = rs.getDouble("curr_bal");
                                    currbal_type = rs.getString("currbal_type");
                                }

                                System.out.println("ledger name is " + ledger);
                                System.out.println("current balance is" + curr_bal);
                                System.out.println("current balance type is" + currbal_type);

                                if (!currbal_type.equals(type)) {

                                    if (Double.parseDouble(amount) > curr_bal) {
                                        currbal_type = type;
                                    }

                                    curr_bal = curr_bal - Double.parseDouble(amount);
                                    System.out.println("curr BAL: is" + ledger + "   " + curr_bal);
                                    curr_bal = Math.abs(curr_bal);
                                } else if (currbal_type.equals(type)) {
                                    curr_bal = curr_bal + Double.parseDouble(amount);
//                        currbal_type=currbal_type;
                                }
                                sql = "UPDATE ledger  SET curr_bal='" + curr_bal + "',currbal_type='" + currbal_type + "' where ledger_name='" + ledger + "'";
//                      st.executeUpdate(sql);
                                System.out.println("update query is " + "UPDATE ledger  SET curr_bal='" + curr_bal + "',currbal_type='" + currbal_type + "' where ledger_name='" + ledger + "'");
                                temp[i] = sql;

                            }
                            System.out.println("Query is " + temp[i]);
                            /*readlength += i;
                            System.out.println("read length : "+readlength);
                            progress = (readlength*10)/100;*/
                           // System.out.println("progress is : "+progress);
                            //setProgress(Math.min(progress, 100));
                            
                            
                            /* st.executeUpdate(temp[i]);
      
                            //        st.addBatch(temp[i]);
                        }
                        //  st.executeBatch();
                        con.commit();
                        st.close();
                        con.close();
                        fin.close();
//                    FileOutputStream fout = new FileOutputStream(fileName);
//                    line = "";
//                    byte b[] = line.getBytes();//converting string into byte array
//                    fout.write(b);
//                    fout.close();
                        JOptionPane.showMessageDialog(this, "data imported successfully");
                        System.out.println();
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                        con.rollback();
                    } catch (IOException ex) {
                        // Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (SQLException ex) {
                    // Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (result == JFileChooser.CANCEL_OPTION) {
                System.out.println("Cancel was selected");
            }
*/
    }//GEN-LAST:event_jButton1ActionPerformed
public static void createAndShowGUI() {
       frame= new JFrame("Import Data");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JComponent newContentPane = new importdata1();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables
}
