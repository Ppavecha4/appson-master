/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Printing;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PDFRenderer;
import com.barcodelib.barcode.Linear;
import static com.itextpdf.text.Annotation.URL;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Font;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Random;

/**
 *
 * @author GARV
 */
public class Bar {
    public static void main (String[] args) throws Exception
    {
        for(int i=0; i<1; i++)
        {
//        testCODABAR();
        testCODE128();
        Document document = new Document();
try
{
    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("AddImageExample.pdf"));
    Rectangle rect1 = new Rectangle(30,30,125,125); 
    document.setPageSize(rect1);
    document.setMargins(1, 1, 1, 1);
    document.open();
    
    
    //document.add(new Paragraph("Image Example"));
    
   
    Image image1 = Image.getInstance("code128.gif");
   
    System.out.println( document.getPageSize());
    System.out.println();
    image1.scaleAbsolute(75, 30);
    document.add(image1);
 com.itextpdf.text.Font fontbold = FontFactory.getFont("Times-Roman", 6, Font.PLAIN);
 document.add(new Paragraph("Image Example",fontbold));
    document.close();
    writer.close();
    FileInputStream fis = new FileInputStream("AddImageExample.pdf");
   PrintPdf printPDFFile = new PrintPdf(fis, "Test Print PDF");
   printPDFFile.print();
} catch (Exception e)
{
e.printStackTrace();
}
        }
    }
    
    
     public static void testCODE128() throws Exception
    {
        Linear barcode = new Linear();
        barcode.setType(Linear.CODE128);
        // barcode data to encode
         Random rand = new Random();

    // nextInt is normally exclusive of the top value,
    // so add 1 to make it inclusive
    int randomNum = rand.nextInt(999999999);
        String a=Integer.toString(123456);
        barcode.setData(a);

        // unit of measure for X, Y, LeftMargin, RightMargin, TopMargin, BottomMargin
        barcode.setUOM(Linear.UOM_PIXEL);
        // barcode module width in pixel
        barcode.setX(3f);
        // barcode module height in pixel
        barcode.setY(75f);

        barcode.setLeftMargin(0f);
        barcode.setRightMargin(0f);
        barcode.setTopMargin(0f);
        barcode.setBottomMargin(0f);
        // barcode image resolution in dpi
        barcode.setResolution(72);

        // disply human readable text under the barcode
        barcode.setShowText(true);
        // human reable text font style
        barcode.setTextFont(new Font("Arial", 0, 24));
        //  ANGLE_0, ANGLE_90, ANGLE_180, ANGLE_270
        barcode.setRotate(Linear.ANGLE_0);

        barcode.renderBarcode("code128.gif");
    }
     
     
//      private static void testCODABAR() throws Exception
//    {
//        Linear barcode = new Linear();
//        barcode.setType(Linear.CODABAR);
//        // barcode data to encode
//        barcode.setData("123456789012");
//        barcode.setN(3);
// // unit of measure for X, Y, LeftMargin, RightMargin, TopMargin, BottomMargin
//        barcode.setUOM(Linear.UOM_PIXEL);
//        // barcode module width in pixel
//        barcode.setX(3f);
//        // barcode module height in pixel
//        barcode.setY(75f);
//
//        barcode.setLeftMargin(0f);
//        barcode.setRightMargin(0f);
//        barcode.setTopMargin(0f);
//        barcode.setBottomMargin(0f);
//        // barcode image resolution in dpi
//        barcode.setResolution(72);
//
//        // disply human readable text under the barcode
//        barcode.setShowText(true);
//        // human reable text font style
//        barcode.setTextFont(new Font("Arial", 0, 12));
//        //  ANGLE_0, ANGLE_90, ANGLE_180, ANGLE_270
//        barcode.setRotate(Linear.ANGLE_0);
//
//        barcode.renderBarcode("codabar.gif");
//
//    }
}
