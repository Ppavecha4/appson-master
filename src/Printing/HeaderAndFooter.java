/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Printing;

import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author app
 */
public class HeaderAndFooter {
  
   
    
    public static String getHeader(){
        String header = null;
        try {
            Connection con=new connection().cone();
            Statement st=con.createStatement();
            ResultSet rs=st.executeQuery("select * from cmp_detail");
            while(rs.next()){
                header=rs.getString(1)+" "+rs.getString(2);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HeaderAndFooter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
         
      return header;   
    }
    public static String getHeaderAddress1(){
        String headeradd = null;
        try {
            Connection con=new connection().cone();
            Statement st=con.createStatement();
            ResultSet rs=st.executeQuery("select * from cmp_detail");
            while(rs.next()){
                headeradd=rs.getString(3)+", "+rs.getString(4);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HeaderAndFooter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
         
      return headeradd;   
    }
    public static String getHeaderAddress2(){
        String headeradd = null;
        try {
            Connection con=new connection().cone();
            Statement st=con.createStatement();
            ResultSet rs=st.executeQuery("select * from cmp_detail");
            while(rs.next()){
                headeradd=rs.getString(5)+", "+rs.getString(6);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HeaderAndFooter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
         
      return headeradd;   
    }
    public static String getHeaderContact(){
        String headercon = null;
        try {
            Connection con=new connection().cone();
            Statement st=con.createStatement();
            ResultSet rs=st.executeQuery("select * from cmp_detail");
            while(rs.next()){
                headercon="Ph.No.- "+rs.getString(7)+", Mob.No.- "+rs.getString(13)+","+rs.getString(8);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HeaderAndFooter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
         
      return headercon;   
    }
   
}
