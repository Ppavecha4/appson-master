package Printing;

import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author admin
 */
public class tagdetail extends javax.swing.JFrame {

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyy");
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyy-mm-dd");
    String dateNow = formatter.format(currentDate.getTime());
    int s = 0;
    String tag = null;
    String bill = null;
    boolean isexist = false;

    /**
     * Creates new form tagdetail
     */
    private static tagdetail obj = null;

    public tagdetail() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public static tagdetail getObj() {
        if (obj == null) {
            obj = new tagdetail();
        }
        return obj;
    }

// Getting tag details
    public void getTag_Details(String choice) {
        isexist = false;
        int rowcount = jTable1.getRowCount();
        isexist = false;
        for (int i = 0; i < rowcount; i++) {
            tag = jTable1.getValueAt(i, 0).toString();
            bill = jTable1.getValueAt(i, 2).toString();
            if (tag.equals(jTextField1.getText())) {
                isexist = true;
            } 
          if (bill.equals(jTextField2.getText())) {
                isexist = true;
            } 
        }

        if (isexist == true) {
            JOptionPane.showMessageDialog(rootPane, "TAG DETAIL ALREADY EXIST");
            jTextField1.setText(null);
            jTextField2.setText(null);
            jTextField1.requestFocus();
        } else {
            DefaultTableModel dtm;
            dtm = (DefaultTableModel) jTable1.getModel();
            //  dtm.getDataVector().removeAllElements();
            ResultSet rs = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            ResultSet rs4 = null;
            ResultSet rs5 = null;

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                String Partyname = "";
                rs = st.executeQuery("SELECT  * FROM Stockid where Stock_No='" + choice + "' ");
                while (rs.next()) {
                    ResultSet rs9 = st1.executeQuery("select party_name from party where party_code='" + rs.getString("Party") + "'");
                    while (rs9.next()) {
                        Partyname = rs9.getString(1);
                    }
                    String desc = rs.getString("De_sc");
                    String des_code = desc.replaceAll("1","M").replaceAll("2", "A").replaceAll("3", "H").replaceAll("4", "V").replaceAll("5", "E").replaceAll("6", "e").replaceAll("7", "R").replaceAll("8", "J").replaceAll("9", "I").replaceAll("0", "Z");
                    Date date2 = formatter1.parse(rs.getString("Da_te"));
                    String date02 = formatter.format(date2);
                    Object o[] = {rs.getString("Stock_No"), Partyname, rs.getString("invoice_number"), "Purchase", rs.getString("branchid"), date02, rs.getString("Ra_te"), rs.getString("Re_tail"), des_code , rs.getString("Pro_duct")};
                    dtm.addRow(o);
                }

                rs2 = st.executeQuery("Select * from StockTransfer where Stock_no='" + choice + "'");

                while (rs2.next()) {
                    Date date2 = formatter1.parse(rs2.getString("St_date"));
                    String date02 = formatter.format(date2);
                    Object o1[] = {rs2.getString("Stock_no"), "", "", "Stock Transfer", rs2.getString("To_branch"), date02, rs2.getString("Ra_te"), rs2.getString("ret_ail"), rs2.getString("prod_desc"), rs2.getString("produ_ct")};
                    dtm.addRow(o1);
                }

                rs3 = st.executeQuery("Select * from billing where Stock_no='" + choice + "' ");
                while (rs3.next()) {
                    Date date2 = formatter1.parse(rs3.getString("date"));
                    String date02 = formatter.format(date2);
                    Object o2[] = {rs3.getString("stock_no"), rs3.getString("customer_name"), rs3.getString("bill_refrence"), "Sales", rs3.getString("branchid"), date02, "", rs3.getString("rate"), "", rs3.getString("description")};
                    dtm.addRow(o2);
                }

                rs4 = st.executeQuery("Select * from salesreturn where stock_no='" + choice + "'");
                while (rs4.next()) {
                    Date date2 = formatter1.parse(rs4.getString("date"));
                    String date02 = formatter.format(date2);
                    Object o3[] = {rs4.getString("stock_no"), rs4.getString("customer_name"), rs4.getString("bill_refrence"), "Sales Return", rs4.getString("branchid"), date02, "", rs4.getString("rate"), "", rs4.getString("description")};
                    dtm.addRow(o3);
                }

                rs1 = st.executeQuery("Select * from purchasereturndualgst where Stock_no='" + choice + "'");
                while (rs1.next()) {
                    Date date2 = formatter1.parse(rs1.getString("date"));
                    String date02 = formatter.format(date2);
                    Object o4[] = {rs1.getString("stock_no"), rs1.getString("by_ledger"), rs1.getString("bill_refrence"), "Purchase Return", "HO", date02, rs1.getString("rate"), "", "", rs1.getString("description")};
                    dtm.addRow(o4);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
            jTextField1.setText(null);
            jTextField2.setText(null);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON :TAG DETAIL");
        setAlwaysOnTop(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setText("Enter Tag NO. :");

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TAG No.", "Party", "Bill No.", "History", "Branch", "Date", "PPrice", "M.R.P", "Purchase Code", "Product"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(150);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(150);
            jTable1.getColumnModel().getColumn(7).setResizable(false);
        }

        jLabel2.setText("Enter Bill No. :");

        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            getTag_Details(jTextField1.getText());
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1KeyReleased

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select stock_no from stockid where invoice_number='" + jTextField2.getText() + "'");
                while (rs.next()) {
                    getTag_Details(rs.getString(1));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jTextField2KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(tagdetail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(tagdetail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(tagdetail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(tagdetail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new tagdetail().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
