/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


/**
 *
 * @author lenovo
 */
public class Scrolldemo extends JScrollPane {
 int screenWidth =Toolkit.getDefaultToolkit().getScreenSize().width;
 int screenHeight =Toolkit.getDefaultToolkit().getScreenSize().height;
    public Scrolldemo(Component cmpnt) {
         super(cmpnt);
       //  cmpnt.setSize(1500, 1500);
    }
    
     
    public void paintComponent(Graphics g) {
		double angle = Math.PI / 2;
		//double mid = Math.min(1350,1600) / 2.0;
                double mid=Math.max(getWidth(), getHeight())/2.0;
		Graphics2D g2d = (Graphics2D) g;
                System.out.println("a= "+mid+"   b= "+angle);
  		g2d.rotate(angle,mid,mid);
                
		super.paintComponent(g2d);
		//g2d.rotate(-angle,400,100);
              //  revalidate();
              repaint();           
    }
    
//     public void paint(Graphics g) {
//                for (int i = 0; i < 11; i++) {
//
//                    g.drawLine(i * 30, 0, i * 30, screenHeight);    
//                }
//                for (int j = 0; j < 11; j++) {
//                    g.drawLine(0, j * 30, screenWidth, j * 30);    
//               }
//                repaint();
//            }
    public Dimension getPreferredSize()
    {
      return new Dimension(screenHeight,screenWidth);
      
    }
    
    public static void main(String[] args) {
        JTextArea a=new JTextArea();
        
        JScrollPane sc=new Scrolldemo(a);
       // a.setPreferredSize(sc.getPreferredSize());
        
       sc.setViewportView(a);
      
        sc.setHorizontalScrollBarPolicy(JScrollPane. HORIZONTAL_SCROLLBAR_ALWAYS);
       sc.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
       
     //   sc.setHorizontalScrollBarPolicy(JScrollPane. HORIZONTAL_SCROLLBAR_ALWAYS);
        //sc.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
 sc.setVerticalScrollBar(sc.createVerticalScrollBar());
        sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getMinimum());
        sc.getHorizontalScrollBar().setValue(sc.getHorizontalScrollBar().getMinimum());
      a.setLineWrap(true);
        
        JFrame f = new JFrame("RotatedTextArea");
      //  f.setSize(400, 400);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//f.getContentPane().add(sc, BorderLayout.CENTER);
                f.getContentPane().add(sc,BorderLayout.PAGE_START);
                
               // f.getContentPane().add(a,BorderLayout.CENTER);
              // f.repaint();
		f.pack();
                
		f.setVisible(true);
    }
    
}
