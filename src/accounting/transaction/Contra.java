package accounting.transaction;

import connection.connection;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Aru and duru
 */
public class Contra extends javax.swing.JFrame {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    PreparedStatement ps = null;

    String to = null, by = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());

    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);

    static String DR, CR;

    /**
     * ]
     */
    private static Contra obj = null;
    public Contra() {
        initComponents();
        this.setLocationRelativeTo(null);
        jButton3.setVisible(false);

        try {
            jDateChooser1.setDate(formatter.parse(dateNow));
        } catch (ParseException ex) {
            Logger.getLogger(journ.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            java.util.Date dateafter = formatter.parse("31-03-20" + getyear + "");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String contra = "CO/";
        String slash = "/";
        String yearwise = (getyear.concat(nextyear)).concat(slash);
        String finalconcat = (contra.concat(yearwise));
        String no = null;

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            int a = 0, b, d = 0;
            int i = 0;
            ResultSet rs = st.executeQuery("SELECT  (s_no) FROM Contra");
            while (rs.next()) {
                if (i == 0) {
                    d = rs.getString(1).lastIndexOf("/");
                    i++;
                }
                b = Integer.parseInt(rs.getString(1).substring(d + 1));
                if (a < b) {
                    a = b;
                }

            }

            String s = finalconcat.concat(Integer.toString(a + 1));
            jTextField1.setText("" + s);
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')");
            while (rs2.next()) {
                choice2.addItem(rs2.getString(1));
            }

            ResultSet rs = st.executeQuery("select Name from Group_create where Under IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')");
            while (rs.next()) {
                String gname = rs.getString(1);
                ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='" + gname + "' ");
                while (rs1.next()) {
                    choice2.addItem(rs1.getString(1));
                }
            }
            st.close();
            st1.close();
            st2.close();
//			conn.close() ;
        } catch (Exception ex) {
            ex.printStackTrace();;
        }

        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement st = conn.createStatement();
            Statement stl = conn.createStatement();
            Statement stg = conn.createStatement();
            ResultSet rs = st.executeQuery("select ledger_name from ledger where groups IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')");

            while (rs.next()) {
                choice1.addItem(rs.getString(1));
            }

            ResultSet rsl = stl.executeQuery("select Name from Group_create where Under IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')");
            while (rsl.next()) {
                String gname = rsl.getString(1);
                ResultSet rsg = stg.executeQuery("select ledger_name from ledger where groups='" + gname + "' ");
                while (rsg.next()) {
                    choice1.addItem(rsg.getString(1));
                }
            }

            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();;
        }

        jTextField3.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {

            }
        });
    }
    
   public static Contra getObj(){
     if(obj == null){
      obj = new Contra();
     }
     return obj;
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        choice1 = new java.awt.Choice();
        choice2 = new java.awt.Choice();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON :CONTRA");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel1.setText("SNO. :");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel2.setText("DATE :");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel3.setText("AMOUNT ");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel5.setText("NARRATION :");

        jTextField1.setEditable(false);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(2);
        jTextArea1.setText("Ch No. : ");
        jTextArea1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextArea1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextArea1);

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel7.setText("PARTICULARS");

        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel4.setText("REC :");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel8.setText("GIV :");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel9.setText("CURR. BAL. :");

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel10.setText("CURR. BAL. :");

        jTextField3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                Add(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField3FocusLost(evt);
            }
        });
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        choice1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                choice1ItemStateChanged(evt);
            }
        });
        choice1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                choice1KeyPressed(evt);
            }
        });

        choice2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                choice2ItemStateChanged(evt);
            }
        });
        choice2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                choice2KeyPressed(evt);
            }
        });

        jTextField4.setEditable(false);
        jTextField4.setBackground(new java.awt.Color(204, 204, 255));
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField4Add(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField4FocusLost(evt);
            }
        });

        jTextField5.setEditable(false);
        jTextField5.setBackground(new java.awt.Color(204, 204, 255));
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField5Add(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField5FocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(choice2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField4, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                    .addComponent(jTextField5))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(jLabel9)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(choice2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(jLabel10)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel2);

        jButton3.setText("UPDATE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jButton3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButton1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(540, 540, 540))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2)
                                .addGap(5, 5, 5)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3)
                                .addGap(56, 56, 56))))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButton1)
                                .addComponent(jButton3)))))
                .addGap(11, 11, 11))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 10, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(735, 329));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
/*
    
    }*/
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

    }//GEN-LAST:event_formWindowClosing

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated

    }//GEN-LAST:event_formWindowActivated

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        setVisible(false);
    }//GEN-LAST:event_jButton2ActionPerformed
    public void save() {
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i == 0) {
            updatebalance();
            PreparedStatement ps1, ps = null;

            try {
                connection c = new connection();
                Connection conne = c.cone();
                Statement stll = conne.createStatement();
                Statement stll1 = conne.createStatement();

                stll.executeUpdate("insert into Contra values('" + jTextField1.getText() + "','" + formatter.format(jDateChooser1.getDate()) + "','" + choice1.getSelectedItem().toString() + "','" + choice2.getSelectedItem().toString() + "','" + jTextField3.getText() + "','" + jTextArea1.getText() + "','0')");
                //         stll.executeUpdate("insert into Contra_ho values('"+jTextField1.getText()+"','"+jTextField2.getText()+"','"+choice1.getSelectedItem().toString()+"','"+choice2.getSelectedItem().toString()+"','"+jTextField3.getText()+"','"+jTextArea1.getText()+"')");  

                String qd = "Update ledger SET curr_bal= '" + jTextField5.getText() + "',currbal_type='" + jLabel6.getText() + "' where ledger_name='" + choice1.getSelectedItem().toString() + "'";

                System.out.println("query for update 1 is" + qd);
                stll.executeUpdate(qd);

                String qdd = "Update ledger SET curr_bal= '" + jTextField4.getText() + "',currbal_type='" + jLabel11.getText() + "' where ledger_name='" + choice2.getSelectedItem().toString() + "'";
                System.out.println("query for update 2 is" + qdd);

                stll1.executeUpdate(qdd);
                JOptionPane.showMessageDialog(null, "Inserted Sucessfully.");
                stll.close();
                stll.close();

                conne.close();
            } catch (Exception e) {
                e.printStackTrace();

            }

        }
    }


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        save();
        Contra contra = new accounting.transaction.Contra();
        setVisible(false);
        contra.setVisible(true);
        contra.jDateChooser1.setDate(jDateChooser1.getDate());

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void choice1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_choice1ItemStateChanged
        // TODO add your handling code here:
        String choice = (String) evt.getItem();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            stmt = connect.createStatement();
            String cbt = null;
            rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + choice + "'");
            while (rs.next()) {
                jTextField5.setText(rs.getString(1));
                jLabel6.setText(rs.getString(2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_choice1ItemStateChanged

    private void choice2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_choice2ItemStateChanged
        // TODO add your handling code here:
        String choice = (String) evt.getItem();
        try {
            connection c = new connection();
            Connection conn = c.cone();
            stmt = conn.createStatement();
            String cbt = null;
            rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + choice + "'");
            rs.next();
            cbt = rs.getString(2);
            to = rs.getString(1);
            jTextField4.setText(to);
            jLabel11.setText(cbt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_choice2ItemStateChanged
    public void updatebalance() {
        if (isupdate == true) {
            try {
                connection c = new connection();
                Connection conn = c.cone();
                stmt = conn.createStatement();
                String cbt = null;
                rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + choice2.getSelectedItem() + "" + "'");
                rs.next();
                cbt = rs.getString(2);
                to = rs.getString(1);
                jTextField4.setText(to);
                jLabel11.setText(cbt);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                connection c = new connection();
                Connection connect = c.cone();
                stmt = connect.createStatement();
                String cbt = null;
                rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + choice1.getSelectedItem() + "'");
                while (rs.next()) {
                    jTextField5.setText(rs.getString(1));
                    jLabel6.setText(rs.getString(2));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        String recieveramount = jTextField4.getText();
        String giveramount = jTextField5.getText();

        double totalamount = Double.parseDouble(jTextField3.getText());

        System.out.println("giver amount is on save" + giveramount);
        System.out.println("total amount for save prateek is " + jTextField3.getText());
        double recieveramount1 = Double.parseDouble(recieveramount);
        double giveramount1 = Double.parseDouble(giveramount);
        double bc = 0;

        if (jLabel11.getText().equals("DR")) {

            recieveramount1 = recieveramount1 + totalamount;
            jTextField4.setText("" + recieveramount1);

            System.out.println("condition 1 is running");
        }
        if (jLabel11.getText().equals("CR")) {
            System.out.println("codeis runninmg");
            if (recieveramount1 > totalamount) {
                recieveramount1 = (recieveramount1 - totalamount);
                jTextField4.setText("" + recieveramount1);
                System.out.println("condition 2 is running");
            } else if (totalamount > recieveramount1) {
                recieveramount1 = (totalamount - recieveramount1);
                jTextField4.setText("" + recieveramount1);
                jLabel11.setText("DR");
                System.out.println("condition 3 is running");
            }
        }

        if (jLabel6.getText().equals("CR")) {

            giveramount1 = giveramount1 + totalamount;
            jTextField5.setText("" + giveramount1);
            System.out.println("condition 4 is running");
        } else if (jLabel6.getText().equals("DR")) {

            if (giveramount1 > totalamount) {
                giveramount1 = giveramount1 - totalamount;
                jTextField5.setText("" + giveramount1);
                System.out.println("giver amount 1 is" + giveramount1);
                System.out.println("condition 5 is running");
            } else if (giveramount1 <= totalamount) {
                giveramount1 = totalamount - giveramount1;
                jTextField5.setText("" + giveramount1);
                jLabel6.setText("CR");
                System.out.println("condition 6 is running");
            }
        }

    }
    private void jTextField3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField3FocusLost
//        String str = jTextField3.getText();
        aftervalue = Double.parseDouble(jTextField3.getText());
        if (aftervalue == beforevalue) {

        } else if (jTextField3.getText().isEmpty()) {

        } else {

        }

    }//GEN-LAST:event_jTextField3FocusLost
    double beforevalue = 0;
    double aftervalue = 0;


    private void Add(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_Add
        // TODO add your handling code here:

        if (!jTextField3.getText().isEmpty()) {
            beforevalue = Double.parseDouble(jTextField3.getText());
        }
    }//GEN-LAST:event_Add

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField4Add(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField4Add
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4Add

    private void jTextField4FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField4FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4FocusLost

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jTextField5Add(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField5Add
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5Add

    private void jTextField5FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField5FocusLost
    }//GEN-LAST:event_jTextField5FocusLost

    String recieverledger = "";
    String giverledger = "";
    double amount = 0;

    double recievercurrentbalance = 0;
    String recievercurrentbalancetype = "";
    double givercurrentbalance = 0;
    String givercurrentbalancetype = "";

    public boolean isupdate = false;

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to update");
        if (i == 0) {
            isupdate = true;
            try {
                connection c = new connection();
                Connection conne = c.cone();
                Statement st = conne.createStatement();
                Statement st1 = conne.createStatement();
                Statement st2 = conne.createStatement();
                Statement st3 = conne.createStatement();
                Statement st4 = conne.createStatement();
                Statement st5 = conne.createStatement();

                ResultSet rs = st.executeQuery("select * from contra where  s_no='" + jTextField1.getText() + "'");

                while (rs.next()) {
                    giverledger = rs.getString(3);
                    recieverledger = rs.getString(4);
                    amount = Double.parseDouble(rs.getString(5));
                }

                ResultSet rs1 = st1.executeQuery("select * from ledger where ledger_name='" + recieverledger + "'");

                while (rs1.next()) {
                    recievercurrentbalance = Double.parseDouble(rs1.getString(10));
                    recievercurrentbalancetype = rs1.getString(11);
                }

                ResultSet rs2 = st2.executeQuery("select * from ledger where ledger_name='" + giverledger + "'");

                while (rs2.next()) {
                    givercurrentbalance = Double.parseDouble(rs2.getString(10));
                    givercurrentbalancetype = rs2.getString(11);
                }

                double bc = 0;

                if (recievercurrentbalancetype.equals("CR")) {

                    recievercurrentbalance = recievercurrentbalance + amount;

                    System.out.println("condition 1 is running");
                }
                if (recievercurrentbalancetype.equals("DR")) {
                    System.out.println("codeis runninmg");
                    if (recievercurrentbalance > amount) {
                        recievercurrentbalance = (recievercurrentbalance - amount);

                        System.out.println("condition 2 is running");
                    }
                    if (amount > recievercurrentbalance) {
                        recievercurrentbalance = (amount - recievercurrentbalance);

                        recievercurrentbalancetype = ("CR");

                    }
                }

                if (givercurrentbalancetype.equals("DR")) {
                    givercurrentbalance = givercurrentbalance + amount;

                } else if (givercurrentbalancetype.equals("CR")) {
                    if (givercurrentbalance > amount) {
                        givercurrentbalance = givercurrentbalance - amount;

                    }

                    if (givercurrentbalance < amount) {
                        givercurrentbalance = amount - givercurrentbalance;

                        givercurrentbalancetype = "DR";
                        System.out.println("condition 6 is running");
                    }
                }

                System.out.println("giver current balance type is " + givercurrentbalancetype);
                jLabel6.setText(givercurrentbalancetype);
                jTextField5.setText("" + givercurrentbalance);
                jLabel11.setText(recievercurrentbalancetype);
                jTextField4.setText("" + recievercurrentbalance);
                st3.executeUpdate("update ledger set curr_bal='" + recievercurrentbalance + "',currbal_type='" + recievercurrentbalancetype + "' where ledger_name='" + recieverledger + "'");
                System.out.println("update for reciever balance " + "update ledger set curr_bal='" + recievercurrentbalance + "',currbal_type='" + recievercurrentbalancetype + "' where ledger_name='" + recieverledger + "'");
                st4.executeUpdate("update ledger set curr_bal='" + givercurrentbalance + "',currbal_type='" + givercurrentbalancetype + "' where ledger_name='" + giverledger + "'");
                System.out.println("update for giver balance " + "update ledger set curr_bal='" + givercurrentbalance + "',currbal_type='" + givercurrentbalancetype + "' where ledger_name='" + giverledger + "'");

                st5.executeUpdate("delete from contra where s_no='" + jTextField1.getText() + "'");

//      jTextField5.setText("");
//      jLabel6.setText("");
//      jTextField4.setText("");
//       jLabel11.setText("");
                save();

            } catch (SQLException ex) {

                ex.printStackTrace();
            }

            dispose();

        }


    }//GEN-LAST:event_jButton3ActionPerformed

    private void choice1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_choice1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            choice2.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_choice1KeyPressed

    private void choice2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_choice2KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            choice1.requestFocus();
        }
    }//GEN-LAST:event_choice2KeyPressed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextArea1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            choice2.requestFocus();
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextArea1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField3.requestFocus();
        }

    }//GEN-LAST:event_jTextArea1KeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jDateChooser1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        try {
            jDateChooser1.setDate(formatter.parse(dateNow));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    obj = null; 
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Contra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Contra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Contra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Contra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Contra().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public java.awt.Choice choice1;
    public java.awt.Choice choice2;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel10;
    public javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    public javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTextArea jTextArea1;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    // End of variables declaration//GEN-END:variables
}
