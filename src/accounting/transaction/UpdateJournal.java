/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accounting.transaction;

/**
 *
 * @author PRATEEK
 */
public class UpdateJournal {
    
   private int vsn=0;

    public int getVsn() {
        return vsn;
    }

    public void setVsn(int vsn) {
        this.vsn = vsn;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getByledger() {
        return byledger;
    }

    public void setByledger(String byledger) {
        this.byledger = byledger;
    }

    public String getToledger() {
        return toledger;
    }

    public void setToledger(String toledger) {
        this.toledger = toledger;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getDebitamnt() {
        return debitamnt;
    }

    public void setDebitamnt(String debitamnt) {
        this.debitamnt = debitamnt;
    }

    public String getCreditamnt() {
        return creditamnt;
    }

    public void setCreditamnt(String creditamnt) {
        this.creditamnt = creditamnt;
    }

    public String getBranchid() {
        return branchid;
    }

    @Override
    public String toString() {
        return "UpdateJournal{" + "vsn=" + vsn + ", sno=" + sno + ", date=" + date + ", byledger=" + byledger + ", toledger=" + toledger + ", narration=" + narration + ", debitamnt=" + debitamnt + ", creditamnt=" + creditamnt + ", branchid=" + branchid + '}';
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }
   private String sno;
   private String date;
   private String byledger;
   private String toledger;
   private String narration;
   private String debitamnt;
   private String creditamnt;
   private String branchid;
   
}
