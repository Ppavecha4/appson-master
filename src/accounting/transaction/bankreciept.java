package accounting.transaction;

import com.toedter.calendar.JTextFieldDateEditor;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.*;

public class bankreciept extends javax.swing.JFrame implements Printable {

    public int a = 0;
    public int point, point1, point2, point3, point4, point5, point6, point7, point8, point9, point10, point11, point12, point13, point14, point15, point16, point17 = 0;
    public String toby[] = new String[50];
    public int selected;
    public String baltypecd;
    public int row = -1;
    public String currentcbt;
    public String rcamntv;
    public List RowList;
    public newRow aFirstRow = null;
    public JComboBox jcb1, jcb2, jcb3, jcb4, jcb5, jcb6, jcb7, jcb8, jcb9, jcb10, jcb11, jcb12, jcb13, jcb14, jcb15, jcb16, jcb17, jcb18, jcb19, jcb20,
            jcb21, jcb22, jcb23, jcb24, jcb25, jcb26, jcb27, jcb28, jcb29, jcb30, jAccountNameCobmoBox, jcb32, jcb33, jcb34, jcb35, jcb36, jcb37, jcb38, jcb49, jcb40;
    public JTextField jtf1, jtf2, jtf3, jtf4, jtf5, jtf6, jtf7, jtf8, jtf9, jtf10, jtf11, jtf12, jtf13, jtf14, jtf15, jtf16, jtf17, jtf18, jtf19, jtf20,
            jtf21, jtf22, jtf23, jtf24, jtf25, jtf26, jtf27, jtf28, jtf29, jtf30, jTextField, jtf32, jtf33, jtf34, jtf35, jtf36, jtf37, jtf38, jtf39, jtf40;
    public JLabel jcl1, jcl2, jcl3, jcl4, jcl5, jcl6, jcl7, jcl8, jcl9, jcl10, jcl11, jcl12, jcl13, jcl14, jcl15, jcl16, jcl17, jcl18, jcl19, jcl20, jcl21, jcl22, jcl23, jcl24, jcl25, jcl26, jcl27, jcl28, jcl29, jcl30, jcl31, jcl32, jcl33, jcl34, jcl35, jcl36, jcl37, jcl38, jcl39, jcl40, jcl41, jcl42, jcl43, jcl44, jcl45, jcl46, jcl47, jcl48, jcl49;
    public JPanel jpanel3;
    public String name[] = new String[100];
    public String checque[] = new String[100];
    public double bal[] = new double[100];
    public double currbal[] = new double[100];
    public String currbaltype[] = new String[100];
    public int count1 = 0;
    public int count2 = 0;
    public int count3 = 0;
    public int count = 0;
    public int s_no[] = new int[100];
    public String da_te[] = new String[100];
    public String by_ledger[] = new String[100];
    public String narra_tion[] = new String[100];
    public String grand_total[] = new String[100];
    public JComboBox jc1;
    public JTextField jt;
    public JLabel jl;
    public Map aRowNoRowObjectMap = new LinkedHashMap(), aLedgerNameCurrentBalMap = new LinkedHashMap(), aLedgerNameCurrentBalMapt = new LinkedHashMap();
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    public double initialPayAccountBal = 0.0;
    public String byledger;
    private Map<String, Double> ledgerNameGrandTotalMapForUpdate = new LinkedHashMap<String, Double>();

    DecimalFormat df = new DecimalFormat("0.00");
    int no_of_copies = 1;

    private static bankreciept obj = null;

    public bankreciept() {
        initComponents();
        this.setLocationRelativeTo(null);
        jDialog4.setLocationRelativeTo(null);
        try {
            JTextFieldDateEditor editor = (JTextFieldDateEditor) jDateChooser1.getDateEditor();
            editor.setEditable(false);
            jDateChooser1.setDate(formatter.parse(dateNow));
            java.util.Date dateafter = formatter.parse("31-03-20" + getyear + "");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jButton3.setVisible(false);
        otherComponents();

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            String payment = "BRC/";
            String slash = "/";
            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String finalconcat = (payment.concat(yearwise));
            int a = 0, b, d = 0;
            int i = 0;
            ResultSet rs = st.executeQuery("SELECT  (s_no) FROM bankreceipt where branchid='0'");
            while (rs.next()) {
                if (i == 0) {
                    d = rs.getString(1).lastIndexOf("/");
                    i++;
                }
                b = Integer.parseInt(rs.getString(1).substring(d + 1));
                if (a < b) {
                    a = b;
                }
            }
            String s = finalconcat.concat(Integer.toString(a + 1));
            jTextField1.setText("" + s);
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        if (a == 0) {
            try {
                connection c = new connection();
                Connection conn = c.cone();
                Statement st = conn.createStatement();
                Statement stl = conn.createStatement();
                Statement stg = conn.createStatement();
                ResultSet rs = st.executeQuery("select ledger_name from ledger where groups='BANK ACCOUNTS' or groups = 'BANK OCC A/C' or groups='BANK OD A/C' ");

                while (rs.next()) {
                    choice1.addItem(rs.getString(1));
                }

                ResultSet rsl = stl.executeQuery("select Name from Group_create where Under IN('CASH IN HAND')");
                while (rsl.next()) {
                    String gname = rsl.getString(1);
                    ResultSet rsg = stg.executeQuery("select ledger_name from ledger where groups='" + gname + "' ");
                    while (rsg.next()) {
                        choice1.addItem(rsg.getString(1));
                    }
                }
                conn.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            String str = (String) choice1.getSelectedItem();
            try {
                connection c = new connection();
                Connection conn = c.cone();
                Statement stmt = conn.createStatement();
                String cbt = null;
                ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
                while (rs.next()) {
                    initialPayAccountBal = Double.parseDouble(rs.getString(1));
                    currentcbt = rs.getString(2);
                    jTextField5.setText(initialPayAccountBal + "");
                    jLabel8.setText(currentcbt);
                }
                conn.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        jcb1 = new JComboBox();
        jtf1 = new JTextField();
        aFirstRow = new newRow();
    }

    public static bankreciept getObj() {
        if (obj == null) {
            obj = new bankreciept();
        }
        return obj;
    }

    public Map<String, Double> getLedgerNameGrandTotalMapForUpdate() {
        return ledgerNameGrandTotalMapForUpdate;
    }

    public void setLedgerNameGrandTotalMapForUpdate(Map<String, Double> ledgerNameGrandTotalMapForUpdate) {
        this.ledgerNameGrandTotalMapForUpdate = ledgerNameGrandTotalMapForUpdate;
    }

    public SecondRow createAnotherRow() {
        SecondRow aSecondRow = new SecondRow();
        aRowNoRowObjectMap.put(row + 1, aSecondRow);
        return aSecondRow;
    }

    public class newRow {

        double valuebefore = 0;
        double valueafter = 0;
        int selected;
        public SecondRow aSecondRow = null;

        public newRow() {
            selected = ++row;
            newRow anewRow = null;
            jcb1 = new JComboBox();
            jcb2 = new JComboBox();
            jtf1 = new JTextField();
            jtf2 = new JTextField();
            //   jcl1 = new JLabel();
            //  jPanel3.add(jcl1);
//            jcl1.setVisible(true);
//            jcl1.setBounds(120, 10 + (row * 40), 800, 25);
            jcl17 = new JLabel();
            jPanel3.add(jcl17);
            jcl17.setVisible(true);
            jcl17.setBounds(380, 10 + (row * 40), 800, 25);
            jcl18 = new JLabel();
            jPanel3.add(jcl18);
            jcl18.setVisible(true);
            jcl18.setBounds(430, 10 + (row * 40), 800, 25);
            jPanel3.add(jcb1);
            jPanel3.add(jtf1);
            jPanel3.add(jtf2);
            jcb1.setModel(new javax.swing.DefaultComboBoxModel(new String[]{""}));
            jcb1.setBounds(25, 10 + (row * 40), 325, 27);
            jtf1.setBounds(485, 10 + (row * 40), 60, 27);
            jtf2.setBounds(570, 10 + (row * 40), 60, 27);
            jcb1.setVisible(true);
            jtf1.setVisible(true);
            jtf1.setEnabled(false);
            jtf2.setVisible(true);
            jtf2.setEnabled(false);

            jcb1.requestFocus();
            try {
                jcb1.removeAll();
                connection c = new connection();
                Connection conn = c.cone();
                Statement st = conn.createStatement();
                Statement st1 = conn.createStatement();
                Statement st2 = conn.createStatement();

                ResultSet rs2 = st2.executeQuery("select ledger_name,kth_code from ledger where groups IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH') ORDER BY ledger_name ASC");
                while (rs2.next()) {

                    jcb1.addItem(rs2.getString(1));
                }
                ResultSet rs = st.executeQuery("select Name from Group_create where Under IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH') ");
                while (rs.next()) {
                    String gname = rs.getString(1);
                    ResultSet rs1 = st1.executeQuery("select ledger_name,kth_code from ledger where groups='" + gname + "' ORDER BY ledger_name ASC ");
                    while (rs1.next()) {
                        jcb1.addItem(rs1.getString(1));
                    }
                }
                st.close();
                st1.close();
                st2.close();
                conn.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            jcb1.addKeyListener(new java.awt.event.KeyAdapter() {

                public void keyPressed(java.awt.event.KeyEvent e) {

                    int key = e.getKeyCode();
                    String sel = jcb1.getSelectedItem().toString();
                    if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_TAB) {
                        jtf1.setEnabled(true);
                        if (!sel.equals("")) {
                            name[count1] = (String) jcb1.getSelectedItem();
                            ++count1;
                        }
                        try {
                            connection c = new connection();
                            Connection conn = c.cone();
                            Statement stmt = conn.createStatement();
                            String cbt = null;
                            String str = jcb1.getSelectedItem().toString();
                            System.out.println(str);
                            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
                            while (rs.next()) {
                                baltypecd = rs.getString(2);
                                rcamntv = rs.getString(1);
                                //       jcl1.setText("CURRENT BAL : -  ");
                                jcl17.setText(rcamntv);
                                jcl18.setText(baltypecd);

                            }
                            if (aLedgerNameCurrentBalMap.containsKey(str)) {
                                jcl7.setText(aLedgerNameCurrentBalMap.get(str).toString());
                                jcl8.setText(aLedgerNameCurrentBalMapt.get(str).toString());
                            }
                            stmt.close();
                            conn.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        jtf1.requestFocus();
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jDateChooser1.requestFocus();
                    }
                }
            });

            jcb1.addFocusListener(new FocusAdapter() {

                @Override
                public void focusLost(FocusEvent e) {
                }
            });
            jtf1.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    valueafter = Double.parseDouble(jtf1.getText());
                }
            });

            jtf1.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    if (jtf1.getText().isEmpty()) {
                        valuebefore = 0;
                    } else {
                        valuebefore = Double.parseDouble(jtf1.getText());
                    }
                }
            });

            jtf1.addKeyListener(new java.awt.event.KeyAdapter() {

                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();

                    if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_TAB) {

                        if (count2 == 0) {
                            bal[count2] = Double.parseDouble(jtf1.getText());
                            System.out.println(" a " + jcl17.getText());
                            currbal[count2] = Double.parseDouble(jcl17.getText());
                            currbaltype[count2] = jcl18.getText();
                            jTextField4.setText("" + bal[count2]);
                            ++count2;
                        } else if (valuebefore != valueafter) {

                        }

                        //        checque[count2]=jtf2.getText();
                        updateBal(jcb1.getSelectedItem().toString());
                        updatePayAccountBal();
                        Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
                        int aRowNo = 0;
                        SecondRow aSecondRow = null;

                        double ramn = 0, sum = 0;
                        if (jtf1.getText() != null && jtf1.getText().trim().length() > 0) {
                            sum = Double.parseDouble(jtf1.getText());
                        }
                        while (aRowNoItertor.hasNext()) {
                            aRowNo = (Integer) aRowNoItertor.next();
                            aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);

                            if (aSecondRow.jTextField != null && aSecondRow.jTextField.getText().trim().length() > 0) {

                                ramn = Double.parseDouble(aSecondRow.jTextField.getText());
                                sum = sum + ramn;
                            }
                        }
                        jTextField4.setText("" + sum);

                        jtf2.requestFocus();
                        jtf2.setEnabled(true);

                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jcb1.requestFocus();
                    }
                }
            });

            jtf2.addKeyListener(new java.awt.event.KeyAdapter() {

                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_TAB) {
                        checque[count3] = jtf2.getText();
                        ++count3;

                        ++point1;

                        if (point1 == 1) {
                            aSecondRow = new SecondRow();

                            aRowNoRowObjectMap.put(row + 1, aSecondRow);
                        } else if (aSecondRow != null) {
                            aSecondRow.jAccountNameCobmoBox.requestFocus();
                        }
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jtf1.requestFocus();
                    }
                }
            });

        }
    }

    public String oldbal;
    public String oldbaltype;

    public void updatePayAccountBal() {
        double samn = initialPayAccountBal;
        double ramn = 0.0;
        String AccountBalType = currentcbt;
        String bankAccount = "";
        if (a == 1) {

            samn = Double.parseDouble(jTextField5.getText());
            AccountBalType = jLabel8.getText().trim();

        }

        Iterator<String> ledgerNameIterator = ledgerNameGrandTotalMapForUpdate.keySet().iterator();

        while (ledgerNameIterator.hasNext()) {
            String ledgerName = ledgerNameIterator.next();

            double grandTotal = ledgerNameGrandTotalMapForUpdate.get(ledgerName);
            System.out.println("ledger name is " + ledgerName + " " + grandTotal + " " + AccountBalType);
            if (AccountBalType.equals("CR")) {
                samn = samn + grandTotal;
                AccountBalType = "CR";
            } else if (AccountBalType.equals("DR")) {
                samn = samn - grandTotal;
                System.out.println("value of samn 2" + samn);
                if (samn >= 0) {
                    AccountBalType = "DR";
                } else {
                    AccountBalType = "CR";
                }
            }

        }
        //   samn = Math.abs(samn);
        if (jtf1 != null && jtf1.getText().trim().length() > 0) {
            ramn = Double.parseDouble(jtf1.getText());
            if (AccountBalType.equals("DR")) {
                samn = samn + ramn;
                AccountBalType = "DR";
            } else if (AccountBalType.equals("CR")) {
                samn = samn - ramn;
                System.out.println("value of samn 2" + samn);
                if (samn >= 0) {
                    AccountBalType = "CR";
                } else {
                    AccountBalType = "DR";
                }
            }
        }
        Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
        int aRowNo = 0;
        SecondRow aSecondRow = null;
        while (aRowNoItertor.hasNext()) {
            aRowNo = (Integer) aRowNoItertor.next();
            aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
            if (aSecondRow.jTextField != null && aSecondRow.jTextField.getText().trim().length() > 0) {
                ramn = Double.parseDouble(aSecondRow.jTextField.getText());
                if (AccountBalType.equals("DR")) {
                    System.out.println("value of samn there" + samn);
                    samn = Math.abs(samn);
                    samn = samn + ramn;
                    AccountBalType = "DR";
                }
                if (AccountBalType.equals("CR")) {
                    samn = samn - ramn;
                    if (samn >= 0) {
                        AccountBalType = "CR";
                    } else {
                        AccountBalType = "DR";
                    }
                }
            }
        }
        System.out.println("value of samn at last" + samn);
        jTextField5.setText("" + Math.abs(samn));
        jLabel8.setText(" " + AccountBalType.trim());
    }

// For Selected ledger
    String Balance = "";

    public void updateBal(String LedgerName) {
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + LedgerName + "'");
            String aBalTypeCd = "", aCurrentBal = "";
            while (rs.next()) {
                aCurrentBal = rs.getString(1);
                aBalTypeCd = rs.getString(2);
                Balance = rs.getString("curr_bal");
            }

            double result = Double.parseDouble(aCurrentBal);
            double oldValue = 0;
            if (ledgerNameGrandTotalMapForUpdate.containsKey(LedgerName)) {
                oldValue = ledgerNameGrandTotalMapForUpdate.get(LedgerName);

                if (aBalTypeCd.equals("CR")) {
                    result = result - oldValue;
                    System.out.println("cr result is " + result);
                    if (result < 0) {
                        aBalTypeCd = "DR";
                    } else {
                        if (result >= 0) {
                            aBalTypeCd = "CR";
                        }
                    }
                } else if (aBalTypeCd.equals("DR")) {
                    result = result + oldValue;
                    System.out.println("dr result is " + result);
                    aBalTypeCd = "DR";
                }

            }

            Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
            int aRowNo = 0;
            SecondRow aSecondRow = null;

            if (((String) jcb1.getSelectedItem()).equals(LedgerName)) {
                double ramn = Double.parseDouble(jtf1.getText());
                result = Math.abs(result);
                if (aBalTypeCd.equals("DR")) {
                    result = result - ramn;
                    System.out.println("cr result is " + result);
                    if (result < 0) {
                        aBalTypeCd = "CR";
                    } else {
                        if (result >= 0) {
                            aBalTypeCd = "DR";
                        }
                    }
                } else if (aBalTypeCd.equals("CR")) {
                    result = result + ramn;
                    System.out.println("dr result is " + result);
                    aBalTypeCd = "CR";
                }

                jcl17.setText("" + Math.abs(result));
                jcl18.setText(aBalTypeCd);

                System.out.println("The result after update prateek is " + Math.abs(result));
                System.out.println("The balance type after update prateek is " + aBalTypeCd);
            }

            while (aRowNoItertor.hasNext()) {
                aRowNo = (Integer) aRowNoItertor.next();
                aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                System.out.println("aSecondRow.jAccountNameCobmoBox.getSelectedItem()  " + aSecondRow.jAccountNameCobmoBox.getSelectedItem());
                if (aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString().equals(LedgerName)) {
                    int ramn = Integer.parseInt(aSecondRow.jTextField.getText());
                    if (aBalTypeCd.equals("DR")) {
                        result = result - ramn;
                        if (result < 0) {
                            aBalTypeCd = "CR";
                        } else if (result >= 0) {
                            aBalTypeCd = "DR";
                        }
                    } else {
                        if (aBalTypeCd.equals("CR")) {
                            result = result + ramn;

                            aBalTypeCd = "CR";

                        }
                    }
                }
            }
            result = Math.abs(result);
            aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
            while (aRowNoItertor.hasNext()) {
                aRowNo = (Integer) aRowNoItertor.next();
                aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                if (aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString().equals(LedgerName)) {
                    aSecondRow.jcl47.setText("" + Math.abs(result));
                    aSecondRow.jcl48.setText(aBalTypeCd);
                }
            }

            jPanel3.repaint();
            aLedgerNameCurrentBalMap.put(LedgerName, "" + Math.abs(result));
            aLedgerNameCurrentBalMapt.put(LedgerName, aBalTypeCd);
        } catch (Exception aExc) {
            aExc.printStackTrace();
        }
    }

    public void updaterevesreBal(String LedgerName) {
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            Statement stmt1 = conn.createStatement();

            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + LedgerName + "'");
            String aBalTypeCd = "", aCurrentBal = "";
            while (rs.next()) {
                aCurrentBal = rs.getString(1);
                aBalTypeCd = rs.getString(2);
            }

            double result = Double.parseDouble(aCurrentBal);

            {
                double ramn = amount;
                result = Math.abs(result);
                if (aBalTypeCd.equals("CR")) {
                    result = result - ramn;
                    System.out.println("cr result is " + result);
                    if (result < 0) {
                        aBalTypeCd = "DR";
                    } else {
                        if (result >= 0) {
                            aBalTypeCd = "CR";
                        }
                    }
                } else if (aBalTypeCd.equals("DR")) {
                    result = result + ramn;
                    System.out.println("dr result is " + result);
                    aBalTypeCd = "DR";
                }
            }

            result = Math.abs(result);
            stmt1.executeUpdate("update ledger set curr_bal='" + result + "',currbal_type='" + aBalTypeCd + "' where ledger_name='" + LedgerName + "' ");
            System.out.println("update reverse to ledger is" + "update ledger set curr_bal='" + result + "',currbal_type='" + aBalTypeCd + "' where ledger_name='" + LedgerName + "' ");

        } catch (Exception aExc) {
            aExc.printStackTrace();
        }
    }

    public void updatePayReverseAccountBal(String Ledgername) {
        double samn = initialPayAccountBal;
        double ramn = 0.0;
        String AccountBalType = currentcbt;
        String bankAccount = "";

        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement st = conn.createStatement();
            Statement stmt1 = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + Ledgername + "'");
            while (rs.next()) {
                samn = Double.parseDouble(rs.getString(10));
                AccountBalType = rs.getString(11);
            }

            ramn = amount;
            if (AccountBalType.equals("CR")) {
                samn = samn + ramn;
                AccountBalType = "CR";
            } else if (AccountBalType.equals("DR")) {
                samn = samn - ramn;
                System.out.println("value of samn 2" + samn);
                if (samn >= 0) {
                    AccountBalType = "DR";
                } else {
                    AccountBalType = "CR";
                }
            }
            samn = Math.abs(samn);
            stmt1.executeUpdate("update ledger set curr_bal='" + samn + "',currbal_type='" + AccountBalType + "' where ledger_name='" + Ledgername + "'");
            System.out.println("update reverse by ledger" + "update ledger set curr_bal='" + samn + "',currbal_type='" + AccountBalType + "' where ledger_name='" + Ledgername + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class SecondRow {

        int selected = 0;
        public JComboBox jAccountNameCobmoBox;
        public JTextField jTextField;
        public JTextField jTextField1;
        public SecondRow aNewRow = null;
        public boolean isRowAlreadyCreated = false;
        String baltypecd, rcamntv;
        public JLabel jcl47, jcl48, jcl16;

        public SecondRow() {
            selected = ++row;
            jAccountNameCobmoBox = new JComboBox();
            jTextField = new JTextField();
            jTextField1 = new JTextField();
//             jcl16=new JLabel();
//             jPanel3.add(jcl16);
//             jcl16.setVisible(true);
//             jcl16.setBounds(120,10+(row*40),800,25) ;
            jPanel3.add(jAccountNameCobmoBox);
            jPanel3.add(jTextField);
            jPanel3.add(jTextField1);
            jAccountNameCobmoBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{""}));
            jAccountNameCobmoBox.setBounds(25, 10 + (row * 40), 325, 27);
            jTextField.setBounds(485, 10 + (row * 40), 60, 31);
            jTextField1.setBounds(570, 10 + (row * 40), 60, 31);
            jAccountNameCobmoBox.setVisible(true);
            jTextField.setVisible(true);
            jTextField.setEnabled(false);

            jTextField1.setVisible(true);
            jTextField1.setEnabled(false);

            jAccountNameCobmoBox.requestFocus();

            jcl47 = new JLabel();
            jPanel3.add(jcl47);
            jcl47.setVisible(true);
            jcl47.setBounds(380, 10 + (row * 40), 800, 25);
            jcl48 = new JLabel();
            jPanel3.add(jcl48);
            jcl48.setVisible(true);
            jcl48.setBounds(430, 10 + (row * 40), 800, 25);
            try {
                jAccountNameCobmoBox.removeAll();
                connection c = new connection();
                Connection conn = c.cone();
                Statement st = conn.createStatement();
                Statement st1 = conn.createStatement();
                Statement st2 = conn.createStatement();
                ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH')ORDER BY ledger_name ASC");
                while (rs2.next()) {
                    jAccountNameCobmoBox.addItem(rs2.getString(1));
                }
                ResultSet rs = st.executeQuery("select Name from Group_create where Under IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH')");
                while (rs.next()) {
                    String gname = rs.getString(1);
                    ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='" + gname + "' ORDER BY ledger_name ASC ");
                    while (rs1.next()) {
                        jAccountNameCobmoBox.addItem(rs1.getString(1));

                    }
                }
                st.close();
                st1.close();
                st2.close();
                conn.close();
            } catch (Exception ex) {
                System.out.println("Error : " + ex);
            }
            jAccountNameCobmoBox.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {

                    int key = e.getKeyCode();
                    String sel = jAccountNameCobmoBox.getSelectedItem().toString();
                    if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_TAB) {
                        jTextField.setEnabled(true);
                        if (sel.equals("")) {
                            jTextArea1.requestFocus();
                        } else {
                            name[count1] = (String) jAccountNameCobmoBox.getSelectedItem();
                            ++count1;

                            try {
                                connection c = new connection();
                                Connection conn = c.cone();
                                Statement stmt = conn.createStatement();
                                String cbt = null;
                                String str = jAccountNameCobmoBox.getSelectedItem().toString();
                                System.out.println(str);
                                ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
                                while (rs.next()) {
                                    baltypecd = rs.getString(2);
                                    rcamntv = rs.getString(1);
                                    // jcl16.setText("CURRENT BAL : -  ");
                                    jcl47.setText(rcamntv);
                                    jcl48.setText(baltypecd);
                                }
                                if (aLedgerNameCurrentBalMap.containsKey(str)) {
                                    jcl47.setText(aLedgerNameCurrentBalMap.get(str).toString());
                                    jcl48.setText(aLedgerNameCurrentBalMapt.get(str).toString());
                                }
                                stmt.close();
                                conn.close();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            jTextField.requestFocus();

                        }
                    }

                    if (key == KeyEvent.VK_ESCAPE) {
                        jTextField.requestFocus();
                    }
                }
            });
            jAccountNameCobmoBox.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                }
            });
            jTextField.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    double sum = 0;
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_TAB) {
                        bal[count2] = Double.parseDouble(jTextField.getText());
                        currbal[count2] = Double.parseDouble(jcl47.getText());
                        currbaltype[count2] = jcl48.getText();

                        ++count2;
                        updatePayAccountBal();
                        updateBal(jAccountNameCobmoBox.getSelectedItem().toString());
                        jTextField1.setEnabled(true);
                        Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
                        int aRowNo = 0;
                        SecondRow aSecondRow = null;
                        double ramn = 0;
                        if (jtf1.getText() != null && jtf1.getText().trim().length() > 0) {
                            sum = Double.parseDouble(jtf1.getText());
                        }
                        while (aRowNoItertor.hasNext()) {
                            aRowNo = (Integer) aRowNoItertor.next();
                            aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                            if (aSecondRow.jTextField != null && aSecondRow.jTextField.getText().trim().length() > 0) {
                                ramn = Double.parseDouble(aSecondRow.jTextField.getText());
                                sum = sum + ramn;
                            }
                        }
                        jTextField4.setText("" + sum);

                        ++point16;
                        jTextField1.requestFocus();

                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jAccountNameCobmoBox.requestFocus();
                    }
                }
            });

            jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    double sum = 0;
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_TAB) {
                        checque[count3] = jTextField1.getText();
                        ++count3;

                        if (!isRowAlreadyCreated) {
                            aNewRow = new SecondRow();
                            aRowNoRowObjectMap.put(row + 1, aNewRow);
                            isRowAlreadyCreated = true;
                        } else if (aNewRow != null) {
                            aNewRow.jAccountNameCobmoBox.requestFocus();
                        }
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jTextField.requestFocus();
                    }
                }
            });

        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jDialog4 = new javax.swing.JDialog();
        jLabel14 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        choice1 = new java.awt.Choice();
        jTextField5 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        jDialog4.setMinimumSize(new java.awt.Dimension(285, 150));

        jLabel14.setText("No Of Copies  - ");

        jTextField7.setText("1");
        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });
        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
        });

        jButton5.setText("Print");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jButton5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton5KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jDialog4Layout = new javax.swing.GroupLayout(jDialog4.getContentPane());
        jDialog4.getContentPane().setLayout(jDialog4Layout);
        jDialog4Layout.setHorizontalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog4Layout.createSequentialGroup()
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog4Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jLabel14)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog4Layout.createSequentialGroup()
                        .addGap(113, 113, 113)
                        .addComponent(jButton5)))
                .addContainerGap(75, Short.MAX_VALUE))
        );
        jDialog4Layout.setVerticalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addGap(34, 34, 34))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : BANK RECIEPT");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel1.setText("SNO. :");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel2.setText("DATE :");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel3.setText("AMOUNT ");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel4.setText("ACCOUNT");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel5.setText("NARRATION :");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel6.setText("GRAND TOTAL :");

        jTextField1.setEditable(false);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(2);
        jTextArea1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextArea1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextArea1);

        jTextField4.setEditable(false);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel7.setText("PARTICULARS");

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel10.setText("CURR. BAL. : ");
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        choice1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                choice1ItemStateChanged(evt);
            }
        });
        choice1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                choice1FocusLost(evt);
            }
        });
        choice1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                choice1KeyPressed(evt);
            }
        });

        jTextField5.setEditable(false);
        jTextField5.setBackground(new java.awt.Color(204, 204, 255));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 707, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 471, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(jPanel3);

        jButton3.setText("UPDATE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel9.setText("CHEQUE NO.");

        jDateChooser1.setDateFormatString("dd-MM-yyy");
        jDateChooser1.setMaxSelectableDate(new java.util.Date(1743359400000L));
        jDateChooser1.setMinSelectableDate(new java.util.Date(1459449000000L));
        jDateChooser1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jDateChooser1FocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 245, Short.MAX_VALUE)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel10)
                            .addGap(12, 12, 12)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6)
                            .addGap(31, 31, 31)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(9, 9, 9))
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 671, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(26, 26, 26)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(304, 304, 304)
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(35, 35, 35)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(103, 103, 103))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jLabel10)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel3)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton3))
                        .addComponent(jButton2)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setSize(new java.awt.Dimension(699, 528));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void otherComponents() {

    }
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        //   jPopupMenu1.setVisible(false);        
    }//GEN-LAST:event_formWindowClosing

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed

    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated

    }//GEN-LAST:event_formWindowActivated

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        setVisible(false);

    }//GEN-LAST:event_jButton2ActionPerformed
    public boolean issave = false;

    public void save() {
        int i1 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i1 == 0 && issave == false) {
            issave = true;
            PreparedStatement ps = null;
//            int i=0;
            String s_no = (jTextField1.getText());
            String da_te = formatter.format(jDateChooser1.getDate());
            String by_ledger = choice1.getSelectedItem().toString();
            String narra_tion = jTextArea1.getText();
            String grand_total = jTextField4.getText();

            try {
                connection c = new connection();
                Connection conneee = c.cone();
                Statement st1 = conneee.createStatement();
                if (jtf1.getText() != null && jtf1.getText().trim().length() > 0) {
                    st1.executeUpdate("insert into bankreceipt values('" + s_no + "','" + da_te + "','" + by_ledger + "','" + jcb1.getSelectedItem().toString() + "','" + jtf1.getText() + "','" + narra_tion + "','" + grand_total + "'," + (1) + ",'0','" + jtf2.getText() + "') ");
                }

                Iterator it1 = aRowNoRowObjectMap.keySet().iterator();
                int i = 1;
                while (it1.hasNext()) {
                    int aRowNo = (Integer) it1.next();
                    SecondRow aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);

                    if (aSecondRow.jTextField.getText() != null && aSecondRow.jTextField.getText().trim().length() > 0) {
                        st1.executeUpdate("insert into bankreceipt values('" + s_no + "','" + da_te + "','" + by_ledger + "','" + aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString() + "','" + aSecondRow.jTextField.getText() + "','" + narra_tion + "','" + grand_total + "'," + (++i) + ",'0','" + aSecondRow.jTextField1.getText() + "') ");
                    }
                    //st1.executeUpdate("insert into payment_ho values('"+s_no+"','"+da_te+"','"+by_ledger+"','"+name[i]+"','"+bal[i]+"','"+narra_tion+"','"+grand_total+"',"+(i+1)+") ");
                }

//               for(int i = 0 ; i < count2 ; i ++ )
//		{
//             st1.executeUpdate("insert into bankreceipt values('"+s_no+"','"+da_te+"','"+by_ledger+"','"+name[i]+"','"+bal[i]+"','"+narra_tion+"','"+grand_total+"',"+(i+1)+",'0','"+checque[i]+"') ");
//        //   st1.executeUpdate("insert into payment_ho values('"+s_no+"','"+da_te+"','"+by_ledger+"','"+name[i]+"','"+bal[i]+"','"+narra_tion+"','"+grand_total+"',"+(i+1)+") ");
//                }
                Statement st2 = conneee.createStatement();
                Iterator it = aLedgerNameCurrentBalMap.keySet().iterator();
                while (it.hasNext()) {
                    String LedgerName = (String) (it.next());
                    String currbal = (String) aLedgerNameCurrentBalMap.get(LedgerName);
                    String baltype = (String) aLedgerNameCurrentBalMapt.get(LedgerName);
                    st1.executeUpdate("update ledger set curr_bal='" + currbal + "',currbal_type='" + baltype + "'  where ledger_name = '" + LedgerName + "'");
                }
                st1.executeUpdate("update ledger set curr_bal='" + jTextField5.getText().toString() + "',currbal_type='" + jLabel8.getText().trim() + "' where ledger_name = '" + choice1.getSelectedItem().toString() + "'");

                System.out.println("query for save is " + "update ledger set curr_bal='" + jTextField5.getText().toString() + "',currbal_type='" + jLabel8.getText().trim() + "' where ledger_name = '" + choice1.getSelectedItem().toString() + "'");

                JOptionPane.showMessageDialog(null, "Inserted Sucessfully.");

                conneee.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
            if (i2 == 0) {
                jDialog4.setVisible(true);
                jTextField7.selectAll();
//            PrinterJob job = PrinterJob.getPrinterJob();
//            PageFormat pf = new PageFormat();
//            Paper paper = new Paper();
//            paper.setSize(5.8d * 72, 8.3d * 72);
//            double margin = 40;
//            paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
//            pf.setPaper(paper);
//            job.setPrintable(this, pf);
//            PrintService service = PrintServiceLookup.lookupDefaultPrintService();
//
//            try {
//                job.setPrintService(service);
//                job.setCopies(1);
//                job.print();
//            } catch (PrinterException ex) {
//                /* The job did not successfully complete */
//                ex.printStackTrace();
//            }
            }

//            dispose();
        } else if (issave == true) {
            JOptionPane.showMessageDialog(rootPane, "Entry is already saved");
        }
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        save();
//        bankreciept brc = new bankreciept();
//        brc.setVisible(true);
//        brc.jDateChooser1.setDate(jDateChooser1.getDate());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void choice1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_choice1ItemStateChanged
        String str = (String) evt.getItem();
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            String cbt = null;
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
            while (rs.next()) {
                initialPayAccountBal = Double.parseDouble(rs.getString(1));
                currentcbt = rs.getString(2);
            }
            jTextField5.setText(initialPayAccountBal + "");
            jLabel8.setText(currentcbt);
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_choice1ItemStateChanged

    private void choice1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_choice1FocusLost

    }//GEN-LAST:event_choice1FocusLost

    private void jTextArea1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea1KeyPressed

        int i = 0;
        double sum = 0;
        int key = evt.getKeyCode();
        if (key == KeyEvent.VK_ENTER) {
//                   while(i<count2)
//             {
//                 sum=bal[i]+sum;
//                 i++;
//             }
//           jTextField4.setText(""+sum);
            jButton1.requestFocus();
        }
    }//GEN-LAST:event_jTextArea1KeyPressed
    public double amount = 0;
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int i1 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to update");
        if (i1 == 0) {
            String toledger;
            String byledger;
            try {
                connection c = new connection();
                Connection conneee = c.cone();
                Statement st1 = conneee.createStatement();
                Statement st2 = conneee.createStatement();
                ResultSet rs = st1.executeQuery("select * from bankreceipt where s_no='" + jTextField1.getText() + "'");

                while (rs.next()) {
                    toledger = rs.getString("to_ledger");
                    byledger = rs.getString("by_ledger");
                    amount = Double.parseDouble(rs.getString("amo_unt"));
                    updaterevesreBal(toledger);
                    updatePayReverseAccountBal(byledger);
                }

                st2.executeUpdate("delete from bankreceipt where s_no='" + jTextField1.getText() + "'");

                updateBal(jcb1.getSelectedItem() + "".trim());
                updatePayAccountBal();
                save();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void choice1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_choice1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            jcb1.requestFocus();
        }
    }//GEN-LAST:event_choice1KeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jDateChooser1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            save();
//            bankreciept brc = new bankreciept();
//            brc.setVisible(true);
//            brc.jDateChooser1.setDate(jDateChooser1.getDate());
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel10MouseClicked

    private void jDateChooser1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jDateChooser1FocusLost

    }//GEN-LAST:event_jDateChooser1FocusLost

    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed

    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton5.requestFocus();
        } else if (key == evt.VK_TAB) {
            jButton5.requestFocus();
        }
    }//GEN-LAST:event_jTextField7KeyPressed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        no_of_copies = Integer.parseInt(jTextField7.getText());
        System.out.println("No of Copies " + no_of_copies);
        jDialog4.setVisible(false);
        PrinterJob job = PrinterJob.getPrinterJob();
        PageFormat pf = new PageFormat();
        Paper paper = new Paper();
        paper.setSize(5.8d * 72, 8.3d * 72);
        double margin = 40;
        paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
        pf.setPaper(paper);
        job.setPrintable(this, pf);
        PrintService service = PrintServiceLookup.lookupDefaultPrintService();

        try {
            job.setPrintService(service);
            job.setCopies(no_of_copies);
            job.print();
        } catch (PrinterException ex) {
            /* The job did not successfully complete */
            ex.printStackTrace();
        }

        dispose();

    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton5KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            no_of_copies = Integer.parseInt(jTextField7.getText());
            System.out.println("No of Copies " + no_of_copies);
            jDialog4.setVisible(false);
            PrinterJob job = PrinterJob.getPrinterJob();
            PageFormat pf = new PageFormat();
            Paper paper = new Paper();
            paper.setSize(5.8d * 72, 8.3d * 72);
            double margin = 40;
            paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            PrintService service = PrintServiceLookup.lookupDefaultPrintService();

            try {
                job.setPrintService(service);
                job.setCopies(no_of_copies);
                job.print();
            } catch (PrinterException ex) {
                /* The job did not successfully complete */
                ex.printStackTrace();
            }
            dispose();
        }
    }//GEN-LAST:event_jButton5KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    @Override
    public int print(Graphics g, PageFormat pf, int page) throws PrinterException {
        if (page > 0) {
            return NO_SUCH_PAGE;
        } else {

            Graphics2D g2d = (Graphics2D) g;
            FontMetrics fontMetrics = g2d.getFontMetrics();

            Font font = new Font(" Bill NO - SL/1718/1", Font.BOLD, 12);
            g2d.setFont(font);
            g2d.drawString(" Khabiya Cloth Store ", 150, 50);

            Font font2 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 10);
            g2d.setFont(font2);

            g2d.drawString(" 37, New Road ", 150, 60);
            g2d.drawString(" Ratlam - -457001(M.P) ", 150, 70);
            g2d.drawString(" Phone No.- 07412-492939", 150, 80);

// Horizontal Line            
            g2d.drawLine(0, 85, 420, 85);

// Left Side box
            g2d.drawString("Receipt No  : ", 50, 100);
            g2d.drawString("Receipt Date : ", 50, 120);
            g2d.drawString("GSTIN : 23AMDPK9264H1ZZ", 50, 140);

// Virtical Line
            g2d.drawLine(185, 85, 185, 170);

// Right Side box
            g2d.drawString("Name : ", 190, 100);
            g2d.drawString("Address : ", 190, 115);
            g2d.drawString("City : ", 190, 153);
            g2d.drawString("Mobile No : ", 190, 165);

// Horizontal Line            
            g2d.drawLine(0, 170, 420, 170);
            Font font3 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 8);
            g2d.setFont(font3);

//Table Header View
//            g2d.drawString("S No", 50, 150);
            g2d.drawString("Bank Receipt", 170, 182);
            g2d.drawLine(0, 190, 420, 190);// Horizontal Line

            g2d.drawString("Balance -- ", 140, 210);
            g2d.drawString("Receipt Amount -- ", 140, 230);
            g2d.drawString("Remaining Balance -- ", 140, 250);

            g2d.drawLine(0, 460, 420, 460);// Horizontal Line
            g2d.drawLine(240, 240, 290, 240);// Horizontal Line
            g2d.drawLine(240, 256, 290, 256);// Horizontal Line
            g2d.drawLine(240, 260, 290, 260);// Horizontal Line

// For Getting Data
            try {
                String party_name = "";
                String paid = "";
                String cheque_no = "";
                String narration = "";
                String remaining_bal = "";
                String address1 = "";
                String address2 = "";
                String address3 = "";
                String address4 = "";
                String mobile_no = "";
                String city = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from bankreceipt where s_no = '" + jTextField1.getText() + "'");
                while (rs.next()) {
                    party_name = rs.getString("to_ledger");
                    paid = rs.getString("amo_unt");
                    cheque_no = rs.getString("cheque");
                    narration = rs.getString("narra_tion");
                }

                ResultSet rs1 = st.executeQuery("Select * from ledger where ledger_name = '" + party_name + "'");
                while (rs1.next()) {
                    remaining_bal = rs1.getString("curr_bal");
                }

                ResultSet rs2 = st.executeQuery("Select * from party_sales where party_name = '" + party_name + "'");
                while (rs2.next()) {
                    address1 = rs2.getString("address1");
                    address2 = rs2.getString("address2");
                    address3 = rs2.getString("address3");
                    address4 = rs2.getString("address4");
                    mobile_no = rs2.getString("mobile_no");
                    city = rs2.getString("city");
                }

                g2d.drawString(df.format(Double.parseDouble(Balance)), 300 - fontMetrics.stringWidth(df.format(Double.parseDouble(Balance))), 210);
                g2d.drawString(df.format(Double.parseDouble(paid)), 300 - fontMetrics.stringWidth(df.format(Double.parseDouble(paid))), 230);
                g2d.drawString(df.format(Double.parseDouble(remaining_bal)), 300 - fontMetrics.stringWidth(df.format(Double.parseDouble(remaining_bal))), 250);

                g2d.drawString("Received Rs. " + df.format(Double.parseDouble(paid)) + " Against our Bill No. Full/Party by Cheque No. " + cheque_no + "", 60, 300);
                // naration print
                //      g2d.drawString(narration, 60, 315);

// For upper right side box   
                g2d.drawString(party_name, 225, 100);
                if(address1 != null){
                g2d.drawString(address1, 233, 115);
                }
                if(address2 != null){
                g2d.drawString(address2, 233, 125);
                }
                if(address3 != null){
                g2d.drawString(address3, 233, 135);
                }
                if(address4 != null){
                g2d.drawString(address4, 233, 145);
                }
//                if (address.length() >= 30) {
//                    String addr = address.substring(0, 30);
//                    g2d.drawString(addr, 233, 115);
//                } else if (address.length() <= 30) {
//                    g2d.drawString(address, 233, 115);
//                }
//
//                if (address.length() > 30 && address.length() >= 60) {
//                    String addr1 = address.substring(30, 60);
//                    g2d.drawString(addr1, 233, 125);
//                } else if (address.length() > 30 && address.length() < 60) {
//                    String addr1 = address.substring(30);
//                    g2d.drawString(addr1, 233, 125);
//                }
//
//                if (address.length() > 60 && address.length() >= 90) {
//                    String addr2 = address.substring(60, 90);
//                    g2d.drawString(addr2, 233, 135);
//                } else if (address.length() > 60 && address.length() < 90) {
//                    String addr2 = address.substring(60);
//                    g2d.drawString(addr2, 233, 135);
//                }
//
//                if (address.length() > 90 && address.length() >= 120) {
//                    String addr3 = address.substring(90, 120);
//                    g2d.drawString(addr3, 233, 145);
//                } else if (address.length() > 90 && address.length() < 120) {
//                    String addr3 = address.substring(90);
//                    g2d.drawString(addr3, 233, 145);
//                }

                g2d.drawString(city, 240, 153);
                if(mobile_no != null){
                g2d.drawString(mobile_no, 242, 165);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
// For Authority Signatury
            g2d.drawString("For Khabiya :", 320, 555);
// For upper left side box   
            g2d.drawString(jTextField1.getText(), 115, 100);
            SimpleDateFormat mdyFormat = new SimpleDateFormat("dd-MM-yyyy");
            String date = mdyFormat.format(jDateChooser1.getDate());
            g2d.drawString(date, 115, 120);

            System.out.println("printing");
            return 0;
        }
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new bankreciept().setVisible(true);
            }
        });
    }

    //  public javax.swing.JTextField jTextField;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public java.awt.Choice choice1;
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JDialog jDialog4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    public javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JTextArea jTextArea1;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField7;
    // End of variables declaration//GEN-END:variables

}
