package accounting.transaction;

import connection.connection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

public class cashpayment extends javax.swing.JFrame {

    public int point, point1, point2, point3, point4, point5, point6, point7, point8, point9, point10, point11, point12, point13, point14, point15, point16, point17 = 0;
    public String toby[] = new String[50];
    public int selected;
    public String baltypecd;
    public int row = -1;
    public String currentcbt;
    public String rcamntv;
    public List RowList;
    public newRow aFirstRow = null;
    public JComboBox jcb1, jcb2, jcb3, jcb4, jcb5, jcb6, jcb7, jcb8, jcb9, jcb10, jcb11, jcb12, jcb13, jcb14, jcb15, jcb16, jcb17, jcb18, jcb19, jcb20,
            jcb21, jcb22, jcb23, jcb24, jcb25, jcb26, jcb27, jcb28, jcb29, jcb30, jAccountNameCobmoBox, jcb32, jcb33, jcb34, jcb35, jcb36, jcb37, jcb38, jcb49, jcb40;
    public JTextField jtf1, jtf2, jtf3, jtf4, jtf5, jtf6, jtf7, jtf8, jtf9, jtf10, jtf11, jtf12, jtf13, jtf14, jtf15, jtf16, jtf17, jtf18, jtf19, jtf20,
            jtf21, jtf22, jtf23, jtf24, jtf25, jtf26, jtf27, jtf28, jtf29, jtf30, jTextField, jtf32, jtf33, jtf34, jtf35, jtf36, jtf37, jtf38, jtf39, jtf40;
    public JLabel jcl1, jcl2, jcl3, jcl4, jcl5, jcl6, jcl7, jcl8, jcl9, jcl10, jcl11, jcl12, jcl13, jcl14, jcl15, jcl16, jcl17, jcl18, jcl19, jcl20, jcl21, jcl22, jcl23, jcl24, jcl25, jcl26, jcl27, jcl28, jcl29, jcl30, jcl31, jcl32, jcl33, jcl34, jcl35, jcl36, jcl37, jcl38, jcl39, jcl40, jcl41, jcl42, jcl43, jcl44, jcl45, jcl46, jcl47, jcl48, jcl49;
    public JPanel jpanel3;
    public String name[] = new String[100];
    public double bal[] = new double[100];
    public double currbal[] = new double[100];
    public String currbaltype[] = new String[100];
    public int count1 = 0;
    public int count2 = 0;
    public int count = 0;
    public int s_no[] = new int[100];
    public String da_te[] = new String[100];
    public String by_ledger[] = new String[100];
    public String narra_tion[] = new String[100];
    public String grand_total[] = new String[100];
    public JComboBox jc1;
    public JTextField jt;
    public JLabel jl;
    public Map aRowNoRowObjectMap = new LinkedHashMap(), aLedgerNameCurrentBalMap = new LinkedHashMap(), aLedgerNameCurrentBalMapt = new LinkedHashMap();
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    public double initialPayAccountBal = 0.0;

    private Map<String, Double> ledgerNameGrandTotalMapForUpdate = new LinkedHashMap<String, Double>();
    Connection newcon = null;
    //first frame element
    JTable injTable;
    JDialog inFrame;
    JScrollPane injScrollPane1;
    JButton inButton;
    Double grossamount = 0.0;

    List totallist = new ArrayList();
    List llist = new ArrayList();
    List alist = new ArrayList();
    List secondrowpartyname = new ArrayList();
    List tabledata = new ArrayList();
    List amtbpaid = new ArrayList();
    List rfno = new ArrayList();
    List rfno2 = new ArrayList();

    DefaultTableModel model;
    Object check = null, billrefrence = null, objectamount = null, amounttobepaid = null, remaining = null;

    int rowCount = 0;
    int rowCountdata = 0;

    String jcb1Name;
    String sno, dddate, acname, type, party_name1;

    Map<Object, Object> mymap = new LinkedHashMap();
    Map<Object, Object> mymap1 = new LinkedHashMap();
    Map<Object, Object> mymap2 = new LinkedHashMap();

    //new frame start
    public void newFrame() {
        String party_name = party_name = jcb1.getSelectedItem().toString();
        inFrame = new JDialog();
        injScrollPane1 = new javax.swing.JScrollPane();
        injTable = new javax.swing.JTable();
        inButton = new javax.swing.JButton("");
        inButton.setFont(new java.awt.Font("Segoe UI", 1, 18));
        Connection newcon = null;
        try {
            newcon = new connection().cone();
        } catch (SQLException ex) {
            Logger.getLogger(cashpayment.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.print("table creation... , ");
        injTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{}, new String[]{"Purchess Bill Number", "Amount", "Amount To Be Paid", "Remaining Amount"}) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, true, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        injScrollPane1.setViewportView(injTable);

        inFrame.getContentPane().add(injScrollPane1, java.awt.BorderLayout.CENTER);

        inButton.setText("Go...!");
        inFrame.getContentPane().add(inButton, java.awt.BorderLayout.PAGE_END);

        inFrame.setSize(600, 400);
        inFrame.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        inFrame.setVisible(true);
        inFrame.pack();
        System.out.print("frame creation and setting up... , ");
        model = (DefaultTableModel) injTable.getModel();
        try {
            if (llist.isEmpty()) {
                System.out.print("if(llist.isEmpty()) , ");
                Statement newst = newcon.createStatement();
                System.out.print("Statement  , ");
                ResultSet newrs = newst.executeQuery("SELECT * FROM againstpayment WHERE party_name ='" + party_name + "' and type ='purchase'");
                System.out.print("ResultSet , ");
                while (newrs.next()) {
                    llist.add(newrs.getString("bill_no"));
                    alist.add(newrs.getString("amount"));
                }

                int a = 0;
                a = llist.size();

                int rowtable = injTable.getRowCount();

                System.out.print("before for loop , ");
                for (int i = 0; i < a; i++) {
                    System.out.print("in for loop , ");
                    double netAmount = 0;
                    double resultAmount = 0;
                    Statement st2 = newcon.createStatement();

                    ResultSet rsamount = st2.executeQuery("select sum(amount) from againstpayment where Ref_Bill_no = '" + llist.get(i) + "" + "'  ");

                    while (rsamount.next()) {
                        resultAmount = (rsamount.getInt(1));

                    }
                    netAmount = Double.parseDouble(alist.get(i).toString()) - resultAmount;
                    System.out.println("net amount : " + netAmount);
                    if (netAmount > 0) {

                        Object object[] = {llist.get(i) + "", netAmount, "", ""};
                        System.out.print(" llist.get(i) : " + llist.get(i) + ", ");
                        model.addRow(object);
                    } else if (netAmount == 0) {
                        String ss = "All The Bills To This Party Is Already Paid\n Do You Want To Treat This Payment As Advanced...!!!";
                        //inFrame.dispose();
                        int yesorno = JOptionPane.showConfirmDialog(rootPane, ss);
                        if (yesorno == 0) {
                            sno = jTextField1.getText().trim();
                            dddate = formatter.format(jDateChooser1.getDate()).trim();
                            acname = choice1.getSelectedItem().trim();
                            party_name1 = jcb1.getSelectedItem().toString();
                            type = "backPayment";
                            billrefrence = "advanced";
                            totallist.clear();
                            totallist.add(jtf1.getText().trim());
                            Statement st9 = newcon.createStatement();
                            rowCountdata = st9.executeUpdate("Insert INTO againstpayment(Bill_no, Ref_Bill_no, date, party_name, Payer_name, Amount,type) VALUES ('" + sno + "','" + billrefrence + "','" + dddate + "','" + party_name1 + "','" + acname + "','" + totallist.get(0).toString() + "','" + type + "')");
                            inFrame.dispose();
                        } else {
                            inFrame.dispose();
                            SecondRow aSecondRow = null;
                            if (point1 == 1) {
                                aSecondRow = new SecondRow();

                                aRowNoRowObjectMap.put(row + 1, aSecondRow);

                            } else if (aSecondRow != null) {
                                aSecondRow.jAccountNameCobmoBox.requestFocus();
                            }
                        }
                    }

                }
                injTable.getModel().setValueAt(jtf1.getText().trim(), 0, 2);
                for (Map.Entry<Object, Object> entry : mymap.entrySet()) {

                    injTable.getModel().setValueAt(entry.getValue(), Integer.parseInt(entry.getKey().toString()), 2);

                }
                for (Map.Entry<Object, Object> entry : mymap2.entrySet()) {

                    injTable.getModel().setValueAt(entry.getValue(), Integer.parseInt(entry.getKey().toString()), 3);

                }

            }

            //for remove the element of map and list  
            mymap.clear();
            mymap2.clear();
            llist.clear();
            rfno.clear();
        } catch (SQLException newEx) {
            newEx.printStackTrace();
        }

        CellEditorListener changeNotification = new CellEditorListener() {
            @Override
            public void editingCanceled(ChangeEvent e) {

                System.out.println("cencel");
            }

            @Override
            public void editingStopped(ChangeEvent e) {

                int row = injTable.getSelectedRow();
                int column = injTable.getSelectedColumn();

                String first = injTable.getModel().getValueAt(row, 1).toString();
                String second = injTable.getModel().getValueAt(row, 2).toString();
                if (second.isEmpty()) {
                    second = "0.0";
                }

                double set = Double.parseDouble(first) - Double.parseDouble(second);
                System.out.println("first" + first + "second" + second + "set" + set);

                injTable.getModel().setValueAt(set, row, 3);
                injTable.repaint();
            }
        };
        injTable.getDefaultEditor(Object.class).addCellEditorListener(changeNotification);

        inButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                sno = jTextField1.getText().trim();
                dddate = formatter.format(jDateChooser1.getDate()).trim();
                acname = choice1.getSelectedItem().trim();
                type = "Payment";
                party_name1 = jcb1.getSelectedItem().toString();
                rowCount = injTable.getRowCount();
                int totalam = 0;
                for (int i = 0; i < rowCount; i++) {
                    System.out.println("value of i " + i);
                    // boolean b = (boolean) injTable.getValueAt(i, 0);
                    //  System.out.println("booleaan value" + b);
                    String ss = injTable.getValueAt(i, 2).toString();
                    System.out.println("!ss.trim().isEmpty() : " + !ss.trim().isEmpty());
                    System.out.println("ss is : " + ss);
                    if (!ss.trim().isEmpty() && !ss.equals("0")) {

                        // check = (Object) injTable.getModel().getValueAt(i, 0);
                        billrefrence = (Object) injTable.getModel().getValueAt(i, 0);
                        objectamount = (Object) injTable.getModel().getValueAt(i, 1);
                        amounttobepaid = (Object) injTable.getModel().getValueAt(i, 2);
                        remaining = (Object) injTable.getModel().getValueAt(i, 3);
                        amtbpaid.add(amounttobepaid);
                        mymap.put(i, amounttobepaid);
                        mymap2.put(i, remaining);
                        rfno.add(billrefrence);
                        totalam += Integer.parseInt(amounttobepaid.toString());

                        System.out.print("if amount : " + amounttobepaid + ", ");
                        System.out.print("check" + check + "billrefrence" + billrefrence + "amount" + objectamount + "amounttobepaid" + amounttobepaid);
                    }

                }
                int nettotalam = Integer.parseInt(jtf1.getText().trim());
                System.out.println("nettotalam :" + nettotalam + "totalam" + totalam);
                if (nettotalam < totalam) {
                    JOptionPane.showMessageDialog(inFrame, "Invaid Entry...");
                    amtbpaid.clear();
                    mymap.clear();
                    mymap2.clear();
                    rfno.clear();
                    inFrame.dispose();

                    newFrame();
                } else {
                    jcb1Name = jcb1.getSelectedItem().toString().trim();
                    inFrame.dispose();
                    SecondRow aSecondRow = null;
                    if (point1 == 1) {
                        aSecondRow = new SecondRow();

                        aRowNoRowObjectMap.put(row + 1, aSecondRow);

                    } else if (aSecondRow != null) {
                        aSecondRow.jAccountNameCobmoBox.requestFocus();
                    }
                }
            }
        });

    }

    //new frame end 
    private static cashpayment obj = null;
    public cashpayment() {
        initComponents();
        this.setLocationRelativeTo(null);
        jButton3.setVisible(false);
        try {
            jDateChooser1.setDate(formatter.parse(dateNow));
        } catch (ParseException ex) {
            Logger.getLogger(journ.class.getName()).log(Level.SEVERE, null, ex);
        }
        otherComponents();
        //int a = 1;

        try {
            java.util.Date dateafter = formatter.parse("31-03-20" + getyear + "");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            String payment = "PAY/";
            String slash = "/";
            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String finalconcat = (payment.concat(yearwise));
            int a = 0, b, d = 0;
            int i = 0;
            ResultSet rs = st.executeQuery("SELECT  (sno) FROM payment where branchid='0'");
            while (rs.next()) {
                if (i == 0) {
                    d = rs.getString(1).lastIndexOf("/");
                    i++;
                }
                b = Integer.parseInt(rs.getString(1).substring(d + 1));
                if (a < b) {
                    a = b;
                }
            }
            String s = finalconcat.concat(Integer.toString(a + 1));
            jTextField1.setText("" + s);
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }

        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement st = conn.createStatement();
            Statement stl = conn.createStatement();
            Statement stg = conn.createStatement();
            ResultSet rs = st.executeQuery("select ledger_name from ledger where ledger_name='Cash' or ledger_name='Cash BSNL' or ledger_name='Cash P'");

            while (rs.next()) {
                choice1.addItem(rs.getString(1));
            }

            ResultSet rsl = stl.executeQuery("select Name from Group_create where Under IN('CASH IN HAND')");
            while (rsl.next()) {
                String gname = rsl.getString(1);
                ResultSet rsg = stg.executeQuery("select ledger_name from ledger where groups='" + gname + "' ");
                while (rsg.next()) {
                    choice1.addItem(rsg.getString(1));
                }
            }
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String str = (String) choice1.getSelectedItem();
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            String cbt = null;
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
            while (rs.next()) {
                initialPayAccountBal = Double.parseDouble(rs.getString(1));
                currentcbt = rs.getString(2);
                jTextField5.setText(initialPayAccountBal + "");
                jLabel8.setText(currentcbt);
            }
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        jcb1 = new JComboBox();
        jtf1 = new JTextField();
        aFirstRow = new newRow();
    }
    
   public static cashpayment getObj(){
     if(obj == null){
      obj = new cashpayment();
     }
     return obj;
    }


    public Map<String, Double> getLedgerNameGrandTotalMapForUpdate() {
        return ledgerNameGrandTotalMapForUpdate;
    }

    public void setLedgerNameGrandTotalMapForUpdate(Map<String, Double> ledgerNameGrandTotalMapForUpdate) {
        this.ledgerNameGrandTotalMapForUpdate = ledgerNameGrandTotalMapForUpdate;
    }

    public SecondRow createAnotherRow() {
        SecondRow aSecondRow = new SecondRow();
        aRowNoRowObjectMap.put(row + 1, aSecondRow);
        return aSecondRow;
    }

    public class newRow {

        int selected;
        public SecondRow aSecondRow = null;

        public newRow() {
            selected = ++row;
            newRow anewRow = null;
            jcb1 = new JComboBox();
            jcb2 = new JComboBox();
            jtf1 = new JTextField();
            jtf2 = new JTextField();
            //   jcl1 = new JLabel();
            //  jPanel3.add(jcl1);
//            jcl1.setVisible(true);
//            jcl1.setBounds(120, 10 + (row * 40), 800, 25);
            jcl17 = new JLabel();
            jPanel3.add(jcl17);
            jcl17.setVisible(true);
            jcl17.setBounds(380, 10 + (row * 40), 800, 25);
            jcl18 = new JLabel();
            jPanel3.add(jcl18);
            jcl18.setVisible(true);
            jcl18.setBounds(430, 10 + (row * 40), 800, 25);
            jPanel3.add(jcb1);
            jPanel3.add(jtf1);
            jcb1.setModel(new javax.swing.DefaultComboBoxModel(new String[]{""}));
            jcb1.setBounds(25, 10 + (row * 40), 325, 27);
            jtf1.setBounds(485, 10 + (row * 40), 100, 27);
            jcb1.setVisible(true);
            jtf1.setVisible(true);
            jtf1.setEnabled(false);
            jcb1.requestFocus();
            try {
                jcb1.removeAll();
                connection c = new connection();
                Connection conn = c.cone();
                Statement st = conn.createStatement();
                Statement st1 = conn.createStatement();
                Statement st2 = conn.createStatement();

                ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH') order by ledger_name ASC");
                while (rs2.next()) {
                    jcb1.addItem(rs2.getString(1));
                }

                ResultSet rs = st.executeQuery("select Name from Group_create where Under IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH')");
                while (rs.next()) {
                    String gname = rs.getString(1);
                    ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='" + gname + "' order by ledger_name ASC ");
                    while (rs1.next()) {
                        jcb1.addItem(rs1.getString(1));
                    }
                }
                st.close();
                st1.close();
                st2.close();
                conn.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            jcb1.addKeyListener(new java.awt.event.KeyAdapter() {

                public void keyPressed(java.awt.event.KeyEvent e) {
                    jtf1.setEnabled(true);
                    int key = e.getKeyCode();
                    String sel = jcb1.getSelectedItem().toString();
                    if (key == KeyEvent.VK_ENTER) {
                        name[count1] = (String) jcb1.getSelectedItem();
                        ++count1;
                        try {
                            connection c = new connection();
                            Connection conn = c.cone();
                            Statement stmt = conn.createStatement();
                            String cbt = null;
                            String str = jcb1.getSelectedItem().toString();
                            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
                            while (rs.next()) {
                                baltypecd = rs.getString(2);
                                rcamntv = rs.getString(1);
                                //       jcl1.setText("CURRENT BAL : -  ");
                                jcl17.setText(rcamntv);
                                jcl18.setText(baltypecd);

                            }
                            if (aLedgerNameCurrentBalMap.containsKey(str)) {
                                jcl7.setText(aLedgerNameCurrentBalMap.get(str).toString());
                                jcl8.setText(aLedgerNameCurrentBalMapt.get(str).toString());
                            }
                            stmt.close();
                            conn.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        jtf1.requestFocus();
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jDateChooser1.requestFocus();
                    }
                }
            });

            jcb1.addFocusListener(new FocusAdapter() {

                @Override
                public void focusLost(FocusEvent e) {
                }
            });

            jtf1.addKeyListener(new java.awt.event.KeyAdapter() {

                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER) {

                        bal[count2] = Double.parseDouble(jtf1.getText());

                        currbal[count2] = Double.parseDouble(jcl17.getText());
                        currbaltype[count2] = jcl18.getText();
                        jTextField4.setText("" + bal[count2]);
                        ++count2;

                        Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
                        /*  int aRowNo = 0;
        SecondRow aSecondRow = null;
        double ramn = 0.0,sum=0;
        if(jtf1.getText() != null && jtf1.getText().trim().length() > 0)
            sum = Double.parseDouble(jtf1.getText());
        while (aRowNoItertor.hasNext()) {
            aRowNo = (Integer) aRowNoItertor.next();
            aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
            if (aSecondRow.jTextField != null && aSecondRow.jTextField.getText().trim().length() > 0) {
                ramn = Double.parseDouble(aSecondRow.jTextField.getText());
                sum = sum + ramn;
            }
        }
        jTextField4.setText(""+sum);*/
                        ++point1;
                        updateBal(jcb1.getSelectedItem().toString());
                        updatePayAccountBal();

                        try {
                            Connection con10 = new connection().cone();
                            Statement stt10 = con10.createStatement();
                            ResultSet rss10 = stt10.executeQuery("select * from againstpayment where party_name = '" + jcb1.getSelectedItem().toString() + "' and type='purchase'");
                            if (rss10.next()) {

                                newFrame();

                            } else if (point1 == 1) {
                                aSecondRow = new SecondRow();
                                aRowNoRowObjectMap.put(row + 1, aSecondRow);
                            } else if (aSecondRow != null) {
                                aSecondRow.jAccountNameCobmoBox.requestFocus();
                            }
                        } catch (SQLException evt) {
                            evt.printStackTrace();
                        }
                        if (key == KeyEvent.VK_ESCAPE) {
                            jcb1.requestFocus();
                        }
                    }
                }
            });
        }
    }

    public void updatePayAccountBal() {
        double samn = initialPayAccountBal;
        double ramn = 0;
        String AccountBalType = currentcbt;

        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + choice1.getSelectedItem() + "" + "'");

            while (rs.next()) {
                samn = Double.parseDouble(rs.getString(1));
                AccountBalType = rs.getString(2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (jtf1 != null && jtf1.getText().trim().length() > 0) {
            ramn = Double.parseDouble(jtf1.getText());
            if (AccountBalType.equals("CR")) {
                samn = samn + ramn;
                AccountBalType = "CR";
            } else if (AccountBalType.equals("DR")) {
                samn = samn - ramn;
                System.out.println("value of samn 2" + samn);
                if (samn >= 0) {
                    AccountBalType = "DR";
                } else {
                    AccountBalType = "CR";
                }
            }
        }
        Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
        int aRowNo = 0;
        SecondRow aSecondRow = null;
        while (aRowNoItertor.hasNext()) {
            aRowNo = (Integer) aRowNoItertor.next();
            aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
            if (aSecondRow.jTextField != null && aSecondRow.jTextField.getText().trim().length() > 0) {
                ramn = Double.parseDouble(aSecondRow.jTextField.getText());
                if (AccountBalType.equals("CR")) {
                    System.out.println("value of samn there" + samn);
                    samn = Math.abs(samn);
                    samn = samn + ramn;
                    AccountBalType = "CR";
                }
                if (AccountBalType.equals("DR")) {
                    samn = samn - ramn;
                    if (samn >= 0) {
                        AccountBalType = "DR";
                    } else {
                        AccountBalType = "CR";
                    }
                }
            }
        }
        jTextField5.setText("" + Math.abs(samn));
        jLabel8.setText(" " + AccountBalType.trim());
    }

    public void updateBal(String LedgerName) {
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + LedgerName + "'");
            String aBalTypeCd = "", aCurrentBal = "";
            while (rs.next()) {
                aCurrentBal = rs.getString(1);
                aBalTypeCd = rs.getString(2);
            }
            System.out.println("ledgerName " + LedgerName + "  " + aCurrentBal + "  " + aBalTypeCd);
            double result = Double.parseDouble(aCurrentBal);

            Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
            int aRowNo = 0;
            SecondRow aSecondRow = null;
            System.out.println("a current balance" + aCurrentBal);

            if (((String) jcb1.getSelectedItem()).equals(LedgerName)) {
                double ramn = Double.parseDouble(jtf1.getText());
                System.out.println("ramn");
                if (aBalTypeCd.equals("CR")) {
                    result = result - ramn;
                    System.out.println("cr result is " + result);
                    if (result < 0) {
                        aBalTypeCd = "DR";
                    } else if (result >= 0) {
                        aBalTypeCd = "CR";
                    }

                } else if (aBalTypeCd.equals("DR")) {
                    result = result + ramn;
                    System.out.println("dr result is " + result);
                    aBalTypeCd = "DR";
                }

                jcl17.setText("" + Math.abs(result));
                jcl18.setText(aBalTypeCd);
            }

            while (aRowNoItertor.hasNext()) {
                aRowNo = (Integer) aRowNoItertor.next();
                aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                System.out.println("aSecondRow.jAccountNameCobmoBox.getSelectedItem()  " + aSecondRow.jAccountNameCobmoBox.getSelectedItem());
                if (aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString().equals(LedgerName)) {
                    int ramn = Integer.parseInt(aSecondRow.jTextField.getText());
                    if (aBalTypeCd.equals("CR")) {
                        result = result - ramn;
                        if (result < 0) {
                            aBalTypeCd = "DR";
                        } else if (result >= 0) {
                            aBalTypeCd = "CR";
                        }
                    } else {
                        if (aBalTypeCd.equals("DR"));
                        {
                            result = result + ramn;

                            aBalTypeCd = "DR";

                        }
                    }
                }
            }
            result = Math.abs(result);
            aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
            while (aRowNoItertor.hasNext()) {
                aRowNo = (Integer) aRowNoItertor.next();
                aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                if (aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString().equals(LedgerName)) {
                    aSecondRow.jcl47.setText("" + Math.abs(result));
                    aSecondRow.jcl48.setText(aBalTypeCd);
                }
            }

            jPanel3.repaint();
            aLedgerNameCurrentBalMap.put(LedgerName, "" + Math.abs(result));
            aLedgerNameCurrentBalMapt.put(LedgerName, aBalTypeCd);
        } catch (Exception aExc) {
            aExc.printStackTrace();
        }
    }

    public void updatereverseBal(String LedgerName) {

        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            Statement stmt1 = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + LedgerName + "'");
            String aBalTypeCd = "", aCurrentBal = "";
            while (rs.next()) {
                aCurrentBal = rs.getString(1);
                aBalTypeCd = rs.getString(2);
            }

            double result = 0;
            Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
            int aRowNo = 0;
            SecondRow aSecondRow = null;
            System.out.println("a current balance" + aCurrentBal);

            double ramn = amount;
            result = Double.parseDouble(aCurrentBal);
            if (aBalTypeCd.equals("DR")) {
                result = result - ramn;
                System.out.println("cr result is " + result);
                if (result < 0) {
                    aBalTypeCd = "CR";
                } else {
                    if (result >= 0) {
                        aBalTypeCd = "DR";
                    }
                }
            } else if (aBalTypeCd.equals("CR")) {
                result = result + ramn;
                System.out.println("dr result is " + result);
                aBalTypeCd = "CR";
            }

            result = Math.abs(result);
            stmt1.executeUpdate("update ledger set curr_bal='" + result + "',currbal_type='" + aBalTypeCd + "' where ledger_name='" + LedgerName + "' ");
            System.out.println("update reverse to ledger is" + "update ledger set curr_bal='" + result + "',currbal_type='" + aBalTypeCd + "' where ledger_name='" + LedgerName + "' ");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePayreverseAccountBal(String Ledgername) {

        double samn = 0;
        double ramn = 0.0;
        String AccountBalType = "";

        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement st = conn.createStatement();
            Statement stmt1 = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + Ledgername + "'");
            while (rs.next()) {
                samn = Double.parseDouble(rs.getString(10));
                AccountBalType = rs.getString(11);
            }
            ramn = amount;
            if (AccountBalType.equals("DR")) {
                samn = samn + ramn;
                AccountBalType = "DR";
            } else if (AccountBalType.equals("CR")) {
                samn = samn - ramn;
                System.out.println("value of samn 2" + samn);
                if (samn >= 0) {
                    AccountBalType = "CR";
                } else {
                    AccountBalType = "DR";
                }
            }

            samn = Math.abs(samn);
            stmt1.executeUpdate("update ledger set curr_bal='" + samn + "',currbal_type='" + AccountBalType + "' where ledger_name='" + Ledgername + "'");
            System.out.println("update reverse by ledger" + "update ledger set curr_bal='" + samn + "',currbal_type='" + AccountBalType + "' where ledger_name='" + Ledgername + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class SecondRow {

        int selected = 0;
        public JComboBox jAccountNameCobmoBox;
        public JTextField jTextField;
        public SecondRow aNewRow = null;
        public boolean isRowAlreadyCreated = false;
        String baltypecd, rcamntv;
        public JLabel jcl47, jcl48, jcl16;

        //second table variable define
        JTable injTable2;
        JDialog inFrame2;
        JScrollPane injScrollPane2;
        JButton inButton2;

        Double grossamount2 = 0.0;

        List totallist2, alist2, secondrowpartyname2, tabledata2, amtbpaid2, llist2;

        DefaultTableModel model2;
        Object check2, billrefrence2, objectamount2, amounttobepaid2, remaining2;

        int rowCount2;
        int rowCountdata2;

        String jcb1Name2;
        String sno2, dddate2, acname2, type2, party_name2;

        Map<Object, Object> mymap3, mymap13, mymap23;
        //end here

        public SecondRow() {
            selected = ++row;

            //second table variable initilization
            totallist2 = new ArrayList();
            llist2 = new ArrayList();
            alist2 = new ArrayList();
            secondrowpartyname2 = new ArrayList();
            tabledata2 = new ArrayList();
            amtbpaid2 = new ArrayList();
            check2 = null;
            billrefrence2 = null;
            objectamount2 = null;
            amounttobepaid2 = null;
            remaining2 = null;
            rowCount2 = 0;
            rowCountdata2 = 0;

            mymap3 = new LinkedHashMap();
            mymap13 = new LinkedHashMap();
            mymap23 = new LinkedHashMap();
            //end here

            jAccountNameCobmoBox = new JComboBox();
            jTextField = new JTextField();
//             jcl16=new JLabel();
//             jPanel3.add(jcl16);
//             jcl16.setVisible(true);
//             jcl16.setBounds(120,10+(row*40),800,25) ;
            jPanel3.add(jAccountNameCobmoBox);
            jPanel3.add(jTextField);
            jAccountNameCobmoBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{""}));
            jAccountNameCobmoBox.setBounds(25, 10 + (row * 40), 325, 27);
            jTextField.setBounds(485, 10 + (row * 40), 100, 31);
            jAccountNameCobmoBox.setVisible(true);
            jTextField.setVisible(true);
            jTextField.setEnabled(false);
            jAccountNameCobmoBox.requestFocus();
            jTextField.setEnabled(false);
            jcl47 = new JLabel();
            jPanel3.add(jcl47);
            jcl47.setVisible(true);
            jcl47.setBounds(380, 10 + (row * 40), 800, 25);
            jcl48 = new JLabel();
            jPanel3.add(jcl48);
            jcl48.setVisible(true);
            jcl48.setBounds(430, 10 + (row * 40), 800, 25);
            try {
                jAccountNameCobmoBox.removeAll();
                connection c = new connection();
                Connection conn = c.cone();
                Statement st = conn.createStatement();
                Statement st1 = conn.createStatement();
                Statement st2 = conn.createStatement();
                ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH')order by ledger_name ASC");
                while (rs2.next()) {
                    jAccountNameCobmoBox.addItem(rs2.getString(1));
                }
                ResultSet rs = st.executeQuery("select Name from Group_create where Under IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS','KTH')");
                while (rs.next()) {
                    String gname = rs.getString(1);
                    ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='" + gname + "' order by ledger_name ASC");
                    while (rs1.next()) {
                        jAccountNameCobmoBox.addItem(rs1.getString(1));

                    }
                }
                st.close();
                st1.close();
                st2.close();
                conn.close();
            } catch (Exception ex) {
                System.out.println("Error : " + ex);
            }
            jAccountNameCobmoBox.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    jTextField.setEnabled(true);
                    int key = e.getKeyCode();
                    String sel = jAccountNameCobmoBox.getSelectedItem().toString();
                    if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_TAB) {
                        if (sel == "") {
                            jTextArea1.requestFocus();
                        } else {
                            name[count1] = (String) jAccountNameCobmoBox.getSelectedItem();
                            ++count1;

                            try {
                                connection c = new connection();
                                Connection conn = c.cone();
                                Statement stmt = conn.createStatement();
                                String cbt = null;
                                String str = jAccountNameCobmoBox.getSelectedItem().toString();
                                System.out.println(str);
                                ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
                                while (rs.next()) {
                                    baltypecd = rs.getString(2);
                                    rcamntv = rs.getString(1);
                                    // jcl16.setText("CURRENT BAL : -  ");
                                    jcl47.setText(rcamntv);
                                    jcl48.setText(baltypecd);
                                }
                                if (aLedgerNameCurrentBalMap.containsKey(str)) {
                                    jcl47.setText(aLedgerNameCurrentBalMap.get(str).toString());
                                    jcl48.setText(aLedgerNameCurrentBalMapt.get(str).toString());
                                }
                                stmt.close();
                                conn.close();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            jTextField.requestFocus();
                        }
                    }

                    if (key == KeyEvent.VK_ESCAPE) {
                        jTextField.requestFocus();
                    }
                }
            });
            jAccountNameCobmoBox.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                }
            });
            jTextField.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    double sum = 0;
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER) {
                        bal[count2] = Double.parseDouble(jTextField.getText());
                        currbal[count2] = Double.parseDouble(jcl47.getText());
                        currbaltype[count2] = jcl48.getText();

                        ++count2;
                        Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
                        int aRowNo = 0;
                        SecondRow aaSecondRow = null;
                        double ramn = 0.0;
                        if (jtf1.getText() != null && jtf1.getText().trim().length() > 0) {
                            sum = Double.parseDouble(jtf1.getText());
                        }
                        while (aRowNoItertor.hasNext()) {
                            aRowNo = (Integer) aRowNoItertor.next();
                            aaSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                            if (aaSecondRow.jTextField != null && aaSecondRow.jTextField.getText().trim().length() > 0) {
                                ramn = Double.parseDouble(aaSecondRow.jTextField.getText());
                                sum = sum + ramn;
                            }
                        }
                        jTextField4.setText("" + sum);
                        updatePayAccountBal();
                        updateBal(jAccountNameCobmoBox.getSelectedItem().toString());
                        ++point16;
                        //my code start here
                        inFrame2 = new JDialog();
                        injScrollPane2 = new javax.swing.JScrollPane();
                        injTable2 = new javax.swing.JTable();
                        inButton2 = new javax.swing.JButton();
                        Connection newcon = null;
                        try {
                            newcon = new connection().cone();
                        } catch (SQLException ex) {
                            Logger.getLogger(cashpayment.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        injTable2.setModel(new javax.swing.table.DefaultTableModel(
                                new Object[][]{}, new String[]{"Purchess Bill Number", "Amount", "Amount To Be Paid", "Remaining Amount"}) {
                            Class[] types = new Class[]{
                                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
                            };
                            boolean[] canEdit = new boolean[]{
                                false, false, true, false
                            };

                            @Override
                            public Class getColumnClass(int columnIndex) {
                                return types[columnIndex];
                            }

                            @Override
                            public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit[columnIndex];
                            }
                        });
                        injScrollPane2.setViewportView(injTable2);

                        inFrame2.getContentPane().add(injScrollPane2, java.awt.BorderLayout.CENTER);

                        inButton2.setText("Go");
                        inFrame2.getContentPane().add(inButton2, java.awt.BorderLayout.PAGE_END);

                        inFrame2.setSize(600, 400);
                        inFrame2.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                        inFrame2.setVisible(true);
                        inFrame2.pack();

                        String pname = jAccountNameCobmoBox.getSelectedItem().toString().trim();
                        try {
                            if (llist2.isEmpty()) {
                                System.out.println("combobox in second row : " + pname);

                                Statement newst = newcon.createStatement();
                                ResultSet newrs = newst.executeQuery("SELECT * FROM againstpayment WHERE party_name ='" + pname + "' and type ='purchase'");
                                while (newrs.next()) {
                                    llist2.add(newrs.getString("bill_no"));
                                    alist2.add(newrs.getString("amount"));
                                }
                                int a = 0;
                                a = llist2.size();
                                System.out.println("a for : " + a);

                                model2 = (DefaultTableModel) injTable2.getModel();
                                injTable2.removeAll();
                               
                                model2.fireTableDataChanged();
                                for (int i = 0; i < a; i++) {
                                    double netAmount = 0;
                                    double resultAmount = 0;
                                    Statement st2 = newcon.createStatement();
                                    System.out.println("list 1 i ; " + llist2.get(i));
                                    ResultSet rsamount = st2.executeQuery("select sum(amount) from againstpayment where Ref_Bill_no = '" + llist2.get(i) + "" + "'  ");

                                    while (rsamount.next()) {
                                        resultAmount = (rsamount.getInt(1));
                                        System.out.println("sum : " + resultAmount);
                                    }
                                    netAmount = Double.parseDouble(alist2.get(i).toString()) - resultAmount;
                                    System.out.println("net amount : " + netAmount);
                                    if (netAmount > 0) {

                                        Object object[] = {llist2.get(i) + "", netAmount, "", ""};
                                        model2.addRow(object);

                                    } else if (netAmount == 0) {
                                        String ss = "All The Bills To This Party Is Already Paid\n Do You Want To Treat This Payment As Advanced...!!!";
                                        //inFrame.dispose();
                                        int yesorno = JOptionPane.showConfirmDialog(rootPane, ss);
                                        if (yesorno == 0) {
                                            sno2 = jTextField.getText().trim();
                                            System.out.println("sno : " + sno);
                                            dddate2 = formatter.format(jDateChooser1.getDate()).trim();
                                            System.out.println("dddate : " + dddate);
                                            acname2 = choice1.getSelectedItem().trim();
                                            System.out.println("acname : " + acname);
                                            System.out.println("acname : " + acname);
                                            type2 = "Payment";
                                            billrefrence2 = "advanced";
                                            totallist2.clear();
                                            totallist2.add(jTextField.getText().trim());
                                            Statement st9 = newcon.createStatement();
                                            rowCountdata2 = st9.executeUpdate("Insert INTO againstpayment(Bill_no, Ref_Bill_no, date, party_name, Payer_name, Amount,type) VALUES ('" + sno2 + "','" + billrefrence2 + "','" + dddate2 + "','" + party_name2 + "','" + acname2 + "','" + totallist2.get(0).toString() + "','" + type2 + "')");
                                            inFrame2.dispose();
                                        } else {
                                            inFrame2.dispose();
                                            SecondRow aSecondRow = null;
                                            if (point1 == 1) {
                                                aSecondRow = new SecondRow();

                                                aRowNoRowObjectMap.put(row + 1, aSecondRow);

                                            } else if (aSecondRow != null) {
                                                aSecondRow.jAccountNameCobmoBox.requestFocus();
                                            }
                                        }
                                    }

                                }
                                
                                if(injTable2.getRowCount() !=0)
                                {
                                injTable2.getModel().setValueAt(jTextField.getText().trim(), 0, 2);
                                for (Map.Entry<Object, Object> entry : mymap3.entrySet()) {

                                    injTable2.getModel().setValueAt(entry.getValue(), Integer.parseInt(entry.getKey().toString()), 2);

                                }
                                for (Map.Entry<Object, Object> entry : mymap23.entrySet()) {

                                    injTable2.getModel().setValueAt(entry.getValue(), Integer.parseInt(entry.getKey().toString()), 3);

                                }
                                }
                            }
                            //for remove the element of map and list  
                            mymap3.clear();
                            mymap23.clear();
                            llist2.clear();
                            rfno2.clear();
                        } catch (SQLException newEx) {
                            newEx.printStackTrace();
                        }
                        CellEditorListener changeNotification = new CellEditorListener() {
                            @Override
                            public void editingCanceled(ChangeEvent e) {

                                System.out.println("cencel");
                            }

                            @Override
                            public void editingStopped(ChangeEvent e) {

                                int row = injTable2.getSelectedRow();
                                int column = injTable2.getSelectedColumn();

                                String first = injTable2.getModel().getValueAt(row, 1).toString();
                                String second = injTable2.getModel().getValueAt(row, 2).toString();
                                if (second.isEmpty()) {
                                    second = "0.0";
                                }

                                double set = Double.parseDouble(first) - Double.parseDouble(second);
                                System.out.println("first" + first + "second" + second + "set" + set);

                                injTable2.getModel().setValueAt(set, row, 3);
                                injTable2.repaint();
                            }
                        };
                        injTable2.getDefaultEditor(Object.class).addCellEditorListener(changeNotification);

                        inButton2.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {

                                sno2 = jTextField.getText().trim();
                                tabledata2.add(sno2);
                                dddate2 = formatter.format(jDateChooser1.getDate()).trim();
                                tabledata2.add(dddate2);
                                acname2 = choice1.getSelectedItem().trim();
                                tabledata2.add(acname2);
                                type2 = "Payment";
                                tabledata2.add(acname2);
                                party_name2 = jAccountNameCobmoBox.getSelectedItem().toString();
                                System.out.println("party name : " + party_name2);
                                secondrowpartyname2.add(party_name2);
                                rowCount2 = injTable2.getRowCount();
                                int totalam = 0;
                                for (int i = 0; i < rowCount2; i++) {
                                    String ss = injTable2.getValueAt(i, 2).toString();
                                    System.out.println("!ss.trim().isEmpty() : " + !ss.trim().isEmpty());
                                    System.out.println("ss is : " + ss);
                                    if (!ss.trim().isEmpty() && !ss.equals("0")) {
                                        billrefrence2 = (Object) injTable2.getModel().getValueAt(i, 0);
                                        rfno2.add(billrefrence2);
                                        //tabledata.add(billrefrence2);
                                        objectamount2 = (Object) injTable2.getModel().getValueAt(i, 1);
                                        // tabledata.add(objectamount2);
                                        amounttobepaid2 = (Object) injTable2.getModel().getValueAt(i, 2);
                                        //  tabledata.add(amounttobepaid2);
                                        remaining2 = (Object) injTable2.getModel().getValueAt(i, 3);
                                        amtbpaid2.add(amounttobepaid2);
                                        mymap3.put(i, amounttobepaid2);
                                        mymap23.put(i, remaining2);
                                        totalam += Integer.parseInt(amounttobepaid2.toString());
                                        System.out.println("check" + check2 + "billrefrence" + billrefrence2 + "amount" + objectamount2 + "amounttobepaid" + amounttobepaid2);
                                    }

                                }
                                int nettotalam = Integer.parseInt(jTextField.getText().trim());
                                System.out.println("nettotalam :" + nettotalam + "totalam" + totalam);
                                if (nettotalam < totalam) {
                                    JOptionPane.showMessageDialog(inFrame2, "Invaid Entry...");
                                    amtbpaid2.clear();
                                    mymap3.clear();
                                    mymap23.clear();
                                    rfno2.clear();
                                    inFrame2.dispose();
                                    inFrame2.setVisible(true);
                                } else {
                                    jcb1Name2 = jAccountNameCobmoBox.getSelectedItem().toString().trim();
                                    // amtbpaid2.clear();
                                    mymap3.clear();
                                    mymap23.clear();
                                    // mymap2.put(party_name2, tabledata2);
                                    inFrame2.dispose();
                                    if (!isRowAlreadyCreated) {
                                        aNewRow = new SecondRow();
                                        aRowNoRowObjectMap.put(row + 1, aNewRow);
                                        isRowAlreadyCreated = true;

                                        // newFrame2();
                                    } else if (aNewRow != null) {
                                        aNewRow.jAccountNameCobmoBox.requestFocus();
                                    }
                                }
                            }
                        });
                        //end here

//                         double samn= Double.parseDouble(jTextField5.getText());
//                       double ramn=Double.parseDouble(jTextField.getText());
//                                   currentcbt= jLabel8.getText();
//                                   if(currentcbt.equals("DR"))
//                                  {
//                                  double result1=samn+ramn;
//                                  jLabel8.setText(" "+"DR");
//                                        jTextField5.setText(""+Math.abs(result1));
//                                  }
//                                  else
//                                  {
//                                      if(currentcbt.equals("CR"));
//                                  {
//                                  double result1=samn-ramn;
//                                    if(result1>0)
//                                    { result1=samn-ramn;
//                                	jLabel8.setText(" "+"CR");
//                                        jTextField5.setText(""+Math.abs(result1));
//                                    }
//                                    else
//                                    {
//                                        if(result1<0)
//                                        {
//                                            jLabel8.setText(" "+"DR");
//                                        jTextField5.setText(""+Math.abs(result1));
//                                        }
//                                    }
//                                  }
//                                  }
                        if (!isRowAlreadyCreated) {
                            aNewRow = new SecondRow();
                            aRowNoRowObjectMap.put(row + 1, aNewRow);
                            isRowAlreadyCreated = true;
                        } else if (aNewRow != null) {
                            aNewRow.jAccountNameCobmoBox.requestFocus();
                        }
                    }

                    if (key == KeyEvent.VK_ESCAPE) {
                        jAccountNameCobmoBox.requestFocus();
                    }
                }
            });
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        choice1 = new java.awt.Choice();
        jTextField5 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : PAYMENT");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel1.setText("SNO. :");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel2.setText("DATE :");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel3.setText("AMOUNT ");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel4.setText("ACCOUNT");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel5.setText("NARRATION :");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel6.setText("GRAND TOTAL :");

        jTextField1.setEditable(false);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(2);
        jTextArea1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextArea1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextArea1);

        jTextField4.setEditable(false);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel7.setText("PARTICULARS");

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel10.setText("CURR. BAL. : ");

        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        choice1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                choice1ItemStateChanged(evt);
            }
        });
        choice1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                choice1FocusLost(evt);
            }
        });
        choice1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                choice1KeyPressed(evt);
            }
        });

        jTextField5.setEditable(false);
        jTextField5.setBackground(new java.awt.Color(204, 204, 255));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 765, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 471, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(jPanel3);

        jButton3.setText("UPDATE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel10)
                        .addGap(12, 12, 12)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(26, 26, 26)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(379, 379, 379)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(321, 321, 321)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton3)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton1)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton2))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(461, 461, 461)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel10)
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton3)
                            .addComponent(jButton1)
                            .addComponent(jButton2))))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 4, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setSize(new java.awt.Dimension(702, 634));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void otherComponents() {

    }
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        //   jPopupMenu1.setVisible(false);        
    }//GEN-LAST:event_formWindowClosing

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed

    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated

    }//GEN-LAST:event_formWindowActivated

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        setVisible(false);

    }//GEN-LAST:event_jButton2ActionPerformed
    public boolean issave = false;

    public void save() {
        int i1 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i1 == 0 && issave == false) {
            issave = true;
            PreparedStatement ps = null;
//            int i=0;
            String s_no = (jTextField1.getText());
            String da_te = formatter.format(jDateChooser1.getDate());
            String by_ledger = choice1.getSelectedItem().toString();
            String narra_tion = jTextArea1.getText();
            String grand_total = jTextField4.getText();

            try {
                connection c = new connection();
                Connection conneee = c.cone();
                Statement st1 = conneee.createStatement();
                if (jtf1.getText() != null && jtf1.getText().trim().length() > 0) {
                    st1.executeUpdate("insert into payment values('" + s_no + "','" + da_te + "','" + by_ledger + "','" + jcb1.getSelectedItem().toString() + "','" + jtf1.getText() + "','" + narra_tion + "','" + grand_total + "'," + (1) + ",'0') ");
                }
                Iterator it1 = aRowNoRowObjectMap.keySet().iterator();
                int i = 1;
                while (it1.hasNext()) {
                    int aRowNo = (Integer) it1.next();
                    SecondRow aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);

                    if (aSecondRow.jTextField.getText() != null && aSecondRow.jTextField.getText().trim().length() > 0) {
                        st1.executeUpdate("insert into payment values('" + s_no + "','" + da_te + "','" + by_ledger + "','" + aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString() + "','" + aSecondRow.jTextField.getText() + "','" + narra_tion + "','" + grand_total + "'," + (++i) + ",'0') ");
                    }

                    // to second row and after second row  
                    String totaal = "";
                    String billno = "";
                    for (int l = 0; l < aSecondRow.amtbpaid2.size(); l++) {
                        billno = rfno2.get(l).toString();
                        totaal = aSecondRow.amtbpaid2.get(l).toString();
                        System.out.println("total second row : " + totaal);
                        newcon = new connection().cone();
                        Statement st = newcon.createStatement();

                        aSecondRow.rowCountdata2 = st.executeUpdate("Insert INTO againstpayment(Bill_no, Ref_Bill_no, date, party_name, Payer_name, Amount,type) VALUES ('" + aSecondRow.sno2 + "','" + billno + "','" + aSecondRow.dddate2 + "','" + aSecondRow.party_name2 + "','" + aSecondRow.acname2 + "','" + totaal + "','" + aSecondRow.type2 + "')");
                        System.out.println("Insert INTO againstpayment(Bill_no, Ref_Bill_no, date, party_name, Payer_name, Amount,type) VALUES ('" + aSecondRow.sno2 + "','" + billno + "','" + aSecondRow.dddate2 + "','" + aSecondRow.party_name2 + "','" + aSecondRow.acname2 + "','" + totaal + "','" + aSecondRow.type2 + "')");
                    }

                }

                Statement st2 = conneee.createStatement();
                Iterator it = aLedgerNameCurrentBalMap.keySet().iterator();
                while (it.hasNext()) {
                    String LedgerName = (String) (it.next());
                    String currbal = (String) aLedgerNameCurrentBalMap.get(LedgerName);
                    String baltype = (String) aLedgerNameCurrentBalMapt.get(LedgerName);
                    st1.executeUpdate("update ledger set curr_bal='" + currbal + "',currbal_type='" + baltype + "'  where ledger_name = '" + LedgerName + "'");
                }
                st1.executeUpdate("update ledger set curr_bal='" + jTextField5.getText().toString() + "',currbal_type='" + jLabel8.getText().trim() + "' where ledger_name = '" + choice1.getSelectedItem().toString() + "'");

                JOptionPane.showMessageDialog(null, "Inserted Sucessfully.");

                conneee.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            dispose();

        }

    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        save();

        try {
            String totaal = "";
            String billno = "";
            for (int k = 0; k < amtbpaid.size(); k++) {
                totaal = amtbpaid.get(k).toString();
                System.out.println("total : " + totaal);
                billno = rfno.get(k).toString();
                newcon = new connection().cone();
                Statement st = newcon.createStatement();
                rowCountdata = st.executeUpdate("Insert INTO againstpayment(Bill_no, Ref_Bill_no, date, party_name, Payer_name, Amount,type) VALUES ('" + sno + "','" + billno + "','" + dddate + "','" + party_name1 + "','" + acname + "','" + totaal + "','" + type + "')");
                System.out.println("Insert INTO againstpayment(Bill_no, Ref_Bill_no, date, party_name, Payer_name, Amount,type) VALUES ('" + sno + "','" + billno + "','" + dddate + "','" + party_name1 + "','" + acname + "','" + totaal + "','" + type + "')");
            }
        } catch (Exception ee) {
            System.out.println(ee);
        }
        cashpayment cp = new cashpayment();
        cp.setVisible(true);
        cp.jDateChooser1.setDate(jDateChooser1.getDate());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void choice1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_choice1ItemStateChanged
        String str = (String) evt.getItem();
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            String cbt = null;
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
            while (rs.next()) {
                initialPayAccountBal = Double.parseDouble(rs.getString(1));
                currentcbt = rs.getString(2);
                jTextField5.setText(initialPayAccountBal + "");
                jLabel8.setText(currentcbt);
            }
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_choice1ItemStateChanged

    private void choice1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_choice1FocusLost

    }//GEN-LAST:event_choice1FocusLost

    private void jTextArea1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea1KeyPressed

        int i = 0;
        double sum = 0;
        int key = evt.getKeyCode();
        if (key == KeyEvent.VK_ENTER) {
            while (i < count2) {
                sum = bal[i] + sum;
                i++;
            }
            jTextField4.setText("" + sum);
            jButton1.requestFocus();
        }
    }//GEN-LAST:event_jTextArea1KeyPressed
    public double amount = 0;
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int i1 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to update");
        if (i1 == 0) {

            String toledger;
            String byledger;

            try {
                connection c = new connection();
                Connection conneee = c.cone();
                Statement st1 = conneee.createStatement();
                Statement st2 = conneee.createStatement();
                ResultSet rs = st1.executeQuery("select * from payment where sno='" + jTextField1.getText() + "'");

                while (rs.next()) {
                    toledger = rs.getString("to_ledger");
                    byledger = rs.getString("by_ledger");
                    amount = Double.parseDouble(rs.getString("amo_unt"));
                    updatereverseBal(toledger);
                    updatePayreverseAccountBal(byledger);
                }

                st2.executeUpdate("delete from payment where sno='" + jTextField1.getText() + "'");

                updateBal(jcb1.getSelectedItem() + "".trim());
                updatePayAccountBal();
                save();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }//GEN-LAST:event_jButton3ActionPerformed

    private void choice1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_choice1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            jcb1.requestFocus();
        }

    }//GEN-LAST:event_choice1KeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jDateChooser1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            save();
            cashpayment cp = new cashpayment();
            cp.setVisible(true);
            cp.jDateChooser1.setDate(jDateChooser1.getDate());
        }

    }//GEN-LAST:event_jButton1KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new cashpayment().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public java.awt.Choice choice1;
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    public javax.swing.JLabel jLabel8;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JTextArea jTextArea1;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    // End of variables declaration//GEN-END:variables
}
