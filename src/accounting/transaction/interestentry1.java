package accounting.transaction;

import com.toedter.calendar.JTextFieldDateEditor;
import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Prateek^
 */
public class interestentry1 extends javax.swing.JFrame {

    /**
     * Creates new form bsnlbilling
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());

    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    String Year;
 private static interestentry1 obj = null;
 
    public interestentry1() {
        initComponents();
        jButton3.setVisible(false);
        JTextFieldDateEditor editor = (JTextFieldDateEditor) jDateChooser1.getDateEditor();
        editor.setEditable(false);
        try {
            System.out.println("date now is" + formatter.parse(dateNow));
            System.out.println("date after is" + formatter.parse("31-03-20" + getyear + ""));
            java.util.Date dateafter = formatter.parse("31-03-20" + getyear + "");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            Statement stmt1 = connect.createStatement();
           Statement st1 = connect.createStatement();
           ResultSet rs_year = st1.executeQuery("Select * from year");
            while(rs_year.next()){
            Year = rs_year.getString("year");
            }
            String sales = "IST/";
            String slash = "/";
            String Branchno = "1/";
            String yearwise = Year.concat(slash);
//            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String finalconcat = ((sales.concat(yearwise)));

            ResultSet rs4 = stmt.executeQuery("SELECT s_no FROM interestentry ");
            int a = 0, b, d = 0;
            int i1 = 0;
            while (rs4.next()) {
                if (i1 == 0) {
                    d = rs4.getString(1).lastIndexOf("/");
                    i1++;
                }
                b = Integer.parseInt(rs4.getString(1).substring(d + 1));
                if (a < b) {
                    a = b;
                }
            }
            String s = finalconcat.concat(Integer.toString(a + 1));
            jTextField1.setText(s);
            jDateChooser1.setDate(formatter.parse(dateNow));

            ResultSet rs = stmt1.executeQuery("select * from ledger where groups='KTH'");
            while (rs.next()) {
                jComboBox1.addItem(rs.getString(1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
        public static interestentry1 getObj() {
        if (obj == null) {
            obj = new interestentry1();
        }
        return obj;
    }

    // Updating current ledger Calculation before final update entry
    public void updatebalance1() {
        double intrestamnt = 0;
        double tdsamnt = 0;
        double totalamnt1 = 0;
        double partycurrbal1 = 0;
        String partycurrbaltype1 = "";
        double partyaccountupdateamount1 = 0;

        double interestcurrbal1 = 0;
        String interestcurrbaltype1 = "";
        double interestaccountupdateamount1 = 0;

        double TDScurrbal1 = 0;
        String TDScurrbaltype1 = "";
        double TDSaccountupdateamount1 = 0;
        String party_name = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            Statement party_st = connect.createStatement();
            ResultSet party_rs = party_st.executeQuery("Select Party_name from interestentry where s_no = '" + jTextField1.getText() + "'");
            while (party_rs.next()) {
                party_name = party_rs.getString("Party_name");
            }
            ResultSet rs = stmt.executeQuery("Select amnt from interestentry where s_no = '" + jTextField1.getText() + "'and by_ledger = '" + party_name + "' and to_ledger = 'INTREST A/C'");
            while (rs.next()) {
                intrestamnt = Double.parseDouble(rs.getString("amnt"));
        
            }
            ResultSet rs1 = stmt.executeQuery("Select amnt from interestentry where s_no = '" + jTextField1.getText() + "'and to_ledger = '" + party_name + "' and by_ledger = 'TDS PAYABLE'");
            while (rs1.next()) {
                tdsamnt = Double.parseDouble(rs1.getString("amnt"));
            }
            ResultSet rs2 = stmt.executeQuery("select * from ledger where ledger_name='" + party_name + "" + "'");

            while (rs2.next()) {
                partycurrbal1 = Double.parseDouble(rs2.getString(10));
                partycurrbaltype1 = rs2.getString(11);

            }
            System.out.println("interest amnt is"+intrestamnt);
             System.out.println("tds amnt is"+tdsamnt);
             
            totalamnt1 = intrestamnt - tdsamnt;

            if (partycurrbaltype1.equals("DR")) {
                partyaccountupdateamount1 = partycurrbal1 + totalamnt1;
                if (partyaccountupdateamount1 > 0) {
                    partycurrbaltype1 = "DR";
                    partyaccountupdateamount1 = (Math.abs(partyaccountupdateamount1));
                }
                if (partyaccountupdateamount1 <= 0) {
                    partycurrbaltype1 = "CR";
                    partyaccountupdateamount1 = Math.abs(partyaccountupdateamount1);
                }
            } else if (partycurrbaltype1.equals("CR")) {
                partyaccountupdateamount1 = partycurrbal1 - totalamnt1;
                partycurrbaltype1 = "CR";
                partyaccountupdateamount1 = Math.abs(partyaccountupdateamount1);

            }
            jLabel6.setText(partyaccountupdateamount1 + "");
            jLabel8.setText(partycurrbaltype1);

            ResultSet rs3 = stmt.executeQuery("select * from ledger where ledger_name='TDS PAYABLE'");

            while (rs3.next()) {
                TDScurrbal1 = Double.parseDouble(rs3.getString(10));
                TDScurrbaltype1 = rs3.getString(11);
            }

            double TDSamnt1 = tdsamnt;

            if (TDScurrbaltype1.equals("DR")) {
                TDSaccountupdateamount1 = TDScurrbal1 + TDSamnt1;
                if (TDSaccountupdateamount1 > 0) {
                    TDScurrbaltype1 = "DR";
                    TDSaccountupdateamount1 = (Math.abs(TDSaccountupdateamount1));
                }
                if (TDSaccountupdateamount1 <= 0) {
                    TDScurrbaltype1 = "CR";
                    TDSaccountupdateamount1 = Math.abs(TDSaccountupdateamount1);
                }
            } else if (TDScurrbaltype1.equals("CR")) {
                TDSaccountupdateamount1 = TDScurrbal1 - TDSamnt1;
                TDScurrbaltype1 = "CR";
                TDSaccountupdateamount1 = Math.abs(TDSaccountupdateamount1);
            }
            ResultSet rs4 = stmt.executeQuery("select * from ledger where ledger_name='INTREST A/C'");

            while (rs4.next()) {
                interestcurrbal1 = Double.parseDouble(rs4.getString(10));
                interestcurrbaltype1 = rs4.getString(11);
            }
            double amnt1 = intrestamnt;

            if (interestcurrbaltype1.equals("CR")) {
                interestaccountupdateamount1 = interestcurrbal1 + amnt1;

                if (interestaccountupdateamount1 > 0) {
                    interestcurrbaltype1 = "CR";
                    interestaccountupdateamount1 = (Math.abs(interestaccountupdateamount1));
                }
                if (interestaccountupdateamount1 <= 0) {
                    interestcurrbaltype1 = "DR";
                    interestaccountupdateamount1 = Math.abs(interestaccountupdateamount1);
                }
            } else if (interestcurrbaltype1.equals("DR")) {
                interestaccountupdateamount1 = interestcurrbal1 - amnt1;
                interestcurrbaltype1 = "DR";
                interestaccountupdateamount1 = Math.abs(interestaccountupdateamount1);
            }
            
            stmt.executeUpdate("Update ledger set curr_bal='" + partyaccountupdateamount1 + "" + "',currbal_type='" + partycurrbaltype1 + "' where ledger_name='" + party_name + " " + "'");
            stmt.executeUpdate("Update ledger set curr_bal='" + TDSaccountupdateamount1 + "" + "',currbal_type='" + TDScurrbaltype1 + "' where ledger_name='TDS PAYABLE'");
            stmt.executeUpdate("Update ledger set curr_bal='" + interestaccountupdateamount1 + "" + "',currbal_type='" + interestcurrbaltype1 + "' where ledger_name='INTREST A/C'");
     
        
//            System.out.println("For Party - "+"Update ledger set curr_bal='" + partyaccountupdateamount1 + "" + "',currbal_type='" + partycurrbaltype1 + "' where ledger_name='" + party_name + " " + "'");
//            System.out.println("For TDS - "+ "Update ledger set curr_bal='" + TDSaccountupdateamount1 + "" + "',currbal_type='" + TDScurrbaltype1 + "' where ledger_name='TDS PAYABLE'");
//            System.out.println("For Interest -"+"Update ledger set curr_bal='" + interestaccountupdateamount1 + "" + "',currbal_type='" + interestcurrbaltype1 + "' where ledger_name='INTREST A/C'");
//            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Updating the inerest entry report
    public void updateIntrestEntry() {
        int response = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to update ?");
        switch (response) {
            case JOptionPane.YES_OPTION:
                updatebalance1();
                //              updatebalance();
                String partyName = "";
                double ledger_bal = 0;
                double updated_ledger_bal = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stmt = connect.createStatement();
                    stmt.executeUpdate("Delete from interestentry where s_no = '" + jTextField1.getText() + "'");
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispose();

                break;
            case JOptionPane.NO_OPTION:
                dispose();
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setText("S No. : -");

        jTextField1.setEditable(false);

        jLabel2.setText("Date: -");

        jLabel3.setText("Party Name: -");

        jLabel4.setText("Interest Amount");

        jTextField4.setText("0");
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel5.setText("TDS PAYABLE");

        jTextField5.setText("0");
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel7.setText("Narration: -");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setText("Interest Account");
        jTextArea1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextArea1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextArea1);

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel6.setText("jLabel6");

        jLabel8.setText("jLabel8");

        jButton3.setText("Update");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(61, 61, 61)
                        .addComponent(jScrollPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton3)
                                        .addGap(18, 18, 18)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButton1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButton2))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(37, 37, 37)
                                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(45, 45, 45))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton2)
                            .addComponent(jButton3))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(85, 85, 85))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:

        jDateChooser1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {

            double previousamount = 0;
            double currentamount = 0;
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select sum(amnt) from interestentry where by_ledger='" + jComboBox1.getSelectedItem().toString() + "' and to_ledger='INTREST A/C'");
                while (rs.next()) {
                    previousamount = rs.getDouble(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            currentamount = Double.parseDouble(jTextField4.getText());

            if ((currentamount + previousamount) > 5000) {
                JOptionPane.showMessageDialog(rootPane, "PARTY IS APPLICABLE FOR TDS");

                jTextField5.requestFocus();
            } else {
                jTextArea1.requestFocus();
            }

        }
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:

        double netamnt = 0;
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jTextArea1.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed
    public boolean issave = false;

    public void save() {
//        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
//        if (i == 0 && issave == false) {
        issave = true;
        updatebalance();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            Statement stmt1 = connect.createStatement();
            Statement stmt2 = connect.createStatement();
            Statement stmt3 = connect.createStatement();

            String date = formatter.format(jDateChooser1.getDate());

            stmt.executeUpdate("insert into interestentry values ('" + jTextField1.getText() + "','" + date + "','" + jComboBox1.getSelectedItem() + "" + "','" + jTextField4.getText() + "','INTREST A/C','" + jComboBox1.getSelectedItem() + "" + "','" + jTextArea1.getText() + "')");

            if (Double.parseDouble(jTextField5.getText()) != 0) {
                stmt.executeUpdate("insert into interestentry values ('" + jTextField1.getText() + "','" + date + "','" + jComboBox1.getSelectedItem() + "" + "','" + jTextField5.getText() + "','" + jComboBox1.getSelectedItem() + "','TDS PAYABLE','" + jTextArea1.getText() + "')");
            }

            stmt1.executeUpdate("Update ledger set curr_bal='" + partyaccountupdateamount + "" + "',currbal_type='" + partycurrbaltype + "' where ledger_name='" + jComboBox1.getSelectedItem() + "" + "'");
            stmt2.executeUpdate("Update ledger set curr_bal='" + TDSaccountupdateamount + "" + "',currbal_type='" + TDScurrbaltype + "' where ledger_name='TDS PAYABLE'");
            stmt3.executeUpdate("Update ledger set curr_bal='" + interestaccountupdateamount + "" + "',currbal_type='" + interestcurrbaltype + "' where ledger_name='INTREST A/C'");

        } catch (Exception e) {
            e.printStackTrace();
        }
        dispose();
        partycurrbal = 0;
        partycurrbaltype = "";
        partyaccountupdateamount = 0;
        partycurrbal = 0;
        partycurrbaltype = "";
        partyaccountupdateamount = 0;
        partycurrbal = 0;
        partycurrbaltype = "";
        partyaccountupdateamount = 0;
        accounting.transaction.interestentry1 is = new interestentry1();
        is.setVisible(true);
        is.jDateChooser1.setDate(jDateChooser1.getDate());

//        }
    }

    double partycurrbal = 0;
    String partycurrbaltype = "";
    double partyaccountupdateamount = 0;

    double interestcurrbal = 0;
    String interestcurrbaltype = "";
    double interestaccountupdateamount = 0;

    double TDScurrbal = 0;
    String TDScurrbaltype = "";
    double TDSaccountupdateamount = 0;

    public void updatebalance() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            Statement stmt1 = connect.createStatement();
            Statement stmt2 = connect.createStatement();

            ResultSet rs = stmt.executeQuery("select * from ledger where ledger_name='" + jComboBox1.getSelectedItem() + "" + "'");

            while (rs.next()) {
                partycurrbal = Double.parseDouble(rs.getString(10));
                partycurrbaltype = rs.getString(11);
            }
            double totalamnt = Double.parseDouble(jTextField4.getText()) - Double.parseDouble(jTextField5.getText());

            if (partycurrbaltype.equals("DR")) {
                partyaccountupdateamount = partycurrbal - totalamnt;
                if (partyaccountupdateamount > 0) {
                    partycurrbaltype = "DR";
                    partyaccountupdateamount = (Math.abs(partyaccountupdateamount));
                }
                if (partyaccountupdateamount <= 0) {
                    partycurrbaltype = "CR";
                    partyaccountupdateamount = Math.abs(partyaccountupdateamount);
                }
            } else if (partycurrbaltype.equals("CR")) {
                partyaccountupdateamount = partycurrbal + totalamnt;
                partycurrbaltype = "CR";
                partyaccountupdateamount = Math.abs(partyaccountupdateamount);

            }
            jLabel6.setText(partyaccountupdateamount + "");
            jLabel8.setText(partycurrbaltype);

            ResultSet rs1 = stmt.executeQuery("select * from ledger where ledger_name='TDS PAYABLE'");

            while (rs1.next()) {
                TDScurrbal = Double.parseDouble(rs1.getString(10));
                TDScurrbaltype = rs1.getString(11);
            }

            double TDSamnt = Double.parseDouble(jTextField5.getText());

            if (TDScurrbaltype.equals("DR")) {
                TDSaccountupdateamount = TDScurrbal - TDSamnt;
                if (TDSaccountupdateamount > 0) {
                    TDScurrbaltype = "DR";
                    TDSaccountupdateamount = (Math.abs(TDSaccountupdateamount));
                }
                if (TDSaccountupdateamount <= 0) {
                    TDScurrbaltype = "CR";
                    TDSaccountupdateamount = Math.abs(TDSaccountupdateamount);
                }
            } else if (TDScurrbaltype.equals("CR")) {
                TDSaccountupdateamount = TDScurrbal + TDSamnt;
                TDScurrbaltype = "CR";
                TDSaccountupdateamount = Math.abs(TDSaccountupdateamount);
            }
            ResultSet rs2 = stmt2.executeQuery("select * from ledger where ledger_name='INTREST A/C'");

            while (rs2.next()) {
                interestcurrbal = Double.parseDouble(rs2.getString(10));
                interestcurrbaltype = rs2.getString(11);
            }
            double amnt = Double.parseDouble(jTextField4.getText());

            if (interestcurrbaltype.equals("CR")) {
                interestaccountupdateamount = interestcurrbal - amnt;

                if (interestaccountupdateamount > 0) {
                    interestcurrbaltype = "CR";
                    interestaccountupdateamount = (Math.abs(interestaccountupdateamount));
                }
                if (interestaccountupdateamount <= 0) {
                    interestcurrbaltype = "DR";
                    interestaccountupdateamount = Math.abs(interestaccountupdateamount);
                }
            } else if (interestcurrbaltype.equals("DR")) {
                interestaccountupdateamount = interestcurrbal + amnt;
                interestcurrbaltype = "DR";
                interestaccountupdateamount = Math.abs(interestaccountupdateamount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i == 0 && issave == false) {
            save();

        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextArea1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jButton1.requestFocus();
        }
    }//GEN-LAST:event_jTextArea1KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            save();
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        // TODO add your handling code here:
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();

            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='" + jComboBox1.getSelectedItem() + "" + "'");
            while (rs.next()) {
                jLabel6.setText(rs.getString(1));
                jLabel8.setText(rs.getString(2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        updateIntrestEntry();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(interestentry1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(interestentry1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(interestentry1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(interestentry1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new interestentry1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JComboBox jComboBox1;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    public javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTextArea jTextArea1;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    // End of variables declaration//GEN-END:variables
}
