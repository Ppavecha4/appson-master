package accounting.transaction;

import connection.connection;
import java.awt.Color;
import java.awt.Panel;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleRole;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.OptionPaneUI;

public class journ extends javax.swing.JFrame {

    int row = -1;
    public List RowList;
    public newRow aFirstRow = null;
    public Map aRowNoRowObjectMap = new LinkedHashMap(), aLedgerNameCurrentBalMap = new LinkedHashMap(), aLedgerNameCurrentBalMapt = new LinkedHashMap();
    public int balanceAmount = 0;
    public int point, point1, point2, point3, point4, point5, point6, point7, point8, point9, point10, point11, point12, point13, point14, point15, point16, point17 = 0;
    public JComboBox jcb1, jcb2;
    public JTextField jtf1, jtf2;
    public JLabel jcl1, jcl2, jcl3;
    String baltypecd;
    public String currentcbt;
    String rcamntv;
    public String toby[] = new String[0];
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    int count = 0;
    static int currbal[] = new int[100];
    static String currbaltype[] = new String[100];
    static String atoby[] = new String[100];
    static String aaccnameto[] = new String[100];
    static String debitamt[] = new String[100];
    static String creditamt[] = new String[100];
    Map<Integer, UpdateJournal> updateJourn = new LinkedHashMap<Integer, UpdateJournal>();
    public int a = 0;

    private static journ obj = null;

    public journ() {
        initComponents();
        this.setLocationRelativeTo(null);
        jButton3.setVisible(false);

        Calendar today = Calendar.getInstance();
        String Journal = "JR/";
        String slash = "/";
        String yearwise = (getyear.concat(nextyear)).concat(slash);
        String finalconcat = (Journal.concat(yearwise));

        try {
            jDateChooser1.setDate(formatter.parse(dateNow));
        } catch (ParseException ex) {
            Logger.getLogger(journ.class.getName()).log(Level.SEVERE, null, ex);
        }

        int a = 0, b, d = 0;
        int i = 0;
        try {

            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  (s_no) FROM journal where branchid='0'");
            while (rs.next()) {
                if (i == 0) {
                    d = rs.getString(1).lastIndexOf("/");
                    i++;
                }
                b = Integer.parseInt(rs.getString(1).substring(d + 1));
                if (a < b) {
                    a = b;
                }
            }
            String s = finalconcat.concat(Integer.toString(a + 1));
            jTextField1.setText("" + s);
        } catch (SQLException sqe) {

            sqe.printStackTrace();
        }

        jcb1 = new JComboBox();
        jtf1 = new JTextField();
        aFirstRow = new newRow();
        //      jTextField2.setText(today.get(today.DATE) +"-"+(today.get(today.MONTH)+1)+"-"+today.get(today.YEAR));
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ENTER) {
                    jDateChooser1.requestFocus();
                }
            }
        });

        jDateChooser1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ENTER) {
                    jcb1.requestFocus();
                }
                if (key == KeyEvent.VK_ESCAPE) {
                    jTextField1.requestFocus();
                }
            }
        });
    }

    public static journ getObj() {
        if (obj == null) {
            obj = new journ();
        }
        return obj;
    }

    public Map<Integer, UpdateJournal> getUpdateJourn() {
        return updateJourn;
    }

    public void setUpdateJourn(Map<Integer, UpdateJournal> updateJourn) {
        this.updateJourn = updateJourn;
    }

    public journ.SecondRow createAnotherRow() {

        journ.SecondRow aSecondRow = new journ.SecondRow();
        aRowNoRowObjectMap.put(row + 1, aSecondRow);
        return aSecondRow;
    }

    public class newRow {

        int selected;
        public SecondRow aSecondRow = null;

        public newRow() {
            selected = ++row;
            jcb1 = new JComboBox();
            jcb2 = new JComboBox();
            jtf1 = new JTextField();
            jtf2 = new JTextField();
//              jcl1 = new JLabel();
//            jPanel2.add(jcl1);
//            jcl1.setVisible(true);
//            jcl1.setBounds(172, 10 + (row * 40), 800, 25);
            jcl2 = new JLabel();
            jPanel2.add(jcl2);
            jcl2.setVisible(true);
            jcl2.setBounds(360, 10 + (row * 40), 800, 25);
            jcl3 = new JLabel();
            jPanel2.add(jcl3);
            jcl3.setVisible(true);
            jcl3.setBounds(410, 10 + (row * 40), 800, 25);
            jPanel2.add(jcb1);
            jPanel2.add(jcb2);
            jPanel2.add(jtf1);
            jPanel2.add(jtf2);
            jcb1.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"TO"}));
            jcb2.setModel(new javax.swing.DefaultComboBoxModel(toby));
            jcb1.setBounds(18, 10 + (row * 40), 57, 27);
            jcb2.setBounds(82, 10 + (row * 40), 240, 27);
            jtf1.setBounds(510, 10 + (row * 40), 100, 27);
            jtf2.setBounds(650, 10 + (row * 40), 100, 27);
            jcb1.setVisible(true);
            jcb2.setVisible(true);
            jtf1.setVisible(true);
            jtf2.setVisible(true);
            jtf1.setEnabled(true);
            jtf2.setEnabled(false);
            jcb1.requestFocus();

            jcb1.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {

                    int key = e.getKeyCode();
                    if (key == KeyEvent.VK_ENTER) {
                        jcb2.requestFocus();
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jDateChooser1.requestFocus();
                    }
                }
            });
            jcb2.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    try {
                        connection c = new connection();
                        Connection conn = c.cone();
                        Statement stmt = conn.createStatement();
                        String cbt = null;
                        String str = (String) jcb2.getSelectedItem();
//                        if (str.equals("") || (str == null)) {
                        if ((str == null)) {

                        } else {
                            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
                            while (rs.next()) {
                                baltypecd = rs.getString(2);
                                rcamntv = rs.getString(1);
//            jcl1.setText("CURR.BAL : -" );
                                jcl2.setText(rcamntv);
                                jcl3.setText(baltypecd);
                            }

                            stmt.close();
                            conn.close();
                            if (aLedgerNameCurrentBalMap.containsKey(str)) {
                                jcl2.setText(aLedgerNameCurrentBalMap.get(str).toString());
                                jcl3.setText(aLedgerNameCurrentBalMapt.get(str).toString());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            jcb2.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    int key1 = e.getKeyCode();
                    if (key == KeyEvent.VK_ENTER) {

                        if (jtf1.isEnabled()) {
                            jtf1.requestFocus();
                        } else {
                            jtf2.requestFocus();
                        }
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jcb1.requestFocus();
                    }
                    if (key1 == KeyEvent.VK_F2) {
                        reporting.accounts.createLedger create = new reporting.accounts.createLedger();
                        create.setVisible(true);
                    }
                }
            });
            jtf1.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER) {
                        count++;
                        balanceAmount += Integer.parseInt(jtf1.getText());
                        updateBal(jcb2.getSelectedItem().toString());
                        ++point1;
                        if (point1 == 1) {
                            aSecondRow = new SecondRow();
                            aRowNoRowObjectMap.put(row + 1, aSecondRow);
                        } else if (aSecondRow != null) {
                            aSecondRow.jToByComboBox.requestFocus();
                        }

                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jcb2.requestFocus();
                    }
                }
            });
            jtf2.addKeyListener(new java.awt.event.KeyAdapter() {
                @Override
                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER) {
                        atoby[count] = (String) jcb1.getSelectedItem();
                        count++;
                        ++point1;
                        if (point1 == 1) {
                            aSecondRow = new SecondRow();
                            aRowNoRowObjectMap.put(row + 1, aSecondRow);
                        } else if (aSecondRow != null) {
                            aSecondRow.jToByComboBox.requestFocus();
                            aSecondRow.jToByComboBox.setSelectedItem("BY");
                        }

                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jcb2.requestFocus();
                    }
                }
            });
            String sel = jcb1.getSelectedItem().toString();
            if (sel == "TO") {
                try {
                    connection c = new connection();
                    Connection conn = c.cone();
                    Statement st = conn.createStatement();
                    Statement st1 = conn.createStatement();
                    Statement st2 = conn.createStatement();

                    //rs = stmt.executeQuery("select ledger_name from ledger where groups IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS')") ;
                    ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups NOT IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND') order by ledger_name ASC");
                    while (rs2.next()) {
                        jcb2.addItem(rs2.getString(1));
                    }

                    ResultSet rs = st.executeQuery("select Name from Group_create where Under NOT IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')");
                    while (rs.next()) {
                        String gname = rs.getString(1);
                        ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='" + gname + "' order by ledger_name ASC ");
                        while (rs1.next()) {
                            jcb2.addItem(rs1.getString(1));
                        }
                    }
                    st.close();
                    st1.close();
                    st2.close();
                    conn.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                jcb2.setSelectedItem(null);
            }
        }

    }

    public void updateBal(String LedgerName) {
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + LedgerName + "'");
            String aBalTypeCd = "", aCurrentBal = "";
            while (rs.next()) {
                aCurrentBal = rs.getString(1);
                aBalTypeCd = rs.getString(2);
            }

            int aRowNo = 0;
            SecondRow aSecondRow = null;
            double result = Double.parseDouble(aCurrentBal);

            Iterator<Map.Entry<Integer, UpdateJournal>> entryIterator = updateJourn.entrySet().iterator();
            while (entryIterator.hasNext()) {
                Map.Entry<Integer, UpdateJournal> previousEntry = entryIterator.next();
                UpdateJournal previousJournal = previousEntry.getValue();

                System.out.println("The Ledger Name is " + LedgerName);
                if (!previousJournal.getDebitamnt().trim().isEmpty() && previousJournal.getToledger() != null && previousJournal.getToledger().equals(LedgerName)) {
                    System.out.println("Previous journal debit ledger name is " + previousJournal.getToledger());
                    System.out.println("Previous journal debit AMOUNT is " + previousJournal.getDebitamnt());

                    ResultSet rs1 = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + previousJournal.getToledger() + "'");

                    while (rs1.next()) {
                        result = Double.parseDouble(rs1.getString(1));
                        aBalTypeCd = rs1.getString(2);
                    }

                    double ramn = Double.parseDouble(previousJournal.getDebitamnt());
                    if (aBalTypeCd.equals("CR")) {
                        result = result - ramn;
                        if (result < 0) {
                            aBalTypeCd = "DR";
                        } else {
                            if (result > 0) {
                                aBalTypeCd = "CR";
                            }
                        }
                    } else {
                        if (aBalTypeCd.equals("DR")) {
                            result = result + ramn;
                            aBalTypeCd = "DR";
                        }
                    }

                } else if (previousJournal.getCreditamnt() != null && !previousJournal.getCreditamnt().trim().isEmpty() && previousJournal.getByledger() != null && previousJournal.getByledger().equals(LedgerName)) {
                    System.out.println("Previous journal credit ledger name is " + previousJournal.getByledger());
                    System.out.println("Previous journal credit AMOUNT is " + previousJournal.getCreditamnt());

                    ResultSet rs2 = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + previousJournal.getByledger() + "'");

                    while (rs2.next()) {
                        result = Double.parseDouble(rs2.getString(1));
                        aBalTypeCd = rs2.getString(2);
                    }

                    double ramn = Double.parseDouble(previousJournal.getCreditamnt());
                    if (aBalTypeCd.equals("DR")) {
                        result = result - ramn;
                        if (result < 0) {
                            aBalTypeCd = "CR";
                        } else if (result > 0) {
                            aBalTypeCd = "DR";
                        }
                    } else {
                        if (aBalTypeCd.equals("CR")) {
                            result = result + ramn;
                            aBalTypeCd = "CR";
                        }
                    }
                }
            }

            Iterator aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
            if (jcb2.getSelectedItem().toString().equals(LedgerName)) {
                double ramn = Double.parseDouble(jtf1.getText());
                if (aBalTypeCd.equals("CR")) {
                    result = result - ramn;
                    if (result < 0) {
                        aBalTypeCd = "DR";
                    } else {
                        if (result > 0) {
                            aBalTypeCd = "CR";
                        }
                    }
                } else {
                    if (aBalTypeCd.equals("DR")) {
                        result = result + ramn;
                        aBalTypeCd = "DR";
                    }
                }
            }
            result = Math.abs(result);
            while (aRowNoItertor.hasNext()) {
                aRowNo = (Integer) aRowNoItertor.next();
                aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                if (aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString().equals(LedgerName)) {
                    if (aSecondRow.jToByComboBox.getSelectedItem().toString().equals("TO")) {
                        double ramn = Double.parseDouble(aSecondRow.jDebitTextField.getText());
                        System.out.println(aBalTypeCd + "  " + result + "  " + ramn);
                        if (aBalTypeCd.equals("CR")) {
                            result = result - ramn;
                            if (result < 0) {
                                aBalTypeCd = "DR";
                            } else {
                                if (result > 0) {
                                    aBalTypeCd = "CR";
                                }
                            }
                        } else {
                            if (aBalTypeCd.equals("DR")) {
                                result = result + ramn;
                                aBalTypeCd = "DR";
                            }
                        }
                        System.out.println(result + "  " + ramn);
                    } else if (aSecondRow.jToByComboBox.getSelectedItem().toString().equals("BY")) {
                        //////////
                        double ramn = Double.parseDouble(aSecondRow.jCreditTextField.getText());
                        if (aBalTypeCd.equals("DR")) {
                            result = result - ramn;
                            if (result < 0) {
                                aBalTypeCd = "CR";
                            } else if (result > 0) {
                                aBalTypeCd = "DR";
                            }
                        } else {
                            if (aBalTypeCd.equals("CR")) {
                                result = result + ramn;
                                aBalTypeCd = "CR";
                            }
                        }
                        /////
                    }
                    result = Math.abs(result);
                }
            }
            aRowNoItertor = aRowNoRowObjectMap.keySet().iterator();
            while (aRowNoItertor.hasNext()) {
                aRowNo = (Integer) aRowNoItertor.next();
                aSecondRow = (SecondRow) aRowNoRowObjectMap.get(aRowNo);
                if (aSecondRow.jAccountNameCobmoBox.getSelectedItem().toString().equals(LedgerName)) {
                    aSecondRow.jBalenceLabel.setText("" + Math.abs(result));
                    aSecondRow.jBalenceTypeLabel.setText(aBalTypeCd);
                }
            }
            if (jcb2.getSelectedItem().toString().equals(LedgerName)) {
                jcl2.setText("" + Math.abs(result));
                jcl3.setText(aBalTypeCd);
            }
            jPanel2.repaint();
            aLedgerNameCurrentBalMap.put(LedgerName, "" + Math.abs(result));
            aLedgerNameCurrentBalMapt.put(LedgerName, aBalTypeCd);
            System.out.println(aLedgerNameCurrentBalMap);
        } catch (Exception aExc) {
            aExc.printStackTrace();
        }
    }

    public class SecondRow {

        int selected = 0;
        public JComboBox jToByComboBox, jAccountNameCobmoBox;
        public JTextField jDebitTextField, jCreditTextField;
        public SecondRow aNewRow = null;
        public boolean isRowAlreadyCreated = false;
        String baltypecd, rcamntv;
        public JLabel jNameLabel, jBalenceTypeLabel, jBalenceLabel;

        public SecondRow() {
            selected = ++row;
            jNameLabel = new JLabel();
            jBalenceLabel = new JLabel();
            jBalenceTypeLabel = new JLabel();
            jPanel2.add(jBalenceLabel);
            jPanel2.add(jBalenceTypeLabel);
//               jPanel2.add(jNameLabel);
            jBalenceTypeLabel.setVisible(true);
//               jNameLabel.setVisible(true);
            jBalenceLabel.setBounds(360, 10 + (row * 40), 800, 25);
            jBalenceTypeLabel.setBounds(410, 10 + (row * 40), 800, 25);
//             jNameLabel.setBounds(172, 10 + (row * 40), 800, 25);
            jToByComboBox = new JComboBox();
            jAccountNameCobmoBox = new JComboBox();
            jDebitTextField = new JTextField();
            jCreditTextField = new JTextField();
            jCreditTextField.setText("");
            //  jBalenceLabel = new JLabel();
            //  jPanel2.add(jBalenceLabel);
            jBalenceLabel.setVisible(true);
            // jBalenceLabel.setBounds(250, 10 + (row * 40), 800, 25);
            jPanel2.add(jToByComboBox);
            jPanel2.add(jAccountNameCobmoBox);
            jPanel2.add(jDebitTextField);
            jPanel2.add(jCreditTextField);
            jToByComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"TO", "BY"}));
            jAccountNameCobmoBox.setModel(new javax.swing.DefaultComboBoxModel(toby));
            jToByComboBox.setBounds(18, 10 + (row * 40), 57, 27);
            jAccountNameCobmoBox.setBounds(82, 10 + (row * 40), 240, 27);
            jDebitTextField.setBounds(510, 10 + (row * 40), 100, 27);
            jCreditTextField.setBounds(650, 10 + (row * 40), 100, 27);
            jToByComboBox.setVisible(true);
            jAccountNameCobmoBox.setVisible(true);
            //  if(a==0)
            {
                jAccountNameCobmoBox.setSelectedItem(null);
            }
            jDebitTextField.setVisible(true);
            jCreditTextField.setVisible(true);
            jDebitTextField.setEnabled(false);
            jCreditTextField.setEnabled(false);
            jToByComboBox.requestFocus();

            jToByComboBox.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    JComboBox aComboBox = (JComboBox) evt.getSource();
                    if (aComboBox.getSelectedIndex() == 0) {
                        if (balanceAmount < 0) {
                            jDebitTextField.setText(Math.abs(balanceAmount) + "");
                            jDebitTextField.setEnabled(true);
                        }
                        jDebitTextField.setEnabled(true);
                        jCreditTextField.setEnabled(false);
                    } else {
                        jDebitTextField.setEnabled(false);
                        if (balanceAmount > 0) {
                            jCreditTextField.setText(Math.abs(balanceAmount) + "");
                            jCreditTextField.setEnabled(true);
                        }
                        jCreditTextField.setEnabled(true);
                    }
                }
            });

            jToByComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {

                    int key = e.getKeyCode();
                    if (key == KeyEvent.VK_ENTER) {
                        jAccountNameCobmoBox.requestFocus();
                        if (jToByComboBox.getSelectedIndex() == 0) {
                            if (balanceAmount < 0) {
                                jDebitTextField.setText(Math.abs(balanceAmount) + "");
                                jDebitTextField.setEnabled(true);
                            }
                            jDebitTextField.setEnabled(true);
                            jCreditTextField.setEnabled(false);
                        } else {
                            jDebitTextField.setEnabled(false);
                            if (balanceAmount > 0) {
                                jCreditTextField.setText(Math.abs(balanceAmount) + "");
                                jCreditTextField.setEnabled(true);
                            }
                            jCreditTextField.setEnabled(true);
                        }

                    }

                    if (key == KeyEvent.VK_ESCAPE) {
                        if (jtf2.isEnabled()) {
                            jtf2.requestFocus();
                        } else {
                            jtf1.requestFocus();
                        }
                    }
                }
            });
            jToByComboBox.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    JComboBox aComboBox = (JComboBox) e.getSource();
                    String sel = aComboBox.getSelectedItem().toString();

                    if (sel == "TO") {
                        //   jAccountNameCobmoBox.removeAllItems();
                        try {
                            connection c = new connection();
                            Connection conn = c.cone();
                            Statement st = conn.createStatement();
                            Statement st1 = conn.createStatement();
                            Statement st2 = conn.createStatement();

                            //rs = stmt.executeQuery("select ledger_name from ledger where groups IN('BRANCH/DIVISION','CAPITAL ACCOUNT','CURRENT ASSESTS','CURRENT LIABLITIES','DEPOSITS(ASSEST)','DIRECT EXPENSES','DIRECT INCOME','DUTIES AND TAXES','EXPENSES (DIRECT)','EXPENSES (INDIRECT)','FIXED ASSESTS','INCOMCE(DIRECT)','INCOMCE(INDIRECT)','INDIRECT EXPENSES','INDIRECT INCOMES','INVESTMENTS','LOANS AND ADVANCES(ASSEST)','LOANS(LIABILITY)','MISC. EXPENSES(ASSEST)','PROVISIONS','PURCHASE ACCOUNTS','RESERVES AND SURPLUS','RETAINED EARNINGS,','SALES ACCOUNTS','SECURED LOANS','STOCK IN HAND','SUNDRY CREDITORS','SUNDRY DEBITORS','SUSPENSE ACCOUNTS','UNSECURED LOANS')") ;
                            ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups NOT IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')order by ledger_name ASC");
                            while (rs2.next()) {
                                jAccountNameCobmoBox.addItem(rs2.getString(1));
                            }

//                        ResultSet rs = st.executeQuery("select Name from Group_create where Under NOT IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')order by ledger_name ASC") ;
//			while(rs.next())
//			{
//                            String gname=rs.getString(1);
//                           ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='"+gname+"' ") ;
//                           while(rs1.next())
//                           {
//                            jAccountNameCobmoBox.addItem(rs1.getString(1)) ;
//                           }
//			}
                            st.close();
                            st1.close();
                            st2.close();
                            conn.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();;
                        }
//            if (a==0)
//            jAccountNameCobmoBox.setSelectedItem(null);
                    } else {
                        if (sel == "BY") {
                            // jAccountNameCobmoBox.removeAllItems();
                            try {
                                connection c = new connection();
                                Connection conn = c.cone();
                                Statement st = conn.createStatement();
                                Statement st1 = conn.createStatement();
                                Statement st2 = conn.createStatement();

                                ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups NOT IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')order by ledger_name ASC");
                                // ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups NOT IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')") ;
                                while (rs2.next()) {
                                    jAccountNameCobmoBox.addItem(rs2.getString(1));
                                }

                                ResultSet rs = st.executeQuery("select Name from Group_create where Under NOT IN('BANK ACCOUNTS','BANK OCC A/C','BANK OD A/C','CASH IN HAND')");
                                while (rs.next()) {
                                    String gname = rs.getString(1);
                                    ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='" + gname + "' order by ledger_name ASC ");
                                    while (rs1.next()) {
                                        jAccountNameCobmoBox.addItem(rs1.getString(1));
                                    }
                                }

                                st.close();
                                st1.close();
                                st2.close();
                                conn.close();
                            } catch (Exception ex) {
                                ex.printStackTrace();;
                            }
                            //   if(a==0)
                            //    jAccountNameCobmoBox.setSelectedItem(null);

                        }
                    }
                }
            });

            jAccountNameCobmoBox.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    try {
                        connection c = new connection();
                        Connection conn = c.cone();
                        Statement stmt = conn.createStatement();
                        String cbt = null;
                        String str = (String) jAccountNameCobmoBox.getSelectedItem();
                        ResultSet rs = stmt.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + str + "'");
                        while (rs.next()) {
                            baltypecd = rs.getString(2);
                            rcamntv = rs.getString(1);
//        jNameLabel.setText( "CURR.BAL : -  ");
                            jBalenceLabel.setText(rcamntv);
                            jBalenceTypeLabel.setText(baltypecd);
                        }
                        if (aLedgerNameCurrentBalMap.containsKey(str)) {
                            jBalenceLabel.setText(aLedgerNameCurrentBalMap.get(str).toString());
                            jBalenceTypeLabel.setText(aLedgerNameCurrentBalMapt.get(str).toString());
                        }
                        stmt.close();
                        conn.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            jAccountNameCobmoBox.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    int key1 = e.getKeyCode();
                    if (key == KeyEvent.VK_ENTER) {
                        String Str = (String) jAccountNameCobmoBox.getSelectedItem();
                        //   "".equals(Str)
                        if (jAccountNameCobmoBox.getSelectedIndex() == -1) {
                            jTextArea1.requestFocus();
                            System.out.println("code is running");

                        } else {
                            if (jDebitTextField.isEnabled()) {
                                jDebitTextField.requestFocus();
                            } else {
                                jCreditTextField.requestFocus();
                            }
                        }
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jToByComboBox.requestFocus();
                    }
                    if (key1 == KeyEvent.VK_F2) {
                        reporting.accounts.createLedger create = new reporting.accounts.createLedger();
                        create.setVisible(true);
                    }
                }
            });
            jDebitTextField.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER) {
                        atoby[count] = (String) jToByComboBox.getSelectedItem();
                        if (aaccnameto[0] == null) {
                            aaccnameto[0] = aaccnameto[count];
                        }
                        if (jToByComboBox.getSelectedItem().toString().equals("BY")) {

                            count++;
                        } else if (jToByComboBox.getSelectedItem().toString().equals("BY")) {
                            count++;
                        }
                        balanceAmount += Integer.parseInt(jDebitTextField.getText());
                        updateBal(jAccountNameCobmoBox.getSelectedItem().toString());
                        ++point2;
                        if (!isRowAlreadyCreated) {
                            aNewRow = new SecondRow();
                            aRowNoRowObjectMap.put(row + 1, aNewRow);
                            isRowAlreadyCreated = true;
                        } else if (aNewRow != null) {
                            aNewRow.jToByComboBox.requestFocus();
                        }
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jAccountNameCobmoBox.requestFocus();
                    }
                }
            });
            jCreditTextField.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent e) {
                    char key = e.getKeyChar();
                    if (key == KeyEvent.VK_ENTER) {
                        atoby[count] = (String) jToByComboBox.getSelectedItem();

                        if (aaccnameto[0] == null) {
                            aaccnameto[0] = aaccnameto[count];
                        }
                        balanceAmount -= Integer.parseInt(jCreditTextField.getText());
                        updateBal(jAccountNameCobmoBox.getSelectedItem().toString());
                        ++point2;
                        if (!isRowAlreadyCreated) {
                            aNewRow = new SecondRow();
                            aRowNoRowObjectMap.put(row + 1, aNewRow);
                            isRowAlreadyCreated = true;

                        } else if (aNewRow != null) {
                            aNewRow.jToByComboBox.requestFocus();

                        }

                        jTextArea1.requestFocus();
                    }
                    if (key == KeyEvent.VK_ESCAPE) {
                        jAccountNameCobmoBox.requestFocus();
                    }
                }
            });

        }
    }

    /**
     * This method iobs called from within the constructor to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton3 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : JOURNAL");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jTextField1.setEditable(false);

        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1122, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 538, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(jPanel2);

        jLabel2.setText("SNO. :");

        jLabel3.setText("DATE :");

        jLabel4.setText("PARTICULARS");

        jLabel5.setText("DEBIT");

        jLabel6.setText("CREDIT");

        jLabel1.setText("NARRATION :");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextArea1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextArea1);

        jButton3.setText("UPDATE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton3)
                                .addGap(29, 29, 29)
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(28, 28, 28)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel4))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(374, 374, 374)
                                        .addComponent(jLabel5)
                                        .addGap(31, 31, 31)
                                        .addComponent(jLabel6)
                                        .addGap(129, 129, 129))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(418, 418, 418)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton2)
                        .addComponent(jButton3)))
                .addContainerGap(78, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(825, 712));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    boolean issave = false;

    public void save() {
        int i1 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i1 == 0 && issave == false) {
            issave = true;
            PreparedStatement ps = null;
            String s_no = jTextField1.getText();
            String da_te = formatter.format(jDateChooser1.getDate());
            String narra_tion = jTextArea1.getText();
            try {
                connection c = new connection();
                Connection conneee = c.cone();
                Statement st1 = conneee.createStatement();

                Iterator it1 = aRowNoRowObjectMap.keySet().iterator();
                Iterator it2 = aRowNoRowObjectMap.keySet().iterator();
                int i = 1;

                String aledgernameby1 = null;

                while (it2.hasNext()) {
                    int aRowNo = (Integer) it2.next();
                    journ.SecondRow aSecondRow3 = (journ.SecondRow) aRowNoRowObjectMap.get(aRowNo);
                    if (aSecondRow3.jToByComboBox.getSelectedItem().toString().equals("BY")) {
                        aledgernameby1 = (String) aSecondRow3.jAccountNameCobmoBox.getSelectedItem();
                    }

                }

                while (it1.hasNext()) {

                    int aRowNo = (Integer) it1.next();
                    journ.SecondRow aSecondRow = (journ.SecondRow) aRowNoRowObjectMap.get(aRowNo);

                    String aledgernameto = null;
                    String aledgernameby = null;
                    if (aSecondRow.jToByComboBox.getSelectedItem().toString().equals("TO")) {
                        aledgernameto = (String) aSecondRow.jAccountNameCobmoBox.getSelectedItem();
                        aledgernameby = aledgernameby1;
                    }

                    if (aSecondRow.jToByComboBox.getSelectedItem().toString().equals("BY")) {
                        aledgernameto = (String) jcb2.getSelectedItem();
                        aledgernameby = (String) aSecondRow.jAccountNameCobmoBox.getSelectedItem();

                    }
                    if (jtf1.getText() == null) {
                        jtf1.setText("0");
                    }
                    if (jtf2.getText() == null) {
                        jtf2.setText("0");
                    }

                    if (i == 1) {
                        st1.executeUpdate("insert into journal values(" + i + ",'" + s_no + "','" + da_te + "','" + jcb2.getSelectedItem().toString() + "','" + aledgernameby + "','" + narra_tion + "','" + jtf1.getText() + "','" + jtf2.getText() + "','0') ");
                    }

//                 if (aSecondRow.jToByComboBox.equals("TO"))
//                 {
                    if ((aSecondRow.jDebitTextField.getText().equals("") && aSecondRow.jCreditTextField.equals(""))) {

                    } else {
                        st1.executeUpdate("insert into journal values(" + (++i) + ",'" + s_no + "','" + da_te + "','" + aledgernameto + "','" + aledgernameby + "','" + narra_tion + "','" + aSecondRow.jDebitTextField.getText() + "','" + aSecondRow.jCreditTextField.getText() + "','0') ");

                    }

                }

                Statement st = conneee.createStatement();
                Iterator it = aLedgerNameCurrentBalMap.keySet().iterator();
                while (it.hasNext()) {

                    String LedgerName = (String) (it.next());
                    String currbal1 = (String) aLedgerNameCurrentBalMap.get(LedgerName);
                    String baltype = (String) aLedgerNameCurrentBalMapt.get(LedgerName);
                    st.executeUpdate("update ledger set curr_bal='" + currbal1 + "',currbal_type='" + baltype + "'  where ledger_name = '" + LedgerName + "'");

                }

                //    st1.executeUpdate("update ledger set curr_bal='"+aSecondRow.jBalenceLabel.getText()+"' , currbal_type='"+jBalenceLabel.getText().toString()+"'  where ledger_name = '"+jAccountNameCobmoBox.getSelectedItem().toString()+"'");
                JOptionPane.showMessageDialog(null, "Inserted Sucessfully.");
                conneee.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            setVisible(false);
            dispose();
            count = 0;

        }

    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        save();
        accounting.transaction.journ jr = new accounting.transaction.journ();
        jr.setVisible(true);
        jr.jDateChooser1.setDate(jDateChooser1.getDate());


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed
    ArrayList<List<String>> oldValueList = new ArrayList<List<String>>();

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        try {
            connection c = new connection();
            Connection connect = c.cone();

            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement stFetch = connect.createStatement();

            List<String> alreadyUpdatedLedgerNameList = new ArrayList<String>();
            updateBal((String) jcb2.getSelectedItem());
            alreadyUpdatedLedgerNameList.add((String) jcb2.getSelectedItem());
            System.out.println("updateJourn  " + updateJourn);
            Iterator<Integer> noIterator = updateJourn.keySet().iterator();
            String debitledger = "";
            String creditledger = "";
            String debitamnt = "";
            String creditamnt = "";
            while (noIterator.hasNext()) {
                UpdateJournal previousledgers = updateJourn.get(noIterator.next());
                debitledger = previousledgers.getToledger();
                creditledger = previousledgers.getByledger();
                System.out.println("Credit ledger name is " + creditledger);
            }

            updateBal(creditledger);
            alreadyUpdatedLedgerNameList.add(creditledger);

            updateBal(debitledger);
            alreadyUpdatedLedgerNameList.add(debitledger);

            Iterator it = aLedgerNameCurrentBalMap.keySet().iterator();
            while (it.hasNext()) {
                String LedgerName = (String) (it.next());
                updateBal(LedgerName);
                alreadyUpdatedLedgerNameList.add(LedgerName);
            }

            st.executeUpdate("delete from journal where s_no='" + jTextField1.getText() + "'");
            save();

            String currBal = "", type = "";

            Iterator<Integer> vsnIterator = updateJourn.keySet().iterator();
            while (vsnIterator.hasNext()) {
                UpdateJournal previousRow = updateJourn.get(vsnIterator.next());
                String accountHolder = "";
                String debitAmount = previousRow.getDebitamnt();
                String creditAmount = previousRow.getCreditamnt();
                if (debitAmount != null && debitAmount.trim().length() > 0) {
                    accountHolder = previousRow.getToledger();
                } else if (creditAmount != null && creditAmount.trim().length() > 0) {
                    accountHolder = previousRow.getByledger();
                }
                if (!alreadyUpdatedLedgerNameList.contains(accountHolder)) {
                    ResultSet rs1 = stFetch.executeQuery("select curr_bal,currbal_type from ledger where ledger_name = '" + accountHolder + "'");
                    while (rs1.next()) {
                        currBal = rs1.getString(1);
                        type = rs1.getString(2);
                    }
                    double result = Double.parseDouble(currBal);
                    if (debitAmount.trim().length() > 0) {
                        double ramn = Double.parseDouble(debitAmount);

                        if (type.equals("DR")) {
                            result = result - ramn;
                            if (result < 0) {
                                type = "CR";
                            } else {
                                if (result > 0) {
                                    type = "DR";
                                }
                            }
                        } else {
                            if (type.equals("CR")) {
                                result = result + ramn;
                                type = "CR";
                            }
                        }
                    } else if (creditAmount.trim().length() > 0) {
                        double ramn = Double.parseDouble(creditAmount);
                        if (type.equals("DR")) {
                            result = result - ramn;
                            if (result < 0) {
                                type = "CR";
                            } else if (result > 0) {
                                type = "DR";
                            }
                        } else {
                            if (type.equals("CR")) {
                                result = result + ramn;
                                type = "CR";
                            }
                        }
                    }

                    result = Math.abs(result);

                    st.executeUpdate("Update ledger set curr_bal='" + currBal + "',currbal_type='" + type + "' where ledger_name='" + accountHolder + "'");

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//save();            
        JOptionPane.showMessageDialog(rootPane, "Entry Updated successfuly");
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        jDateChooser1.requestFocus();
    }//GEN-LAST:event_formKeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:

        jDateChooser1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jTextArea1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            jButton1.requestFocus();
        }

    }//GEN-LAST:event_jTextArea1KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:

        char key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            save();
            accounting.transaction.journ jr = new accounting.transaction.journ();
            jr.setVisible(true);
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(journ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(journ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(journ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(journ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new journ().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    public javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JTextArea jTextArea1;
    public javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
