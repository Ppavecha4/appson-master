package connection;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class connection {

    private Connection con;

    public connection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            System.err.println("Could not find the database driver");
        } catch (Exception e) {
            System.err.println("Could not connect to the database");
        }

    }

    public Connection cone() throws SQLException {

        con = DriverManager.getConnection("jdbc:mysql://localhost:3307/appson_master?useUnicode=true&characterEncoding=UTF-8", "root", "root");
        return con;
    }

    public void query(String sql, String branch) throws SQLException, FileNotFoundException, IOException {
        connection c = new connection();
        Connection con = c.cone();

        String sql1 = sql;
        System.out.println("" + sql1);

        String line = "";
        FileInputStream fin = new FileInputStream("data" + branch + ".sql");
        int i = 0;
        while ((i = fin.read()) != -1) {
            line += (char) i;
        }
        fin.close();
        FileOutputStream fout = new FileOutputStream("data" + branch + ".sql");
        if (line.equals("")) {
            line = sql;
        } else {
            line = line + "/t@#" + sql;
        }
        byte b[] = line.getBytes();//converting string into byte array  
        fout.write(b);
        fout.close();

    }

}
