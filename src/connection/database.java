package connection;



import java.io.IOException;

import static java.lang.Math.log;

import java.sql.Connection;

import java.sql.SQLException;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.logging.Level;

import java.util.logging.Logger;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GARV
 */

public class database 
{
   
      
        
     static  public boolean backupDataWithDatabase(String host, String port, String user, String password, String database, String backupPath) throws IOException, InterruptedException {
     boolean status = false;
      String dumpExePath="\"C:\\Program Files\\MySQL\\MySQL Server 5.1\\bin\\mysqldump\"";
            Process p = null;
 
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String filepath =database+"-"+dateFormat.format(date) + ".sql";
 
            String batchCommand = "";
            if (password != "") {
                //Backup with database
                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            } else {
                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            }
 
            Runtime runtime = Runtime.getRuntime();
            p = runtime.exec(batchCommand);
            int processComplete = p.waitFor();
 
            if (processComplete == 0) {
                status = true;
             
            } else {
                status = false;
               
            }
 
        
        return status;
    }


static public boolean restoreDatabase(String dbUserName, String dbPassword, String source) throws IOException, InterruptedException {
    String sqlpath="\"C:\\Program Files\\MySQL\\MySQL Server 5.1\\bin\\mysql\"";    
    boolean status = false;
        String[] executeCmd = new String[]{sqlpath, "--user=" + dbUserName, "--password=" + dbPassword, "-e", "source " + source};
 
 Process runtimeProcess;
        
 runtimeProcess = Runtime.getRuntime().exec(executeCmd);
 int processComplete = runtimeProcess.waitFor();
 
            if (processComplete == 0) {
                System.out.println("Backup restored successfully with " + source);
                status= true;
            } else {
              System.out.print("Could not restore the backup " + source);
               status= false;
            }
      
 
        return status;
 
    }

}

