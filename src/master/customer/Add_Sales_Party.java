package master.customer;

import connection.connection;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aashish
 */
public class Add_Sales_Party extends javax.swing.JFrame {

    /**
     * Creates new form Add_Sales_Party
     */
    Add_Sales_Party as;
    public int igst_radio;
    public boolean is_Update = false;
    String old_partyCode = "";

    private static Add_Sales_Party obj = null;

    public Add_Sales_Party() {
        initComponents();
        fillCombo();
        this.setLocationRelativeTo(null);
        jTextField2.requestFocus();
        jButton2.setVisible(false);
    }

    public static Add_Sales_Party getObj() {
        if (obj == null) {
            obj = new Add_Sales_Party();
        }
        return obj;
    }

//-----------Function for setting data in Combobox -----------------------------
    public void fillCombo() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement city_st = connect.createStatement();
            Statement purchase_st = connect.createStatement();
            ResultSet city_rs = city_st.executeQuery("SELECT * FROM city");
            while (city_rs.next()) {
                jComboBox1.addItem(city_rs.getString("city"));
                jComboBox2.addItem(city_rs.getString("state"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------ 
//-----------Function for setting Party Code and settin State Name--------------
    public void setPartyCode() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement set_code = connect.createStatement();
            Statement set_state = connect.createStatement();
            String citycode = null;
            String Slash = "/";
            ResultSet code_rs = set_code.executeQuery("SELECT  (party_code) from party_sales where city='" + (String) jComboBox1.getSelectedItem() + "' and branch_id='0' order by party_code Asc");
            int a = 0, b, d = 0;
            int i = 0;
            while (code_rs.next()) {
                if (i == 0) {
                    d = code_rs.getString("party_code").lastIndexOf("/");
                    i++;
                }
                b = Integer.parseInt(code_rs.getString("party_code").substring(d + 1));
                if (a < b) {
                    a = b;
                }
            }
            ResultSet state_rs = set_state.executeQuery("select * from city where city='" + (String) jComboBox1.getSelectedItem() + "'");
            while (state_rs.next()) {
                citycode = state_rs.getString("city_code");
                jComboBox2.setSelectedItem("" + state_rs.getString("state"));
            }
            String finalconc = "S/".concat(citycode).concat(Slash).concat(Integer.toString(a + 1));
            jTextField1.setText(finalconc);
            jTextField2.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------   
//---------Function For Checking Party Name exist or not------------------------
    public void checkName() {
        ArrayList ledger_name = new ArrayList();
        ArrayList party_name = new ArrayList();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement check_ledger = connect.createStatement();
            Statement check_party = connect.createStatement();
            ResultSet ledger_rs = check_ledger.executeQuery("Select ledger_name from ledger");
            while (ledger_rs.next()) {
                ledger_name.add(ledger_rs.getString("ledger_name"));
            }
            ResultSet party_rs = check_party.executeQuery("Select party_name from party_sales");
            while (party_rs.next()) {
                party_name.add(party_rs.getString("party_name"));
            }

            if (ledger_name.contains(jTextField2.getText()) || party_name.contains(jTextField2.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Party name all ready exist");
                jTextField2.requestFocus();
            } else {
                jTextField3.requestFocus();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//--------Funtion for saving purchase form entry--------------------------------
    public void saveData() {
        String ledger_name = jTextField2.getText();
        String alias = jTextField1.getText();
        String groups = "SUNDRY CREDITORS";
        String inv_affected = "";
        String recon_date = "";
        String percent = "";
        String method = "";
        String open_bal = jTextField16.getText();
        String bal_type = jComboBox3.getSelectedItem().toString();
        String curr_bal = jTextField16.getText();
        String currbal_type = jComboBox3.getSelectedItem().toString();
        String typeoftax = "";
        String kth = jTextField1.getText();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement save_st = connect.createStatement();
            save_st.executeUpdate("Insert into party_sales values ('" + jTextField1.getText() + "','" + jTextField2.getText() + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + jTextField5.getText() + "','" + jTextField6.getText() + "','" + jTextField7.getText() + "','" + (String) jComboBox1.getSelectedItem() + "','" + (String) jComboBox2.getSelectedItem() + "','" + jTextField8.getText() + "','" + igst_radio + "','" + jTextField9.getText() + "','" + jTextField11.getText() + "','" + jTextField19.getText() + "','" + jTextField12.getText() + "','" + jTextField13.getText() + "','" + jTextField10.getText() + "','" + jTextField14.getText() + "','" + jTextField15.getText() + "','" + jTextField18.getText() + "','" + jTextField16.getText() + "','" + (String) jComboBox3.getSelectedItem() + "','" + jTextField17.getText() + "', '0')");
            save_st.executeUpdate("Insert into ledger values ('" + ledger_name + "' , '" + alias + "' , '" + groups + "' , '" + inv_affected + "' , '" + recon_date + "' ,'" + percent + "'  ,'" + method + "'  , '" + open_bal + "' , '" + bal_type + "' , '" + curr_bal + "','" + currbal_type + "','" + typeoftax + "','" + kth + "','" + igst_radio + "','','1')");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//-----------Function for Updating Entry----------------------------------------
    public void updateData() {
        String newledgername = jTextField2.getText();
        String oldledgername = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement update_st = connect.createStatement();

            update_st.executeUpdate("Update party_sales set party_name = '" + jTextField2.getText() + "', mobile_no = '" + jTextField3.getText() + "', address1 = '" + jTextField4.getText() + "', address2 = '" + jTextField5.getText() + "', address3 = '" + jTextField6.getText() + "', address4 = '" + jTextField7.getText() + "', city = '" + (String) jComboBox1.getSelectedItem() + "', state = '" + (String) jComboBox2.getSelectedItem() + "', mark_down = '" + jTextField8.getText() + "', igst = '" + igst_radio + "', pan_no = '" + jTextField9.getText() + "', gstin_no = '" + jTextField11.getText() + "', whatsapp_no = '" + jTextField19.getText() + "', bank_account1 = '" + jTextField12.getText() + "', bank_name1 = '" + jTextField13.getText() + "', bank_ifsc1 = '" + jTextField10.getText() + "', bank_account2 = '" + jTextField14.getText() + "', bank_name2 = '" + jTextField15.getText() + "', bank_ifsc2 = '" + jTextField18.getText() + "', opening_bal = '" + jTextField16.getText() + "', bal_type = '" + (String) jComboBox3.getSelectedItem() + "', email_id = '" + jTextField17.getText() + "' where party_code = '" + old_partyCode + "' ");

            if (!oldledgername.equals(newledgername)) {
                /*For ledger */
                update_st.executeUpdate("update ledger set ledger_name='" + newledgername + "' where ledger_name='" + oldledgername + "'");
                update_st.executeUpdate("update ledger_details set ledger_name='" + newledgername + "' where ledger_name='" + oldledgername + "'");
                update_st.executeUpdate("update ledgerfinal set ledger_name='" + newledgername + "' where ledger_name='" + oldledgername + "'");

                /*For Bank payment and Bank Receipt */
                update_st.executeUpdate("update bankpayment set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update bankpayment set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update bankreceipt set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update bankreceipt set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");

                update_st.executeUpdate("update contra set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update contra set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update journal set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update journal set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");

                /*For Payment and Receipt */
                update_st.executeUpdate("update payment set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update payment set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update receipt set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update receipt set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");

                /*For To Bank */
                update_st.executeUpdate("update tobank set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update tobank set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update tobank set ba_nk='" + newledgername + "' where ba_nk='" + oldledgername + "'");

                /*For billing */
                update_st.executeUpdate("update billing set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update billing set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");
                update_st.executeUpdate("Update billing set sundrydebtorsaccount = '" + newledgername + "' where sundrydebtorsaccount = '" + oldledgername + "'");

                /*For billing1  */
                update_st.executeUpdate("update billing1 set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update billing1 set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("Update billing1 set sundrydebtorsaccount = '" + newledgername + "' where sundrydebtorsaccount = '" + oldledgername + "'");

                /*For SalesReturn  */
                update_st.executeUpdate("update salesreturn set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update salesreturn set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");

                /*For Stock table */
                update_st.executeUpdate("update stock set by_ledger='" + newledgername + "' where by_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update stock set to_ledger='" + newledgername + "' where to_ledger='" + oldledgername + "'");
                update_st.executeUpdate("update stock set supplier_name='" + newledgername + "' where supplier_name='" + oldledgername + "'");

                /*For Stock1  */
                update_st.executeUpdate("update stock1 set Supplier_name ='" + newledgername + "' where Supplier_name = '" + oldledgername + "'");
                update_st.executeUpdate("Update stock1 set to_ledger = '" + newledgername + "' where to_ledger = '" + oldledgername + "'");
                update_st.executeUpdate("Update stock1 set by_ledger = '" + newledgername + "' where by_ledger = '" + oldledgername + "'");

                /*For purchase Return dual gst */
                update_st.executeUpdate("Update purchasereturndualgst set customer_name = '" + newledgername + "' where customer_name = '" + oldledgername + "'");
                update_st.executeUpdate("Update purchasereturndualgst set to_ledger = '" + newledgername + "' where to_ledger = '" + oldledgername + "'");
                update_st.executeUpdate("Update purchasereturndualgst set by_ledger = '" + newledgername + "' where by_ledger = '" + oldledgername + "'");
                update_st.executeUpdate("Update purchasereturndualgst set sundrydebtorsaccount = '" + newledgername + "' where sundrydebtorsaccount = '" + oldledgername + "'");

                /*For purchaseReturndualgst1 */
                update_st.executeUpdate("Update purchasereturndualgst1 set customer_name = '" + newledgername + "' where customer_name = '" + oldledgername + "'");
                update_st.executeUpdate("Update purchasereturndualgst1 set to_ledger = '" + newledgername + "' where to_ledger = '" + oldledgername + "'");
                update_st.executeUpdate("Update purchasereturndualgst1 set by_ledger = '" + newledgername + "' where by_ledger = '" + oldledgername + "'");
                update_st.executeUpdate("Update purchasereturndualgst1 set sundrydebtorsaccount = '" + newledgername + "' where sundrydebtorsaccount = '" + oldledgername + "'");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel14 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jComboBox3 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField18 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jTextField19 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sales Party");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(224, 223, 223));

        jLabel1.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 52, 53));
        jLabel1.setText("Party Details -");

        jLabel2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel2.setText("Party Code -");

        jTextField1.setEditable(false);

        jLabel3.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel3.setText("Party Name -");

        jTextField2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField2FocusLost(evt);
            }
        });
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel4.setText("Mobile No -");

        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel5.setText("Address 1 -");

        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel6.setText("Address 2 -");

        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel7.setText("Address 3 -");

        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel8.setText("Address 4 -");

        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel9.setText("City -");

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel10.setText("State -");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ANDAMAN & NICOBAR ISLANDS", "ANDHRA PRADESH", "ARUNACHAL PRADESHASSAM", "BIHAR", "CHANDIGARH", "CHHATISGARH", "DADRA & NAGAR HAVELI", "DAMAN & DUE", "DELHI", "GOA", "GUJARAT", "HARYANA", "HIMACHAL PRADESH", "JAMMU & KASHMIR", "JHARKHAND", "KARNATAKA", "KERALA", "LAKSHADWEEP", "MADHYA PRADESH", "MAHARASTRA", "MANIPUR", "MEGHALAYA", "MIZORAM", "NAGALAND", "ORISSA", "PONDICHERRY", "PUNJAB", "RAJASTHAN", "SIKKIM", "TAMIL NADU", "TRIPURA", "UTTARAKHAND", "UTTAR PRADESH", "WEST BENGAL" }));
        jComboBox2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addGap(35, 35, 35)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField1)
                                    .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField3)
                                    .addComponent(jTextField4)
                                    .addComponent(jTextField5)
                                    .addComponent(jTextField6)
                                    .addComponent(jTextField7)
                                    .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jComboBox2, 0, 277, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );

        jPanel2.setBackground(new java.awt.Color(224, 223, 223));

        jLabel11.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 52, 53));
        jLabel11.setText("Accounts Details -");

        jLabel12.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel12.setText("Mark Down -");

        jTextField8.setText("0");
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel13.setText("IGST Applicable -");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jRadioButton1.setText("YES");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jRadioButton2.setText("NO");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel14.setText("PAN No -");

        jTextField9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField9KeyPressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel15.setText("GSTIN No -");

        jLabel16.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel16.setText("Whatsapp No -");

        jTextField11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField11KeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel17.setText("Bank A/C No -");

        jTextField12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField12KeyPressed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel18.setText("Name -");

        jTextField13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField13KeyPressed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel19.setText("Bank A/C No -");

        jTextField14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField14KeyPressed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel20.setText("Name -");

        jTextField15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField15KeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel21.setText("Opening Bal. -");

        jTextField16.setText("0");
        jTextField16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField16KeyPressed(evt);
            }
        });

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CR", "DR" }));
        jComboBox3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox3KeyPressed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(69, 66, 66));
        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(69, 66, 66));
        jButton2.setText("UPDATE");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jButton3.setForeground(new java.awt.Color(69, 66, 66));
        jButton3.setText("CANCLE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel22.setText("E-Mail Id -");

        jTextField17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField17KeyPressed(evt);
            }
        });

        jTextField10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField10KeyPressed(evt);
            }
        });

        jTextField18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField18KeyPressed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel23.setText("IFSC -");

        jLabel24.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel24.setText("IFSC -");

        jTextField19.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField19KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator2)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel12)
                            .addComponent(jLabel19)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox3, 0, 161, Short.MAX_VALUE)
                                .addGap(96, 96, 96))
                            .addComponent(jTextField17)
                            .addComponent(jTextField9)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField12, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                    .addComponent(jTextField14))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel18)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel20)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel23)
                                    .addComponent(jLabel24))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField18, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                                    .addComponent(jTextField10)))
                            .addComponent(jTextField11)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jTextField8)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel13)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton1)
                                .addGap(26, 26, 26)
                                .addComponent(jRadioButton2))
                            .addComponent(jTextField19)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton2)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField2.getText().isEmpty()) {
                checkName();
            } else if (jTextField2.getText().isEmpty()) {
                jTextField2.getText();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jButton3.requestFocus();
        }
    }//GEN-LAST:event_jTextField2KeyPressed

    private void jTextField2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField2FocusLost
        if (!jTextField2.getText().isEmpty()) {
            checkName();
        } else if (jTextField2.getText().isEmpty()) {
            jTextField2.getText();
        }
    }//GEN-LAST:event_jTextField2FocusLost

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField3.getText().isEmpty()) {
                String mobile = jTextField3.getText();
                if (!(mobile.matches("^[0-9 ]*+$") && (mobile.length() >= 10) && (mobile.length() < 11))) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Valide Mobile Number");
                    jTextField3.requestFocus();
                } else {
                    jTextField4.requestFocus();
                }
            } else if (jTextField3.getText().isEmpty()) {
                jTextField4.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField2.requestFocus();
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField4.getText().isEmpty()) {
                String address1 = jTextField4.getText();
                if (address1.length() > 20) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Maximum 20 Charecter");
                    evt.consume();
                    jTextField4.requestFocus();
                } else {
                    jTextField5.requestFocus();
                }
            } else if (jTextField4.getText().isEmpty()) {
                jTextField5.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField3.requestFocus();
        }
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField5.getText().isEmpty()) {
                String address2 = jTextField5.getText();
                if (address2.length() > 20) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Maximum 20 Charecter");
                    evt.consume();
                    jTextField5.requestFocus();
                } else {
                    jTextField6.requestFocus();
                }
            } else if (jTextField5.getText().isEmpty()) {
                jTextField6.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField6.getText().isEmpty()) {
                String address3 = jTextField6.getText();
                if (address3.length() > 20) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Maximum 20 Charecter");
                    evt.consume();
                    jTextField6.requestFocus();
                } else {
                    jTextField7.requestFocus();
                }
            } else if (jTextField6.getText().isEmpty()) {
                jTextField7.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField7.getText().isEmpty()) {
                String address4 = jTextField7.getText();
                if (address4.length() > 20) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Maximum 20 Charecter");
                    evt.consume();
                    jTextField7.requestFocus();
                } else {
                    jComboBox1.requestFocus();
                }
            } else if (jTextField7.getText().isEmpty()) {
                jComboBox1.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField6.requestFocus();
        }
    }//GEN-LAST:event_jTextField7KeyPressed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox2.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField7.requestFocus();
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jComboBox2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox2KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField8.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jComboBox2KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jRadioButton1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox2.requestFocus();
        }
    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTextField9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField9KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jRadioButton2.requestFocus();
        }
    }//GEN-LAST:event_jTextField9KeyPressed

    private void jTextField12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField12KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField13.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField19.requestFocus();
        }
    }//GEN-LAST:event_jTextField12KeyPressed

    private void jTextField11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField11KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField19.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField9.requestFocus();
        }
    }//GEN-LAST:event_jTextField11KeyPressed

    private void jTextField13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField13KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField10.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField12.requestFocus();
        }
    }//GEN-LAST:event_jTextField13KeyPressed

    private void jTextField14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField14KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField15.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField10.requestFocus();
        }
    }//GEN-LAST:event_jTextField14KeyPressed

    private void jTextField15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField15KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField18.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField14.requestFocus();
        }
    }//GEN-LAST:event_jTextField15KeyPressed

    private void jTextField16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField16KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField18.requestFocus();
        }
    }//GEN-LAST:event_jTextField16KeyPressed

    private void jComboBox3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox3KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField17.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField16.requestFocus();
        }
    }//GEN-LAST:event_jComboBox3KeyPressed

    private void jTextField17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField17KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox3.requestFocus();
        }
    }//GEN-LAST:event_jTextField17KeyPressed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        if(is_Update == false){ 
        setPartyCode();
        }
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jTextField10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField10KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField14.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }
    }//GEN-LAST:event_jTextField10KeyPressed

    private void jTextField18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField18KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField16.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField15.requestFocus();
        }
    }//GEN-LAST:event_jTextField18KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField2.getText().isEmpty()) {
                saveData();
                JOptionPane.showMessageDialog(rootPane, "Data Successfully Saved.");
                this.dispose();
                as = new Add_Sales_Party();
                as.setVisible(true);
            } else if (jTextField2.getText().isEmpty()) {
                JOptionPane.showMessageDialog(rootPane, "Please Enter Party Name.");
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (!jTextField2.getText().isEmpty()) {
            saveData();
            JOptionPane.showMessageDialog(rootPane, "Data Successfully Saved.");
            this.dispose();
            as = new Add_Sales_Party();
            as.setVisible(true);
        } else if (jTextField2.getText().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Party Name.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (is_Update == true) {
                updateData();
                JOptionPane.showMessageDialog(rootPane, "Data Successfully Updated.");
                 this.dispose();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jButton2KeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (is_Update == true) {
            updateData();
            JOptionPane.showMessageDialog(rootPane, "Data Successfully Updated.");
             this.dispose();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            System.exit(0);
        }
        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jButton3KeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        igst_radio = 1;
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        igst_radio = 0;
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jTextField19KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField19KeyPressed
       char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField9.getText().isEmpty()) {
                String mobile = jTextField9.getText();
                if (!(mobile.matches("^[0-9 ]*+$") && (mobile.length() >= 10) && (mobile.length() < 11))) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Valide Mobile Number");
                    jTextField9.requestFocus();
                } else {
                    jTextField12.requestFocus();
                }
            } else if (jTextField9.getText().isEmpty()) {
                jTextField12.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField19KeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Add_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Add_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Add_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Add_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Add_Sales_Party().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    public javax.swing.JComboBox<String> jComboBox1;
    public javax.swing.JComboBox<String> jComboBox2;
    public javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JRadioButton jRadioButton1;
    public javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    public javax.swing.JTextField jTextField17;
    public javax.swing.JTextField jTextField18;
    public javax.swing.JTextField jTextField19;
    public javax.swing.JTextField jTextField2;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    public javax.swing.JTextField jTextField6;
    public javax.swing.JTextField jTextField7;
    public javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
