package master.customer;

import connection.connection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Aashish
 */
public class Update_Sales_Party extends javax.swing.JFrame {
    /**
     * Creates new form Update_Purchase_Party
     */
    
    private static Update_Sales_Party obj = null;
    Add_Sales_Party as = new Add_Sales_Party();
    
    public Update_Sales_Party() {
        initComponents();
        ShowTableData();
        this.setLocationRelativeTo(null);
    }
    
// Creating Run time Single instance     
   public static Update_Sales_Party getObj(){
     if(obj == null){
      obj = new Update_Sales_Party();
     }
     return obj;
    }
   
// Getting value in jtable from party table
    private void ShowTableData() {
        try {
            DefaultTableModel dtm;
            dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            String sql = "SELECT * from Party_sales";
            connection c = new connection();
            Connection connect = c.cone();
            PreparedStatement pst = connect.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("mobile_no"), rs.getString("gstin_no")};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     // Searching Data by party name from party table
    public void searchData() {
        try {
            DefaultTableModel dtm;
            dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            String keyword = jTextField1.getText();
            if (keyword.trim().length() > 0) {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT  * FROM Party_sales where party_code LIKE '" + keyword + "%' || party_name LIKE '" + keyword + "%' || mobile_no LIKE '" + keyword + "%'");
                while (rs.next()) {
                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("mobile_no"), rs.getString("gstin_no")};
                    dtm.addRow(o);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
// Move Selected data of jtable for updation
    public void moveData() {
        try {
            int index = jTable1.getSelectedRow();
            as.setVisible(true);
            this.dispose();
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  * FROM Party_sales where party_code = '"+jTable1.getValueAt(index, 0)+"'");
            while(rs.next()){
            as.jTextField2.setText(""+rs.getString("party_name"));
            as.jTextField3.setText(""+rs.getString("mobile_no"));
            as.jTextField4.setText(""+rs.getString("address1"));
            as.jTextField5.setText(""+rs.getString("address2"));
            as.jTextField6.setText(""+rs.getString("address3"));
            as.jTextField7.setText(""+rs.getString("address4"));
            as.jComboBox1.setSelectedItem(rs.getString("city"));
            as.jComboBox2.setSelectedItem(rs.getString("state"));
            as.jTextField8.setText(""+rs.getString("mark_down"));
            if(rs.getBoolean("igst") == true){
            as.jRadioButton1.setSelected(true);
            }else if(rs.getBoolean("igst") == false){
            as.jRadioButton2.setSelected(true);
            }
            as.jTextField9.setText(""+rs.getString("pan_no"));
            as.jTextField11.setText(""+rs.getString("gstin_no"));
            as.jTextField19.setText(rs.getString("whatsapp_no"));
            as.jTextField12.setText(""+rs.getString("bank_account1"));
            as.jTextField13.setText(""+rs.getString("bank_name1"));
            as.jTextField10.setText(""+rs.getString("bank_ifsc1"));
            as.jTextField14.setText(""+rs.getString("bank_account2"));
            as.jTextField15.setText(""+rs.getString("bank_name2"));
            as.jTextField18.setText(""+rs.getString("bank_ifsc2"));
            as.jTextField16.setText(""+rs.getString("opening_bal"));
            as.jComboBox3.setSelectedItem(rs.getString("bal_type"));
            as.jTextField17.setText(""+rs.getString("email_id"));
            as.old_partyCode = rs.getString("party_code");
            }
            as.jTextField1.setText(""+jTable1.getValueAt(index, 0));
            as.jButton1.setVisible(false);
            as.jButton2.setVisible(true);
            as.jTextField3.requestFocus();
            as.is_Update = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("View Sales Party");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 52, 53));
        jLabel1.setText("Sales Party List -");

        jTextField1.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField1CaretUpdate(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 52, 53));
        jLabel2.setText("Search -");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Party Code ", "Party Name ", "City", "Mobile Number", "GSTIN Number"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 703, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField1CaretUpdate
        searchData();
        if(jTextField1.getText().isEmpty()){
            ShowTableData();
        }
    }//GEN-LAST:event_jTextField1CaretUpdate

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
          obj = null;
    }//GEN-LAST:event_formWindowClosed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        moveData();
    }//GEN-LAST:event_jTable1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Update_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Update_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Update_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Update_Sales_Party.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Update_Sales_Party().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
