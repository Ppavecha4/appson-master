package reporting.accounts;
//import Printing.HeaderAndFooter;

import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public final class BalanceSheet1 extends javax.swing.JFrame {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    static boolean isParticular = true;
    boolean isPeriodCase = false;
    int totalcr, totaldr, total, total1, totalcrperiod, totaldrperiod, totalperiod, total1period = 0;
    public int diff, totcr, totdr, totcrperiod, totdrperiod;
    static String command = null;
    public int difference = 0;
    period a = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("01-04-yyy");
    String dateNow1 = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyy-MM-dd");
    SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyy");
    String currentdate = formatter2.format(currentDate.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yyy");
    String dateNow = formatter1.format(currentDate.getTime());
    String getyear = yearnow.format(currentDate.getTime());
    DecimalFormat df = new DecimalFormat("0.00");

    DefaultTableModel dtm1, dtm2;

    private void addrow() {
        int tb1RowCount = jTable1.getRowCount();
        int tb2RowCount = jTable2.getRowCount();
        if (tb1RowCount == tb2RowCount) {

        } else if (tb1RowCount > tb2RowCount) {
            int addrow = tb1RowCount - tb2RowCount;
            for (int i = 0; i < addrow; i++) {
                Object o[] = {"", ""};
                // System.out.println("o is "+o[i]);
                dtm2.addRow(o);
            }
        } else {
            int addrow = tb2RowCount - tb1RowCount;
            for (int i = 0; i < addrow; i++) {
                Object o[] = {"", ""};
                dtm1.addRow(o);
            }
        }
    }

    private static BalanceSheet1 obj = null;
    boolean theIsPeriodCase;

    public BalanceSheet1() {
        a = new period();
        isPeriodCase = theIsPeriodCase;
        initComponents();
        this.setLocationRelativeTo(null);
        jLabel6.setText("" + currentdate);
        jScrollPane1.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        // the following statement binds the same BoundedRangeModel to both vertical scrollbars.
        jScrollPane1.getVerticalScrollBar().setModel(
                jScrollPane2.getVerticalScrollBar().getModel());
        dtm1 = (DefaultTableModel) jTable1.getModel();
        dtm2 = (DefaultTableModel) jTable2.getModel();
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });
        jTable2.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });

    }

//     public BalanceSheet1() {
////        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    public static BalanceSheet1 getObj() {
        if (obj == null) {
            obj = new BalanceSheet1();
        }
        return obj;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jTextField2 = new javax.swing.JTextField();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();

        jLabel3.setText("TOTAL :");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : BALANCESHEET");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel1.setMinimumSize(new java.awt.Dimension(810, 650));

        jTextField2.setEditable(false);
        jTextField2.setFont(new java.awt.Font("sansserif", 0, 13)); // NOI18N
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("sansserif", 0, 13)); // NOI18N
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jLabel1.setText("TOTAL :");

        jLabel5.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jLabel5.setText("TOTAL :");

        jLabel6.setFont(new java.awt.Font("sansserif", 1, 16)); // NOI18N
        jLabel6.setText("jLabel6");

        jLabel7.setFont(new java.awt.Font("sansserif", 1, 16)); // NOI18N
        jLabel7.setText("BALANCE SHEET AS ON DATE :-");

        jButton1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jButton1.setText("Print");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "LIABILITIES", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable1MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(150);
        }

        jTable2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ASSETS", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable2MousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setPreferredWidth(150);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(133, 133, 133)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 455, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addGap(3, 3, 3)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel1)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 786, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setSize(new java.awt.Dimension(802, 592));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        if (!isPeriodCase) {
            trial();
            double first = 0, second = 0;
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                String num = jTable1.getValueAt(i, 1).toString();
                first = first + Double.parseDouble(num);
                jTextField2.setText(df.format(first));
            }
            for (int i = 0; i < jTable2.getRowCount(); i++) {
                String num = jTable2.getValueAt(i, 1).toString();
                second = second + Double.parseDouble(num);
                jTextField1.setText(df.format(second));
            }
            addrow();

        } else {
            trial_periodpals();
        }
        double first;
        first = 0;
        double second = 0;
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            String num = jTable1.getValueAt(i, 1).toString();
            if (num.equals("")) {
                System.out.println("num" + num);
                num = "0";
            }
            first = first + Double.parseDouble(num);
            jTextField1.setText(df.format(first));
            System.out.println("num is : " + num);
            System.out.println("first is " + first);
        }
        for (int i = 0; i < jTable2.getRowCount(); i++) {
            String num = jTable2.getValueAt(i, 1).toString();

            if (num.equals("")) {
                System.out.println("num" + num);
                num = "0";
            }

            second = second + Double.parseDouble(num);
            System.out.println("num is : " + num);
            System.out.println("second is " + second);
            jTextField2.setText(df.format(second));
        }
        addrow();
    }//GEN-LAST:event_formWindowOpened

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        PalsPrint1 pp = new PalsPrint1();
        pp.printing();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MousePressed
        // TODO add your handling code here:
        int row = jTable1.getSelectedRow();
        if (evt.getClickCount() == 2) {
            command = jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString().trim();

            if (command.equals("CURRENT PROFIT")) {
              reporting.accounts.pals1 pal = new reporting.accounts.pals1();
//                reporting.accounts.pals1 pal = new reporting.accounts.pals1(isPeriodCase);
                pal.setVisible(true);
            } else if (command.equals("CASH AND BANK BALANCE")) {
                reporting.accounts.trailbalwithopen listpr = new reporting.accounts.trailbalwithopen();
                reporting.accounts.trailbalwithopen listpr1 = new reporting.accounts.trailbalwithopen();
                listpr.particular("CASH IN HAND");
                listpr1.particular("BANK ACCOUNTS");
                listpr.setVisible(true);
                listpr1.setVisible(true);
                // listpr.setVisible(true);
                // reporting.accounts.trialBalance listpr1 = new reporting.accounts.trialBalance(false,"BANK ACCOUNTS", isPeriodCase);
            } else if (command.equals("DIFF In TRIAL")) {

            } else {
                //reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);
                reporting.accounts.trailbalwithopen listpr = new reporting.accounts.trailbalwithopen();
                listpr.particular(command);
                listpr.setVisible(true);
            }
        }
    }//GEN-LAST:event_jTable1MousePressed

    private void jTable2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MousePressed
        // TODO add your handling code here:
        int row = jTable2.getSelectedRow();
        if (evt.getClickCount() == 2) {

            command = jTable2.getValueAt(jTable2.getSelectedRow(), 0).toString().trim();
            System.out.println("command is :" + command);
            if (command.equals("Current loss")) {
                reporting.accounts.pals pal = new reporting.accounts.pals(isPeriodCase);
                pal.setVisible(true);
            } else if (command.equals("CASH AND BANK BALANCE")) {
//         reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false,"CASH IN HAND", isPeriodCase);
//           reporting.accounts.trialBalance listpr1 = new reporting.accounts.trialBalance(false,"BANK ACCOUNTS", isPeriodCase);

                reporting.accounts.trailbalwithopen listpr1 = new reporting.accounts.trailbalwithopen();

                listpr1.particular("BANK ACCOUNTS");
                listpr1.setVisible(true);
                reporting.accounts.trailbalwithopen listpr = new reporting.accounts.trailbalwithopen();
                listpr.particular("CASH IN HAND");
                listpr.setVisible(true);

            } else if (command.equals("DIFF In TRIAL")) {

            } else {
//       reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);
                reporting.accounts.trailbalwithopen listpr = new reporting.accounts.trailbalwithopen();
                listpr.particular(jTable2.getValueAt(row, 0) + "");
                listpr.setVisible(true);
            }
        }
    }//GEN-LAST:event_jTable2MousePressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed
    double dr = 0;
    double cr = 0;
//double total8=0;
    /**
     * @param args the command line arguments
     */
    java.util.Date dateafter;
    java.util.Date datenow;

    public void trial() {
        int sales = 0;
        int purchase = 0;
        //FOR SALES
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rssales = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='SALES ACCOUNTS'");

            while (rssales.next()) {
                if (!rssales.getString(1).equals("0")) {
//                 
                    dr = dr + (int) Double.parseDouble(rssales.getString(1));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String date = getyear.concat("-04-01");

        try {
            connection c = new connection();
            try (Connection conn3 = c.cone()) {
                Statement stopening = conn3.createStatement();
                ResultSet rsopening = stopening.executeQuery("select sum(ra_te) from stockid where CAST(Da_te AS date) < '" + date + "' ");
                int opening = 0;
                while (rsopening.next()) {
                    opening = rsopening.getInt(1);
                }
                cr = cr + opening;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ///////// for closing stock        
        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid2");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid21");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //closing stock for branch-2
        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid22");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid23");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid24");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid241");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid242");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid25");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }

                int result = closing;
//  Object object1[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(object1);

                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//FOR PURCHASE
        try {
            connection c = new connection();
            try (Connection conn2 = c.cone()) {
                Statement stpurchase = conn2.createStatement();
                ResultSet rspurchase = stpurchase.executeQuery("select curr_bal,ledger_name from ledger where groups='PURCHASE ACCOUNTS' ");

                while (rspurchase.next()) {
                    
                    if (rspurchase.getDouble(1)!= 0) {
                        cr = (int) (cr + Double.parseDouble(rspurchase.getString(1)));
                    }
                    }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

//FOR OPENING STOCK
        try {
            dateafter = formatter1.parse(getyear + "-04-01");
            datenow = formatter1.parse(dateNow);
        } catch (ParseException ex) {
            Logger.getLogger(trailbalwithopen.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (datenow.before(dateafter)) {
            getyear = Integer.toString(Integer.parseInt(getyear) - 1);

        }

////stock out
        // for other closing stock
// Direct incomes
        int direct = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsdirectincome = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT INCOMES' ");

            while (rsdirectincome.next()) {
                //     direct=rsdirectincome.getInt(1);

                if (!rsdirectincome.getString(1).equals("0")) {
                    dr = (int) (dr + Double.parseDouble(rsdirectincome.getString(1)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Direct Expense
        int directexpense = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsdirectexpense = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT EXPENSES' ");

            while (rsdirectexpense.next()) {
                if (!rsdirectexpense.getString(1).equals("0")) {
//             list1.add(""+rsdirectexpense.getString(2));
//             list2.add(""+rsdirectexpense.getString(1));
                    cr = (int) (cr + Double.parseDouble(rsdirectexpense.getString(1)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        double diffss = 0;

        double result = dr - cr;
        System.out.println("result" + result);
        if (cr >= dr) {

            diffss = cr - dr;
            total = (int) (diffss + dr);
            totalcr = (int) diffss;
        } else {

            diffss = dr - cr;
            double profitpercent = (diffss / sales) * 100;
            String profitpercent1 = df.format(profitpercent);

            totaldr = (int) diffss;
            total = (int) (diffss + cr);

        }

        if (cr >= dr) {

            diffss = cr - dr;

            totalcr = (int) diffss;

        } else {

            diffss = dr - cr;
            double profitpercent = (diffss / sales) * 100;
            String profitpercent1 = df.format(profitpercent);

            totaldr = (int) diffss;

        }

        //indirect incomes   
        int indirect = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsindirectincome = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='INDIRECT INCOMES' ");

            while (rsindirectincome.next()) {
                if (!rsindirectincome.getString(1).equals("0")) {

                    totaldr = (int) (totaldr + Double.parseDouble(rsindirectincome.getString(1)));
                }
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //indirect expense   
        int indirectexpenses = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsindirectexpenses = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='INDIRECT EXPENSES' ");

            while (rsindirectexpenses.next()) {
                if (!rsindirectexpenses.getString(1).equals("0")) {
                    totalcr = (int) (totalcr + Double.parseDouble(rsindirectexpenses.getString(1)));
                }
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double balancesheetcr = 0;
        double balancesheetdr = 0;

        if (totaldr > totalcr) {
            double netprofit = (double) totaldr - totalcr;

            Object o[] = {" CURRENT PROFIT", df.format(netprofit)};
            dtm1.addRow(o);
            balancesheetcr = balancesheetcr + netprofit;

        }
        if (totalcr > totaldr) {
            double netloss = (double) totalcr - totaldr;

            System.out.println("netLoss " + df.format(netloss));
            Object o1[] = {"CURRENT LOSS  ", df.format(netloss)};
            dtm2.addRow(o1);
            balancesheetdr = balancesheetdr + netloss;
        }

        double resultcapital = 0;
        double capitalcr = 0;
        double capitaldr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stcapital = conn1.createStatement();
            ResultSet rscapital = stcapital.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='CAPITAL ACCOUNT' group by currbal_type ");

            while (rscapital.next()) {
                if (!rscapital.getString(1).equals("0")) {
                    if (rscapital.getString(2).equals("DR")) {

                        capitaldr = Long.parseLong(rscapital.getString(1));
                    }
                    if (rscapital.getString(2).equals("CR")) {

                        capitalcr = Long.parseLong(rscapital.getString(1));

                    }

                }
            }
            if (capitalcr > capitaldr) {
                resultcapital = capitalcr - capitaldr;
//   list1.add("CAPITAL ACCOUNT");
//   list2.add(""+resultcapital);   
                Object o[] = {"CAPITAL ACCOUNT", df.format(resultcapital)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultcapital;
            } else if (capitaldr > capitalcr) {
                resultcapital = capitaldr - capitalcr;
//   list3.add("CAPITAL ACCOUNT");
//   list4.add(""+resultcapital);   

                Object o1[] = {"CAPITAL ACCOUNT", df.format(resultcapital)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultcapital;
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

/// duties start 
        double resultduties = 0;
        double dutiescr = 0;
        double dutiesdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stduties = conn1.createStatement();
            ResultSet rsduties = stduties.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='DUTIES AND TAXES' group by currbal_type ");

            while (rsduties.next()) {
                if (!rsduties.getString(1).equals("0")) {
                    if (rsduties.getString(2).equals("DR")) {

                        dutiesdr = Double.parseDouble(rsduties.getString(1));
                        Object o1[] = {"DUTIES AND TAXES", df.format(dutiesdr)};
                        dtm2.addRow(o1);
                        balancesheetdr = balancesheetdr + dutiesdr;

                    }
                    if (rsduties.getString(2).equals("CR")) {

                        dutiescr = Double.parseDouble(rsduties.getString(1));
                        Object o[] = {"DUTIES AND TAXES", df.format(dutiescr)};
                        dtm1.addRow(o);
                        balancesheetcr = balancesheetcr + dutiescr;

                    }

                }
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

// duties End   
        double resultKTH = 0;
        double KTHcr = 0;
        double KTHdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stkth = conn1.createStatement();
            ResultSet rskth = stkth.executeQuery("select sum(curr_bal),currbal_type ledger_name from ledger where groups='KTH' group by currbal_type ");

            while (rskth.next()) {
                if (!rskth.getString(1).equals("0")) {
                    if (rskth.getString(2).equals("DR")) {

                        KTHdr = Long.parseLong(rskth.getString(1));
                    }
                    if (rskth.getString(2).equals("CR")) {

                        KTHcr = Long.parseLong(rskth.getString(1));

                    }

                }

            }

            if (KTHcr > KTHdr) {
                resultKTH = KTHcr - KTHdr;
//   list1.add("KTH");
//   list2.add(""+resultKTH);   
                Object o[] = {"KTH", df.format(resultKTH)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultKTH;
            } else if (KTHdr > KTHcr) {
                resultKTH = KTHdr - KTHcr;
//   list3.add("KTH");
//   list4.add(""+resultKTH);   

                Object o1[] = {"KTH", df.format(resultKTH)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultKTH;
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long SCR = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stSCR = conn1.createStatement();
            ResultSet rsSCR = stSCR.executeQuery("select curr_bal,ledger_name from ledger where groups='SUNDRY CREDITORS' and currbal_type='CR' ");

            while (rsSCR.next()) {
                if (!rsSCR.getString(1).equals("0")) {

                    SCR = SCR + rsSCR.getLong(1);

                }

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long SCR1 = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stSCR1 = conn1.createStatement();
            ResultSet rsSCR1 = stSCR1.executeQuery("select curr_bal,ledger_name from ledger where groups='SUNDRY CREDITORS' and currbal_type='DR' ");

            while (rsSCR1.next()) {
                if (!rsSCR1.getString(1).equals("0")) {

                    SCR1 = SCR1 + rsSCR1.getLong(1);

                }

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long resultSCR = 0;
        if (SCR > SCR1) {
            resultSCR = SCR - SCR1;
            Object o[] = {"SUNDRY CREDITORS", df.format(resultSCR)};
            dtm1.addRow(o);
            balancesheetcr = balancesheetcr + resultSCR;
        } else if (SCR1 > SCR) {
            resultSCR = SCR1 - SCR;
            Object o1[] = {"SUNDRY CREDITORS", df.format(resultSCR)};
            dtm2.addRow(o1);
            balancesheetdr = balancesheetdr + resultSCR;
        }

        double resultSCRfamily = 0;
        double SCRfamilycr = 0;
        double SCRfamilydr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stSCRfamily = conn1.createStatement();
            ResultSet rsSCRfamily = stSCRfamily.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='SUNDRY CREDITORS FAMILY AND RELATIVE' group by currbal_type ");

            while (rsSCRfamily.next()) {
                if (!rsSCRfamily.getString(1).equals("0")) {
//                   
                    if (rsSCRfamily.getString(2).equals("DR")) {

                        SCRfamilydr = Long.parseLong(rsSCRfamily.getString(1));
                    }
                    if (rsSCRfamily.getString(2).equals("CR")) {

                        SCRfamilycr = Long.parseLong(rsSCRfamily.getString(1));

                    }

                }

            }

            if (SCRfamilycr > SCRfamilydr) {
                resultSCRfamily = SCRfamilycr - SCRfamilydr;
                Object o[] = {"SUNDRY CREDITORS FAMILY & RELATIVE", df.format(resultSCRfamily)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultSCRfamily;
            } else if (SCRfamilydr > SCRfamilycr) {
                resultSCRfamily = SCRfamilydr - SCRfamilycr;

                Object o1[] = {"SUNDRY CREDITORS FAMILY & RELATIVE", (resultSCRfamily)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultSCRfamily;
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultbankocc = 0;
        double bankocccr = 0;
        double bankoccdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stbankocc = conn1.createStatement();
            ResultSet rsbankocc = stbankocc.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='BANK OCC A/C' group by currbal_type ");

            while (rsbankocc.next()) {
                if (!rsbankocc.getString(1).equals("0")) {
                    if (rsbankocc.getString(2).equals("DR")) {

                        bankoccdr = Long.parseLong(rsbankocc.getString(1));
                    }
                    if (rsbankocc.getString(2).equals("CR")) {

                        bankocccr = Long.parseLong(rsbankocc.getString(1));

                    }

                }

            }

            if (bankocccr > bankoccdr) {
                resultbankocc = bankocccr - bankoccdr;

                Object o[] = {"BANK OCC A/C", df.format(resultbankocc)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultbankocc;
            } else if (bankoccdr > bankocccr) {
                resultbankocc = bankoccdr - bankocccr;
                Object o1[] = {"BANK OCC A/C", df.format(resultbankocc)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultbankocc;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultfixedasset = 0;
        double fixedassetcr = 0;
        double fixedassetdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stfixedasset = conn1.createStatement();
            ResultSet rsfixedasset = stfixedasset.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='FIXED ASSESTS' group by currbal_type");

            while (rsfixedasset.next()) {
                if (!rsfixedasset.getString(1).equals("0")) {

                    if (rsfixedasset.getString(2).equals("DR")) {

                        fixedassetdr = Double.parseDouble(rsfixedasset.getString(1));
                    }
                    if (rsfixedasset.getString(2).equals("CR")) {

                        fixedassetcr = Double.parseDouble(rsfixedasset.getString(1));

                    }

                }

            }

            if (fixedassetcr > fixedassetdr) {
                resultfixedasset = fixedassetcr - fixedassetdr;
                Object o[] = {"FIXED ASSESTS", df.format(resultfixedasset)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultfixedasset;
            } else if (fixedassetdr > fixedassetcr) {
                resultfixedasset = fixedassetdr - fixedassetcr;

                Object o1[] = {"FIXED ASSESTS", df.format(resultfixedasset)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultfixedasset;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultsundrydebtors = 0;
        double sundrydebtorscr = 0;
        double sundrydebtorsdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsundrydebtors = conn1.createStatement();
            ResultSet rssundrydebtors = stsundrydebtors.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='SUNDRY DEBITORS' group by currbal_type");

            while (rssundrydebtors.next()) {
                if (!rssundrydebtors.getString(1).equals("0")) {
                    if (rssundrydebtors.getString(2).equals("DR")) {

                        sundrydebtorsdr = Long.parseLong(rssundrydebtors.getString(1));
                    }
                    if (rssundrydebtors.getString(2).equals("CR")) {

                        sundrydebtorscr = Long.parseLong(rssundrydebtors.getString(1));

                    }
                }

            }

            if (sundrydebtorscr > sundrydebtorsdr) {
                resultsundrydebtors = sundrydebtorscr - sundrydebtorsdr;

                Object o[] = {"SUNDRY DEBITORS", df.format(resultsundrydebtors)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultsundrydebtors;
            } else if (sundrydebtorsdr > sundrydebtorscr) {
                resultsundrydebtors = sundrydebtorsdr - sundrydebtorscr;

                Object o1[] = {"SUNDRY DEBITORS", df.format(resultsundrydebtors)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultsundrydebtors;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultsundrydebtorsretail = 0;
        double sundrydebtorsretailcr = 0;
        double sundrydebtorsretaildr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsundrydebtorsretail = conn1.createStatement();
            ResultSet rssundrydebtorsretail = stsundrydebtorsretail.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='SUNDRY DEBTORS RETAIL' group by currbal_type ");

            while (rssundrydebtorsretail.next()) {
                if (!rssundrydebtorsretail.getString(1).equals("0")) {
                    if (rssundrydebtorsretail.getString(2).equals("DR")) {

                        sundrydebtorsretaildr = Long.parseLong(rssundrydebtorsretail.getString(1));
                    }
                    if (rssundrydebtorsretail.getString(2).equals("CR")) {

                        sundrydebtorsretailcr = Long.parseLong(rssundrydebtorsretail.getString(1));

                    }
                }

            }

            if (sundrydebtorsretailcr > sundrydebtorsretaildr) {
                resultsundrydebtorsretail = sundrydebtorsretailcr - sundrydebtorsretaildr;
//   list1.add("SUNDRY DEBTORS RETAIL");
//   list2.add(""+resultsundrydebtorsretail);   

                Object o[] = {"SUNDRY DEBTORS RETAIL", df.format(resultsundrydebtorsretail)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultsundrydebtorsretail;
            } else if (sundrydebtorsretaildr > sundrydebtorsretailcr) {
                resultsundrydebtorsretail = sundrydebtorsretaildr - sundrydebtorsretailcr;
//   list3.add("SUNDRY DEBTORS RETAIL");
//   list4.add(""+resultsundrydebtorsretail);   

                Object o1[] = {"SUNDRY DEBTORS RETAIL", df.format(resultsundrydebtorsretail)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultsundrydebtorsretail;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultdepositeasset = 0;
        double depositeassetcr = 0;
        double depositeassetdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stdepositeasset = conn1.createStatement();
            ResultSet rsdepositeasset = stdepositeasset.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='DEPOSITS(ASSEST)' group by currbal_type ");

            while (rsdepositeasset.next()) {
                if (!rsdepositeasset.getString(1).equals("0")) {
                    if (rsdepositeasset.getString(2).equals("DR")) {

                        depositeassetdr = Long.parseLong(rsdepositeasset.getString(1));
                    }
                    if (rsdepositeasset.getString(2).equals("CR")) {

                        depositeassetcr = Long.parseLong(rsdepositeasset.getString(1));

                    }
                }

            }

            if (depositeassetcr > depositeassetdr) {
                resultdepositeasset = depositeassetcr - depositeassetdr;
//   list1.add("DEPOSITS(ASSEST)");
//   list2.add(""+resultdepositeasset);   

                Object o[] = {"DEPOSITS(ASSEST)", df.format(resultdepositeasset)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultdepositeasset;
            } else if (depositeassetdr > depositeassetcr) {
                resultdepositeasset = depositeassetdr - depositeassetcr;
//   list3.add("DEPOSITS(ASSEST)");
//   list4.add(""+resultdepositeasset);   

                Object o1[] = {"DEPOSITS(ASSEST)", df.format(resultdepositeasset)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultdepositeasset;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultbranchdiv = 0;
        double branchdivcr = 0;
        double branchdivdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stbranchdiv = conn1.createStatement();
            ResultSet rsbranchdiv = stbranchdiv.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='BRANCH/DIVISION' group by currbal_type");

            while (rsbranchdiv.next()) {
                if (!rsbranchdiv.getString(1).equals("0")) {
                    if (rsbranchdiv.getString(2).equals("DR")) {

                        branchdivdr = Long.parseLong(rsbranchdiv.getString(1));
                    }
                    if (rsbranchdiv.getString(2).equals("CR")) {

                        branchdivcr = Long.parseLong(rsbranchdiv.getString(1));

                    }

                }

            }

            if (branchdivcr > branchdivdr) {
                resultbranchdiv = branchdivcr - branchdivdr;
//   list1.add("BRANCH/DIVISION");
//   list2.add(""+resultbranchdiv);   

                Object o[] = {"BRANCH/DIVISION", df.format(resultbranchdiv)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultbranchdiv;
            } else if (branchdivdr > branchdivcr) {
                resultbranchdiv = branchdivdr - branchdivcr;
//   list3.add("BRANCH/DIVISION");
//   list4.add(""+resultbranchdiv);   

                Object o1[] = {"BRANCH/DIVISION", df.format(resultbranchdiv)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultbranchdiv;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultcurrentasset = 0;
        double currentassetcr = 0;
        double currentassetdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stcurrentasset = conn1.createStatement();
            ResultSet rscurrentasset = stcurrentasset.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='CURRENT ASSESTS' group by currbal_type ");

            while (rscurrentasset.next()) {
                if (rscurrentasset.getString(2).equals("DR")) {

                    currentassetdr = Long.parseLong(rscurrentasset.getString(1));
                }
                if (rscurrentasset.getString(2).equals("CR")) {

                    currentassetcr = Long.parseLong(rscurrentasset.getString(1));

                }

            }
            if (currentassetcr > currentassetdr) {
                resultcurrentasset = currentassetcr - currentassetdr;
//   list1.add("CURRENT ASSESTS");
//   list2.add(""+resultcurrentasset);   

                Object o[] = {"CURRENT ASSESTS", df.format(resultcurrentasset)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultcurrentasset;
            } else if (currentassetdr > currentassetcr) {
                resultcurrentasset = currentassetdr - currentassetcr;
//   list3.add("CURRENT ASSEST");
//   list4.add(""+resultcurrentasset);   

                Object o1[] = {"CURRENT ASSEST", df.format(resultcurrentasset)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultcurrentasset;

            }
            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultbank = 0;
        double bankcr = 0;
        double bankdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stbank = conn1.createStatement();
            ResultSet rsbank = stbank.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='BANK ACCOUNTS' or groups='CASH IN HAND' group by currbal_type");

            while (rsbank.next()) {
                if (!rsbank.getString(1).equals("0")) {
                    if (rsbank.getString(2).equals("DR")) {
                        bankdr = Double.parseDouble(rsbank.getString(1));
                    }
                    if (rsbank.getString(2).equals("CR")) {
                        bankcr = Double.parseDouble(rsbank.getString(1));
                    }

//                   list1.add(""+rsindirectexpenses.getString(2));
//                   list2.add(""+rsindirectexpenses.getString(1));
                }

            }

            if (bankcr > bankdr) {
                resultbank = bankcr - bankdr;
//   list1.add("CASH AND BANK BALANCE");
//   list2.add(""+resultbank);   

                Object o[] = {"CASH AND BANK BALANCE", df.format(resultbank)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultbank;
            } else if (bankdr > bankcr) {
                resultbank = bankdr - bankcr;
//   list3.add("CASH AND BANK BALANCE");
//   list4.add(""+resultbank);   

                Object o1[] = {"CASH AND BANK BALANCE", df.format(resultbank)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultbank;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultcurrentliab = 0;
        double currentliabcr = 0;
        double currentliabdr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stcurrentliab = conn1.createStatement();
            ResultSet rscurrentliab = stcurrentliab.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='CURRENT LIABLITIES' group by currbal_type");

            while (rscurrentliab.next()) {
                if (!rscurrentliab.getString(1).equals("0")) {
                    if (rscurrentliab.getString(2).equals("DR")) {
                        currentliabdr = Long.parseLong(rscurrentliab.getString(1));
                    }
                    if (rscurrentliab.getString(2).equals("CR")) {
                        currentliabcr = Long.parseLong(rscurrentliab.getString(1));
                    }

//                   list1.add(""+rsindirectexpenses.getString(2));
//                   list2.add(""+rsindirectexpenses.getString(1));
                }

            }

            if (currentliabcr > currentliabdr) {
                resultcurrentliab = currentliabcr - currentliabdr;
//   list1.add("CURRENT LIABLITIES");
//   list2.add(""+resultcurrentliab);   

                Object o[] = {"CURRENT LIABLITIES", df.format(resultcurrentliab)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultcurrentliab;
            } else if (currentliabdr > currentliabcr) {
                resultcurrentliab = currentliabdr - currentliabcr;
//   list3.add("CURRENT LIABLITIES");
//   list4.add(""+resultcurrentliab);   

                Object o1[] = {"CURRENT LIABLITIES", df.format(resultcurrentliab)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultcurrentliab;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        double resultprovision = 0;
        double provisioncr = 0;
        double provisiondr = 0;

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stprovision = conn1.createStatement();
            ResultSet rsprovision = stprovision.executeQuery("select sum(curr_bal),currbal_type from ledger where groups='PROVISION FOR TAX' group by currbal_type");

            while (rsprovision.next()) {
                if (!rsprovision.getString(1).equals("0")) {
                    if (rsprovision.getString(2).equals("DR")) {
                        provisiondr = Long.parseLong(rsprovision.getString(1));
                    }
                    if (rsprovision.getString(2).equals("CR")) {
                        provisioncr = Long.parseLong(rsprovision.getString(1));
                    }

//                   list1.add(""+rsindirectexpenses.getString(2));
//                   list2.add(""+rsindirectexpenses.getString(1));
                }

            }

            if (provisioncr > provisiondr) {
                resultprovision = provisioncr - provisiondr;
//   list1.add("PROVISION FOR TAX");
//   list2.add(""+resultprovision);   

                Object o[] = {"PROVISION FOR TAX", df.format(resultprovision)};
                dtm1.addRow(o);
                balancesheetcr = balancesheetcr + resultprovision;
            } else if (provisiondr > provisioncr) {
                resultprovision = provisiondr - provisioncr;
//   list3.add("PROVISION FOR TAX");
//   list4.add(""+resultprovision);   

                Object o1[] = {"PROVISION FOR TAX", df.format(resultprovision)};
                dtm2.addRow(o1);
                balancesheetdr = balancesheetdr + resultprovision;

            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

//
//   long closintstock1=4849996;
////   list3.add("CLOSING STOCK -1");
////   list4.add(""+closintstock1);        
//    
//   Object o1[]={"CLOSING STOCK -1",closintstock1};
//                                    dtm2.addRow(o1);
//    balancesheetdr=balancesheetdr+closintstock1;
//    
//     long closintstock2=648736;
////   list3.add("CLOSING STOCK -2");
////   list4.add(""+closintstock2);        
//   
//   Object o11[]={"CLOSING STOCK -2",closintstock2};
//                                    dtm2.addRow(o11);
//    balancesheetdr=balancesheetdr+closintstock2;
//    
//      long closintstock3=448362;
////   list3.add("CLOSING STOCK -3");
////   list4.add(""+closintstock3);        
//    
//   Object o111[]={"CLOSING STOCK -3",closintstock3};
//                                    dtm2.addRow(o111);
//    balancesheetdr=balancesheetdr+closintstock3;
//    
//      long closintstock4=2383984;
////   list3.add("CLOSING STOCK -4");
////   list4.add(""+closintstock4);        
//    
//   Object o4[]={"CLOSING STOCK -4",closintstock4};
//                                    dtm2.addRow(o4);
//    balancesheetdr=balancesheetdr+closintstock4;
//    
//      long closintstock5=7374537;
////   list3.add("CLOSING STOCK -5");
////   list4.add(""+closintstock5);        
//    
//   Object o5[]={"CLOSING STOCK -5",closintstock5};
//                                    dtm2.addRow(o5);
//    balancesheetdr=balancesheetdr+closintstock5;
        double closintstock = 0;
//   list3.add("CLOSING STOCK ");
//   list4.add(""+closintstock);        

        connection c = new connection();
        Connection conn1;
        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid2");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);
                System.out.println("closing stock : " + df.format(closintstock));
            }
            Object o6[] = {"CLOSING STOCK", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }
/*
        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid21");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);
                System.out.println("closing stock : " + df.format(closingstock));
            }
            Object o6[] = {"CLOSING STOCK-1", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid22");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);
                System.out.println("closing stock : " + df.format(closingstock));
            }
            Object o6[] = {"CLOSING STOCK-2", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid23");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);
                System.out.println("closing stock : " + df.format(closingstock));
            }
            Object o6[] = {"CLOSING STOCK-3", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid24");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);
                System.out.println("closing stock : " + df.format(closintstock));
            }
            Object o6[] = {"CLOSING STOCK-4", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid241");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);
                System.out.println("closing stock : " + df.format(closingstock));
            }
            Object o6[] = {"CLOSING STOCK-41", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid242");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);
                System.out.println("closing stock : " + df.format(closingstock));
            }
            Object o6[] = {"CLOSING STOCK-42", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            conn1 = c.cone();
            Statement closing = conn1.createStatement();
            ResultSet closingstock = closing.executeQuery("select sum(Ra_te) from stockid25");
            while (closingstock.next()) {
                closintstock = closingstock.getInt(1);

            }
            Object o6[] = {"CLOSING STOCK-5", df.format(closintstock)};
            dtm2.addRow(o6);
            balancesheetdr = balancesheetdr + closintstock;
        } catch (SQLException ex) {
            Logger.getLogger(BalanceSheet1.class.getName()).log(Level.SEVERE, null, ex);
        }
*/
        double balancesheetresult = 0;
        if (balancesheetcr > balancesheetdr) {
            balancesheetresult = (long) (balancesheetcr - balancesheetdr);

//        list3.add("DIFF In TRIAL");
//        list4.add(""+balancesheetresult);
            Object o8[] = {"DIFF In TRIAL", df.format(balancesheetresult)};
            dtm2.addRow(o8);
        } else if (balancesheetdr > balancesheetcr) {
            balancesheetresult = (long) (balancesheetdr - balancesheetcr);
//        list1.add("DIFF In TRIAL");
//        list2.add(""+balancesheetresult);

            Object o[] = {"DIFF In TRIAL", df.format(balancesheetresult)};
            dtm1.addRow(o);
        }

    }

    public String curr_Date = null;

    private void trial_periodpals() {
        try {
            connection c = new connection();
            Connection conn = c.cone();
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[500];
            String grpname[] = new String[50000];
            String match2[] = new String[500];
            String cretedgc[] = new String[500];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgc = conn.createStatement();
//            ResultSet rsgcc = stgc.executeQuery("select Max(Curr_Date) from LedgerFinal where Curr_Date Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
//            while(rsgcc.next())
//            {
//               curr_Date = rsgcc.getString(1);
//            }
            ResultSet rsgc = stgc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgc.next()) {
                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME")
                            || groupname.equals("PURCHASE ACCOUNTS")
                            || groupname.equals("SALES ACCOUNTS")) //                   groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                    {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1 = conn.createStatement();
                            ResultSet rsdk = stgc1.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bal = 0;
                            int bal1 = 0;
                            int balnet = 0;
                            while (rsdk.next()) {
                                String baltype = rsdk.getString(1);
                                if (baltype.equals("CR")) {
                                    bal = rsdk.getInt(2);
                                }
                                if (baltype.equals("DR")) {
                                    bal1 = rsdk.getInt(2);
                                }
                            }
                            if (bal > bal1) {
                                balnet = bal - bal1;
                                String netbalc = Integer.toString(balnet);
                                //  list4.addItem(netbalc);
                                totalperiod = totalperiod + Integer.parseInt(netbalc);
                                //   list3.addItem(groupname);
                                Object o8[] = {groupname, netbalc};
                                dtm2.addRow(o8);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bal1 > bal) {
                                    balnet = bal1 - bal;
                                }
                                String netbalc1 = Integer.toString(balnet);
                                //   list2.addItem(netbalc1);
                                total1period = total1period + Integer.parseInt(netbalc1);
                                //  list1.addItem(groupname);
                                Object o[] = {groupname, netbalc1};
                                dtm1.addRow(o);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                    j++;
                }
            }
            if (totalperiod > total1period) {
                // list1.addItem("GROSS PROFIT : ");
                diff = (totalperiod - total1period);
                //  list2.addItem("" + diff);

                Object o[] = {"GROSS PROFIT : ", df.format(diff)};
                dtm1.addRow(o);
                totdrperiod = total1period + diff;
//                jTextField2.setText("" + (diff + total1));
//                jTextField1.setText("" + (total));
            } else {
                //  list3.addItem("GROSS LOSS : ");
                diff = (total1period - totalperiod);
                //  list4.addItem("" + diff);

                Object o1[] = {"GROSS LOSS : ", df.format(diff)};
                dtm2.addRow(o1);
                totcrperiod = totalperiod + diff;
//                jTextField2.setText("" + (total1));
//                jTextField1.setText("" + (diff + total));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        try {
            connection c = new connection();
            Connection conn = c.cone();
            int ii = 0;
            int jj = 0;
            int lml = 0;
            String matchh[] = new String[500];
            String match2h[] = new String[500];
            String cretedgcc[] = new String[500];
            match2h[jj] = ("durgeshk");
            cretedgcc[lml] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgcc = conn.createStatement();
            ResultSet rsgcc = stgcc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgcc.next()) {
                aIsAdded = false;
                String groupname1 = rsgcc.getString(1);
                String createdgroup1 = rsgcc.getString(2);
                matchh[ii] = groupname1;
                if (match2h[jj].equals(matchh[ii]) || match2h[jj].equals(cretedgcc[lml])) {
                    groupname = null;
                    createdgroup = null;
                    matchh[ii] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")) {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1c = conn.createStatement();
                            ResultSet rsdkc = stgc1c.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bala = 0;
                            int bala1 = 0;
                            int balneta = 0;
                            while (rsdkc.next()) {
                                String baltype = rsdkc.getString(1);
                                if (baltype.equals("CR")) {
                                    bala = rsdkc.getInt(2);
                                }
                                if (baltype.equals("DR")) {
                                    bala1 = rsdkc.getInt(2);

                                }
                            }
                            if (bala > bala1) {
                                balneta = bala - bala1;
                                String netbalc = Integer.toString(balneta);
                                // list4.addItem(netbalc);
                                totalcrperiod = totalcrperiod + Integer.parseInt(netbalc);
                                // list3.addItem(groupname);

                                Object o1[] = {groupname, netbalc};
                                dtm2.addRow(o1);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bala1 > bala) {
                                    balneta = bala1 - bala;
                                }
                                String netbalc1 = Integer.toString(balneta);
                                // list2.addItem(netbalc1);
                                totaldrperiod = totaldrperiod + Integer.parseInt(netbalc1);
                                //  list1.addItem(groupname);

                                Object o[] = {groupname, netbalc1};
                                dtm1.addRow(o);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2h[jj + 1] = matchh[ii];
                    matchh[ii] = null;
                    jj++;
                }
            }
            if (totalcrperiod > totaldrperiod) {
                //  list3.addItem("NET PROFIT : ");
                difference = totalcrperiod - totaldrperiod;
                //  list4.addItem("" + difference);

                Object o1[] = {"NET PROFIT : ", df.format(difference)};
                dtm2.addRow(o1);
                System.out.print("difference" + df.format(difference));
                System.out.print("totalcrperiod" + df.format(totalcrperiod));
                System.out.print("totalcrperiod" + df.format(totaldrperiod));
                jTextField1.setText("" + df.format((difference + totaldrperiod + total1period)));
                jTextField2.setText("" + df.format((totcrperiod + totalcrperiod)));
            } else {
                // list1.addItem("NET LOSS : ");
                difference = totaldrperiod - totalcrperiod;
                //  list2.addItem("" + difference);

                Object o[] = {"NET LOSS : ", df.format(difference)};
                dtm1.addRow(o);
                System.out.print("else difference" + df.format(difference));
                System.out.print("else totalcrperiod" + df.format(totalcrperiod));
                System.out.print("else totalcrperiod" + df.format(totaldrperiod));
                jTextField1.setText("" + df.format((totaldrperiod + totdrperiod)));
                jTextField2.setText("" + df.format((difference + totalcrperiod + totalperiod)));
            }
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BalanceSheet1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BalanceSheet1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BalanceSheet1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BalanceSheet1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BalanceSheet1().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    public static javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    public static javax.swing.JLabel jLabel5;
    public static javax.swing.JLabel jLabel6;
    public static javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    public static javax.swing.JTextField jTextField1;
    public static javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables

    class PalsPrint1 implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPrintable(this);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }

        }

        java.util.List list1, list2, list3, list4;

        @Override
        public int print(Graphics g, PageFormat pf, int pageIndex)
                throws PrinterException {

            /* tell the caller that this page is part of the printed document */
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;

            }
            //set paper size 
            Paper p = pf.getPaper();
            double margin = 10;
            p.setImageableArea(margin, p.getImageableY(), p.getWidth() - 2 * margin, p.getImageableHeight());
            pf.setPaper(p);

            Graphics2D g2d = (Graphics2D) g;

            g2d.translate(pf.getImageableX(), pf.getImageableY());

            //into list
            list1 = new LinkedList<Object>();
            list2 = new LinkedList<Object>();
            list3 = new LinkedList<Object>();
            list4 = new LinkedList<Object>();

            for (int i = 0; i < jTable1.getRowCount(); i++) {
                list1.add(jTable1.getValueAt(i, 0));
                list2.add(jTable1.getValueAt(i, 1));
                list3.add(jTable2.getValueAt(i, 0));
                list4.add(jTable2.getValueAt(i, 1));
            }

            //into list
            //set font in drawing string 
            Font fonth = new Font("This Time Roman", Font.BOLD, 12);
            g2d.setFont(fonth);
//        g2d.drawString(HeaderAndFooter.getHeader(),10,50);
            Font font = new Font("This Time Roman", Font.BOLD, 8);
            g2d.setFont(font);
            FontMetrics fm1 = g2d.getFontMetrics(font);

            //               g2d.drawString(HeaderAndFooter.getHeaderAddress1(),10,60);
//                g2d.drawString(HeaderAndFooter.getHeaderAddress2(),10,70);
//                g2d.drawString(HeaderAndFooter.getHeaderContact(),10,80);
            Font font2 = new Font("This Time Roman", Font.BOLD, 10);
            g2d.setFont(font2);
            g2d.drawString("BALANCE SHEET", 240, 95);
            g2d.drawString("AS ON " + jLabel6.getText().trim(), 240, 108);

            g2d.drawLine(12, 115, 550, 115); //line draw 

            g2d.drawString("Total :  ", 20, 750); //print total first
            g2d.drawString("Total :  ", 305, 750); //print total second
            g2d.drawLine(12, 730, 570, 730);
            g2d.drawLine(12, 760, 570, 760);

            g2d.drawString(jTextField1.getText(), 275 - fm1.stringWidth(jTextField1.getText()), 750); //print total first
            g2d.drawString(jTextField2.getText(), 550 - fm1.stringWidth(jTextField2.getText()), 750); //print total second

// g2d.drawString(pals.jLabel2.getText(),10,25); //print profit and loss current date 
            Font font1 = new Font("This Time Roman", Font.PLAIN, 10);
            g2d.setFont(font1);
            FontMetrics fm = g2d.getFontMetrics(font1);

//List 1 Print starting x-co-ordinate 10 and y-co-ordinate 30
            int xlist1 = 20;
            int ylist1 = 150;

            g2d.drawString("LIABILITIES", 15, ylist1 - 18);

            g2d.drawLine(12, ylist1 - 12, 550, ylist1 - 12);
            for (int index = 0; index < list1.size(); index++) {

                g2d.drawString(list1.get(index).toString(), xlist1, ylist1);

                ylist1 += 13;

            }//end list1

            //start list2  
            int xlist2 = 280;
            int ylist2 = 150;
            g2d.drawString("Amount", 245, ylist2 - 18);
            for (int index = 0; index < list2.size(); index++) {

                g2d.drawString(list2.get(index).toString(), xlist2 - fm.stringWidth(list2.get(index).toString()), ylist2);
                ylist2 += 13;

            }                   //end list2

            //start list3
            int xlist3 = 300;
            int ylist3 = 150;
            g2d.drawString("ASSETS", 300, ylist3 - 18);
            for (int index = 0; index < list3.size(); index++) {

                g2d.drawString(list3.get(index).toString(), xlist3, ylist3);
                ylist3 += 13;

            }             //end list3

            //start list4
            int xlist4 = 550;
            int ylist4 = 150;
            g2d.drawString("Amount", 510, ylist4 - 18);
            for (int index = 0; index < list4.size(); index++) {

                g2d.drawString(list4.get(index).toString(), xlist4 - fm.stringWidth(list4.get(index).toString()), ylist4);
                ylist4 += 13;
                System.out.println("ylist4 " + ylist4);
            }

            return PAGE_EXISTS;
        }
    }
}
