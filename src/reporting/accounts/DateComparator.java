/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporting.accounts;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Vector;
import java.util.Date;

/**
 *
 * @author Prateek
 */
public class DateComparator implements Comparator<Vector<String>>{
    
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public int compare(Vector<String> o1, Vector<String> o2) {
        String dateStr1 = o1.get(0);
        String dateStr2 = o2.get(0);
        
        if(dateStr1 != null && !dateStr1.isEmpty() && dateStr2 != null && !dateStr2.isEmpty()) {
            try {
                Date date1 = dateFormat.parse(dateStr1);
                Date date2 = dateFormat.parse(dateStr2);
                int compare = -1;
                if(date1.after(date2))
                    compare = 1;
             //   System.out.println(dateStr1 + "  " + dateStr2 + "  " + date1 + "  " + date2 + " return " + compare);
                return compare;
                } catch(Exception exc) {
                exc.printStackTrace();
            }
            
        }
        return 1;
    }

    
    
}
