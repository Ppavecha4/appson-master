package reporting.accounts;

import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable.PrintMode;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author app
 */
public class PartyOutstandingReports extends javax.swing.JFrame {

    /**
     * Creates new form PartyOutstandingReport
     */
    private TableRowSorter sorter;

    private static PartyOutstandingReports obj = null;

    public PartyOutstandingReports() {
        initComponents();
        this.setLocationRelativeTo(null);
        try {
            Connection con = new connection().cone();
            Statement st = con.createStatement();
            Statement st1 = con.createStatement();
            Statement st2 = con.createStatement();
            Statement st3 = con.createStatement();
            Statement st4 = con.createStatement();
            ResultSet rs = st.executeQuery("select distinct(city) from party order by city ASC");
            while (rs.next()) {
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();

                Object object[] = {rs.getString(1), "", "", "", "", "", ""};
                dtm.addRow(object);

                ResultSet rsParty = st1.executeQuery("select againstpayment.party_name,SUM(againstpayment.Amount) from againstpayment , party where againstpayment.party_name=party.party_name and  party.city='" + rs.getString(1) + "' group by againstpayment.party_name ASC ");
                // ResultSet rsParty=st1.executeQuery("select againstpayment.party_name,party.party_name,againstpayment.date,againstpayment.Bill_no,againstpayment.Ref_Bill_no,SUM(againstpayment.Amount) from againstpayment,party where againstpayment.party_name=party.party_name and againstpayment.type = 'BankPayment' ");
                while (rsParty.next()) {
                    if (rs.getString(1) != null) {
                        Object o[] = {"", rsParty.getString(1), "", "", "", "", ""};
                        dtm.addRow(o);
                    }
                    int grandTotal = 0;
                    ResultSet rspayment = st2.executeQuery("select Bill_no,Ref_Bill_no,date from againstpayment where party_name='" + rsParty.getString(1) + "' and type='purchase'");
                    while (rspayment.next()) {
                        int total = 0;

                        ResultSet rspayment1 = st3.executeQuery("select amount from againstpayment where Bill_no='" + rspayment.getString(1) + "' ");

                        while (rspayment1.next()) {
                            ResultSet rspayment2 = st4.executeQuery("select SUM(amount) from againstpayment where Ref_Bill_no='" + rspayment.getString(1) + "' and type!='purchase' ");

                            while (rspayment2.next()) {

                                total = rspayment1.getInt(1) - rspayment2.getInt(1);

                                grandTotal = grandTotal + total;
                            }

                        }
                        if (total != 0) {
                            Object o1[] = {"", "", rspayment.getString(3), rspayment.getString(1), rspayment.getString(2), total, ""};
                            dtm.addRow(o1);
                        }

                    }
                    Object o1[] = {"", "", "", "", "", "", grandTotal};
                    dtm.addRow(o1);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        int rowcount = jTable1.getRowCount();
        int gtotal = 0;
        for (int i = 0; i < rowcount; i++) {
            String t = jTable1.getValueAt(i, 5).toString();
            if (t.equals("")) {
                t = "0";
            }
            int total = Integer.parseInt(t);
            gtotal = gtotal + total;
        }
        jTextField2.setText(String.valueOf(gtotal));
    }

    public static PartyOutstandingReports getObj() {
        if (obj == null) {
            obj = new PartyOutstandingReports();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Party Outstanding Report");
        setLocationByPlatform(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 18)); // NOI18N
        jLabel1.setText("Party Outstanding Report");

        jButton1.setText("Print");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "City ", "Party Name ", "Date", "Purchase Bill No.", "Invoice No.", "Amount", "Total Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Long.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                jTable1CaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(110);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(220);
        }

        jTextField2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel3.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel3.setText("Grand Total");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1140, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(387, 387, 387)
                .addComponent(jLabel1)
                .addGap(28, 459, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:\
        char c = evt.getKeyChar();


    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable1CaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jTable1CaretPositionChanged
        // TODO add your handling code here:
        // String name=jTextField1.getText();


    }//GEN-LAST:event_jTable1CaretPositionChanged
    public void printingTable() {
        try {
            PrinterJob job;
            Book b = new Book();;
            PageFormat pf = new PageFormat();
            job = PrinterJob.getPrinterJob();
            pf = job.defaultPage(pf);
            Paper p = pf.getPaper();

            double margin = 10;
            p.setImageableArea(margin, p.getImageableY(), p.getWidth() - 2 * margin, p.getImageableHeight());

            pf.setPaper(p);

            MessageFormat[] header = new MessageFormat[6];
            header[0] = new MessageFormat("");
            header[1] = new MessageFormat(HeaderAndFooter.getHeader());
            header[2] = new MessageFormat(HeaderAndFooter.getHeaderAddress1());
            header[3] = new MessageFormat(HeaderAndFooter.getHeaderAddress2());
            header[4] = new MessageFormat(HeaderAndFooter.getHeaderContact());
            header[5] = new MessageFormat(jLabel1.getText().trim());

            MessageFormat[] footer = new MessageFormat[1];
            footer[0] = new MessageFormat("");
            //job.setPrintable(new Printing.MyTablePrintable(table, PrintMode.FIT_WIDTH, header, footer));

            b.append(new Printing.MyTablePrintable(jTable1, PrintMode.FIT_WIDTH, header, footer), pf, 50000);

            job.setPageable(b);

            job.print();

        } catch (PrinterException ex) {
            Logger.getLogger(trialBalance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        //printingTable();

        partyPrint pp = new partyPrint();
        pp.printing();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PartyOutstandingReports.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PartyOutstandingReports.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PartyOutstandingReports.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PartyOutstandingReports.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PartyOutstandingReports().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
 class partyPrint implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            Paper paper = new Paper();
            PageFormat pf = job.defaultPage();
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }

        }

        @Override
        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;

            }
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            int rowcount = jTable1.getRowCount();
            int columncount = jTable1.getColumnCount();

            int x1 = 2;
            int x2 = 70;
            int x3 = 150;
            int x4 = 210;
            int x5 = 300;
            int x6 = 380;
            int x7 = 450;
            double width = pageFormat.getImageableWidth();
            System.out.println("width" + width);
            Graphics2D g2d = (Graphics2D) graphics;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

            Font font1 = new Font("", Font.BOLD, 12);
            g2d.setFont(font1);
            // header section start 
            g2d.drawString(HeaderAndFooter.getHeader(), 0, 9);

            Font font2 = new Font("", Font.PLAIN, 9);
            g2d.setFont(font2);
            // g2d.drawString(jTextField1.getText().trim(), 410, 10);
            g2d.drawString(HeaderAndFooter.getHeaderAddress1(), 0, 20);
            g2d.drawString(HeaderAndFooter.getHeaderAddress2(), 0, 30);
            g2d.drawString(HeaderAndFooter.getHeaderContact(), 0, 40);
            //header section end 
            Font font3 = new Font("", Font.BOLD, 10);
            g2d.setFont(font3);
            //table lable 
            g2d.drawString(jLabel1.getText().trim(), 180, 60);
            // First table start 

            int x = 2;
            int y = 110;
            int endWidthX = 465;
            int endLine = 0;

            // get column and print
            for (int i = 0; i < columncount; i++) {
                if (i == 1) {
                    x = 70;
                }
                if (i == 2) {
                    x = 150;
                }
                if (i == 3) {
                    x = 190;
                }
                if (i == 4) {
                    x = 280;
                }

                if (i == 5) {
                    x = 345;
                }
                if (i == 6) {
                    x = 395;
                }
                g2d.drawString(jTable1.getColumnName(i), x, y);

                System.out.println(x);
            }
            // draw line before column 
            g2d.drawLine(0, y - 10, endWidthX, y - 10);
            //draw line after column name 
            g2d.drawLine(0, y + 5, endWidthX, y + 5);

            Font font4 = new Font("", Font.PLAIN, 8);
            g2d.setFont(font4);
            FontMetrics fm4 = g2d.getFontMetrics(font4);
            y = y + 15;
            //table row print 

            for (int i = 0; i < rowcount; i++) {
                int j = 17;
                System.out.println(j);

                g2d.drawString(jTable1.getValueAt(i, 0) + "", x1, y + (j * i));
                g2d.setFont(font3);
                g2d.drawString(jTable1.getValueAt(i, 1) + "", x2, y + (j * i));
                g2d.setFont(font4);
                g2d.drawString(jTable1.getValueAt(i, 2) + "", x3, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 3) + "", x4, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 4) + "", x5, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 5) + "", x6 - fm4.stringWidth(jTable1.getValueAt(i, 5).toString()), y + (j * i));
                g2d.setFont(font3);
                g2d.drawString(jTable1.getValueAt(i, 6) + "", x7 - fm4.stringWidth(jTable1.getValueAt(i, 6).toString()), y + (j * i));
                g2d.setFont(font4);
                if (!jTable1.getValueAt(i, 6).toString().isEmpty()) {
                    g2d.drawLine(0, y + (j * i) - 10, endWidthX, y + (j * i) - 10);
                    g2d.drawLine(0, y + (j * i) + 3, endWidthX, y + (j * i) + 3);
                }
                endLine = (120 + (j * i)) + 2;
                i++;
            }
            g2d.setFont(font3);
            FontMetrics fm3 = g2d.getFontMetrics(font3);
            int pageHeight = (int) pageFormat.getImageableHeight();

            g2d.drawString(jLabel3.getText().trim(), x1, pageHeight - 8);
            g2d.drawString(jTextField2.getText().trim(), x7 - fm3.stringWidth(jTextField2.getText().trim()), pageHeight - 8);

            g2d.drawLine(0, pageHeight - 2, endWidthX, pageHeight - 2);
            g2d.drawLine(0, pageHeight - 20, endWidthX, pageHeight - 20);
            return Printable.PAGE_EXISTS;

        }

    }

}
