package reporting.accounts;

import connection.connection;
import java.awt.Cursor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
//import java.time.Year;
import java.util.Calendar;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFPrintSetup;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Aashish
 */
public class PartyWiseStock_AnalysisReport extends javax.swing.JFrame {

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    String dateNow1 = formatter1.format(currentDate.getTime());
    DecimalFormat df = new DecimalFormat("0.00");
    public boolean dateStatus = true;
    public boolean comboStatus = false;
    XSSFWorkbook workbook = new XSSFWorkbook();
    private static PartyWiseStock_AnalysisReport obj = null;

    /**
     * Creates new form PartyWiseStock_AnalysisReport
     */
    public PartyWiseStock_AnalysisReport() {
        initComponents();
        jTextField1.setText(dateNow1 + "");
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        jDialog1.setLocationRelativeTo(null);
        jDialog2.setLocationRelativeTo(null);
        fillData();
//        getData();
    }

    public static PartyWiseStock_AnalysisReport getObj() {
        if (obj == null) {
            obj = new PartyWiseStock_AnalysisReport();
        }
        return obj;
    }

//--------------Getting Data in combobox----------------------------------------
    public void fillData() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement fill_st = connect.createStatement();
            ResultSet fill_rs = fill_st.executeQuery("select distinct (city) from party");
            while (fill_rs.next()) {
                String city = fill_rs.getString("city");
                jComboBox1.addItem(city);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//-----------------Gatting Default Data-----------------------------------------

    public void getData() {
        this.setCursor(WAIT_CURSOR);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        double opening_stcok = 0;
        double opening_amount = 0;
        double purchase = 0;
        double purchase_amount = 0;
        double purcahse_return = 0;
        double pur_return_amount = 0;
        double sales = 0;
        double sales_amount = 0;
        double sales_return = 0;
        double sales_return_amount = 0;
        double closing_stock = 0;
        double closing_amount = 0;
        String party_code = "";
        String party_name = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement party_st = connect.createStatement();
            Statement opening_st = connect.createStatement();
            Statement purchase_st = connect.createStatement();
            Statement pr_st = connect.createStatement();
            Statement sales_st = connect.createStatement();
            Statement sr_st = connect.createStatement();
            ResultSet party_rs = party_st.executeQuery("select * from party order by party_name ASC");
            while (party_rs.next()) {
                party_code = party_rs.getString("party_code");
                party_name = party_rs.getString("party_name");

// For Opening Stock               
                ResultSet opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te < '2017-04-01'");
                while (opening_rs.next()) {
                    opening_stcok = opening_rs.getDouble("qnt");
                    opening_amount = opening_rs.getDouble("Ra_te");
                }
// For Purchase stock            
                ResultSet purchase_rs = purchase_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te >'2017-03-31'");
                while (purchase_rs.next()) {
                    purchase = purchase_rs.getDouble("qnt");
                    purchase_amount = purchase_rs.getDouble("Ra_te");
                }
// For Purchase return
                ResultSet pr_rs = pr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from purchasereturndualgst where  party_code='" + party_code + "' ");
                while (pr_rs.next()) {
                    purcahse_return = pr_rs.getDouble("qnty");
                    pur_return_amount = pr_rs.getDouble("rate");
                }
// For billing or sales
                ResultSet sales_rs = sales_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Ra_te) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM billing WHERE party_code = '" + party_code + "')");
                while (sales_rs.next()) {
                    sales = sales_rs.getDouble("qnt");
                    sales_amount = sales_rs.getDouble("Ra_te");
                }

// For sales return
                ResultSet sr_rs = sr_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Ra_te) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM salesreturn WHERE party_code = '" + party_code + "')");
                while (sr_rs.next()) {
                    sales_return = sr_rs.getDouble("qnt");
                    sales_return_amount = sr_rs.getDouble("Ra_te");
                }

                closing_stock = (opening_stcok + purchase + sales_return) - (sales + purcahse_return);
                closing_amount = (opening_amount + purchase_amount + sales_return_amount) - (sales_amount + pur_return_amount);

                if (opening_stcok == 0 && opening_amount == 0 && purchase == 0 && purchase_amount == 0 && purcahse_return == 0 && pur_return_amount == 0 && sales == 0 && sales_amount == 0 && sales_return == 0 && sales_return_amount == 0 && closing_stock == 0 && closing_amount == 0) {
                } else {
                    Object o[] = {party_name, df.format(opening_stcok), df.format(opening_amount), df.format(purchase), df.format(purchase_amount), df.format(purcahse_return), df.format(pur_return_amount), df.format(sales), df.format(sales_amount), df.format(sales_return), df.format(sales_return_amount), df.format(closing_stock), df.format(closing_amount)};
                    dtm.addRow(o);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        total();
        this.setCursor(Cursor.getDefaultCursor());
    }
//------------------------------------------------------------------------------ 
//---------------Get Filter Data------------------------------------------------

    public void getFilterData() {
        this.setCursor(WAIT_CURSOR);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        String date = "";
        String from_date = "";
        String to_date = "";
        if (dateStatus == true) {
            java.util.Date date1 = jDateChooser1.getDate();
            if (jDateChooser1.getDate() != null) {
                date = formatter.format(date1);
            }
        } else if (dateStatus == false) {
            java.util.Date date2 = jDateChooser2.getDate();
            from_date = formatter.format(date2);

            java.util.Date date3 = jDateChooser3.getDate();
            to_date = formatter.format(date3);
        }

        double opening_stcok = 0;
        double opening_amount = 0;
        double purchase = 0;
        double purchase_amount = 0;
        double purcahse_return = 0;
        double pur_return_amount = 0;
        double sales = 0;
        double sales_amount = 0;
        double sales_return = 0;
        double sales_return_amount = 0;
        double closing_stock = 0;
        double closing_amount = 0;

        double privious_opening_stcok = 0;
        double privious_opening_amount = 0;
        double privious_purchase = 0;
        double privious_purchase_amount = 0;
        double privious_purcahse_return = 0;
        double privious_pur_return_amount = 0;
        double privious_sales = 0;
        double privious_sales_amount = 0;
        double privious_sales_return = 0;
        double privious_sales_return_amount = 0;
        double privious_closing_stock = 0;
        double privious_closing_amount = 0;

        String party_code = "";
        String party_name = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement party_st = connect.createStatement();
            Statement opening_st = connect.createStatement();
            Statement purchase_st = connect.createStatement();
            Statement pr_st = connect.createStatement();
            Statement sales_st = connect.createStatement();
            Statement sr_st = connect.createStatement();
            Statement prv_opening_st = connect.createStatement();
            Statement prv_purchase_st = connect.createStatement();
            Statement prv_pr_st = connect.createStatement();
            Statement prv_sales_st = connect.createStatement();
            Statement prv_sr_st = connect.createStatement();
            ResultSet party_rs = null;
            ResultSet opening_rs = null;
            ResultSet purchase_rs = null;
            ResultSet pr_rs = null;
            ResultSet sales_rs = null;
            ResultSet sr_rs = null;
            if (jComboBox1.getSelectedItem().equals("ALL")) {
                party_rs = party_st.executeQuery("select * from party order by party_name ASC");
            } else if (!jComboBox1.getSelectedItem().equals("ALL")) {
                party_rs = party_st.executeQuery("select * from party where city = '" + (String) jComboBox1.getSelectedItem() + "'order by party_name ASC");
            }
            while (party_rs.next()) {
                party_code = party_rs.getString("party_code");
                party_name = party_rs.getString("party_name");
// For Purchase Price
                if (jComboBox2.getSelectedItem().equals("Purchase")) {
// For Opening Stock                    
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te < '2017-04-01'");
                    }

                    if (!date.equals("")) {
                        opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te = '" + date + "'");

                        ResultSet pr_op_rs = prv_opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te < '" + date + "'");
                        while (pr_op_rs.next()) {
                            privious_opening_stcok = pr_op_rs.getDouble("qnt");
                            privious_opening_amount = pr_op_rs.getDouble("Ra_te");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
//                        opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te Between '" + from_date + "' and '" + to_date + "'");
                        opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te < '" + from_date + "' and Da_te >'" + to_date + "'");

                        ResultSet pr_op_rs = prv_opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te < '" + from_date + "'");
                        while (pr_op_rs.next()) {
                            privious_opening_stcok = pr_op_rs.getDouble("qnt");
                            privious_opening_amount = pr_op_rs.getDouble("Ra_te");
                        }
                    }

                    while (opening_rs.next()) {
                        opening_stcok = opening_rs.getDouble("qnt");
                        opening_amount = opening_rs.getDouble("Ra_te");
                    }
// For Purchase stock 
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        purchase_rs = purchase_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te > '2017-03-31' ");
                    }

                    if (!date.equals("")) {
                        purchase_rs = purchase_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te < '" + date + "' ");
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
                        purchase_rs = purchase_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Ra_te) as Ra_te from stockid where  Party='" + party_code + "' and Da_te Between '" + from_date + "' and '" + to_date + "' ");
                    }

                    while (purchase_rs.next()) {
                        purchase = purchase_rs.getDouble("qnt");
                        purchase_amount = purchase_rs.getDouble("Ra_te");
                    }
// For Purchase return
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        pr_rs = pr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from purchasereturndualgst where  party_code='" + party_code + "' ");
                    }

                    if (!date.equals("")) {
                        pr_rs = pr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from purchasereturndualgst where  party_code='" + party_code + "' and date = '" + date + "' ");

                        ResultSet prv_pr_rs = prv_pr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from purchasereturndualgst where  party_code='" + party_code + "' and date < '" + date + "' ");
                        while (prv_pr_rs.next()) {
                            privious_purcahse_return = prv_pr_rs.getDouble("qnty");
                            privious_pur_return_amount = prv_pr_rs.getDouble("rate");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
                        pr_rs = pr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from purchasereturndualgst where  party_code='" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "' ");

                        ResultSet prv_pr_rs = prv_pr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from purchasereturndualgst where  party_code='" + party_code + "' and date < '" + from_date + "' ");
                        while (prv_pr_rs.next()) {
                            privious_purcahse_return = prv_pr_rs.getDouble("qnty");
                            privious_pur_return_amount = prv_pr_rs.getDouble("rate");
                        }
                    }

                    while (pr_rs.next()) {
                        purcahse_return = pr_rs.getDouble("qnty");
                        pur_return_amount = pr_rs.getDouble("rate");
                    }
// For billing or sales
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
//                        sales_rs = sales_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM billing WHERE party_code='" + party_code + "')");
                        sales_rs = sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from billing where party_code='" + party_code + "'");
                    }

                    if (!date.equals("")) {
//                        sales_rs = sales_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM billing WHERE party_code = '" + party_code + "' and date = '" + date + "')");
                        sales_rs = sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date = '" + date + "'");
//                        ResultSet prv_sales_rs = prv_sales_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM billing WHERE party_code = '" + party_code + "' and date < '" + date + "')");
                        ResultSet prv_sales_rs = prv_sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date < '" + date + "'");

                        while (prv_sales_rs.next()) {
                            privious_sales = prv_sales_rs.getDouble("qnty");
                            privious_sales_amount = prv_sales_rs.getDouble("rate");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
//                        sales_rs = sales_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM billing WHERE party_code = '" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "')");
//                        ResultSet prv_sales_rs = prv_sales_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM billing WHERE party_code = '" + party_code + "' and date < '" + from_date + "')");
                        sales_rs = sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "'");
                        ResultSet prv_sales_rs = prv_sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date < '" + from_date + "'");
                        while (prv_sales_rs.next()) {
                            privious_sales = prv_sales_rs.getDouble("qnty");
                            privious_sales_amount = prv_sales_rs.getDouble("rate");
                        }
                    }

                    while (sales_rs.next()) {
                        sales = sales_rs.getDouble("qnty");
                        sales_amount = sales_rs.getDouble("rate");
                    }
// For sales return
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
//                        sr_rs = sr_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM salesreturn WHERE party_code = '" + party_code + "')");
                        sr_rs = sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "'");
                    }

                    if (!date.equals("")) {
//                        sr_rs = sr_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM salesreturn WHERE party_code = '" + party_code + "' and date = '" + date + "')");
//                        ResultSet prv_sr_rs = prv_sr_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM salesreturn WHERE party_code = '" + party_code + "' and date < '" + date + "')");
                        sr_rs = sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date = '" + date + "'");

                        ResultSet prv_sr_rs = prv_sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date < '" + date + "'");
                        while (prv_sr_rs.next()) {
                            privious_sales_return = prv_sr_rs.getDouble("qnty");
                            privious_sales_return_amount = prv_sr_rs.getDouble("rate");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
//                        sr_rs = sr_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM salesreturn WHERE party_code ='" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "')");
//                        ResultSet prv_sr_rs = prv_sr_st.executeQuery("SELECT SUM(qnt) AS qnt , SUM(qnt * Re_tail) AS Ra_te FROM stockid WHERE stock_no IN (SELECT stock_no FROM salesreturn WHERE party_code = '" + party_code + "' and date < '" + from_date + "')");
                        sr_rs = sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "'");
                        ResultSet prv_sr_rs = prv_sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date < '" + from_date + "'");
                        while (prv_sr_rs.next()) {
                            privious_sales_return = prv_sr_rs.getDouble("qnty");
                            privious_sales_return_amount = prv_sr_rs.getDouble("rate");
                        }
                    }

                    while (sr_rs.next()) {
                        sales_return = sr_rs.getDouble("qnty");
                        sales_return_amount = sr_rs.getDouble("rate");
                    }
                } // For Sales Price                
                else if (jComboBox2.getSelectedItem().equals("Sales")) {
// For Opening Stok                     
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "' and Da_te < '2017-04-01'");
                    }

                    if (!date.equals("")) {
                        opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "'  and Da_te = '" + date + "'");

                        ResultSet prv_opening_rs = prv_opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "'  and Da_te < '" + date + "'");
                        while (prv_opening_rs.next()) {
                            privious_opening_stcok = prv_opening_rs.getDouble("qnt");
                            privious_opening_amount = prv_opening_rs.getDouble("Re_tail");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
                        //                       opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "' and Da_te Between '" + from_date + "' and '" + to_date + "'");
                        opening_rs = opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "' and Da_te < '" + from_date + "' and Da_te >'" + to_date + "'");

                        ResultSet prv_opening_rs = prv_opening_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "'  and Da_te < '" + from_date + "'");
                        while (prv_opening_rs.next()) {
                            privious_opening_stcok = prv_opening_rs.getDouble("qnt");
                            privious_opening_amount = prv_opening_rs.getDouble("Re_tail");
                        }
                    }

                    while (opening_rs.next()) {
                        opening_stcok = opening_rs.getDouble("qnt");
                        opening_amount = opening_rs.getDouble("Re_tail");
                    }
// For Purchase stock 
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        purchase_rs = purchase_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "' and Da_te > '2017-03-31' ");
                    }

                    if (!date.equals("")) {
                        purchase_rs = purchase_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "' and Da_te = '" + date + "' ");
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
                        purchase_rs = purchase_st.executeQuery("select sum(qnt) as qnt ,sum(qnt * Re_tail) as Re_tail from stockid where  Party='" + party_code + "' and Da_te Between '" + from_date + "' and '" + to_date + "' ");
                    }

                    while (purchase_rs.next()) {
                        purchase = purchase_rs.getDouble("qnt");
                        purchase_amount = purchase_rs.getDouble("Re_tail");
                    }
// For Purchase return
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        pr_rs = pr_st.executeQuery("SELECT SUM(qnt) AS qnt, SUM(qnt * Re_tail) AS Re_tail FROM stockid WHERE stock_no IN (SELECT stock_no FROM purchasereturndualgst where party_code ='" + party_code + "') ");
                    }

                    if (!date.equals("")) {
                        pr_rs = pr_st.executeQuery("SELECT SUM(qnt) AS qnt, SUM(qnt * Re_tail) AS Re_tail FROM stockid WHERE stock_no IN (SELECT stock_no FROM purchasereturndualgst where party_code ='" + party_code + "' and date = '" + date + "' )");

                        ResultSet prv_pr_rs = prv_pr_st.executeQuery("SELECT SUM(qnt) AS qnt, SUM(qnt * Re_tail) AS Re_tail FROM stockid WHERE stock_no IN (SELECT stock_no FROM purchasereturndualgst where party_code ='" + party_code + "' and date < '" + date + "' )");
                        while (prv_pr_rs.next()) {
                            privious_purcahse_return = prv_pr_rs.getDouble("qnt");
                            privious_pur_return_amount = prv_pr_rs.getDouble("Re_tail");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
                        pr_rs = pr_st.executeQuery("SELECT SUM(qnt) AS qnt, SUM(qnt * Re_tail) AS Re_tail FROM stockid WHERE stock_no IN (SELECT stock_no FROM purchasereturndualgst where party_code ='" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "') ");

                        ResultSet prv_pr_rs = prv_pr_st.executeQuery("SELECT SUM(qnt) AS qnt, SUM(qnt * Re_tail) AS Re_tail FROM stockid WHERE stock_no IN (SELECT stock_no FROM purchasereturndualgst where party_code ='" + party_code + "' and date < '" + from_date + "' )");
                        while (prv_pr_rs.next()) {
                            privious_purcahse_return = prv_pr_rs.getDouble("qnt");
                            privious_pur_return_amount = prv_pr_rs.getDouble("Re_tail");
                        }
                    }

                    while (pr_rs.next()) {
                        purcahse_return = pr_rs.getDouble("qnt");
                        pur_return_amount = pr_rs.getDouble("Re_tail");
                    }
// For billing or sales
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        sales_rs = sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate from billing where party_code='" + party_code + "'");
                    }

                    if (!date.equals("")) {
                        sales_rs = sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date = '" + date + "'");

                        ResultSet prv_sales_rs = prv_sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date < '" + date + "'");
                        while (prv_sales_rs.next()) {
                            privious_sales = prv_sales_rs.getDouble("qnty");
                            privious_sales_amount = prv_sales_rs.getDouble("rate");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
                        sales_rs = sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "'");

                        ResultSet prv_sales_rs = prv_sales_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from billing where party_code='" + party_code + "' and date < '" + from_date + "'");
                        while (prv_sales_rs.next()) {
                            privious_sales = prv_sales_rs.getDouble("qnty");
                            privious_sales_amount = prv_sales_rs.getDouble("rate");
                        }
                    }

                    while (sales_rs.next()) {
                        sales = sales_rs.getDouble("qnty");
                        sales_amount = sales_rs.getDouble("rate");
                    }
// For sales return
                    if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                        sr_rs = sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "'");
                    }

                    if (!date.equals("")) {
                        sr_rs = sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date = '" + date + "'");

                        ResultSet prv_sr_rs = prv_sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date < '" + date + "'");
                        while (prv_sr_rs.next()) {
                            privious_sales_return = prv_sr_rs.getDouble("qnty");
                            privious_sales_return_amount = prv_sr_rs.getDouble("rate");
                        }
                    }

                    if (!from_date.equals("") && !to_date.equals("")) {
                        sr_rs = sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date Between '" + from_date + "' and '" + to_date + "'");

                        ResultSet prv_sr_rs = prv_sr_st.executeQuery("select sum(qnty) as qnty, sum(qnty * rate) as rate  from salesreturn where party_code='" + party_code + "' and date < '" + from_date + "'");
                        while (prv_sr_rs.next()) {
                            privious_sales_return = prv_sr_rs.getDouble("qnty");
                            privious_sales_return_amount = prv_sr_rs.getDouble("rate");
                        }
                    }

                    while (sr_rs.next()) {
                        sales_return = sr_rs.getDouble("qnty");
                        sales_return_amount = sr_rs.getDouble("rate");
                    }
                }
                opening_stcok = opening_stcok + privious_opening_stcok + privious_sales_return - privious_sales - privious_purcahse_return;
                opening_amount = opening_amount + privious_opening_amount + privious_sales_return_amount - privious_sales_amount - privious_pur_return_amount;

                closing_stock = (opening_stcok + purchase + sales_return) - (sales + purcahse_return);
                closing_amount = (opening_amount + purchase_amount + sales_return_amount) - (sales_amount + pur_return_amount);

                if (opening_stcok == 0 && opening_amount == 0 && purchase == 0 && purchase_amount == 0 && purcahse_return == 0 && pur_return_amount == 0 && sales == 0 && sales_amount == 0 && sales_return == 0 && sales_return_amount == 0 && closing_stock == 0 && closing_amount == 0) {
                } else {
                    Object o[] = {party_name, df.format(opening_stcok), df.format(opening_amount), df.format(purchase), df.format(purchase_amount), df.format(purcahse_return), df.format(pur_return_amount), df.format(sales), df.format(sales_amount), df.format(sales_return), df.format(sales_return_amount), df.format(closing_stock), df.format(closing_amount)};
                    dtm.addRow(o);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        total();
        this.setCursor(Cursor.getDefaultCursor());
    }
//------------------------------------------------------------------------------    
//-------------Getting Table total data-----------------------------------------

    public void total() {
        jTextField2.setText("0");
        jTextField3.setText("0");
        jTextField4.setText("0");
        jTextField5.setText("0");
        jTextField6.setText("0");
        jTextField7.setText("0");
        jTextField8.setText("0");
        jTextField9.setText("0");
        jTextField10.setText("0");
        jTextField11.setText("0");
        jTextField12.setText("0");
        jTextField13.setText("0");

        int rowcount = jTable1.getRowCount();
        double opening_st = 0;
        double openin_amnt = 0;
        double purchase_st = 0;
        double purchase_amnt = 0;
        double pur_re_st = 0;
        double pur_re_amnt = 0;
        double sales_st = 0;
        double sales_amnt = 0;
        double sales_re_st = 0;
        double sales_re_amnt = 0;
        double closing_st = 0;
        double closing_amnt = 0;

        for (int i = 0; i < rowcount; i++) {
            double os = Double.parseDouble(jTable1.getValueAt(i, 1) + "");
            opening_st = os + opening_st;

            double oa = Double.parseDouble(jTable1.getValueAt(i, 2) + "");
            openin_amnt = oa + openin_amnt;

            double ps = Double.parseDouble(jTable1.getValueAt(i, 3) + "");
            purchase_st = ps + purchase_st;

            double pa = Double.parseDouble(jTable1.getValueAt(i, 4) + "");
            purchase_amnt = pa + purchase_amnt;

            double prs = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
            pur_re_st = prs + pur_re_st;

            double pra = Double.parseDouble(jTable1.getValueAt(i, 6) + "");
            pur_re_amnt = pra + pur_re_amnt;

            double ss = Double.parseDouble(jTable1.getValueAt(i, 7) + "");
            sales_st = ss + sales_st;

            double sa = Double.parseDouble(jTable1.getValueAt(i, 8) + "");
            sales_amnt = sa + sales_amnt;

            double srs = Double.parseDouble(jTable1.getValueAt(i, 9) + "");
            sales_re_st = srs + sales_re_st;

            double sra = Double.parseDouble(jTable1.getValueAt(i, 10) + "");
            sales_re_amnt = sra + sales_re_amnt;

            double cs = Double.parseDouble(jTable1.getValueAt(i, 11) + "");
            closing_st = cs + closing_st;

            double ca = Double.parseDouble(jTable1.getValueAt(i, 12) + "");
            closing_amnt = ca + closing_amnt;

        }

        jTextField2.setText("" + df.format(opening_st));
        jTextField3.setText("" + df.format(openin_amnt));
        jTextField4.setText("" + df.format(purchase_st));
        jTextField5.setText("" + df.format(purchase_amnt));
        jTextField6.setText("" + df.format(pur_re_st));
        jTextField7.setText("" + df.format(pur_re_amnt));
        jTextField8.setText("" + df.format(sales_st));
        jTextField9.setText("" + df.format(sales_amnt));
        jTextField10.setText("" + df.format(sales_re_st));
        jTextField11.setText("" + df.format(sales_re_amnt));
        jTextField12.setText("" + df.format(closing_st));
        jTextField13.setText("" + df.format(closing_amnt));
    }
//------------------------------------------------------------------------------   
//---------Function to write export data into excel------------------------------------------------------------

    public void toExcel() throws FileNotFoundException, IOException {
        TableModel model = jTable1.getModel();
// Creating Sheet        
        XSSFSheet spreadsheet = workbook.createSheet();
// Creating Rows
        XSSFRow row = spreadsheet.createRow((short) 0);
        XSSFRow row1 = spreadsheet.createRow((short) 1);
        XSSFRow row2;
// Creating Cell        
        XSSFCell cell1 = (XSSFCell) row.createCell((short) 0);
// Creating Font        
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Verdana");
        font.setItalic(false);
        font.setBold(true);
        font.setColor(HSSFColor.AUTOMATIC.index);
// Creating Style        
        XSSFCellStyle style = workbook.createCellStyle();
        XSSFCellStyle style1 = workbook.createCellStyle();
        style1.setAlignment(HorizontalAlignment.RIGHT);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        style.setFont(font);
        style.setWrapText(true);
        row.setHeight((short) 700);
        cell1.setCellValue("Khabiya Cloth Store - 07412-492939" + "\n" + "City -" + jComboBox1.getSelectedItem());
        cell1.setCellStyle(style);
// Marging the rows        
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 12));
// Setting Row header        
        for (int i = 0; i < model.getColumnCount(); i++) {
            XSSFCell cell = (XSSFCell) row1.createCell((short) i);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
// Getting Value for Each Column        
        for (int i = 0; i < model.getRowCount(); i++) {
// Creating row after header and row header            
            row = spreadsheet.createRow(i + 2);
            for (int j = 0; j < model.getColumnCount(); j++) {
// Creating Cell for puting values                
                XSSFCell cell2 = (XSSFCell) row.createCell((short) j);
                cell2.setCellValue(model.getValueAt(i, j).toString());
// Condition to check numeric value then right align                 
                if (j >= 1) {
                    cell2.setCellStyle(style1);
                }
// Condition to put table total after all rows                
                if (i == (model.getRowCount() - 1)) {
                    row2 = spreadsheet.createRow(i + 4);
                    XSSFCell c1 = (XSSFCell) row2.createCell((short) 0);
                    c1.setCellValue("Total");
                    c1.setCellStyle(style);
//                    spreadsheet.addMergedRegion(new CellRangeAddress(i + 4, i + 4, 0, 0));
                    c1 = (XSSFCell) row2.createCell((short) 1);
                    c1.setCellValue(jTextField2.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 2);
                    c1.setCellValue(jTextField3.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 3);
                    c1.setCellValue(jTextField4.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 4);
                    c1.setCellValue(jTextField5.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 5);
                    c1.setCellValue(jTextField6.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 6);
                    c1.setCellValue(jTextField7.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 7);
                    c1.setCellValue(jTextField8.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 8);
                    c1.setCellValue(jTextField9.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 9);
                    c1.setCellValue(jTextField10.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 10);
                    c1.setCellValue(jTextField11.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 11);
                    c1.setCellValue(jTextField12.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 12);
                    c1.setCellValue(jTextField13.getText());
                    c1.setCellStyle(style1);
                }
// Setting excel sheet area for printing                
                workbook.setPrintArea(0, 0, j, 0, i);
            }
        }
// Resizing cloumn
        for (int i = 0; i < model.getColumnCount(); i++) {
            spreadsheet.autoSizeColumn(i);
        }
// Setup for printing
        XSSFPrintSetup ps = (XSSFPrintSetup) spreadsheet.getPrintSetup();
//        ps.setLandscape(true);
        spreadsheet.setAutobreaks(true);
        spreadsheet.setFitToPage(true);
        ps.setFitWidth((short) 1);
        ps.setFitHeight((short) 0);
    }
//-------------------------------------------------------------------------------------------------------------     

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel17 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel20 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jTextField13 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();

        jDialog1.setTitle("Select Date");
        jDialog1.setSize(new java.awt.Dimension(333, 136));

        jLabel17.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(57, 57, 58));
        jLabel17.setText("Date - ");

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jButton1.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(57, 57, 58));
        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(jLabel17)
                        .addGap(31, 31, 31)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(jButton1)))
                .addContainerGap(67, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jDialog2.setTitle("Select Date");
        jDialog2.setSize(new java.awt.Dimension(400, 203));

        jLabel18.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(0, 51, 255));
        jLabel18.setText("Date From -");

        jLabel19.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(0, 51, 255));
        jLabel19.setText("Date To - ");

        jDateChooser3.setDateFormatString("dd-MM-yyy");

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jButton2.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(57, 57, 58));
        jButton2.setText("Submit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(jLabel18))
                        .addGap(33, 33, 33)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(119, 119, 119)
                        .addComponent(jButton2)))
                .addContainerGap(89, Short.MAX_VALUE))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(9, 9, 9))
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel19))
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(45, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Party Wise Stock Analysis Report");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(57, 57, 58));
        jLabel1.setText("Party Wise Stock Analysis Report");

        jLabel2.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(57, 57, 58));
        jLabel2.setText("Date -");

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(57, 57, 58));
        jLabel3.setText("City - ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ALL" }));
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(57, 57, 58));
        jLabel20.setText("Price -");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Purchase", "Sales" }));
        jComboBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox2ItemStateChanged(evt);
            }
        });

        jButton3.setForeground(new java.awt.Color(57, 57, 58));
        jButton3.setText("Export");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 518, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(444, 444, 444)
                        .addComponent(jLabel2)
                        .addGap(26, 26, 26)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63)
                        .addComponent(jLabel20)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Party Name ", "Opening St", "Opening Amnt", "Purchase ", "Purchase Amnt", "Purchase Return ", "Pur Return Amnt", "Sales ", "Sales Amount", "Sales Return", "Sales Ret Amnt", "Closing Stock", "Closing Amnt"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel4.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(57, 57, 58));
        jLabel4.setText("Total - ");

        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField12.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField13.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel5.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(57, 57, 58));
        jLabel5.setText("Opening S");

        jLabel6.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(57, 57, 58));
        jLabel6.setText("Opening A");

        jLabel7.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(57, 57, 58));
        jLabel7.setText("Purchase S");

        jLabel8.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(57, 57, 58));
        jLabel8.setText("Purchase A");

        jLabel9.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(57, 57, 58));
        jLabel9.setText("Pur Re S");

        jLabel10.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(57, 57, 58));
        jLabel10.setText("Pur Re A");

        jLabel11.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(57, 57, 58));
        jLabel11.setText("Sales S");

        jLabel12.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(57, 57, 58));
        jLabel12.setText("Sales A");

        jLabel13.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(57, 57, 58));
        jLabel13.setText("Sales Re S");

        jLabel14.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(57, 57, 58));
        jLabel14.setText("Sales Re A");

        jLabel15.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(57, 57, 58));
        jLabel15.setText("Closing S");

        jLabel16.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(57, 57, 58));
        jLabel16.setText("Closing A");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(70, 70, 70)
                                .addComponent(jLabel5)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel6))
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel7)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel8))
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel9)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(jLabel11)
                                .addGap(31, 31, 31))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(38, 38, 38)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel13)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel14)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addGap(20, 20, 20)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addGap(22, 22, 22)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        getFilterData();
        jDialog1.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        getFilterData();
        jDialog2.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        getFilterData();
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jComboBox2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox2ItemStateChanged
        getFilterData();
    }//GEN-LAST:event_jComboBox2ItemStateChanged

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            dateStatus = true;
            jDialog1.setVisible(true);
        }
        if (key == evt.VK_F2) {
            dateStatus = false;
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        JFileChooser fc = new JFileChooser();
        int option = fc.showSaveDialog(PartyWiseStock_AnalysisReport.this);
        if (option == JFileChooser.APPROVE_OPTION) {
            String filename = fc.getSelectedFile().getName();
            String path = fc.getSelectedFile().getParentFile().getPath();

            int len = filename.length();
            String ext = "";
            String file = "";

            if (len > 4) {
                ext = filename.substring(len - 4, len);
            }

            if (ext.equals(".xlsx")) {
                file = path + "\\" + filename;
            } else {
                file = path + "\\" + filename + ".xlsx";
            }
            try {
                toExcel();
                FileOutputStream out = new FileOutputStream(new File(file));
                //write operation workbook using file out object 
                workbook.write(out);

//                Desktop desktop = Desktop.getDesktop();
//                try {
//                    desktop.print(new File(file));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                System.out.println("createworkbook.xlsx written successfully");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PartyWiseStock_AnalysisReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PartyWiseStock_AnalysisReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PartyWiseStock_AnalysisReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PartyWiseStock_AnalysisReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PartyWiseStock_AnalysisReport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
