package reporting.accounts;

//import com.sun.glass.events.KeyEvent;
import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author prateek
 */
public class ProductWiseStockReport extends javax.swing.JFrame {

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
    String dateNow1 = formatter1.format(currentDate.getTime());
    int rowcount = 0;
    int rowcount1 = 0;
    int totalpsales = 0;
    int totalpsalesreturn = 0;
    int totalssales = 0;
    int totalssalesreturn = 0;
    int pnetsales = 0;
    int snetsales = 0;
    int openingstock = 0;
    int purchasedstock = 0;
    int purchasedreturnstock = 0;
    int closingstock = 0;
    int stocktransfer = 0;
    int stockrecieved = 0;
    int previoussales = 0;
    int previousstocktransfer = 0;
    int previouspurchasereturn = 0;
    int previoussalesreturn = 0;
    int previousstockrecieved = 0;
    int salesreturnstock = 0;
    int salesstock = 0;
    public boolean isdate = false;
    public boolean isperiod = false;
    String productcode;
    String productname;

    /**
     * Creates new form
     */
    private void tableSetting() {
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(70);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(30);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setResizable(false);

        }
    }

    private void totaloftable() {

        long total1 = 0;
        long total2 = 0;
        long total3 = 0;
        long total4 = 0;
        long total5 = 0;
        long total6 = 0;
        long total7 = 0;
        long amount1 = 0;
        long amount2 = 0;
        long amount3 = 0;
        long amount4 = 0;
        long amount5 = 0;
        long amount6 = 0;
        long amount7 = 0;

        for (int i = 0; i < jTable1.getRowCount(); i++) {
            amount1 = Long.parseLong(jTable1.getValueAt(i, 1).toString());
            total1 += amount1;

            amount2 = Long.parseLong(jTable1.getValueAt(i, 2).toString());
            total2 += amount2;

            amount3 = Long.parseLong(jTable1.getValueAt(i, 3).toString());
            total3 += amount3;

            amount4 = Long.parseLong(jTable1.getValueAt(i, 4).toString());
            total4 += amount4;

            amount5 = Long.parseLong(jTable1.getValueAt(i, 5).toString());
            total5 += amount5;

            amount6 = Long.parseLong(jTable1.getValueAt(i, 6).toString());
            total6 += amount6;

            amount7 = Long.parseLong(jTable1.getValueAt(i, 7).toString());
            total7 += amount7;
        }
        jFormattedTextField1.setText(String.valueOf(total1));
        jFormattedTextField2.setText(String.valueOf(total2));
        jFormattedTextField3.setText(String.valueOf(total3));
        jFormattedTextField4.setText(String.valueOf(total4));
        jFormattedTextField5.setText(String.valueOf(total5));
        jFormattedTextField6.setText(String.valueOf(total6));
        jFormattedTextField7.setText(String.valueOf(total7));
    }

    private void removetext() {
        jFormattedTextField1.setText("");
        jFormattedTextField2.setText("");
        jFormattedTextField3.setText("");
        jFormattedTextField4.setText("");
        jFormattedTextField5.setText("");
        jFormattedTextField6.setText("");
        jFormattedTextField7.setText("");

    }

    private static ProductWiseStockReport obj = null;

    public ProductWiseStockReport() {

        initComponents();
        this.setLocationRelativeTo(null);
        tableSetting();
        jLabel28.setText("" + dateNow1);
        jPanel1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        jTable1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        //for sales return
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();

            Statement st2 = connect.createStatement();
            Statement st14 = connect.createStatement();
            ResultSet rsparty = st.executeQuery("select * from product order by product_code ASC");

            while (rsparty.next()) {
                productcode = rsparty.getString(1);
                productname = rsparty.getString(2);

                ResultSet rs2 = st2.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) < '2016-04-01' and  Pro_duct='" + productcode + "'");
                while (rs2.next()) {
                    openingstock = rs2.getInt(1);
                }
                Statement st8 = connect.createStatement();
                ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) >= '2016-04-01' and Pro_duct='" + productcode + "' ");
                while (rs3.next()) {
                    purchasedstock = rs3.getInt(1);
                }
                Statement st9 = connect.createStatement();
                ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where Produ_ct='" + productcode + "' ");
                while (rs6.next()) {
                    stockrecieved = rs6.getInt(1);
                }
                Statement st10 = connect.createStatement();
                ResultSet rs10 = st10.executeQuery("select count(Stock_No) from salesreturn where  description='" + productcode + "'");
                while (rs10.next()) {
                    salesreturnstock = rs10.getInt(1);
                }

                int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                Statement st11 = connect.createStatement();
                ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing where  description='" + productcode + "'");
                while (rs11.next()) {
                    salesstock = rs11.getInt(1);
                }

                Statement st12 = connect.createStatement();
                ResultSet rs4 = st1.executeQuery("select count(Stock_no) from purchasereturn where  Produ_ct='" + productcode + "' ");
                while (rs4.next()) {
                    purchasedreturnstock = rs4.getInt(1);
                }

                Statement st13 = connect.createStatement();
                ResultSet rs5 = st1.executeQuery("select count(Stock_no) from stocktransfer where Produ_ct='" + productcode + "' ");
                while (rs5.next()) {
                    stocktransfer = rs5.getInt(1);
                }

                closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                Statement st16 = connect.createStatement();
                ResultSet rs17 = st16.executeQuery("select sum(rate) from billing where  description='" + productcode + "'");
                while (rs17.next()) {
                    totalssales = rs17.getInt(1);
                }
                Statement st17 = connect.createStatement();

                ResultSet rs18 = st17.executeQuery("select sum(rate) from salesreturn where  description='" + productcode + "'");

                while (rs18.next()) {
                    totalssalesreturn = rs18.getInt(1);
                }

                snetsales = totalssales - totalssalesreturn;

                if (openingstock == 0 && purchasedstock == 0 && salesreturnstock == 0 && salesstock == 0 && purchasedreturnstock == 0 && stocktransfer == 0 && closingstock == 0 && snetsales == 0) {

                } else {
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    Object o[] = {productcode, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock, snetsales,};
                    dtm.addRow(o);

                }
            }

            connect.close();
            st.close();
            st1.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        jDialog3.setVisible(false);
        jPanel1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        jTable1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        totaloftable();
    }

    public static ProductWiseStockReport getObj() {
        if (obj == null) {
            obj = new ProductWiseStockReport();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel33 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jDialog3 = new javax.swing.JDialog();
        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jFormattedTextField1 = new javax.swing.JFormattedTextField();
        jFormattedTextField2 = new javax.swing.JFormattedTextField();
        jFormattedTextField3 = new javax.swing.JFormattedTextField();
        jFormattedTextField4 = new javax.swing.JFormattedTextField();
        jFormattedTextField5 = new javax.swing.JFormattedTextField();
        jFormattedTextField6 = new javax.swing.JFormattedTextField();
        jFormattedTextField7 = new javax.swing.JFormattedTextField();
        jButton3 = new javax.swing.JButton();

        jDialog1.setMinimumSize(new java.awt.Dimension(450, 125));

        jLabel31.setText("From:-");

        jLabel32.setText("To:-");

        jDateChooser1.setDateFormatString("yyyy-MM-dd");

        jDateChooser2.setDateFormatString("yyyy-MM-dd");

        jButton1.setText("GO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel31)
                        .addComponent(jLabel32)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(220, 130));

        jLabel33.setText("Date: -");

        jDateChooser3.setDateFormatString("yyyy-MM-dd");

        jButton2.setText("GO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser3, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(36, 36, 36))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jDialog3.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog3.setLocationByPlatform(true);

        jProgressBar1.setStringPainted(true);
        jProgressBar1.setIndeterminate(true);

        jLabel2.setFont(new java.awt.Font("sansserif", 1, 16)); // NOI18N
        jLabel2.setText("Please Wait  Some Time....");

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addGap(124, 124, 124)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog3Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        jDialog3.getAccessibleContext().setAccessibleParent(this);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("PRODUCT WISE STOCK REPORT");
        setLocationByPlatform(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("PRODUCT WISE ANALYSIS STOCK REPORT");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                " PRODUCTS CODE", "OPENING STOCK", "PURCHASE", "SALES  RETURN", "SALES", "PURCHASE RETURN", "CLOSING STOCK", "S NET SALES"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(250);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setResizable(false);
        }

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel27.setText("Date: -");

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel28.setText("jLabel28");
        jLabel28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel28KeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("sansserif", 1, 18)); // NOI18N
        jLabel3.setText("Total :");

        jFormattedTextField1.setEditable(false);
        jFormattedTextField1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        jFormattedTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jFormattedTextField2.setEditable(false);
        jFormattedTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jFormattedTextField3.setEditable(false);
        jFormattedTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jFormattedTextField4.setEditable(false);
        jFormattedTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jFormattedTextField5.setEditable(false);
        jFormattedTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jFormattedTextField6.setEditable(false);
        jFormattedTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jFormattedTextField7.setEditable(false);
        jFormattedTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jFormattedTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextField7ActionPerformed(evt);
            }
        });

        jButton3.setText("Print");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(160, 160, 160)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(jFormattedTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jFormattedTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jFormattedTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jFormattedTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jFormattedTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jFormattedTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 11, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 1086, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel27)
                        .addComponent(jLabel28))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton3))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 548, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jFormattedTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(54, 54, 54))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 45, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 669, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 42, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleName("CITY WISE STOCK REPORT");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        removetext();
        jPanel1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        jTable1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        jDialog1.dispose();

        salesstock = 0;
        salesreturnstock = 0;
        totalpsales = 0;
        totalpsalesreturn = 0;
        totalssales = 0;
        totalssalesreturn = 0;
        pnetsales = 0;
        snetsales = 0;
        openingstock = 0;
        purchasedstock = 0;
        purchasedreturnstock = 0;
        closingstock = 0;
        stocktransfer = 0;
        stockrecieved = 0;
        previoussales = 0;
        previousstocktransfer = 0;
        previouspurchasereturn = 0;
        previoussalesreturn = 0;
        previousstockrecieved = 0;

        isperiod = true;
        isdate = false;

        Date date11 = jDateChooser1.getDate();
        String date1 = formatter.format(date11);
        Date date12 = jDateChooser2.getDate();
        String date2 = formatter.format(date12);
        System.out.println("date1 is" + date1);
        System.out.println("date2 is" + date2);
        jLabel28.setText("" + formatter1.format(date2));

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();

        //for sales return
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st14 = connect.createStatement();

            ResultSet rsparty = st.executeQuery("select * from product order by product_code ASC");

            while (rsparty.next()) {

                productcode = rsparty.getString(1);
                productname = rsparty.getString(2);

                ResultSet rs2 = st2.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) < '" + date1 + "' and Pro_duct='" + productcode + "'");
                while (rs2.next()) {
                    openingstock = rs2.getInt(1);
                    System.out.println("opening" + openingstock);
//        jLabel20.setText(""+openingstock);
                }
                // Date date=formatter.parse(dateNow);
                Statement st3 = connect.createStatement();
                ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing where date <'" + date1 + "' and description='" + productcode + "'");
                while (rsprevioussales.next()) {
                    previoussales = rsprevioussales.getInt(1);

                }
                Statement st4 = connect.createStatement();
                ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and Produ_ct='" + productcode + "'");
                while (rspreviousst.next()) {
                    previousstocktransfer = rspreviousst.getInt(1);
                }
                Statement st5 = connect.createStatement();
                ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(Invoice_date as date) <'" + date1 + "' and Produ_ct='" + productcode + "'");
                while (rspreviouspr.next()) {
                    previouspurchasereturn = rspreviouspr.getInt(1);
                }
                Statement st6 = connect.createStatement();
                ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) from salesreturn where date <'" + date1 + "'and description='" + productcode + "'");
                while (rsprevioussr.next()) {
                    previoussalesreturn = rsprevioussr.getInt(1);
                }
                Statement st7 = connect.createStatement();
                ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "'and Produ_ct='" + productcode + "' ");
                while (rspreviousstockrecieved.next()) {
                    previousstockrecieved = rspreviousstockrecieved.getInt(1);
                }

                Statement st8 = connect.createStatement();
                ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) Between '" + date1 + "' and '" + date2 + "' and Pro_duct='" + productcode + "' ");
                while (rs3.next()) {
                    purchasedstock = rs3.getInt(1);
                }
                Statement st9 = connect.createStatement();
                ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where st_date Between '" + date1 + "' and '" + date2 + "' and Produ_ct='" + productcode + "' ");
                while (rs6.next()) {
                    stockrecieved = rs6.getInt(1);
                }

                openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                Statement st10 = connect.createStatement();
                ResultSet rs10 = st10.executeQuery("select count(Stock_No) from salesreturn where date between '" + date1 + "' and '" + date2 + "' and description='" + productcode + "' ");
                while (rs10.next()) {
                    salesreturnstock = rs10.getInt(1);
                }

                int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                Statement st11 = connect.createStatement();
                ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing where date between '" + date1 + "' and '" + date2 + "' and description='" + productcode + "'");
                while (rs11.next()) {
                    salesstock = rs11.getInt(1);
                }

                Statement st12 = connect.createStatement();
                ResultSet rs4 = st12.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) between '" + date1 + "' and '" + date2 + "' and Produ_ct='" + productcode + "' ");
                while (rs4.next()) {
                    purchasedreturnstock = rs4.getInt(1);
                }
                Statement st13 = connect.createStatement();
                ResultSet rs5 = st13.executeQuery("select count(Stock_no) from stocktransfer where st_date between '" + date1 + "' and '" + date2 + "' and Produ_ct='" + productcode + "' ");
                while (rs5.next()) {
                    stocktransfer = rs5.getInt(1);
                }

                closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                Statement st16 = connect.createStatement();
                ResultSet rs17 = st16.executeQuery("select sum(rate) from billing where date between '" + date1 + "' and '" + date2 + "' and description='" + productcode + "'");
                while (rs17.next()) {
                    totalssales = rs17.getInt(1);
                }
                Statement st17 = connect.createStatement();

                ResultSet rs18 = st17.executeQuery("select sum(rate) from salesreturn where date  between '" + date1 + "' and '" + date2 + "' and description='" + productcode + "'");

                while (rs18.next()) {
                    totalssalesreturn = rs18.getInt(1);
                }
                snetsales = totalssales - totalssalesreturn;

                Object o[] = {productcode, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock, snetsales};
                dtm.addRow(o);

            }

            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        totaloftable();
        jPanel1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        jTable1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        removetext();
        jPanel1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        jTable1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        jDialog2.dispose();
        salesstock = 0;
        salesreturnstock = 0;
        totalpsales = 0;
        totalpsalesreturn = 0;
        totalssales = 0;
        totalssalesreturn = 0;
        pnetsales = 0;
        snetsales = 0;
        openingstock = 0;
        purchasedstock = 0;
        purchasedreturnstock = 0;
        closingstock = 0;
        stocktransfer = 0;
        stockrecieved = 0;
        previoussales = 0;
        previousstocktransfer = 0;
        previouspurchasereturn = 0;
        previoussalesreturn = 0;
        previousstockrecieved = 0;

        isdate = true;
        isperiod = false;

        jDialog2.dispose();

        Date date11 = jDateChooser3.getDate();
        String date1 = formatter.format(date11);
        jLabel28.setText("" + formatter1.format(date1));

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();

        //for sales return
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st14 = connect.createStatement();

            ResultSet rsparty = st.executeQuery("select * from product order by product_code ASC");

            while (rsparty.next()) {
                productcode = rsparty.getString(1);
                productname = rsparty.getString(2);

                ResultSet rs2 = st2.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) < '" + date1 + "' and Pro_duct='" + productcode + "'");
                while (rs2.next()) {
                    openingstock = rs2.getInt(1);

//        jLabel20.setText(""+openingstock);
                }
                // Date date=formatter.parse(dateNow);
                Statement st3 = connect.createStatement();
                ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing where date <'" + date1 + "' and description='" + productcode + "'");
                while (rsprevioussales.next()) {
                    previoussales = rsprevioussales.getInt(1);

                }
                Statement st4 = connect.createStatement();
                ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and Produ_ct='" + productcode + "'");
                while (rspreviousst.next()) {
                    previousstocktransfer = rspreviousst.getInt(1);
                }
                Statement st5 = connect.createStatement();
                ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(Invoice_date as date) <'" + date1 + "' and Produ_ct='" + productcode + "'");
                while (rspreviouspr.next()) {
                    previouspurchasereturn = rspreviouspr.getInt(1);
                }
                Statement st6 = connect.createStatement();
                ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) from salesreturn where date <'" + date1 + "'and description='" + productcode + "'");
                while (rsprevioussr.next()) {
                    previoussalesreturn = rsprevioussr.getInt(1);
                }
                Statement st7 = connect.createStatement();
                ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "'and Produ_ct='" + productcode + "' ");
                while (rspreviousstockrecieved.next()) {
                    previousstockrecieved = rspreviousstockrecieved.getInt(1);
                }

                Statement st8 = connect.createStatement();
                ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) = '" + date1 + "' and Pro_duct='" + productcode + "' ");
                while (rs3.next()) {
                    purchasedstock = rs3.getInt(1);
                }
                Statement st9 = connect.createStatement();
                ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where st_date = '" + date1 + "' and Produ_ct='" + productcode + "' ");
                while (rs6.next()) {
                    stockrecieved = rs6.getInt(1);
                }

                openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                Statement st10 = connect.createStatement();
                ResultSet rs10 = st10.executeQuery("select count(Stock_No) from salesreturn where date ='" + date1 + "'and description='" + productcode + "'");
                while (rs10.next()) {
                    salesreturnstock = rs10.getInt(1);
                }

                int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                Statement st11 = connect.createStatement();
                ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing where date ='" + date1 + "'and description='" + productcode + "'");
                while (rs11.next()) {
                    salesstock = rs11.getInt(1);
                }

                Statement st12 = connect.createStatement();
                ResultSet rs4 = st12.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Produ_ct='" + productcode + "' ");
                while (rs4.next()) {
                    purchasedreturnstock = rs4.getInt(1);
                }
                Statement st13 = connect.createStatement();
                ResultSet rs5 = st13.executeQuery("select count(Stock_no) from stocktransfer where st_date = '" + date1 + "' and Produ_ct='" + productcode + "' ");
                while (rs5.next()) {
                    stocktransfer = rs5.getInt(1);
                }

                closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                Statement st16 = connect.createStatement();
                ResultSet rs17 = st16.executeQuery("select sum(rate) from billing where date = '" + date1 + "'  and description='" + productcode + "'");
                while (rs17.next()) {
                    totalssales = rs17.getInt(1);
                }
                Statement st17 = connect.createStatement();

                ResultSet rs18 = st17.executeQuery("select sum(rate) from salesreturn where date  = '" + date1 + "'  and description='" + productcode + "'");

                while (rs18.next()) {
                    totalssalesreturn = rs18.getInt(1);
                }
                snetsales = totalssales - totalssalesreturn;

                Object o[] = {productcode, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock, snetsales};
                dtm.addRow(o);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jPanel1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        jTable1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        totaloftable();

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jPanel1KeyPressed

    private void jLabel28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel28KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);
        }
    }//GEN-LAST:event_jLabel28KeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jLabel1KeyPressed

    private void jFormattedTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFormattedTextField7ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        ProductPrint pp = new ProductPrint();
        pp.printing();

    }//GEN-LAST:event_jButton3ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProductWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProductWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProductWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProductWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ProductWiseStockReport().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JFormattedTextField jFormattedTextField1;
    private javax.swing.JFormattedTextField jFormattedTextField2;
    private javax.swing.JFormattedTextField jFormattedTextField3;
    private javax.swing.JFormattedTextField jFormattedTextField4;
    private javax.swing.JFormattedTextField jFormattedTextField5;
    private javax.swing.JFormattedTextField jFormattedTextField6;
    private javax.swing.JFormattedTextField jFormattedTextField7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
class ProductPrint implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            Paper paper = new Paper();
            PageFormat pf = job.defaultPage();
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }

        }

        @Override
        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;

            }
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            int rowcount = jTable1.getRowCount();
            int columncount = jTable1.getColumnCount();

            int x1 = 2;
            int x2 = 110;
            int x3 = 180;
            int x4 = 220;
            int x5 = 280;
            int x6 = 330;
            int x7 = 390;
            int x8 = 452;
            double width = pageFormat.getImageableWidth();
            System.out.println("width" + width);
            Graphics2D g2d = (Graphics2D) graphics;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

            Font font1 = new Font("", Font.BOLD, 12);
            g2d.setFont(font1);
            // header section start 
            g2d.drawString(HeaderAndFooter.getHeader(), 0, 9);

            Font font2 = new Font("", Font.PLAIN, 9);
            g2d.setFont(font2);
            // g2d.drawString(jTextField1.getText().trim(), 410, 10);
            g2d.drawString(HeaderAndFooter.getHeaderAddress1(), 0, 20);
            g2d.drawString(HeaderAndFooter.getHeaderAddress2(), 0, 30);
            g2d.drawString(HeaderAndFooter.getHeaderContact(), 0, 40);
            //header section end 
            Font font3 = new Font("", Font.BOLD, 10);
            g2d.setFont(font3);
            //table lable 
            g2d.drawString(jLabel1.getText().trim(), 120, 60);
            g2d.drawString("For The Period 01-04-2016 TO " + jLabel28.getText().trim(), 130, 72);
            // First table start 
            Font fonth = new Font("", Font.BOLD, 8);
            g2d.setFont(fonth);
            int x = 2;
            int y = 110;
            int endWidthX = 465;
            int endLine = 0;

            // get column and print
            g2d.drawString("Product Code", 0, y);
            g2d.drawString("Opening Stk.", 95, y);
            g2d.drawString("Purchase", 157, y);
            g2d.drawString("Sr. Return", 200, y);
            g2d.drawString("Sales", 265, y);
            g2d.drawString("Pur.Return", 310, y);
            g2d.drawString("Closing Stk.", 360, y);
            g2d.drawString("Net Sale", 425, y);

            // draw line before column 
            g2d.drawLine(0, y - 10, endWidthX, y - 10);
            //draw line after column name 
            g2d.drawLine(0, y + 5, endWidthX, y + 5);

            Font font4 = new Font("", Font.PLAIN, 8);
            g2d.setFont(font4);
            FontMetrics fm4 = g2d.getFontMetrics(font4);
            y = y + 15;
            //table row print 
            for (int i = 0; i < rowcount; i++) {
                int j = 17;
                System.out.println(j);
                g2d.drawString(jTable1.getValueAt(i, 0) + "", x1, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", x2, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 2) + "", x3 - fm4.stringWidth(jTable1.getValueAt(i, 2).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 3) + "", x4 - fm4.stringWidth(jTable1.getValueAt(i, 3).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 4) + "", x5 - fm4.stringWidth(jTable1.getValueAt(i, 4).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 5) + "", x6 - fm4.stringWidth(jTable1.getValueAt(i, 5).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 6) + "", x7 - fm4.stringWidth(jTable1.getValueAt(i, 6).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 7) + "", x8 - fm4.stringWidth(jTable1.getValueAt(i, 7).toString()), y + (j * i));

                endLine = (120 + (j * i)) + 2;
            }
            g2d.setFont(font3);
            FontMetrics fm3 = g2d.getFontMetrics(font3);
            int pageHeight = (int) pageFormat.getImageableHeight();

            g2d.drawString(jLabel3.getText().trim(), x1, pageHeight - 8);
            g2d.drawString(jFormattedTextField1.getText().trim(), x2 - fm3.stringWidth(jFormattedTextField1.getText().trim()), pageHeight - 8);
            g2d.drawString(jFormattedTextField2.getText().trim(), x3 - fm3.stringWidth(jFormattedTextField2.getText().trim()), pageHeight - 8);
            g2d.drawString(jFormattedTextField3.getText().trim(), x4 - fm3.stringWidth(jFormattedTextField3.getText().trim()), pageHeight - 8);
            g2d.drawString(jFormattedTextField4.getText().trim(), x5 - fm3.stringWidth(jFormattedTextField4.getText().trim()), pageHeight - 8);
            g2d.drawString(jFormattedTextField5.getText().trim(), x6 - fm3.stringWidth(jFormattedTextField5.getText().trim()), pageHeight - 8);
            g2d.drawString(jFormattedTextField6.getText().trim(), x7 - fm3.stringWidth(jFormattedTextField6.getText().trim()), pageHeight - 8);
            g2d.drawString(jFormattedTextField7.getText().trim(), x8 - fm3.stringWidth(jFormattedTextField7.getText().trim()), pageHeight - 8);

            g2d.drawLine(0, pageHeight - 2, endWidthX, pageHeight - 2);
            g2d.drawLine(0, pageHeight - 20, endWidthX, pageHeight - 20);
            return Printable.PAGE_EXISTS;

        }

    }
}
