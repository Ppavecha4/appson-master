package reporting.accounts;
//import com.sun.glass.events.KeyEvent;

import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable.PrintMode;
import javax.swing.table.DefaultTableModel;

public class RetailRateWiseStockReport extends javax.swing.JFrame {

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
    String dateNow1 = formatter1.format(currentDate.getTime());

    int rowcount = 0;
    int rowcount1 = 0;
    int totalpsales = 0;
    int totalpsalesreturn = 0;
    int totalssales = 0;
    int totalssalesreturn = 0;
    int pnetsales = 0;
    int snetsales = 0;
    int openingstock = 0;
    int purchasedstock = 0;
    int purchasedreturnstock = 0;
    int closingstock = 0;
    int stocktransfer = 0;
    int stockrecieved = 0;
    int previoussales = 0;
    int previousstocktransfer = 0;
    int previouspurchasereturn = 0;
    int previoussalesreturn = 0;
    int previousstockrecieved = 0;
    int salesreturnstock = 0;
    int salesstock = 0;
    public boolean isdate = false;
    public boolean isperiod = false;
    String partycode;
    String partyname;
    int raterangemax;
    int raterangemax1;
    int raterangemin;
    int raterangemin2;
    int rate = 0;

    /**
     * Creates new form
     */
    private void tableTotal() {
        int t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0;
        int rowcount = jTable1.getRowCount();
        for (int i = 0; i < rowcount; i++) {
            //opening stock
            int a1 = Integer.parseInt(jTable1.getValueAt(i, 1).toString());
            t1 = t1 + a1;
            //Purchase 
            int a2 = Integer.parseInt(jTable1.getValueAt(i, 2).toString());
            t2 = t2 + a2;
            //Sales Return
            int a3 = Integer.parseInt(jTable1.getValueAt(i, 3).toString());
            t3 = t3 + a3;
            //Sales
            int a4 = Integer.parseInt(jTable1.getValueAt(i, 4).toString());
            t4 = t4 + a4;
            //Purchase Return  
            int a5 = Integer.parseInt(jTable1.getValueAt(i, 5).toString());
            t5 = t5 + a5;
            //Closing Stock
            int a6 = Integer.parseInt(jTable1.getValueAt(i, 6).toString());
            t6 = t6 + a6;

            //net Sales
            // int a7=Integer.parseInt(jTable1.getValueAt(i, 7).toString());
            //t7=t7+a7;
        }
        jTextField1.setText(String.valueOf(t1));
        jTextField2.setText(String.valueOf(t2));
        jTextField3.setText(String.valueOf(t3));
        jTextField4.setText(String.valueOf(t4));
        jTextField5.setText(String.valueOf(t5));
        jTextField6.setText(String.valueOf(t6));
        //jTextField7.setText(String.valueOf(t7));
    }

    private void tableSetting() {
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
        }
    }

    private static RetailRateWiseStockReport obj = null;

    public RetailRateWiseStockReport() {
        initComponents();
        this.setLocationRelativeTo(null);
        //tableSetting();
        jLabel28.setText("" + dateNow1);
        jComboBox1.addItem(" ALL ");
        jComboBox1.setSelectedIndex(0);
        while (raterangemax <= 1500) {
            raterangemax += 500;
            jComboBox1.addItem(String.valueOf(raterangemin) + " To " + String.valueOf(raterangemax));
            raterangemin = raterangemax;

        }
        raterangemax = 2000;
        raterangemin = 2000;
        while (raterangemax <= 9500) {
            raterangemax += 1000;
            jComboBox1.addItem(String.valueOf(raterangemin) + " To " + String.valueOf(raterangemax));
            raterangemin = raterangemax;
        }
        raterangemax = 10000;
        raterangemin = 10000;
        while (raterangemax <= 45000) {
            raterangemax += 5000;
            jComboBox1.addItem(String.valueOf(raterangemin) + " To " + String.valueOf(raterangemax));
            raterangemin = raterangemax;
        }
        raterangemax = 50000;
        raterangemin = 50000;
        while (raterangemax <= 95000) {
            raterangemax += 10000;
            jComboBox1.addItem(String.valueOf(raterangemin) + " To " + String.valueOf(raterangemax));
            raterangemin = raterangemax;
        }

    }

    public static RetailRateWiseStockReport getObj() {
        if (obj == null) {
            obj = new RetailRateWiseStockReport();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel33 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jTextField6 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();

        jDialog1.setMinimumSize(new java.awt.Dimension(450, 125));

        jLabel31.setText("From:-");

        jLabel32.setText("To:-");

        jDateChooser1.setDateFormatString("yyyy-MM-dd");

        jDateChooser2.setDateFormatString("yyyy-MM-dd");

        jButton1.setText("GO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel31)
                        .addComponent(jLabel32)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(220, 130));

        jLabel33.setText("Date: -");

        jDateChooser3.setDateFormatString("yyyy-MM-dd");

        jButton2.setText("GO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser3, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(36, 36, 36))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("RATE WISE STOCK REPORT");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("RETAIL RATE WISE ANALYSIS STOCK REPORT");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RATE", "OPENING STOCK", "PURCHASE", "SALES RETURN", "SALES", "PURCHASE RETURN", "CLOSING STOCK"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel27.setText("Date: -");

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel28.setText("jLabel28");
        jLabel28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel28KeyPressed(evt);
            }
        });

        jLabel36.setText("SELECT RANGE : -");

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel2.setText("Total :");

        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jButton3.setText("Print");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(141, 141, 141)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(814, Short.MAX_VALUE))))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 1110, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel27)
                                .addComponent(jLabel28))
                            .addComponent(jButton3)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 40, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 661, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        jDialog1.dispose();
        jPanel1.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        jTable1.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        String selecteditem = (String) jComboBox1.getSelectedItem();
        int min = 0;
        int max = 0;
        System.out.println("selecteditem" + selecteditem);
        if (!selecteditem.contains("ALL")) {
            System.out.println("selecteditem" + selecteditem);
            try {
                String[] words = selecteditem.split("\\s");
                String smin = words[0];
                String smax = words[2];

                max = Integer.parseInt(smax);
                min = Integer.parseInt(smin);
                System.out.println("min : " + min + " " + words[1] + " max : " + max);
            } catch (ArrayIndexOutOfBoundsException err) {
                System.err.println(err);
            }
        }
        if (selecteditem.contains("ALL")) {

            salesstock = 0;
            salesreturnstock = 0;
            totalpsales = 0;
            totalpsalesreturn = 0;
            totalssales = 0;
            totalssalesreturn = 0;
            pnetsales = 0;
            snetsales = 0;
            openingstock = 0;
            purchasedstock = 0;
            purchasedreturnstock = 0;
            closingstock = 0;
            stocktransfer = 0;
            stockrecieved = 0;
            previoussales = 0;
            previousstocktransfer = 0;
            previouspurchasereturn = 0;
            previoussalesreturn = 0;
            previousstockrecieved = 0;

            isperiod = true;
            isdate = false;

            Date date11 = jDateChooser1.getDate();
            String date1 = formatter.format(date11);
            Date date12 = jDateChooser2.getDate();
            String date2 = formatter.format(date12);
            System.out.println("date1 is" + date1);
            System.out.println("date2 is" + date2);
            jLabel28.setText("" + formatter1.format(date2));

            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();

            //for sales return
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                Statement st2 = connect.createStatement();
                Statement st14 = connect.createStatement();
                ResultSet rscity1 = st14.executeQuery("select distinct (Re_tail) from stockid  ORDER by CAST(Re_tail as SIGNED INTEGER)  asc");
                while (rscity1.next()) {

                    rate = rscity1.getInt(1);

                    ResultSet rsparty = st.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                    while (rsparty.next()) {
                        openingstock = rsparty.getInt(1);
                        System.out.println("openingstock 2 : " + openingstock);
                        Statement st3 = connect.createStatement();
                        ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing  where CAST(date as date) < '" + date1 + "' and rate =" + rate + "  ");
                        while (rsprevioussales.next()) {
                            previoussales = rsprevioussales.getInt(1);

                        }

                        Statement st4 = connect.createStatement();
                        ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "");
                        while (rspreviousst.next()) {
                            previousstocktransfer = rspreviousst.getInt(1);
                        }
                        Statement st5 = connect.createStatement();
                        ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                        while (rspreviouspr.next()) {
                            previouspurchasereturn = rspreviouspr.getInt(1);
                        }
                        Statement st6 = connect.createStatement();
                        ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) FROM salesreturn WHERE salesreturn.date <'" + date1 + "' AND rate =" + rate + " ");
                        while (rsprevioussr.next()) {
                            previoussalesreturn = rsprevioussr.getInt(1);
                        }
                        Statement st7 = connect.createStatement();
                        ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "   ");
                        while (rspreviousstockrecieved.next()) {
                            previousstockrecieved = rspreviousstockrecieved.getInt(1);
                        }

                        Statement st8 = connect.createStatement();
                        ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) between '" + date1 + "' and '" + date2 + "' and Re_tail =" + rate + "    ");
                        while (rs3.next()) {
                            purchasedstock = rs3.getInt(1);
                        }
                        Statement st9 = connect.createStatement();
                        ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + "  ");
                        while (rs6.next()) {
                            stockrecieved = rs6.getInt(1);
                        }

                        openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                        Statement st10 = connect.createStatement();
                        ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) = '" + date1 + "' AND rate =" + rate + "");
                        while (rs10.next()) {
                            salesreturnstock = rs10.getInt(1);
                        }

                        int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                        Statement st11 = connect.createStatement();
                        ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where CAST(date as date) = '" + date1 + "' and rate =" + rate + "  ");
                        while (rs11.next()) {
                            salesstock = rs11.getInt(1);
                        }

                        ResultSet rs4 = st1.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Re_tail =" + rate + "  ");
                        while (rs4.next()) {
                            purchasedreturnstock = rs4.getInt(1);
                        }

                        ResultSet rs5 = st1.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + "  ");
                        while (rs5.next()) {
                            stocktransfer = rs5.getInt(1);
                        }

                        closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                        Statement st16 = connect.createStatement();
                        ResultSet rs17 = st16.executeQuery("select sum(rate) FROM billing WHERE  date between '" + date1 + "' and '" + date2 + "' AND rate =" + rate + "");
                        while (rs17.next()) {
                            totalssales = rs17.getInt(1);
                        }
                        Statement st17 = connect.createStatement();
                        ResultSet rs18 = st17.executeQuery("select sum(rate) from salesreturn where date  between '" + date1 + "' and '" + date2 + "' and rate =" + rate + " ");

                        while (rs18.next()) {
                            totalssalesreturn = rs18.getInt(1);
                        }
                        snetsales = totalssales - totalssalesreturn;

                        Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                        dtm.addRow(o);

                    }
                }

                connect.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            salesstock = 0;
            salesreturnstock = 0;
            totalpsales = 0;
            totalpsalesreturn = 0;
            totalssales = 0;
            totalssalesreturn = 0;
            pnetsales = 0;
            snetsales = 0;
            openingstock = 0;
            purchasedstock = 0;
            purchasedreturnstock = 0;
            closingstock = 0;
            stocktransfer = 0;
            stockrecieved = 0;
            previoussales = 0;
            previousstocktransfer = 0;
            previouspurchasereturn = 0;
            previoussalesreturn = 0;
            previousstockrecieved = 0;

            isperiod = true;
            isdate = false;

            Date date11 = jDateChooser1.getDate();
            String date1 = formatter.format(date11);
            Date date12 = jDateChooser2.getDate();
            String date2 = formatter.format(date12);

            jLabel28.setText("" + formatter1.format(date2));

            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                Statement st2 = connect.createStatement();
                ResultSet rscity1 = st2.executeQuery("select distinct (Re_tail) from stockid where Re_tail between " + min + " and " + max + " ORDER by CAST(Re_tail as SIGNED INTEGER)  asc ");
                while (rscity1.next()) {

                    rate = rscity1.getInt(1);

                    ResultSet rsparty = st.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                    while (rsparty.next()) {
                        openingstock = rsparty.getInt(1);
                        System.out.println("openingstock 2 : " + openingstock);
                        Statement st3 = connect.createStatement();
                        ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing  where CAST(date as date) < '" + date1 + "' and rate =" + rate + "  ");
                        while (rsprevioussales.next()) {
                            previoussales = rsprevioussales.getInt(1);

                        }

                        Statement st4 = connect.createStatement();
                        ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                        while (rspreviousst.next()) {
                            previousstocktransfer = rspreviousst.getInt(1);
                        }
                        Statement st5 = connect.createStatement();
                        ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                        while (rspreviouspr.next()) {
                            previouspurchasereturn = rspreviouspr.getInt(1);
                        }
                        Statement st6 = connect.createStatement();
                        ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + "");
                        while (rsprevioussr.next()) {
                            previoussalesreturn = rsprevioussr.getInt(1);
                        }
                        Statement st7 = connect.createStatement();
                        ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "   ");
                        while (rspreviousstockrecieved.next()) {
                            previousstockrecieved = rspreviousstockrecieved.getInt(1);
                        }

                        Statement st8 = connect.createStatement();
                        ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) between '" + date1 + "' and '" + date2 + "' and Re_tail =" + rate + "    ");
                        while (rs3.next()) {
                            purchasedstock = rs3.getInt(1);
                        }
                        Statement st9 = connect.createStatement();
                        ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + "  ");
                        while (rs6.next()) {
                            stockrecieved = rs6.getInt(1);
                        }

                        openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                        Statement st10 = connect.createStatement();
                        ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) = '" + date1 + "' AND rate =" + rate + "");
                        while (rs10.next()) {
                            salesreturnstock = rs10.getInt(1);
                        }

                        int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                        Statement st11 = connect.createStatement();
                        ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where CAST(date as date) = '" + date1 + "' and rate =" + rate + "  ");
                        while (rs11.next()) {
                            salesstock = rs11.getInt(1);
                        }

                        ResultSet rs4 = st1.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Re_tail =" + rate + "  ");
                        while (rs4.next()) {
                            purchasedreturnstock = rs4.getInt(1);
                        }

                        ResultSet rs5 = st1.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + "  ");
                        while (rs5.next()) {
                            stocktransfer = rs5.getInt(1);
                        }

                        closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                        Statement st16 = connect.createStatement();
                        ResultSet rs17 = st16.executeQuery("select sum(rate) FROM billing WHERE  date between '" + date1 + "' and '" + date2 + "' AND rate =" + rate + "");
                        while (rs17.next()) {
                            totalssales = rs17.getInt(1);
                        }
                        Statement st17 = connect.createStatement();
                        ResultSet rs18 = st17.executeQuery("select sum(rate) from salesreturn where date  between '" + date1 + "' and '" + date2 + "' and rate =" + rate + " ");

                        while (rs18.next()) {
                            totalssalesreturn = rs18.getInt(1);
                        }
                        snetsales = totalssales - totalssalesreturn;

                        Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                        dtm.addRow(o);

                    }
                }

                connect.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        tableTotal();
        jPanel1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        jTable1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();
        jPanel1.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        jTable1.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        String selecteditem = (String) jComboBox1.getSelectedItem();
        int min = 0;
        int max = 0;
        System.out.println("selecteditem" + selecteditem);
        if (!selecteditem.contains("ALL")) {
            System.out.println("selecteditem" + selecteditem);
            try {
                String[] words = selecteditem.split("\\s");
                String smin = words[0];
                String smax = words[2];

                max = Integer.parseInt(smax);
                min = Integer.parseInt(smin);
                System.out.println("min : " + min + " " + words[1] + " max : " + max);
            } catch (ArrayIndexOutOfBoundsException err) {
                System.err.println(err);
            }
        }

        if (selecteditem.contains("ALL")) {

            salesstock = 0;
            salesreturnstock = 0;
            totalpsales = 0;
            totalpsalesreturn = 0;
            totalssales = 0;
            totalssalesreturn = 0;
            pnetsales = 0;
            snetsales = 0;
            openingstock = 0;
            purchasedstock = 0;
            purchasedreturnstock = 0;
            closingstock = 0;
            stocktransfer = 0;
            stockrecieved = 0;
            previoussales = 0;
            previousstocktransfer = 0;
            previouspurchasereturn = 0;
            previoussalesreturn = 0;
            previousstockrecieved = 0;

            isdate = true;
            isperiod = false;

            jDialog2.dispose();

            Date date11 = jDateChooser3.getDate();
            String date1 = formatter.format(date11);
            jLabel28.setText("" + formatter1.format(date1));

            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();

            //for sales return
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                Statement st2 = connect.createStatement();
                Statement st14 = connect.createStatement();

                ResultSet rscity1 = st14.executeQuery("select distinct (Re_tail) from stockid  ORDER by CAST(Re_tail as SIGNED INTEGER)  asc  ");
                while (rscity1.next()) {

                    rate = rscity1.getInt(1);
                    ResultSet rsparty = st.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) ='" + date1 + "' and Re_tail =" + rate + " ");

                    while (rsparty.next()) {

                        openingstock = rsparty.getInt(1);
                        System.out.println("openingstock 1 : " + openingstock);
                        ResultSet rs2 = st2.executeQuery("select count(Stock_No) from billing  where CAST(date as date) < '" + date1 + "' and rate =" + rate + "  ");
                        while (rs2.next()) {
                            previoussales = rs2.getInt(1);
                            System.out.println("previoussales : " + previoussales);
                        }
                        Statement st3 = connect.createStatement();
                        ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                        while (rsprevioussales.next()) {
                            previousstocktransfer = rsprevioussales.getInt(1);

                        }
                        Statement st4 = connect.createStatement();
                        ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                        while (rspreviousst.next()) {
                            previouspurchasereturn = rspreviousst.getInt(1);
                        }
                        Statement st5 = connect.createStatement();
                        ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + "");
                        while (rspreviouspr.next()) {
                            previoussalesreturn = rspreviouspr.getInt(1);
                        }
                        Statement st6 = connect.createStatement();
                        ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                        while (rsprevioussr.next()) {
                            previousstockrecieved = rsprevioussr.getInt(1);
                        }
                        Statement st7 = connect.createStatement();
                        ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                        while (rspreviousstockrecieved.next()) {
                            purchasedstock = rspreviousstockrecieved.getInt(1);
                        }

                        Statement st8 = connect.createStatement();
                        ResultSet rs3 = st8.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                        while (rs3.next()) {
                            stockrecieved = rs3.getInt(1);
                        }

                        openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                        Statement st10 = connect.createStatement();
                        ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) ='" + date1 + "' AND rate =" + rate + "");
                        while (rs10.next()) {
                            salesreturnstock = rs10.getInt(1);
                        }

                        int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                        Statement st11 = connect.createStatement();
                        ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where CAST(date as date) < '" + date1 + "' and rate =" + rate + "  ");
                        while (rs11.next()) {
                            salesstock = rs11.getInt(1);
                        }

                        Statement st12 = connect.createStatement();
                        ResultSet rs4 = st12.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                        while (rs4.next()) {
                            purchasedreturnstock = rs4.getInt(1);
                        }
                        Statement st13 = connect.createStatement();
                        ResultSet rs5 = st13.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                        while (rs5.next()) {
                            stocktransfer = rs5.getInt(1);
                        }

                        closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                        Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                        dtm.addRow(o);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            salesstock = 0;
            salesreturnstock = 0;
            totalpsales = 0;
            totalpsalesreturn = 0;
            totalssales = 0;
            totalssalesreturn = 0;
            pnetsales = 0;
            snetsales = 0;
            openingstock = 0;
            purchasedstock = 0;
            purchasedreturnstock = 0;
            closingstock = 0;
            stocktransfer = 0;
            stockrecieved = 0;
            previoussales = 0;
            previousstocktransfer = 0;
            previouspurchasereturn = 0;
            previoussalesreturn = 0;
            previousstockrecieved = 0;

            isdate = true;
            isperiod = false;

            jDialog2.dispose();

            Date date11 = jDateChooser3.getDate();
            String date1 = formatter.format(date11);
            jLabel28.setText("" + formatter1.format(date1));

            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();

            //for sales return
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                Statement st2 = connect.createStatement();

                ResultSet rscity1 = st1.executeQuery("select distinct (Re_tail) from stockid where Re_tail between " + min + " and " + max + " ORDER by CAST(Re_tail as SIGNED INTEGER)  asc  ");
                while (rscity1.next()) {

                    rate = rscity1.getInt(1);
                    ResultSet rsparty = st.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) !='" + date1 + "' and Re_tail =" + rate + " ");

                    while (rsparty.next()) {

                        openingstock = rsparty.getInt(1);
                        System.out.println("openingstock 1 : " + openingstock);
                        ResultSet rs2 = st2.executeQuery("select count(Stock_No) from billing  where CAST(date as date) <'" + date1 + "' and rate =" + rate + " ");
                        while (rs2.next()) {
                            previoussales = rs2.getInt(1);
                            System.out.println("previoussales : " + previoussales);
                        }
                        Statement st3 = connect.createStatement();
                        ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                        while (rsprevioussales.next()) {
                            previousstocktransfer = rsprevioussales.getInt(1);

                        }
                        Statement st4 = connect.createStatement();
                        ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                        while (rspreviousst.next()) {
                            previouspurchasereturn = rspreviousst.getInt(1);
                        }
                        Statement st5 = connect.createStatement();
                        ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + " ");
                        while (rspreviouspr.next()) {
                            previoussalesreturn = rspreviouspr.getInt(1);
                        }
                        Statement st6 = connect.createStatement();
                        ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                        while (rsprevioussr.next()) {
                            previousstockrecieved = rsprevioussr.getInt(1);
                        }
                        Statement st7 = connect.createStatement();
                        ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                        while (rspreviousstockrecieved.next()) {
                            purchasedstock = rspreviousstockrecieved.getInt(1);
                        }

                        Statement st8 = connect.createStatement();
                        ResultSet rs3 = st8.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                        while (rs3.next()) {
                            stockrecieved = rs3.getInt(1);
                        }

                        openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                        Statement st10 = connect.createStatement();
                        ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) = '" + date1 + "' AND rate =" + rate + "");
                        while (rs10.next()) {
                            salesreturnstock = rs10.getInt(1);
                        }

                        int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                        Statement st11 = connect.createStatement();
                        ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where CAST(date as date) ='" + date1 + "' and rate =" + rate + " ");
                        while (rs11.next()) {
                            salesstock = rs11.getInt(1);
                        }

                        Statement st12 = connect.createStatement();
                        ResultSet rs4 = st12.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                        while (rs4.next()) {
                            purchasedreturnstock = rs4.getInt(1);
                        }
                        Statement st13 = connect.createStatement();
                        ResultSet rs5 = st13.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                        while (rs5.next()) {
                            stocktransfer = rs5.getInt(1);
                        }

                        closingstock = total - salesstock - purchasedreturnstock - stocktransfer;
                        Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                        dtm.addRow(o);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        tableTotal();
        jPanel1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        jTable1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jPanel1KeyPressed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        // TODO add your handling code here:
        jPanel1.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        jTable1.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        String selecteditem = (String) jComboBox1.getSelectedItem();
        int min = 0;
        int max = 0;
        if (!selecteditem.contains("ALL")) {
            System.out.println("selecteditem" + selecteditem);
            try {
                String[] words = selecteditem.split("\\s");
                String smin = words[0];
                String smax = words[2];

                max = Integer.parseInt(smax);
                min = Integer.parseInt(smin);
            } catch (ArrayIndexOutOfBoundsException err) {
                err.printStackTrace();
            }
        }
        if (selecteditem.contains("ALL")) {

            try {

                if (isdate == true) {

                    salesstock = 0;
                    salesreturnstock = 0;
                    openingstock = 0;
                    purchasedstock = 0;
                    purchasedreturnstock = 0;
                    closingstock = 0;
                    stocktransfer = 0;
                    stockrecieved = 0;
                    previoussales = 0;
                    previousstocktransfer = 0;
                    previouspurchasereturn = 0;
                    previoussalesreturn = 0;
                    previousstockrecieved = 0;

                    isdate = true;

                    jDialog2.dispose();

                    Date date11 = jDateChooser3.getDate();
                    String date1 = formatter.format(date11);
                    jLabel28.setText("" + formatter1.format(date1));

                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.getDataVector().removeAllElements();
                    dtm.fireTableDataChanged();

                    //for sales return
                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement st = connect.createStatement();
                        Statement st1 = connect.createStatement();
                        Statement st2 = connect.createStatement();
                        Statement st14 = connect.createStatement();

                        ResultSet rscity1 = st14.executeQuery("select distinct (Re_tail) from stockid where Re_tail between " + min + " and " + max + " ORDER by CAST(Re_tail as SIGNED INTEGER)  asc  ");
                        while (rscity1.next()) {

                            rate = rscity1.getInt(1);
                            ResultSet rsparty = st.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) !='" + date1 + "' and Re_tail =" + rate + " ");

                            while (rsparty.next()) {

                                openingstock = rsparty.getInt(1);
                                ResultSet rs2 = st2.executeQuery("select count(Stock_No) from billing  where CAST(date as date) <'" + date1 + "' and rate =" + rate + "  ");

                                while (rs2.next()) {
                                    previoussales = rs2.getInt(1);
                                }
                                Statement st3 = connect.createStatement();
                                ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                                while (rsprevioussales.next()) {
                                    previousstocktransfer = rsprevioussales.getInt(1);

                                }
                                Statement st4 = connect.createStatement();
                                ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                                while (rspreviousst.next()) {
                                    previouspurchasereturn = rspreviousst.getInt(1);
                                }
                                Statement st5 = connect.createStatement();
                                ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) < '" + date1 + "' AND rate =" + rate + "");
                                while (rspreviouspr.next()) {
                                    previoussalesreturn = rspreviouspr.getInt(1);
                                }
                                Statement st6 = connect.createStatement();
                                ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                                while (rsprevioussr.next()) {
                                    previousstockrecieved = rsprevioussr.getInt(1);
                                }
                                Statement st7 = connect.createStatement();
                                ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                                while (rspreviousstockrecieved.next()) {
                                    purchasedstock = rspreviousstockrecieved.getInt(1);
                                }

                                Statement st8 = connect.createStatement();
                                ResultSet rs3 = st8.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                                while (rs3.next()) {
                                    stockrecieved = rs3.getInt(1);
                                }

                                openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                                Statement st10 = connect.createStatement();
                                ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) ='" + date1 + "' AND rate =" + rate + "");
                                while (rs10.next()) {
                                    salesreturnstock = rs10.getInt(1);
                                }

                                int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                                Statement st11 = connect.createStatement();
                                ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where  CAST(date as date) ='" + date1 + "' and rate =" + rate + " ");
                                while (rs11.next()) {
                                    salesstock = rs11.getInt(1);
                                }

                                Statement st12 = connect.createStatement();
                                ResultSet rs4 = st12.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                                while (rs4.next()) {
                                    purchasedreturnstock = rs4.getInt(1);
                                }
                                Statement st13 = connect.createStatement();
                                ResultSet rs5 = st13.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                                while (rs5.next()) {
                                    stocktransfer = rs5.getInt(1);
                                }

                                closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

//                                Statement st16 = connect.createStatement();
//                                ResultSet rs17 = st16.executeQuery("select sum(stockid.rate) from billing join stockid where stockid.Stock_no=billing.Stock_no  and  billing.date = '" + date1 + "'  and stockid.Re_tail =" + rate + "");
//                                while (rs17.next()) {
//                                    totalssales = rs17.getInt(1);
//                                }
//                                Statement st17 = connect.createStatement();
//
//                                ResultSet rs18 = st17.executeQuery("select sum(stockid.Re_tail) from salesreturn join stockid where stockid.Stock_no=salesreturn.Stock_no and  salesreturn.date  = '" + date1 + "' and stockid.Re_tail =" + rate + " ");
//
//                                while (rs18.next()) {
//                                    totalssalesreturn = rs18.getInt(1);
//                                }
//                                snetsales = totalssales - totalssalesreturn;
                                Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                                dtm.addRow(o);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (isperiod == true) {

                    salesstock = 0;
                    salesreturnstock = 0;
                    totalpsales = 0;
                    totalpsalesreturn = 0;
                    totalssales = 0;
                    totalssalesreturn = 0;
                    pnetsales = 0;
                    snetsales = 0;
                    openingstock = 0;
                    purchasedstock = 0;
                    purchasedreturnstock = 0;
                    closingstock = 0;
                    stocktransfer = 0;
                    stockrecieved = 0;
                    previoussales = 0;
                    previousstocktransfer = 0;
                    previouspurchasereturn = 0;
                    previoussalesreturn = 0;
                    previousstockrecieved = 0;

                    isperiod = true;

                    Date date11 = jDateChooser1.getDate();
                    String date1 = formatter.format(date11);
                    Date date12 = jDateChooser2.getDate();
                    String date2 = formatter.format(date12);
                    jLabel28.setText("" + formatter1.format(date2));

                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.getDataVector().removeAllElements();
                    dtm.fireTableDataChanged();
                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement st = connect.createStatement();
                        Statement st1 = connect.createStatement();
                        Statement st2 = connect.createStatement();
                        Statement st14 = connect.createStatement();

                        ResultSet rscity1 = st14.executeQuery("select distinct (Re_tail) from stockid where Re_tail between " + min + " and " + max + " ORDER by CAST(Re_tail as SIGNED INTEGER)  asc ");
                        while (rscity1.next()) {

                            rate = rscity1.getInt(1);

                            ResultSet rsparty = st.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) !='" + date1 + "' and Re_tail =" + rate + " ");
                            while (rsparty.next()) {
                                openingstock = rsparty.getInt(1);
                                Statement st3 = connect.createStatement();
                                ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing  where CAST(date as date) <'" + date1 + "' and rate =" + rate + " ");
                                while (rsprevioussales.next()) {
                                    previoussales = rsprevioussales.getInt(1);

                                }

                                Statement st4 = connect.createStatement();
                                ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "  ");
                                while (rspreviousst.next()) {
                                    previousstocktransfer = rspreviousst.getInt(1);
                                }
                                Statement st5 = connect.createStatement();
                                ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + " ");
                                while (rspreviouspr.next()) {
                                    previouspurchasereturn = rspreviouspr.getInt(1);
                                }
                                Statement st6 = connect.createStatement();
                                ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + "");
                                while (rsprevioussr.next()) {
                                    previoussalesreturn = rsprevioussr.getInt(1);
                                }
                                Statement st7 = connect.createStatement();
                                ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "   ");
                                while (rspreviousstockrecieved.next()) {
                                    previousstockrecieved = rspreviousstockrecieved.getInt(1);
                                }

                                Statement st8 = connect.createStatement();
                                ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) between '" + date1 + "' and '" + date2 + "' and Re_tail =" + rate + "    ");
                                while (rs3.next()) {
                                    purchasedstock = rs3.getInt(1);
                                }
                                Statement st9 = connect.createStatement();
                                ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + "  ");
                                while (rs6.next()) {
                                    stockrecieved = rs6.getInt(1);
                                }

                                openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                                Statement st10 = connect.createStatement();
                                ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + " ");
                                while (rs10.next()) {
                                    salesreturnstock = rs10.getInt(1);
                                }

                                int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                                Statement st11 = connect.createStatement();
                                ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where CAST(date as date) ='" + date1 + "' and rate =" + rate + " ");
                                while (rs11.next()) {
                                    salesstock = rs11.getInt(1);
                                }

                                ResultSet rs4 = st1.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Re_tail =" + rate + "  ");
                                while (rs4.next()) {
                                    purchasedreturnstock = rs4.getInt(1);
                                }

                                ResultSet rs5 = st1.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + "  ");
                                while (rs5.next()) {
                                    stocktransfer = rs5.getInt(1);
                                }

                                closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                                Statement st16 = connect.createStatement();
                                ResultSet rs17 = st16.executeQuery("select sum(rate) FROM billing WHERE  date between '" + date1 + "' and '" + date2 + "' AND rate =" + rate + "");
                                while (rs17.next()) {
                                    totalssales = rs17.getInt(1);
                                }
                                Statement st17 = connect.createStatement();
                                ResultSet rs18 = st17.executeQuery("select sum(rate) from salesreturn where date  between '" + date1 + "' and '" + date2 + "' and rate =" + rate + " ");

                                while (rs18.next()) {
                                    totalssalesreturn = rs18.getInt(1);
                                }
                                snetsales = totalssales - totalssalesreturn;

                                Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                                dtm.addRow(o);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.getDataVector().removeAllElements();
                    dtm.fireTableDataChanged();
                    //for sales return

                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement st = connect.createStatement();
                        Statement st1 = connect.createStatement();
                        Statement st2 = connect.createStatement();
                        Statement st14 = connect.createStatement();
                        double rate = 0;
                        ResultSet rscity1 = st14.executeQuery("select distinct (Re_tail) from stockid where Re_tail !='0' ORDER by CAST(Re_tail as SIGNED INTEGER)  asc");
                        while (rscity1.next()) {
                            rate = rscity1.getDouble(1);
                            ResultSet rs2 = st2.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) < '2017-04-01' and Re_tail=" + rate + " ");

                            while (rs2.next()) {
                                openingstock = rs2.getInt(1);
                            }
                            Statement st8 = connect.createStatement();
                            ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) >= '2017-04-01' and Re_tail=" + rate + " ");
                            while (rs3.next()) {
                                purchasedstock = rs3.getInt(1);
                            }
                            Statement st9 = connect.createStatement();
                            ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where  ret_ail=" + rate + " ");
                            while (rs6.next()) {
                                stockrecieved = rs6.getInt(1);
                            }
                            Statement st10 = connect.createStatement();
                            ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn WHERE  rate =" + rate + " ");
                            while (rs10.next()) {
                                salesreturnstock = rs10.getInt(1);
                            }

                            int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                            Statement st11 = connect.createStatement();
                            ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where  rate =" + rate + " ");
                            while (rs11.next()) {
                                salesstock = rs11.getInt(1);
                            }

//                            ResultSet rs4 = st1.executeQuery("select count(Stock_no) from purchasereturn where  Re_tail =" + rate + " ");
//                            while (rs4.next()) {
//                                purchasedreturnstock = rs4.getInt(1);
//                            }

                            ResultSet rs5 = st1.executeQuery("select count(Stock_no) from stocktransfer where  ret_ail =" + rate + " ");
                            while (rs5.next()) {
                                stocktransfer = rs5.getInt(1);
                            }

                            closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                            Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                            dtm.addRow(o);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (isdate == true) {
                salesstock = 0;
                salesreturnstock = 0;
                totalpsales = 0;
                totalpsalesreturn = 0;
                totalssales = 0;
                totalssalesreturn = 0;
                pnetsales = 0;
                snetsales = 0;
                openingstock = 0;
                purchasedstock = 0;
                purchasedreturnstock = 0;
                closingstock = 0;
                stocktransfer = 0;
                stockrecieved = 0;
                previoussales = 0;
                previousstocktransfer = 0;
                previouspurchasereturn = 0;
                previoussalesreturn = 0;
                previousstockrecieved = 0;

                isdate = true;

                Date date11 = jDateChooser3.getDate();
                String date1 = formatter.format(date11);
                jLabel28.setText("" + formatter1.format(date1));

                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                dtm.getDataVector().removeAllElements();
                dtm.fireTableDataChanged();

                //for sales return
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    Statement st1 = connect.createStatement();
                    Statement st2 = connect.createStatement();
                    ResultSet rs1 = st1.executeQuery("select distinct (Re_tail) from stockid where Re_tail between " + min + " and " + max + " ORDER by CAST(Re_tail as SIGNED INTEGER)  asc ");
                    while (rs1.next()) {
                        rate = rs1.getInt(1);
                        ResultSet rs2 = st2.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) !='" + date1 + "' and stockid.Re_tail =" + rate + " ");
                        while (rs2.next()) {
                            openingstock = rs2.getInt(1);
//        jLabel20.setText(""+openingstock);
                        }
                        // Date date=formatter.parse(dateNow);
                        Statement st3 = connect.createStatement();
                        ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing  where CAST(date as date) = '" + date1 + "' and rate =" + rate + " ");
                        while (rsprevioussales.next()) {
                            previoussales = rsprevioussales.getInt(1);

                        }
                        Statement st4 = connect.createStatement();
                        ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + " ");
                        while (rspreviousst.next()) {
                            previousstocktransfer = rspreviousst.getInt(1);
                        }
                        Statement st5 = connect.createStatement();
                        ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + "");
                        while (rspreviouspr.next()) {
                            previouspurchasereturn = rspreviouspr.getInt(1);
                        }
                        Statement st6 = connect.createStatement();
                        ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + " ");
                        while (rsprevioussr.next()) {
                            previoussalesreturn = rsprevioussr.getInt(1);
                        }
                        Statement st7 = connect.createStatement();
                        ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "'and ret_ail =" + rate + " ");
                        while (rspreviousstockrecieved.next()) {
                            previousstockrecieved = rspreviousstockrecieved.getInt(1);
                        }

                        Statement st8 = connect.createStatement();
                        ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                        while (rs3.next()) {
                            purchasedstock = rs3.getInt(1);
                        }
                        Statement st9 = connect.createStatement();
                        ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                        while (rs6.next()) {
                            stockrecieved = rs6.getInt(1);
                        }

                        openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                        Statement st10 = connect.createStatement();
                        ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + " ");
                        while (rs10.next()) {
                            salesreturnstock = rs10.getInt(1);
                        }

                        int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                        Statement st11 = connect.createStatement();
                        ResultSet rs11 = st11.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + " ");
                        while (rs11.next()) {
                            salesstock = rs11.getInt(1);
                        }

                        Statement st12 = connect.createStatement();
                        ResultSet rs4 = st12.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + date1 + "' and Re_tail =" + rate + " ");
                        while (rs4.next()) {
                            purchasedreturnstock = rs4.getInt(1);
                        }
                        Statement st13 = connect.createStatement();
                        ResultSet rs5 = st13.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + date1 + "' and ret_ail =" + rate + " ");
                        while (rs5.next()) {
                            stocktransfer = rs5.getInt(1);
                        }

                        closingstock = total - salesstock - purchasedreturnstock - stocktransfer;
                        Object o[] = {rate, openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                        dtm.addRow(o);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (isperiod == true) {

                salesstock = 0;
                salesreturnstock = 0;
                totalpsales = 0;
                totalpsalesreturn = 0;
                totalssales = 0;
                totalssalesreturn = 0;
                pnetsales = 0;
                snetsales = 0;
                openingstock = 0;
                purchasedstock = 0;
                purchasedreturnstock = 0;
                closingstock = 0;
                stocktransfer = 0;
                stockrecieved = 0;
                previoussales = 0;
                previousstocktransfer = 0;
                previouspurchasereturn = 0;
                previoussalesreturn = 0;
                previousstockrecieved = 0;

                isperiod = true;

                Date date11 = jDateChooser1.getDate();
                String date1 = formatter.format(date11);
                Date date12 = jDateChooser2.getDate();
                String date2 = formatter.format(date12);
                jLabel28.setText("" + formatter1.format(date2));

                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                dtm.getDataVector().removeAllElements();
                dtm.fireTableDataChanged();

                //for sales return
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    Statement st1 = connect.createStatement();
                    Statement st2 = connect.createStatement();
                    ResultSet rs1 = st1.executeQuery("select distinct (Re_tail) from stockid where Re_tail between " + min + " and " + max + " ORDER by CAST(Re_tail as SIGNED INTEGER)  asc  ");
                    while (rs1.next()) {
                        rate = rs1.getInt(1);
                        ResultSet rs2 = st2.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) <'" + date1 + "' and Re_tail =" + rate + "");
                        while (rs2.next()) {
                            openingstock = rs2.getInt(1);
//        jLabel20.setText(""+openingstock);
                        }
                        // Date date=formatter.parse(dateNow);
                        Statement st3 = connect.createStatement();
                        ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing  where CAST(date as date) ='" + date1 + "' and rate =" + rate + " ");
                        while (rsprevioussales.next()) {
                            previoussales = rsprevioussales.getInt(1);
                        }
                        Statement st4 = connect.createStatement();
                        ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "' and ret_ail =" + rate + "");
                        while (rspreviousst.next()) {
                            previousstocktransfer = rspreviousst.getInt(1);
                        }
                        Statement st5 = connect.createStatement();
                        ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + date1 + "' and Re_tail =" + rate + "");
                        while (rspreviouspr.next()) {
                            previouspurchasereturn = rspreviouspr.getInt(1);
                        }
                        Statement st6 = connect.createStatement();
                        ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) <'" + date1 + "' AND rate =" + rate + " ");
                        while (rsprevioussr.next()) {
                            previoussalesreturn = rsprevioussr.getInt(1);
                        }
                        Statement st7 = connect.createStatement();
                        ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + date1 + "'and ret_ail =" + rate + " ");
                        while (rspreviousstockrecieved.next()) {
                            previousstockrecieved = rspreviousstockrecieved.getInt(1);
                        }

                        Statement st8 = connect.createStatement();
                        ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) between '" + date1 + "' and '" + date2 + "' and Re_tail =" + rate + " ");
                        while (rs3.next()) {
                            purchasedstock = rs3.getInt(1);
                        }
                        Statement st9 = connect.createStatement();
                        ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + " ");
                        while (rs6.next()) {
                            stockrecieved = rs6.getInt(1);
                        }

                        openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                        Statement st10 = connect.createStatement();
                        ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) between '" + date1 + "' and '" + date2 + "' AND rate =" + rate + " ");
                        while (rs10.next()) {
                            salesreturnstock = rs10.getInt(1);
                        }

                        int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                        Statement st11 = connect.createStatement();

                        ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where CAST(date as date) between '" + date1 + "' and '" + date2 + "' and rate =" + rate + " ");
                        while (rs11.next()) {
                            salesstock = rs11.getInt(1);
                        }

                        Statement st12 = connect.createStatement();
                        ResultSet rs4 = st12.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) between '" + date1 + "' and '" + date2 + "' and Re_tail =" + rate + " ");
                        while (rs4.next()) {
                            purchasedreturnstock = rs4.getInt(1);
                        }
                        Statement st13 = connect.createStatement();
                        ResultSet rs5 = st13.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) between '" + date1 + "' and '" + date2 + "' and ret_ail =" + rate + " ");
                        while (rs5.next()) {
                            stocktransfer = rs5.getInt(1);
                        }

                        closingstock = total - salesstock - purchasedreturnstock - stocktransfer;

                        Object o[] = {rs1.getInt(1), openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                        dtm.addRow(o);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                salesstock = 0;
                salesreturnstock = 0;
                totalpsales = 0;
                totalpsalesreturn = 0;
                totalssales = 0;
                totalssalesreturn = 0;
                pnetsales = 0;
                snetsales = 0;
                openingstock = 0;
                purchasedstock = 0;
                purchasedreturnstock = 0;
                closingstock = 0;
                stocktransfer = 0;
                stockrecieved = 0;
                previoussales = 0;
                previousstocktransfer = 0;
                previouspurchasereturn = 0;
                previoussalesreturn = 0;
                previousstockrecieved = 0;

                //for sales return
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                dtm.getDataVector().removeAllElements();
                dtm.fireTableDataChanged();
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    Statement st1 = connect.createStatement();
                    Statement st2 = connect.createStatement();

                    ResultSet rsparty = st.executeQuery("select distinct (Re_tail) from stockid where Re_tail between " + min + " and " + max + " ORDER by CAST(Re_tail as SIGNED INTEGER)  asc ");
                    while (rsparty.next()) {
                        rate = rsparty.getInt(1);
                        ResultSet rs2 = st2.executeQuery("select count(Stock_No) from stockid where CAST(CAST(Da_te as date) as date) !='" + dateNow + "' and Re_tail =" + rate + "");
                        while (rs2.next()) {
                            openingstock = rs2.getInt(1);
//        jLabel20.setText(""+openingstock);
                        }
                        // Date date=formatter.parse(dateNow);
                        Statement st3 = connect.createStatement();
                        ResultSet rsprevioussales = st3.executeQuery("select count(Stock_No) from billing  where CAST(date as date) < '" + dateNow + "' and rate =" + rate + "  ");
                        while (rsprevioussales.next()) {
                            previoussales = rsprevioussales.getInt(1);

                        }
                        Statement st4 = connect.createStatement();
                        ResultSet rspreviousst = st4.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + dateNow + "' and ret_ail =" + rate + " ");
                        while (rspreviousst.next()) {
                            previousstocktransfer = rspreviousst.getInt(1);
                        }
                        Statement st5 = connect.createStatement();
                        ResultSet rspreviouspr = st5.executeQuery("select count(Stock_No) from purchasereturn where CAST(invoice_date as date) <'" + dateNow + "' and Re_tail =" + rate + "");
                        while (rspreviouspr.next()) {
                            previouspurchasereturn = rspreviouspr.getInt(1);
                        }
                        Statement st6 = connect.createStatement();
                        ResultSet rsprevioussr = st6.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) < '" + dateNow + "' AND rate =" + rate + "");
                        while (rsprevioussr.next()) {
                            previoussalesreturn = rsprevioussr.getInt(1);
                        }
                        Statement st7 = connect.createStatement();
                        ResultSet rspreviousstockrecieved = st7.executeQuery("select count(Stock_No) from stocktransfer where CAST(St_date as date) <'" + dateNow + "'and ret_ail =" + rate + " ");
                        while (rspreviousstockrecieved.next()) {
                            previousstockrecieved = rspreviousstockrecieved.getInt(1);
                        }

//else if(rowcount1!=0)
//{
//    for(int i=0;i<rowcount1;i++)
//    {
//    ResultSet rs2=st1.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) != '"+dateNow+"' and Stock_No != '"+jTable3.getValueAt(i, 1)+"" +"'  ");
// while (rs2.next())
// {
//     openingstock=rs2.getInt(1);
// }
//    }
//}
                        Statement st8 = connect.createStatement();
                        ResultSet rs3 = st8.executeQuery("select count(Stock_No) from stockid where CAST(Da_te as date) = '" + dateNow + "' and Re_tail =" + rate + " ");
                        while (rs3.next()) {
                            purchasedstock = rs3.getInt(1);
                        }
                        Statement st9 = connect.createStatement();
                        ResultSet rs6 = st9.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + dateNow + "' and ret_ail =" + rate + " ");
                        while (rs6.next()) {
                            stockrecieved = rs6.getInt(1);
                        }
// if(openingstock != 0)
// {
//     openingstock=openingstock+rowcount+purchasedreturnstock+stocktransfer;
// }

                        openingstock = openingstock - previoussales - previouspurchasereturn - previousstocktransfer + previoussalesreturn + previousstockrecieved;

                        Statement st10 = connect.createStatement();
                        ResultSet rs10 = st10.executeQuery("select count(Stock_No) FROM salesreturn where CAST(date as date) = '" + dateNow + "' AND rate =" + rate + "");
                        while (rs10.next()) {
                            salesreturnstock = rs10.getInt(1);
                        }

                        int total = openingstock + purchasedstock + salesreturnstock + stockrecieved;

                        Statement st11 = connect.createStatement();
                        ResultSet rs11 = st11.executeQuery("select count(Stock_No) from billing  where CAST(date as date) < '" + dateNow + "' and rate =" + rate + "  ");
                        while (rs11.next()) {
                            salesstock = rs11.getInt(1);
                        }

                        ResultSet rs4 = st1.executeQuery("select count(Stock_no) from purchasereturn where CAST(invoice_date as date) = '" + dateNow + "' and Re_tail =" + rate + " ");
                        while (rs4.next()) {
                            purchasedreturnstock = rs4.getInt(1);
                        }

                        ResultSet rs5 = st1.executeQuery("select count(Stock_no) from stocktransfer where CAST(st_date as date) = '" + dateNow + "' and ret_ail =" + rate + " ");
                        while (rs5.next()) {
                            stocktransfer = rs5.getInt(1);
                        }

                        Object o[] = {rsparty.getInt(1), openingstock, purchasedstock, salesreturnstock, salesstock, purchasedreturnstock, closingstock};
                        dtm.addRow(o);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        tableTotal();
        jPanel1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        jTable1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jLabel28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel28KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);
        }
    }//GEN-LAST:event_jLabel28KeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jLabel1KeyPressed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed
    public void printingTable() {
        try {

            PrinterJob job;
            Book b = new Book();;
            PageFormat pf = new PageFormat();
            job = PrinterJob.getPrinterJob();
            pf = job.defaultPage(pf);
            Paper p = pf.getPaper();

            double margin = 10;
            p.setImageableArea(margin, p.getImageableY(), p.getWidth() - 2 * margin, p.getImageableHeight());

            pf.setPaper(p);

            MessageFormat[] header = new MessageFormat[5];
            header[0] = new MessageFormat("");
            header[1] = new MessageFormat("Name : Anupam");
            header[2] = new MessageFormat("Finacial Year : 2015-16");
            header[3] = new MessageFormat("Account Name : ");
            header[4] = new MessageFormat("Date : 01/04/2015 To 31/03/2016");

            MessageFormat[] footer = new MessageFormat[1];
            footer[0] = new MessageFormat("");
            //job.setPrintable(new Printing.MyTablePrintable(table, PrintMode.FIT_WIDTH, header, footer));
            b.append(new Printing.MyTablePrintable(jTable1, PrintMode.FIT_WIDTH, header, footer), pf, 50000);
            job.setPageable(b);
            job.print();
        } catch (PrinterException ex) {
            Logger.getLogger(trialBalance.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        RetailPrint pp = new RetailPrint();
        pp.printing();
        //printingTable();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RetailRateWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RetailRateWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RetailRateWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RetailRateWiseStockReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RetailRateWiseStockReport().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox jComboBox1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    // End of variables declaration//GEN-END:variables

    class RetailPrint implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            Paper paper = new Paper();
            PageFormat pf = job.defaultPage();
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }

        }

        @Override
        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;

            }
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            int rowcount = jTable1.getRowCount();
            int columncount = jTable1.getColumnCount();

            int x1 = 25;
            int x2 = 80;
            int x3 = 140;
            int x4 = 210;
            int x5 = 280;
            int x6 = 360;
            int x7 = 440;
            // int x8=452;
            double width = pageFormat.getImageableWidth();
            System.out.println("width" + width);
            Graphics2D g2d = (Graphics2D) graphics;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

            Font font1 = new Font("", Font.BOLD, 12);
            g2d.setFont(font1);
            // header section start 
            g2d.drawString(HeaderAndFooter.getHeader(), 0, 9);

            Font font2 = new Font("", Font.PLAIN, 9);
            g2d.setFont(font2);
            // g2d.drawString(jTextField1.getText().trim(), 410, 10);
            g2d.drawString(HeaderAndFooter.getHeaderAddress1(), 0, 20);
            g2d.drawString(HeaderAndFooter.getHeaderAddress2(), 0, 30);
            g2d.drawString(HeaderAndFooter.getHeaderContact(), 0, 40);
            //header section end 
            Font font3 = new Font("", Font.BOLD, 10);
            g2d.setFont(font3);
            //table lable 
            g2d.drawString(jLabel1.getText().trim(), 130, 60);
            g2d.drawString("For The Period 01-04-2016 TO " + jLabel28.getText().trim(), 145, 72);
            // First table start 
            Font fonth = new Font("", Font.BOLD, 8);
            g2d.setFont(fonth);
            int x = 2;
            int y = 110;
            int endWidthX = 465;
            int endLine = 0;

            // get column and print
            g2d.drawString("Rate", 10, y);
            g2d.drawString("Opening Stk.", 55, y);
            g2d.drawString("Purchase", 120, y);
            g2d.drawString("SL. Return", 190, y);
            g2d.drawString("Sales", 270, y);
            g2d.drawString("Pur.Return", 340, y);
            g2d.drawString("Closing Stk.", 415, y);
            //g2d.drawString("Net Sale", 420, y);

            // draw line before column 
            g2d.drawLine(0, y - 10, endWidthX, y - 10);
            //draw line after column name 
            g2d.drawLine(0, y + 5, endWidthX, y + 5);

            Font font4 = new Font("", Font.PLAIN, 8);
            g2d.setFont(font4);
            FontMetrics fm4 = g2d.getFontMetrics(font4);
            y = y + 15;
            //table row print 
            for (int i = 0; i < 30; i++) {
                int j = 17;
                System.out.println(j);
                g2d.drawString(jTable1.getValueAt(i, 0) + "", x1 - fm4.stringWidth(jTable1.getValueAt(i, 0).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", x2 - fm4.stringWidth(jTable1.getValueAt(i, 1).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 2) + "", x3 - fm4.stringWidth(jTable1.getValueAt(i, 2).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 3) + "", x4 - fm4.stringWidth(jTable1.getValueAt(i, 3).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 4) + "", x5 - fm4.stringWidth(jTable1.getValueAt(i, 4).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 5) + "", x6 - fm4.stringWidth(jTable1.getValueAt(i, 5).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 6) + "", x7 - fm4.stringWidth(jTable1.getValueAt(i, 6).toString()), y + (j * i));
                // g2d.drawString(jTable1.getValueAt(i, 7) + "", x8-fm4.stringWidth(jTable1.getValueAt(i, 7).toString()), y + (j * i));

                endLine = (120 + (j * i)) + 2;
            }
            g2d.setFont(font3);
            FontMetrics fm3 = g2d.getFontMetrics(font3);
            int pageHeight = (int) pageFormat.getImageableHeight();

            g2d.drawString(jLabel2.getText().trim(), 2, pageHeight - 8);
            g2d.drawString(jTextField1.getText().trim(), x2 - fm3.stringWidth(jTextField1.getText().trim()), pageHeight - 8);
            g2d.drawString(jTextField2.getText().trim(), x3 - fm3.stringWidth(jTextField2.getText().trim()), pageHeight - 8);
            g2d.drawString(jTextField3.getText().trim(), x4 - fm3.stringWidth(jTextField3.getText().trim()), pageHeight - 8);
            g2d.drawString(jTextField4.getText().trim(), x5 - fm3.stringWidth(jTextField4.getText().trim()), pageHeight - 8);
            g2d.drawString(jTextField5.getText().trim(), x6 - fm3.stringWidth(jTextField5.getText().trim()), pageHeight - 8);
            g2d.drawString(jTextField6.getText().trim(), x7 - fm3.stringWidth(jTextField6.getText().trim()), pageHeight - 8);
            // g2d.drawString(jTextField7.getText().trim(), x8-fm3.stringWidth(jTextField7.getText().trim()), pageHeight-8);

            g2d.drawLine(0, pageHeight - 2, endWidthX, pageHeight - 2);
            g2d.drawLine(0, pageHeight - 20, endWidthX, pageHeight - 20);
            return Printable.PAGE_EXISTS;

        }

    }

}
