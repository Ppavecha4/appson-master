package reporting.accounts;

import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.List;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 *
 */
public class Tradingaccount1 extends javax.swing.JFrame {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    public String curr_Date = null;
    static String command = null;
    static String choice = null;
    static boolean isParticular = true;
    static boolean isPeriodCase = false;
    static double totalcr, totaldr, total, total1, tcr, tdr, totalcrperiod, totaldrperiod, totalperiod, total1period, tcrperiod, tdrperiod = 0;
    public double diff, totcr, totdr, balcr, baldr, totcrperiod, totdrperiod;
    public double difference, cr, dr, renewcr, renewdr, crperiod, drperiod = 0;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat yearnow = new SimpleDateFormat("yyy");
    String getyear = yearnow.format(currentDate.getTime());
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    period a = null;
    DefaultTableModel dtm1, dtm2;
 String barcode="";
    private void addrow() {
        int tb1RowCount = jTable1.getRowCount();
        int tb2RowCount = jTable2.getRowCount();
        System.out.println("tb1 : " + tb1RowCount + " tb2 : " + tb2RowCount);
        if (tb1RowCount == tb2RowCount) {

        } else if (tb1RowCount > tb2RowCount) {
            int addrow = tb1RowCount - tb2RowCount;
            for (int i = 0; i < addrow; i++) {
                Object o[] = {"", ""};
                // System.out.println("o is "+o[i]);
                dtm2.addRow(o);
            }
        } else {
            int addrow = tb2RowCount - tb1RowCount;
            for (int i = 0; i < addrow; i++) {
                Object o[] = {"", ""};
                dtm1.addRow(o);
            }
        }
    }

    private static Tradingaccount1 obj = null;

    public Tradingaccount1() {
        a = new period();
        isPeriodCase = false;
        initComponents();
        
        
        try
        {
              connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsetting = conn1.createStatement();
            ResultSet rssetting=stsetting.executeQuery("select Barcode from setting");
            
            while(rssetting.next())
            {
        barcode =rssetting.getString(1);
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        this.setLocationRelativeTo(null);
        jScrollPane1.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        // the following statement binds the same BoundedRangeModel to both vertical scrollbars.
        jScrollPane1.getVerticalScrollBar().setModel(
                jScrollPane2.getVerticalScrollBar().getModel());
        dtm1 = (DefaultTableModel) jTable1.getModel();
        dtm2 = (DefaultTableModel) jTable2.getModel();
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });
        jTable2.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });

    }

    public static Tradingaccount1 getObj() {
        if (obj == null) {
            obj = new Tradingaccount1();
        }
        return obj;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jDialog1 = new javax.swing.JDialog();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jDialog2 = new javax.swing.JDialog();
        jLabel9 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jDialog3 = new javax.swing.JDialog();
        jLabel10 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        jLabel3.setText("TOTAL :");

        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel6.setText("Enter Period :");

        jLabel7.setText("From :");

        jLabel8.setText("To :");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addContainerGap())
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(340, 115));

        jLabel9.setText("TOTAL OPENING STOCK: -");

        jTextField5.setEditable(false);

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(58, Short.MAX_VALUE))
        );

        jDialog3.setMinimumSize(new java.awt.Dimension(341, 115));

        jLabel10.setText("TOTAL CLOSING STOCK :-");

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(57, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : TRADING ACCOUNT");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jLabel1.setText("TOTAL :");

        jLabel5.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jLabel5.setText("TOTAL :");

        jButton1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jButton1.setText("EXPAND");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "LIABILITIES", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable1MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jTable2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ASSETS", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable2MousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jButton2.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jButton2.setText("Print");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("sansserif", 1, 18)); // NOI18N
        jLabel2.setText("Trading Account ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(179, 179, 179)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(192, 192, 192)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(797, 551));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:

        {
            trial();
            addrow();
        }
    }//GEN-LAST:event_formWindowOpened

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    public void trialexpand() {

        int sales = 0;
        int purchase = 0;
        cr = 0;
        dr = 0;
        totalcr = 0;
        totaldr = 0;

        try {
            connection c = new connection();
            try (Connection conn3 = c.cone()) {

                java.util.Date dateafter = formatter.parse(getyear + "-04-01");
                java.util.Date datenow = formatter.parse(dateNow);
                if (datenow.before(dateafter)) {
                    getyear = Integer.toString(Integer.parseInt(getyear) - 1);

                }
                String date = getyear.concat("-04-01");

                Statement stopening = conn3.createStatement();

                ResultSet rsopening = stopening.executeQuery("select sum(ra_te) from stockid where CAST(Da_te as date) < '" + date + "' ");
                long opening = 0;
                while (rsopening.next()) {
                    opening = rsopening.getInt(1);
                }

                Object o[] = {"OPENING STOCK", "" + opening};
                dtm1.addRow(o);
//                list1.add("OPENING STOCK");
//                list2.add(""+opening);
                cr = cr + opening;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//FOR SALES
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            Statement stsales1 = conn1.createStatement();
            ResultSet rssales = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='SALES ACCOUNTS' ");

            while (rssales.next()) {
                if (!rssales.getString(1).equals("0")) {
                    Object o[] = {rssales.getString(2), rssales.getInt(1)};
                    dtm2.addRow(o);
//                 list3.add(""+rssales.getString(2));
//             list4.add(""+rssales.getString(1));
                    dr = dr + rssales.getInt(1);
                }

                ResultSet rssales1 = stsales1.executeQuery("select sum(curr_bal) from ledger where groups='SALES ACCOUNTS' group by groups");

                while (rssales1.next()) {
                    sales = rssales1.getInt(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("dr after sales" + dr);

        System.out.println("dr after closing stock" + dr);

//FOR PURCHASE
        try {
            connection c = new connection();
            try (Connection conn2 = c.cone()) {
                Statement stpurchase = conn2.createStatement();
                ResultSet rspurchase = stpurchase.executeQuery("select curr_bal,ledger_name from ledger where groups='PURCHASE ACCOUNTS' ");

                while (rspurchase.next()) {
                    if (!rspurchase.getString(1).equals("0")) {
                        Object o[] = {rspurchase.getString(2), rspurchase.getInt(1)};
                        dtm1.addRow(o);
//                   list1.add(""+rspurchase.getString(2));
//                list2.add(""+rspurchase.getString(1));
                        cr = cr + rspurchase.getInt(1);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("dr after sales" + dr);

        System.out.println("dr after closing stock" + dr);

//FOR OPENING STOCK
        System.out.println("cr after opening stock" + cr);
////stock out

        ///////// for closing stock        
        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();

                ResultSet rsclosing = stclosing.executeQuery("select sum(Ra_te) from stockid2");
                int closing = 0;
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }
                //     int result=closing+stockout0;
                int result = closing;

//                list3.add("CLOSING STOCK");
//                list4.add(""+result);
                Object o[] = {"CLOSING STOCK", result};
                dtm2.addRow(o);
                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

// for other closing stock
//   try
//{
//connection c = new connection();
//            try (Connection conn10 = c.cone()) {
//                Statement stclosing=conn10.createStatement();
//                Statement ststockclosingout0=conn10.createStatement();
//                
//
//                
//                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te) from stockid21");
//                int closing=0;
//                while (rsclosing.next() )
//                {
//                    closing=rsclosing.getInt(1);
//                }
//           //     int result=closing+stockout0;
//                int result=closing;
//                
////                list3.add("CLOSING STOCK");
////                list4.add(""+result);
//                
//                Object o[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(o);
//                dr=dr+result;
//            }
//}
//    catch(Exception e)
//    {
//        e.printStackTrace();
//    }
//   
        int direct = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsdirectincome = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT INCOMES' ");

            while (rsdirectincome.next()) {
                //     direct=rsdirectincome.getInt(1);

                if (!rsdirectincome.getString(1).equals("0")) {
//             list3.add(""+rsdirectincome.getString(2));
//             list4.add(""+rsdirectincome.getString(1));
                    Object o[] = {rsdirectincome.getString(2), rsdirectincome.getString(1)};
                    dtm2.addRow(o);
                    dr = (dr + rsdirectincome.getInt(1));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        int directexpense = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsdirectexpense = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT EXPENSES' ");

            while (rsdirectexpense.next()) {
                if (!rsdirectexpense.getString(1).equals("0")) {
//             list1.add(""+rsdirectexpense.getString(2));
//             list2.add(""+rsdirectexpense.getString(1));

                    Object o[] = {rsdirectexpense.getString(2), rsdirectexpense.getInt(1)};
                    dtm1.addRow(o);

                    cr = cr + rsdirectexpense.getInt(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        double diffss = 0;
//            long aftercr=0,afterdr=0;
        System.out.println(totalcr + " : " + totaldr);

        System.out.println("" + cr);
        System.out.println("" + dr);
        double result = dr - cr;
        System.out.println("result" + result);
        if (cr >= dr) {

            diffss = (long) (cr - dr);

            Object o[] = {"GROSS LOSS ", Math.abs(diffss)};
            dtm2.addRow(o);
            // total=(int) (diffss+dr);
            //jTextField1.setText("" + total);
            totalcr = (int) diffss;
        } else {

            diffss = (long) (dr - cr);

            double diffss1 = (double) diffss;
            double sales1 = (double) sales;
            double profitpercent = (diffss1 / sales1) * 100;

            DecimalFormat df = new DecimalFormat("0.00");
            String profitpercent1 = df.format(profitpercent);

//                    list1.addItem("GROSS PROFIT "+"("+profitpercent1+"%)");
//                    list2.addItem(""+Math.abs(diffss));
            Object o[] = {"GROSS PROFIT " + "(" + profitpercent1 + "%)", Math.abs(diffss)};
            dtm1.addRow(o);
            totaldr = (int) diffss;

        }
    }


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
//     list1.removeAll();
//     list2.removeAll();
//     list3.removeAll();
//     list4.removeAll();

        dtm1.getDataVector().removeAllElements();
        dtm2.getDataVector().removeAllElements();
        dtm1.fireTableDataChanged();
        dtm2.fireTableDataChanged();
        jPanel1.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        trialexpand();
        addrow();
        jPanel1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MousePressed
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {
            command = jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString().trim();

            if (command.equals("GROSS PROFIT : ")) {

            } else if (command.equals("GROSS LOSS : ")) {

            } else if (command.equals("NET PROFIT : ")) {

            } else if (command.equals("NET LOSS : ")) {

            } else {
                reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);

            }
        }

    }//GEN-LAST:event_jTable1MousePressed

    private void jTable2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MousePressed
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {

            command = jTable1.getValueAt(jTable2.getSelectedRow(), 0).toString().trim();
            System.out.println("command is :" + command);
            if (command.equals("GROSS PROFIT : ")) {

            } else if (command.equals("GROSS LOSS : ")) {

            } else if (command.equals("NET PROFIT : ")) {

            } else if (command.equals("NET LOSS : ")) {

            } else {
                reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);
            }
        }
    }//GEN-LAST:event_jTable2MousePressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:

        PalsPrint1 pp = new PalsPrint1();
        pp.printing();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:

    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed
    /**
     * @param args the command line arguments
     */
    private void trial() {

        int sales = 0;
        long purchase = 0;

//FOR OPENING STOCK
        try {
            java.util.Date dateafter = formatter.parse(getyear + "-04-01");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);

            }
            String date = getyear.concat("-04-01");
            System.out.println("date is" + date);
            connection c = new connection();
            try (Connection conn3 = c.cone()) {
                Statement stopening = conn3.createStatement();
                ResultSet rsopening=null;
                if(barcode.equals("Enable"))
                {
                 rsopening = stopening.executeQuery("select sum(ra_te * qnt) from stockid1 ");
                }
                else if(barcode.equals("Disable"))
                {
                     rsopening = stopening.executeQuery("select Amount from openingstock ");
                }
                long opening = 0;
                while (rsopening.next()) {
                    opening = rsopening.getInt(1);
                }

                Object o[] = {"OPENING STOCK", opening};
                dtm1.addRow(o);
                cr = cr + opening;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //FOR SALES
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rssales = stsales.executeQuery("select sum(curr_bal) from ledger where groups='SALES ACCOUNTS' group by groups");

            while (rssales.next()) {
                sales = rssales.getInt(1);
            }
//             list3.add("SALES ACCOUNTS");
//             list4.add(""+sales);

            Object o[] = {"SALES ACCOUNTS", sales};
            dtm2.addRow(o);
            dr = dr + sales;
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("dr after sales" + dr);

        System.out.println("dr after closing stock" + dr);

//FOR PURCHASE
        try {
            connection c = new connection();
            try (Connection conn2 = c.cone()) {
                Statement stpurchase = conn2.createStatement();
                ResultSet rspurchase = stpurchase.executeQuery("select sum(curr_bal) from ledger where groups='PURCHASE ACCOUNTS' group by groups");

                while (rspurchase.next()) {
                    purchase = rspurchase.getInt(1);
                }
//                list1.add("PURCHASE ACCOUNTS");
//                list2.add(""+purchase);

                Object o[] = {"PURCHASE ACCOUNTS", purchase};
                dtm1.addRow(o);
                cr = cr + purchase;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        // for other closing stock        
        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosingother = conn10.createStatement();

//                ResultSet rsstockout0=ststockclosingout0.executeQuery("select sum(ra_te) from stocktransfer where To_branch='0'");
//                int stockout0=0;
//                while (rsstockout0.next() )
//                {
//                    stockout0=rsstockout0.getInt(1);
//                }
                //       System.out.println("Value of stockin at branch1-"+stockout0);
                ResultSet rsclosingother = stclosingother.executeQuery("select * from closingstock");
                long closingother = 0;
                while (rsclosingother.next()) {
                    closingother = Long.parseLong(rsclosingother.getString(2));

//                list3.add("CLOSING STOCK"+rsclosingother.getString(1));
//                list4.add(""+closingother);
                    Object o[] = {"CLOSING STOCK" + rsclosingother.getString(1), closingother};
                    dtm1.addRow(o);
                    dr = dr + closingother;

                }
                //     int result=closing+stockout0;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ///////// for closing stock        
        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();
 double closing = 0;
                ResultSet rsclosing =null;
                        
                if(barcode.equals("Enable"))
                {
                    rsclosing    = stclosing.executeQuery("select sum(Ra_te) from stockid2");
                }
                else if(barcode.equals("Disable"))
                   
                while (rsclosing.next()) {
                    closing = rsclosing.getInt(1);
                }
                double result = closing;
//                list3.add("CLOSING STOCK");
//                list4.add(""+result);
                Object o[] = {"CLOSING STOCK", result};
                dtm2.addRow(o);
                dr = dr + result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

// for closing stock branch1
//        try
//{
//connection c = new connection();
//            try (Connection conn10 = c.cone()) {
//                Statement stclosing=conn10.createStatement();
//                Statement ststockclosingout0=conn10.createStatement();
//                
//                
//                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te) from stockid21");
//                int closing=0;
//                while (rsclosing.next() )
//                {
//                    closing=rsclosing.getInt(1);
//                }
// //               int result=closing+stockout0;
//   int result=closing;
////                list3.add("CLOSING STOCK");
////                list4.add(""+result);
//                Object o[]={"CLOSING STOCK-1",result};
//                         dtm2.addRow(o);
//                dr=dr+result;
//            }
//}
//    catch(Exception e)
//    {
//        e.printStackTrace();
//    }
//
//        
        // Direct incomes
        int direct = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsdirectincome = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT INCOMES' ");

            while (rsdirectincome.next()) {
                //     direct=rsdirectincome.getInt(1);

                if (!rsdirectincome.getString(1).equals("0")) {
//             list3.add(""+rsdirectincome.getString(2));
//             list4.add(""+rsdirectincome.getString(1));
                    Object o[] = {rsdirectincome.getString(2), rsdirectincome.getString(1)};
                    dtm2.addRow(o);
                    dr = (dr + rsdirectincome.getInt(1));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        int directexpense = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            ResultSet rsdirectexpense = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT EXPENSES' ");

            while (rsdirectexpense.next()) {
                if (!rsdirectexpense.getString(1).equals("0")) {
//             list1.add(""+rsdirectexpense.getString(2));
//             list2.add(""+rsdirectexpense.getString(1));

                    Object o[] = {rsdirectexpense.getString(2), rsdirectexpense.getInt(1)};
                    dtm1.addRow(o);

                    cr = cr + rsdirectexpense.getInt(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        double diffss;
        System.out.println(totalcr + " : " + totaldr);

        System.out.println("" + cr);
        System.out.println("" + dr);
        double result = dr - cr;
        System.out.println("result" + result);
        if (cr >= dr) {

//                list3.addItem("GROSS LOSS ");
            diffss = (long) (cr - dr);
//                list4.addItem("" + Math.abs(diffss));

            Object o[] = {"GROSS LOSS ", Math.abs(diffss)};
            dtm2.addRow(o);
            total = diffss + dr;
            long totalamnt = (long) total;
            long cramnt = (long) cr;
            jTextField1.setText("" + totalamnt);
            jTextField2.setText("" + cramnt);
        } else {

            diffss = (long) (dr - cr);
            double diffss1 = (double) diffss;
            double sales1 = (double) sales;
            double profitpercent = (diffss1 / sales1) * 100;
            DecimalFormat df = new DecimalFormat("0.00");
            String profitpercent1 = df.format(profitpercent);

//                    list1.addItem("GROSS PROFIT "+"("+profitpercent1+"%)");
//                    list2.addItem(""+Math.abs(diffss)); 
            Object o[] = {"GROSS PROFIT " + "(" + profitpercent1 + "%)", Math.abs(diffss)};
            dtm1.addRow(o);
            long dramnt = (long) dr;
            long totalamnt = (long) total;
            jTextField1.setText("" + (dramnt));
            total = cr + diffss;
            jTextField2.setText("" + dramnt);
        }
    }

    public void trial_periodbalance() {
        try {
            connection c = new connection();
            try (Connection connect = c.cone()) {
                int i = 0;
                int j = 0;
                int lm = 0;
                String match[] = new String[500];
                String grpname[] = new String[50000];
                String match2[] = new String[500];
                String cretedgc[] = new String[500];
                match2[j] = ("durgeshk");
                cretedgc[lm] = ("null");
                String groupname = null;
                String createdgroup = null;
                ArrayList aAlreadyAddedGroupNameList = new ArrayList();
                Statement stgc = connect.createStatement();
//                ResultSet rsgcc = stgc.executeQuery("select Max(Curr_Date) from LedgerFinal where Curr_Date Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
//                while(rsgcc.next())
//                {
//                    curr_Date = rsgcc.getString(1);
//                }
                ResultSet rsgc = stgc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
                boolean aIsAdded = false;
                while (rsgc.next()) {
                    aIsAdded = false;
                    String groupname1 = rsgc.getString(1);
                    String createdgroup1 = rsgc.getString(2);
                    match[i] = groupname1;
                    if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                        groupname = null;
                        createdgroup = null;
                        match[i] = null;
                    } else {
                        groupname = groupname1;
                        createdgroup = createdgroup1;
                        if (groupname.equals("BANK ACCOUNTS") || groupname.equals("BANK OCC A/C") || groupname.equals("BANK OD A/C")
                                || groupname.equals("BRANCH/DIVISION") || groupname.equals("CAPITAL ACCOUNT") || groupname.equals("CASH IN HAND")
                                || groupname.equals("CURRENT ASSESTS") || groupname.equals("CURRENT LIABLITIES") || groupname.equals("DEPOSITS(ASSEST)")
                                || groupname.equals("DUTIES AND TAXES")
                                || groupname.equals("EXPENSES (DIRECT)") || groupname.equals("EXPENSES (INDIRECT)") || groupname.equals("INCOMCE(DIRECT)")
                                || groupname.equals("INCOMCE(INDIRECT)")
                                || groupname.equals("FIXED ASSESTS") || groupname.equals("INVESTMENTS") || groupname.equals("") || groupname.equals("")
                                || groupname.equals("") || groupname.equals("") || groupname.equals("LOANS AND ADVANCES(ASSEST)")
                                || groupname.equals("LOANS(LIABILITY)") || groupname.equals("MISC. EXPENSES(ASSEST)") || groupname.equals("PROVISIONS")
                                || groupname.equals("RESERVES AND SURPLUS") || groupname.equals("RETAINED EARNINGS")
                                || groupname.equals("SECURED LOANS") || groupname.equals("STOCK IN HAND") || groupname.equals("SUNDRY CREDITORS")
                                || groupname.equals("SUNDRY DEBITORS") || groupname.equals("SUSPENSE ACCOUNTS") || groupname.equals("UNSECURED LOANS")) {
                            if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                                Statement stgc1 = connect.createStatement();
                                ResultSet rsdk = stgc1.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                                int bal = 0;
                                int bal1 = 0;
                                int balnet = 0;
                                while (rsdk.next()) {
                                    String baltype = rsdk.getString(1);
                                    if (baltype.equals("CR")) {
                                        bal = rsdk.getInt(2);

                                    }
                                    if (baltype.equals("DR")) {
                                        bal1 = rsdk.getInt(2);

                                    }
                                }
                                if (bal > bal1) {
                                    balnet = bal - bal1;
                                    String netbalc = Integer.toString(balnet);
                                    System.out.println("netbalance main group" + netbalc);
//                                    list2.add(netbalc);
                                    crperiod = crperiod + Integer.parseInt(netbalc);
                                    System.out.println("maingroup cr is :" + crperiod);
//                                    list1.add(groupname);

                                    Object o[] = {groupname, netbalc};
                                    dtm1.addRow(o);
                                    aAlreadyAddedGroupNameList.add(groupname);
                                    aIsAdded = true;

                                } else {
                                    if (bal1 > bal) {
                                        balnet = bal1 - bal;
                                    }
                                    String netbalc1 = Integer.toString(balnet);
//                                    list4.add(netbalc1);
                                    System.out.println("netbalance main group" + netbalc1);
                                    drperiod = drperiod + Integer.parseInt(netbalc1);
                                    System.out.println("maingroup DR is correct" + drperiod);
//                                    list3.add(groupname);
                                    Object o[] = {groupname, netbalc1};
                                    dtm2.addRow(o);
                                    aAlreadyAddedGroupNameList.add(groupname);
                                    aIsAdded = true;
                                }
                            }
                        }
                        match2[j + 1] = match[i];
                        match[i] = null;
                        j++;
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error - " + ex);
        }
///////// for pals account
        try {

            connection c = new connection();
            Connection conn = c.cone();
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[500];
            String grpname[] = new String[50000];
            String match2[] = new String[500];
            String cretedgc[] = new String[500];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgc = conn.createStatement();
            ResultSet rsgc = stgc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgc.next()) {

                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME")
                            || groupname.equals("PURCHASE ACCOUNTS")
                            || groupname.equals("SALES ACCOUNTS")) //                   groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                    {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1 = conn.createStatement();
                            ResultSet rsdk = stgc1.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bal = 0;
                            int bal1 = 0;
                            int balnet = 0;
                            while (rsdk.next()) {
                                String baltype = rsdk.getString(1);
                                if (baltype.equals("CR")) {
                                    bal = rsdk.getInt(2);

                                }
                                if (baltype.equals("DR")) {
                                    bal1 = rsdk.getInt(2);

                                }
                            }
                            if (bal > bal1) {
                                balnet = bal - bal1;
                                String netbalc = Integer.toString(balnet);
//                                list4.addItem(netbalc);
                                totalperiod = totalperiod + Integer.parseInt(netbalc);

//                                list3.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bal1 > bal) {
                                    balnet = bal1 - bal;
                                }
                                String netbalc1 = Integer.toString(balnet);
//                                list2.addItem(netbalc1);
                                total1period = total1period + Integer.parseInt(netbalc1);
                                System.out.println("purchase account total dr" + total1);
//                                list1.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                    j++;
                }
            }
            if (totalperiod > total1period) {
//                list1.addItem("GROSS PROFIT : ");
                diff = (totalperiod - total1period);
//                list2.addItem("" + diff);
                totdrperiod = total1period + diff;
                System.out.println("totdr of purchase" + totdrperiod);

//                jTextField2.setText("" + (diff + total1));
//                jTextField1.setText("" + (total));
            } else {
//                list3.addItem("GROSS LOSS : ");
                diff = (total1period - totalperiod);
//                list4.addItem("" + diff);;
                totcrperiod = totalperiod + diff;
                System.out.println("totcr of purchase" + totcrperiod);
//                jTextField2.setText("" + (total1));
//                jTextField1.setText("" + (diff + total));
            }

        } catch (SQLException ex) {
            System.out.println("Error - " + ex);
        }
        try {
            connection c = new connection();
            Connection conn = c.cone();
            int ii = 0;
            int jj = 0;
            int lml = 0;
            String matchh[] = new String[500];
            String match2h[] = new String[500];
            String cretedgcc[] = new String[500];
            match2h[jj] = ("durgeshk");
            cretedgcc[lml] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgcc = conn.createStatement();
            ResultSet rsgcc = stgcc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgcc.next()) {

                aIsAdded = false;
                String groupname1 = rsgcc.getString(1);
                String createdgroup1 = rsgcc.getString(2);
                matchh[ii] = groupname1;
                if (match2h[jj].equals(matchh[ii]) || match2h[jj].equals(cretedgcc[lml])) {
                    groupname = null;
                    createdgroup = null;
                    matchh[ii] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")) {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1c = conn.createStatement();
                            ResultSet rsdkc = stgc1c.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bala = 0;
                            int bala1 = 0;
                            int balneta = 0;
                            while (rsdkc.next()) {
                                String baltype = rsdkc.getString(1);
                                if (baltype.equals("CR")) {
                                    bala = rsdkc.getInt(2);
                                }
                                if (baltype.equals("DR")) {
                                    bala1 = rsdkc.getInt(2);

                                }
                            }
                            if (bala > bala1) {
                                balneta = bala - bala1;
                                String netbalc = Integer.toString(balneta);
//                                list4.addItem(netbalc);
                                totalcrperiod = totalcrperiod + Integer.parseInt(netbalc);
                                System.out.println("total  income CR" + totalcrperiod);
//                                list3.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bala1 > bala) {
                                    balneta = bala1 - bala;
                                }
                                String netbalc1 = Integer.toString(balneta);
//                                list2.addItem(netbalc1);
                                totaldrperiod = totaldrperiod + Integer.parseInt(netbalc1);
                                System.out.println("total  income Dr" + totaldrperiod);
//                                list1.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2h[jj + 1] = matchh[ii];
                    matchh[ii] = null;
                    jj++;
                }
            }

            long diffss;
            System.out.println(totalcr + " : " + totaldr);
            if (crperiod > drperiod) {
                System.out.println("Prateek cr is" + cr);
//                list1.addItem("PROFIT AND LOSS A/C");
                difference = totalcrperiod - totaldrperiod;
//                list2.addItem("" +difference);

                Object o[] = {"PROFIT AND LOSS A/C", difference};
                dtm1.addRow(o);
                System.out.println("prateek rocks" + difference);
                jTextField2.setText("" + (difference + crperiod));
                renewcr = difference + crperiod;
//                list3.addItem("DIFF. IN OPEN. BAL. : ");
//                diffss = renewcr - drperiod;
//                list4.addItem("" + Math.abs(diffss));

                Object o1[] = {"DIFF. IN OPEN. BAL. : ", ""};
                dtm2.addRow(o1);
                
                jTextField1.setText("" + renewcr);
            } else {

                // list3.addItem("PROFIT AND LOSS A/C");
                difference = totaldrperiod - totalcrperiod;
                //  list4.addItem("" + difference);
                System.out.println("prateek rocks" + difference);
                Object o1[] = {"PROFIT AND LOSS A/C", difference};
                dtm2.addRow(o1);
                jTextField1.setText("" + (difference + drperiod));
                renewdr = difference + drperiod;
                System.out.println("Prateek renewdr" + renewdr);
                // list1.addItem("DIFF. IN OPEN. BAL. : ");
 //               diffss = renewdr - crperiod;

                // list2.addItem(""+Math.abs(diffss));


                jTextField2.setText("" + renewdr);
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error - " + ex);
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Tradingaccount1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Tradingaccount1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Tradingaccount1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Tradingaccount1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new Tradingaccount1().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private static javax.swing.JTextField jTextField1;
    private static javax.swing.JTextField jTextField2;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    // End of variables declaration//GEN-END:variables

    class PalsPrint1 implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPrintable(this);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }

        }

        java.util.List list1, list2, list3, list4;

        @Override
        public int print(Graphics g, PageFormat pf, int pageIndex)
                throws PrinterException {

            /* tell the caller that this page is part of the printed document */
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;

            }
            //set paper size 
            Paper p = pf.getPaper();
            double margin = 10;
            p.setImageableArea(margin, p.getImageableY(), p.getWidth() - 2 * margin, p.getImageableHeight());
            pf.setPaper(p);

            Graphics2D g2d = (Graphics2D) g;

            g2d.translate(pf.getImageableX(), pf.getImageableY());

            //into list
            list1 = new LinkedList<Object>();
            list2 = new LinkedList<Object>();
            list3 = new LinkedList<Object>();
            list4 = new LinkedList<Object>();

            for (int i = 0; i < jTable1.getRowCount(); i++) {
                list1.add(jTable1.getValueAt(i, 0));
                list2.add(jTable1.getValueAt(i, 1));
                list3.add(jTable2.getValueAt(i, 0));
                list4.add(jTable2.getValueAt(i, 1));
            }

            //into list
            //set font in drawing string 
            Font font = new Font("This Time Roman", Font.BOLD, 7);
            g2d.setFont(font);

            FontMetrics fm1 = g2d.getFontMetrics(font);
            g2d.drawString(HeaderAndFooter.getHeader(), 10, 50);
            g2d.drawString(HeaderAndFooter.getHeaderAddress1() + ", " + HeaderAndFooter.getHeaderAddress2(), 10, 65);
            g2d.drawString(HeaderAndFooter.getHeaderContact(), 10, 80);
            g2d.drawString("Trading Account ", 10, 95);

            g2d.drawLine(12, 105, 550, 105); //line draw 
            Font font2 = new Font("This Time Roman", Font.BOLD, 11);
            g2d.setFont(font2);
            g2d.drawString("Total :  ", 20, 750); //print total first
            g2d.drawString("Total :  ", 305, 750); //print total second
            g2d.drawLine(12, 730, 570, 730);
            g2d.drawLine(12, 760, 570, 760);

            g2d.drawString(jTextField2.getText(), 275 - fm1.stringWidth(jTextField2.getText()), 750); //print total first
            g2d.drawString(jTextField1.getText(), 550 - fm1.stringWidth(jTextField1.getText()), 750); //print total second

// g2d.drawString(pals.jLabel2.getText(),10,25); //print profit and loss current date 
            Font font1 = new Font("This Time Roman", Font.PLAIN, 11);
            g2d.setFont(font1);
            FontMetrics fm = g2d.getFontMetrics(font1);

//List 1 Print starting x-co-ordinate 10 and y-co-ordinate 30
            int xlist1 = 20;
            int ylist1 = 140;

            g2d.drawString("LIABILITIES", 15, ylist1 - 18);

            g2d.drawLine(12, ylist1 - 12, 550, ylist1 - 12);
            for (int index = 0; index < list1.size(); index++) {

                g2d.drawString(list1.get(index).toString(), xlist1, ylist1);

                ylist1 += 13;

            }//end list1

            //start list2  
            int xlist2 = 280;
            int ylist2 = 140;
            g2d.drawString("", 205, ylist2 - 18);
            for (int index = 0; index < list2.size(); index++) {

                g2d.drawString(list2.get(index).toString(), xlist2 - fm.stringWidth(list2.get(index).toString()), ylist2);
                ylist2 += 13;

            }                   //end list2

            //start list3
            int xlist3 = 300;
            int ylist3 = 140;
            g2d.drawString("ASSETS", 300, ylist3 - 18);
            for (int index = 0; index < list3.size(); index++) {

                g2d.drawString(list3.get(index).toString(), xlist3, ylist3);
                ylist3 += 13;

            }             //end list3

            //start list4
            int xlist4 = 550;
            int ylist4 = 140;
            g2d.drawString("", 470, ylist4 - 18);
            for (int index = 0; index < list4.size(); index++) {

                g2d.drawString(list4.get(index).toString(), xlist4 - fm.stringWidth(list4.get(index).toString()), ylist4);
                ylist4 += 13;
                System.out.println("ylist4 " + ylist4);
            }

            return PAGE_EXISTS;
        }
    }

}
