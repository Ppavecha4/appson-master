package reporting.accounts;
//import com.sun.glass.events.KeyEvent;
import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author prateek
 */
public class dailyreport1 extends javax.swing.JFrame implements Printable {

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());

    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    String datetoday = formatter1.format(currentDate.getTime());
    int rowcount = 0;
    int rowcount1 = 0;
    double totalsales = 0;
    double totalsalesreturn = 0;
    int openingstock = 0;
    int purchasedstock = 0;
    int purchasedreturnstock = 0;
    int closingstock = 0;
    int stocktransfer = 0;
    int stockrecieved = 0;
    int previoussales = 0;
    int previousstocktransfer = 0;
    int previouspurchasereturn = 0;
    int previoussalesreturn = 0;
    int previousstockrecieved = 0;
    public boolean isdate = false;
    public boolean isperiod = false;

    /**
     * Creates new form dailyreport
     */
    private static dailyreport1 obj = null;

    public dailyreport1() {
        initComponents();
        this.setLocationRelativeTo(null);
        jTextField7.setText("" + dateNow);
        try {

            connection c = new connection();
            try (Connection connect = c.cone()) {

                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();

                Statement st2 = connect.createStatement();

                ResultSet rs = st.executeQuery("select * from billing where date ='" + dateNow + "'");
                while (rs.next()) {
                    System.out.println("" + rs.getString(1));
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    Object d[] = {rs.getString(1), rs.getString(7), rs.getString(9)};
                    dtm.addRow(d);
                }
                rowcount = jTable1.getRowCount();
                if (rowcount == 0) {
                    jTextField2.setText("" + "");
                } else {
                    jTextField2.setText("" + rowcount);
                }

                for (int i = 0; i < rowcount; i++) {
                    totalsales = totalsales + Double.parseDouble(jTable1.getValueAt(i, 2) + "");
                }
                if (totalsales == 0) {
                    jTextField1.setText("" + "");
                } else {
                    jTextField1.setText("" + (int) totalsales);
                }

                connect.close();
                st.close();
                st1.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //for sales return
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("select * from salesreturn where date='" + dateNow + "' ");
            while (rs1.next()) {
                DefaultTableModel dtm = (DefaultTableModel) jTable3.getModel();
                Object d[] = {rs1.getString(1), rs1.getString(7), rs1.getString(9)};
                dtm.addRow(d);
            }
            rowcount1 = jTable3.getRowCount();
            if (rowcount1 == 0) {
                jTextField4.setText("" + "");
            } else {
                jTextField4.setText("" + rowcount1);
            }

            for (int i1 = 0; i1 < rowcount1; i1++) {
                totalsalesreturn = totalsalesreturn + Double.parseDouble(jTable3.getValueAt(i1, 2) + "");
            }
            if (totalsalesreturn == 0) {
                jTextField3.setText("" + "");
            } else {
                jTextField3.setText("" + (int) totalsalesreturn);
            }

            double netsales = totalsales - totalsalesreturn;

            if (netsales == 0) {
                jTextField5.setText("" + (""));
            } else {
                jTextField5.setText("" + (int) netsales);
            }

            int netquantity = rowcount - rowcount1;

            if (netquantity == 0) {
                jTextField6.setText("" + (""));
            } else {
                jTextField6.setText("" + netquantity);
            }

            connect.close();
            st.close();
            st1.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static dailyreport1 getObj() {
        if (obj == null) {
            obj = new dailyreport1();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel33 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jLabel34 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();

        jDialog1.setMinimumSize(new java.awt.Dimension(450, 125));

        jLabel31.setText("From:-");

        jLabel32.setText("To:-");

        jDateChooser1.setDateFormatString("yyyy-MM-dd");

        jDateChooser2.setDateFormatString("yyyy-MM-dd");

        jButton1.setText("GO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel31)
                        .addComponent(jLabel32)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(220, 130));

        jLabel33.setText("Date: -");

        jDateChooser3.setDateFormatString("yyyy-MM-dd");

        jButton2.setText("GO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateChooser3, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(36, 36, 36))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DAILY REPORT");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("TAG WISE SALES REPORT");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Sales");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bill No.", "Stock No.", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
        }

        jLabel5.setText("TOTAL: -");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Sales Return");

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bill No.", "Stock No.", " Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(jTable3);
        if (jTable3.getColumnModel().getColumnCount() > 0) {
            jTable3.getColumnModel().getColumn(0).setResizable(false);
            jTable3.getColumnModel().getColumn(1).setResizable(false);
            jTable3.getColumnModel().getColumn(2).setResizable(false);
        }

        jLabel8.setText("TOTAL: -");

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel34.setText("NET SALES :-");

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });

        jTextField3.setEditable(false);
        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jTextField4.setEditable(false);
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jTextField5.setEditable(false);
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jTextField6.setEditable(false);
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jTextField7.setEditable(false);
        jTextField7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField7MouseClicked(evt);
            }
        });
        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
        });

        jButton3.setText("Print");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(25, 25, 25)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(89, 89, 89)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 1, Short.MAX_VALUE)
                                .addComponent(jButton3)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 301, Short.MAX_VALUE)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(161, 161, 161)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(138, 138, 138)
                                .addComponent(jLabel7))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(177, 177, 177)
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 389, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton3))
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(68, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 40, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 645, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }


    }//GEN-LAST:event_jTable1KeyPressed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }


    }//GEN-LAST:event_jPanel1KeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }


    }//GEN-LAST:event_jLabel1KeyPressed

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);
        }
    }//GEN-LAST:event_jTable3KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        jDialog1.dispose();

        rowcount = 0;
        rowcount1 = 0;
        totalsales = 0;
        totalsalesreturn = 0;
        openingstock = 0;
        purchasedstock = 0;
        purchasedreturnstock = 0;
        closingstock = 0;
        stocktransfer = 0;
        stockrecieved = 0;
        previoussales = 0;
        previousstocktransfer = 0;
        previouspurchasereturn = 0;
        previoussalesreturn = 0;
        previousstockrecieved = 0;

        isperiod = true;
        isdate = false;

        Date date11 = jDateChooser1.getDate();
        String date1 = formatter.format(date11);
        Date date12 = jDateChooser2.getDate();
        String date2 = formatter.format(date12);
        System.out.println("date1 is" + date1);
        System.out.println("date2 is" + date2);
        jTextField7.setText("" + formatter1.format(date12));

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from billing where date between '" + date1 + "' and '" + date2 + "' ");
            while (rs.next()) {

                Object d[] = {rs.getString(1), rs.getString(7), rs.getInt(9)};
                dtm.addRow(d);
            }
            rowcount = jTable1.getRowCount();
            if (rowcount == 0) {
                jTextField2.setText("" + "");
            } else {
                jTextField2.setText("" + rowcount);
            }

            for (int i = 0; i < rowcount; i++) {
                totalsales = totalsales + Double.parseDouble(jTable1.getValueAt(i, 2) + "");
            }

            if (totalsales == 0) {
                jTextField1.setText("" + "");
            } else {
                jTextField1.setText("" + (int) totalsales);
            }
            connect.close();
            st.close();
            st1.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        //for sales return
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("select * from salesreturn where date between '" + date1 + "' and '" + date2 + "' ");
            while (rs1.next()) {
                Object d[] = {rs1.getString(1), rs1.getString(7), rs1.getString(9)};
                dtm.addRow(d);
            }
            rowcount1 = jTable3.getRowCount();

            if (rowcount1 == 0) {
                jTextField4.setText("" + "");
            } else {
                jTextField4.setText("" + rowcount1);
            }

            for (int i1 = 0; i1 < rowcount1; i1++) {
                System.out.println("salesreturn" + totalsalesreturn);
                totalsalesreturn = totalsalesreturn + Double.parseDouble(jTable3.getValueAt(i1, 2) + "");
                System.out.println("" + totalsalesreturn);
            }
            if (totalsalesreturn == 0) {
                jTextField3.setText("" + "");
            } else {
                jTextField3.setText("" + (int) totalsalesreturn);
            }

            double netsales = totalsales - totalsalesreturn;
            if (netsales == 0) {
                jTextField5.setText("" + "");
            } else {
                jTextField5.setText("" + (int) netsales);
            }

            int netquantity = rowcount - rowcount1;

            if (netquantity == 0) {
                jTextField6.setText("" + (""));
            } else {
                jTextField6.setText("" + netquantity);
            }

            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();

        rowcount = 0;
        rowcount1 = 0;
        totalsales = 0;
        totalsalesreturn = 0;
        openingstock = 0;
        purchasedstock = 0;
        purchasedreturnstock = 0;
        closingstock = 0;
        stocktransfer = 0;
        stockrecieved = 0;
        previoussales = 0;
        previousstocktransfer = 0;
        previouspurchasereturn = 0;
        previoussalesreturn = 0;
        previousstockrecieved = 0;

        isdate = true;
        isperiod = false;

        jDialog2.dispose();

        Date date11 = jDateChooser3.getDate();
        String date1 = formatter.format(date11);
        jTextField7.setText("" + formatter1.format(date11));

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from billing where date ='" + date1 + "'  ");
            while (rs.next()) {

                Object d[] = {rs.getString(1), rs.getString(7), rs.getInt(9)};
                dtm.addRow(d);
            }
            rowcount = jTable1.getRowCount();
            if (rowcount == 0) {
                jTextField2.setText("" + "");
            } else {
                jTextField2.setText("" + rowcount);
            }

            for (int i = 0; i < rowcount; i++) {
                totalsales = totalsales + Double.parseDouble(jTable1.getValueAt(i, 2) + "");
            }
            if (totalsales == 0) {
                jTextField1.setText("" + "");
            } else {
                jTextField1.setText("" + (int) totalsales);
            }
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //for sales return
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("select * from salesreturn where date = '" + date1 + "'  ");
            while (rs1.next()) {
                Object d[] = {rs1.getString(1), rs1.getString(7), rs1.getString(9)};
                dtm.addRow(d);
            }
            rowcount1 = jTable3.getRowCount();
            if (rowcount1 == 0) {
                jTextField4.setText("" + "");
            } else {
                jTextField4.setText("" + rowcount1);
            }

            for (int i1 = 0; i1 < rowcount1; i1++) {
                System.out.println("salesreturn" + totalsalesreturn);
                totalsalesreturn = totalsalesreturn + Double.parseDouble(jTable3.getValueAt(i1, 2) + "");
                System.out.println("" + totalsalesreturn);
            }
            if (totalsalesreturn == 0) {
                jTextField3.setText("" + "");
            } else {
                jTextField3.setText("" + (int) totalsalesreturn);
            }

            double netsales = totalsales - totalsalesreturn;

            if (netsales == 0) {
                jTextField5.setText("" + "");
            } else {
                jTextField5.setText("" + (int) netsales);
            }

            int netquantity = rowcount - rowcount1;

            if (netquantity == 0) {
                jTextField6.setText("" + (""));
            } else {
                jTextField6.setText("" + netquantity);
            }

            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField7MouseClicked
        // TODO add your handling code here:

        jDialog2.setVisible(true);
        jDialog2.setBounds(850, 50, 210, 110);
    }//GEN-LAST:event_jTextField7MouseClicked

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jTextField7KeyPressed

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jTextField2KeyPressed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }


    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }

    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }


    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(850, 50, 450, 125);
        }

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(850, 50, 210, 110);

        }


    }//GEN-LAST:event_jTextField5KeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        int tb1 = jTable1.getRowCount();
        int tb2 = jTable3.getRowCount();
        if (tb1 > 0 || tb2 > 0) {
            PrinterJob job = PrinterJob.getPrinterJob();
            Paper paper = new Paper();
            PageFormat pf = job.defaultPage();
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            job.setPrintable(this);
            boolean b = job.printDialog();
            try {
                if (b == true) {
                    job.print();
                }
            } catch (PrinterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "No Value To print...");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dailyreport1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dailyreport1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dailyreport1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dailyreport1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new dailyreport1().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    // End of variables declaration//GEN-END:variables

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }
        // get table row
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        int columncount = jTable1.getColumnCount();

        int x1 = 4;
        int x2 = 250;
        int x3 = 460;
        double width = pageFormat.getImageableWidth();
        System.out.println("width" + width);
        Graphics2D g2d = (Graphics2D) graphics;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        Font font1 = new Font("", Font.BOLD, 12);
        g2d.setFont(font1);
        // header section start 
        g2d.drawString(HeaderAndFooter.getHeader(), 0, 9);

        Font font2 = new Font("", Font.PLAIN, 9);
        g2d.setFont(font2);
        // g2d.drawString(jTextField1.getText().trim(), 410, 10);
        g2d.drawString(HeaderAndFooter.getHeaderAddress1(), 0, 20);
        g2d.drawString(HeaderAndFooter.getHeaderAddress2(), 0, 30);
        g2d.drawString(HeaderAndFooter.getHeaderContact(), 0, 40);
        //header section end 
        Font font3 = new Font("", Font.BOLD, 10);
        g2d.setFont(font3);
        //table lable 
        g2d.drawString(jLabel1.getText().trim(), 180, 60);
        g2d.drawString("As on : " + jTextField7.getText().trim(), 200, 72);
        g2d.drawString(jLabel2.getText().trim(), 0, 90);
        // First table start 

        int x = 4;
        int y = 110;
        int endWidthX = 465;
        int endLine = 0;

        // get column and print
        for (int i = 0; i < columncount; i++) {
            if (i == 1) {
                x = 208;
            }
            if (i == 2) {
                x = 420;
            }
            g2d.drawString(jTable1.getColumnName(i), x, y);
            System.out.println(x);
        }
        // draw line before column 
        g2d.drawLine(0, y - 10, endWidthX, y - 10);
        //draw line after column name 
        g2d.drawLine(0, y + 5, endWidthX, y + 5);

        Font font4 = new Font("", Font.PLAIN, 10);
        g2d.setFont(font4);
        FontMetrics fm4 = g2d.getFontMetrics(font4);
        y = y + 15;
        if (rowcount > 0) {
            //table row print 
            for (int i = 0; i < rowcount; i++) {
                int j = 17;
                System.out.println(j);
                g2d.drawString(jTable1.getValueAt(i, 0) + "", x1, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", x2 - fm4.stringWidth(jTable1.getValueAt(i, 1).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 2) + "", x3 - fm4.stringWidth(jTable1.getValueAt(i, 2).toString()), y + (j * i));
                endLine = (120 + (j * i)) + 2;
            }
        } else {
            endLine = y + 20;
        }

        // draw line vertical left-top to bottom alignment of table
        // g2d.drawLine(0, 75, 0, endLine + 15);
        //draw line vertical right-top to bottom alignment of table
        //  g2d.drawLine(460, 75, 460, endLine + 15);
        // draw line horizontal left-right alignment of table after total  
        g2d.drawLine(0, endLine + 25, endWidthX, endLine + 25);

        // draw line horizontal left-right alignment of table before total 
        g2d.drawLine(0, endLine + 8, endWidthX, endLine + 8);

        g2d.setFont(font3);
        FontMetrics fm3 = g2d.getFontMetrics(font3);
        //total print first table
        g2d.drawString("TOTAL : ", x1, endLine + 20);
        g2d.drawString(jTextField2.getText().trim(), x2 - fm3.stringWidth(jTextField2.getText().trim()) - 15, endLine + 20);
        g2d.drawString(jTextField1.getText().trim(), x3 - fm3.stringWidth(jTextField1.getText().trim()), endLine + 20);

        //End of First Table
        //Start Second Table
        // table lable 
        endLine = endLine + 15;

        g2d.drawString(jLabel7.getText().trim(), 0, endLine + 50);

        int afterTable1PointY = endLine + 58;

        DefaultTableModel dtm2 = (DefaultTableModel) jTable3.getModel();
        int rowcount1 = jTable3.getRowCount();
        int columncount1 = jTable3.getColumnCount();

        System.out.println("row count " + rowcount1);
        int endLine1 = 0;
        int x11 = 4;
        int pointY = afterTable1PointY + 10 + 20;
        for (int i = 0; i < columncount1; i++) {
            if (i == 1) {
                x11 = 208;
            }
            if (i == 2) {
                x11 = 420;
            }
            g2d.drawString(jTable3.getColumnName(i), x11, afterTable1PointY + 10);

            System.out.println(x11);
        }

        //draw after column   
        g2d.drawLine(0, afterTable1PointY + 15, endWidthX, afterTable1PointY + 15);
        //draw before column
        g2d.drawLine(0, afterTable1PointY, endWidthX, afterTable1PointY);
        if (rowcount1 > 0) {
            // print table column

            g2d.setFont(font4);

            // print table row 
            for (int i = 0; i < rowcount1; i++) {
                int j = 17;
                System.out.println(j);
                g2d.drawString(jTable3.getValueAt(i, 0) + "", x1, pointY + (j * i));
                g2d.drawString(jTable3.getValueAt(i, 1) + "", x2 - fm4.stringWidth(jTable3.getValueAt(i, 1).toString()), pointY + (j * i));
                g2d.drawString(jTable3.getValueAt(i, 2) + "", x3 - fm4.stringWidth(jTable3.getValueAt(i, 2).toString()), pointY + (j * i));

                endLine1 = (pointY + (j * i)) + 2;
                System.out.println("end line1" + endLine1);
                // g2d.drawLine(40,endLine1,440,endLine1);
            }
        } else {
            endLine1 = pointY;
        }
        //drawing table border
        // g2d.drawLine(0, afterTable1PointY, 0, endLine1 + 15);

        //g2d.drawLine(460, afterTable1PointY,460, endLine1 + 15);
        //draw line after total
        g2d.drawLine(0, endLine1 + 18, endWidthX, endLine1 + 18);
        //draw line before total
        g2d.drawLine(0, endLine1 + 4, endWidthX, endLine1 + 4);

        g2d.setFont(font3);
        //second table total print 
        g2d.drawString("TOTAL : ", x1, endLine1 + 15);
        g2d.drawString(jTextField4.getText().trim(), x2 - fm3.stringWidth(jTextField4.getText().trim()) - 15, endLine1 + 15);
        g2d.drawString(jTextField3.getText().trim(), x3 - fm3.stringWidth(jTextField3.getText().trim()), endLine1 + 15);

        int ypoint = endLine1 + 15 + 20;
        //net sale printing 
        g2d.drawString("NET SALE : ", x1, ypoint + 20);
        g2d.drawString(jTextField6.getText().trim(), x2 - fm3.stringWidth(jTextField6.getText().trim()) - 15, ypoint + 20);
        g2d.drawString(jTextField5.getText().trim(), x3 - fm3.stringWidth(jTextField5.getText().trim()), ypoint + 20);

        g2d.drawLine(0, ypoint + 7, endWidthX, ypoint + 7);
        g2d.drawLine(0, ypoint + 25, endWidthX, ypoint + 25);
        return PAGE_EXISTS;
    }
}
