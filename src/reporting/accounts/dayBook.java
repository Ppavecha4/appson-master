package reporting.accounts;

import accounting.transaction.Contra;
import accounting.transaction.UpdateJournal;
import accounting.transaction.bankpayment;
import accounting.transaction.bankreciept;
import accounting.transaction.journ;
import accounting.transaction.cashpayment;
import accounting.transaction.cashreciept;
import accounting.transaction.interestentry;
import connection.connection;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import transaction.New_Dual_SalesReturn;
import transaction.Pur_trans_dualGst;
import transaction.bsnlbilling;

public class dayBook extends javax.swing.JFrame {

    public String type = null;
    static String str = null;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    int row = 1;
    int selected = 0;

    public dayBook() {
        initComponents();
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    jDialog1.setVisible(true);
                }
            }
        });
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement st = conn.createStatement();
            Statement std = conn.createStatement();
            Statement stp = conn.createStatement();
            Statement strr = conn.createStatement();
            if (str != null) {
                dateNow = str;
            }
            //for contra setting 
            String matchsno4 = "test";
            ResultSet rss = st.executeQuery("select * from Contra where v_date = '" + dateNow + "'");
            while (rss.next()) {
                String matchsno1 = rss.getString(1);

                if (matchsno4.equals(matchsno1)) {

                } else {
                    String match = (matchsno1);
                    Object o[] = {rss.getString(2), rss.getString(3), "contra", match, null, rss.getString(5)};
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.addRow(o);

                    matchsno4 = matchsno1;
                }
            }
            //for journal seeting
            matchsno4 = "test";
            ResultSet rssdg = std.executeQuery("select * from journal where da_te = '" + dateNow + "' ");
            while (rssdg.next()) {
                String matchsno1 = rssdg.getString(2);

                if (matchsno4.equals(matchsno1)) {

                } else {
                    String match = (matchsno1);
                    Object oo[] = {rssdg.getString(3), rssdg.getString(4), "journal", match, rssdg.getString(7), rssdg.getString(8)};
                    DefaultTableModel dtmm = (DefaultTableModel) jTable1.getModel();
                    dtmm.addRow(oo);
                    matchsno4 = matchsno1;
                }
            }
            //for payment setting 
            matchsno4 = "test";
            ResultSet rssp = stp.executeQuery("select * from payment where da_te = '" + dateNow + "'");
            while (rssp.next()) {
                String matchsno1 = rssp.getString(1);

                if (matchsno4.equals(matchsno1)) {

                } else {
                    String match = (matchsno1);
                    Object o[] = {rssp.getString(2), rssp.getString(3), "payment", match, rssp.getString(5), null};
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }
            //for recipt setting 
            matchsno4 = "test";
            ResultSet rssr = strr.executeQuery("select * from receipt where da_te = '" + dateNow + "'");
            while (rssr.next()) {
                String matchsno1 = rssr.getString(1);

                if (matchsno4.equals(matchsno1)) {

                } else {
                    String match = (matchsno1);
                    Object o[] = {rssr.getString(2), rssr.getString(3), "receipt", match, rssr.getString(5), null};
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }
            matchsno4 = "test";
            ResultSet rsspu = strr.executeQuery("select * from Stock where Invoice_date = '" + dateNow + "'");
            while (rsspu.next()) {
                String matchsno1 = (rsspu.getString(1));

                if (matchsno4.equals(matchsno1)) {

                } else {
                    int match = Integer.parseInt(matchsno1);
                    Object o[] = {rsspu.getString(3), rsspu.getString(2), "purchase", match, rsspu.getString(21), null};
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }
            Statement Purchasereturn = conn.createStatement();
            matchsno4 = "test";
            ResultSet purreturn = Purchasereturn.executeQuery("select * from PurchaseReturn where Invoice_date = '" + dateNow + "'");
            while (purreturn.next()) {
                String matchsno1 = purreturn.getString(5);

                if (matchsno4.equals(matchsno1)) {

                } else {
                    int match = Integer.parseInt(matchsno1);
                    Object o[] = {purreturn.getString(4), purreturn.getString(26), "purchase return", match, null, purreturn.getString(22)};
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }
            matchsno4 = "test";
            Statement sales = conn.createStatement();
            ResultSet rssales = sales.executeQuery("select * from billing where date = '" + dateNow + "' ");
            while (rssales.next()) {
                String matchsno1 = (rssales.getString(1));

                if (matchsno4.equals(matchsno1)) {

                } else {
                    String match = (matchsno1);
                    Object oo[] = {rssales.getString(2), rssales.getString(25), "sales", match, rssales.getString(22), ""};
                    DefaultTableModel dtmm = (DefaultTableModel) jTable1.getModel();
                    dtmm.addRow(oo);
                    matchsno4 = matchsno1;
                }
            }
            matchsno4 = "test";
            Statement salesreturn = conn.createStatement();
            ResultSet rssalesreturn = salesreturn.executeQuery("select * from salesreturn where date = '" + dateNow + "' ");
            while (rssalesreturn.next()) {
                String matchsno1 = String.valueOf(rssalesreturn.getInt(1));

                if (matchsno4.equals(matchsno1)) {

                } else {
                    int match = Integer.parseInt(matchsno1);
                    Object oo[] = {rssalesreturn.getString(2), rssalesreturn.getString(26), "sales return", match, "", rssales.getString(22)};
                    DefaultTableModel dtmm = (DefaultTableModel) jTable1.getModel();
                    dtmm.addRow(oo);
                    matchsno4 = matchsno1;
                }
            }

            st.close();
            std.close();
            stp.close();
            strr.close();
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        jTable1.setEditingColumn(-1);
        jTable1.setEditingRow(-1);
        jTable1.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent theMouseEvent) {
                if (theMouseEvent.getClickCount() == 2) {

                    System.out.println("Mouse clicked ");
                    ResultSet rss, rss1, rss2, rss3 = null;
                    Statement st, st1 = null, st2, st3 = null;
                    DefaultTableModel defaultTableModel = (DefaultTableModel) jTable1.getModel();
                    int row = jTable1.getSelectedRow();
                    type = (String) jTable1.getValueAt(row, 2);
////Working for Contra     
                    govind(type, 0);
                }
            }
        });

    }

    public String govind(String nam, int a) {
        int aColumnNo = 0;
        String date = null;
        String sno = null;
        int selected;
        DefaultTableModel dtm;
        String type = nam;
        if (a == 1) {
            aColumnNo = trialBalance.table.getSelectedColumn();
            // trialBalance balance=new trialBalance();   
            selected = trialBalance.table.getSelectedRow();
            dtm = (DefaultTableModel) trialBalance.table.getModel();
            sno = ("" + dtm.getValueAt(selected, 1));
            date = dtm.getValueAt(selected, 0).toString();
        } else {
            aColumnNo = jTable1.getSelectedColumn();
            selected = jTable1.getSelectedRow();

            dtm = (DefaultTableModel) jTable1.getModel();
            sno = ("" + dtm.getValueAt(selected, 3));
            date = dtm.getValueAt(selected, 0).toString();
        }

        /*For Contra*/
        if (type.equals("CO")) {
            Statement st, st1, st2, st3 = null;
            try {
                Contra cnt = new Contra();
                cnt.show();
                cnt.jButton1.setVisible(false);
                cnt.jButton3.setVisible(true);
                connection c = new connection();
                Connection conn = c.cone();
                st = conn.createStatement();
                st1 = conn.createStatement();
                st2 = conn.createStatement();
                st3 = conn.createStatement();

                cnt.jTextField1.setText("" + sno);
                cnt.jDateChooser1.setDate(formatter.parse(date));

                ResultSet rss = st.executeQuery("select * from Contra where s_no = '" + sno + "'");
                //  if (aColumnNo >= 0) 
                {
                    String by = null, to = null;
                    while (rss.next()) {
                        by = rss.getString(3);
                        to = rss.getString(4);
                        cnt.jTextField3.setText("" + rss.getString(5));
                        cnt.jTextArea1.setText("" + rss.getString(6));
                    }
                    String ss = ("" + dtm.getValueAt(selected, 3));
                    ResultSet rss1 = st1.executeQuery("select * from ledger where ledger_name='" + by + "'");
                    ResultSet rss2 = st2.executeQuery("select * from ledger where ledger_name='" + to + "'");
                    ResultSet rss3 = st3.executeQuery("select * from Contra where s_no='" + sno + "'");
                    while (rss1.next()) {
                        cnt.choice1.addItem(by);
                        cnt.choice2.addItem(to);
                        cnt.jTextField5.setText("" + rss1.getString(10));
                        cnt.jLabel6.setText("" + rss1.getString(11));
                    }
                    while (rss2.next()) {
                        cnt.choice1.select("" + dtm.getValueAt(selected, 1));
                        cnt.choice2.select(to);
                        cnt.jTextField4.setText("" + rss2.getString(10));
                        cnt.jLabel11.setText("" + rss2.getString(11));
                    }
                    while (rss3.next()) {
                        cnt.choice2.select("" + rss3.getString(4));
                        cnt.choice1.select("" + rss3.getString(3));

                    }

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        /*Working for journal  */
        if (type.equals("JR")) {
            try {
                int count = 0;

                journ jou = new journ();
                jou.show();
                jou.jButton1.setVisible(false);
                jou.jButton3.setVisible(true);
                jou.a = 1;
                try {
                    connection c = new connection();
                    Connection conn = c.cone();
                    Statement st0 = conn.createStatement();
                    Statement stg = conn.createStatement();
                    Statement st13 = conn.createStatement();
                    journ.SecondRow aRow = null, aPreviousRow = null;
                    Map<Integer, UpdateJournal> updateJourn = new LinkedHashMap<Integer, UpdateJournal>();
                    String snoo = sno;
                    System.out.println("" + snoo);
                    String by = null, to = null;
                    ResultSet rs1 = st13.executeQuery("select * from journal where s_no='" + snoo + "'");
                    while (rs1.next()) {
                        UpdateJournal updateJ = new UpdateJournal();
                        updateJ.setVsn(rs1.getInt(1));
                        updateJ.setBranchid(rs1.getString(9));
                        updateJ.setByledger(rs1.getString(4));
                        updateJ.setCreditamnt(rs1.getString(8));
                        updateJ.setDebitamnt(rs1.getString(7));
                        updateJ.setNarration(rs1.getString(6));
                        updateJ.setSno(rs1.getString(2));
                        updateJ.setToledger(rs1.getString(5));
                        updateJ.setDate(rs1.getString(3));
                        updateJourn.put(updateJ.getVsn(), updateJ);
                        jou.jTextArea1.setText(rs1.getString(6));
                        count++;
                        if (count == 1) {
                            String byname = rs1.getString(4);
                            String toname = rs1.getString(5);
                            String debita = rs1.getString(7);
                            String creadita = rs1.getString(8);

                            if (debita.length() > 1) {
                                jou.jtf1.setText("" + debita);
                                jou.jtf1.setEnabled(true);
                            } else {
                                jou.jtf2.setText("" + creadita);
                                jou.jtf2.setEnabled(true);
                            }
                            if (byname.length() > 1) {
                                jou.jcb2.addItem("" + byname);
                                jou.jcb2.setSelectedItem("" + byname);
                                jou.jcb1.setSelectedItem("BY");
                                jou.jcb1.setEnabled(true);
                            } else {
                                jou.jcb1.setSelectedItem("TO");
                                jou.jcb2.addItem("" + toname);
                                jou.jcb2.setSelectedItem("" + toname);
                                jou.jcb2.setEnabled(true);
                            }

                            ResultSet balance = st0.executeQuery("select * from ledger where ledger_name='" + byname + "'");
                            while (balance.next()) {
                                jou.jcl2.setText("" + balance.getString(10));
                                jou.jcl3.setText("" + balance.getString(11));
                            }
                        } else {
                            String bynames = rs1.getString(4);
                            String tonames = rs1.getString(5);
                            String debitas = rs1.getString(7);
                            String creaditas = rs1.getString(8);

                            System.out.println("bynames " + bynames + " tonames " + tonames + " debitas " + debitas + " creaditas " + creaditas);
                            jou.point1 = 2;
                            aPreviousRow = aRow;
                            aRow = jou.createAnotherRow();
                            if (aPreviousRow == null) {
                                jou.aFirstRow.aSecondRow = aRow;
                            }

                            ResultSet rs2 = st0.executeQuery("select ledger_name from ledger order by ledger_name ASC  ");

                            while (rs2.next()) {
                                aRow.jAccountNameCobmoBox.addItem(rs2.getString(1));
                            }

                            if (debitas.trim().length() > 0) {
                                System.out.println("" + debitas.length());
                                aRow.jDebitTextField.setText("" + debitas);
                                aRow.jDebitTextField.setEnabled(true);
                            }
                            System.out.println("creaditas.trim().length() " + creaditas.trim().length());
                            if (creaditas.trim().length() > 0) {
                                aRow.jCreditTextField.setText("" + creaditas);
                                aRow.jCreditTextField.setEnabled(true);
                            }
                            if (creaditas.trim().length() > 0) {
                                aRow.jToByComboBox.setSelectedItem("BY");
                                aRow.jAccountNameCobmoBox.addItem("" + tonames);
                                aRow.jAccountNameCobmoBox.setSelectedItem("" + tonames);
                                aRow.jToByComboBox.setEnabled(true);
                            } else {
                                aRow.jToByComboBox.setSelectedItem("TO");
                                aRow.jAccountNameCobmoBox.addItem("" + bynames);
                                aRow.jAccountNameCobmoBox.setSelectedItem("" + bynames);
                                aRow.jAccountNameCobmoBox.setEnabled(true);
                            }
                            ResultSet nextbalance = stg.executeQuery("select * from ledger where ledger_name='" + bynames + "' ");
                            while (nextbalance.next()) {
                                aRow.jNameLabel.setText("CURRENT BAL : -  ");
                                aRow.jBalenceLabel.setText("" + nextbalance.getString(10));
                                aRow.jBalenceTypeLabel.setText("" + nextbalance.getString(11));
                            }
                            ResultSet nextbal = stg.executeQuery("select * from ledger where ledger_name='" + tonames + "' ");
                            while (nextbal.next()) {
                                aRow.jNameLabel.setText("CURRENT BAL : -  ");
                                aRow.jBalenceLabel.setText("" + nextbal.getString(10));
                                aRow.jBalenceTypeLabel.setText("" + nextbal.getString(11));
                            }

                            if (aPreviousRow != null) {
                                aPreviousRow.isRowAlreadyCreated = true;
                                aPreviousRow.aNewRow = aRow;
                            }
                        }
                    }
                    conn.close();
                    jou.setUpdateJourn(updateJourn);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                jou.jTextField1.setText("" + sno);
                jou.jDateChooser1.setDate(formatter.parse(date));

            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }

        /* working for interest entry   */
        if (type.equals("IST")) {
            Statement st, st1, st2, st3 = null;
            try {
                interestentry ie = new interestentry();
                ie.show();
                ie.jButton1.setVisible(false);
                ie.jButton3.setVisible(true);
                connection c = new connection();
                Connection conn = c.cone();
                st = conn.createStatement();
                st1 = conn.createStatement();
                st2 = conn.createStatement();
                st3 = conn.createStatement();

                ie.jTextField1.setText("" + sno);
                ie.jDateChooser1.setDate(formatter.parse(date));
                ResultSet rss = st.executeQuery("select * from interestentry where s_no = '" + sno + "'");
                {
                    String by = null, to = null;
                    while (rss.next()) {
                        by = rss.getString("by_ledger");
                        to = rss.getString("to_ledger");
                        if (to.equals("INTREST A/C")) {
                            ie.jTextField4.setText("" + rss.getString("amnt"));
                        }
                        if (by.equals("TDS PAYABLE")) {
                            ie.jTextField5.setText("" + rss.getString("amnt"));
                        }
                        ie.jTextArea1.setText("" + rss.getString("narration"));
                        ie.jComboBox1.setSelectedItem("" + rss.getString("Party_name"));
                    }
                    String ss = ("" + dtm.getValueAt(selected, 3));
                    ResultSet rss1 = st1.executeQuery("select * from ledger where ledger_name='" + by + "'");
                    ResultSet rss2 = st2.executeQuery("select * from ledger where ledger_name='" + to + "'");

                    while (rss2.next()) {
                        ie.jLabel8.setText("" + rss2.getString(11));
                    }

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        /* working for bsnlbilling entry */
        if (type.equals("BSL")) {
            Statement st = null;
            try {
                bsnlbilling bb = new bsnlbilling();
                bb.show();
                bb.jButton1.setVisible(false);
                bb.jButton3.setVisible(true);
                connection c = new connection();
                Connection conn = c.cone();
                st = conn.createStatement();

                bb.jTextField1.setText("" + sno);
                bb.jDateChooser1.setDate(formatter.parse(date));

                ResultSet rs = st.executeQuery("select * from bsnlbilling where s_no = '" + sno + "'");
                {
                    while (rs.next()) {
                        bb.jTextField3.setText("" + rs.getString("customer_name"));
                        bb.jTextField4.setText("" + rs.getString("amnt"));
                        bb.jTextField5.setText("" + rs.getString("disc_amnt"));
                        bb.jTextField6.setText("" + rs.getString("net_amnt"));
                        bb.jTextArea1.setText("" + rs.getString("narration"));
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        /*  Working for payment  */
        if (type.equals("PAY")) {
            try {
                int count = 0;
                LinkedHashMap<String, Double> ledgerNameGrandTotalMap = new LinkedHashMap<String, Double>();
                cashpayment pay = new cashpayment();
                pay.show();
                pay.jButton1.setVisible(false);
                pay.jButton3.setVisible(true);
                String byledger = "";
                try {
                    connection c = new connection();
                    Connection conn = c.cone();
                    Statement st0 = conn.createStatement();
                    Statement stg = conn.createStatement();
                    Statement st00 = conn.createStatement();
                    ResultSet rs00;
                    if (a == 1) {
                        rs00 = st00.executeQuery(" select * from ledger where ledger_name='" + trialBalance.table.getValueAt(row, 1) + "'");
                    } else {
                        rs00 = st00.executeQuery(" select * from ledger where ledger_name='" + jTable1.getValueAt(row, 1) + "'");

                    }
                    cashpayment.SecondRow aRow = null, aPreviousRow = null;
                    while (rs00.next()) {
                        pay.jTextField5.setText("" + rs00.getString(10));
                        pay.jLabel8.setText("" + rs00.getString(11));
                    }
                    Statement stpst = conn.createStatement();
                    String snoo = sno;
                    System.out.println("" + snoo);
                    ResultSet pst = stpst.executeQuery("select to_ledger,amo_unt,narra_tion,grand_total,by_ledger from payment where sno='" + snoo + "' ");
                    while (pst.next()) {
                        byledger = pst.getString("by_ledger");
                        pay.jTextArea1.setText(pst.getString(3));
                        pay.jTextField4.setText(pst.getString(4));
                        double grandTotal = 0;
                        if (pst.getString(4) != null && !pst.getString(4).trim().isEmpty()) {
                            try {
                                grandTotal = Double.parseDouble(pst.getString(4));
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                        count++;
                        if (count == 1) {
                            String name = pst.getString(1);
                            ledgerNameGrandTotalMap.put(name, grandTotal);
                            pay.jcb1.setSelectedItem("" + name);
                            pay.jtf1.setText(pst.getString(2));
                            pay.jtf1.setEnabled(true);
                            ResultSet balance = st0.executeQuery("select * from ledger where ledger_name='" + name + "'");
                            while (balance.next()) {
                                pay.jcl17.setText("" + balance.getString(10));
                                pay.jcl18.setText("" + balance.getString(11));
                            }
                        } else {
                            pay.point1 = 2;
                            aPreviousRow = aRow;
                            aRow = pay.createAnotherRow();
                            if (aPreviousRow == null) {
                                pay.aFirstRow.aSecondRow = aRow;
                            }
                            String nextname = pst.getString(1);
                            aRow.jAccountNameCobmoBox.setSelectedItem(nextname);
                            aRow.jTextField.setText(pst.getString(2));
                            aRow.jTextField.setEnabled(true);
                            ResultSet nextbalance = stg.executeQuery("select * from ledger where ledger_name='" + nextname + "'");
                            while (nextbalance.next()) {
                                aRow.jcl47.setText("" + nextbalance.getString(10));
                                aRow.jcl48.setText("" + nextbalance.getString(11));
                            }
                            if (aPreviousRow != null) {
                                aPreviousRow.isRowAlreadyCreated = true;
                                aPreviousRow.aNewRow = aRow;
                            }
                        }
                    }
                    conn.close();
                } catch (Exception ex) {

                    ex.printStackTrace();

                }
                pay.choice1.addItem("" + byledger);
                pay.choice1.select("" + byledger);
                pay.jTextField1.setText("" + sno);
                pay.jDateChooser1.setDate(formatter.parse(date));
                pay.setLedgerNameGrandTotalMapForUpdate(ledgerNameGrandTotalMap);
            } catch (ParseException ex) {
                Logger.getLogger(dayBook.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /*  Working for bank payment */
        if (type.equals("BPAY")) {
            try {
                int count = 0;
                LinkedHashMap<String, Double> ledgerNameGrandTotalMap = new LinkedHashMap<String, Double>();
                bankpayment bpay = new bankpayment();
                bpay.show();
                bpay.jButton1.setVisible(false);
                bpay.jButton3.setVisible(true);
                bpay.a = 1;
                String byledger = "";
                try {
                    connection c = new connection();
                    Connection conn = c.cone();
                    Statement st0 = conn.createStatement();
                    Statement stg = conn.createStatement();
                    Statement st1 = conn.createStatement();

                    bankpayment.SecondRow aRow = null, aPreviousRow = null;
                    Statement stpst = conn.createStatement();
                    String snoo = sno;
                    System.out.println("" + snoo);
                    ResultSet pst = stpst.executeQuery("select to_ledger,amo_unt,narra_tion,grand_total,by_ledger,cheque from bankpayment where sno='" + snoo + "' ");
                    while (pst.next()) {
                        byledger = pst.getString("by_ledger");
                        bpay.jTextArea1.setText(pst.getString(3));
                        bpay.jTextField4.setText(pst.getString(4));
                        double grandTotal = 0;
                        if (pst.getString(4) != null && !pst.getString(4).trim().isEmpty()) {
                            try {
                                grandTotal = Double.parseDouble(pst.getString(4));
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                        count++;

                        if (count == 1) {
                            String name = pst.getString(1);
                            ledgerNameGrandTotalMap.put(name, grandTotal);
                            bpay.jcb1.setSelectedItem("" + name);
                            bpay.jtf1.setText(pst.getString(2));
                            bpay.jtf1.setEnabled(true);
                            bpay.jtf2.setEnabled(true);
                            bpay.jtf2.setText(pst.getString(6));
                            ResultSet balance = st0.executeQuery("select * from ledger where ledger_name='" + name + "'");
                            while (balance.next()) {
                                bpay.jcl17.setText("" + balance.getString(10));
                                bpay.jcl18.setText("" + balance.getString(11));
                            }
                        } else {
                            bpay.point1 = 2;
                            aPreviousRow = aRow;
                            aRow = bpay.createAnotherRow();
                            if (aPreviousRow == null) {
                                bpay.aFirstRow.aSecondRow = aRow;
                            }
                            String nextname = pst.getString(1);
                            aRow.jAccountNameCobmoBox.setSelectedItem(nextname);
                            aRow.jTextField.setText(pst.getString(2));
                            aRow.jTextField.setEnabled(true);
                            aRow.jTextField1.setText(pst.getString(6));
                            aRow.jTextField1.setEnabled(true);
                            ResultSet nextbalance = stg.executeQuery("select * from ledger where ledger_name='" + nextname + "'");
                            while (nextbalance.next()) {
//                                  aRow.jcl16.setText("CURRENT BAL : -  ");
                                aRow.jcl47.setText("" + nextbalance.getString(10));
                                aRow.jcl48.setText("" + nextbalance.getString(11));
                            }
                            if (aPreviousRow != null) {
                                aPreviousRow.isRowAlreadyCreated = true;
                                aPreviousRow.aNewRow = aRow;
                            }
                        }
                    }
                    bpay.choice1.select("" + byledger);

                    ResultSet rs2 = st1.executeQuery("select * from ledger where ledger_name='" + byledger + "'");

                    while (rs2.next()) {
                        bpay.jTextField5.setText(rs2.getString(10));
                        bpay.jLabel8.setText(rs2.getString(11));
                    }
                    conn.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                bpay.jTextField1.setText("" + sno);
                bpay.jDateChooser1.setDate(formatter.parse(date));
                bpay.setLedgerNameGrandTotalMapForUpdate(ledgerNameGrandTotalMap);

            } catch (ParseException ex) {
                Logger.getLogger(dayBook.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /* Working for receipt */
        if (type.equals("RC")) {
            try {
                int count = 0;
                LinkedHashMap<String, Double> ledgerNameGrandTotalMap = new LinkedHashMap<String, Double>();
                cashreciept rec = new cashreciept();
                rec.show();
                rec.jButton1.setVisible(false);
                rec.jButton3.setVisible(true);
                String byledger = "";
                try {
                    connection c = new connection();
                    Connection conn = c.cone();
                    Statement st0 = conn.createStatement();
                    Statement stg = conn.createStatement();
                    Statement st00 = conn.createStatement();
                    ResultSet rs00;
                    if (a == 1) {
                        rs00 = st00.executeQuery(" select * from ledger where ledger_name='" + trialBalance.table.getValueAt(row, 1) + "'");
                    } else {
                        rs00 = st00.executeQuery(" select * from ledger where ledger_name='" + jTable1.getValueAt(row, 1) + "'");

                    }
                    cashreciept.SecondRow aRow = null, aPreviousRow = null;

                    while (rs00.next()) {
                        rec.jTextField5.setText("" + rs00.getString(10));
                        rec.jLabel8.setText("" + rs00.getString(11));
                    }

                    Statement stpst = conn.createStatement();
                    String snoo = sno;
                    System.out.println("" + snoo);
                    ResultSet pst = stpst.executeQuery("select to_ledger,amo_unt,narra_tion,grand_total,by_ledger from receipt where s_no='" + snoo + "' ");
                    while (pst.next()) {
                        byledger = pst.getString("by_ledger");
                        rec.jTextArea1.setText(pst.getString(3));
                        rec.jTextField4.setText(pst.getString(4));
                        double grandTotal = 0;
                        if (pst.getString(4) != null && !pst.getString(4).trim().isEmpty()) {
                            try {
                                grandTotal = Double.parseDouble(pst.getString(4));
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                        count++;
                        if (count == 1) {
                            String name = pst.getString(1);
                            ledgerNameGrandTotalMap.put(name, grandTotal);
                            rec.jcb1.setSelectedItem("" + name);
                            rec.jtf1.setText(pst.getString(2));
                            rec.jtf1.setEnabled(true);
                            ResultSet balance = st0.executeQuery("select * from ledger where ledger_name='" + name + "'");
                            while (balance.next()) {
                                rec.jcl17.setText("" + balance.getString(10));
                                rec.jcl18.setText("" + balance.getString(11));
                            }
                        } else {
                            rec.point1 = 2;
                            aPreviousRow = aRow;
                            aRow = rec.createAnotherRow();
                            if (aPreviousRow == null) {
                                rec.aFirstRow.aSecondRow = aRow;
                            }
                            String nextname = pst.getString(1);
                            aRow.jAccountNameCobmoBox.setSelectedItem(nextname);
                            aRow.jTextField.setText(pst.getString(2));
                            aRow.jTextField.setEnabled(true);
                            ResultSet nextbalance = stg.executeQuery("select * from ledger where ledger_name='" + nextname + "'");
                            while (nextbalance.next()) {
                                aRow.jcl47.setText("" + nextbalance.getString(10));
                                aRow.jcl48.setText("" + nextbalance.getString(11));
                            }
                            if (aPreviousRow != null) {
                                aPreviousRow.isRowAlreadyCreated = true;
                                aPreviousRow.aNewRow = aRow;
                            }
                        }
                    }
                    conn.close();
                } catch (Exception ex) {

                    ex.printStackTrace();

                }
                rec.choice1.addItem("" + byledger);
                rec.choice1.select("" + byledger);
                rec.jTextField1.setText("" + sno);
                rec.jDateChooser1.setDate(formatter.parse(date));
                rec.setLedgerNameGrandTotalMapForUpdate(ledgerNameGrandTotalMap);
            } catch (ParseException ex) {

                Logger.getLogger(dayBook.class.getName()).log(Level.SEVERE, null, ex);

            }
        }

        /* working for bank receipt  */
        if (type.equals("BRC")) {
            try {
                int count = 0;
                LinkedHashMap<String, Double> ledgerNameGrandTotalMap = new LinkedHashMap<String, Double>();
                LinkedHashMap<String, Integer> ByledgerNameMap = new LinkedHashMap<String, Integer>();
                bankreciept brc = new bankreciept();
                brc.show();
                brc.jButton1.setVisible(false);
                brc.jButton3.setVisible(true);
                brc.a = 1;
                String byledger = "";
                try {
                    connection c = new connection();
                    Connection conn = c.cone();
                    Statement st0 = conn.createStatement();
                    Statement stg = conn.createStatement();
                    Statement st00 = conn.createStatement();
                    Statement st1 = conn.createStatement();

                    bankreciept.SecondRow aRow = null, aPreviousRow = null;

                    Statement stpst = conn.createStatement();
                    String snoo = sno;
                    System.out.println("" + snoo);
                    ResultSet pst = stpst.executeQuery("select to_ledger,amo_unt,narra_tion,grand_total,by_ledger,cheque from bankreceipt where s_no='" + snoo + "' ");
                    while (pst.next()) {
                        byledger = pst.getString("by_ledger");
                        brc.byledger = byledger;
                        brc.jTextArea1.setText(pst.getString(3));
                        brc.jTextField4.setText(pst.getString(4));
                        double grandTotal = 0;
                        if (pst.getString(4) != null && !pst.getString(4).trim().isEmpty()) {
                            try {
                                grandTotal = Double.parseDouble(pst.getString(4));
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                        count++;

                        if (count == 1) {
                            String name = pst.getString(1);
                            ledgerNameGrandTotalMap.put(name, grandTotal);
                            ledgerNameGrandTotalMap.put(byledger, grandTotal);
                            brc.jcb1.setSelectedItem("" + name);
                            brc.jtf1.setText(pst.getString(2));
                            brc.jtf1.setEnabled(true);
                            brc.jtf2.setEnabled(true);
                            brc.jtf2.setText(pst.getString(6));
                            ResultSet balance = st0.executeQuery("select * from ledger where ledger_name='" + name + "'");
                            while (balance.next()) {
                                brc.jcl17.setText("" + balance.getString(10));
                                brc.jcl18.setText("" + balance.getString(11));
                            }
                        } else {
                            brc.point1 = 2;
                            aPreviousRow = aRow;
                            aRow = brc.createAnotherRow();
                            if (aPreviousRow == null) {
                                brc.aFirstRow.aSecondRow = aRow;
                            }
                            String nextname = pst.getString(1);
                            aRow.jAccountNameCobmoBox.setSelectedItem(nextname);
                            aRow.jTextField.setText(pst.getString(2));
                            aRow.jTextField.setEnabled(true);
                            aRow.jTextField1.setText(pst.getString(6));
                            aRow.jTextField1.setEnabled(true);
                            ResultSet nextbalance = stg.executeQuery("select * from ledger where ledger_name='" + nextname + "'");
                            while (nextbalance.next()) {
//                                  aRow.jcl16.setText("CURRENT BAL : -  ");
                                aRow.jcl47.setText("" + nextbalance.getString(10));
                                aRow.jcl48.setText("" + nextbalance.getString(11));
                            }
                            if (aPreviousRow != null) {
                                aPreviousRow.isRowAlreadyCreated = true;
                                aPreviousRow.aNewRow = aRow;
                            }
                        }
                    }

                    brc.choice1.select("" + byledger);
                    ResultSet rs2 = st1.executeQuery("select * from ledger where ledger_name='" + byledger + "'");

                    while (rs2.next()) {
                        brc.jTextField5.setText(rs2.getString(10));
                        brc.jLabel8.setText(rs2.getString(11));
                    }

                    conn.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                brc.jTextField1.setText("" + sno);
                brc.jDateChooser1.setDate(formatter.parse(date));
                brc.setLedgerNameGrandTotalMapForUpdate(ledgerNameGrandTotalMap);

            } catch (ParseException ex) {
                Logger.getLogger(dayBook.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        /* For Purchase Transaction dual gst */
        if (type.equals("PR")) {
            transaction.Pur_trans_dualGst pur = new transaction.Pur_trans_dualGst();
            pur.setVisible(true);
            pur.jDateChooser2.setEnabled(true);
            pur.jTextField2.setEnabled(true);
            pur.jTextField3.setEnabled(false);
            pur.jTextField4.setEnabled(true);
            pur.jTextField5.setEnabled(false);
            pur.jTextField6.setEnabled(false);
            pur.jTextField7.setEnabled(false);
            pur.jTextField8.setEnabled(false);
            pur.jTextField9.setEnabled(false);
            pur.jTextField10.setEnabled(false);
            pur.jTextField11.setEnabled(true);
            pur.jTextField12.setEnabled(true);
            pur.jTextField13.setEnabled(true);
            pur.jTextField14.setEnabled(true);
            pur.jTextField15.setEnabled(true);
            pur.jTextField16.setEnabled(true);
            pur.jTextField40.setEditable(false);
            pur.jTextField23.setEnabled(true);
            pur.jRadioButton3.setEnabled(true);
            pur.jRadioButton4.setEnabled(true);
            pur.jComboBox4.setEnabled(false);
            pur.jComboBox1.setEnabled(false);
            pur.jComboBox2.setEnabled(false);
            pur.jButton1.setVisible(false);
            pur.jButton5.setVisible(false);
            double igst5 = 0;
            double igst12 = 0;
            double sgst25 = 0;
            double sgst6 = 0;
            double cgst25 = 0;
            double cgst6 = 0;
            String purchase_account = "";
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                Statement st2 = connect.createStatement();
                Statement st3 = connect.createStatement();
                Statement st4 = connect.createStatement();
                Statement st5 = connect.createStatement();

                ResultSet rs = st.executeQuery("select * from stock1 where Invoice_number='" + jTable1.getValueAt(row, 1) + "" + "'");
                while (rs.next()) {
                    pur.jTextField22.setText(rs.getString(1));
                    pur.jTextField12.setText(rs.getString(2));
                    pur.jDateChooser2.setDate(formatter.parse(rs.getString(3)));
                    pur.jTextField2.setText(rs.getString(4));

                    if (rs.getString("Purchase_type").equals("Credit")) {
                        pur.jRadioButton4.setSelected(true);
                        pur.purch = "Credit";
                    } else {
                        pur.purch = "Cash";
                        pur.jRadioButton3.setSelected(true);
                    }
                    pur.jTextField4.setText(rs.getString("Refrence_number"));
                    if (rs.getString("partypurchase_date").equals("")) {

                    } else if (!rs.getString("partypurchase_date").equals("")) {
                        pur.jDateChooser1.setDate(formatter.parse(rs.getString("partypurchase_date")));
                    }
                    String byforgation = rs.getString("Byforgation");
                    if (!byforgation.equals(null)) {
                        byforgation = "";
                    }
                    String articleno = rs.getString("articleno");
                    if (!articleno.equals(null)) {
                        articleno = "";
                    }

                    DefaultTableModel dtm1 = (DefaultTableModel) pur.jTable1.getModel();

                    Object o[] = {rs.getString("Sno"), rs.getString("by_ledger"), rs.getString("Product"), rs.getString("Qty"), rs.getString("Rate"), rs.getString("Amount"), rs.getString("Pdesc"), rs.getString("Retail"), rs.getString("Desc"), byforgation, articleno, rs.getString("samebarcode")};
                    dtm1.addRow(o);
                    pur.rpurc_account.add(0, rs.getString("by_ledger"));
                    pur.jTextField13.setText(rs.getString("Bill_amount"));
                    pur.jTextField15.setText(rs.getString("amount_after_discount"));
                    pur.jTextField14.setText(rs.getString("Other_Charges"));
                    pur.jTextField16.setText(rs.getString("Net_amount"));
                    double discount = Double.parseDouble(pur.jTextField13.getText()) - Double.parseDouble(pur.jTextField15.getText());

                    pur.jTextField23.setText("" + discount);

                    float dis_per;
                    double dis_amnt = 0;
                    double total_amnt = 0;

                    dis_amnt = Double.parseDouble(pur.jTextField23.getText());
                    total_amnt = Double.parseDouble(pur.jTextField13.getText());

                    dis_per = (float) ((dis_amnt / total_amnt) * 100);

                    DecimalFormat df1 = new DecimalFormat("0.00");
                    String per = df1.format(dis_per);

                    pur.jTextField11.setText(per + "");

                    pur.sundrycreditorold = rs.getString("Supplier_name");
                    System.out.println("sundry creditor name - " + pur.sundrycreditorold);
                    pur.Purchaseaccount = rs.getString("by_ledger");
                    pur.Party_cst = rs.getString("cgst_account");
                    pur.other_charges = rs.getString("otherchargesaccount");
                    pur.Party_vat = rs.getString("sgst_account");
                    pur.Party_Igst = rs.getString("igst_account");
                    pur.old_freight_gst = rs.getString("Freight_Gst");
                    pur.freight_gst = rs.getDouble("Freight_Gst");
                    pur.jTextField40.setText("" + rs.getString("Freight_Gst_Amount"));

                    if (pur.other_charges.equals("null")) {
                        pur.other_charges = null;
                    }

                    if (pur.other_charges != null) {

                    } else {
                        ResultSet rs4 = st4.executeQuery("select * from ledger where ledger_name='" + pur.other_charges + "'");
                        while (rs4.next()) {
                            pur.jLabel45.setText(rs4.getString(10));
                            pur.jLabel46.setText(rs4.getString(11));
                        }
                    }

                }
                ResultSet rs_qnty = st1.executeQuery("select sum(Qty) as Qty from stock1 where Invoice_number='" + jTable1.getValueAt(row, 1) + "" + "'");
                while (rs_qnty.next()) {
                    pur.jTextField28.setText(rs_qnty.getString("Qty"));
                }

                ResultSet rs_gst_5 = st2.executeQuery("select * from stock1 where Invoice_number='" + (jTable1.getValueAt(row, 1) + "") + "' AND by_ledger = 'PURCHASE 12%'");
                while (rs_gst_5.next()) {
                    igst12 = rs_gst_5.getDouble("total_igst_amnt_12");
                    sgst6 = rs_gst_5.getDouble("total_sgst_amnt_6");
                    cgst6 = rs_gst_5.getDouble("total_cgst_amnt_6");
                }

                ResultSet rs_gst_12 = st2.executeQuery("select * from stock1 where Invoice_number='" + (jTable1.getValueAt(row, 1) + "") + "' AND by_ledger = 'Purchase 5%'");
                while (rs_gst_12.next()) {
                    igst5 = rs_gst_12.getDouble("total_igst_amnt_5");
                    sgst25 = rs_gst_12.getDouble("total_sgst_amnt_25");
                    cgst25 = rs_gst_12.getDouble("total_cgst_amnt_25");
                }

                pur.jTextField17.setText("" + cgst25);
                pur.jTextField25.setText("" + cgst6);
                pur.old_cgst_25 = cgst25 + "";
                pur.old_cgst_6 = cgst6 + "";

                pur.jTextField24.setText("" + sgst25);
                pur.jTextField26.setText("" + sgst6);
                pur.old_sgst_25 = sgst25 + "";
                pur.old_sgst_6 = sgst6 + "";

                pur.jTextField1.setText("" + igst5);
                pur.jTextField27.setText("" + igst12);
                pur.old_igst_5 = igst5 + "";
                pur.old_igst_12 = igst12 + "";

                ResultSet g_rs = st.executeQuery("select * from party where party_code='" + pur.jTextField22.getText() + "" + "'");
                while (g_rs.next()) {
                    if (g_rs.getBoolean("IGST") == true) {
                        pur.isIgstApplicable = true;
                        pur.jTextField17.setEnabled(false);
                        pur.jTextField24.setEnabled(false);
                        pur.jTextField25.setEnabled(false);
                        pur.jTextField26.setEnabled(false);
                    } else if (g_rs.getBoolean("IGST") == false) {
                        pur.isIgstApplicable = false;
                        pur.jTextField1.setEnabled(false);
                        pur.jTextField27.setEnabled(false);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* For Purchase Return */
        if (type.equals("PRR")) {
            transaction.New_Dual_PurchaseReturn b1 = new transaction.New_Dual_PurchaseReturn();
            b1.setVisible(true);
            String type1 = null;
            String pn = "";
            String party_code = "";
            String fre_gst = "";
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT  * FROM purchasereturndualgst where bill_refrence='" + jTable1.getValueAt(row, 1) + "" + "'");
                while (rs.next()) {
                    pn = rs.getString("customer_name");
                    party_code = rs.getString("customer_account");

                    b1.jTextField1.setText(rs.getString(1));
                    b1.jDateChooser1.setDate(formatter.parse(rs.getString(2)));
                    type1 = rs.getString(3);
                    b1.jTextField3.setText(rs.getString(4));
                    b1.jTextField14.setText(rs.getString(5));

                    DefaultTableModel dtmr = (DefaultTableModel) b1.jTable1.getModel();
                    b1.sno = Integer.parseInt(rs.getString(6)) + 1;
                    b1.jTextField14.setText(b1.sno + "");
                    Object o[] = {rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), ""};
                    dtmr.addRow(o);
                    b1.jTextField15.setText(rs.getString(17));
                    b1.jTextField16.setText(rs.getString(18));
                    b1.jTextField17.setText(rs.getString(19));
                    b1.jTextField18.setText(rs.getString(20));
                    b1.jTextField19.setText(rs.getString(21));
                    b1.jTextField20.setText(rs.getString(22));
                    b1.jTextField19.setText(rs.getString("total_value_before_tax"));
                    b1.jTextField30.setText(rs.getString("total_igst_first_amnt"));
                    b1.jTextField40.setText(rs.getString("total_igst_second_amnt"));
                    b1.jTextField31.setText(rs.getString("total_sgst_first_amnt"));
                    b1.jTextField41.setText(rs.getString("total_sgst_second_amnt"));
                    b1.jTextField32.setText(rs.getString("total_cgst_first_amnt"));
                    b1.jTextField42.setText(rs.getString("total_cgst_second_amnt"));
                    b1.jTextField38.setText(rs.getString("reference"));
                    if (rs.getString("mobile_no").equals(null) || rs.getString("mobile_no").equals("null")) {
                        b1.jTextField37.setText("");
                    } else {
                        b1.jTextField37.setText(rs.getString("mobile_no"));
                    }

                    if (rs.getString("Round_Off_Value").equals(null) || rs.getString("Round_Off_Value").equals("null")) {
                        b1.jTextField33.setText("0");
                    } else {
                        b1.jTextField33.setText(rs.getString("Round_Off_Value"));
                    }

                    /* Can be used in near future but it is not required for now beacuse we are not updating from here
                
                b1.tiv = rs.getDouble("total_igst_first_amnt");
                b1.tsv = rs.getDouble("total_sgst_first_amnt");
                b1.tcv = rs.getDouble("total_cgst_first_amnt");
                b1.tiv1 = rs.getDouble("total_igst_second_amnt");
                b1.tsv1 = rs.getDouble("total_sgst_second_amnt");
                b1.tcv1 = rs.getDouble("total_cgst_second_amnt");
                b1.iv = rs.getDouble("igst_first");
                b1.sv = rs.getDouble("sgst_first");
                b1.cv = rs.getDouble("cgst_first");
                b1.iv1 = rs.getDouble("igst_second");
                b1.sv1 = rs.getDouble("sgst_second");
                b1.cv1 = rs.getDouble("cgst_second");

                b1.totalamount = Double.parseDouble(rs.getString(17));
                b1.totalquantity = Double.parseDouble(rs.getString(18));
                b1.totaldiscount = Double.parseDouble(rs.getString(19));
                b1.totalamountbeforetax = Double.parseDouble(rs.getString(21));
                b1.totalamountaftertax = Double.parseDouble(rs.getString(22));
                b1.sundrydebtors = rs.getString("sundrydebtorsaccount");
                b1.freight_gst = rs.getDouble("Freight_Gst");
                b1.old_freight = rs.getDouble("Freight_Gst_Amount");
                     */
                    b1.freight_account = rs.getString("freight_account");
                    b1.jTextField44.setText(rs.getString("freight_amount"));
                    fre_gst = rs.getString("Freight_Gst");
                    b1.jTextField45.setText("" + rs.getString("Freight_Gst_Amount"));

                }

                DecimalFormat df = new DecimalFormat("0");
                b1.jComboBox11.setSelectedItem(df.format(Double.parseDouble(fre_gst) * 2));

                ResultSet rs1 = st1.executeQuery("SELECT party_name, IGST FROM party where party_name = '" + pn + "'");
                while (rs1.next()) {
                    if (rs1.getBoolean("IGST") == true) {
                        b1.isIgstApplicable = true;
                        b1.jComboBox11.setSelectedItem(fre_gst);
                    } else if (rs1.getBoolean("IGST") == false) {
                        b1.isIgstApplicable = false;
                        b1.jComboBox11.setSelectedItem(df.format(Double.parseDouble(fre_gst) * 2));
                    }
                }

                if (type1 == null) {
                } else if (type1.equals("Cash")) {
                    b1.jRadioButton1.setSelected(true);
                } else if (type1.equals("Debit")) {
                    b1.jRadioButton2.setSelected(true);
                    if (party_code != null) {
                        b1.jLabel4.setVisible(true);
                        b1.jTextField4.setVisible(true);
                        b1.jTextField4.setText("" + party_code);
                    }
                }
                b1.jButton1.setVisible(false);
                b1.jButton7.setVisible(false);
                b1.jButton5.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* For Billing */
        if (type.equals("SL")) {
            transaction.New_Dual_Billing b1 = new transaction.New_Dual_Billing();
            b1.setVisible(true);
            String type2 = null;
            String pn = "";
            String party_code = "";
            String sr_no = "";
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT  * FROM Billing where bill_refrence='" + jTable1.getValueAt(row, 1) + "" + "'");
                while (rs.next()) {
                    pn = rs.getString("customer_name");
                    party_code = rs.getString("customer_account");

                    b1.jTextField1.setText(rs.getString(1));
                    b1.jDateChooser1.setDate(formatter.parse(rs.getString(2)));
                    type = rs.getString(3);
                    b1.jTextField3.setText(rs.getString(4));
                    b1.jTextField14.setText(rs.getString(5));

                    DefaultTableModel dtms = (DefaultTableModel) b1.jTable1.getModel();
                    b1.sno = Integer.parseInt(rs.getString(6)) + 1;
                    b1.jTextField14.setText(b1.sno + "");
                    Object o[] = {rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16)};
                    dtms.addRow(o);
                    b1.jTextField15.setText(rs.getString(17));
                    b1.jTextField16.setText(rs.getString(18));
                    b1.jTextField17.setText(rs.getString(19));
                    b1.jTextField18.setText(rs.getString(20));
                    b1.jTextField19.setText(rs.getString(21));
                    b1.jTextField20.setText(rs.getString(22));
                    b1.jTextField19.setText(rs.getString("total_value_before_tax"));
                    b1.jTextField30.setText(rs.getString("total_igst_taxamnt"));
                    b1.jTextField31.setText(rs.getString("total_sgst_taxamnt"));
                    b1.jTextField32.setText(rs.getString("total_cgst_taxamnt"));
                    b1.jTextField38.setText(rs.getString("reference"));
                    if (rs.getString("mobile_no").equals(null) || rs.getString("mobile_no").equals("null")) {
                        b1.jTextField37.setText("");
                    } else {
                        b1.jTextField37.setText(rs.getString("mobile_no"));
                    }

                    if (rs.getString("Round_Off_Value").equals(null) || rs.getString("Round_Off_Value").equals("null")) {
                        b1.jTextField33.setText("0");
                    } else {
                        b1.jTextField33.setText(rs.getString("Round_Off_Value"));
                    }

                    /*  Can be used in near future but for now we are not updating from here 
                
                b1.tiv = rs.getDouble("total_igst_taxamnt");
                b1.tsv = rs.getDouble("total_sgst_taxamnt");
                b1.tcv = rs.getDouble("total_cgst_taxamnt");
                b1.iv = rs.getDouble("vatamnt");
                b1.sv = rs.getDouble("Sgstamnt");
                b1.cv = rs.getDouble("Sgstamnt");
                b1.sundrydebtors = rs.getString("sundrydebtorsaccount");
                b1.totalamount = Double.parseDouble(rs.getString(17));
                b1.totalquantity = Double.parseDouble(rs.getString(18));
                b1.totaldiscount = Double.parseDouble(rs.getString(19));
                b1.totalamountbeforetax = Double.parseDouble(rs.getString(21));
                b1.totalamountaftertax = Double.parseDouble(rs.getString(22));
                     */
                }

                ResultSet sr_rs = st.executeQuery("SELECT Sr_Ref from billing where bill_refrence='" + jTable1.getValueAt(row, 1) + "" + "' group by bill_refrence ");
                while (sr_rs.next()) {
                    sr_no = sr_rs.getString("SR_Ref");
                    b1.jComboBox3.addItem(sr_no);
                    b1.jComboBox3.setSelectedItem(sr_no);
                }

                ResultSet rs1 = st1.executeQuery("SELECT party_name, IGST FROM party_sales where party_name = '" + pn + "'");
                while (rs1.next()) {
                    if (rs1.getBoolean("IGST") == true) {
                        b1.isIgstApplicable = true;
                    } else if (rs1.getBoolean("IGST") == false) {
                        b1.isIgstApplicable = false;
                    }
                }

                if (type.equals("Cash")) {
                    b1.jRadioButton1.setSelected(true);
                }

                if (type.equals("Debit")) {
                    b1.jRadioButton2.setSelected(true);

                    if (party_code != null) {
                        b1.jLabel4.setVisible(true);
                        b1.jTextField4.setVisible(true);
                        b1.jTextField4.setText("" + party_code);
                    }
                }
                b1.jButton1.setVisible(false);
                b1.jButton7.setVisible(false);
                b1.jButton5.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /* For Sales Return */
        if (type.equals("SR")) {
            New_Dual_SalesReturn b1 = new New_Dual_SalesReturn();
            b1.setVisible(true);
            String type3 = null;
            String pn = "";
            String party_code = "";
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT  * FROM salesreturn where bill_refrence='" + jTable1.getValueAt(row, 1) + "" + "'");
                while (rs.next()) {

                    pn = rs.getString("customer_name");
                    party_code = rs.getString("customer_account");

                    b1.jTextField1.setText(rs.getString(1));
                    b1.jDateChooser1.setDate(formatter.parse(rs.getString(2)));
                    type3 = rs.getString(3);
                    b1.jTextField3.setText(rs.getString(4));
                    b1.jTextField14.setText(rs.getString(5));

                    DefaultTableModel dtmr = (DefaultTableModel) b1.jTable1.getModel();
                    b1.sno = Integer.parseInt(rs.getString(6)) + 1;
                    b1.jTextField14.setText(b1.sno + "");
                    Object o[] = {rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), "", rs.getString("bill_no")};
                    dtmr.addRow(o);
                    b1.jTextField15.setText(rs.getString(17));
                    b1.jTextField16.setText(rs.getString(18));
                    b1.jTextField17.setText(rs.getString(19));
                    b1.jTextField18.setText(rs.getString(20));
                    b1.jTextField19.setText(rs.getString(21));
                    b1.jTextField20.setText(rs.getString(22));

                    b1.jTextField2.setText(rs.getString("total_igst_taxamnt"));
                    b1.jTextField21.setText(rs.getString("total_sgst_taxamnt"));
                    b1.jTextField22.setText(rs.getString("total_cgst_taxamnt"));
                    b1.jTextField23.setText(rs.getString("Round_Off_Value"));

                    /* Can be used in near future but for now we are not updating from here
                    
                b1.tiv = rs.getDouble("total_igst_taxamnt");
                b1.tsv = rs.getDouble("total_sgst_taxamnt");
                b1.tcv = rs.getDouble("total_cgst_taxamnt");
                b1.iv = rs.getDouble("vatamnt");
                b1.sv = rs.getDouble("Sgstamnt");
                b1.cv = rs.getDouble("Sgstamnt");

                b1.totalamount = Double.parseDouble(rs.getString(17));
                b1.totalquantity = Double.parseDouble(rs.getString(18));
                b1.totaldiscount = Double.parseDouble(rs.getString(19));
                b1.totalamountbeforetax = Double.parseDouble(rs.getString(21));
                b1.totalamountaftertax = Double.parseDouble(rs.getString(22));
                     */
                }

                ResultSet rs1 = st1.executeQuery("SELECT party_name, IGST FROM party_sales where party_name = '" + pn + "'");
                while (rs1.next()) {
                    if (rs1.getBoolean("IGST") == true) {
                        b1.isIgstApplicable = true;
                    } else if (rs1.getBoolean("IGST") == false) {
                        b1.isIgstApplicable = false;
                    }
                }

                if (type3.equals("Cash")) {
                    b1.jRadioButton1.setSelected(true);
                }

                if (type3.equals("Credit")) {
                    b1.jRadioButton2.setSelected(true);
                    if (party_code != null) {
                        b1.jLabel4.setVisible(true);
                        b1.jTextField4.setVisible(true);
                        b1.jTextField4.setText("" + party_code);
                    }
                }
                b1.jButton1.setVisible(false);
                b1.jButton7.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField(dateNow);
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jDialog1.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jDialog1.setTitle("ENTER DATE");
        jDialog1.setMinimumSize(new java.awt.Dimension(300, 100));
        jDialog1.setUndecorated(true);
        jDialog1.setOpacity(0.88F);
        jDialog1.setResizable(false);

        jLabel1.setText("ENTER DATE :");

        jTextField1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField1MouseClicked(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap(40, Short.MAX_VALUE)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(48, 48, 48))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : DAY BOOK");
        setFocusTraversalPolicyProvider(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                formKeyTyped(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "DATE", "PARTICULAR", "VCH TYPE", "VCH NO", "DEBIT AMT", "CREDIT AMT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setToolTipText("");
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 912, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-928)/2, (screenSize.height-533)/2, 928, 533);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        // TODO add your handling code here:
//        vouchers("HELLO");
    }//GEN-LAST:event_formWindowActivated

    private void formKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_formKeyTyped

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            str = jTextField1.getText();
            jDialog1.dispose();
            dispose();
            new dayBook().setVisible(true);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTextField1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField1MouseClicked
        jTextField1.setText(new Main.DatePicker().setPickedDate());
    }//GEN-LAST:event_jTextField1MouseClicked

    /**
     * @param args the command line arguments
     */
//    void vouchers(String choice) {
//
//        try {
//             Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
//           Connection conn = DriverManager.getConnection("jdbc:odbc:Appson");
//            stmt = conn.createStatement();
//
//            rs = stmt.executeQuery("select s_no,v_date,v_type,by_ledger,amo_unt from vouchers where v_date = '"+choice+"'");
//            int row = rs.getRow();
//            Object data[][] = new Object[50][6];
//            int i = 0;
//            while (rs.next()) {
//                if (rs.getString(3).equals("CONTRA") || rs.getString(3).equals("PAYMENT")) {
//                    data[i][0] = rs.getString(2);
//                    data[i][4] = rs.getString(5);
//                    data[i][1] = rs.getString(4);
//                    data[i][2] = rs.getString(3);
//                    data[i][3] = rs.getString(1);
//                    data[i][5] = "";
//                } else {
//                    data[i][0] = rs.getString(2);
//                    data[i][5] = rs.getString(5);
//                    data[i][1] = rs.getString(4);
//                    data[i][2] = rs.getString(3);
//                    data[i][3] = rs.getString(1);
//                    data[i][4] = "";
//                }
//
//                i++;
//            }
//            String heads[] = {"DATE", "PARTICULARS", "TYPE", "NO.", "DEBIT", "CREDIT"};
//            javax.swing.JTable jTable = new javax.swing.JTable(data, heads);
//      //      jPanel2.add(jTable);
//          //  jPanel2.setVisible(true);
//            jTable.setBounds(5, 20, 600, 400);
//
//            conn.close();
//        } catch (ClassNotFoundException | SQLException ex) {
//            System.out.println("Error - " + ex);
//        }
//    }
    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dayBook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dayBook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dayBook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dayBook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new dayBook().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
