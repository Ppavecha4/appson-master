package reporting.accounts;
import connection.connection;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.List;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import static java.rmi.Naming.list;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Collections.list;
public final class pals extends javax.swing.JFrame implements Printable{
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    static boolean isParticular = true;
    boolean isPeriodCase=false;
    int totalcr, totaldr, total, total1,totalcrperiod,totaldrperiod,totalperiod,total1period = 0;
    public int diff,totcr,totdr,totcrperiod,totdrperiod;
    static String command = null;
    public int difference=0;
    period a = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("01-04-yyyy");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-YYYY");
    String currentdate = formatter1.format(currentDate.getTime());
    SimpleDateFormat yearnow= new SimpleDateFormat("yyy");
String getyear=yearnow.format(currentDate.getTime());
    public pals(boolean theIsPeriodCase) {
        a = new period();
        isPeriodCase = theIsPeriodCase;
        initComponents();
        jLabel6.setText(""+currentdate);
        
         list1.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });
        list3.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });

        
//            period.jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
//            @Override
//            public void keyPressed(java.awt.event.KeyEvent e) {
//                char key = e.getKeyChar();
//                if (key == KeyEvent.VK_ENTER) {
//                                        a.dispose();
//                    if (!period.jTextField1.getText().isEmpty() || !period.jTextField2.getText().isEmpty() ) {
//                     isPeriodCase = true;
//                     list1.removeAll();
//                    list2.removeAll();
//                    list3.removeAll();
//                    list4.removeAll();
//                    jTextField1.removeAll();
//                    jTextField2.removeAll();
//                    trial_periodpals();
//                    }
//                }
//            }
//        });
        
        
        
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        list1 = new java.awt.List();
        list2 = new java.awt.List();
        list3 = new java.awt.List();
        list4 = new java.awt.List();
        jSeparator1 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        jLabel3.setText("TOTAL :");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : PROFIT AND LOSS");
        setAlwaysOnTop(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel1.setMinimumSize(new java.awt.Dimension(810, 650));

        jLabel2.setText("PARTICULARS");

        jLabel4.setText("TRADING ACCOUNT");

        list1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        list1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                list1MousePressed(evt);
            }
        });
        list1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                list1ItemStateChanged(evt);
            }
        });
        list1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list1ActionPerformed(evt);
            }
        });
        list1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                list1KeyPressed(evt);
            }
        });

        list2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        list3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        list3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                list3MousePressed(evt);
            }
        });
        list3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                list3ItemStateChanged(evt);
            }
        });
        list3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list3ActionPerformed(evt);
            }
        });
        list3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                list3KeyPressed(evt);
            }
        });

        list4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jTextField1.setEditable(false);

        jTextField2.setEditable(false);

        jLabel1.setText("TOTAL :");

        jLabel5.setText("TOTAL :");

        jLabel6.setText("jLabel6");

        jLabel7.setText("P & L As on Date: -");

        jButton1.setText("Print");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jSeparator1)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(list1, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(list2, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(list3, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(list4, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(70, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(177, 177, 177)
                                .addComponent(jLabel1)))
                        .addGap(58, 58, 58)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
                    .addComponent(list2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(list3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(list4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(25, 25, 25)
                .addComponent(jButton1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(823, 685));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    private void list3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list3ActionPerformed
       System.out.println("Ankit list3 action change....");
         command = (String) evt.getActionCommand();
    }//GEN-LAST:event_list3ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        if(!isPeriodCase)
            trial();
        else
            trial_periodpals();
    }//GEN-LAST:event_formWindowOpened

    private void list3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_list3ItemStateChanged
list4.select(list3.getSelectedIndex());
    }//GEN-LAST:event_list3ItemStateChanged

    private void list1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list1ActionPerformed
           System.out.println("Ankit list1 action change....");
      command = (String) evt.getActionCommand();
    }//GEN-LAST:event_list1ActionPerformed

    private void list1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_list1ItemStateChanged
 list2.select(list1.getSelectedIndex());
    }//GEN-LAST:event_list1ItemStateChanged

    private void list1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list1KeyPressed
        // TODO add your handling code here:
        
        
        
        
    }//GEN-LAST:event_list1KeyPressed

    private void list1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list1MousePressed
        // TODO add your handling code here:
        if(evt.getClickCount()==2)
        {
        command = ((List)evt.getSource()).getSelectedItem();
     command = ((List) evt.getSource()).getSelectedItem();
        if(command.equals("GROSS PROFIT : ") )
        {
            
        }
        else if(command.equals("GROSS LOSS : "))
        {
       
        }
        else if(command.equals("NET PROFIT : "))
        {
       
        }
        else if(command.equals("NET LOSS : "))
        {
       
        }
        else
        {
       reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);
        
        
        }
        }
    }//GEN-LAST:event_list1MousePressed

    private void list3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list3KeyPressed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_list3KeyPressed

    private void list3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list3MousePressed
        // TODO add your handling code here:
        if(evt.getClickCount()==2)
        {
                    
        command = ((List) evt.getSource()).getSelectedItem();
        if(command.equals("GROSS PROFIT : ") )
        {
            
        }
        else if(command.equals("GROSS LOSS : "))
        {
       
        }
        else if(command.equals("NET PROFIT : "))
        {
       
        }
        else if(command.equals("NET LOSS : "))
        {
       
        }
        else
        {
        reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);
        }
        }
    }//GEN-LAST:event_list3MousePressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
     
        PalsPrint pp=new PalsPrint() {

            @Override
            public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
                         pp.printing();
        
        
        
        
    }//GEN-LAST:event_jButton1ActionPerformed
int dr=0;
int cr=0;
//double total8=0;
    /**
     * @param args the command line arguments
     */
   
    public void trial() {
   
        int sales=0;
        int purchase=0;
  //FOR SALES
        
     
        
        try
{
connection c = new connection();
Connection conn1 = c.cone();
             Statement stsales=conn1.createStatement();
             ResultSet rssales=stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='SALES ACCOUNTS' ");
               
             while (rssales.next() )
             {
                if(!rssales.getString(1).equals("0"))
                {
                 list3.add(""+rssales.getString(2));
             list4.add(""+rssales.getString(1));
             dr=dr+(int)Double.parseDouble(rssales.getString(1));
                }
             }
           ResultSet rssales1=stsales.executeQuery("select sum(curr_bal) from ledger where groups='SALES ACCOUNTS' ");
               
             while (rssales1.next() )
             {
                sales=rssales1.getInt(1);
             }  
                 
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
        
System.out.println("dr after sales"+dr);
        
System.out.println("dr after closing stock"+dr);


    //FOR OPENING STOCK
        String date=getyear.concat("-04-01");
        try
{
connection c = new connection();
            try (Connection conn3 = c.cone()) {
                Statement stopening=conn3.createStatement();
                ResultSet rsopening=stopening.executeQuery("select sum(ra_te) from stockid where Da_te < '"+date+"'  ");
                int opening=0;
                while (rsopening.next() )
                {
                    opening=rsopening.getInt(1);
                }
                list1.add("OPENING STOCK");
                list2.add(""+opening);
                cr=cr+opening;
            }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }        


//FOR PURCHASE
        
try
{
connection c = new connection();
            try (Connection conn2 = c.cone()) {
                Statement stpurchase=conn2.createStatement();
                ResultSet rspurchase=stpurchase.executeQuery("select curr_bal,ledger_name from ledger where groups='PURCHASE ACCOUNTS' ");
                
                while (rspurchase.next() )
                {
                    if(!rspurchase.getString(1).equals("0"))
                    {
                list1.add(""+rspurchase.getString(2));
                list2.add(""+rspurchase.getString(1));
                cr=(int) (cr+Double.parseDouble(rspurchase.getString(1)));
                    }
                    }
                
            }

}
    catch(Exception e)
    {
        e.printStackTrace();
    } 
        System.out.println("dr after sales"+dr);
        
System.out.println("dr after closing stock"+dr);


//FOR OPENING STOCK
        
    
        System.out.println("cr after opening stock"+cr);
////stock out

        
                 
         
          ///////// for closing stock        
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
                
//                ResultSet rsstockout0=ststockclosingout0.executeQuery("select sum(ra_te) from stocktransfer where To_branch='0'");
//                int stockout0=0;
//                while (rsstockout0.next() )
//                {
//                    stockout0=rsstockout0.getInt(1);
//                }
  //              System.out.println("Value of stockin at branch1-"+stockout0);
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te) from stockid2");
                int closing=0;
                while (rsclosing.next() )
                {
                    closing=rsclosing.getInt(1);
                }
//                int result=closing+stockout0;
  int result=closing;
                list3.add("CLOSING STOCK");
                list4.add(""+result);
                dr=dr+result;
            }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }

          
          
          
          
          System.out.println("dr after closing stock-0"+dr);
                    
          
          

// Direct incomes
    int direct=0;
        try
{
connection c = new connection();
Connection conn1 = c.cone();
             Statement stsales=conn1.createStatement();
             ResultSet rsdirectincome=stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT INCOME' ");
               
             while (rsdirectincome.next() )
             {
            //     direct=rsdirectincome.getInt(1);
            
            if(!rsdirectincome.getString(1).equals("0"))
            {    
             list3.add(""+rsdirectincome.getString(2));
             list4.add(""+rsdirectincome.getString(1));
              dr=(int) (dr+Double.parseDouble(rsdirectincome.getString(1)));
            }
             }
             
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
      
        
        // Direct Expense
    int directexpense=0;
        try
{
connection c = new connection();
Connection conn1 = c.cone();
             Statement stsales=conn1.createStatement();
             ResultSet rsdirectexpense=stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT EXPENSES' ");
               
             while (rsdirectexpense.next() )
             {
                 if(!rsdirectexpense.getString(1).equals("0"))
                 {
             list1.add(""+rsdirectexpense.getString(2));
             list2.add(""+rsdirectexpense.getString(1));
              cr=(int) (cr+Double.parseDouble(rsdirectexpense.getString(1)));
                 }
                 }
             
}
    catch(Exception e)
    {
        e.printStackTrace();
    }

          
          
          
            double diffss=0;
//            double aftercr=0,afterdr=0;
            System.out.println(totalcr + " : " + totaldr);
               
            
            System.out.println(""+cr);
            System.out.println(""+dr);
double result=dr-cr;
        System.out.println("result"+result);
            if (cr >= dr) {
                            
                list3.addItem("GROSS LOSS ");
                diffss = cr - dr;
                list4.addItem("" + Math.abs(diffss));
                total=(int) (diffss+dr);
               
                totalcr=(int)diffss;
               
                 
            }
            else if (dr > cr) {
               
                diffss = dr - cr;
                double profitpercent=(diffss/sales)*100;    
                 DecimalFormat df=new DecimalFormat("0.00");
        String profitpercent1 = df.format(profitpercent);
                
                    list1.addItem("GROSS PROFIT "+"("+profitpercent1+"%)");
                    list2.addItem(""+Math.abs(diffss));
                    totaldr=(int)diffss;
                    total=(int)diffss+cr;

                  }    
        
           
int x =list1.getItemCount();
int y=list3.getItemCount();

if(x>y)
{
    
    list1.addItem("--------------------------------------------", x+1);
    list2.addItem("--------------", x+1);
    for(int i=y;i<x;i++)
    {
    list3.addItem("", i);
    list4.addItem("", i);
    }
    list3.addItem("--------------------------------------------", x+1);
     list4.addItem("-------------", x+1);
    
}


if(y>x)
{
    
     for(int i=x;i<y;i++)
    {
    list1.addItem("", i);
    list2.addItem("", i);
    
    }
    list1.addItem("--------------------------------------------", y+1);
    list2.addItem("--------------", y+1);
    
    list3.addItem("--------------------------------------------", y+1);
list4.addItem("--------------", y+1);    

}


list1.add("TOTAL");
list2.add(""+total);
list3.add("TOTAL");
list4.add(""+total);



 list1.addItem("--------------------------------------------");
 list2.addItem("--------------");
    
list3.addItem("--------------------------------------------");
list4.addItem("--------------"); 
        list1.add("");
        list2.add("");
        list3.add("");
        list4.add("");
        
        list1.add("");
        list2.add("");
        list3.add("");
        list4.add("");


          list1.add("");
        list2.add("            PROFIT &");
        list3.add("LOSS ACCOUNT");
        list4.add("");
        
        
        list1.addItem("--------------------------------------------");
 list2.addItem("--------------");
    
list3.addItem("--------------------------------------------");
list4.addItem("--------------"); 
        

        
  if (cr >= dr) {
                            
                list1.addItem("GROSS LOSS ");
                diffss = cr - dr;
                list2.addItem("" + Math.abs(diffss));
               // total=(int) (diffss+dr);
                //jTextField1.setText("" + total);
                totalcr=(int)diffss;
                
               
            }
            else {
               
                diffss = dr - cr;
                double profitpercent=(diffss/sales)*100;    
                 DecimalFormat df=new DecimalFormat("0.00");
        String profitpercent1 = df.format(profitpercent);
                
                    list3.addItem("GROSS PROFIT "+"("+profitpercent1+"%)");
                    list4.addItem(""+Math.abs(diffss));
                    totaldr=(int)diffss;
                    

                  }    
        

   
        
        
     //indirect incomes   
        int indirect=0;
        try
{
connection c = new connection();
Connection conn1 = c.cone();
             Statement stsales=conn1.createStatement();
             ResultSet rsindirectincome=stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='INDIRECT INCOMES' ");
               
             while (rsindirectincome.next() )
             {
                 if(!rsindirectincome.getString(1).equals("0"))
                 {
                     list3.add(""+rsindirectincome.getString(2));
             list4.add(""+rsindirectincome.getString(1));
              totaldr=(int) (totaldr+Double.parseDouble(rsindirectincome.getString(1)));
                 }
                 }
            
conn1.close();
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
        


   
        
        
     //indirect expense   
        int indirectexpenses=0;
        try
{
connection c = new connection();
Connection conn1 = c.cone();
             Statement stsales=conn1.createStatement();
             ResultSet rsindirectexpenses=stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='INDIRECT EXPENSES' ");
               
             while (rsindirectexpenses.next() )
             {
                 if(!rsindirectexpenses.getString(1).equals("0"))
                 {
                   list1.add(""+rsindirectexpenses.getString(2));
                   list2.add(""+rsindirectexpenses.getString(1));
                   totalcr=(int) (totalcr+Double.parseDouble(rsindirectexpenses.getString(1)))  ;
                 }
                 }
             
conn1.close();
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
       
    
    if(totaldr>totalcr)
    {
 double netprofit=totaldr-totalcr;
list1.add("NET PROFIT");
list2.add(""+netprofit);
jTextField1.setText(""+(int)totaldr);
jTextField2.setText(""+(int)(totaldr+netprofit));

    }
    if(totalcr>totaldr)
    {
        
double netloss=totalcr-totaldr;
list3.add("NET LOSS");
list4.add(""+netloss);
jTextField1.setText(""+(int)totaldr);
jTextField2.setText(""+(int)(totalcr+netloss));
    }
    }
    
    
    
    
    
    
    
    
    
    public String curr_Date = null;
    
    private void trial_periodpals() {
        try {
connection c = new connection();
Connection conn = c.cone();
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[500];
            String grpname[] = new String[50000];
            String match2[] = new String[500];
            String cretedgc[] = new String[500];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgc = conn.createStatement();
//            ResultSet rsgcc = stgc.executeQuery("select Max(Curr_Date) from LedgerFinal where Curr_Date Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
//            while(rsgcc.next())
//            {
//               curr_Date = rsgcc.getString(1);
//            }
            ResultSet rsgc = stgc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgc.next()) {
                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME")
                            || groupname.equals("PURCHASE ACCOUNTS")
                            || groupname.equals("SALES ACCOUNTS")) //                   groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                    {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1 = conn.createStatement();
                            ResultSet rsdk = stgc1.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bal = 0;
                            int bal1 = 0;
                            int balnet = 0;
                            while (rsdk.next()) {
                                String baltype = rsdk.getString(1);
                                if (baltype.equals("CR")) {
                                    bal = rsdk.getInt(2);
                                }
                                if (baltype.equals("DR")) {
                                    bal1 = rsdk.getInt(2);
                                }
                            }
                            if (bal > bal1) {
                                balnet = bal - bal1;
                                String netbalc = Integer.toString(balnet);
                                list4.addItem(netbalc);
                                totalperiod = totalperiod + Integer.parseInt(netbalc);
                                list3.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bal1 > bal) {
                                    balnet = bal1 - bal;
                                }
                                String netbalc1 = Integer.toString(balnet);
                                list2.addItem(netbalc1);
                                total1period = total1period + Integer.parseInt(netbalc1);
                                list1.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                    j++;
                }
            }
            if (totalperiod > total1period) {
                list1.addItem("GROSS PROFIT : ");
                diff = (totalperiod - total1period);
                list2.addItem("" + diff);
                totdrperiod=total1period+diff;
//                jTextField2.setText("" + (diff + total1));
//                jTextField1.setText("" + (total));
            } else {
                list3.addItem("GROSS LOSS : ");
                diff = (total1period - totalperiod);
                list4.addItem("" + diff);
                totcrperiod=totalperiod+diff;
//                jTextField2.setText("" + (total1));
//                jTextField1.setText("" + (diff + total));
            }

        } catch (SQLException ex) {
            System.out.println("Error - " + ex);
        }
      
        try {
connection c = new connection();
Connection conn = c.cone();
            int ii = 0;
            int jj = 0;
            int lml = 0;
            String matchh[] = new String[500];
            String match2h[] = new String[500];
            String cretedgcc[] = new String[500];
            match2h[jj] = ("durgeshk");
            cretedgcc[lml] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgcc = conn.createStatement();
            ResultSet rsgcc = stgcc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgcc.next()) {
                aIsAdded = false;
                String groupname1 = rsgcc.getString(1);
                String createdgroup1 = rsgcc.getString(2);
                matchh[ii] = groupname1;
                if (match2h[jj].equals(matchh[ii]) || match2h[jj].equals(cretedgcc[lml])) {
                    groupname = null;
                    createdgroup = null;
                    matchh[ii] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")) {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1c = conn.createStatement();
                            ResultSet rsdkc = stgc1c.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bala = 0;
                            int bala1 = 0;
                            int balneta = 0;
                            while (rsdkc.next()) {
                                String baltype = rsdkc.getString(1);
                                if (baltype.equals("CR")) {
                                    bala = rsdkc.getInt(2);
                                }
                                if (baltype.equals("DR")) {
                                    bala1 = rsdkc.getInt(2);

                                }
                            }
                            if (bala > bala1) {
                                balneta = bala - bala1;
                                String netbalc = Integer.toString(balneta);
                                list4.addItem(netbalc);
                                totalcrperiod = totalcrperiod + Integer.parseInt(netbalc);
                                list3.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bala1 > bala) {
                                    balneta = bala1 - bala;
                                }
                                String netbalc1 = Integer.toString(balneta);
                                list2.addItem(netbalc1);
                                totaldrperiod = totaldrperiod + Integer.parseInt(netbalc1);
                                list1.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2h[jj + 1] = matchh[ii];
                    matchh[ii] = null;
                    jj++;
                }
            }
            if (totalcrperiod > totaldrperiod) {
                list3.addItem("NET PROFIT : ");
                difference = totalcrperiod - totaldrperiod;
                list4.addItem("" + difference);
                System.out.print("difference"+difference);
                System.out.print("totalcrperiod"+totalcrperiod);
                System.out.print("totalcrperiod"+totaldrperiod);
               jTextField2.setText("" + (difference + totaldrperiod + total1period));
                jTextField1.setText("" + (totcrperiod + totalcrperiod));
            } else {
                list1.addItem("NET LOSS : ");
                difference = totaldrperiod - totalcrperiod;
                list2.addItem("" + difference);
                System.out.print("else difference"+difference);
                System.out.print("else totalcrperiod"+totalcrperiod);
                System.out.print("else totalcrperiod"+totaldrperiod);
                jTextField2.setText("" + (totaldrperiod + totdrperiod));
                jTextField1.setText("" + (difference + totalcrperiod + totalperiod));
            }
            conn.close();
        } catch ( SQLException ex) {
            System.out.println("Error - " + ex);
        }
    }
    
    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(pals.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(pals.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(pals.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(pals.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pals(false).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    public static javax.swing.JLabel jLabel1;
    public static javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    public static javax.swing.JLabel jLabel4;
    public static javax.swing.JLabel jLabel5;
    public static javax.swing.JLabel jLabel6;
    public static javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    public static javax.swing.JTextField jTextField1;
    public static javax.swing.JTextField jTextField2;
    public static java.awt.List list1;
    public static java.awt.List list2;
    public static java.awt.List list3;
    public static java.awt.List list4;
    // End of variables declaration//GEN-END:variables

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}


abstract class PalsPrint implements Printable{
    
    
    public void printing(){
     PrinterJob job = PrinterJob.getPrinterJob();
         job.setPrintable(this);
         boolean ok = job.printDialog();
         if (ok) {
             try {
                  job.print();
             } catch (PrinterException ex) {
              /* The job did not successfully complete */
             }
         }
    
    }

    public int print1(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
   
        if(pageIndex>0){
        return Printable.NO_SUCH_PAGE;
    
        }
           //set paper size 
            Paper p = pageFormat.getPaper();
            double margin = 10;
            p.setImageableArea(margin,p.getImageableY(),p.getWidth() - 2* margin, p.getImageableHeight());
            pageFormat.setPaper(p); 
    
Graphics2D g2d=(Graphics2D)graphics;

g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
  
       //set font in drawing string 
        Font font=new Font("This Time Roman",Font.PLAIN,11);
        g2d.setFont(font);
        g2d.drawString("P & L As on Date: -"+pals.jLabel6.getText(),200,10);
        
        g2d.drawLine(12,29, 530,29); //line draw 

        g2d.drawString("Total :"+pals.jTextField2.getText(),170,720); //print total first
        g2d.drawString("Total :"+pals.jTextField1.getText(),440,720); //print total second
        g2d.drawString(pals.jLabel2.getText(),10,25); //print profit and loss current date 


//List 1 Print starting x-co-ordinate 10 and y-co-ordinate 30
      int xlist1=10;
      int ylist1=40;
                             
    
for(int index=0;index <pals.list1.getItemCount();index++){
    
g2d.drawString(pals.list1.getItem(index), xlist1,ylist1);
    
ylist1+=12;
    
}//end list1

                        //start list2  
int xlist2=205;
int ylist2=40;
g2d.drawString(pals.jLabel4.getText(),180,25);
for(int index=0;index <pals.list2.getItemCount();index++){
    
g2d.drawString(pals.list2.getItem(index), xlist2,ylist2);
          ylist2+=12;
    
}                   //end list2

                    //start list3
int xlist3=300;
int ylist3=40;

for(int index=0;index <pals.list3.getItemCount();index++){
    
g2d.drawString(pals.list3.getItem(index), xlist3,ylist3);
          ylist3+=12;
    
}             //end list3

              //start list4
int xlist4=460;
int ylist4=40;
    
for(int index=0;index <pals.list4.getItemCount();index++){
    
g2d.drawString(pals.list4.getItem(index), xlist4,ylist4);
          ylist4+=12;
    
}


return Printable.PAGE_EXISTS;

    }
}