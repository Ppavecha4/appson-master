package reporting.accounts;
//import Printing.HeaderAndFooter;

import connection.connection;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public final class pals1 extends javax.swing.JFrame implements Printable {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    static boolean isParticular = true;
    boolean isPeriodCase = false;
    long totalcr, totaldr, total, total1, totalcrperiod, totaldrperiod, totalperiod, total1period = 0;
    public int diff, totcr, totdr, totcrperiod, totdrperiod;
    static String command = null;
    public int difference = 0;
    period a = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-YYYY");
    String currentdate = formatter1.format(currentDate.getTime());
    String profitpercent1 = "";
 String barcode="";
    SimpleDateFormat yearnow = new SimpleDateFormat("yyy");
    String getyear = yearnow.format(currentDate.getTime());
    private static pals1 obj = null;

    DefaultTableModel dtm1, dtm2;

    private void addrow() {
        int tb1RowCount = jTable1.getRowCount();
        int tb2RowCount = jTable2.getRowCount();
        System.out.println("tb1 : " + tb1RowCount + " tb2 : " + tb2RowCount);
        if (tb1RowCount == tb2RowCount) {

        } else if (tb1RowCount > tb2RowCount) {
            int addrow = tb1RowCount - tb2RowCount;
            for (int i = 0; i < addrow; i++) {
                Object o[] = {"", ""};
                // System.out.println("o is "+o[i]);
                dtm2.addRow(o);
            }
        } else {
            int addrow = tb2RowCount - tb1RowCount;
            for (int i = 0; i < addrow; i++) {
                Object o[] = {"", ""};
                dtm1.addRow(o);
            }
        }
    }
    boolean theIsPeriodCase;

    public pals1() {
        a = new period();
        isPeriodCase = theIsPeriodCase;
        initComponents();
        
        
        try
        {
              connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsetting = conn1.createStatement();
            ResultSet rssetting=stsetting.executeQuery("select Barcode from setting");
            
            while(rssetting.next())
            {
        barcode =rssetting.getString(1);
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
                
        jScrollPane1.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        // the following statement binds the same BoundedRangeModel to both vertical scrollbars.
        jScrollPane1.getVerticalScrollBar().setModel(
                jScrollPane2.getVerticalScrollBar().getModel());
        dtm1 = (DefaultTableModel) jTable1.getModel();
        dtm2 = (DefaultTableModel) jTable2.getModel();

        jLabel6.setText("" + currentdate);

        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });
        jTable2.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    a = new period();
                    a.setVisible(true);
                }
            }
        });

    }

    public static pals1 getObj() {
        if (obj == null) {
            obj = new pals1();
        }
        return obj;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();

        jLabel3.setText("TOTAL :");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : PROFIT AND LOSS");
        setFocusCycleRoot(false);
        setLocationByPlatform(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel1.setFocusable(false);
        jPanel1.setMinimumSize(new java.awt.Dimension(810, 650));

        jTextField1.setEditable(false);

        jTextField2.setEditable(false);

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel1.setText("TOTAL :");

        jLabel5.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel5.setText("TOTAL :");

        jLabel6.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel6.setText("jLabel6");

        jLabel7.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel7.setText("P & L As on Date: -");

        jTable1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PARTICULARS", "Amount", "BAL."
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Long.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable1MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(210);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
        }

        jTable2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PARTICULARS", "Amount", "BAL."
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Long.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable2MousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(0).setPreferredWidth(200);
            jTable2.getColumnModel().getColumn(1).setResizable(false);
            jTable2.getColumnModel().getColumn(1).setPreferredWidth(100);
            jTable2.getColumnModel().getColumn(2).setResizable(false);
            jTable2.getColumnModel().getColumn(2).setPreferredWidth(100);
        }

        jButton1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jButton1.setText("Print");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jButton2.setText("Show Chart");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(316, 316, 316)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(17, 17, 17))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jSeparator1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(80, 80, 80)
                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 501, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 473, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(78, 78, 78)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(20, 20, 20)))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 972, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jButton1))
                        .addGap(3, 3, 3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addGap(18, 18, 18)))
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 611, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(1002, 671));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened

        jPanel1.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        if (!isPeriodCase) {
            System.out.println("trial ");
            trial();
            addrow();
        } else {
            trial_periodpals();
        }
        System.out.println("trial_periodpals ");
        addrow();

        jPanel1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    }//GEN-LAST:event_formWindowOpened

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        PalsPrint1 pp = new PalsPrint1();
        // multipage pp=new multipage();
        pp.printing();


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
//        ProfitChart chart=new ProfitChart("Profit chart");
//        chart.pack();
//        chart.setVisible(true);


    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTable1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MousePressed
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {
            command = jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString().trim();

            if (command.equals("GROSS PROFIT : ")) {

            } else if (command.equals("GROSS LOSS : ")) {

            } else if (command.equals("NET PROFIT : ")) {

            } else if (command.equals("NET LOSS : ")) {

            } else {
                reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);

            }
        }

    }//GEN-LAST:event_jTable1MousePressed

    private void jTable2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MousePressed
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {

            command = jTable1.getValueAt(jTable2.getSelectedRow(), 0).toString().trim();
            System.out.println("command is :" + command);
            if (command.equals("GROSS PROFIT : ")) {

            } else if (command.equals("GROSS LOSS : ")) {

            } else if (command.equals("NET PROFIT : ")) {

            } else if (command.equals("NET LOSS : ")) {

            } else {
                reporting.accounts.trialBalance listpr = new reporting.accounts.trialBalance(false, command, isPeriodCase);
            }
        }
    }//GEN-LAST:event_jTable2MousePressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed
    double dr = 0;
    double cr = 0;
    java.util.Date dateafter;
    java.util.Date datenow;

    DecimalFormat df = new DecimalFormat("0.00");
//double total8=0;

    /**
     * @param args the command line arguments
     */

    public void trial() {

        double sales = 0;
        double purchase = 0;
        //FOR SALES

        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();

            ResultSet rssales1 = stsales.executeQuery("select sum(curr_bal) from ledger where groups='SALES ACCOUNTS' ");

            while (rssales1.next()) {
                sales = rssales1.getDouble(1);

                Object object[] = {"SALES ACCOUNTS", "", df.format(sales)};
                dtm2.addRow(object);
            }

            ResultSet rssales = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='SALES ACCOUNTS' ");

            while (rssales.next()) {
                if (!rssales.getString(1).equals("0")) {
                    double decimalsalescurrbal = Double.parseDouble(rssales.getString(1));
                    double salescurrbal = decimalsalescurrbal;
                    Object object[] = {rssales.getString(2), df.format(salescurrbal), ""};
                    dtm2.addRow(object);

                    dr = dr + Double.parseDouble(rssales.getString(1));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            dateafter = formatter.parse(getyear + "-04-01");
            datenow = formatter.parse(dateNow);
        } catch (ParseException ex) {
            Logger.getLogger(trailbalwithopen.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (datenow.before(dateafter)) {
            getyear = Integer.toString(Integer.parseInt(getyear) - 1);

        }

        String date = getyear.concat("-04-01");

        try {
            connection c = new connection();
            try (Connection conn3 = c.cone()) {
                Statement stopening = conn3.createStatement();

                System.out.println("date for opening is" + date);
ResultSet rsopening=null;

                 if(barcode.equals("Enable"))
                 {
                 rsopening = stopening.executeQuery("select sum(ra_te*qnt)  from stockid1 ");
                 }
                 else if(barcode.equals("Disable"))
                 {
                      rsopening = stopening.executeQuery("select Amount from openingstock ");
                 }
                 long opening = 0;
                while (rsopening.next()) {
                    opening = (long) (rsopening.getDouble(1));

                }
                Object object1[] = {"OPENING STOCK", "", df.format(opening)};
                dtm1.addRow(object1);

                //    list1.add("OPENING STOCK");
                //    list2.add(""+opening);
                cr = cr + opening;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//FOR PURCHASE
        try {

            connection c = new connection();
            try (Connection conn2 = c.cone()) {
                Statement stpurchase = conn2.createStatement();

                Statement stsales = conn2.createStatement();

                ResultSet rssales1 = stsales.executeQuery("select sum(curr_bal) from ledger where groups='PURCHASE ACCOUNTS' ");

                while (rssales1.next()) {
                    Object object2[] = {"", "", ""};
                    dtm1.addRow(object2);

                    sales = rssales1.getDouble(1);
                    Object object[] = {"PURCHASEE ACCOUNTS", "", df.format(sales)};
                    dtm1.addRow(object);
                }

                ResultSet rspurchase = stpurchase.executeQuery("select curr_bal,ledger_name from ledger where groups='PURCHASE ACCOUNTS' ");

                while (rspurchase.next()) {
                    if (!rspurchase.getString(1).equals("0")) {
                        double purchasedoublevalue = Double.parseDouble(rspurchase.getString(1));

                        double purchasecurrbal = purchasedoublevalue;
                        Object object1[] = {rspurchase.getString(2), df.format(purchasecurrbal), ""};
                        dtm1.addRow(object1);

                        cr = (cr + Double.parseDouble(rspurchase.getString(1)));
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("dr after sales" + dr);

        System.out.println("dr after closing stock" + dr);

//FOR OPENING STOCK
        System.out.println("cr after opening stock" + cr);
////stock out

        ///////// for closing stock        
        try {
            connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing = conn10.createStatement();
                Statement ststockclosingout0 = conn10.createStatement();
ResultSet rsclosing=null;
                
if(barcode.equals("Enable"))
{
                 rsclosing = stclosing.executeQuery("select sum(Ra_te *qnt) from stockid2");
}
else if(barcode.equals("Disable"))
{
                 rsclosing = stclosing.executeQuery("select Amount from closingstock");
}
                double closing = 0;
                if (rsclosing.next()) {
                    closing = rsclosing.getDouble(1);
                }

                double result = closing;
                Object object2[] = {"", "", ""};
                dtm2.addRow(object2);
                
                if(closing!=0)
  {
                Object object1[] = {"CLOSING STOCK", "", df.format(result)};
                dtm2.addRow(object1);

                dr = dr + result;
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
 System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock 1       
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid21");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 1","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
            }
            }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }     

        System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock  11        
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid211");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 11","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
            }
            }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
     
        
        
                System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock 2       
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid22");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 2","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
            }
            }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
        
        System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock   3     
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid23");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 3","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
            }
            }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }

        
System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock 4        
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid24");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 4","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
            }
            }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }        
        
        System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock 41       
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid241");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 41","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
            }
}
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
        
        System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock   42     
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid242");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 42","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
            }
}
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
        
        
        System.out.println("cr after opening stock"+cr);
////stock out

     
         
          ///////// for closing stock  5      
        
        try
{
connection c = new connection();
            try (Connection conn10 = c.cone()) {
                Statement stclosing=conn10.createStatement();
                Statement ststockclosingout0=conn10.createStatement();
  
                
                ResultSet rsclosing=stclosing.executeQuery("select sum(Ra_te *qnt) from stockid25");
                double closing=0;
                if (rsclosing.next() )
                {
                    closing=rsclosing.getDouble(1);
                }

  double result=closing;
//  Object object2[]={"","",""};
//                    dtm2.addRow(object2);
  if(closing!=0)
  {
  Object object1[]={"CLOSING STOCK 5","",df.format(result)};
                         dtm2.addRow(object1);

                dr=dr+result;
  }          
  }
}
    catch(Exception e)
    {
        e.printStackTrace();
    }
//stock in
        try {
            connection c = new connection();
            try (Connection conn9 = c.cone()) {
                Statement ststockin = conn9.createStatement();
                ResultSet rsstockin = ststockin.executeQuery("select sum(ra_te) from stockrecieved");
                int stockin = 0;
                while (rsstockin.next()) {
                    stockin = rsstockin.getInt(1);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

// Direct incomes
        int direct = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();

            Statement stsales1 = conn1.createStatement();

            ResultSet rssales1 = stsales1.executeQuery("select sum(curr_bal) from ledger where groups='DIRECT INCOME' ");

            while (rssales1.next()) {
                Object object2[] = {"", "", ""};
                dtm2.addRow(object2);
                sales = rssales1.getDouble(1);
                Object object[] = {"DIRECT INCOME", "", df.format(sales)};
                dtm2.addRow(object);
            }

            ResultSet rsdirectincome = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT INCOME' ");

            while (rsdirectincome.next()) {

                if (!rsdirectincome.getString(1).equals("0")) {
                    double directincomedoublevalue = Double.parseDouble(rsdirectincome.getString(1));
                    double directincomecurrbal = directincomedoublevalue;
                    Object object1[] = {rsdirectincome.getString(2), df.format(directincomecurrbal), ""};
                    dtm2.addRow(object1);
                    dr = (dr + Double.parseDouble(rsdirectincome.getString(1)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Direct Expense
        int directexpense = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();

            Statement stsales1 = conn1.createStatement();

            ResultSet rssales1 = stsales1.executeQuery("select sum(curr_bal) from ledger where groups='DIRECT EXPENSES' ");

            while (rssales1.next()) {
                sales = rssales1.getDouble(1);
                Object object2[] = {"", "", ""};
                dtm1.addRow(object2);
                Object object[] = {"DIRECT EXPENSES", "", df.format(sales)};
                dtm1.addRow(object);
            }

            ResultSet rsdirectexpense = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='DIRECT EXPENSES' ");

            while (rsdirectexpense.next()) {
                if (!rsdirectexpense.getString(1).equals("0")) {
                    double directexpensedoublevalue = Double.parseDouble(rsdirectexpense.getString(1));
                    double directexpensecurrbal = directexpensedoublevalue;

                    Object object1[] = {rsdirectexpense.getString(2), df.format(directexpensecurrbal), ""};
                    dtm1.addRow(object1);

                    cr = (cr + Double.parseDouble(rsdirectexpense.getString(1)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        long diffss = 0;
        System.out.println(totalcr + " : " + totaldr);

        System.out.println("" + cr);
        System.out.println("" + dr);
        double result = dr - cr;
        System.out.println("result" + result);
        if (cr >= dr) {

            //    list3.addItem("GROSS LOSS ");
            diffss = (long) (cr - dr);
            //    list4.addItem("" + Math.abs(diffss));

            total = (long) (diffss + dr);

            totalcr = (long) diffss;

            double diffss1 = (double) diffss;
            double sales1 = (double) sales;
            double losspercent = (double) ((diffss1 / sales1) * 100);

            DecimalFormat df = new DecimalFormat("0.00");
            String losspercent1 = df.format(losspercent);
            Object object2[] = {"", "", ""};
            dtm2.addRow(object2);
            Object object1[] = {"GROSS LOSS " + "(" + losspercent1 + "%)", "", df.format(Math.abs(diffss))};
            dtm2.addRow(object1);
        } else if (dr > cr) {

            diffss = (long) (dr - cr);

            double diffss1 = (double) diffss;
            double sales1 = (double) sales;
            double profitpercent = (diffss1 / sales1) * 100;
            DecimalFormat df = new DecimalFormat("0.00");
            String profitpercent1 = df.format(profitpercent);

            //    list1.addItem("GROSS PROFIT "+"("+profitpercent1+"%)");
            //    list2.addItem(""+Math.abs(diffss));
            Object object2[] = {"", "", ""};
            dtm1.addRow(object2);
            Object object1[] = {"GROSS PROFIT " + "(" + profitpercent1 + "%)", "", df.format(Math.abs(diffss))};
            dtm1.addRow(object1);
            totaldr = (long) diffss;
            total = (long) (diffss + cr);

        }

        int x = jTable1.getRowCount();
        int y = jTable2.getRowCount();

        if (x > y) {

            //    list1.addItem("--------------------------------------------", x+1);
            //    list2.addItem("--------------", x+1);
            Object object1[] = {"------------------------------------------------", "---------------------", "------------------------------"};
            dtm1.addRow(object1);
            for (int i = y; i < x; i++) {
                Object object2[] = {"", ""};
                dtm2.addRow(object2);
                //    list3.addItem("", i);
                //    list4.addItem("", i);
            }
            Object object2[] = {"----------------------------------------------", "---------------------", "-----------------------"};
            dtm2.addRow(object2);
            //    list3.addItem("--------------------------------------------", x+1);
            //    list4.addItem("-------------", x+1);

        }

        if (y > x) {

            for (int i = x; i < y; i++) {
                //    list1.addItem("", i);
                //    list2.addItem("", i);
                Object object2[] = {"", ""};
                dtm1.addRow(object2);
            }
            //    list1.addItem("--------------------------------------------", y+1);
            //    list2.addItem("--------------", y+1);
            Object object2[] = {"------------------------------------------------", "---------------------", "---------------------"};
            dtm1.addRow(object2);
            //    list3.addItem("--------------------------------------------", y+1);
//    list4.addItem("--------------", y+1);    
            Object object3[] = {"----------------------------------------------", "---------------------", "---------------------"};
            dtm2.addRow(object3);
        }

//    list1.add("TOTAL");
//    list2.add(""+total);
//    list3.add("TOTAL");
//    list4.add(""+total);
        Object object2[] = {"TOTAL", "", df.format(total)};
        dtm1.addRow(object2);
        dtm2.addRow(object2);

        //    list1.addItem("--------------------------------------------");
        //    list2.addItem("--------------");
//    list3.addItem("--------------------------------------------");
//    list4.addItem("--------------");
        Object objectw[] = {"------------------------------------------------", "---------------------", "---------------------"};
        Object object5[] = {"----------------------------------------------", "--------------------", "---------------------"};
        dtm1.addRow(objectw);
        dtm2.addRow(object5);

        //    list1.add("");
        //    list2.add("");
        //    list3.add("");
        //    list4.add("");
        //    list1.add("");
        //    list2.add("");
        //    list3.add("");
        //    list4.add("");
        Object object6[] = {"", ""};
        dtm1.addRow(object6);
        dtm2.addRow(object6);
        dtm1.addRow(object6);
        dtm2.addRow(object6);

        //    list1.add("");
        //    list2.add("            PROFIT &");
        //    list3.add("LOSS ACCOUNT");
        //    list4.add("");
        Object object1[] = {"", "", "PROFIT &"};
        dtm1.addRow(object1);
        Object object11[] = {"LOSS ACCOUNT", "", ""};
        dtm2.addRow(object11);
        Object object0[] = {"------------------------------------------------", "---------------------"};
        Object object01[] = {"----------------------------------------------", "--------------------"};
        dtm1.addRow(object0);
        dtm2.addRow(object01);
        Object objectp[] = {"PARTICULARS", "AMOUNT"};
        dtm1.addRow(objectp);
        Object objectp1[] = {"PARTICULARS", "AMOUNT"};
        dtm2.addRow(objectp1);

        //    list1.addItem("--------------------------------------------");
        //    list2.addItem("--------------");
        Object objectq[] = {"------------------------------------------------", "---------------------"};
        Object object111[] = {"----------------------------------------------", "--------------------"};
        dtm1.addRow(objectq);
        dtm2.addRow(object111);
//    list3.addItem("--------------------------------------------");
//    list4.addItem("--------------"); 

        if (cr >= dr) {

            //    list1.addItem("GROSS LOSS ");
            diffss = (long) (cr - dr);
            //    list2.addItem("" + Math.abs(diffss));
            // total=(int) (diffss+dr);
            //jTextField1.setText("" + total);
            double diffss1 = (double) diffss;
            double sales1 = (double) sales;
            double losspercent = (diffss1 / sales1) * 100;
            DecimalFormat df = new DecimalFormat("0.00");
            String losspercent1 = df.format(losspercent);

            Object object3[] = {"", "", ""};
            dtm1.addRow(object3);
            Object object[] = {"GROSS LOSS " + "(" + losspercent1 + "%)", "", df.format(Math.abs(diffss))};
            dtm1.addRow(object);
            totalcr = (int) diffss;

        } else {

            diffss = (long) (dr - cr);
            double diffss1 = (double) diffss;
            double sales1 = (double) sales;
            double profitpercent = (diffss1 / sales1) * 100;
            DecimalFormat df = new DecimalFormat("0.00");
            profitpercent1 = df.format(profitpercent);

            //    list3.addItem("GROSS PROFIT "+"("+profitpercent1+"%)");
            //    list4.addItem(""+Math.abs(diffss));
            Object object3[] = {"", "", ""};
            dtm2.addRow(object3);

            Object object[] = {"GROSS PROFIT " + "(" + profitpercent1 + "%)", "", df.format(Math.abs(diffss))};
            dtm2.addRow(object);
            totaldr = (long) diffss;

        }

        //indirect incomes   
        int indirect = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            Statement stsales1 = conn1.createStatement();
            ResultSet rssales1 = stsales1.executeQuery("select sum(curr_bal) from ledger where groups='INDIRECT INCOMES' ");

            while (rssales1.next()) {
                Object object3[] = {"", "", ""};
                dtm2.addRow(object3);
                sales = rssales1.getDouble(1);
                Object object[] = {"INDIRECT INCOMES", "", df.format(sales)};
                dtm2.addRow(object);
            }

            ResultSet rsindirectincome = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='INDIRECT INCOMES' ");

            while (rsindirectincome.next()) {
                if (!rsindirectincome.getString(1).equals("0")) {
                    //    list3.add(""+rsindirectincome.getString(2));
                    //    list4.add(""+rsindirectincome.getString(1));
                    double indirectincomedoublevalue = Double.parseDouble(rsindirectincome.getString(1));
                    double indirectincomecurrbal = indirectincomedoublevalue;
                    Object object[] = {rsindirectincome.getString(2), df.format(indirectincomecurrbal), ""};
                    dtm2.addRow(object);
                    totaldr = (long) (totaldr + Double.parseDouble(rsindirectincome.getString(1)));
                }
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //indirect expense   
        int indirectexpenses = 0;
        try {
            connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsales = conn1.createStatement();
            Statement stsales1 = conn1.createStatement();
            ResultSet rssales1 = stsales1.executeQuery("select sum(curr_bal) from ledger where groups='INDIRECT EXPENSES' ");

            while (rssales1.next()) {
                Object object3[] = {"", "", ""};
                dtm1.addRow(object3);
                sales = rssales1.getDouble(1);
                Object object[] = {"INDIRECT EXPENSES", "", df.format(sales)};
                dtm1.addRow(object);
            }

            ResultSet rsindirectexpenses = stsales.executeQuery("select curr_bal,ledger_name from ledger where groups='INDIRECT EXPENSES' ");

            while (rsindirectexpenses.next()) {
                if (!rsindirectexpenses.getString(1).equals("0")) {
                    double indirectexpensedoublevalue = Double.parseDouble(rsindirectexpenses.getString(1));
                    double indirectexpensecurrbal = indirectexpensedoublevalue;

                    Object object[] = {rsindirectexpenses.getString(2), df.format(indirectexpensecurrbal), ""};
                    dtm1.addRow(object);
                    totalcr = (long) (totalcr + Double.parseDouble(rsindirectexpenses.getString(1)));
                }
            }

            conn1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (totaldr > totalcr) {
            long netprofit = totaldr - totalcr;

            double netprofit1 = (double) netprofit;
            double sales1 = (double) sales;

            double netprofitpercent = (netprofit1 / sales1) * 100;

//    list1.add("NET PROFIT");
//    list2.add(""+netprofit);
            DecimalFormat df = new DecimalFormat("0.00");
            String netprofitpercent1 = df.format(netprofitpercent);

            Object object3[] = {"", "", ""};
            dtm1.addRow(object3);
            Object object[] = {"NET PROFIT" + "(" + netprofitpercent1 + "%)", "", df.format(netprofit)};
            dtm1.addRow(object);
            jTextField1.setText("" + (long) totaldr);
            jTextField2.setText("" + (long) (totalcr + netprofit));

        }
        if (totalcr > totaldr) {

            long netloss = totalcr - totaldr;
//    list3.add("NET LOSS");
//    list4.add(""+netloss);

            Object object3[] = {"", "", ""};
            dtm2.addRow(object3);
            Object object[] = {"NET LOSS", "", df.format(netloss)};
            dtm2.addRow(object);
            jTextField1.setText("" + (long) totaldr + netloss);
            jTextField2.setText("" + (long) (totalcr));
        }
    }

    public String curr_Date = null;

    private void trial_periodpals() {
        try {
            connection c = new connection();
            Connection conn = c.cone();
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[500];
            String grpname[] = new String[50000];
            String match2[] = new String[500];
            String cretedgc[] = new String[500];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgc = conn.createStatement();
//            ResultSet rsgcc = stgc.executeQuery("select Max(Curr_Date) from LedgerFinal where Curr_Date Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
//            while(rsgcc.next())
//            {
//               curr_Date = rsgcc.getString(1);
//            }
            ResultSet rsgc = stgc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgc.next()) {
                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME")
                            || groupname.equals("PURCHASE ACCOUNTS")
                            || groupname.equals("SALES ACCOUNTS")) //                   groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                    {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1 = conn.createStatement();
                            ResultSet rsdk = stgc1.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bal = 0;
                            int bal1 = 0;
                            int balnet = 0;
                            while (rsdk.next()) {
                                String baltype = rsdk.getString(1);
                                if (baltype.equals("CR")) {
                                    bal = rsdk.getInt(2);
                                }
                                if (baltype.equals("DR")) {
                                    bal1 = rsdk.getInt(2);
                                }
                            }
                            if (bal > bal1) {
                                balnet = bal - bal1;
                                String netbalc = Integer.toString(balnet);
                                Object object[] = {groupname, netbalc};
                                dtm2.addRow(object);
                                //    list4.addItem(netbalc);
                                totalperiod = totalperiod + Integer.parseInt(netbalc);
                                //    list3.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bal1 > bal) {
                                    balnet = bal1 - bal;
                                }
                                String netbalc1 = Integer.toString(balnet);
                                Object object[] = {groupname, netbalc1};
                                dtm1.addRow(object);
                                //    list2.addItem(netbalc1);
                                total1period = total1period + Integer.parseInt(netbalc1);
                                //    list1.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                    j++;
                }
            }
//            if (totalperiod > total1period) {
//                //    list1.addItem("GROSS PROFIT : ");
//                diff = (totalperiod - total1period);
//                //    list2.addItem("" + diff);
//                Object object[]={"GROSS PROFIT : ",diff};
//                                 dtm1.addRow(object);
//                totdrperiod=total1period+diff;
////                jTextField2.setText("" + (diff + total1));
////                jTextField1.setText("" + (total));
//            } else {
//                //    list3.addItem("GROSS LOSS : ");
//                diff = (total1period - totalperiod);
//                //    list4.addItem("" + diff);
//                Object object[]={"GROSS LOSS : ",diff};
//                                 dtm2.addRow(object);
//                totcrperiod=totalperiod+diff;
////                jTextField2.setText("" + (total1));
////                jTextField1.setText("" + (diff + total));
//            }

        } catch (SQLException ex) {
            System.out.println("Error - " + ex);
        }

        try {
            connection c = new connection();
            Connection conn = c.cone();
            int ii = 0;
            int jj = 0;
            int lml = 0;
            String matchh[] = new String[500];
            String match2h[] = new String[500];
            String cretedgcc[] = new String[500];
            match2h[jj] = ("durgeshk");
            cretedgcc[lml] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgcc = conn.createStatement();
            ResultSet rsgcc = stgcc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgcc.next()) {
                aIsAdded = false;
                String groupname1 = rsgcc.getString(1);
                String createdgroup1 = rsgcc.getString(2);
                matchh[ii] = groupname1;
                if (match2h[jj].equals(matchh[ii]) || match2h[jj].equals(cretedgcc[lml])) {
                    groupname = null;
                    createdgroup = null;
                    matchh[ii] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")) {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            Statement stgc1c = conn.createStatement();
                            ResultSet rsdkc = stgc1c.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                            int bala = 0;
                            int bala1 = 0;
                            int balneta = 0;
                            while (rsdkc.next()) {
                                String baltype = rsdkc.getString(1);
                                if (baltype.equals("CR")) {
                                    bala = rsdkc.getInt(2);
                                }
                                if (baltype.equals("DR")) {
                                    bala1 = rsdkc.getInt(2);

                                }
                            }
                            if (bala > bala1) {
                                balneta = bala - bala1;
                                String netbalc = Integer.toString(balneta);
                                Object object[] = {groupname, netbalc};
                                dtm2.addRow(object);
                                //    list4.addItem(netbalc);
                                totalcrperiod = totalcrperiod + Integer.parseInt(netbalc);
                                //    list3.addItem(groupname);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            } else {
                                if (bala1 > bala) {
                                    balneta = bala1 - bala;
                                }
                                String netbalc1 = Integer.toString(balneta);
                                //    list2.addItem(netbalc1);
                                totaldrperiod = totaldrperiod + Integer.parseInt(netbalc1);
                                //    list1.addItem(groupname);
                                Object object[] = {groupname, netbalc1};
                                dtm1.addRow(object);
                                aAlreadyAddedGroupNameList.add(groupname);
                                aIsAdded = true;
                            }
                        }
                    }
                    match2h[jj + 1] = matchh[ii];
                    matchh[ii] = null;
                    jj++;
                }
            }
            if (totalcrperiod > totaldrperiod) {
                //    list3.addItem("NET PROFIT : ");
//                difference = totalcrperiod - totaldrperiod;
//                //    list4.addItem("" + difference);
//                
//                Object object[]={"NET PROFIT : ",difference};
//                                 dtm2.addRow(object);
//                System.out.print("difference"+difference);
//                System.out.print("totalcrperiod"+totalcrperiod);
//                System.out.print("totalcrperiod"+totaldrperiod);
//               jTextField2.setText("" + (difference + totaldrperiod + total1period));
//                jTextField1.setText("" + (totcrperiod + totalcrperiod));
//            } else {
//                //    list1.addItem("NET LOSS : ");
//                difference = totaldrperiod - totalcrperiod;
//                //    list2.addItem("" + difference);
//                Object object[]={"NET LOSS  : ",difference};
//                                 dtm1.addRow(object);
//                System.out.print("else difference"+difference);
//                System.out.print("else totalcrperiod"+totalcrperiod);
//                System.out.print("else totalcrperiod"+totaldrperiod);
//                jTextField2.setText("" + (totaldrperiod + totdrperiod));
//                jTextField1.setText("" + (difference + totalcrperiod + totalperiod));
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error - " + ex);
        }

    }

    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(pals1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(pals1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(pals1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(pals1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pals1().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public static javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    public static javax.swing.JLabel jLabel5;
    public static javax.swing.JLabel jLabel6;
    public static javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    public static javax.swing.JTextField jTextField1;
    public static javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    class ProfitChart extends JFrame {

        /**
         * Creates a new demo.
         *
         * @param title the frame title.
         */
//    public ProfitChart(final String title) {
//
//        super(title);
//
//        // create a dataset...
//        final DefaultValueDataset dataset = new DefaultValueDataset(new Double(profitpercent1));
//
//        // create the chart...
//        final ThermometerPlot plot = new ThermometerPlot(dataset);
//        final JFreeChart chart = new JFreeChart("Profit Chart",  // chart title
//                                          JFreeChart.DEFAULT_TITLE_FONT,
//                                          plot,                 // plot
//                                          false);               // include legend
//
//
//        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
//    //    plot.setInsets(new Insets(5, 5, 5, 5));
//        //plot.setRangeInfo(ThermometerPlot.NORMAL, 0.0, 55.0, 0.0, 100.0);
//        //plot.setRangeInfo(ThermometerPlot.WARNING, 55.0, 75.0, 0.0, 100.0);
//        //plot.setRangeInfo(ThermometerPlot.CRITICAL, 75.0, 100.0, 0.0, 100.0);
//
//        plot.setThermometerStroke(new BasicStroke(2.0f));
//        plot.setThermometerPaint(Color.black);
//        plot.setBackgroundPaint(Color.WHITE);
//        plot.setMercuryPaint(Color.red);
//        
//        // OPTIONAL CUSTOMISATION COMPLETED.
//
//        // add the chart to a panel...
//        final ChartPanel chartPanel = new ChartPanel(chart);
//        setContentPane(chartPanel);
//        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        pack();
//        requestFocus(true);
//         setSize(100,100);
//    }
        // ****************************************************************************
        // * JFREECHART DEVELOPER GUIDE                                               *
        // * The JFreeChart Developer Guide, written by David Gilbert, is available   *
        // * to purchase from Object Refinery Limited:                                *
        // *                                                                          *
        // * http://www.object-refinery.com/jfreechart/guide.html                     *
        // *                                                                          *
        // * Sales are used to provide funding for the JFreeChart project - please    * 
        // * support us so that we can continue developing free software.             *
        // ****************************************************************************
        /**
         * Starting point for the demonstration application.
         *
         * @param args ignored.
         */
        /*public static void main(final String[] args) {

        final ThermometerDemo2 demo = new ThermometerDemo2("Profite");
        demo.pack();
        demo.setVisible(true);

    }*/
    }

    class PalsPrint1 implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            //Paper paper=new Paper();
            // PageFormat pf= job.defaultPage();
            // pf.setPaper(paper);
            job.setPrintable(this);
            boolean b = job.printDialog();
            try {
                if (b == true) {
                    job.print();
                }
            } catch (PrinterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        List list1, list2, list3, list4;

        @Override
        public int print(Graphics g, PageFormat pf, int pageIndex)
                throws PrinterException {

            /* tell the caller that this page is part of the printed document */
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;

            }

            System.out.println("Pageindex is " + pageIndex);
            //set paper size 
            Paper p = pf.getPaper();
            double margin = 10;
            p.setImageableArea(margin, p.getImageableY(), p.getWidth() - 2 * margin, p.getImageableHeight());
            pf.setPaper(p);

            Graphics2D g2d = (Graphics2D) g;

            g2d.translate(pf.getImageableX(), pf.getImageableY());

            //into list
            list1 = new LinkedList<Object>();
            list2 = new LinkedList<Object>();
            list3 = new LinkedList<Object>();
            list4 = new LinkedList<Object>();

            for (int i = 0; i < jTable1.getRowCount(); i++) {
                list1.add(jTable1.getValueAt(i, 0));
                list2.add(jTable1.getValueAt(i, 1));
                list3.add(jTable2.getValueAt(i, 0));
                list4.add(jTable2.getValueAt(i, 1));
            }

            //into list
            //set font in drawing string 
            Font fonth = new Font("This Time Roman", Font.BOLD, 12);
            g2d.setFont(fonth);
//             g2d.drawString(HeaderAndFooter.getHeader(),10,50);

            Font font = new Font("This Time Roman", Font.BOLD, 8);
            g2d.setFont(font);
            FontMetrics fm1 = g2d.getFontMetrics(font);

            //              g2d.drawString(HeaderAndFooter.getHeaderAddress1(),10,60);
            //               g2d.drawString(HeaderAndFooter.getHeaderAddress2(),10,70);
            //               g2d.drawString(HeaderAndFooter.getHeaderContact(),10,80);
            Font font3 = new Font("This Time Roman", Font.BOLD, 10);
            g2d.setFont(font3);
            g2d.drawString("Trading Account", 250, 90);
            g2d.drawString("For The Period " + " 01-04-2016 TO " + jLabel6.getText(), 195, 107);
            g2d.drawString("", 240, 120);

            g2d.drawLine(12, 126, 550, 126); //line draw 111

// g2d.drawString(pals.jLabel2.getText(),10,25); //print profit and loss current date 
            Font font1 = new Font("This Time Roman", Font.PLAIN, 10);
            g2d.setFont(font1);
            FontMetrics fm = g2d.getFontMetrics(font1);

            int beforey = 160;
//List 1 Print starting x-co-ordinate 10 and y-co-ordinate 30
            int xlist1 = 20;
            int ylist1 = beforey;

            g2d.drawString("PARTICULARS", 12, ylist1 - 18);

            g2d.drawLine(12, ylist1 - 12, 550, ylist1 - 12);
            for (int index = 0; index < list1.size(); index++) {

                g2d.drawString(list1.get(index).toString(), xlist1, ylist1);

                ylist1 += 13;

            }//end list1

            //start list2  
            int xlist2 = 280;
            int ylist2 = beforey;
            g2d.drawString("AMOUNT", 235, ylist2 - 18);
            for (int index = 0; index < list2.size(); index++) {

                g2d.drawString(list2.get(index).toString(), xlist2 - fm.stringWidth(list2.get(index).toString()), ylist2);
                ylist2 += 13;

            }                   //end list2

            //start list3
            int xlist3 = 300;
            int ylist3 = beforey;
            g2d.drawString("PARTICULARS", 300, ylist3 - 18);
            for (int index = 0; index < list3.size(); index++) {

                g2d.drawString(list3.get(index).toString(), xlist3, ylist3);
                ylist3 += 13;
            }             //end list3

            //start list4
            int xlist4 = 550;
            int ylist4 = beforey;
            g2d.drawString("AMOUNT", 505, ylist4 - 18);
            for (int index = 0; index < list4.size(); index++) {

                g2d.drawString(list4.get(index).toString(), xlist4 - fm.stringWidth(list4.get(index).toString()), ylist4);
                ylist4 += 13;

            }
            Font font4 = new Font("This Time Roman", Font.BOLD, 11);
            g2d.setFont(font4);
            g2d.drawString("Total :  ", 20, 750); //print total first
            g2d.drawString("Total :  ", 305, 750); //print total second
            g2d.drawLine(12, 730, 570, 730);
            g2d.drawLine(12, 760, 570, 760);

            g2d.drawString(jTextField2.getText(), 270 - fm1.stringWidth(jTextField2.getText()), 750); //print total first
            g2d.drawString(jTextField1.getText(), 550 - fm1.stringWidth(jTextField1.getText()), 750); //print total second
            return PAGE_EXISTS;

        }
    }

    class multipage implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPrintable(this);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }

        }
        int[] pageBreaks;  // array of page break line positions.

        /* Synthesise some sample lines of text */
        String[] textLines;

        private void initTextLines() {
            int numLines = jTable1.getRowCount() + 8;
            textLines = new String[numLines];

            //               textLines[0]= HeaderAndFooter.getHeader();
            //               textLines[1]= HeaderAndFooter.getHeaderAddress1() + ", " + HeaderAndFooter.getHeaderAddress2();
//                textLines[2]= HeaderAndFooter.getHeaderContact();
            textLines[3] = "    ";
            textLines[4] = jLabel7.getText() + " " + jLabel6.getText();
            textLines[5] = "    ";
            textLines[6] = "Particular" + "                              " + "Tranding A/C" + "     " + "Particular" + "                              " + "Tranding A/C";
            textLines[7] = "    ";

            int j = 8;
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                String tb10 = jTable1.getValueAt(i, 0).toString();
                String tb11 = jTable1.getValueAt(i, 1).toString();
                String tb20 = jTable2.getValueAt(i, 0).toString();
                String tb21 = jTable2.getValueAt(i, 1).toString();
                textLines[j] = tb10 + " " + tb11 + " " + tb20 + " " + tb21;
                // tableLines[i]= tb10+" "+tb11+" "+tb20+" "+tb21;
                System.out.println("textLines[i]" + textLines[i]);
                j++;
                System.out.println("j is " + j);
            }

        }

        @Override
        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {

            Font font = new Font("Serif", Font.PLAIN, 10);
            FontMetrics metrics = graphics.getFontMetrics(font);
            int lineHeight = metrics.getHeight();

            if (pageBreaks == null) {
                initTextLines();
                int linesPerPage = (int) (pageFormat.getImageableHeight() / lineHeight);
                int numBreaks = (textLines.length - 1) / linesPerPage;
                pageBreaks = new int[numBreaks];
                for (int b = 0; b < numBreaks; b++) {
                    pageBreaks[b] = (b + 1) * linesPerPage;
                }
            }

            if (pageIndex > pageBreaks.length) {
                return NO_SUCH_PAGE;
            }

            /* User (0,0) is typically outside the imageable area, so we must
         * translate by the X and Y values in the PageFormat to avoid clipping
         * Since we are drawing text we
             */
            Graphics2D g2d = (Graphics2D) graphics;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

            /* Draw each line that is on this page.
         * Increment 'y' position by lineHeight for each line.
             */
            int y = 50;
            int start = (pageIndex == 0) ? 0 : pageBreaks[pageIndex - 1];
            int end = (pageIndex == pageBreaks.length)
                    ? textLines.length : pageBreaks[pageIndex];
            for (int line = start; line < end; line++) {
                y += lineHeight;
                System.out.println("textLines[line]  " + textLines[line]);

                try {
                    if (textLines[line].equals("null") || textLines[line].equals(null) || textLines[line].equals("") || textLines[line].contains("------")) {
                        System.out.println(" if condition ");
                    } else {
                        g2d.drawString(textLines[line], 50, y);

                    }
                } catch (NullPointerException n) {
                    n.printStackTrace();
                }
            }
            return PAGE_EXISTS;
        }

    }
}
