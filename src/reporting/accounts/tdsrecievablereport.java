/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporting.accounts;

import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author anupam staff
 */
public class tdsrecievablereport extends javax.swing.JFrame {

    /**
     * Creates new form tdspayablereport
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyy");
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    String partypan = null;

    private static tdsrecievablereport obj = null;

    public tdsrecievablereport() {
        initComponents();
        this.setLocationRelativeTo(null);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();

        ArrayList partylist = new ArrayList();

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();

            ResultSet rs = st.executeQuery("select Distinct(to_ledger) from journal where  by_ledger='TDS RECIVEBAL' ");

            String matchsno4 = "test";
            while (rs.next()) {

                String match = rs.getString(1);
                if (matchsno4.equals(match)) {

                } else {
                    partylist.add(rs.getString(1));
                    matchsno4 = match;
                }
            }

            ResultSet rsinterest = st1.executeQuery("select Distinct(by_ledger) from interestentry where  to_ledger='TDS RECIVEBAL' ");

            matchsno4 = "test";
            while (rsinterest.next()) {

                String match = rsinterest.getString(1);
                if (matchsno4.equals(match)) {

                } else {
                    if (partylist.contains(rsinterest.getString(1))) {

                    } else {
                        partylist.add(rs.getString(1));
                    }
                    matchsno4 = match;
                }
            }

            int length = partylist.size();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();

            for (int i = 0; i < length; i++) {

                int amount = 0;
                int amount1 = 0;
                int netamount = 0;
                int intrestamount = 0;
                ResultSet rs1 = st1.executeQuery("select sum(credit_amt) from journal where  by_ledger='TDS RECIVEBAL' and to_ledger='" + partylist.get(i) + "" + "' ");

                while (rs1.next()) {
                    amount = rs1.getInt(1);

                }
                ResultSet rsint = st1.executeQuery("select sum(credit_amt) from journal where  to_ledger='INTREST RECIEVE A/C' and by_ledger='" + partylist.get(i) + "" + "' ");

                while (rsint.next()) {
                    intrestamount = rsint.getInt(1);

                }

                ResultSet rsinterest1 = st1.executeQuery("select sum(amnt) from interestentry where  to_ledger='TDS RECIVEBAL' and by_ledger='" + partylist.get(i) + "" + "' ");

                while (rsinterest1.next()) {
                    amount1 = rsinterest1.getInt(1);

                }

                netamount = amount1 + amount;
                ResultSet rs2 = st.executeQuery("select * from ledger_details where ledger_name='" + partylist.get(i) + "'");
//                     System.out.println("ledger name is"+rs.getString(4));
                while (rs2.next()) {
                    if (rs2.getString(7) == null) {
                        partypan = "";
                    } else {
                        partypan = rs2.getString(7);
                    }

                }

                Object o[] = {partylist.get(i), partypan, netamount, intrestamount};
                dtm.addRow(o);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        int rowcount = jTable1.getRowCount();
        int totalamnt = 0;
        int amnt = 0;
        int intrestamt = 0;
        int totalintrestamnt = 0;
        for (int i = 0; i < rowcount; i++) {
            amnt = Integer.parseInt(jTable1.getValueAt(i, 2) + "");
            totalamnt = amnt + totalamnt;
            intrestamt = Integer.parseInt(jTable1.getValueAt(i, 3) + "");
            totalintrestamnt = intrestamt + totalintrestamnt;
        }
        jTextField1.setText("" + totalamnt);
        jTextField2.setText("" + totalintrestamnt);
    }

    public static tdsrecievablereport getObj() {
        if (obj == null) {
            obj = new tdsrecievablereport();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel6 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        jDialog1.setMinimumSize(new java.awt.Dimension(480, 135));

        jLabel6.setText("FROM: -");

        jDateChooser1.setDateFormatString("yyy-MM-dd");

        jLabel7.setText("TO: -");

        jDateChooser2.setDateFormatString("yyy-MM-dd");

        jButton1.setText("ok");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("TDS RECIEVABLE REPORT");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PARTY NAME", "PAN NO.", "TDS", "INTEREST"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(250);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
        }

        jLabel8.setText("TOTAL: -");

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jButton2.setText("Print");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addGap(144, 144, 144)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 660, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public boolean isperiod = false;
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jDialog1.dispose();
        isperiod = true;

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();

        Date date1 = jDateChooser1.getDate();
        String date01 = formatter1.format(date1);
        Date date2 = jDateChooser2.getDate();
        String date02 = formatter1.format(date2);

        String date11 = formatter.format(date1);
        String date12 = formatter.format(date2);

        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();

            ResultSet rs = st.executeQuery("select * from journal where  da_te between '" + date01 + "' and '" + date02 + "'  and to_ledger='TDS PAYABLE' ");

            String matchsno4 = "test";
            while (rs.next()) {

                String match = rs.getString("s_no");
                if (matchsno4.equals(match)) {

                } else {
                    ResultSet rs1 = st1.executeQuery("select * from ledger_details where ledger_name='" + rs.getString(4) + "' ");
                    while (rs1.next()) {
                        partypan = rs1.getString(7);
                    }
                    Date date = formatter1.parse(rs.getString(3));
                    String date13 = formatter.format(date);
                    Object o[] = {date13, rs.getString(4), partypan, rs.getString(7)};
                    dtm.addRow(o);
                    matchsno4 = match;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        int rowcount = jTable1.getRowCount();
        int totalamnt = 0;
        int amnt = 0;

        int intrestamnt = 0;
        int totalintrestamnt = 0;
        for (int i = 0; i < rowcount; i++) {
            amnt = Integer.parseInt(jTable1.getValueAt(i, 2) + "");
            totalamnt = amnt + totalamnt;
            intrestamnt = Integer.parseInt(jTable1.getValueAt(i, 3) + "");
            totalintrestamnt = intrestamnt + totalintrestamnt;
        }
        jTextField1.setText("" + totalamnt);
        jTextField2.setText("" + intrestamnt);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }

    }//GEN-LAST:event_jTable1KeyPressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }
    }//GEN-LAST:event_formKeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }
    }//GEN-LAST:event_jLabel1KeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        TDSRcvPrint pp = new TDSRcvPrint();
        pp.printing();

    }//GEN-LAST:event_jButton2ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(tdsrecievablereport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(tdsrecievablereport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(tdsrecievablereport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(tdsrecievablereport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new tdsrecievablereport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables

    class TDSRcvPrint implements Printable {

        public void printing() {
            PrinterJob job = PrinterJob.getPrinterJob();
            Paper paper = new Paper();
            PageFormat pf = job.defaultPage();
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }

        }

        @Override
        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;

            }
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            int rowcount = jTable1.getRowCount();
            int columncount = jTable1.getColumnCount();

            int x1 = 10;
            int x2 = 150;
            int x3 = 330;
            int x4 = 440;

            // int x8=452;
            double width = pageFormat.getImageableWidth();
            System.out.println("width" + width);
            Graphics2D g2d = (Graphics2D) graphics;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

            Font font1 = new Font("", Font.BOLD, 12);
            g2d.setFont(font1);
            // header section start 
            g2d.drawString(HeaderAndFooter.getHeader(), 0, 9);

            Font font2 = new Font("", Font.PLAIN, 9);
            g2d.setFont(font2);
            // g2d.drawString(jTextField1.getText().trim(), 410, 10);
            g2d.drawString(HeaderAndFooter.getHeaderAddress1(), 0, 20);
            g2d.drawString(HeaderAndFooter.getHeaderAddress2(), 0, 30);
            g2d.drawString(HeaderAndFooter.getHeaderContact(), 0, 40);
            //header section end 
            Font font3 = new Font("", Font.BOLD, 10);
            g2d.setFont(font3);
            //table lable 
            g2d.drawString(jLabel1.getText().trim(), 180, 60);
            // First table start 
            Font fonth = new Font("", Font.BOLD, 10);
            g2d.setFont(fonth);
            int x = 2;
            int y = 110;
            int endWidthX = 465;
            int endLine = 0;

            // get column and print
            g2d.drawString("PARTY NAME", 10, y);
            g2d.drawString("PAN NO.", 150, y);
            g2d.drawString("TDS.", 315, y);
            g2d.drawString("INTREST ", 420, y);

            // draw line before column 
            g2d.drawLine(0, y - 10, endWidthX, y - 10);
            //draw line after column name 
            g2d.drawLine(0, y + 5, endWidthX, y + 5);

            Font font4 = new Font("", Font.PLAIN, 10);
            g2d.setFont(font4);
            FontMetrics fm4 = g2d.getFontMetrics(font4);
            y = y + 15;
            //table row print 
            for (int i = 0; i < rowcount; i++) {
                int j = 17;
                System.out.println(j);
                g2d.drawString(jTable1.getValueAt(i, 0) + "", x1, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", x2, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 2) + "", x3 - fm4.stringWidth(jTable1.getValueAt(i, 2).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 3) + "", x4 - fm4.stringWidth(jTable1.getValueAt(i, 3).toString()), y + (j * i));
                endLine = (120 + (j * i)) + 2;
            }
            g2d.setFont(font3);
            FontMetrics fm3 = g2d.getFontMetrics(font3);
            int pageHeight = (int) pageFormat.getImageableHeight();

            g2d.drawString(jLabel8.getText().trim(), x1, pageHeight - 8);
            g2d.drawString(jTextField1.getText().trim(), x3 - fm3.stringWidth(jTextField1.getText().trim()), pageHeight - 8);
            g2d.drawString(jTextField2.getText().trim(), x4 - fm3.stringWidth(jTextField2.getText().trim()), pageHeight - 8);

            g2d.drawLine(0, pageHeight - 2, endWidthX, pageHeight - 2);
            g2d.drawLine(0, pageHeight - 20, endWidthX, pageHeight - 20);
            return Printable.PAGE_EXISTS;

        }

    }

}
