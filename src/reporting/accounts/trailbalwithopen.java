package reporting.accounts;

import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Prateek
 */
public class trailbalwithopen extends javax.swing.JFrame implements Printable {

    /**
     * Creates new form trailbalwithopen
     */
    ArrayList parti = new ArrayList();
    ArrayList cr = new ArrayList();
    ArrayList dr = new ArrayList();
    public boolean isparticularopen = false;
    public boolean istrialopen = false;
    public boolean isparticular = false;
    public boolean istrial = false;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yyy");
    String getyear = yearnow.format(currentDate.getTime());
 String barcode="";
 
    DecimalFormat df= new DecimalFormat("0.00");
    private static trailbalwithopen obj = null;

    public trailbalwithopen() {
        initComponents();
        
        try
        {
              connection c = new connection();
            Connection conn1 = c.cone();
            Statement stsetting = conn1.createStatement();
            ResultSet rssetting=stsetting.executeQuery("select Barcode from setting");
            
            while(rssetting.next())
            {
        barcode =rssetting.getString(1);
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        
        this.setLocationRelativeTo(null);

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();

        jTable1.getTableHeader().setAlignmentY(RIGHT_ALIGNMENT);

        jRadioButton1.setSelected(true);
        trial_bal();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static trailbalwithopen getObj() {
        if (obj == null) {
            obj = new trailbalwithopen();
        }
        return obj;
    }

// for trial opening balance    
    public void trial_balopen() {
        try {

            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            parti.clear();
            cr.clear();
            dr.clear();
            isparticularopen = false;
            istrialopen = true;
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            //to remove repetation
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[50000];
            String grpname[] = new String[50000];
            String match2[] = new String[50000];
            String cretedgc[] = new String[50000];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgc = connect.createStatement();
            ResultSet rsgc = stgc.executeQuery("SELECT ledger.groups, Group_create.Name, ledger.ledger_name FROM ledger left JOIN Group_create ON ledger.groups=Group_create.Under ");
            boolean aIsAdded = false;
            while (rsgc.next()) {
                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
                // System.out.println("\n enterd or replesed group: " + "" + match2[j] + "\npredifined group:" + match[i] + "\n" + "created group:" + createdgroup1 + "of " + match[i] + "\n");
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                    // System.out.println(match[i] + " " + "hello ");
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("BANK ACCOUNTS") || groupname.equals("BANK OCC A/C") || groupname.equals("BANK OD A/C")
                            || groupname.equals("BRANCH/DIVISION") || groupname.equals("CAPITAL ACCOUNT") || groupname.equals("CASH IN HAND")
                            || groupname.equals("CURRENT ASSESTS") || groupname.equals("CURRENT LIABLITIES") || groupname.equals("DEPOSITS(ASSEST)")
                            || groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME") || groupname.equals("DUTIES AND TAXES")
                            || groupname.equals("EXPENSES (DIRECT)") || groupname.equals("EXPENSES (INDIRECT)") || groupname.equals("INCOMCE(DIRECT)")
                            || groupname.equals("INCOMCE(INDIRECT)") || groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                            || groupname.equals("FIXED ASSESTS") || groupname.equals("INVESTMENTS") || groupname.equals("") || groupname.equals("")
                            || groupname.equals("") || groupname.equals("") || groupname.equals("LOANS AND ADVANCES(ASSEST)")
                            || groupname.equals("LOANS(LIABILITY)") || groupname.equals("MISC. EXPENSES(ASSEST)") || groupname.equals("PROVISIONS")
                            || groupname.equals("PURCHASE ACCOUNTS") || groupname.equals("RESERVES AND SURPLUS") || groupname.equals("RETAINED EARNINGS")
                            || groupname.equals("SALES ACCOUNTS") || groupname.equals("SECURED LOANS") || groupname.equals("STOCK IN HAND") || groupname.equals("SUNDRY CREDITORS") || groupname.equals("KTH")
                            || groupname.equals("SUNDRY DEBITORS") || groupname.equals("SUSPENSE ACCOUNTS") || groupname.equals("UNSECURED LOANS")) {
                        //  System.out.println("Ankit in ifcondigion");
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            //   System.out.println("Ankit in second if");
                            parti.add(groupname);
                            aAlreadyAddedGroupNameList.add(groupname);
                            aIsAdded = true;
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                    //  System.out.println(match2[j] + " " + "intered in match2 array " + match[i]);
                    j++;
                }
                // System.out.println(groupname + "\n");
                boolean aIsCRAlreadyAdded = false, aIsDRAlreadyAdded = false;
                ResultSet rsdk = st.executeQuery("select bal_type,sum(open_bal) from ledger where groups='" + groupname + "' or groups='" + createdgroup + "' group by bal_type");
                while (rsdk.next()) {
                    String curbaltype = rsdk.getString(1);
                    if (curbaltype.equals("CR")) {
                        if (aIsAdded && !aIsCRAlreadyAdded) {

                            if (rsdk.getLong(2) == 0) {
                                cr.add("");
                            } else {
                                cr.add(df.format(rsdk.getDouble(2)));
                            }

                            aIsCRAlreadyAdded = true;
                        }
                    } else if (curbaltype.equals("DR")) {
                        if (aIsAdded && !aIsDRAlreadyAdded) {
                            if (rsdk.getLong(2) == 0) {
                                dr.add("");
                            } else {
                                dr.add(df.format(rsdk.getDouble(2)));
                            }
                            aIsDRAlreadyAdded = true;
                        }
                    }
                }
                if (aIsDRAlreadyAdded && !aIsCRAlreadyAdded) {
                    cr.add("");
                }
                if (!aIsDRAlreadyAdded && aIsCRAlreadyAdded) {
                    dr.add("");
                }
            }
            Statement stopening = connect.createStatement();
            double openingstock = 0;
         ResultSet rsopening=null;

   if(barcode.equals("Enable"))
   {
             rsopening = stopening.executeQuery("select sum(ra_te * qnt) from stockid1  ");
   }
   else if (barcode.equals("Disable"))
   {
             rsopening = stopening.executeQuery("select Amount from openingstock  ");
   }
            while (rsopening.next()) {
                parti.add("OPENING STOCK");
                cr.add("");
                openingstock = rsopening.getDouble(1);
                dr.add("" + df.format(openingstock));
            }
            int a = parti.size();
            for (i = 0; i < a; i++) {
                String scr = cr.get(i).toString();
                String sdr = dr.get(i).toString();
//    if(scr.equals("") || scr.isEmpty()){
//        scr="0";
//    }
//    if(sdr.equals("") || sdr.isEmpty()){
//        sdr="0";
//    }
//        long crr=Long.parseLong(scr) ;
//        long drr=Long.parseLong(sdr);
                Object o[] = {parti.get(i) + "", scr, sdr};
                dtm.addRow(o);
            }
            ResultSet rs = st.executeQuery("select sum(open_bal) from ledger group by bal_type");
            rs.next();
            jLabel2.setText(rs.getString(1));
            rs.next();
            jLabel2.setText(rs.getString(1));
            ResultSet rs2 = st.executeQuery("select bal_type,sum(open_bal) from ledger where bal_type='DR' group by bal_type");
            rs2.next();
            double dr1 = rs2.getDouble(2);
            ResultSet rs3 = st.executeQuery("select bal_type,sum(open_bal) from ledger where bal_type='CR' group by bal_type");
            rs3.next();
            double cr1 = rs3.getDouble(2);
            double diff;
            dr1 = dr1 + openingstock;
            if (cr1 > dr1) {
                parti.add("DIFF. IN OPENING. BAL. : ");
                diff = cr1 - dr1;
                dr.add("" + diff);
                Object o[] = {"DIFF. IN OPENING. BAL. : ", "", df.format(diff)};
                dtm.addRow(o);
                jLabel2.setText(""+df.format(diff + dr1));
                jLabel3.setText(""+df.format(cr1));
            } else {
                parti.add("DIFF. IN OPENING. BAL. : ");
                diff = dr1 - cr1;
                cr.add("" + diff);
                Object o[] = {"DIFF. IN OPENING. BAL. : ", df.format(diff), ""};
                dtm.addRow(o);
                jLabel2.setText(""+df.format(dr1));
                jLabel3.setText("" +df.format(diff + cr1));
            }
            st.close();
            connect.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

//for particular opening balance     
    public void particularopen(String choice) {

        isparticularopen = true;
        istrialopen = false;
        parti.clear();
        dr.clear();
        cr.clear();
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        double sumcreditbal;
        double sumdebitbal;
        double sumcr = 0;
        double sumdr = 0;
        double sumucr = 0;
        double sumudr = 0;
        try {
            connection c = new connection();
            Connection connec = c.cone();
            Statement stm = connec.createStatement();
            Statement stmt1 = connec.createStatement();

            ResultSet rs1 = stm.executeQuery("select ledger_name,open_bal,bal_type from ledger where groups = '" + choice + "'");
            while (rs1.next()) {
                parti.add(rs1.getString(1));
                String baltype = (String) rs1.getString(3);
                if (baltype.equals("CR")) {
                    cr.add(df.format(rs1.getDouble(2)));
                    dr.add("");
                } else {
                    cr.add("");
                    dr.add(df.format(rs1.getDouble(2)));
                }
            }
            rs1 = stm.executeQuery("select sum(open_bal),bal_type from ledger where groups = '" + choice + "' group by bal_type");
            while (rs1.next()) {
                String baltype = (String) rs1.getString(2);

                if (baltype.equals("CR")) {
                    sumcr = rs1.getDouble(1);
                } else {
                    sumdr = rs1.getDouble(1);
                }
            }
            //for fetching from group_create 
            Statement stmg = connec.createStatement();
            Statement stmb = connec.createStatement();
            ResultSet rsg = stmg.executeQuery("select Name from Group_create where Under = '" + choice + "'");
            String underg = null;
            while (rsg.next()) {
                underg = rsg.getString(1);
                parti.add(underg);
            }
            ResultSet rsb = stmb.executeQuery("select sum(open_bal),bal_type from ledger where groups = '" + underg + " ' group by bal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    cr.add(df.format(rsb.getDouble(1)));
                    dr.add("");
                } else {
                    cr.add("");
                    dr.add(df.format(rsb.getDouble(1)));
                }
            }
            int b = parti.size();

            for (int i = 0; i < b; i++) {
                String scr = cr.get(i).toString();
                String sdr = dr.get(i).toString();
                if (scr.equals("") || scr.isEmpty()) {
                    scr = "0";
                }
                if (sdr.equals("") || sdr.isEmpty()) {
                    sdr = "0";
                }
                double crr = Double.parseDouble(scr);
                double drr = Double.parseDouble(sdr);
                Object o[] = {parti.get(i), df.format(crr), df.format(drr)};
                dtm.addRow(o);
            }

            rsb = stmb.executeQuery("select sum(open_bal),bal_type from ledger where groups = '" + underg + "' group by bal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    sumucr = rsb.getInt(1);
                } else {
                    sumudr = rsb.getInt(1);
                }
            }
            sumcreditbal = sumcr + sumucr;
            sumdebitbal = sumudr + sumudr;
            jLabel2.setText("" +df.format(sumcreditbal));
            jLabel3.setText("" +df.format(sumdebitbal) );
            stmg.close();
            stmb.close();
            stm.close();
            connec.close();
        } catch (SQLException ex) {

            ex.printStackTrace();
        }
        int rowcount = jTable1.getRowCount();
        double total = 0;
        for (int i = 0; i < rowcount; i++) {
            double total1 = Double.parseDouble(jTable1.getValueAt(i, 2).toString());
            total = total + total1;
        }
        jLabel3.setText(df.format(total));
    }
    java.util.Date dateafter;
    java.util.Date datenow;

// trial with current balance
    public void trial_bal() {

        try {
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            parti.clear();
            cr.clear();
            dr.clear();
            isparticular = false;
            istrial = true;
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();

            //to remove repetation
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[50000];
            String grpname[] = new String[50000];
            String match2[] = new String[50000];
            String cretedgc[] = new String[50000];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgc = connect.createStatement();
            ResultSet rsgc = stgc.executeQuery("SELECT ledger.groups, Group_create.Name, ledger.ledger_name FROM ledger left JOIN Group_create ON ledger.groups=Group_create.Under ");
            boolean aIsAdded = false;
            while (rsgc.next()) {
                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
                // System.out.println("\n enterd or replesed group: " + "" + match2[j] + "\npredifined group:" + match[i] + "\n" + "created group:" + createdgroup1 + "of " + match[i] + "\n");
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                    // System.out.println(match[i] + " " + "hello ");
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("BANK ACCOUNTS") || groupname.equals("BANK OCC A/C") || groupname.equals("BANK OD A/C")
                            || groupname.equals("BRANCH/DIVISION") || groupname.equals("CAPITAL ACCOUNT") || groupname.equals("CASH IN HAND")
                            || groupname.equals("CURRENT ASSESTS") || groupname.equals("CURRENT LIABLITIES") || groupname.equals("DEPOSITS(ASSEST)")
                            || groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME") || groupname.equals("DUTIES AND TAXES")
                            || groupname.equals("EXPENSES (DIRECT)") || groupname.equals("EXPENSES (INDIRECT)") || groupname.equals("INCOMCE(DIRECT)")
                            || groupname.equals("INCOMCE(INDIRECT)") || groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                            || groupname.equals("FIXED ASSESTS") || groupname.equals("INVESTMENTS") || groupname.equals("") || groupname.equals("")
                            || groupname.equals("") || groupname.equals("") || groupname.equals("LOANS AND ADVANCES(ASSEST)")
                            || groupname.equals("LOANS(LIABILITY)") || groupname.equals("MISC. EXPENSES(ASSEST)") || groupname.equals("PROVISIONS")
                            || groupname.equals("PURCHASE ACCOUNTS") || groupname.equals("RESERVES AND SURPLUS") || groupname.equals("RETAINED EARNINGS")
                            || groupname.equals("SALES ACCOUNTS") || groupname.equals("SECURED LOANS") || groupname.equals("STOCK IN HAND") || groupname.equals("SUNDRY CREDITORS") || groupname.equals("KTH")
                            || groupname.equals("SUNDRY DEBITORS") || groupname.equals("SUSPENSE ACCOUNTS") || groupname.equals("UNSECURED LOANS")) {
                        //  System.out.println("Ankit in ifcondigion");
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            //   System.out.println("Ankit in second if");
                            parti.add(groupname);
                            aAlreadyAddedGroupNameList.add(groupname);
                            aIsAdded = true;
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                    //  System.out.println(match2[j] + " " + "intered in match2 array " + match[i]);
                    j++;
                }
                // System.out.println(groupname + "\n");
                boolean aIsCRAlreadyAdded = false, aIsDRAlreadyAdded = false;
                ResultSet rsdk = st.executeQuery("select currbal_type,sum(curr_bal) from ledger where groups='" + groupname + "' or groups='" + createdgroup + "' group by currbal_type");
                while (rsdk.next()) {
                    String curbaltype = rsdk.getString(1);
                    if (curbaltype.equals("CR")) {
                        if (aIsAdded && !aIsCRAlreadyAdded) {

                            if (Double.parseDouble(rsdk.getString(2)) == 0) {
                                cr.add("");
                            } else {
                                cr.add(df.format(rsdk.getDouble(2)));
                            }

                            aIsCRAlreadyAdded = true;
                        }
                    } else if (curbaltype.equals("DR")) {
                        if (aIsAdded && !aIsDRAlreadyAdded) {

                            if (Double.parseDouble(rsdk.getString(2)) == 0) {
                                dr.add("");
                            } else {
                                dr.add(df.format(rsdk.getDouble(2)));
                            }
                            aIsDRAlreadyAdded = true;
                        }
                    }
                }
                if (aIsDRAlreadyAdded && !aIsCRAlreadyAdded) {
                    cr.add("");
                }
                if (!aIsDRAlreadyAdded && aIsCRAlreadyAdded) {
                    dr.add("");
                }
            }
            Statement stopening = connect.createStatement();
            double openingstock = 0;
            try {
                dateafter = formatter.parse(getyear + "-04-01");
                datenow = formatter.parse(dateNow);
            } catch (ParseException ex) {
                Logger.getLogger(trailbalwithopen.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);

            }
   ResultSet rsopening=null;

   if(barcode.equals("Enable"))
   {
             rsopening = stopening.executeQuery("select sum(ra_te * qnt) from stockid1  ");
   }
   else if (barcode.equals("Disable"))
   {
             rsopening = stopening.executeQuery("select Amount from openingstock  ");
   }
            while (rsopening.next()) {
                parti.add("OPENING STOCK");
                cr.add("");
                openingstock = rsopening.getDouble(1);
                dr.add("" +df.format(openingstock));
            }
            int a = parti.size();

            for (i = 0; i < a; i++) {
                String scr = cr.get(i).toString();
                String sdr = dr.get(i).toString();

                Object o[] = {parti.get(i) + "", scr, sdr};
                dtm.addRow(o);
            }

            ResultSet rs = st.executeQuery("select sum(curr_bal) from ledger group by currbal_type");

            while (rs.next()) {
                jLabel2.setText(rs.getString(1));
                jLabel2.setText(rs.getString(1));
            }
            double dr1 = 0;
            ResultSet rs2 = st.executeQuery("select currbal_type,sum(curr_bal) from ledger where currbal_type='DR' group by currbal_type");

            while (rs2.next()) {
                dr1 = rs2.getDouble(2);
            }
            double cr1 = 0;
            ResultSet rs3 = st.executeQuery("select currbal_type,sum(curr_bal) from ledger where currbal_type='CR' group by currbal_type");
            while (rs3.next()) {
                cr1 = rs3.getDouble(2);
            }
            double diff;
            dr1 = dr1 + openingstock;
            if (cr1 > dr1) {
                parti.add("DIFF. IN OPENING. BAL. : ");
                diff = cr1 - dr1;
                dr.add("" + diff);
                Object o[] = {"DIFF. IN OPENING. BAL. : ", "", df.format(diff) };
                dtm.addRow(o);
                jLabel2.setText("" + df.format(diff + dr1));
                jLabel3.setText("" + df.format(cr1));
            } else {
                parti.add("DIFF. IN OPENING. BAL. : ");
                diff = dr1 - cr1;
                cr.add("" + diff);
                Object o[] = {"DIFF. IN OPENING. BAL. : ", df.format(diff), ""};
                dtm.addRow(o);
                jLabel2.setText("" + df.format(dr1));
                jLabel3.setText("" + df.format(diff + cr1));
            }
            st.close();
            connect.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

// for trial balance
    public void particular(String choice) {

        isparticular = true;
        istrial = false;
        parti.clear();
        dr.clear();
        cr.clear();
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        double sumcreditbal;
        double sumdebitbal;
        double sumcr = 0;
        double sumdr = 0;
        double sumucr = 0;
        double sumudr = 0;
        try {
            connection c = new connection();
            Connection connec = c.cone();
            Statement stm = connec.createStatement();
            Statement stmt1 = connec.createStatement();

            ResultSet rs1 = stm.executeQuery("select ledger_name,curr_bal,currbal_type from ledger where groups = '" + choice + "'");
            while (rs1.next()) {
                parti.add(rs1.getString(1));
                String baltype = (String) rs1.getString(3);
                if (baltype.equals("CR")) {
                    if (rs1.getDouble(2) == 0) {
                        cr.add("");
                    } else {
                        cr.add(df.format(rs1.getDouble(2)));
                    }
                    dr.add("");
                } else {
                    cr.add("");
                    if (rs1.getDouble(2) == 0) {
                        dr.add("");
                    } else {
                        dr.add(df.format(rs1.getDouble(2)));
                    }
                }
            }
            rs1 = stm.executeQuery("select sum(curr_bal),currbal_type from ledger where groups = '" + choice + "' group by currbal_type");
            while (rs1.next()) {
                String baltype = (String) rs1.getString(2);

                if (baltype.equals("CR")) {
                    sumcr = rs1.getDouble(1);
                } else {
                    sumdr = rs1.getDouble(1);
                }
            }
            //for fetching from group_create 
            Statement stmg = connec.createStatement();
            Statement stmb = connec.createStatement();
            ResultSet rsg = stmg.executeQuery("select Name from Group_create where Under = '" + choice + "'");
            String underg = null;
            while (rsg.next()) {
                underg = rsg.getString(1);
                parti.add(underg);
            }
            ResultSet rsb = stmb.executeQuery("select sum(curr_bal),currbal_type from ledger where groups = '" + underg + " ' group by currbal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    if (rsb.getDouble(1) == 0) {
                        cr.add("");
                    } else {
                        cr.add(df.format(rsb.getDouble(1)));

                    }
                    dr.add("");
                } else {
                    cr.add("");
                    if (rsb.getDouble(1) == 0) {
                        dr.add("");
                    } else {
                        dr.add(df.format(rsb.getDouble(1)));
                    }
                }
            }
            int b = parti.size();

            for (int i = 0; i < b; i++) {
                String scr = cr.get(i).toString();
                String sdr = dr.get(i).toString();

               if(scr.equals("") && sdr.equals(""))
               {
                   
               }
               else
               {
                Object o[] = {parti.get(i), scr, sdr};
                dtm.addRow(o);
               }
               }

            rsb = stmb.executeQuery("select sum(curr_bal),currbal_type from ledger where groups = '" + underg + "' group by currbal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    sumucr = rsb.getDouble(1);
                } else {
                    sumudr = rsb.getDouble(1);
                }
            }
            sumcreditbal = sumcr + sumucr;
            sumdebitbal = sumudr + sumudr;
            jLabel2.setText("" + df.format(sumcreditbal));
            jLabel3.setText("" + df.format(sumdebitbal));
            stmg.close();
            stmb.close();
            stm.close();
            connec.close();
        } catch (SQLException ex) {
            System.out.println("Error : " + ex);
            ex.printStackTrace();
        }
        
        int rowcount = jTable1.getRowCount();
        double total = 0;
        for (int i = 0; i < rowcount; i++) {
            String tt = jTable1.getValueAt(i, 2).toString();
            if (tt.isEmpty() || tt.equals("")) {
                tt = "0";
            }
            double total1 = Double.parseDouble(tt);
            total = total + total1;
        }
        jLabel3.setText(df.format(total));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel5 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        jDialog1.setMinimumSize(new java.awt.Dimension(300, 120));

        jLabel5.setText("PLEASE CHOOSE THE TYPE OF TRIAL ?");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Trial with Current Balance.");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Trial with Opening Balance.");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                        .addGap(0, 4, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jRadioButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jRadioButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jRadioButton1)
                .addGap(7, 7, 7)
                .addComponent(jRadioButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("TRIAL BALANCE ");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name ", "Credit", "Debit"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setGridColor(new java.awt.Color(0, 0, 0));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(230);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
        }

        jLabel2.setText("jLabel2");

        jLabel3.setText("jLabel3");

        jLabel4.setText("GRAND TOTAL: -");

        jButton1.setText("<");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton3.setText("PRINT");
        jButton3.setToolTipText("Print Button for Printing");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 159, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(71, 71, 71)
                        .addComponent(jButton3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(118, 118, 118)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 474, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
String tbcontant = "";
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        int row = jTable1.getSelectedRow();
        tbcontant = jTable1.getValueAt(row, 0).toString();
        System.out.println("table name : " + tbcontant);
        System.out.println("row" + row);
        if (jRadioButton2.isSelected()) {
            if (evt.getClickCount() == 2) {
                if (istrialopen == true && isparticularopen == false) {
                    tbcontant = jTable1.getValueAt(row, 0) + "";
                    particularopen(jTable1.getValueAt(row, 0) + "");
                }
            }
        }

        if (jRadioButton1.isSelected()) {
            if (evt.getClickCount() == 2) {
                if (istrial = true && isparticular == false) {
                    particular(jTable1.getValueAt(row, 0) + "");
                } else if (isparticular == true && istrial == false) {
                    viewlledger vw = new viewlledger();
                    vw.vouchers(jTable1.getValueAt(row, 0) + "");
                } else {
                       
                       }
            }
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (jRadioButton2.isSelected()) {
            if (isparticularopen == true) {
                trial_balopen();
            } else {

            }
        } else if (jRadioButton1.isSelected()) {
            if (isparticular == true && istrial == false) {
                trial_bal();
            } else {

            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public int print(Graphics graphics, PageFormat pageFormat, int page)
            throws PrinterException {
        if (page > 0) {
            return NO_SUCH_PAGE;
        }
        {

            Paper paper = new Paper();
            paper.setSize(68.89d * 72, 97.45d * 72);
            pageFormat.getPaper();

            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            int rowcount = jTable1.getRowCount();
            int columncount = jTable1.getColumnCount();

            int x1 = 4;
            int x2 = 400;
            int x3 = 460;
            double width = pageFormat.getImageableWidth();
            System.out.println("width" + width);
            Graphics2D g2d = (Graphics2D) graphics;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

            Font font1 = new Font("", Font.BOLD, 12);
            g2d.setFont(font1);

            int xheader = 0;
            int yheader = 10;
            // header section start 
            g2d.drawString(HeaderAndFooter.getHeader(), xheader, yheader);

            Font font2 = new Font("", Font.PLAIN, 9);
            g2d.setFont(font2);
            // g2d.drawString(jTextField1.getText().trim(), 410, 10);
            g2d.drawString(HeaderAndFooter.getHeaderAddress1(), xheader, yheader + 10);
            g2d.drawString(HeaderAndFooter.getHeaderAddress2(), xheader, yheader + 20);
            g2d.drawString(HeaderAndFooter.getHeaderContact(), xheader, yheader + 30);
            //header section end 
            Font font3 = new Font("", Font.BOLD, 10);
            g2d.setFont(font3);
            Calendar currentDate = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String date = formatter.format(currentDate.getTime());

            //table lable 
            if (!tbcontant.equals("")) {
                g2d.drawString("Group : " + tbcontant, 160, 70);
            } else {

                g2d.drawString(jLabel1.getText().trim(), 175, 70);
            }
            g2d.drawString("AS ON " + date, 175, 85);
            // g2d.drawString("As on : "+jTextField6.getText().trim(), 190, 72);
            // g2d.drawString(jLabel9.getText().trim(), 0, 90);
            // First table start 

            int x = 4;
            int y = 110;
            int endWidthX = 590;
            int endLine = 0;

            // get column and print
            for (int i = 0; i < columncount; i++) {
                if (i == 1) {
                    x = 370;
                }
                if (i == 2) {
                    x = 433;
                }
                g2d.drawString(jTable1.getColumnName(i), x, y);
                System.out.println(x);
            }
            // draw line before column 
            g2d.drawLine(0, y - 10, endWidthX, y - 10);
            //draw line after column name 
            g2d.drawLine(0, y + 5, endWidthX, y + 5);

            Font font4 = new Font("", Font.PLAIN, 10);
            g2d.setFont(font4);
            FontMetrics fm4 = g2d.getFontMetrics(font4);
            y = y + 15;
            int k = 0;
            //table row print 
            for (int i = 0; i < rowcount; i++) {

                String col1 = jTable1.getValueAt(i, 0).toString().trim();
                String col2 = jTable1.getValueAt(i, 1).toString().trim();
                String col3 = jTable1.getValueAt(i, 2).toString().trim();

                if (col3.equals("0") || col3.equals("0.0")) {
                    col3 = "";

                }

                if (col2.equals("0") || col2.equals("0.0")) {
                    col2 = "";
                }
                if (col2.equals("") && col3.equals("")) {
                    System.out.println("in if statement ");
                } else {

                    int j = 14;

                    g2d.drawString(col1 + "", x1, y + (j * k));
                    g2d.drawString(col2 + "", x2 - fm4.stringWidth(col2), y + (j * k));
                    g2d.drawString(col3 + "", x3 - fm4.stringWidth(col3), y + (j * k));
                    endLine = (120 + (j * k)) + 2;
                    System.out.println("k is " + k);
                    k++;
                }
                System.out.println("endLine : " + endLine);
            }
            g2d.setFont(font3);
            FontMetrics fm3 = g2d.getFontMetrics(font3);
            int newendLine = endLine + 40;
            int pageHeight = (int) pageFormat.getImageableHeight();

            g2d.drawString(jLabel4.getText().trim(), x1, pageHeight - 8);
            g2d.drawString(jLabel2.getText().trim(), x2 - fm3.stringWidth(jLabel2.getText().trim()), pageHeight - 8);
            g2d.drawString(jLabel3.getText().trim(), x3 - fm3.stringWidth(jLabel3.getText().trim()), pageHeight - 8);

            g2d.drawLine(0, pageHeight - 2, endWidthX, pageHeight - 2);
            g2d.drawLine(0, pageHeight - 20, endWidthX, pageHeight - 20);
            return PAGE_EXISTS;
        }

    }


    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
        if (i2 == 0) {

            int tb1 = jTable1.getRowCount();

            if (tb1 > 0) {
                PrinterJob job = PrinterJob.getPrinterJob();
                Paper paper = new Paper();
                PageFormat pf = job.defaultPage();
                pf.setPaper(paper);
                job.setPrintable(this, pf);
                job.setPrintable(this);
                boolean b = job.printDialog();
                try {
                    if (b == true) {
                        job.print();
                    }
                } catch (PrinterException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(rootPane, "No Value To print...");
            }

        }

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();

        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }

    }//GEN-LAST:event_jTable1KeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }

    }//GEN-LAST:event_jLabel1KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }

    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }


    }//GEN-LAST:event_jButton3KeyPressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }

    }//GEN-LAST:event_formKeyPressed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // TODO add your handling code here:

        trial_bal();
        jLabel1.setText("Trial Balance");
        jDialog1.dispose();
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // TODO add your handling code here:

        trial_balopen();
        jLabel1.setText("Trial Balance with Opening balance");
        jDialog1.dispose();
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(trailbalwithopen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(trailbalwithopen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(trailbalwithopen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(trailbalwithopen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new trailbalwithopen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

}
