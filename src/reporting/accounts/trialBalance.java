package reporting.accounts;

import connection.connection;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public final class trialBalance extends JFrame {

    String curr_Date = null;
    float crd;
    float drd;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    static JLabel total, debit, credit, diff, need;
    static JPanel pane1, pane2;
    static List parti, cr, dr;
    static boolean particular = true;
    static boolean particularperiod = true;
    static boolean particulperiod = true;
    static boolean particul = true;
    boolean isPeriodCase = false;
    static JTable table = null;
    static JTextField d, n;
    String acct = null;
    period itsPeriod = null;
    JButton backword=new JButton("<");
    
    public trialBalance(final boolean isFirstFrameRequired, String selectedAccount, boolean theIsPeriodCase ) {
        initilizeComponent(isFirstFrameRequired, selectedAccount, theIsPeriodCase);
        setVisible(true);
    }
    
    public void initilizeComponent(final boolean isFirstFrameRequired, String selectedAccount, boolean theIsPeriodCase )
    {
        this.isPeriodCase = theIsPeriodCase;
        System.out.println("Ankit isPeriodCase:  " + isPeriodCase);
        setTitle("TRIAL BALANCE");
        setSize(new Dimension(800, 600));        
        setLayout(null);
        add(pane1 = new JPanel()).setBounds(10, 10, 710, 600);
        pane1.setLayout(null);
        pane1.add(backword).setBounds(0,500,75,30);
        pane1.add(new JLabel("PARTICULARS")).setBounds(0, 0, 500, 25);
        pane1.add(new JLabel("CREDIT")).setBounds(500, 0, 100, 25);
        pane1.add(new JLabel("DEBIT")).setBounds(600, 0, 100, 25);
        pane1.add(parti = new List()).setBounds(0, 25, 500, 400);
        pane1.add(cr = new List()).setBounds(500, 25, 100, 400);
        pane1.add(dr = new List()).setBounds(600, 25, 100, 400);
        cr.setEnabled(false);
        dr.setEnabled(false);
        
//        itsPeriod = new reporting.accounts.period();
        
       
        parti.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F2) {
                    itsPeriod = new period();
                    itsPeriod.setVisible(true);
                }
            }
        });

         parti.addItemListener(new java.awt.event.ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
//                if(parti.getSelectedIndex()!=-1)
//                {
                
cr.select(parti.getSelectedIndex());
dr.select(parti.getSelectedIndex());
//                }
                
            }
    } );

        if (isFirstFrameRequired ) {
            trial_bal();
        } else {
            acct = parti.getSelectedItem();
            if(acct == null)
                acct =  selectedAccount;
            parti.removeAll();
            cr.removeAll();
            dr.removeAll();

            pane1.add(total = new JLabel("GRAND TOTAL : -")).setBounds(0, 425, 500, 25);
            if (credit == null) {
                pane1.add(credit = new JLabel()).setBounds(500, 425, 100, 25);
            } else {
                credit.setText("");
            }
            if (debit == null) {
                pane1.add(debit = new JLabel()).setBounds(600, 425, 100, 25);
            } else {
                debit.setText("");
            }
            System.out.println("acct " + acct);
            if(!isPeriodCase)
            { 
                particular(acct);
                particular = false;
              
              }
            
            else
            {
                try 
                {
connection c = new connection();
Connection connect = c.cone();
                    Statement stgc = connect.createStatement();
//                    ResultSet rsgcc = stgc.executeQuery("select Max(Curr_Date) from LedgerFinal where Curr_Date Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
//                 //  System.out.println("select Max(Curr_Date) from LedgerFinal where Curr_Date Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
//                    while (rsgcc.next()) {
//                        curr_Date = rsgcc.getString(1);
//                 //  System.out.println("Current Date :"+curr_Date);     
//                        
//                    }
                }
                
                catch (Exception theException)
                {
                    theException.printStackTrace();
                }
                
                particularperiod(acct);
                particularperiod = false;
            }
            
        }
        parti.addMouseListener(new MouseAdapter() {
            @Override
            public void  mousePressed(MouseEvent ae) {
              if(ae.getClickCount()==2)
              {
                 
                if(!isPeriodCase)
                {
                    
                
                    acct=parti.getSelectedItem();       
                 
                    
   backword.setVisible(true);             
//   credit.setText("");
//   debit.setText("");
if (acct.equals("DIFF. IN OPENING. BAL. : "))
{
                

}
             
                              else if (particular) {
                    if(parti != null)
                                    parti.removeAll();
                                if(cr != null)
                                    cr.removeAll();
                                if(dr != null)
                                    dr.removeAll();
                             //   System.out.println("Ankit Parti");
                    particular(acct);
                   particular = false;
                    System.out.println("Particular n times"+ acct);
                }
                else if (particul) {
                    if(parti != null)
                                    parti.removeAll();
                                if(cr != null)
                                    cr.removeAll();
                                if(dr != null)
                                    dr.removeAll();
                    particul(acct);
                    particul = false;
                    System.out.println("particul n times"+ acct);
                } else {
                   // System.out.println("Ankit....");
                    System.out.println("vouchers n times"+ acct);
                    vouchers(acct);
                    particular = true;
                    particul = false;
                }
              }
                else
                {
                    
 //                               acct = (String) parti.getSelectedItem();
                                if(parti != null)
                                    parti.removeAll();
                                if(cr != null)
                                    cr.removeAll();
                                if(dr != null)
                                    dr.removeAll();
                                credit.setText("");
                                debit.setText("");
                                if (particularperiod) {
                                    particularperiod(acct);
                                    particularperiod = false;
                                } else if (particulperiod) {
                                    particulperiod(acct);
                                    particulperiod = false;
                                } else {
//                                    vouchersperiod(acct);
                                    particularperiod = true;
                                    particulperiod = false;
                                }
                    
                }
                
            }
            }
        });

//        period.jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
//            @Override
//            public void keyPressed(java.awt.event.KeyEvent e) {
//                char key = e.getKeyChar();
//                if (key == KeyEvent.VK_ENTER) {
//                    isPeriodCase = true;
//                    itsPeriod.dispose();
//                    if (!period.jTextField1.getText().isEmpty() || !period.jTextField2.getText().isEmpty()) {
//                        pane1.add(total = new JLabel("GRAND TOTAL : -")).setBounds(0, 425, 500, 25);
//                        if (credit == null) {
//                            pane1.add(credit = new JLabel()).setBounds(500, 425, 100, 25);
//                        } else {
//                            credit.setText("");
//                        }
//                        if (debit == null) {
//                            pane1.add(debit = new JLabel()).setBounds(600, 425, 100, 25);
//                        } else {
//                            debit.setText("");
//                        }
//                        if(parti != null)   
//                            parti.removeAll();
//                        if(cr != null)
//                            cr.removeAll();
//                        if(dr != null)
//                            dr.removeAll();
//
//                        if (isFirstFrameRequired ) {
//                            trialperiod();
//                        } else {
//                            acct = parti.getSelectedItem();
//                            particular(acct); 
//                            particular = false;
//                        }
//                        
//                    }
//                }
//            }
//        });









        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                dispose();
            }
        });
    }
    String[] value = new String[200];
    String[] values = new String[200];
    String[] finals = new String[200];
    String lname = null;
    String firstar[] = null;
    String curbaltype = null;
    int cgbalancetyp = 0;
    String grouptype = null;
    String chnggrouptype = null;
    String creategroup = null;
    String crtypenm = null;
    String cgcurtype = null;
    int balancetyp = 0;
    int balcr = 0;
    int baldr = 0;
    int totalbalcr = 0;
    static int countar = 0;
    int incr = 0;
    int incrs = 0;
    int k = 0;

    public void trial_bal() {
        try {
            System.out.println("trial_bal");
connection c = new connection();
Connection connect = c.cone();
            Statement st = connect.createStatement();
            //to remove repetation
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[50000];
            String grpname[] = new String[50000];
            String match2[] = new String[50000];
            String cretedgc[] = new String[50000];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
             Statement stgc = connect.createStatement();
            ResultSet rsgc = stgc.executeQuery("SELECT ledger.groups, Group_create.Name, ledger.ledger_name FROM ledger left JOIN Group_create ON ledger.groups=Group_create.Under ");
            boolean aIsAdded = false;
            while (rsgc.next()) {
                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
               // System.out.println("\n enterd or replesed group: " + "" + match2[j] + "\npredifined group:" + match[i] + "\n" + "created group:" + createdgroup1 + "of " + match[i] + "\n");
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                   // System.out.println(match[i] + " " + "hello ");
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("BANK ACCOUNTS") || groupname.equals("BANK OCC A/C") || groupname.equals("BANK OD A/C")
                            || groupname.equals("BRANCH/DIVISION") || groupname.equals("CAPITAL ACCOUNT") || groupname.equals("CASH IN HAND")
                            || groupname.equals("CURRENT ASSESTS") || groupname.equals("CURRENT LIABLITIES") || groupname.equals("DEPOSITS(ASSEST)")
                            || groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME") || groupname.equals("DUTIES AND TAXES")
                            || groupname.equals("EXPENSES (DIRECT)") || groupname.equals("EXPENSES (INDIRECT)") || groupname.equals("INCOMCE(DIRECT)")
                            || groupname.equals("INCOMCE(INDIRECT)") || groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                            || groupname.equals("FIXED ASSESTS") || groupname.equals("INVESTMENTS") || groupname.equals("") || groupname.equals("")
                            || groupname.equals("") || groupname.equals("") || groupname.equals("LOANS AND ADVANCES(ASSEST)")
                            || groupname.equals("LOANS(LIABILITY)") || groupname.equals("MISC. EXPENSES(ASSEST)") || groupname.equals("PROVISIONS")
                            || groupname.equals("PURCHASE ACCOUNTS") || groupname.equals("RESERVES AND SURPLUS") || groupname.equals("RETAINED EARNINGS")
                            || groupname.equals("SALES ACCOUNTS") || groupname.equals("SECURED LOANS") || groupname.equals("STOCK IN HAND") || groupname.equals("SUNDRY CREDITORS")||groupname.equals("KTH") || groupname.equals("SUNDRY CREDITORS  FAMILY AND RELATIVE") || groupname.equals("AO BSNL") || groupname.equals("PROVISION FOR TAX")
                            || groupname.equals("SUNDRY DEBITORS") || groupname.equals("SUSPENSE ACCOUNTS") || groupname.equals("UNSECURED LOANS")) {
                      //  System.out.println("Ankit in ifcondigion");
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                         //   System.out.println("Ankit in second if");
                            parti.add(groupname);
                            aAlreadyAddedGroupNameList.add(groupname);
                            aIsAdded = true;
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                  //  System.out.println(match2[j] + " " + "intered in match2 array " + match[i]);
                    j++;
                }
               // System.out.println(groupname + "\n");
                boolean aIsCRAlreadyAdded = false, aIsDRAlreadyAdded = false;
                ResultSet rsdk = st.executeQuery("select currbal_type,sum(curr_bal) from ledger where groups='" + groupname + "' or groups='" + createdgroup + "' group by currbal_type");
                while (rsdk.next()) {
                    String curbaltype = rsdk.getString(1);
                    if (curbaltype.equals("CR")) {
                        if (aIsAdded && !aIsCRAlreadyAdded) {
                            cr.add(rsdk.getString(2));
                            aIsCRAlreadyAdded = true;
                        }
                    } else if (curbaltype.equals("DR")) {
                        if (aIsAdded && !aIsDRAlreadyAdded) {
                            dr.add(rsdk.getString(2));
                            aIsDRAlreadyAdded = true;
                        }
                    }
                }
                if (aIsDRAlreadyAdded && !aIsCRAlreadyAdded) {
                    cr.add("");
                }
                if (!aIsDRAlreadyAdded && aIsCRAlreadyAdded) {
                    dr.add("");
                }
            }
            Statement stopening=connect.createStatement();
            int openingstock=0;
            ResultSet rsopening=stopening.executeQuery("select sum(ra_te) from stockid where branchid !='0' ");
            
            while (rsopening.next())
            {
                parti.add("OPENING STOCK");
                cr.add("");
                openingstock=rsopening.getInt(1);
                dr.add(""+openingstock);
            }
            
            rs = st.executeQuery("select sum(curr_bal) from ledger group by currbal_type");
            pane1.add(total = new JLabel("GRAND TOTAL : -")).setBounds(0, 425, 500, 25);
          while(rs.next())  
          {
pane1.add(credit = new JLabel(rs.getString(1))).setBounds(500, 425, 100, 25);              
     pane1.add(debit = new JLabel(rs.getString(1))).setBounds(600, 425, 100, 25);
          
          }
            
            
            int dr1=0;
            ResultSet rs2 = st.executeQuery("select currbal_type,sum(curr_bal) from ledger where currbal_type='DR' group by currbal_type");
           while (rs2.next())
           {
             dr1 = rs2.getInt(2);
           }
              int cr1=0;
            ResultSet rs3 = st.executeQuery("select currbal_type,sum(curr_bal) from ledger where currbal_type='CR' group by currbal_type");
            while(rs3.next())
            {
             cr1 = rs3.getInt(2);
            }
         
            long diff;
            dr1=dr1+openingstock;
            if (cr1 > dr1) {
                parti.addItem("DIFF. IN OPENING. BAL. : ");
                diff = cr1 - dr1;
                dr.addItem("" + diff);
                credit.setText("" + (diff + dr1));
                debit.setText("" + (cr1));
            } else {
                parti.addItem("DIFF. IN OPENING. BAL. : ");
                diff = dr1 - cr1;
                cr.addItem("" + diff);
                credit.setText("" + (dr1));
                debit.setText("" + (diff + cr1));
            }
            st.close();
            connect.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void particular(String choice) {
backword.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
System.out.println("Listner isPeriodCase  " + isPeriodCase);
remove(pane1);
initilizeComponent(true, "", isPeriodCase);

trial_bal();
backword.setVisible(false);
}});
        int sumcreditbal;
        int sumdebitbal;
        int sumcr = 0;
        int sumdr = 0;
        int sumucr = 0;
        int sumudr = 0;
        try {
connection c = new connection();
Connection connec = c.cone();
            Statement stm = connec.createStatement();
            Statement stmt1=connec.createStatement();


            ResultSet rs1 = stm.executeQuery("select ledger_name,curr_bal,currbal_type from ledger where groups = '" + choice + "'");
            while (rs1.next()) {
                parti.add(rs1.getString(1));
                String baltype = (String) rs1.getString(3);
             
                if (baltype.equals("CR")) {
                    cr.add(rs1.getString(2));
                    dr.add("");
                } else {
                    cr.add("");
                    dr.add(rs1.getString(2));
                }
            }
            rs1 = stm.executeQuery("select sum(curr_bal),currbal_type from ledger where groups = '" + choice + "' group by currbal_type");
            while (rs1.next()) {
                String baltype = (String) rs1.getString(2);

                if (baltype.equals("CR")) {
                    sumcr = rs1.getInt(1);
                } else {
                    sumdr = rs1.getInt(1);
                }
            }
            //for fetching from group_create 
            Statement stmg = connec.createStatement();
            Statement stmb = connec.createStatement();
            ResultSet rsg = stmg.executeQuery("select Name from Group_create where Under = '" + choice + "'");
            String underg = null;
            while (rsg.next()) {
                underg = rsg.getString(1);
                parti.add(underg);
            }
            ResultSet rsb = stmb.executeQuery("select sum(curr_bal),currbal_type from ledger where groups = '" + underg + " ' group by currbal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    cr.add(rsb.getString(1));
                    dr.add("");
                } else {
                    cr.add("");
                    dr.add(rsb.getString(1));
                }
            }
            rsb = stmb.executeQuery("select sum(curr_bal),currbal_type from ledger where groups = '" + underg + "' group by currbal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    sumucr = rsb.getInt(1);
                } else {
                    sumudr = rsb.getInt(1);
                }
            }
            sumcreditbal = sumcr + sumucr;
            sumdebitbal = sumudr + sumudr;
            credit.setText("" + sumcreditbal);
            debit.setText("" + sumdebitbal);
            stmg.close();
            stmb.close();
            stm.close();
            connec.close();
        } catch ( SQLException ex) {
            System.out.println("Error : " + ex);
            ex.printStackTrace();
        }
    }

    public void particul(String choice) {
        int sumcr = 0;
        int sumdr = 0;
        try {
connection c = new connection();
Connection connec = c.cone();         
Statement stm = connec.createStatement();
            // a = stm.executeQuery("select ledger_name,curr_bal,currbal_type from ledger where groups = '"+ choice +"'");
            ResultSet rs1 = stm.executeQuery("select ledger_name,curr_bal,currbal_type from ledger where groups = '" + choice + "'");
            Statement stmdk = connec.createStatement();
            ResultSet rs2 = stmdk.executeQuery("select ledger_name,curr_bal,currbal_type from ledger where groups = '" + choice + "'");
           // System.out.println(" ffff: " + rs1);
            if (rs2.next()) {
               // System.out.println("ankit true");
                while (rs1.next()) {
                    parti.add(rs1.getString(1));
                    String baltype = (String) rs1.getString(3);
                    if (baltype.equals("CR")) {
                        cr.add(rs1.getString(2));
                        dr.add("");
                    } else {
                        cr.add("");
                        dr.add(rs1.getString(2));
                    }
                }
            } else {
               // System.out.println("Ankit Ch...");
                vouchers(acct);
            }
            rs1 = stm.executeQuery("select sum(curr_bal),currbal_type from ledger where groups = '" + choice + "' group by currbal_type");
            while (rs1.next()) {
                String baltype = (String) rs1.getString(2);

                if (baltype.equals("CR")) {
                    sumcr = rs1.getInt(1);
                } else {
                    sumdr = rs1.getInt(1);
                }
            }
            credit.setText("" + sumcr);
            debit.setText("" + sumdr);
            stm.close();
            connec.close();
        } catch ( SQLException ex) {
            System.out.println("Error : " + ex);
            ex.printStackTrace();
        }
       
    }

    public void vouchers(String choice) {
        
      //  hide();
try {
connection c = new connection();
Connection conne = c.cone();
Statement stmt1 = conne.createStatement();
         // System.out.println("Ankit Chelawat: ");
   parti = null;
            Vector heads = new Vector();
            heads.add("DATE");
            heads.add("VOUCHER NO.");
            heads.add("DESCRIPTON");
            heads.add("PARTICULAR");
            heads.add("CREDIT");
            heads.add("DEBIT");
            
           
            Vector data= new Vector();
            Vector rowvector=new Vector();
            int i = 0;         

ResultSet rs=stmt1.executeQuery("Select open_bal,bal_type from ledger where ledger_name='"+choice+"'");

while(rs.next())
{
    rowvector=new Vector();
    String openbaltype=rs.getString(2);
    if(openbaltype.equals("CR"))
    {
                    rowvector.add("")  ;
                    rowvector.add("")  ;
                   rowvector.add("")  ;
                    rowvector.add("OPENING BALANCE") ; 
                     rowvector.add(rs.getString(1))  ;
                     rowvector.add("")  ;
                  
    data.add(rowvector);
    }
    else
    {
                    rowvector.add("")  ;
                    rowvector.add("")  ;
                    rowvector.add("")  ;
                    rowvector.add("OPENING BALANCE") ; 
                    rowvector.add("")  ;
                    rowvector.add(rs.getString(1))  ;

data.add(rowvector);
    }

}


ResultSet rs2 = stmt1.executeQuery("select * from Contra where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
         
            String matchsno4 = "test";
            while (rs2.next()) {
                rowvector=new Vector();
                String matchsno1 = rs2.getString(1);
                
                String abyledger=rs2.getString(3);
                String atoledger=rs2.getString(4);
                
                if (matchsno4.equals(matchsno1)) {
                } else {
                         if(choice.equals(abyledger))
                {
                 
                    rowvector.add(rs2.getString(2))  ;
                    rowvector.add(matchsno1 )  ;
                    rowvector.add(rs2.getString(6))  ;
                    rowvector.add(atoledger) ; 
                    rowvector.add(rs2.getString(5))  ;
                    rowvector.add("")  ;
            data.add(rowvector);        
               
                }               
                         else if(choice.equals(atoledger)) 
                         {
                    rowvector.add(rs2.getString(2))  ;
                    rowvector.add(matchsno1 )  ;
                    rowvector.add(rs2.getString(6))  ;
                    rowvector.add(abyledger) ; 
                    rowvector.add("")  ;
                    rowvector.add(rs2.getString(5))  ;
            data.add(rowvector);                 
                                    
                         }
                   matchsno4 = matchsno1;
                }
            
            }
            
                Statement stmtbsnl=conne.createStatement();
            ResultSet rsbsnl = stmtbsnl.executeQuery("select * from bsnlbilling where to_ledger = '" + choice + "' or by_ledger = '" + choice + "'  or disc_ledger='"+choice+"'");
         
           
            while (rsbsnl.next()) {
                rowvector=new Vector();
                String matchsno1 = rsbsnl.getString(1);
                
                String abyledger=rsbsnl.getString(8);
                String atoledger=rsbsnl.getString(7);
                String adiscledger=rsbsnl.getString(9);
                
                if (matchsno4.equals(matchsno1)) {
                } else {
                         if(choice.equals(abyledger))
                {
                 
                    rowvector.add(rsbsnl.getString(2))  ;
                    rowvector.add(matchsno1 )  ;
                    rowvector.add("")  ;
                    rowvector.add(atoledger) ; 
                    rowvector.add(rsbsnl.getString(4))  ;
                    rowvector.add("")  ;
            data.add(rowvector);        
               
                }               
                         else if(choice.equals(atoledger)) 
                         {
                    rowvector.add(rsbsnl.getString(2))  ;
                    rowvector.add(matchsno1 )  ;
                    rowvector.add("")  ;
                    rowvector.add(abyledger) ; 
                    rowvector.add("")  ;
                    rowvector.add(rsbsnl.getString(6))  ;
            data.add(rowvector);                 
                                    
                         }
                         else if (choice.equals(adiscledger))
                         {
                    rowvector.add(rsbsnl.getString(2))  ;
                    rowvector.add(matchsno1 )  ;
                    rowvector.add("")  ;
                    rowvector.add(abyledger) ; 
                    rowvector.add("")  ;
                    rowvector.add(rsbsnl.getString(5))  ;
            data.add(rowvector);                 
                                                   
                         }
                   matchsno4 = matchsno1;
                }
            
            }
            
            
            ResultSet rs3 = stmt1.executeQuery("select * from interestentry where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
         
           matchsno4 = "test";
            while (rs3.next()) {
                rowvector=new Vector();
                String matchsno1 = rs3.getString(1);
                
                String abyledger=rs3.getString("by_ledger");
                String atoledger=rs3.getString("to_ledger");
                
                 {
                         if(choice.equals(abyledger))
                {
                 
                    rowvector.add(rs3.getString(2))  ;
                    rowvector.add(matchsno1 )  ;
                    rowvector.add(rs3.getString(7))  ;
                    rowvector.add(atoledger) ; 
                    rowvector.add(rs3.getString(4))  ;
                    rowvector.add("")  ;
            data.add(rowvector);        
               
                }               
                         else if(choice.equals(atoledger)) 
                         {
                    rowvector.add(rs3.getString(2))  ;
                    rowvector.add(matchsno1 )  ;
                    rowvector.add(rs3.getString(7))  ;
                    rowvector.add(abyledger) ; 
                    rowvector.add("")  ;
                    rowvector.add(rs3.getString(4))  ;
            data.add(rowvector);                 
                                    
                         }
              //     matchsno4 = matchsno1;
                }
            
            }
            
            Statement stmtjourn = conne.createStatement();
            ResultSet rsjourn = stmtjourn.executeQuery("select * from journal where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
            parti = null;
            matchsno4 = "test";
            while (rsjourn.next()) {
                rowvector=new Vector();
                String matchsno1 = rsjourn.getString(2);                
                String atoledger=rsjourn.getString(5);
                String abyledger=rsjourn.getString(4);
                if(choice.equals(atoledger) && !rsjourn.getString(8).equals(""))
                {
                if (matchsno4.equals(matchsno1)) {
                } 
                else 
                {
                    rowvector.add(rsjourn.getString(3));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(abyledger); 
                    rowvector.add(rsjourn.getString(8));
                    rowvector.add("")  ;
                    matchsno4 = matchsno1;
            data.add(rowvector);
                }
                }
                else if (choice.equals(abyledger) && !rsjourn.getString(7).equals(""))
                {
                 if (matchsno4.equals(matchsno1)) {
                } 
                else 
                {
                    
                       rowvector.add(rsjourn.getString(3));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(atoledger); 
                    rowvector.add("");
                    rowvector.add(rsjourn.getString(7));
             
                    matchsno4 = matchsno1;
                data.add(rowvector);
                }   
                }
            
            
            }
            Statement stmtpayment = conne.createStatement();
            ResultSet rspayment = stmtpayment.executeQuery("select * from payment where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
            parti = null;
            matchsno4 = "test";
            while (rspayment.next()) {
                rowvector=new Vector();
                String matchsno1 = rspayment.getString(1);
          String     byledger=rspayment.getString(3);
          String     toledger=rspayment.getString(4);
                if (matchsno4.equals(matchsno1)) {
                } 
 else {
                    if(choice.equals(byledger))
                    {
                        rowvector.add(rspayment.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(toledger); 
                    rowvector.add(rspayment.getString(5));
                    rowvector.add("");
                    }
                    else if(choice.equals(toledger))
                    {
                        rowvector.add(rspayment.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(byledger); 
                    rowvector.add("");
                    rowvector.add(rspayment.getString(5));
                    }
                    matchsno4 = matchsno1;
data.add(rowvector);
                }

            }
            
//cash reciept
            
            Statement stmtreciept = conne.createStatement();
            ResultSet rsreciept = stmtreciept.executeQuery("select * from receipt where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
            parti = null;
            matchsno4 = "test";
            while (rsreciept.next()) {
                rowvector=new Vector();
                String matchsno1 = rsreciept.getString(1);
          String     byledger=rsreciept.getString(3);
          String     toledger=rsreciept.getString(4);
                if (matchsno4.equals(matchsno1)) {
                } 
 else {
                    if(choice.equals(byledger))
                    {
                        rowvector.add(rsreciept.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(toledger); 
                    rowvector.add("");
                    rowvector.add(rsreciept.getString(5));
                    }
                    else if(choice.equals(toledger))
                    {
                        rowvector.add(rsreciept.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(byledger); 
                    rowvector.add(rsreciept.getString(5));
                    rowvector.add("");
                    }
                    matchsno4 = matchsno1;
data.add(rowvector);
                }

            }
            
            
            Statement stmtbankpayment = conne.createStatement();
            ResultSet rsbankpayment = stmtbankpayment.executeQuery("select * from bankpayment where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
            parti = null;
            matchsno4 = "test";
            while (rsbankpayment.next()) {
                rowvector=new Vector();
                String matchsno1 = rsbankpayment.getString(1);
                String abyledger = rsbankpayment.getString(3);
                String atoledger = rsbankpayment.getString(4);
                if(choice.equals(abyledger))
                {
                    rowvector.add(rsbankpayment.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add(rsbankpayment.getString(10));
                    rowvector.add(atoledger); 
                    rowvector.add(rsbankpayment.getString(5));
                    rowvector.add("");
                    matchsno4 = matchsno1;
                data.add(rowvector);
                }
                else if(choice.equals(atoledger))
                        {
//                 if (matchsno4.equals(matchsno1)) {
//                } 
   //             else {
                    
                    rowvector.add(rsbankpayment.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add(rsbankpayment.getString(10));
                    rowvector.add(abyledger); 
                    rowvector.add("");
                    rowvector.add(rsbankpayment.getString(5));
                    
                     
              //      matchsno4 = matchsno1;
                data.add(rowvector);
              //   }           
                        }
                
           
            }
            
            Statement stmtbankreciept = conne.createStatement();
            ResultSet rsbankreciept = stmtbankreciept.executeQuery("select * from bankreceipt where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
            parti = null;
            matchsno4 = "test";
            while (rsbankreciept.next()) {
                rowvector=new Vector();
                String matchsno1 = rsbankreciept.getString(1);
                String abyledger = rsbankreciept.getString(3);
                String atoledger = rsbankreciept.getString(4);
                if(choice.equals(abyledger))
                {
                    rowvector.add(rsbankreciept.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add(rsbankreciept.getString(10));
                    rowvector.add(atoledger); 
                    rowvector.add("");
                    rowvector.add(rsbankreciept.getString(5));
                    matchsno4 = matchsno1;
                data.add(rowvector);
                }
                else if(choice.equals(atoledger))
                        {
//                 if (matchsno4.equals(matchsno1)) {
//                } 
   //             else {
                    
                    rowvector.add(rsbankreciept.getString(2));
                    rowvector.add(matchsno1 );
                    rowvector.add(rsbankreciept.getString(10));
                    rowvector.add(abyledger); 
                    rowvector.add(rsbankreciept.getString(5));
                    rowvector.add("");
                    
                     
              //      matchsno4 = matchsno1;
                data.add(rowvector);
              //   }           
                        }
                
           
            }
            
//            Statement stmtreceipt = conne.createStatement();
//            ResultSet rsreceipt = stmtreceipt.executeQuery("select * from receipt where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
//            parti = null;
//            matchsno4 = "test";
//            while (rsreceipt.next()) {
//                
//                String matchsno1 = rsreceipt.getString(1);
//                if (matchsno4.equals(matchsno1)) {
//                } else {
//                    data[i][0] = rsreceipt.getString(2);
//                    data[i][1] = matchsno1;
//                 
//                    data[i][2] = rsreceipt.getString(3);
//                    data[i][3] = rsreceipt.getString(5);
//                    data[i][4] = "";
//                    data[i][5] = rsreceipt.getString(6);
//                    i++;
//                    matchsno4 = matchsno1;
//                }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
//            }
  //          System.out.println("code is running1");
               Statement stmtpur = conne.createStatement();
  //          System.out.println("choiced account"+choice);
       if(choice.equals("DISCOUNT ACCOUNT"))
 {
 ResultSet rspur = stmtpur.executeQuery("select * from Stock where  discount != '0'");                
 parti = null;
            matchsno4 = "test";
            while (rspur.next()) {
                rowvector=new Vector();
               
                String matchsno1 =rspur.getString(4);
               if (matchsno4.equals(matchsno1)) 
               {
                
               } 
                
                else {
                    Double discountamount=(Double.parseDouble(rspur.getString(16)))-(Double.parseDouble(rspur.getString(18)));
                    rowvector.add(rspur.getString(3));
                    rowvector.add(matchsno1 );
                    rowvector.add("Discount Entry");
                    rowvector.add(rspur.getString(2)); 
                    rowvector.add(discountamount);
                    rowvector.add("");
                    matchsno4 = matchsno1;
                data.add(rowvector); 
               }
            } 
            
 
 }
    else
            {
     ResultSet rspur = stmtpur.executeQuery("select * from Stock where  to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or vataccount='"+choice+"' or taxaccount='"+choice+"' or otherchargesaccount='"+choice+"'");
                 parti = null;
            matchsno4 = "test";
            while (rspur.next()) {
                rowvector=new Vector();
                System.out.println("code is running");
                String matchsno1 =rspur.getString(4);
                String avatacc=rspur.getString(25);
                String ataxaccount=rspur.getString("taxaccount");
                String aothercharges=rspur.getString("otherchargesaccount");
                String netamount="" ,atoledger="",netamount1="";
                String abyledger=rspur.getString("by_ledger");
               if (choice.equals(avatacc))
               {
                   netamount1=rspur.getString("vatamount");
                   atoledger=rspur.getString("to_ledger");
               }
               else if(choice.equals(aothercharges))
               {
                   netamount1=rspur.getString("Other_charges");
                   atoledger=rspur.getString("to_ledger");
                   netamount="";
               }
               else if(choice.equals(ataxaccount)) 
               {
                   netamount1=rspur.getString("taxamount");
                   atoledger=rspur.getString("to_ledger");
               }
               
               else if(choice.equals(abyledger))
               {
                  atoledger=rspur.getString("to_ledger");
                  netamount1=rspur.getString("Bill_amount");
               }
              else
                {
                   
                    netamount=rspur.getString("Net_amount");
                    atoledger=rspur.getString("by_ledger");
                }
               if (matchsno4.equals(matchsno1)) {
                } 
                
                else {
                    
                    rowvector.add(rspur.getString(3));
                    rowvector.add(matchsno1 );
                    rowvector.add(rspur.getString(6));
                    rowvector.add(atoledger); 
                    rowvector.add(netamount);
                    rowvector.add(netamount1);
                    
                    matchsno4 = matchsno1;
               data.add(rowvector);
               }
            }
            }
            
     Statement stmtpurreturn = conne.createStatement();
     ResultSet rspurreturn = stmtpurreturn.executeQuery("select * from PurchaseReturn  where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or vataccount='"+choice+"' or taxaccount='"+choice+"' or otherchargesaccount='"+choice+"'  ");
    // System.out.println(""+"select * from Stock where Supplier_name = '" + choice + "' or Purchase_type = '" + choice + "' or vataccount='"+choice+"'");
            parti = null;
            matchsno4 = "test";
            while (rspurreturn.next()) 
            {
                   rowvector=new Vector();
                String matchsno1 =rspurreturn.getString("Invoice_no");
                String avatacc=rspurreturn.getString("vataccount");
                String ataxaccount=rspurreturn.getString("taxaccount");
                String aotherchargesaccount=rspurreturn.getString("otherchargesaccount");
                String netamount="" ,atoledger=rspurreturn.getString("to_ledger"),abyledger=rspurreturn.getString("by_ledger");
                
               
                String netamount1="";
               //System.out.println(""+matchsno1);
               if(choice.equals(avatacc))
                {
                    netamount=rspurreturn.getString("vatamount");
                    
                    netamount1="";
                }
               else if(choice.equals(ataxaccount))
               {
                    netamount=rspurreturn.getString("taxamt");
                    atoledger=rspurreturn.getString("to_ledger");
                    netamount1="";
                   
               }
               else if(choice.equals(aotherchargesaccount))
               {
                    netamount = rspurreturn.getString("Other_charges");
                    atoledger = rspurreturn.getString("to_ledger");
                    netamount1= "";
                   
               }
               else if(choice.equals(abyledger))
                {
                    netamount1=rspurreturn.getString("Net_amt");
                    atoledger=rspurreturn.getString("to_ledger");
                    netamount="";
                }

                   else 
               {
                   netamount=rspurreturn.getString("Net_amt");
                   netamount1="";
                   atoledger=rspurreturn.getString("by_ledger");
               }
               
               if (matchsno4.equals(matchsno1)) {
                
               
               } 
                
                else {
                    
                    rowvector.add(rspurreturn.getString("Invoice_date"));
                    rowvector.add(matchsno1 );
                    rowvector.add(rspurreturn.getString(5));
                    rowvector.add(atoledger); 
                    rowvector.add(netamount);
                    rowvector.add(netamount1);
                   
//                    data[i][0] = ;
//                    data[i][1] = atoledger;
//                    data[i][2] = matchsno1;
//                    data[i][3] = rspurreturn.getString("Purchase_type");
//                    data[i][4] = netamount;
//                    data[i][5] = netamount1;
//                    i++;
                    System.out.println("value of i"+i);
                    matchsno4 = matchsno1;
                     data.add(rowvector);
                    
                }
            }
            
          Statement  stmtsales =conne.createStatement();
          ResultSet rssales = stmtsales.executeQuery("select * from billing  where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or vataccount='"+choice+"'");
    // System.out.println(""+"select * from Stock where Supplier_name = '" + choice + "' or Purchase_type = '" + choice + "' or vataccount='"+choice+"'");
            parti = null;
            matchsno4 = "test";
            while (rssales.next()) {
                rowvector=new Vector();
                String matchsno1 =rssales.getString("bill_refrence");
                String avatacc=rssales.getString("vataccount");
                String netamount="" ,atoledger=rssales.getString("to_ledger"),abyledger=rssales.getString("by_ledger");
                String netamount1="";
               //System.out.println(""+matchsno1);
               if(choice.equals(avatacc))
                {
                    Statement stm=conne.createStatement();
                   ResultSet rscalc=stm.executeQuery("select sum(vatamnt) from billing where bill_refrence ='"+matchsno1+"'  ");
       int calculation=0;
       
       while(rscalc.next())
       {
           calculation= rscalc.getInt(1);
       }
                  netamount=""+calculation;
                    
                //    netamount=rssales.getString("vatamnt");
                    atoledger=rssales.getString("to_ledger");
                    netamount1="";
                }
               else if(choice.equals(atoledger))
               {
                   netamount="";
                   netamount1=rssales.getString("total_value_after_tax");
                   atoledger=rssales.getString("by_ledger");
               }
               else if(choice.equals(abyledger))
                {
                    Statement stm=conne.createStatement();
                   ResultSet rscalc=stm.executeQuery("select sum(rate),sum(vatamnt) from billing where bill_refrence ='"+matchsno1+"' and by_ledger='"+choice+"' ");
       Integer calculation=0;
       
       while(rscalc.next())
       {
           calculation=(int)(Double.parseDouble(rscalc.getString(1))-Double.parseDouble(rscalc.getString(2)));
       }
                  netamount=""+calculation;
       
                  atoledger=rssales.getString("to_ledger");
                }
               if (matchsno4.equals(matchsno1)) 
               {
                
               
               } 
                
                else {
  
                    rowvector.add(rssales.getString("date"));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(atoledger); 
                    rowvector.add(netamount);
                    rowvector.add(netamount1);
                    
                    matchsno4 = matchsno1;
               data.add(rowvector);
                   
                   
                 
                }
            }
          Statement  stmtsalesreturn =conne.createStatement();
          ResultSet rssalesreturn = stmtsalesreturn.executeQuery("select * from salesreturn  where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or vataccount='"+choice+"'");
    // System.out.println(""+"select * from Stock where Supplier_name = '" + choice + "' or Purchase_type = '" + choice + "' or vataccount='"+choice+"'");
            parti = null;
            matchsno4 = "test";
            while (rssalesreturn.next()) {
                                rowvector=new Vector();
                String matchsno1 =rssalesreturn.getString("bill_refrence");
                String avatacc=rssalesreturn.getString("vataccount");
                String netamount="" ,atoledger=rssalesreturn.getString("to_ledger"),abyledger=rssalesreturn.getString("by_ledger");
                String netamount1="";
               //System.out.println(""+matchsno1);
               if(choice.equals(avatacc))
                {
                    Statement stm=conne.createStatement();
                   ResultSet rscalc=stm.executeQuery("select sum(vatamnt) from salesreturn where bill_refrence ='"+matchsno1+"'  ");
       int calculation=0;
       
       while(rscalc.next())
       {
           calculation= rscalc.getInt(1);
       }
                  netamount1=""+calculation;
                    netamount="";
                    atoledger=rssalesreturn.getString("to_ledger");
 //                   netamount1=rssalesreturn.getString("vatamnt");
                }
                if(choice.equals(atoledger))
               {              
                   System.out.println("+case is running");
                   netamount1="";
                   netamount=rssalesreturn.getString("total_value_after_tax");
                   atoledger=rssalesreturn.getString("by_ledger");
               }
               else if (choice.equals(abyledger))
                {
                     Statement stm=conne.createStatement();
                   ResultSet rscalc=stm.executeQuery("select sum(rate),sum(vatamnt) from salesreturn where bill_refrence ='"+matchsno1+"' and by_ledger='"+choice+"' ");
                  Integer calculation=0;
       
       while(rscalc.next())
       {
           calculation=(int)(Double.parseDouble(rscalc.getString(1))-Double.parseDouble(rscalc.getString(2)));
       }

                    netamount1=""+calculation;
                    atoledger=rssalesreturn.getString("to_ledger");
                    netamount="";
                }
               if (matchsno4.equals(matchsno1)) 
               {
                
               
               } 
                             else {
                 rowvector.add(rssalesreturn.getString("date"));
                    rowvector.add(matchsno1 );
                    rowvector.add("");
                    rowvector.add(atoledger); 
                    rowvector.add(netamount);
                    rowvector.add(netamount1);
                    
                    matchsno4 = matchsno1;
                    data.add(rowvector);
                }
            }
                     Statement stmttobank = conne.createStatement();
            ResultSet rstobank = stmtjourn.executeQuery("select * from tobank where to_ledger = '" + choice + "' or by_ledger = '"+choice+"'");
            parti = null;
            matchsno4 = "test";
            while (rstobank.next()) {
                rowvector=new Vector();
                String matchsno1 = rstobank.getString(1);
                if (matchsno4.equals(matchsno1)) {
                } 
    else
                {
               String byledger=rstobank.getString(5);
               String toledger=rstobank.getString(6);

               if(choice.equals(byledger))
               {               
                   System.out.println("code is running for bank by ledger");
           rowvector.add(rstobank.getString(7));
                    rowvector.add(rstobank.getString(1));
                    rowvector.add("");
                    rowvector.add(toledger); 
                    rowvector.add(rstobank.getString(3));
                    rowvector.add("");
 
                
                    data.add(rowvector);
               }
               else if(choice.equals(toledger))
               {
                    System.out.println("code is running for bank to ledger");
                   rowvector.add(rstobank.getString(7));
                    rowvector.add(rstobank.getString(1));
                    rowvector.add("");
                    rowvector.add(byledger); 
                    rowvector.add("");
                    rowvector.add(rstobank.getString(3));

                
                    data.add(rowvector);
               }
                    matchsno4 = matchsno1;
                }
            }
            

            Statement stmttoHO = conne.createStatement();
            ResultSet rstoHO = stmttoHO.executeQuery("select * from toHO where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' ");
            parti = null;
            matchsno4 = "test";
            while (rstoHO.next()) {
                rowvector=new Vector();
                String matchsno1 = rstoHO.getString(2);
//                if (matchsno4.equals(matchsno1)) {
//                } 
//    else
                {
               String byledger=rstoHO.getString(6);
               String toledger=rstoHO.getString(5);

               if(choice.equals(byledger))
               {               
               rowvector.add(rstoHO.getString(7));
                    rowvector.add(rstoHO.getString(1));
                    rowvector.add("");
                    rowvector.add(toledger); 
                    rowvector.add("");
                    rowvector.add(rstoHO.getString(3));

                
                    data.add(rowvector);
               }
               else if(choice.equals(toledger))
               {
                    rowvector.add(rstoHO.getString(7));
                    rowvector.add(rstoHO.getString(1));
                    rowvector.add("");
                    rowvector.add(byledger); 
                    rowvector.add(rstoHO.getString(3));
                    rowvector.add("");

                
                    data.add(rowvector);
               }
                   
                }
           
            
            
            
            
            }
            
            
//            stmtreceipt.close();
            stmtpayment.close();
            stmtjourn.close();
            stmt1.close();
            conne.close();
            JFrame f = new JFrame(""+choice.toUpperCase());
            f.setSize(1000, 400);
 
           f.setVisible(true);
         //  JPanel panel=new JPanel();
      ///     panel.setSize(1000, 600);
       //    f.add(panel);
         f.add(table = new JTable(data, heads));
         table.setBounds(0,500, 1000, 400);

         JScrollPane scroll=new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            scroll.setSize(1000, 600);
            Collections.sort(data, new DateComparator());
            f.add(scroll);
         
            table.setModel(new javax.swing.table.DefaultTableModel(data, heads) {
                boolean[] canEdit = new boolean[] {
                    false, false, false,false, false, false
                };
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });
            int rowcount=table.getRowCount();
            double credit =0,debit =0,totalcredit=0,totaldebit=0;
            
            for( i=0;i<rowcount;i++)
            {
                if(("".equals(table.getValueAt(i, 4)+"")) || ((table.getValueAt(i, 4)+""))==null)
                {
                    credit=0;
                } else {
    
                    credit=Double.parseDouble(table.getValueAt(i, 4)+"");
                }
                
                totalcredit=credit+totalcredit;
                 
                if(("".equals(table.getValueAt(i, 5)+"")) || ((table.getValueAt(i, 5)+""))==null )
                {
                    debit=0;
                } 
                else {
                    
                debit=Double.parseDouble(table.getValueAt(i, 5)+"");        
                }
                
                totaldebit=debit+totaldebit;
                
            }
            double total=0;
            if(totalcredit>=totaldebit)
            {
             rowvector=new Vector();
             
           Double result=totalcredit-totaldebit;
rowvector.add("");
                    rowvector.add("");
                    rowvector.add("");
                    rowvector.add("CLOSING BALANCE"); 
                    rowvector.add("");
                    rowvector.add(Math.abs(result));
           total=totalcredit;
          data.add(rowvector);
                  
            }
            else if(totaldebit>totalcredit)
            {
                   rowvector=new Vector();
             
           Double result=totaldebit-totalcredit;
rowvector.add("");
                    rowvector.add("");
                    rowvector.add("");
                    rowvector.add("CLOSING BALANCE"); 
                    rowvector.add(Math.abs(result));
                    rowvector.add("");
           total=totaldebit;
          data.add(rowvector);
            }
                       rowvector=new Vector();
             
           Double result=totaldebit-totalcredit;
                    rowvector.add("");
                    rowvector.add("");
                    rowvector.add("");
                    rowvector.add("TOTAL"); 
                    rowvector.add(Math.abs(total));
                    rowvector.add(Math.abs(total));
            data.add(rowvector);
            
            TableColumn column=null;
         column= table.getColumnModel().getColumn(3);
            column.setWidth(150);
            
            
            table.addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent theMouseEvent) {
                    if(theMouseEvent.getClickCount()==2)
                    {
                    int aColumnNo = table.columnAtPoint(theMouseEvent.getPoint());
                    int aRowNo = table.rowAtPoint(theMouseEvent.getPoint());
                    if (aRowNo >= 0 && aColumnNo >= 0) {
                        System.out.println("code here");
                    }
                    DefaultTableModel dtm = (DefaultTableModel) table.getModel();
                    int selected = table.getSelectedRow();
                    String type = (String) dtm.getValueAt(selected, 2);
                    String Part[]=type.split("/");
                    String vchtype=Part[0];
                    dayBook dBook = new dayBook();
                    dBook.govind(vchtype, 1);
                    
                    } 
                }
            });
            table.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    char c = e.getKeyChar();
                    if (c == KeyEvent.VK_ENTER) {
                        int aRowNo = table.getSelectedRow();
                        if (aRowNo != -1) {
                           // System.out.println("hello..");
                        }
                    }
                }
            });
        } catch ( SQLException  ex) 
        {
            System.out.println("Error : " + ex);
            ex.printStackTrace();
        }
   
    }

    public void trialperiod() {
        try {
connection c = new connection();
Connection connect = c.cone();
            Statement st = connect.createStatement();
            //to remove repetation
            int i = 0;
            int j = 0;
            int lm = 0;
            String match[] = new String[500];
            String grpname[] = new String[50000];
            String match2[] = new String[500];
            String cretedgc[] = new String[500];
            match2[j] = ("durgeshk");
            cretedgc[lm] = ("null");
            String groupname = null;
            String createdgroup = null;
            ArrayList aAlreadyAddedGroupNameList = new ArrayList();
            Statement stgc = connect.createStatement();
//            ResultSet rsgcc = stgc.executeQuery("select Max(Curr_Date) from LedgerFinal where Curr_Date Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
//            while (rsgcc.next()) {
//                curr_Date = rsgcc.getString(1);
//            }
            ResultSet rsgc = stgc.executeQuery("SELECT LedgerFinal.groups, Group_create.Name, LedgerFinal.ledger_name FROM LedgerFinal left JOIN Group_create ON LedgerFinal.groups=Group_create.Under where Curr_Date ='" + curr_Date + "'  ");
            boolean aIsAdded = false;
            while (rsgc.next()) {
                aIsAdded = false;
                String groupname1 = rsgc.getString(1);
                String createdgroup1 = rsgc.getString(2);
                match[i] = groupname1;
                System.out.println("\n enterd or replesed group: " + "" + match2[j] + "\npredifined group:" + match[i] + "\n" + "created group:" + createdgroup1 + "of " + match[i] + "\n");
                if (match2[j].equals(match[i]) || match2[j].equals(cretedgc[lm])) {
                    System.out.println(match[i] + " " + "hello ");
                    groupname = null;
                    createdgroup = null;
                    match[i] = null;
                } else {
                    groupname = groupname1;
                    createdgroup = createdgroup1;
                    if (groupname.equals("BANK ACCOUNTS") || groupname.equals("BANK OCC A/C") || groupname.equals("BANK OD A/C")
                            || groupname.equals("BRANCH/DIVISION") || groupname.equals("CAPITAL ACCOUNT") || groupname.equals("CASH IN HAND")
                            || groupname.equals("CURRENT ASSESTS") || groupname.equals("CURRENT LIABLITIES") || groupname.equals("DEPOSITS(ASSEST)")
                            || groupname.equals("DIRECT EXPENSES") || groupname.equals("DIRECT INCOME") || groupname.equals("DUTIES AND TAXES")
                            || groupname.equals("EXPENSES (DIRECT)") || groupname.equals("EXPENSES (INDIRECT)") || groupname.equals("INCOMCE(DIRECT)")
                            || groupname.equals("INCOMCE(INDIRECT)") || groupname.equals("INDIRECT EXPENSES") || groupname.equals("INDIRECT INCOMES")
                            || groupname.equals("FIXED ASSESTS") || groupname.equals("INVESTMENTS") || groupname.equals("") || groupname.equals("")
                            || groupname.equals("") || groupname.equals("") || groupname.equals("LOANS AND ADVANCES(ASSEST)")
                            || groupname.equals("LOANS(LIABILITY)") || groupname.equals("MISC. EXPENSES(ASSEST)") || groupname.equals("PROVISIONS")
                            || groupname.equals("PURCHASE ACCOUNTS") || groupname.equals("RESERVES AND SURPLUS") || groupname.equals("RETAINED EARNINGS")
                            || groupname.equals("SALES ACCOUNTS") || groupname.equals("SECURED LOANS") || groupname.equals("STOCK IN HAND") || groupname.equals("SUNDRY CREDITORS") ||groupname.equals("KTH")
                            || groupname.equals("SUNDRY DEBITORS") || groupname.equals("SUSPENSE ACCOUNTS") || groupname.equals("UNSECURED LOANS")) {
                        if (!aAlreadyAddedGroupNameList.contains(groupname)) {
                            parti.add(groupname);
                            aAlreadyAddedGroupNameList.add(groupname);
                            aIsAdded = true;
                        }
                    }
                    match2[j + 1] = match[i];
                    match[i] = null;
                    System.out.println(match2[j] + " " + "intered in match2 array " + match[i]);
                    j++;
                }
                System.out.println(groupname + "\n");
                boolean aIsCRAlreadyAdded = false, aIsDRAlreadyAdded = false;
                ResultSet rsdk = st.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where groups='" + groupname + "' or groups='" + createdgroup + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
                while (rsdk.next()) {
                    String curbaltype = rsdk.getString(1);
                    if (curbaltype.equals("CR")) {
                        if (aIsAdded && !aIsCRAlreadyAdded) {
                            cr.add(rsdk.getString(2));
                            aIsCRAlreadyAdded = true;
                        }
                    } else if (curbaltype.equals("DR")) {
                        if (aIsAdded && !aIsDRAlreadyAdded) {
                            dr.add(rsdk.getString(2));
                            aIsDRAlreadyAdded = true;
                        }
                    }
                }
                if (aIsDRAlreadyAdded && !aIsCRAlreadyAdded) {
                    cr.add("");
                }
                if (!aIsDRAlreadyAdded && aIsCRAlreadyAdded) {
                    dr.add("");
                }
            }
            rs = st.executeQuery("select sum(curr_bal) from ledger group by currbal_type");
            pane1.add(total = new JLabel("GRAND TOTAL : -")).setBounds(0, 425, 500, 25);
            rs.next();
            pane1.add(credit = new JLabel(rs.getString(1))).setBounds(500, 425, 100, 25);
            rs.next();
            pane1.add(debit = new JLabel(rs.getString(1))).setBounds(600, 425, 100, 25);
            ResultSet rs2 = st.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where currbal_type='DR' and Curr_Date='" + curr_Date + "' group by currbal_type");
            rs2.next();
            int dr1 = rs2.getInt(2);
            ResultSet rs3 = st.executeQuery("select currbal_type,sum(curr_bal) from LedgerFinal where currbal_type='CR' and Curr_Date='" + curr_Date + "' group by currbal_type");
            rs3.next();
            int cr1 = rs3.getInt(2);
            int diff;
            if (cr1 > dr1) {
                parti.addItem("DIFF. IN OPENING. BAL. : ");
                diff = cr1 - dr1;
                dr.addItem("" + diff);
                credit.setText("" + (diff + dr1));
                debit.setText("" + (cr1));
            } else {
                parti.addItem("DIFF. IN OPENING. BAL. : ");
                diff = dr1 - cr1;
                cr.addItem("" + diff);
                credit.setText("" + (dr1));
                debit.setText("" + (diff + cr1));
            }
            st.close();
            connect.close();

        } catch (SQLException ex) {
            System.out.println("Error : " + ex);
        }

    }

    public void particularperiod(String choice) {

        int sumcreditbal;
        int sumdebitbal;
        int sumcr = 0;
        int sumdr = 0;
        int sumucr = 0;
        int sumudr = 0;
        try {
connection c = new connection();
Connection connec = c.cone();
            Statement stm = connec.createStatement();
            ResultSet rs1 = stm.executeQuery("select ledger_name,curr_bal,currbal_type from LedgerFinal where groups = '" + choice + "' and Curr_Date = '" + curr_Date + "'");
            while (rs1.next()) {
                parti.add(rs1.getString(1));
                String baltype = (String) rs1.getString(3);
                if (baltype.equals("CR")) {
                    cr.add(rs1.getString(2));
                    dr.add("");
                } else {
                    cr.add("");
                    dr.add(rs1.getString(2));
                }
            }
            rs1 = stm.executeQuery("select sum(curr_bal),currbal_type from LedgerFinal where groups = '" + choice + "' and Curr_Date='" + curr_Date + "' group by currbal_type");
            while (rs1.next()) {
                String baltype = (String) rs1.getString(2);

                if (baltype.equals("CR")) {
                    sumcr = rs1.getInt(1);
                } else {
                    sumdr = rs1.getInt(1);
                }
            }
            //for fetching from group_create 
            Statement stmg = connec.createStatement();
            Statement stmb = connec.createStatement();
            ResultSet rsg = stmg.executeQuery("select Name from Group_create where Under = '" + choice + "'");
            String underg = null;
            while (rsg.next()) {
                underg = rsg.getString(1);
                parti.add(underg);
            }
            ResultSet rsb = stmb.executeQuery("select sum(curr_bal),currbal_type from LedgerFinal where groups = '" + underg + "' and Curr_Date='" + curr_Date + "' group by currbal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    cr.add(rsb.getString(1));
                    dr.add("");
                } else {
                    cr.add("");
                    dr.add(rsb.getString(1));
                }
            }
            rsb = stmb.executeQuery("select sum(curr_bal),currbal_type from LedgerFinal where groups = '" + underg + "' and Curr_Date='" + curr_Date + "' group by currbal_type");
            while (rsb.next()) {
                String baltype = (String) rsb.getString(2);
                if (baltype.equals("CR")) {
                    sumucr = rsb.getInt(1);
                } else {
                    sumudr = rsb.getInt(1);
                }
            }
            sumcreditbal = sumcr + sumucr;
            sumdebitbal = sumudr + sumudr;
            credit.setText("" + sumcreditbal);
            debit.setText("" + sumdebitbal);
            stmg.close();
            stmb.close();
            stm.close();
            connec.close();
        } catch (SQLException ex) {
            System.out.println("Error : " + ex);
        }
    }

    public void particulperiod(String choice) {
        int sumcr = 0;
        int sumdr = 0;
        try {
connection c = new connection();
Connection connec = c.cone();
            Statement stm = connec.createStatement();
            // a = stm.executeQuery("select ledger_name,curr_bal,currbal_type from ledger where groups = '"+ choice +"'");
            ResultSet rs1 = stm.executeQuery("select ledger_name,curr_bal,currbal_type from LedgerFinal where groups = '" + choice + "' and Curr_Date ='" + curr_Date + "'  ");
            Statement stmdk = connec.createStatement();
            ResultSet rs2 = stmdk.executeQuery("select ledger_name,curr_bal,currbal_type from LedgerFinal where groups = '" + choice + "' and Curr_Date='" + curr_Date + "'");
            System.out.println(" ffff: " + rs1);
            if (rs2.next()) {
                System.out.println("ankit true");
                while (rs1.next()) {
                    parti.add(rs1.getString(1));
                    String baltype = (String) rs1.getString(3);
                    if (baltype.equals("CR")) {
                        cr.add(rs1.getString(2));
                        dr.add("");
                    } else {
                        cr.add("");
                        dr.add(rs1.getString(2));
                    }
                }
            } else {
//                vouchersperiod(acct);
            }
            rs1 = stm.executeQuery("select sum(curr_bal),currbal_type from LedgerFinal where groups = '" + choice + "' and Curr_Date = '" + curr_Date + "' group by currbal_type");
            while (rs1.next()) {
                String baltype = (String) rs1.getString(2);

                if (baltype.equals("CR")) {
                    sumcr = rs1.getInt(1);
                } else {
                    sumdr = rs1.getInt(1);
                }
            }
            credit.setText("" + sumcr);
            debit.setText("" + sumdr);
            stm.close();
            connec.close();
        } catch (SQLException ex) {
            System.out.println("Error : " + ex);
        }
        dispose();
    }

//    public void vouchersperiod(String choice) {
//        try {
//connection c = new connection();
//Connection conne = c.cone();
//            Statement stmt1 = conne.createStatement();
////            String aQuery = "select * from Contra where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and v_date between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ";
////            System.out.println("Ankit aQuery:  " + aQuery);
////            ResultSet rs2 = stmt1.executeQuery(aQuery);
////            
////            parti = null;
////            String heads[] = {"SNO", "TYPE", "DATE", "BY", "TO", "AMOUNT", "NARRATION"};
////            Object data[][] = new Object[1000][7];
////            int i = 0;
////            String matchsno4 = "test";
////            while (rs2.next()) {
////                String matchsno1 = rs2.getString(1);
////                if (matchsno4.equals(matchsno1)) {
////                } else {
////                    data[i][0] = rs2.getString(2);
////                    data[i][1] = rs2.getString(3);
////                    data[i][2] = ("contra");
////                    data[i][3] = matchsno1;
////                    data[i][4] = rs2.getString(4);
////                    data[i][5] = rs2.getString(5);
////                    data[i][6] = rs2.getString(6);
////                    i++;
////                    matchsno4 = matchsno1;
////                }
////            }
////            Statement stmtjourn = conne.createStatement();
////            ResultSet rsjourn = stmtjourn.executeQuery("select * from journal where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' and da_te Between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
////            parti = null;
////            matchsno4 = "test";
////            while (rsjourn.next()) {
////                String matchsno1 = rsjourn.getString(2);
////                if (matchsno4.equals(matchsno1)) {
////                } else {
////                    data[i][0] = rsjourn.getString(3);
////                    data[i][1] = rsjourn.getString(4);
////                    data[i][2] = ("journal");
////                    data[i][3] = matchsno1;
////                    data[i][4] = rsjourn.getString(5);
////                    data[i][5] = rsjourn.getString(7);
////                    data[i][6] = rsjourn.getString(6);
////                    i++;
////                    matchsno4 = matchsno1;
////                }
////            }
////            Statement stmtpayment = conne.createStatement();
////            ResultSet rspayment = stmtpayment.executeQuery("select * from payment where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' and da_te between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
////            parti = null;
////            matchsno4 = "test";
////            while (rspayment.next()) {
////                String matchsno1 = rspayment.getString(1);
////                if (matchsno4.equals(matchsno1)) {
////                } else {
////                    data[i][0] = rspayment.getString(2);
////                    data[i][1] = rspayment.getString(3);
////                    data[i][2] = ("payment");
////                    data[i][3] = matchsno1;
////                    data[i][4] = rspayment.getString(4);
////                    data[i][5] = rspayment.getString(5);
////                    data[i][6] = rspayment.getString(6);
////                    i++;
////                    matchsno4 = matchsno1;
////                }
////            }
////            Statement stmtreceipt = conne.createStatement();
////            ResultSet rsreceipt = stmtreceipt.executeQuery("select * from receipt where to_ledger = '" + choice + "' or by_ledger = '" + choice + "' and da_te between '" + period.jTextField1.getText() + "' and '" + period.jTextField2.getText() + "' ");
////            parti = null;
////            matchsno4 = "test";
////            while (rsreceipt.next()) {
////                String matchsno1 = rsreceipt.getString(1);
////                if (matchsno4.equals(matchsno1)) {
////                } else {
////                    data[i][0] = rsreceipt.getString(2);
////                    data[i][1] = rsreceipt.getString(3);
////                    data[i][2] = ("receipt");
////                    data[i][3] = matchsno1;
////                    data[i][4] = rsreceipt.getString(4);
////                    data[i][5] = rsreceipt.getString(5);
////                    data[i][6] = rsreceipt.getString(6);
////                    i++;
////                    matchsno4 = matchsno1;
////                }
////            }
////            stmtreceipt.close();
////            stmtpayment.close();
////            stmtjourn.close();
//            stmt1.close();
//            conne.close();
//            JFrame f = new JFrame("VOUCHERS");
//            f.setSize(600, 400);
//            f.setVisible(true);
////            f.add(table = new JTable(data, heads)).setBounds(0, 0, 600, 400);
// //           table.setModel(new javax.swing.table.DefaultTableModel(data, heads) {
//                boolean[] canEdit = new boolean[]{
//                    false, false, false, false, false, false, false
//                };
//
////                public boolean isCellEditable(int rowIndex, int columnIndex) {
////                    return canEdit[columnIndex];
////                }
////            });
////            table.addMouseListener(new MouseAdapter() {
////                public void mouseClicked(MouseEvent theMouseEvent) {
////                    int aColumnNo = table.columnAtPoint(theMouseEvent.getPoint());
////                    int aRowNo = table.rowAtPoint(theMouseEvent.getPoint());
////                    if (aRowNo >= 0 && aColumnNo >= 0) {
////                        System.out.println("code here");
////                    }
////                    DefaultTableModel dtm = (DefaultTableModel) table.getModel();
////                    int selected = table.getSelectedRow();
////                    String type = (String) dtm.getValueAt(selected, 2);
////                    System.out.println("" + type);
////                    dayBook dBook = new dayBook();
////                    dBook.govind(type, 1);
////                }
////            });
////            table.addKeyListener(new KeyAdapter() {
////                @Override
////                public void keyPressed(KeyEvent e) {
////                    char c = e.getKeyChar();
////                    if (c == KeyEvent.VK_ENTER) {
////                        int aRowNo = table.getSelectedRow();
////                        if (aRowNo != -1) {
////                            System.out.println("hello..");
////                        }
////                    }
////                }
////            });
////        } catch (SQLException  ex) {
////            System.out.println("Error : " + ex);
////        }
//    }

    public static void main(String args[]) {
         System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(trialBalance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(trialBalance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(trialBalance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(trialBalance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                trialBalance trail = new trialBalance(true, "", false);
                trail.setVisible(true);

            }
        });
    }
}