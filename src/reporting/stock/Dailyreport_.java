package reporting.stock;

import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import transaction.New_Dual_Billing;
import transaction.New_Dual_SalesReturn;

/**
 *
 * @author admin
 */
public class Dailyreport_ extends javax.swing.JFrame implements Printable {

    /**
     * Creates new form Dailyreport_
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    Calendar currentDate1 = Calendar.getInstance();
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
    String dateNow1 = formatter1.format(currentDate1.getTime());
    Calendar currentDate2 = Calendar.getInstance();
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyy");
    String dateNow2 = formatter2.format(currentDate2.getTime());

    SimpleDateFormat formatter4 = new SimpleDateFormat("dd-MM-yyy");
    String dateNow4 = formatter.format(currentDate.getTime());
    double totalquantity = 0;
    double totalamount = 0;
    double amount = 0;
    double quantity = 0;

    double totalquantityreturn = 0;
    double totalamountreturn = 0;
    double amountreturn = 0;
    double quantityreturn = 0;
    double netamount = 0;
    double netquantity = 0;

    private static Dailyreport_ obj = null;

    public Dailyreport_() {
        initComponents();
        this.setLocationRelativeTo(null);
        jTextField6.setText(dateNow4);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  * FROM Billing where date='" + dateNow + "'");
            String matchsno4 = "test";
            while (rs.next()) {
                String matchsno1 = rs.getString(1);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    DefaultTableModel dtm;
                    dtm = (DefaultTableModel) jTable1.getModel();

                    Object o[] = {formatter4.format(formatter.parse(rs.getString(2))), rs.getString(1), rs.getInt(18), rs.getInt(22)};
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }

//SALES RETURN                
            ResultSet rs1 = st.executeQuery("SELECT  * FROM salesreturn where date='" + dateNow + "'");
            String matchsno = "test";
            while (rs1.next()) {

                String matchsno1 = rs1.getString(1);
                if (matchsno.equals(matchsno1)) {
                } else {
                    DefaultTableModel dtm;
                    dtm = (DefaultTableModel) jTable3.getModel();
//                    int qty=Integer.parseInt(rs1.getString(18));
//                    int amnt=Integer.parseInt(rs1.getString(22));
                    Object o[] = {rs1.getString(2), rs1.getString(1), rs1.getInt(18), rs1.getInt(22)};
                    dtm.addRow(o);
                    matchsno = matchsno1;
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(Dailyreport_.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        int rowcount = jTable1.getRowCount();

        for (int i = 0; i < rowcount; i++) {
            amount = Double.parseDouble(jTable1.getValueAt(i, 3) + "");
            quantity = Double.parseDouble(jTable1.getValueAt(i, 2) + "");
            totalamount = amount + totalamount;
            totalquantity = quantity + totalquantity;
        }

        if (totalamount == 0 && totalquantity == 0) {
            jTextField4.setText("" + "");
            jTextField5.setText("" + "");
        } else {
            jTextField4.setText("" + (int) totalquantity);
            jTextField5.setText("" + (int) totalamount);
        }
        int rowcount1 = jTable3.getRowCount();

        for (int i = 0; i < rowcount1; i++) {
            amountreturn = Double.parseDouble(jTable3.getValueAt(i, 3) + "");
            quantityreturn = Double.parseDouble(jTable3.getValueAt(i, 2) + "");
            totalamountreturn = amountreturn + totalamountreturn;
            totalquantityreturn = quantityreturn + totalquantityreturn;
        }

        if (totalquantityreturn == 0 && totalamountreturn == 0) {
            jTextField7.setText("" + "");
            jTextField8.setText("" + "");
        } else {
            jTextField7.setText("" + (int) totalquantityreturn);
            jTextField8.setText("" + (int) totalamountreturn);
        }
        netamount = totalamount - totalamountreturn;
        netquantity = totalquantity - totalquantityreturn;

        if (netamount == 0 && netquantity == 0) {
            jTextField9.setText("" + "");
            jTextField10.setText("" + "");
        } else {
            jTextField9.setText("" + (int) netquantity);
            jTextField10.setText("" + (int) netamount);
        }
    }

    public static Dailyreport_ getObj() {
        if (obj == null) {
            obj = new Dailyreport_();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jDialog3 = new javax.swing.JDialog();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel16 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();

        jDialog1.setMinimumSize(new java.awt.Dimension(250, 100));

        jLabel1.setText("Enter Date : ");

        jDateChooser1.setDateFormatString("yyy-MM-dd");

        jButton1.setText("GO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(362, 160));

        jLabel2.setText("Enter Period :");

        jLabel3.setText("From :");

        jLabel4.setText("To :");

        jDateChooser2.setDateFormatString("yyy-MM-dd");

        jDateChooser3.setDateFormatString("yyy-MM-dd");

        jButton2.setText("GO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(53, 53, 53))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jDialog3.setMinimumSize(new java.awt.Dimension(235, 398));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "STOCK NO.", "RATE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(1).setResizable(false);
        }

        jLabel16.setText("Total Amnt: -");

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 40, Short.MAX_VALUE))
        );

        jDialog3.getAccessibleContext().setAccessibleParent(null);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON: SALES DAILY REPORT");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date ", "Bill No.", "Quantity ", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(5);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
        }

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("BILL WISE SALES REPORT");

        jLabel6.setText("Total Quantity: -");

        jTextField4.setEditable(false);
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel7.setText("Total amount: -");

        jTextField5.setEditable(false);
        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });

        jTextField6.setEditable(false);
        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("SALES");

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No.", "Quantity", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(jTable3);
        if (jTable3.getColumnModel().getColumnCount() > 0) {
            jTable3.getColumnModel().getColumn(0).setResizable(false);
            jTable3.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable3.getColumnModel().getColumn(1).setResizable(false);
            jTable3.getColumnModel().getColumn(1).setPreferredWidth(0);
            jTable3.getColumnModel().getColumn(2).setResizable(false);
            jTable3.getColumnModel().getColumn(2).setPreferredWidth(0);
            jTable3.getColumnModel().getColumn(3).setResizable(false);
            jTable3.getColumnModel().getColumn(3).setPreferredWidth(0);
        }

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("SALES RETURN");

        jLabel11.setText("Total Quantity: -");

        jTextField7.setEditable(false);
        jTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel12.setText("Total amount: -");

        jTextField8.setEditable(false);
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setText("NET SALES");

        jLabel14.setText("Total Quantity: -");

        jTextField9.setEditable(false);
        jTextField9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel15.setText("Total amount: -");

        jTextField10.setEditable(false);
        jTextField10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });

        jButton3.setText("Print");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel15)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel12)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 46, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(212, 212, 212))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(247, 247, 247))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(225, 225, 225))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addGap(9, 9, 9)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);

        }

    }//GEN-LAST:event_jTable1KeyPressed
    int row = 0;

    public void old() {
        jDialog3.setVisible(true);
        jDialog3.setBounds(750, 30, 235, 398);
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        dtm.getDataVector().removeAllElements();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  Stock_No,total FROM Billing where bill_refrence='" + jTable1.getValueAt(row, 1) + "" + "'");
            while (rs.next()) {
                Object o[] = {rs.getString(1), rs.getString(2)};
                dtm.addRow(o);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        int rowcount = jTable2.getRowCount();
        double amnt = 0;
        double totalamnt = 0;
        for (int i = 0; i < rowcount; i++) {
            amnt = Double.parseDouble("" + jTable2.getValueAt(i, 1));
            totalamnt = amnt + totalamnt;
        }
        jTextField11.setText("" + (int) totalamnt);
    }
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        int count = evt.getClickCount();
        if (count == 2) {
            row = jTable1.getSelectedRow();
            String billno = jTable1.getValueAt(row, 1).toString();
            String date = jTable1.getValueAt(row, 0).toString();
            New_Dual_Billing b = new New_Dual_Billing();
            b.setVisible(true);
            b.jButton1.setVisible(false);
            b.jButton5.setVisible(true);
            b.jTextField5.setEnabled(false);
            b.jTable1.setEnabled(false);
            b.jTextField17.setEditable(false);
            b.jTextField2.setEditable(false);
            b.jRadioButton1.setEnabled(false);
            b.jRadioButton2.setEnabled(false);
            b.jRadioButton5.setEnabled(false);
            DefaultTableModel dtm = (DefaultTableModel) b.jTable1.getModel();
            try {
                String sno = "";
                Connection con = new connection().cone();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("select * from billing where bill_refrence='" + billno + "' ");
                while (rs.next()) {
                    String billingno = rs.getString(1);
                    String dates = rs.getString(2);
                    String type = rs.getString(3);
                    String name = rs.getString(4);
                    String ac = rs.getString(5);
                    sno = rs.getString(6);
                    String stock_no = rs.getString(7);
                    String product_code = rs.getString(8);
                    String rate = rs.getString(9);
                    String qnty = rs.getString(10);
                    String value = rs.getString(11);
                    String disc_code = rs.getString(12);
                    String disc = rs.getString(13);
                    String disc_Amt = rs.getString(14);
                    String total = rs.getString(15);
                    String staff = rs.getString(16);
                    String total_Amt = rs.getString(17);
                    String total_qnty = rs.getString(18);
                    String total_disc_amt = rs.getString(19);
                    String total_amt_after_disc = rs.getString(20);
                    String total_value_bfore_tax = rs.getString(21);
                    String total_value_after_tax = rs.getString(22);
                    String vat_amt = rs.getString(23);
                    String vat_acc = rs.getString(24);
                    String party_code = rs.getString(28);
                    b.jTextField1.setText(billingno);
                    java.util.Date dat = new SimpleDateFormat("dd-MM-yyyy").parse(date);
                    b.jDateChooser1.setDate(dat);
                    b.jTextField4.setText(ac);
                    b.jTextField3.setText(name);

                    Object o[] = {sno, stock_no, product_code, rate, qnty, value, disc_code, disc, disc_Amt, total, staff};
                    dtm.addRow(o);

                    b.jTextField15.setText(total_Amt);
                    b.jTextField16.setText(total_qnty);
                    b.jTextField17.setText(total_disc_amt);
                    b.jTextField18.setText(total_amt_after_disc);
                    b.jTextField19.setText(total_value_bfore_tax);
                    b.jTextField20.setText(total_value_after_tax);

                    if (type.equalsIgnoreCase(b.jRadioButton1.getText())) {
                        b.jRadioButton1.setSelected(true);
                    } else if (type.equalsIgnoreCase(b.jRadioButton2.getText())) {
                        b.jRadioButton2.setSelected(true);
                    } else if (type.equalsIgnoreCase(b.jRadioButton5.getText())) {
                        b.jRadioButton5.setSelected(true);
                    }
                }

                int son1 = Integer.parseInt(sno);
                b.jTextField14.setText("" + (son1 + 1));
            } catch (SQLException ex) {
                Logger.getLogger(Dailyreport_.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(Dailyreport_.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);

        }

    }//GEN-LAST:event_formKeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }

    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable3KeyPressed
    public void old2() {
        row = jTable3.getSelectedRow();
        jDialog3.setVisible(true);
        jDialog3.setBounds(750, 30, 235, 398);
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        dtm.getDataVector().removeAllElements();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  Stock_No,rate FROM salesreturn where bill_refrence='" + jTable3.getValueAt(row, 1) + "" + "'");

            while (rs.next()) {
                Object o[] = {rs.getString(1), rs.getString(2)};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int rowcount = jTable2.getRowCount();
        double amnt = 0;
        double totalamnt = 0;
        for (int i = 0; i < rowcount; i++) {
            amnt = Double.parseDouble("" + jTable2.getValueAt(i, 1));
            totalamnt = amnt + totalamnt;
        }
        jTextField11.setText("" + (int) totalamnt);
    }
    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        int count = evt.getClickCount();
        if (count == 2) {
            row = jTable3.getSelectedRow();
            String billno = jTable3.getValueAt(row, 1).toString();
            String date = jTable3.getValueAt(row, 0).toString();
            New_Dual_SalesReturn b = new New_Dual_SalesReturn();
            b.setVisible(true);
            DefaultTableModel dtm = (DefaultTableModel) b.jTable1.getModel();
            try {
                String sno = "";
                Connection con = new connection().cone();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("select * from salesreturn where bill_refrence='" + billno + "' ");
                while (rs.next()) {

                    String billingno = rs.getString(1);
                    System.out.println("billingno " + billingno);
                    String dates = rs.getString(2);
                    System.out.println("dates " + dates);
                    String type = rs.getString(3);
                    System.out.println("type " + type);
                    String name = rs.getString(4);
                    System.out.println("name " + name);
                    String ac = rs.getString(5);
                    System.out.println("ac " + ac);
                    sno = rs.getString(6);
                    System.out.println("sno " + sno);
                    String stock_no = rs.getString(7);
                    System.out.println("stock_no " + stock_no);
                    String product_code = rs.getString(8);
                    System.out.println("product_code " + product_code);
                    String rate = rs.getString(9);
                    System.out.println("rate " + rate);
                    String qnty = rs.getString(10);
                    System.out.println("qnty " + qnty);
                    String value = rs.getString(11);
                    System.out.println("value " + value);
                    String disc_code = rs.getString(12);
                    System.out.println("disc_code " + disc_code);
                    String disc = rs.getString(13);
                    System.out.println("disc " + disc);
                    String disc_Amt = rs.getString(14);
                    System.out.println("disc_Amt " + disc_Amt);
                    String total = rs.getString(15);
                    System.out.println("total " + total);
                    String staff = rs.getString(16);
                    System.out.println("staff " + staff);
                    String total_Amt = rs.getString(17);
                    System.out.println("total_Amt " + total_Amt);
                    String total_qnty = rs.getString(18);
                    System.out.println("total_qnty " + total_qnty);
                    String total_disc_amt = rs.getString(19);
                    System.out.println("total_disc_amt " + total_disc_amt);
                    String total_amt_after_disc = rs.getString(20);
                    System.out.println("total_amt_after_disc " + total_amt_after_disc);
                    String total_value_bfore_tax = rs.getString(21);
                    System.out.println("total_value_bfore_tax " + total_value_bfore_tax);
                    String total_value_after_tax = rs.getString(22);
                    System.out.println("total_value_after_tax " + total_value_after_tax);
                    String vat_amt = rs.getString(23);
                    System.out.println("vat_amt " + vat_amt);
                    String vat_acc = rs.getString(24);
                    System.out.println("vat_acc " + vat_acc);
                    String party_code = rs.getString(28);

                    b.jTextField1.setText(billingno);

//                b.jTextField2.setText(date);
                    b.jTextField4.setText(ac);
                    b.jTextField3.setText(name);

                    Object o[] = {sno, stock_no, product_code, rate, qnty, value, disc_code, disc, disc_Amt, total, staff};
                    dtm.addRow(o);

                    b.jTextField15.setText(total_Amt);
                    b.jTextField16.setText(total_qnty);
                    b.jTextField17.setText(total_disc_amt);
                    b.jTextField18.setText(total_amt_after_disc);
                    b.jTextField19.setText(total_value_bfore_tax);
                    b.jTextField20.setText(total_value_after_tax);

                    if (type.equalsIgnoreCase(b.jRadioButton1.getText())) {
                        b.jRadioButton1.setSelected(true);
                    } else if (type.equalsIgnoreCase(b.jRadioButton2.getText())) {
                        b.jRadioButton2.setSelected(true);
                    }

                }

                int son1 = Integer.parseInt(sno);
                b.jTextField14.setText("" + (son1 + 1));
            } catch (SQLException ex) {
                Logger.getLogger(Dailyreport_.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }//GEN-LAST:event_jTable3MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        jDialog1.dispose();
        Date Date11 = jDateChooser1.getDate();
        String date1 = formatter.format(Date11);
        String date11 = formatter4.format(Date11);

        jTextField6.setText(date11);
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        DefaultTableModel dtm1 = (DefaultTableModel) jTable3.getModel();
        dtm1.getDataVector().removeAllElements();
        dtm.fireTableStructureChanged();
        dtm1.fireTableStructureChanged();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  * FROM Billing where date='" + date1 + "'");
            String matchsno4 = "test";
            while (rs.next()) {
                String matchsno1 = rs.getString(1);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    dtm = (DefaultTableModel) jTable1.getModel();
                    Object o[] = {formatter4.format(formatter.parse(rs.getString(2))), rs.getString(1), rs.getInt(18), rs.getInt(22)};
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }

            ResultSet rs1 = st.executeQuery("SELECT  * FROM salesreturn where date='" + date1 + "'");
            String matchsno = "test";
            while (rs1.next()) {
                String matchsno1 = rs1.getString(1);
                if (matchsno.equals(matchsno1)) {
                } else {
                    dtm = (DefaultTableModel) jTable3.getModel();
                    Object o[] = {formatter4.format(formatter.parse(rs1.getString(2))), rs1.getString(1), rs1.getInt(18), rs1.getInt(22)};
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }

        } catch (ParseException ex) {
            Logger.getLogger(Dailyreport_.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException sqe) {
            System.out.println("SQl error");
            sqe.printStackTrace();
        }

        double amount1 = 0;
        double quantity1 = 0;
        double totalamount1 = 0;
        double totalquantity1 = 0;

        double amount1return = 0;
        double quantity1return = 0;
        double totalamount1return = 0;
        double totalquantity1return = 0;

        double netamount = 0;
        double netquantity = 0;
        int rowcount = jTable1.getRowCount();
        for (int i = 0; i < rowcount; i++) {
            amount1 = Double.parseDouble(jTable1.getValueAt(i, 3) + "");
            quantity1 = Double.parseDouble(jTable1.getValueAt(i, 2) + "");
            totalamount1 = amount1 + totalamount1;
            totalquantity1 = quantity1 + totalquantity1;
            System.out.println("loop is running");
        }
        if (totalquantity1 == 0 && totalamount1 == 0) {
            jTextField4.setText("" + "");
            jTextField5.setText("" + "");
        } else {
            jTextField4.setText("" + (int) totalquantity1);
            jTextField5.setText("" + (int) totalamount1);
        }
        int rowcount1 = jTable3.getRowCount();
        for (int i = 0; i < rowcount1; i++) {
            amount1return = Double.parseDouble(jTable3.getValueAt(i, 3) + "");
            quantity1return = Double.parseDouble(jTable3.getValueAt(i, 2) + "");
            totalamount1return = amount1return + totalamount1return;
            totalquantity1return = quantity1return + totalquantity1return;
        }

        if (totalquantity1return == 0 && totalamount1return == 0) {
            jTextField7.setText("" + "");
            jTextField8.setText("" + "");
        } else {
            jTextField7.setText("" + (int) totalquantity1return);
            jTextField8.setText("" + (int) totalamount1return);
        }
        netamount = totalamount1 - totalamount1return;
        netquantity = totalquantity1 - totalquantity1return;

        if (netquantity == 0 && netamount == 0) {
            jTextField9.setText("" + "");
            jTextField10.setText("" + "");
        } else {
            jTextField9.setText("" + (int) netquantity);
            jTextField10.setText("" + (int) netamount);
        }


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();
        DefaultTableModel dtm;
        Date Date11 = jDateChooser2.getDate();
        String date1 = formatter.format(Date11);

        Date Date12 = jDateChooser3.getDate();
        String date2 = formatter.format(Date12);

        String date22 = formatter4.format(Date12);

        jTextField6.setText(date22);
        dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        DefaultTableModel dtm1 = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  * FROM Billing where date Between '" + date1 + "' and '" + date2 + "'");
            String matchsno4 = "test";
            while (rs.next()) {
                String matchsno1 = rs.getString(1);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    dtm = (DefaultTableModel) jTable1.getModel();
                    Object o[] = {formatter4.format(formatter.parse(rs.getString(2))), rs.getString(1), rs.getInt(18), rs.getInt(22)};
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }
            }

            ResultSet rs1 = st.executeQuery("SELECT  * FROM salesreturn where date between '" + date1 + "' and '" + date2 + "'");
            String matchsno = "test";
            while (rs1.next()) {
                String matchsno1 = rs1.getString(1);
                if (matchsno.equals(matchsno1)) {
                } else {

                    dtm = (DefaultTableModel) jTable3.getModel();
                    Object o[] = {formatter4.format(formatter.parse(rs1.getString(2))), rs1.getString(1), rs1.getInt(18), rs1.getInt(22)};
                    dtm.addRow(o);
                    matchsno4 = matchsno1;
                }

            }

        } catch (ParseException ex) {
            Logger.getLogger(Dailyreport_.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException sqe) {
            System.out.println("SQl error");
            sqe.printStackTrace();
        }

        double amount1 = 0;
        double quantity1 = 0;
        double totalamount1 = 0;
        double totalquantity1 = 0;

        double amount1return = 0;
        double quantity1return = 0;
        double totalamount1return = 0;
        double totalquantity1return = 0;

        double netamount = 0;
        double netquantity = 0;
        int rowcount = jTable1.getRowCount();
        for (int i = 0; i < rowcount; i++) {
            amount1 = Double.parseDouble(jTable1.getValueAt(i, 3) + "");
            quantity1 = Double.parseDouble(jTable1.getValueAt(i, 2) + "");
            totalamount1 = amount1 + totalamount1;
            totalquantity1 = quantity1 + totalquantity1;
            System.out.println("loop is running");
        }
        if (totalquantity1 == 0 && totalamount1 == 0) {
            jTextField4.setText("" + "");
            jTextField5.setText("" + "");
        } else {
            jTextField4.setText("" + (int) totalquantity1);
            jTextField5.setText("" + (int) totalamount1);
        }

        int rowcount1 = jTable3.getRowCount();
        for (int i = 0; i < rowcount1; i++) {
            amount1return = Double.parseDouble(jTable3.getValueAt(i, 3) + "");
            quantity1return = Double.parseDouble(jTable3.getValueAt(i, 2) + "");
            totalamount1return = amount1return + totalamount1return;
            totalquantity1return = quantity1return + totalquantity1return;
        }
        if (totalquantity1return == 0 && totalamount1return == 0) {
            jTextField7.setText("" + "");
            jTextField8.setText("" + "");
        } else {
            jTextField7.setText("" + (int) totalquantity1return);
            jTextField8.setText("" + (int) totalamount1return);
        }

        netamount = totalamount1 - totalamount1return;
        netquantity = totalquantity1 - totalquantity1return;

        if (netamount == 0 && netquantity == 0) {
            jTextField9.setText("" + "");
            jTextField10.setText("" + "");
        } else {
            jTextField9.setText("" + (int) netquantity);
            jTextField10.setText("" + (int) netamount);
        }


    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        int tb1 = jTable1.getRowCount();
        int tb2 = jTable3.getRowCount();
        if (tb1 > 0 || tb2 > 0) {
            Printing psrp = new Printing();
            PrinterJob job = PrinterJob.getPrinterJob();

            job.setPrintable(psrp);
            job.printDialog();
            try {
                job.print();
            } catch (PrinterException e) {

                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "No Value To print...");
        }


    }//GEN-LAST:event_jButton3ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Dailyreport_.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Dailyreport_.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Dailyreport_.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Dailyreport_.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Dailyreport_().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JLabel jLabel1;
    public static javax.swing.JLabel jLabel10;
    public static javax.swing.JLabel jLabel11;
    public static javax.swing.JLabel jLabel12;
    public static javax.swing.JLabel jLabel13;
    public static javax.swing.JLabel jLabel14;
    public static javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    public static javax.swing.JLabel jLabel5;
    public static javax.swing.JLabel jLabel6;
    public static javax.swing.JLabel jLabel7;
    public static javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    public static javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    public static javax.swing.JTable jTable3;
    public static javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    public static javax.swing.JTextField jTextField4;
    public static javax.swing.JTextField jTextField5;
    public static javax.swing.JTextField jTextField6;
    public static javax.swing.JTextField jTextField7;
    public static javax.swing.JTextField jTextField8;
    public static javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {

        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }
        // get table row
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        int columncount = jTable1.getColumnCount();

        int x1 = 4;
        int x2 = 124;
        int x3 = 298;
        int x4 = 447;
        double width = pageFormat.getImageableWidth();
        System.out.println("width" + width);
        Graphics2D g2d = (Graphics2D) graphics;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        Font font1 = new Font("", Font.BOLD, 12);
        g2d.setFont(font1);
        // header section start 
        g2d.drawString(HeaderAndFooter.getHeader(), 0, 9);

        Font font2 = new Font("", Font.PLAIN, 9);
        g2d.setFont(font2);
        // g2d.drawString(jTextField1.getText().trim(), 410, 10);
        g2d.drawString(HeaderAndFooter.getHeaderAddress1(), 0, 20);
        g2d.drawString(HeaderAndFooter.getHeaderAddress2(), 0, 30);
        g2d.drawString(HeaderAndFooter.getHeaderContact(), 0, 40);
        //header section end 
        Font font3 = new Font("", Font.BOLD, 10);
        g2d.setFont(font3);

        //table lable 
        g2d.drawString(jLabel5.getText().trim(), 177, 60);
        g2d.drawString("As on : " + jTextField6.getText().trim(), 190, 72);
        g2d.drawString(jLabel9.getText().trim(), 0, 90);
        // First table start 

        int x = 4;
        int y = 110;
        int endWidthX = 465;
        int endLine = 0;

        // get column and print
        for (int i = 0; i < columncount; i++) {
            g2d.drawString(jTable1.getColumnName(i), x, y);
            x = x + 135;
            System.out.println(x);
        }
        // draw line before column 
        g2d.drawLine(0, y - 10, endWidthX, y - 10);
        //draw line after column name 
        g2d.drawLine(0, y + 5, endWidthX, y + 5);

        Font font4 = new Font("", Font.PLAIN, 10);
        g2d.setFont(font4);
        FontMetrics fm4 = g2d.getFontMetrics(font4);
        y = y + 15;
        if (rowcount > 0) {
            //table row print 
            for (int i = 0; i < rowcount; i++) {
                int j = 14;
                System.out.println(j);
                g2d.drawString(jTable1.getValueAt(i, 0) + "", x1, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", x2, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 2) + "", x3 - fm4.stringWidth(jTable1.getValueAt(i, 2).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 3) + "", x4 - fm4.stringWidth(jTable1.getValueAt(i, 3).toString()), y + (j * i));
                endLine = (120 + (j * i)) + 2;
            }
        } else {
            endLine = y + 20;
        }

        // draw line vertical left-top to bottom alignment of table
        // g2d.drawLine(0, 75, 0, endLine + 15);
        //draw line vertical right-top to bottom alignment of table
        //  g2d.drawLine(460, 75, 460, endLine + 15);
        // draw line horizontal left-right alignment of table after total  
        g2d.drawLine(0, endLine + 25, endWidthX, endLine + 25);

        // draw line horizontal left-right alignment of table before total 
        g2d.drawLine(0, endLine + 8, endWidthX, endLine + 8);

        g2d.setFont(font3);
        FontMetrics fm3 = g2d.getFontMetrics(font3);
        //total print first table
        g2d.drawString("TOTAL : ", 130, endLine + 20);
        g2d.drawString(jTextField4.getText().trim(), x3 - fm3.stringWidth(jTextField4.getText().trim()), endLine + 20);
        g2d.drawString(jTextField5.getText().trim(), x4 - fm3.stringWidth(jTextField5.getText().trim()), endLine + 20);

        //End of First Table
        //Start Second Table
        // table lable 
        g2d.drawString(jLabel10.getText().trim(), 0, endLine + 50);

        int afterTable1PointY = endLine + 58;

        DefaultTableModel dtm2 = (DefaultTableModel) jTable3.getModel();
        int rowcount1 = jTable3.getRowCount();
        int columncount1 = jTable3.getColumnCount();

        System.out.println("row count " + rowcount1);
        int endLine1 = 0;
        int x11 = 4;
        int pointY = afterTable1PointY + 10 + 20;
        for (int i = 0; i < columncount1; i++) {
            g2d.drawString(jTable1.getColumnName(i), x11, afterTable1PointY + 10);
            x11 = x11 + 135;
            System.out.println(x1);
        }

        //draw after column   
        g2d.drawLine(0, afterTable1PointY + 15, endWidthX, afterTable1PointY + 15);
        //draw before column
        g2d.drawLine(0, afterTable1PointY, endWidthX, afterTable1PointY);
        if (rowcount1 > 0) {
            // print table column

            g2d.setFont(font4);

            // print table row 
            for (int i = 0; i < rowcount1; i++) {
                int j = 14;
                System.out.println(j);
                g2d.drawString(jTable3.getValueAt(i, 0) + "", x1, pointY + (j * i));
                g2d.drawString(jTable3.getValueAt(i, 1) + "", x2, pointY + (j * i));
                g2d.drawString(jTable3.getValueAt(i, 2) + "", x3 - fm4.stringWidth(jTable3.getValueAt(i, 2).toString()), pointY + (j * i));
                g2d.drawString(jTable3.getValueAt(i, 3) + "", x4 - fm4.stringWidth(jTable3.getValueAt(i, 3).toString()), pointY + (j * i));

                endLine1 = (pointY + (j * i)) + 2;
                System.out.println("end line1" + endLine1);
                // g2d.drawLine(40,endLine1,440,endLine1);
            }
        } else {
            endLine1 = pointY;
        }
        //drawing table border
        // g2d.drawLine(0, afterTable1PointY, 0, endLine1 + 15);

        //g2d.drawLine(460, afterTable1PointY,460, endLine1 + 15);
        //draw line after total
        g2d.drawLine(0, endLine1 + 18, endWidthX, endLine1 + 18);
        //draw line before total
        g2d.drawLine(0, endLine1 + 4, endWidthX, endLine1 + 4);

        g2d.setFont(font3);
        //second table total print 
        g2d.drawString("TOTAL : ", 4, endLine1 + 15);
        g2d.drawString(jTextField7.getText().trim(), x3 - fm3.stringWidth(jTextField7.getText().trim()), endLine1 + 15);
        g2d.drawString(jTextField8.getText().trim(), x4 - fm3.stringWidth(jTextField8.getText().trim()), endLine1 + 15);

        int ypoint = endLine1 + 15 + 20;
        //net sale printing 
        g2d.drawString("NET SALE : ", 0, ypoint + 20);
        g2d.drawString(jTextField9.getText().trim(), x3 - fm3.stringWidth(jTextField9.getText().trim()), ypoint + 20);
        g2d.drawString(jTextField10.getText().trim(), x4 - fm3.stringWidth(jTextField10.getText().trim()), ypoint + 20);

        g2d.drawLine(0, ypoint + 7, endWidthX, ypoint + 7);
        g2d.drawLine(0, ypoint + 25, endWidthX, ypoint + 25);
        return PAGE_EXISTS;
    }

    final class Printing implements Printable {

        java.util.List<String> table1, table2, table3, table4;
        int rowCount1, rowCount2;

        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateNow = formatter.format(currentDate.getTime());

        public Printing() {

            rowCount1 = jTable1.getRowCount();
            rowCount2 = jTable3.getRowCount();

            table1 = new ArrayList<String>();
            table2 = new ArrayList<String>();
            table3 = new ArrayList<String>();
            table4 = new ArrayList<String>();
            // addTitle1();
            addTable1();
            addTable1Total();
            addTitle2();
            addColumnTital2();
            addTable2();
            addTable2Total();
        }

        public void addTable1() {
            if (rowCount1 > 0) {
                for (int i = 0; i < rowCount1; i++) {
                    table1.add(jTable1.getValueAt(i, 0).toString());
                }

                for (int i = 0; i < rowCount1; i++) {
                    table2.add(jTable1.getValueAt(i, 1).toString());
                }
                for (int i = 0; i < rowCount1; i++) {
                    table3.add(jTable1.getValueAt(i, 2).toString());
                }
                for (int i = 0; i < rowCount1; i++) {
                    table4.add(jTable1.getValueAt(i, 3).toString());
                }
            }
        }

        public void addTable1Total() {
            table1.add("Total");
            table2.add("");
            table3.add(jTextField4.getText());
            table4.add(jTextField5.getText());
        }

        public void addTitle1() {
            table1.add(jLabel9.getText().trim());
            table2.add("");
            table3.add("");
            table4.add("");
        }

        public void addTitle2() {
            table1.add(jLabel10.getText().trim());
            table2.add("");
            table3.add("");
            table4.add("");
        }

        public void addColumnTital2() {
            table1.add("" + jTable3.getColumnName(0));
            table2.add("" + jTable3.getColumnName(1));
            table3.add("" + jTable3.getColumnName(2));
            table4.add("" + jTable3.getColumnName(3));
        }

        public void addTable2() {
            if (rowCount2 > 0) {
                for (int i = 0; i < rowCount2; i++) {
                    table1.add(jTable3.getValueAt(i, 0).toString());
                }

                for (int i = 0; i < rowCount2; i++) {
                    table2.add(jTable3.getValueAt(i, 1).toString());
                }
                for (int i = 0; i < rowCount2; i++) {
                    table3.add(jTable3.getValueAt(i, 2).toString());
                }
                for (int i = 0; i < rowCount2; i++) {
                    table4.add(jTable3.getValueAt(i, 3).toString());
                }
            }
        }

        public void addTable2Total() {
            table1.add("Total");
            table2.add("");
            table3.add(jTextField7.getText());
            table4.add(jTextField8.getText());
        }

        @Override
        public int print(Graphics g, PageFormat pf, int pageIndex) {
            int linesPerPage = 33;
            if (pageIndex > 0) {
                linesPerPage = 35;
            }
            if (pageIndex * linesPerPage >= table1.size()) {
                return NO_SUCH_PAGE;
            }

            Graphics2D g2d = (Graphics2D) g;
            Font fontHeaderBold = new Font("", Font.BOLD, 12);
            Font fontHeaderPlain = new Font("", Font.BOLD, 10);
            Font fontcolumn = new Font("", Font.BOLD, 9);
            Font fontrow = new Font("", Font.PLAIN, 10);
            int height = (int) pf.getImageableHeight();
            int width = (int) pf.getImageableWidth();

            g2d.drawString(String.valueOf(pageIndex + 1), 300, 765);

            System.out.println("height" + height + "  width" + width);
            int x = 20;
            int y = 30;
            if (pageIndex == 0) {
                g2d.setFont(fontHeaderBold);
                g2d.drawString(HeaderAndFooter.getHeader(), x, y);
                g2d.setFont(fontHeaderPlain);
                g2d.drawString(HeaderAndFooter.getHeaderAddress1(), x, y + 15);
                g2d.drawString(HeaderAndFooter.getHeaderAddress2(), x, y + 25);
                g2d.drawString(HeaderAndFooter.getHeaderContact(), x, y + 35);
                g2d.drawString("Bill Wise Sales Report", 250, y + 47);
                g2d.drawString("As On " + jTextField6.getText(), 255, y + 60);
                g2d.drawString("Sale ", x, y + 70);
            }
            if (pageIndex == 0) {
                y = y + 90;// last header point 
            } else {
                y = 50;
            }

            // get column and print
            int x1 = 20;
            int x2 = 170;
            int x3 = 340;
            int x4 = width - 75;

            // line draw bfore column
            g2d.drawLine(10, y - 10, 600, y - 10);
            // line draw bfore column
            g2d.drawLine(10, y + 5, 600, y + 5);
            g2d.setFont(fontHeaderPlain);
            int columncount1 = jTable1.getColumnCount();
            for (int i = 0; i < columncount1; i++) {
                if (i == 1) {
                    x = x2;
                }
                if (i == 2) {
                    x = x3 - 2;
                }
                if (i == 3) {
                    x = x4 + 4;
                }
                g2d.drawString(jTable1.getColumnName(i), x, y);
            }
            x = 20;
            y = y + 15;
            g2d.setFont(fontrow);
            FontMetrics fm = g2d.getFontMetrics(fontrow);

            for (int i = linesPerPage * pageIndex; i < table1.size()
                    && i < linesPerPage * (pageIndex + 1); i++) {
                g2d.setFont(fontrow);
                //if total is found then draw line before and after total 
                if (table1.get(i).trim().equalsIgnoreCase("Total")) {
                    y += 15;
                    g2d.setFont(fontHeaderPlain);
                    g2d.drawLine(10, y - 10, 600, y - 10);
                    g2d.drawLine(10, y + 5, 600, y + 5);
                }
                //if sales return found then y is increment and font is bold
                if (table1.get(i).trim().equalsIgnoreCase("SALES RETURN")) {
                    y += 15;
                    g2d.setFont(fontHeaderPlain);
                }
                //if table column found then print bold font and line 
                if (table1.get(i).trim().equalsIgnoreCase("Date")) {
                    y += 15;
                    g2d.setFont(fontHeaderPlain);
                    g2d.drawLine(10, y - 10, 600, y - 10);
                    g2d.drawLine(10, y + 5, 600, y + 5);
                }
                g2d.drawString(table1.get(i), x1, y);
                g2d.drawString(table2.get(i), x2, y);
                g2d.drawString(table3.get(i), (x3 + 35) - fm.stringWidth(table3.get(i)), y);
                g2d.drawString(table4.get(i), (x4 + 37) - fm.stringWidth(table4.get(i)), y);
                y += 15;
                System.out.println("i is" + i);
                System.out.println("table size" + table1.size());

                if (i == (table1.size() - 1)) {
                    y += 15;
                    g2d.setFont(fontHeaderPlain);
                    g2d.drawLine(10, y - 10, 600, y - 10);
                    g2d.drawLine(10, y + 5, 600, y + 5);
                    g2d.drawString("NET SALE ", 20, y + 1);
                    g2d.drawString(jTextField9.getText().trim(), (x3 + 35) - fm.stringWidth(jTextField9.getText().trim()), y + 1);
                    g2d.drawString(jTextField10.getText().trim(), (x4 + 37) - fm.stringWidth(jTextField10.getText().trim()), y + 1);

                }
            }
            // g2d.drawString(jTextField2.getText().trim(),250, y);

            return PAGE_EXISTS;
        }
    }

}
