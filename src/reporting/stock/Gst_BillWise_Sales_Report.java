package reporting.stock;

import connection.connection;
import java.awt.Cursor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFrame;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.table.DefaultTableModel;
import java.text.DecimalFormat;
import javax.swing.JFileChooser;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFPrintSetup;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Aashish
 */
public class Gst_BillWise_Sales_Report extends javax.swing.JFrame {

    /**
     * Creates new form Gst_BillWise_Sales_Report
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    String dateNow1 = formatter1.format(currentDate.getTime());
    public boolean dateStatus = true;
    DecimalFormat df = new DecimalFormat("0.00");
    XSSFWorkbook workbook = new XSSFWorkbook();

    private static Gst_BillWise_Sales_Report obj = null;

    public Gst_BillWise_Sales_Report() {
        initComponents();
        jTextField1.setText(dateNow1);
        jTextField1.requestFocus();
        jDialog1.setLocationRelativeTo(null);
        jDialog2.setLocationRelativeTo(null);
        this.setLocationRelativeTo(null);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        getData();
    }

    public static Gst_BillWise_Sales_Report getObj() {
        if (obj == null) {
            obj = new Gst_BillWise_Sales_Report();
        }
        return obj;
    }

//-----------Function For Getting Default Data----------------------------------
    public void getData() {
        this.setCursor(WAIT_CURSOR);

        try {
            double amount5percent = 0;
            double amount12percent = 0;
            double igst_5 = 0;
            double sgst_25 = 0;
            double cgst_25 = 0;
            double igst_12 = 0;
            double sgst_6 = 0;
            double cgst_6 = 0;
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement taxable_st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT DATE_FORMAT(billing.DATE, '%d-%m-%Y') AS DATE, billing.bill_refrence , billing.sundrydebtorsaccount, billing.cashaccount,billing.cardaccount, party_sales.gstin_no, billing.total_qnty, billing.total_value_after_tax FROM billing Left JOIN party_sales ON party_sales.party_name = billing.sundrydebtorsaccount WHERE MONTH(billing.date) = MONTH(CURRENT_DATE()) Group by bill_refrence Order by date asc");
//            ResultSet rs = st.executeQuery("SELECT DATE_FORMAT(billing.DATE, '%d-%m-%Y') AS DATE, billing.bill_refrence , billing.sundrydebtorsaccount, billing.cashaccount,billing.cardaccount, party_sales.gstin_no, billing.total_qnty, billing.total_value_after_tax FROM billing Left JOIN party_sales ON sales_party.party_name = billing.sundrydebtorsaccount WHERE MONTH(billing.date) = MONTH(CURRENT_DATE()) Group by bill_refrence Order by date asc");
            while (rs.next()) {
                String Invoice_number = rs.getString("bill_refrence");
                String Invoice_date = rs.getString("date");
                String sundrydebetors_account = rs.getString("sundrydebtorsaccount");
                String cash_account = rs.getString("cashaccount");
                String card_account = rs.getString("cardaccount");
                double qnty = rs.getDouble("total_qnty");
                double net_amnt = rs.getDouble("total_value_after_tax");
                String GSTno = rs.getString("gstin_no");
                double taxable_5 = 0;
                double taxable_12 = 0;
                String round_off_5 = "";
                String round_off_12 = "";

                ResultSet rs5percentamount = taxable_st.executeQuery("select sum(total) as total, sum(vatamnt) as vatamnt ,sum(Sgstamnt) as Sgstamnt ,sum(Cgstamnt) as Cgstamnt, sum(Round_Off_Value) as Round_Off_Value from billing where by_ledger='Sales 5%' and bill_refrence='" + Invoice_number + "' ");
                while (rs5percentamount.next()) {
                    amount5percent = rs5percentamount.getDouble("total");
                    igst_5 = rs5percentamount.getDouble("vatamnt");
                    sgst_25 = rs5percentamount.getDouble("Sgstamnt");
                    cgst_25 = rs5percentamount.getDouble("Cgstamnt");
                    round_off_5 = rs5percentamount.getString("Round_Off_Value");
                    if (round_off_5 == null) {
                        round_off_5 = "0";
                    } else {
                        taxable_5 = amount5percent - (igst_5 + sgst_25 + cgst_25 + Double.parseDouble(round_off_5));
                    }

                }
                ResultSet rs12percentamount = taxable_st.executeQuery("select sum(total) as total, sum(vatamnt) as vatamnt ,sum(Sgstamnt) as Sgstamnt ,sum(Cgstamnt) as Cgstamnt, sum(Round_Off_Value) as Round_Off_Value from billing where by_ledger='SALES 12%' and bill_refrence='" + Invoice_number + "' ");
                while (rs12percentamount.next()) {
                    amount12percent = rs12percentamount.getDouble("total");
                    igst_12 = rs12percentamount.getDouble("vatamnt");
                    sgst_6 = rs12percentamount.getDouble("Sgstamnt");
                    cgst_6 = rs12percentamount.getDouble("Cgstamnt");
                    round_off_12 = rs12percentamount.getString("Round_Off_Value");
                    if (round_off_12 == null) {
                        round_off_12 = "0";
                    } else {
                        taxable_12 = amount12percent - (igst_12 + sgst_6 + cgst_6 + Double.parseDouble(round_off_12));
                    }
                }

                if (sundrydebetors_account.equals("null")) {
                    sundrydebetors_account = "";
                }
                if (cash_account.equals("null")) {
                    cash_account = "";
                }
                if (card_account.equals("null")) {
                    card_account = "";
                }
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                Object o[] = {Invoice_date, Invoice_number, (sundrydebetors_account + cash_account + card_account), GSTno, df.format(qnty), df.format(taxable_5), df.format(taxable_12), df.format(igst_5), df.format(igst_12), df.format(sgst_25), df.format(sgst_6), df.format(cgst_25), df.format(cgst_6), df.format(net_amnt)};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        total_amnt();
        this.setCursor(Cursor.getDefaultCursor());
    }
//    public void getData() {
//     DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
//        try {
//            double amount5percent = 0;
//            double amount12percent = 0;
//            double igst_5 = 0;
//            double sgst_25 = 0;
//            double cgst_25 = 0;
//            double igst_12 = 0;
//            double sgst_6 = 0;
//            double cgst_6 = 0;
//            connection c = new connection();
//            Connection connect = c.cone();
//            Statement st = connect.createStatement();
//            Statement gst_st = connect.createStatement();
//            Statement st_amnt_5 = connect.createStatement();
//            Statement st_amnt_12 = connect.createStatement();
//            
//            ResultSet rs = st.executeQuery("select * from billing  group by bill_refrence order by date ASC");
//            while (rs.next()) {
//                String Invoice_number = rs.getString("bill_refrence");
//                String Invoice_date = rs.getString("date");
//                String Supplier_name = rs.getString("sundrydebtorsaccount");
//                double qnty = rs.getDouble("total_qnty");
//                double net_amnt = rs.getDouble("total_value_after_tax");
//                String GSTno = "";
//                double taxable_5 = 0;
//                double taxable_12 = 0;
//                String round_off_5 = "";
//                String round_off_12 = "";
//
//                ResultSet rsgstno = gst_st.executeQuery("select tin_no from Party_sales where Party_name='" + Supplier_name + "'");
//                if (rsgstno.next()) {
//                    GSTno = rsgstno.getString("tin_no");
//                }
//
//                ResultSet rs5percentamount = st_amnt_5.executeQuery("select sum(total) as total, sum(vatamnt) as vatamnt ,sum(Sgstamnt) as Sgstamnt ,sum(Cgstamnt) as Cgstamnt, sum(Round_Off_Value) as Round_Off_Value from billing where by_ledger='Sales 5%' and bill_refrence='" + Invoice_number + "' ");
//                while (rs5percentamount.next()) {
//                    amount5percent = rs5percentamount.getDouble("total");
//                    igst_5 = rs5percentamount.getDouble("vatamnt");
//                    sgst_25 = rs5percentamount.getDouble("Sgstamnt");
//                    cgst_25 = rs5percentamount.getDouble("Cgstamnt");
//                    round_off_5 = rs5percentamount.getString("Round_Off_Value");
//                    if (round_off_5 == null) {
//                        round_off_5 = "0";
//                    } else {
//                        taxable_5 = amount5percent - (igst_5 + sgst_25 + cgst_25 + Double.parseDouble(round_off_5));
//                    }
//
//                }
//                ResultSet rs12percentamount = st_amnt_12.executeQuery("select sum(total) as total, sum(vatamnt) as vatamnt ,sum(Sgstamnt) as Sgstamnt ,sum(Cgstamnt) as Cgstamnt, sum(Round_Off_Value) as Round_Off_Value from billing where by_ledger='SALES 12%' and bill_refrence='" + Invoice_number + "' ");
//                while (rs12percentamount.next()) {
//                    amount12percent = rs12percentamount.getDouble("total");
//                    igst_12 = rs12percentamount.getDouble("vatamnt");
//                    sgst_6 = rs12percentamount.getDouble("Sgstamnt");
//                    cgst_6 = rs12percentamount.getDouble("Cgstamnt");
//                    round_off_12 = rs12percentamount.getString("Round_Off_Value");
//                    if (round_off_12 == null) {
//                        round_off_12 = "0";
//                    } else {
//                        taxable_12 = amount12percent - (igst_12 + sgst_6 + cgst_6 + Double.parseDouble(round_off_12));
//                    }
//                }
//
//                if (Supplier_name.equals("null")) {
//                    Supplier_name = "";
//                }
//
//                if (Invoice_date == null) {
//                } else {
////                   System.out.println(""+formatter1.format(formatter.parse(Invoice_date))+""+Invoice_number+""+ Supplier_name+""+GSTno+""+df.format(qnty)+""+df.format(taxable_5)+""+df.format(taxable_12)+""+df.format(igst_5)+""+df.format(igst_12)+""+df.format(sgst_25)+""+df.format(sgst_6)+""+df.format(cgst_25)+""+df.format(cgst_6)+""+df.format(net_amnt));
//                    String Bill_date = formatter1.format(formatter.parse(Invoice_date));                   
//                    Object o[] = {Bill_date, Invoice_number, Supplier_name, GSTno, df.format(qnty), df.format(taxable_5), df.format(taxable_12), df.format(igst_5), df.format(igst_12), df.format(sgst_25), df.format(sgst_6), df.format(cgst_25), df.format(cgst_6), df.format(net_amnt)};
//                    dtm.addRow(o);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        total_amnt();
//    }
//------------------------------------------------------------------------------
//---------------Function for getting Filter Data-------------------------------
    public void getFilterData() {
        this.setCursor(WAIT_CURSOR);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        String date = "";
        String from_date = "";
        String to_date = "";
        if (dateStatus == true) {
            java.util.Date date1 = jDateChooser1.getDate();
            if (jDateChooser1.getDate() != null) {
                date = formatter.format(date1);
            }
        } else if (dateStatus == false) {
            java.util.Date date2 = jDateChooser2.getDate();
            from_date = formatter.format(date2);

            java.util.Date date3 = jDateChooser3.getDate();
            to_date = formatter.format(date3);
        }
        try {
            double amount5percent = 0;
            double amount12percent = 0;
            double igst_5 = 0;
            double sgst_25 = 0;
            double cgst_25 = 0;
            double igst_12 = 0;
            double sgst_6 = 0;
            double cgst_6 = 0;

            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement gst_st = connect.createStatement();
            Statement st_amnt_5 = connect.createStatement();
            Statement st_amnt_12 = connect.createStatement();
            ResultSet bill_rs = null;
            if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                bill_rs = st.executeQuery("select * from billing WHERE MONTH(date) = MONTH(CURRENT_DATE()) group by bill_refrence order by date ASC");
            }
            if (!date.equals("")) {
                bill_rs = st.executeQuery("select * from billing where date = '" + date + "' group by bill_refrence order by date ASC");
            }
            if (!from_date.equals("") && !to_date.equals("")) {
                bill_rs = st.executeQuery("select * from billing where date Between '" + from_date + "' and '" + to_date + "' group by bill_refrence order by date ASC");
            }
            while (bill_rs.next()) {
                String Invoice_number = bill_rs.getString("bill_refrence");
                String Invoice_date = bill_rs.getString("date");
//                String Supplier_name = bill_rs.getString("sundrydebtorsaccount");
                String sundrydebetors_account = bill_rs.getString("sundrydebtorsaccount");
                String cash_account = bill_rs.getString("cashaccount");
                String card_account = bill_rs.getString("cardaccount");
                double qnty = bill_rs.getDouble("total_qnty");
                double net_amnt = bill_rs.getDouble("total_value_after_tax");
                String GSTno = "";
                double taxable_5 = 0;
                double taxable_12 = 0;
                String round_off_5 = "";
                String round_off_12 = "";

                ResultSet rsgstno = gst_st.executeQuery("select gstin_no from party_sales where Party_name='" + sundrydebetors_account + "'");
                if (rsgstno.next()) {
                    GSTno = rsgstno.getString("gstin_no");
                }

                ResultSet rs5percentamount = st_amnt_5.executeQuery("select sum(total) as total, sum(vatamnt) as vatamnt ,sum(Sgstamnt) as Sgstamnt ,sum(Cgstamnt) as Cgstamnt, sum(Round_Off_Value) as Round_Off_Value from billing where by_ledger='Sales 5%' and bill_refrence='" + Invoice_number + "' and date = '" + Invoice_date + "' ");
                while (rs5percentamount.next()) {
                    amount5percent = rs5percentamount.getDouble("total");
                    igst_5 = rs5percentamount.getDouble("vatamnt");
                    sgst_25 = rs5percentamount.getDouble("Sgstamnt");
                    cgst_25 = rs5percentamount.getDouble("Cgstamnt");
                    round_off_5 = rs5percentamount.getString("Round_Off_Value");
                    if (round_off_5 == null) {
                        round_off_5 = "0";
                    } else {
                        taxable_5 = amount5percent - (igst_5 + sgst_25 + cgst_25 + Double.parseDouble(round_off_5));
                    }

                }
                ResultSet rs12percentamount = st_amnt_12.executeQuery("select sum(total) as total, sum(vatamnt) as vatamnt ,sum(Sgstamnt) as Sgstamnt ,sum(Cgstamnt) as Cgstamnt, sum(Round_Off_Value) as Round_Off_Value from billing where by_ledger='SALES 12%' and bill_refrence='" + Invoice_number + "' and date = '" + Invoice_date + "' ");
                while (rs12percentamount.next()) {
                    amount12percent = rs12percentamount.getDouble("total");
                    igst_12 = rs12percentamount.getDouble("vatamnt");
                    sgst_6 = rs12percentamount.getDouble("Sgstamnt");
                    cgst_6 = rs12percentamount.getDouble("Cgstamnt");
                    round_off_12 = rs12percentamount.getString("Round_Off_Value");
                    if (round_off_12 == null) {
                        round_off_12 = "0";
                    } else {
                        taxable_12 = amount12percent - (igst_12 + sgst_6 + cgst_6 + Double.parseDouble(round_off_12));
                    }
                }

                if (jComboBox1.getSelectedItem().equals("ALL")) {
                    if (sundrydebetors_account.equals("null")) {
                        sundrydebetors_account = "";
                    }
                    if (cash_account.equals("null")) {
                        cash_account = "";
                    }
                    if (card_account.equals("null")) {
                        card_account = "";
                    }
                    Object o[] = {formatter1.format(formatter.parse(Invoice_date)), Invoice_number, (sundrydebetors_account + cash_account + card_account), GSTno, df.format(qnty), df.format(taxable_5), df.format(taxable_12), df.format(igst_5), df.format(igst_12), df.format(sgst_25), df.format(sgst_6), df.format(cgst_25), df.format(cgst_6), df.format(net_amnt)};
                    dtm.addRow(o);

                }
                if (jComboBox1.getSelectedItem().equals("Registered")) {
                    if (!sundrydebetors_account.equals("null") && !GSTno.equals("")) {
                        Object o[] = {formatter1.format(formatter.parse(Invoice_date)), Invoice_number, sundrydebetors_account, GSTno, df.format(qnty), df.format(taxable_5), df.format(taxable_12), df.format(igst_5), df.format(igst_12), df.format(sgst_25), df.format(sgst_6), df.format(cgst_25), df.format(cgst_6), df.format(net_amnt)};
                        dtm.addRow(o);
                    }
                }
                if (jComboBox1.getSelectedItem().equals("Un-Registered")) {
                    if (GSTno.equals("")) {
                        if (sundrydebetors_account.equals("null")) {
                            sundrydebetors_account = "";
                        }
                        if (cash_account.equals("null")) {
                            cash_account = "";
                        }
                        if (card_account.equals("null")) {
                            card_account = "";
                        }
                        Object o[] = {formatter1.format(formatter.parse(Invoice_date)), Invoice_number, (sundrydebetors_account + cash_account + card_account), GSTno, df.format(qnty), df.format(taxable_5), df.format(taxable_12), df.format(igst_5), df.format(igst_12), df.format(sgst_25), df.format(sgst_6), df.format(cgst_25), df.format(cgst_6), df.format(net_amnt)};
                        dtm.addRow(o);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        total_amnt();
        this.setCursor(Cursor.getDefaultCursor());
    }
//------------------------------------------------------------------------------    
//---------------Function Getting table total Data------------------------------

    public void total_amnt() {
        int rowcount = jTable1.getRowCount();
        double qty = 0;
        double amnt = 0;
        double taxamnt_5 = 0;
        double taxamnt_12 = 0;
        double igst_5 = 0;
        double igst_12 = 0;
        double sgst_25 = 0;
        double sgst_6 = 0;
        double cgst_25 = 0;
        double cgst_6 = 0;
        double totalqty = 0;
        double totalamnt = 0;
        double totaltaxamnt_5 = 0;
        double totaltaxamnt_12 = 0;
        double total_igst_5 = 0;
        double total_igst_12 = 0;
        double total_sgst_25 = 0;
        double total_sgst_6 = 0;
        double total_cgst_25 = 0;
        double total_cgst_6 = 0;
        for (int i = 0; i < rowcount; i++) {
            qty = Double.parseDouble(jTable1.getValueAt(i, 4) + "");
            totalqty = qty + totalqty;

            taxamnt_5 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
            totaltaxamnt_5 = taxamnt_5 + totaltaxamnt_5;

            taxamnt_12 = Double.parseDouble(jTable1.getValueAt(i, 6) + "");
            totaltaxamnt_12 = taxamnt_12 + totaltaxamnt_12;

            igst_5 = Double.parseDouble(jTable1.getValueAt(i, 7) + "");
            total_igst_5 = igst_5 + total_igst_5;

            igst_12 = Double.parseDouble(jTable1.getValueAt(i, 8) + "");
            total_igst_12 = igst_12 + total_igst_12;

            sgst_25 = Double.parseDouble(jTable1.getValueAt(i, 9) + "");
            total_sgst_25 = sgst_25 + total_sgst_25;

            sgst_6 = Double.parseDouble(jTable1.getValueAt(i, 10) + "");
            total_sgst_6 = sgst_6 + total_sgst_6;

            cgst_25 = Double.parseDouble(jTable1.getValueAt(i, 11) + "");
            total_cgst_25 = cgst_25 + total_cgst_25;

            cgst_6 = Double.parseDouble(jTable1.getValueAt(i, 12) + "");
            total_cgst_6 = cgst_6 + total_cgst_6;

            amnt = Double.parseDouble(jTable1.getValueAt(i, 13) + "");
            totalamnt = amnt + totalamnt;
        }

        jTextField2.setText("" + df.format(totalqty));
        jTextField3.setText("" + df.format(totaltaxamnt_5));
        jTextField4.setText("" + df.format(totaltaxamnt_12));
        jTextField5.setText("" + df.format(total_igst_5));
        jTextField6.setText("" + df.format(total_igst_12));
        jTextField7.setText("" + df.format(total_sgst_25));
        jTextField8.setText("" + df.format(total_sgst_6));
        jTextField9.setText("" + df.format(total_cgst_25));
        jTextField10.setText("" + df.format(total_cgst_6));
        jTextField11.setText("" + df.format(totalamnt));
    }
//------------------------------------------------------------------------------  
//---------Function to write export data into excel------------------------------------------------------------
    public void toExcel() throws FileNotFoundException, IOException {
        String Company_Name = "";
        String Company_No = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("Select * from cmp_detail");
            while(rs.next()){
            Company_Name = rs.getString("cmp_name");
            Company_No = rs.getString("phone_no");        
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        TableModel model = jTable1.getModel();
// Creating Sheet        
        XSSFSheet spreadsheet = workbook.createSheet();
// Creating Rows
        XSSFRow row = spreadsheet.createRow((short) 0);
        XSSFRow row1 = spreadsheet.createRow((short) 1);
        XSSFRow row2;
// Creating Cell        
        XSSFCell cell1 = (XSSFCell) row.createCell((short) 0);
// Creating Font        
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Verdana");
        font.setItalic(false);
        font.setBold(true);
        font.setColor(HSSFColor.AUTOMATIC.index);
// Creating Style        
        XSSFCellStyle style = workbook.createCellStyle();
        XSSFCellStyle style1 = workbook.createCellStyle();
        style1.setAlignment(HorizontalAlignment.RIGHT);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        style.setFont(font);
        style.setWrapText(true);
        row.setHeight((short) 700);
        cell1.setCellValue(Company_Name+" - "+Company_No + "\n" + "Party -" + jComboBox1.getSelectedItem());
//        cell1.setCellValue("Khabiya Cloth Store - 07412-492939" + "\n" + "Party -" + jComboBox1.getSelectedItem());
        cell1.setCellStyle(style);
// Marging the rows        
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 13));
// Setting Row header        
        for (int i = 0; i < model.getColumnCount(); i++) {
            XSSFCell cell = (XSSFCell) row1.createCell((short) i);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
// Getting Value for Each Column        
        for (int i = 0; i < model.getRowCount(); i++) {
// Creating row after header and row header            
            row = spreadsheet.createRow(i + 2);
            for (int j = 0; j < model.getColumnCount(); j++) {
// Creating Cell for puting values                
                XSSFCell cell2 = (XSSFCell) row.createCell((short) j);
                cell2.setCellValue((model.getValueAt(i, j)+""));
// Condition to check numeric value then right align                 
                if (j >= 3) {
                    cell2.setCellStyle(style1);
                }
// Condition to put table total after all rows                
                if (i == (model.getRowCount() - 1)) {
                    row2 = spreadsheet.createRow(i + 4);
                    XSSFCell c1 = (XSSFCell) row2.createCell((short) 2);
                    c1.setCellValue("Total");
                    c1.setCellStyle(style);
//                    spreadsheet.addMergedRegion(new CellRangeAddress(i + 4, i + 4, 0, 0));
                    c1 = (XSSFCell) row2.createCell((short) 4);
                    c1.setCellValue(jTextField2.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 5);
                    c1.setCellValue(jTextField3.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 6);
                    c1.setCellValue(jTextField4.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 7);
                    c1.setCellValue(jTextField5.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 8);
                    c1.setCellValue(jTextField6.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 9);
                    c1.setCellValue(jTextField7.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 10);
                    c1.setCellValue(jTextField8.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 11);
                    c1.setCellValue(jTextField9.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 12);
                    c1.setCellValue(jTextField10.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 13);
                    c1.setCellValue(jTextField11.getText());
                    c1.setCellStyle(style1);
                }
// Setting excel sheet area for printing                
                workbook.setPrintArea(0, 0, j, 0, i);
            }
        }
// Resizing cloumn
        for (int i = 0; i < model.getColumnCount(); i++) {
            spreadsheet.autoSizeColumn(i);
        }
// Setup for printing
        XSSFPrintSetup ps = (XSSFPrintSetup) spreadsheet.getPrintSetup();
//        ps.setLandscape(true);
        spreadsheet.setAutobreaks(true);
        spreadsheet.setFitToPage(true);
        ps.setFitWidth((short) 1);
        ps.setFitHeight((short) 0);
    }
//-------------------------------------------------------------------------------------------------------------     

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel15 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();

        jDialog1.setTitle("Select Date");
        jDialog1.setSize(new java.awt.Dimension(334, 145));

        jLabel15.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 51, 255));
        jLabel15.setText("Date - ");

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jButton1.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(57, 57, 58));
        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jLabel15)
                        .addGap(31, 31, 31)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(105, 105, 105)
                        .addComponent(jButton1)))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addGap(34, 34, 34)
                .addComponent(jButton1)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        jDialog2.setTitle("Select Date");
        jDialog2.setSize(new java.awt.Dimension(392, 190));

        jLabel16.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(0, 51, 255));
        jLabel16.setText("Date From -");

        jLabel17.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(0, 51, 255));
        jLabel17.setText("Date To - ");

        jDateChooser3.setDateFormatString("dd-MM-yyy");

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jButton2.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(57, 57, 58));
        jButton2.setText("Submit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(jButton2))
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel16))
                        .addGap(33, 33, 33)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(9, 9, 9))
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel17))
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addContainerGap(53, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("GST Sales Report");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(57, 57, 58));
        jLabel1.setText("GST Sales Report");

        jLabel2.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(57, 57, 58));
        jLabel2.setText("Party - ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ALL", "Registered", "Un-Registered" }));
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(57, 57, 58));
        jLabel3.setText("Date - ");

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill Number", "Party Name", "GST Number", "Qnty", "Taxable 5%", "Taxable 12%", "IGST 5%", "IGST 12%", "SGST 2.5%", "SGST 6%", "CGST 2.5%", "CGST 6%", "Net Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true, false, true, true, true, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(80);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(90);
            jTable1.getColumnModel().getColumn(1).setMinWidth(100);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(35);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(150);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(170);
            jTable1.getColumnModel().getColumn(3).setMinWidth(120);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(150);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(45);
        }

        jLabel4.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(57, 57, 58));
        jLabel4.setText("Total -");

        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField3.setEditable(false);
        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField4.setEditable(false);
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField5.setEditable(false);
        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField6.setEditable(false);
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField7.setEditable(false);
        jTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField8.setEditable(false);
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField9.setEditable(false);
        jTextField9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField10.setEditable(false);
        jTextField10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField11.setEditable(false);
        jTextField11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel5.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(57, 57, 58));
        jLabel5.setText("Qnty.");

        jLabel6.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(57, 57, 58));
        jLabel6.setText("Taxable 5%");

        jLabel7.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(57, 57, 58));
        jLabel7.setText("Taxable 12%");

        jLabel8.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(57, 57, 58));
        jLabel8.setText("IGST 5%");

        jLabel9.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(57, 57, 58));
        jLabel9.setText("IGST 12%");

        jLabel10.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(57, 57, 58));
        jLabel10.setText("SGST 2.5%");

        jLabel11.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(57, 57, 58));
        jLabel11.setText("SGST 6%");

        jLabel12.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(57, 57, 58));
        jLabel12.setText("CGST 2.5%");

        jLabel13.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(57, 57, 58));
        jLabel13.setText("CGST 6%");

        jLabel14.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(57, 57, 58));
        jLabel14.setText("Net Amnt");

        jButton3.setForeground(new java.awt.Color(57, 57, 58));
        jButton3.setText("Export");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(357, 357, 357)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(jLabel5)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel6))
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel7)
                                .addGap(44, 44, 44)
                                .addComponent(jLabel8)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(jLabel9)
                                .addGap(37, 37, 37)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11)
                                .addGap(30, 30, 30)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel12)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel13)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addGap(30, 30, 30)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jButton3))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        getFilterData();
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            dateStatus = true;
            jDialog1.setVisible(true);
        }
        if (key == evt.VK_F2) {
            dateStatus = false;
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        getFilterData();
        jDialog1.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        getFilterData();
        jDialog2.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened

    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JFileChooser fc = new JFileChooser();
        int option = fc.showSaveDialog(Gst_BillWise_Sales_Report.this);
        if (option == JFileChooser.APPROVE_OPTION) {
            String filename = fc.getSelectedFile().getName();
            String path = fc.getSelectedFile().getParentFile().getPath();

            int len = filename.length();
            String ext = "";
            String file = "";

            if (len > 4) {
                ext = filename.substring(len - 4, len);
            }

            if (ext.equals(".xlsx")) {
                file = path + "\\" + filename;
            } else {
                file = path + "\\" + filename + ".xlsx";
            }
            try {
                toExcel();
                FileOutputStream out = new FileOutputStream(new File(file));
                //write operation workbook using file out object 
                workbook.write(out);

//                Desktop desktop = Desktop.getDesktop();
//                try {
//                    desktop.print(new File(file));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                System.out.println("createworkbook.xlsx written successfully");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gst_BillWise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gst_BillWise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gst_BillWise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gst_BillWise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Gst_BillWise_Sales_Report().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jComboBox1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
