package reporting.stock;

import Printing.HeaderAndFooter;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PRATEEK
 */
public class ProductWiseDailySalesReport extends javax.swing.JFrame implements Printable {

    /**
     * Creates new form PartyWiseDailyPurchaseReport
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());

    SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyy");
    String dateNow2 = formatter2.format(currentDate.getTime());
    String Productname;
    String Productcode;
    String Cityname;
    double quantity = 0;
    double amount = 0;

    double citywisetotalquantity = 0;
    double citywisetotalamount = 0;

    double Totalquantity = 0;
    double Totalamount = 0;
    boolean isdate = false;

    private static ProductWiseDailySalesReport obj = null;

    public ProductWiseDailySalesReport() {
        initComponents();
        this.setLocationRelativeTo(null);
        jTextField1.setText(dateNow2);

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();

            ResultSet rs = st.executeQuery("Select Product_name,Product_Code from Product  order by Product_name ASC");
            while (rs.next()) {
                Productname = rs.getString("Product_name");
                Productcode = rs.getString("Product_code");

                ResultSet rs1 = st1.executeQuery("select sum(qnty),sum(rate * qnty) from billing where description='" + Productcode + "' and date='" + dateNow + "'  ");

                while (rs1.next()) {
                    quantity = rs1.getDouble(1);
                    amount = rs1.getDouble(2);
                }

                Totalamount = amount + Totalamount;
                Totalquantity = quantity + Totalquantity;
                if (quantity != 0 && amount != 0) {
                    Object o[] = {Productcode, quantity, amount};

                    dtm.addRow(o);

                }

            }

            jTextField2.setText(Totalquantity + "");
            jTextField3.setText(Totalamount + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //salesreturn
        try {

            Totalquantity = 0;
            Totalamount = 0;
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            DefaultTableModel dtm = (DefaultTableModel) jTable3.getModel();

            ResultSet rs = st.executeQuery("Select Party_name,Party_Code from Party where city ='" + Cityname + "' order by Party_name ASC");
            while (rs.next()) {
                Productname = rs.getString("Product_name");
                Productcode = rs.getString("Product_code");

                ResultSet rs1 = st1.executeQuery("select sum(qnty),sum(rate* qnty) from salesreturn where Description='" + Productcode + "' and date='" + dateNow + "'  ");

                while (rs1.next()) {
                    quantity = rs1.getDouble(1);
                    amount = rs1.getDouble(2);
                }

                Totalquantity = quantity + Totalquantity;
                Totalamount = amount + Totalamount;
                if (quantity != 0 && amount != 0) {
                    Object o[] = {Productcode, quantity, amount};
                    dtm.addRow(o);

                }

            }

            jTextField4.setText("" + Totalquantity);
            jTextField5.setText("" + Totalamount);

        } catch (Exception e) {
            e.printStackTrace();
        }

        double netquantity = Double.parseDouble(jTextField2.getText()) - Double.parseDouble(jTextField4.getText());
        double netamount = Double.parseDouble(jTextField3.getText()) - Double.parseDouble(jTextField5.getText());

        jTextField6.setText("" + netquantity);
        jTextField8.setText("" + netamount);

    }

    public static ProductWiseDailySalesReport getObj() {
        if (obj == null) {
            obj = new ProductWiseDailySalesReport();
        }
        return obj;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jDialog2 = new javax.swing.JDialog();
        jLabel3 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jTextField7 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();

        jDialog1.setMinimumSize(new java.awt.Dimension(250, 350));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Stock No", "R Rate"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(1).setResizable(false);
        }

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(250, 100));

        jLabel3.setText("Enter Date : ");

        jDateChooser1.setDateFormatString("yyy-MM-dd");

        jButton2.setText("GO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );

        jTextField7.setEditable(false);
        jTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Product Wise Daily Sales Report");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Product Wise Sales Report");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField1MouseClicked(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Quantity", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(0);
        }

        jLabel2.setText("Total: -");
        jLabel2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel2KeyPressed(evt);
            }
        });

        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });

        jTextField3.setEditable(false);
        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jButton1.setText("PRINT");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton3.setText("CHANGE DATE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("SALES RETURN");

        jTable3.setAutoCreateRowSorter(true);
        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Quantity", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(jTable3);
        if (jTable3.getColumnModel().getColumnCount() > 0) {
            jTable3.getColumnModel().getColumn(0).setResizable(false);
            jTable3.getColumnModel().getColumn(0).setPreferredWidth(100);
            jTable3.getColumnModel().getColumn(1).setResizable(false);
            jTable3.getColumnModel().getColumn(1).setPreferredWidth(0);
            jTable3.getColumnModel().getColumn(2).setResizable(false);
            jTable3.getColumnModel().getColumn(2).setPreferredWidth(0);
        }

        jLabel5.setText("Total: -");

        jTextField4.setEditable(false);
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jTextField5.setEditable(false);
        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("NET SALES: -");

        jTextField6.setEditable(false);
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jTextField8.setEditable(false);
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(101, 101, 101)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(105, 105, 105))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jButton3)
                                            .addGap(57, 57, 57))
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel5)
                                                    .addGap(125, 125, 125))
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                    .addGap(1, 1, 1)
                                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addGap(17, 17, 17)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton3))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:

        if (isdate == true) {
            dateNow = date1;
        } else {

        }
        jDialog1.setVisible(true);
        jDialog1.setBounds(200, 100, 150, 135);
        int row = jTable1.getSelectedRow();
        DefaultTableModel dtm1 = (DefaultTableModel) jTable2.getModel();
        dtm1.getDataVector().removeAllElements();
        dtm1.fireTableDataChanged();

        String PartyCode = "";

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();

            PartyCode = jTable1.getValueAt(row, 0) + "";

            ResultSet rs = st.executeQuery("select stock_no,rate from billing where Description='" + PartyCode + "' and date='" + dateNow + "'  ");
            while (rs.next()) {

                if (rs.getString(1) != null) {
                    Object o[] = {rs.getString(1), rs.getString(2)};
                    dtm1.addRow(o);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_jTable1MouseClicked
    String date1 = "";
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:

        jDialog2.dispose();
        date1 = formatter.format(jDateChooser1.getDate());
        String date11 = formatter2.format(jDateChooser1.getDate());
        jTextField1.setText(date11);

        isdate = true;
        quantity = 0;
        amount = 0;
        citywisetotalquantity = 0;
        citywisetotalamount = 0;

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();

            ResultSet rs = st.executeQuery("Select Product_name,Product_code from product order by Product_name ASC");
            while (rs.next()) {
                Productname = rs.getString("Product_name");
                Productcode = rs.getString("Product_code");

                ResultSet rs1 = st1.executeQuery("select sum(qnty),sum(rate * qnty) from billing where Description='" + Productcode + "' and date='" + date1 + "'  ");

                while (rs1.next()) {
                    quantity = rs1.getDouble(1);
                    amount = rs1.getDouble(2);
                }

                Totalquantity = quantity + Totalquantity;
                Totalamount = amount + Totalamount;
                if (quantity != 0 && amount != 0) {
                    Object o[] = {Productcode, quantity, amount};
                    dtm.addRow(o);
                }
            }

            jTextField2.setText(Totalquantity + "");
            jTextField3.setText(Totalamount + "");

        } catch (Exception e) {
            e.printStackTrace();
        }

        //salesreturn
        Totalamount = 0;
        Totalquantity = 0;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            DefaultTableModel dtm = (DefaultTableModel) jTable3.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();

            ResultSet rs = st.executeQuery("Select Party_name,Party_Code from Party where city ='" + Cityname + "' order by Party_name ASC");
            while (rs.next()) {
                Productname = rs.getString("Product_name");
                Productcode = rs.getString("Product_code");

                ResultSet rs1 = st1.executeQuery("select som(qnty),sum(rate * qnty) from salesreturn where Party_code='" + Productcode + "' and date='" + date1 + "'  ");

                while (rs1.next()) {
                    quantity = rs1.getDouble(1);
                    amount = rs1.getDouble(2);
                }
                if (quantity != 0 && amount != 0) {
                    Object o[] = {Productcode, quantity, amount};

                    dtm.addRow(o);

                }

                Totalquantity = quantity + Totalquantity;
                Totalamount = amount + Totalamount;

            }

            jTextField4.setText(Totalquantity + "");
            jTextField5.setText(Totalamount + "");

        } catch (Exception e) {
            e.printStackTrace();
        }

        double netquantity = Double.parseDouble(jTextField2.getText()) - Double.parseDouble(jTextField4.getText());
        double netamount = Double.parseDouble(jTextField3.getText()) - Double.parseDouble(jTextField5.getText());

        jTextField6.setText("" + netquantity);
        jTextField8.setText("" + netamount);

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        jDialog2.setVisible(true);

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTextField1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField1MouseClicked
        // TODO add your handling code here:
        jDialog2.setVisible(true);
    }//GEN-LAST:event_jTextField1MouseClicked

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }

    }//GEN-LAST:event_formKeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jLabel1KeyPressed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jLabel2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel2KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jLabel2KeyPressed

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField2KeyPressed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jButton3KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:

        if (isdate == true) {
            dateNow = date1;
        } else {

        }
        jDialog1.setVisible(true);
        jDialog1.setBounds(200, 100, 150, 135);
        int row = jTable3.getSelectedRow();
        DefaultTableModel dtm1 = (DefaultTableModel) jTable2.getModel();
        dtm1.getDataVector().removeAllElements();
        dtm1.fireTableDataChanged();

        String PartyCode = jTable3.getValueAt(row, 0) + "";

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();

            ResultSet rs = st.executeQuery("select stock_no,rate from salesreturn where Description='" + PartyCode + "' and date='" + dateNow + "'  ");
            while (rs.next()) {

                if (rs.getString(1) != null) {
                    Object o[] = {rs.getString(1), rs.getString(2)};
                    dtm1.addRow(o);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_jTable3MouseClicked

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }


    }//GEN-LAST:event_jTable3KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        int tb1 = jTable1.getRowCount();
        int tb2 = jTable3.getRowCount();
        if (tb1 > 0 || tb2 > 0) {
            PrinterJob job = PrinterJob.getPrinterJob();
            Paper paper = new Paper();
            PageFormat pf = job.defaultPage();
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            boolean b = job.printDialog();
            try {
                if (b == true) {
                    job.print();
                }
            } catch (PrinterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "No Value To print...");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProductWiseDailySalesReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProductWiseDailySalesReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProductWiseDailySalesReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProductWiseDailySalesReport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ProductWiseDailySalesReport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    // End of variables declaration//GEN-END:variables

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }
        // get table row
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        int columncount = jTable1.getColumnCount();

        double width = pageFormat.getImageableWidth();
        System.out.println("width" + width);
        Graphics2D g2d = (Graphics2D) graphics;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        Font font1 = new Font("", Font.BOLD, 12);
        g2d.setFont(font1);
        // header section start 
        g2d.drawString(HeaderAndFooter.getHeader(), 0, 9);

        Font font2 = new Font("", Font.PLAIN, 9);
        g2d.setFont(font2);
        // g2d.drawString(jTextField1.getText().trim(), 410, 10);
        g2d.drawString(HeaderAndFooter.getHeaderAddress1(), 0, 20);
        g2d.drawString(HeaderAndFooter.getHeaderAddress2(), 0, 30);
        g2d.drawString(HeaderAndFooter.getHeaderContact(), 0, 40);
        //header section end 
        Font font3 = new Font("", Font.BOLD, 10);
        g2d.setFont(font3);
        //table lable 
        g2d.drawString(jLabel1.getText().trim(), 180, 60);
        g2d.drawString("As on : " + jTextField1.getText().trim(), 190, 72);
        g2d.drawString("SALES ", 0, 90);
        // First table start 

        int x = 4;
        int y = 110;
        int endWidthX = 465;
        int endLine = 0;

        // get column and print
        for (int i = 0; i < columncount; i++) {
            g2d.drawString(jTable1.getColumnName(i), x, y);
            x = x + 185;
            System.out.println(x);
        }
        // draw line before column 
        g2d.drawLine(0, y - 10, endWidthX, y - 10);
        //draw line after column name 
        g2d.drawLine(0, y + 5, endWidthX, y + 5);

        Font font4 = new Font("", Font.PLAIN, 10);
        g2d.setFont(font4);
        FontMetrics fm4 = g2d.getFontMetrics(font4);
        y = y + 15;
        if (rowcount > 0) {
            //table row print 
            for (int i = 0; i < rowcount; i++) {
                int j = 17;
                System.out.println(j);
                g2d.drawString(jTable1.getValueAt(i, 0) + "", 4, y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", 210 - fm4.stringWidth(jTable1.getValueAt(i, 1).toString()), y + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 2) + "", 410 - fm4.stringWidth(jTable1.getValueAt(i, 2).toString()), y + (j * i));
                endLine = (120 + (j * i)) + 2;
            }
        } else {
            endLine = y + 20;
        }

        // draw line vertical left-top to bottom alignment of table
        // g2d.drawLine(0, 75, 0, endLine + 15);
        //draw line vertical right-top to bottom alignment of table
        //  g2d.drawLine(460, 75, 460, endLine + 15);
        // draw line horizontal left-right alignment of table after total  
        g2d.drawLine(0, endLine + 25, endWidthX, endLine + 25);

        // draw line horizontal left-right alignment of table before total 
        g2d.drawLine(0, endLine + 8, endWidthX, endLine + 8);

        g2d.setFont(font3);
        FontMetrics fm3 = g2d.getFontMetrics(font3);
        //total print first table
        g2d.drawString("TOTAL : ", 4, endLine + 20);
        g2d.drawString(jTextField2.getText().trim(), 210 - fm3.stringWidth(jTextField2.getText().trim()), endLine + 20);
        g2d.drawString(jTextField3.getText().trim(), 410 - fm3.stringWidth(jTextField3.getText().trim()), endLine + 20);

        //End of First Table
        //Start Second Table
        // table lable 
        g2d.drawString(jLabel4.getText().trim(), 0, endLine + 50);

        int afterTable1PointY = endLine + 58;

        DefaultTableModel dtm2 = (DefaultTableModel) jTable3.getModel();
        int rowcount1 = jTable3.getRowCount();
        int columncount1 = jTable3.getColumnCount();

        System.out.println("row count " + rowcount1);
        int endLine1 = 0;
        int x1 = 4;
        int pointY = afterTable1PointY + 10 + 20;
        for (int i = 0; i < columncount1; i++) {
            g2d.drawString(jTable1.getColumnName(i), x1, afterTable1PointY + 10);
            x1 = x1 + 185;
            System.out.println(x1);
        }

        //draw after column   
        g2d.drawLine(0, afterTable1PointY + 15, endWidthX, afterTable1PointY + 15);
        //draw before column
        g2d.drawLine(0, afterTable1PointY, endWidthX, afterTable1PointY);
        if (rowcount1 > 0) {
            // print table column

            g2d.setFont(font4);

            // print table row 
            for (int i = 0; i < rowcount1; i++) {
                int j = 17;
                System.out.println(j);
                g2d.drawString(jTable3.getValueAt(i, 0) + "", 4, pointY + (j * i));
                g2d.drawString(jTable3.getValueAt(i, 1) + "", 210 - fm4.stringWidth(jTable3.getValueAt(i, 1).toString()), pointY + (j * i));
                g2d.drawString(jTable3.getValueAt(i, 2) + "", 410 - fm4.stringWidth(jTable3.getValueAt(i, 2).toString()), pointY + (j * i));

                endLine1 = (pointY + (j * i)) + 2;
                System.out.println("end line1" + endLine1);
                // g2d.drawLine(40,endLine1,440,endLine1);
            }
        } else {
            endLine1 = pointY;
        }
        //drawing table border
        // g2d.drawLine(0, afterTable1PointY, 0, endLine1 + 15);

        //g2d.drawLine(460, afterTable1PointY,460, endLine1 + 15);
        //draw line after total
        g2d.drawLine(0, endLine1 + 18, endWidthX, endLine1 + 18);
        //draw line before total
        g2d.drawLine(0, endLine1 + 4, endWidthX, endLine1 + 4);

        g2d.setFont(font3);
        //second table total print 
        g2d.drawString("TOTAL : ", 4, endLine1 + 15);
        g2d.drawString(jTextField4.getText().trim(), 210 - fm3.stringWidth(jTextField4.getText().trim()), endLine1 + 15);
        g2d.drawString(jTextField5.getText().trim(), 410 - fm3.stringWidth(jTextField5.getText().trim()), endLine1 + 15);

        int ypoint = endLine1 + 15 + 20;
        //net sale printing 
        g2d.drawString("NET SALE : ", 0, ypoint + 20);
        g2d.drawString(jTextField6.getText().trim(), 210 - fm3.stringWidth(jTextField6.getText().trim()), ypoint + 20);
        g2d.drawString(jTextField8.getText().trim(), 410 - fm3.stringWidth(jTextField8.getText().trim()), ypoint + 20);

        g2d.drawLine(0, ypoint + 7, endWidthX, ypoint + 7);
        g2d.drawLine(0, ypoint + 25, endWidthX, ypoint + 25);
        return PAGE_EXISTS;
    }
}
