package reporting.stock;

import connection.connection;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Aashish
 */
public class Staffwise_Sales_Report extends javax.swing.JFrame {

    /**
     * Creates new form Satffwise_Sales_Report
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    String dateNow1 = formatter1.format(currentDate.getTime());
    DecimalFormat df = new DecimalFormat("0.00");
    public boolean dateStatus = true;
    public boolean comboStatus = false;

    private static Staffwise_Sales_Report obj = null;

    public Staffwise_Sales_Report() {
        initComponents();
        this.setLocationRelativeTo(null);
        jDialog1.setLocationRelativeTo(null);
        jDialog2.setLocationRelativeTo(null);
        jTextField1.setText(dateNow1 + "");
        fll_combo();
        getData();
    }

    public static Staffwise_Sales_Report getObj() {
        if (obj == null) {
            obj = new Staffwise_Sales_Report();
        }
        return obj;
    }

// Getting staff data in combobox
    public void fll_combo() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement combo_st = connect.createStatement();
            ResultSet combo_rs = combo_st.executeQuery("Select Emp_Code from staff");
            while (combo_rs.next()) {
                jComboBox1.addItem(combo_rs.getString("Emp_Code"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//---------------------------Getting Default Data------------------------------------------------------------- 
    public void getData() {
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        DefaultTableModel dtm1 = (DefaultTableModel) jTable2.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        dtm1.getDataVector().removeAllElements();
        dtm1.fireTableDataChanged();
        String Emp_code = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement sales_st = connect.createStatement();
            Statement sales_st1 = connect.createStatement();
            Statement sr_st = connect.createStatement();
            Statement sr_st1 = connect.createStatement();
// For Billing            
            ResultSet sales_rs = sales_st.executeQuery("Select Name, Emp_Code from staff");
            while (sales_rs.next()) {
                String Emp_Name = sales_rs.getString("Name");
                Emp_code = sales_rs.getString("Emp_Code");
                ResultSet sales_rs1 = sales_st1.executeQuery("select sum(qnty) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from billing where staff = '" + Emp_code + "'");
                while (sales_rs1.next()) {
                    int qnty = sales_rs1.getInt("stock_no");
                    double rate = sales_rs1.getDouble("rate");
                    double disc_amnt = sales_rs1.getDouble("disc_amnt");

                    if (qnty != 0 && rate != 0) {
                        Object sales[] = {Emp_Name, qnty, disc_amnt, rate};
                        dtm.addRow(sales);
                    }
                }
            }
// For Sales Return
            ResultSet sr_rs = sr_st.executeQuery("Select Name, Emp_Code from staff");
            while (sr_rs.next()) {
                String Emp_Name = sr_rs.getString("Name");
                Emp_code = sr_rs.getString("Emp_Code");
                ResultSet sr_rs1 = sr_st1.executeQuery("select sum(qnty) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from salesreturn where staff = '" + Emp_code + "'");
                while (sr_rs1.next()) {
                    int qnty = sr_rs1.getInt("stock_no");
                    double rate = sr_rs1.getDouble("rate");
                    double disc_amnt = sr_rs1.getDouble("disc_amnt");

                    if (qnty != 0 && rate != 0) {
                        Object sr[] = {Emp_Name, qnty, disc_amnt, rate};
                        dtm1.addRow(sr);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        total();
    }
//---------------------------------------------------------------------------------------------------------------
//----------------jCombobox filter-------------------------------------------------------------------------------

    public void getFilterData() {
        String date = "";
        String from_date = "";
        String to_date = "";
        if (dateStatus == true) {
            java.util.Date date1 = jDateChooser1.getDate();
            if (jDateChooser1.getDate() != null) {
                date = formatter.format(date1);
            }
        } else if (dateStatus == false) {
            java.util.Date date2 = jDateChooser2.getDate();
            from_date = formatter.format(date2);

            java.util.Date date3 = jDateChooser3.getDate();
            to_date = formatter.format(date3);
        }

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        DefaultTableModel dtm1 = (DefaultTableModel) jTable2.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        dtm1.getDataVector().removeAllElements();
        dtm1.fireTableDataChanged();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement staff_st = connect.createStatement();
            Statement sales_st = connect.createStatement();
            Statement sr_st = connect.createStatement();
            ResultSet staff_rs = null;
            ResultSet sales_rs = null;
            ResultSet sr_rs = null;
            if (jComboBox1.getSelectedItem().equals("ALL")) {
                staff_rs = staff_st.executeQuery("Select Name, Emp_Code from Staff");
            } else if (!jComboBox1.getSelectedItem().equals("ALL")) {
                staff_rs = staff_st.executeQuery("Select Name, Emp_Code from Staff where Emp_Code = '" + (String) jComboBox1.getSelectedItem() + "'");
            }
            while (staff_rs.next()) {
                String Emp_Name = staff_rs.getString("Name");
                String Emp_Code = staff_rs.getString("Emp_Code");

                if (!date.equals("")) {
                    sales_rs = sales_st.executeQuery("select sum(qnty) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from billing where staff = '" + Emp_Code + "' and  date = '" + date + "' ");
                } else if (!from_date.equals("") && !to_date.equals("")) {
                    sales_rs = sales_st.executeQuery("select sum(qnty) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from billing where staff = '" + Emp_Code + "' and date Between '" + from_date + "' and '" + to_date + "' ");
                }
                if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                    sales_rs = sales_st.executeQuery("select sum(rate) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from billing where staff = '" + Emp_Code + "' ");
                }
                while (sales_rs.next()) {
                    int sales_qnty = sales_rs.getInt("stock_no");
                    double sales_rate = sales_rs.getDouble("rate");
                    double sales_disc_amnt = sales_rs.getDouble("disc_amnt");

                    if (sales_qnty != 0 && sales_rate != 0) {
                        Object sales[] = {Emp_Name, sales_qnty, sales_disc_amnt, sales_rate};
                        dtm.addRow(sales);
                    }

                }

                if (!date.equals("")) {
                    sr_rs = sr_st.executeQuery("select sum(qnty) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from salesreturn where staff = '" + Emp_Code + "' and  date = '" + date + "'");
                } else if (!from_date.equals("") && !to_date.equals("")) {
                    sr_rs = sr_st.executeQuery("select sum(qnty) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from salesreturn where staff = '" + Emp_Code + "' and date Between '" + from_date + "' and '" + to_date + "'");
                }
                if (date.equals("") && from_date.equals("") && to_date.equals("")) {
                    sr_rs = sr_st.executeQuery("select sum(qnty) as stock_no, sum(rate * qnty) as rate ,sum(disc_amnt) as disc_amnt from salesreturn where staff = '" + Emp_Code + "'");
                }

                while (sr_rs.next()) {
                    int sr_qnty = sr_rs.getInt("stock_no");
                    double sr_rate = sr_rs.getDouble("rate");
                    double sr_disc_amnt = sr_rs.getDouble("disc_amnt");

                    if (sr_qnty != 0 && sr_rate != 0) {
                        Object sr[] = {Emp_Name, sr_qnty, sr_disc_amnt, sr_rate};
                        dtm1.addRow(sr);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        total();
    }
//---------------------------------------------------------------------------------------------------------------    
//---------------------Getting table total data------------------------------------------------------------------

    public void total() {
        jTextField4.setText("0");
        jTextField3.setText("0");
        jTextField2.setText("0");

        jTextField7.setText("0");
        jTextField6.setText("0");
        jTextField5.setText("0");

        jTextField8.setText("0");
        jTextField9.setText("0");
        jTextField10.setText("0");

        int rowcount = jTable1.getRowCount();
        int rowcount1 = jTable2.getRowCount();
        DecimalFormat df = new DecimalFormat("0.00");
        int sales_qnty = 0;
        double sales_dis_amnt = 0;
        double sales_amnt = 0;
        int sr_qnty = 0;
        double sr_dis_amnt = 0;
        double sr_amnt = 0;
        int total_qnyt = 0;
        double toal_dis_amnt = 0;
        double total_amount = 0;

        for (int i = 0; i < rowcount; i++) {
            int q = Integer.parseInt(jTable1.getValueAt(i, 1) + "");
            sales_qnty = q + sales_qnty;

            double da = Double.parseDouble(jTable1.getValueAt(i, 2) + "");
            sales_dis_amnt = da + sales_dis_amnt;

            double amt = Double.parseDouble(jTable1.getValueAt(i, 3) + "");
            sales_amnt = amt + sales_amnt;
        }

        for (int j = 0; j < rowcount1; j++) {
            int q = Integer.parseInt(jTable2.getValueAt(j, 1) + "");
            sr_qnty = q + sr_qnty;

            double da = Double.parseDouble(jTable2.getValueAt(j, 2) + "");
            sr_dis_amnt = da + sr_dis_amnt;

            double amt = Double.parseDouble(jTable2.getValueAt(j, 3) + "");
            sr_amnt = amt + sr_amnt;
        }

        total_qnyt = sales_qnty - sr_qnty;
        toal_dis_amnt = sales_dis_amnt - sr_dis_amnt;
        total_amount = sales_amnt - sr_amnt;

        jTextField4.setText("" + sales_qnty);
        jTextField3.setText("" + df.format(sales_dis_amnt));
        jTextField2.setText("" + df.format(sales_amnt));

        jTextField7.setText("" + sr_qnty);
        jTextField6.setText("" + df.format(sr_dis_amnt));
        jTextField5.setText("" + df.format(sr_amnt));

        jTextField8.setText("" + total_qnyt);
        jTextField9.setText("" + df.format(toal_dis_amnt));
        jTextField10.setText("" + df.format(total_amount));

    }
//----------------------------------------------------------------------------------------------------

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel14 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();

        jDialog1.setTitle("Select Date");
        jDialog1.setSize(new java.awt.Dimension(334, 145));

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jLabel14.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 51, 255));
        jLabel14.setText("Date - ");

        jButton1.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(55, 31, 31));
        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(jLabel14)
                        .addGap(31, 31, 31)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jButton1)))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(29, 29, 29))
        );

        jDialog2.setTitle("Select Date");
        jDialog2.setSize(new java.awt.Dimension(392, 190));

        jLabel15.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 51, 255));
        jLabel15.setText("Date From -");

        jLabel16.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(0, 51, 255));
        jLabel16.setText("Date To - ");

        jDateChooser3.setDateFormatString("dd-MM-yyy");

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jButton2.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(71, 44, 44));
        jButton2.setText("Submit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel15))
                        .addGap(33, 33, 33)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(jButton2)))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addGap(9, 9, 9))
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel16))
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addComponent(jButton2)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Staff Wise Sales Report");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(57, 57, 58));
        jLabel1.setText("Staff Wise Sales Report -");

        jLabel2.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(57, 57, 58));
        jLabel2.setText("Date -");

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(57, 57, 58));
        jLabel3.setText("By Staff -");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Emp Name", "Quantity", "Dis Amount", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Emp Name", "Quantity", "Dis Amount", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jLabel4.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(57, 57, 58));
        jLabel4.setText("Sales - ");

        jLabel5.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(57, 57, 58));
        jLabel5.setText("Sales Return -");

        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField3.setEditable(false);
        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField4.setEditable(false);
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel6.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(57, 57, 58));
        jLabel6.setText("Total -");

        jTextField5.setEditable(false);
        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField6.setEditable(false);
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField7.setEditable(false);
        jTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel7.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(57, 57, 58));
        jLabel7.setText("Total -");

        jTextField8.setEditable(false);
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField9.setEditable(false);
        jTextField9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextField10.setEditable(false);
        jTextField10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel8.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(44, 29, 29));
        jLabel8.setText("Qnty");

        jLabel9.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(44, 29, 29));
        jLabel9.setText("Dis_Amnt");

        jLabel10.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(44, 29, 29));
        jLabel10.setText("Amount");

        jLabel11.setFont(new java.awt.Font("sansserif", 2, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(65, 24, 24));
        jLabel11.setText("Net Total -");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ALL" }));
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                jComboBox1PopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel4))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 444, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addGap(38, 38, 38)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(49, 49, 49)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14)
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(180, 180, 180)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator3)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addGap(82, 82, 82)
                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel8)
                .addGap(85, 85, 85)
                .addComponent(jLabel9)
                .addGap(67, 67, 67)
                .addComponent(jLabel10)
                .addGap(47, 47, 47))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(8, 8, 8)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6))))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addGap(1, 1, 1)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel8))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            dateStatus = true;
            jDialog1.setVisible(true);
        }
        if (key == evt.VK_F2) {
            dateStatus = false;
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//        java.util.Date date1 = jDateChooser1.getDate();
//        String date = formatter.format(date1);
//        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
//        DefaultTableModel dtm1 = (DefaultTableModel) jTable2.getModel();
//        dtm.getDataVector().removeAllElements();
//        dtm.fireTableDataChanged();
//        dtm1.getDataVector().removeAllElements();
//        dtm1.fireTableDataChanged();
//        try {
//            connection c = new connection();
//            Connection connect = c.cone();
//            Statement staff_st = connect.createStatement();
//            Statement sales_st = connect.createStatement();
//            Statement sr_st = connect.createStatement();
//            ResultSet staff_rs = null;
//            if (jComboBox1.getSelectedItem().equals("ALL")) {
//                staff_rs = staff_st.executeQuery("Select Name, Emp_Code from Staff");
//            } else if (!jComboBox1.getSelectedItem().equals("ALL")) {
//                staff_rs = staff_st.executeQuery("Select Name, Emp_Code from Staff where Emp_Code = '" + (String) jComboBox1.getSelectedItem() + "'");
//            }
//            while (staff_rs.next()) {
//                String Emp_Name = staff_rs.getString("Name");
//                String Emp_Code = staff_rs.getString("Emp_Code");
//
//                ResultSet sales_rs = sales_st.executeQuery("select count(stock_no) as stock_no, sum(rate) as rate ,sum(disc_amnt) as disc_amnt from billing where staff = '" + Emp_Code + "' and date = '" + date + "' ");
//                while (sales_rs.next()) {
//                    int sales_qnty = sales_rs.getInt("stock_no");
//                    double sales_rate = sales_rs.getDouble("rate");
//                    double sales_disc_amnt = sales_rs.getDouble("disc_amnt");
//
//                    if (sales_qnty != 0 && sales_rate != 0) {
//                        Object sales[] = {Emp_Name, sales_qnty, sales_disc_amnt, sales_rate};
//                        dtm.addRow(sales);
//                    }
//
//                }
//
//                ResultSet sr_rs = sr_st.executeQuery("select count(stock_no) as stock_no, sum(rate) as rate ,sum(disc_amnt) as disc_amnt from salesreturn where staff = '" + Emp_Code + "' and date = '" + date + "'");
//                while (sr_rs.next()) {
//                    int sr_qnty = sr_rs.getInt("stock_no");
//                    double sr_rate = sr_rs.getDouble("rate");
//                    double sr_disc_amnt = sr_rs.getDouble("disc_amnt");
//
//                    if (sr_qnty != 0 && sr_rate != 0) {
//                        Object sr[] = {Emp_Name, sr_qnty, sr_disc_amnt, sr_rate};
//                        dtm1.addRow(sr);
//                    }
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        total();
        getFilterData();
        jDialog1.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
//        java.util.Date date2 = jDateChooser2.getDate();
//        String from_date = formatter.format(date2);
//
//        java.util.Date date3 = jDateChooser3.getDate();
//        String to_date = formatter.format(date3);
//
//        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
//        DefaultTableModel dtm1 = (DefaultTableModel) jTable2.getModel();
//        dtm.getDataVector().removeAllElements();
//        dtm.fireTableDataChanged();
//        dtm1.getDataVector().removeAllElements();
//        dtm1.fireTableDataChanged();
//        try {
//            connection c = new connection();
//            Connection connect = c.cone();
//            Statement staff_st = connect.createStatement();
//            Statement sales_st = connect.createStatement();
//            Statement sr_st = connect.createStatement();
//            ResultSet staff_rs = null;
//            if (jComboBox1.getSelectedItem().equals("ALL")) {
//                staff_rs = staff_st.executeQuery("Select Name, Emp_Code from Staff");
//            } else if (!jComboBox1.getSelectedItem().equals("ALL")) {
//                staff_rs = staff_st.executeQuery("Select Name, Emp_Code from Staff where Emp_Code = '" + (String) jComboBox1.getSelectedItem() + "'");
//            }
//            while (staff_rs.next()) {
//                String Emp_Name = staff_rs.getString("Name");
//                String Emp_Code = staff_rs.getString("Emp_Code");
//
//                ResultSet sales_rs = sales_st.executeQuery("select count(stock_no) as stock_no, sum(rate) as rate ,sum(disc_amnt) as disc_amnt from billing where staff = '" + Emp_Code + "' and date Between '" + from_date + "' and '" + to_date + "' ");
//                while (sales_rs.next()) {
//                    int sales_qnty = sales_rs.getInt("stock_no");
//                    double sales_rate = sales_rs.getDouble("rate");
//                    double sales_disc_amnt = sales_rs.getDouble("disc_amnt");
//
//                    if (sales_qnty != 0 && sales_rate != 0) {
//                        Object sales[] = {Emp_Name, sales_qnty, sales_disc_amnt, sales_rate};
//                        dtm.addRow(sales);
//                    }
//
//                }
//
//                ResultSet sr_rs = sr_st.executeQuery("select count(stock_no) as stock_no, sum(rate) as rate ,sum(disc_amnt) as disc_amnt from salesreturn where staff = '" + Emp_Code + "' and date Between '" + from_date + "' and '" + to_date + "'");
//                while (sr_rs.next()) {
//                    int sr_qnty = sr_rs.getInt("stock_no");
//                    double sr_rate = sr_rs.getDouble("rate");
//                    double sr_disc_amnt = sr_rs.getDouble("disc_amnt");
//
//                    if (sr_qnty != 0 && sr_rate != 0) {
//                        Object sr[] = {Emp_Name, sr_qnty, sr_disc_amnt, sr_rate};
//                        dtm1.addRow(sr);
//                    }
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        total();
        getFilterData();
        jDialog2.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBox1PopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_jComboBox1PopupMenuWillBecomeInvisible
        getFilterData();
    }//GEN-LAST:event_jComboBox1PopupMenuWillBecomeInvisible

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        getFilterData();
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Staffwise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Staffwise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Staffwise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Staffwise_Sales_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Staffwise_Sales_Report().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
