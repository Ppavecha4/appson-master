package reporting.stock;

import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PRATEEK
 */
public class gstsalesreturnreport extends javax.swing.JFrame {

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    Calendar currentDate1 = Calendar.getInstance();
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
    String dateNow1 = formatter1.format(currentDate1.getTime());
    Calendar currentDate2 = Calendar.getInstance();
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyy");
    String dateNow2 = formatter2.format(currentDate2.getTime());

    SimpleDateFormat formatter4 = new SimpleDateFormat("dd-MM-yyy");
    String dateNow4 = formatter.format(currentDate.getTime());

    /**
     * Creates new form gstsalesreport
     */
    private static gstsalesreturnreport obj = null;

    public gstsalesreturnreport() {
        initComponents();
        this.setLocationRelativeTo(null);
        jTextField9.setText(dateNow);
        jTextField2.setVisible(false);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        double value = 0;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            ResultSet rs = st.executeQuery("select to_ledger,bill_refrence,description,sum(qnty),total,vatamnt,cgstamnt,sgstamnt,sum(vatamnt),sum(sgstamnt),sum(cgstamnt),sum(total),date from salesreturn group by bill_refrence,description,rate ORDER BY CAST(date as date) ASC");
            while (rs.next()) {
                String GSTno = "";
                String HSLcode = "";

                String IGSTaccount = "";
                String CGSTaccount = "";
                String SGSTaccount = "";

                String IGSTperc = "";
                String CGSTperc = "";
                String SGSTperc = "";
                ResultSet rshsncode = st2.executeQuery("select HSL_CODE,IGST,SGST,CGST from product where product_code='" + rs.getString("Description") + "'");
                if (rshsncode.next()) {
                    HSLcode = rshsncode.getString(1);
                    IGSTaccount = rshsncode.getString("IGST");
                    SGSTaccount = rshsncode.getString("SGST");
                    CGSTaccount = rshsncode.getString("CGST");
                }
                ResultSet rsgstno = st1.executeQuery("select tin_no from Party_sales where Party_name='" + rs.getString("to_ledger") + "'");
                if (rsgstno.next()) {
                    GSTno = rsgstno.getString(1);
                }

                ResultSet rsigstperc = st2.executeQuery("select percent from ledger where ledger_name='" + IGSTaccount + "'");
                if (rsigstperc.next()) {
                    IGSTperc = rsigstperc.getString(1);
                }

                ResultSet rssgstperc = st2.executeQuery("select percent from ledger where ledger_name='" + SGSTaccount + "'");
                if (rssgstperc.next()) {
                    SGSTperc = rssgstperc.getString(1);
                }

                ResultSet rscgstperc = st2.executeQuery("select percent from ledger where ledger_name='" + CGSTaccount + "'");
                if (rscgstperc.next()) {
                    CGSTperc = rscgstperc.getString(1);
                }

                if (rs.getDouble(9) == 0) {
                    IGSTperc = "0";
                }
                if (rs.getDouble(10) == 0) {
                    CGSTperc = "0";
                }
                if (rs.getDouble(11) == 0) {
                    SGSTperc = "0";
                }

                value = rs.getDouble(12) - rs.getDouble(9) - rs.getDouble(10) - rs.getDouble(11);
                value = value / rs.getDouble(4);

                Object o[] = {rs.getString("to_ledger"), GSTno, rs.getString("bill_refrence"), formatter4.format(formatter.parse(rs.getString("date"))), rs.getString("description"), HSLcode, rs.getDouble(4), value, rs.getDouble(4) * value, IGSTperc, rs.getDouble(9), CGSTperc, rs.getDouble(10), SGSTperc, rs.getDouble(11), rs.getDouble(12)};
                dtm.addRow(o);

            }
            totalcalculation();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static gstsalesreturnreport getObj() {
        if (obj == null) {
            obj = new gstsalesreturnreport();
        }
        return obj;
    }

    public void totalcalculation() {
        int rowcount = jTable1.getRowCount();
        double totalqty = 0;
        double value = 0;
        double totalvalue = 0;
        double totaligst = 0;
        double totalsgst = 0;
        double totalcgst = 0;
        double nettotal = 0;

        for (int i = 0; i < rowcount; i++) {
            totalqty = totalqty + Double.parseDouble(jTable1.getValueAt(i, 6) + "");
            value = value + Double.parseDouble(jTable1.getValueAt(i, 7) + "");
            totalvalue = totalvalue + Double.parseDouble(jTable1.getValueAt(i, 8) + "");
            totaligst = totaligst + Double.parseDouble(jTable1.getValueAt(i, 10) + "");
            totalsgst = totalsgst + Double.parseDouble(jTable1.getValueAt(i, 12) + "");
            totalcgst = totalcgst + Double.parseDouble(jTable1.getValueAt(i, 14) + "");
            nettotal = nettotal + Double.parseDouble(jTable1.getValueAt(i, 15) + "");
        }
        DecimalFormat df = new DecimalFormat("0.00");
        jTextField1.setText("" + df.format(totalqty));
        jTextField2.setText("" + df.format(value));
        jTextField3.setText("" + df.format(totalvalue));
        jTextField4.setText("" + df.format(totaligst));
        jTextField5.setText("" + df.format(totalsgst));
        jTextField6.setText("" + df.format(totalcgst));
        jTextField8.setText("" + df.format(nettotal));

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog2 = new javax.swing.JDialog();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jDialog1 = new javax.swing.JDialog();
        jLabel5 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();

        jDialog2.setMinimumSize(new java.awt.Dimension(362, 160));

        jLabel2.setText("Enter Period :");

        jLabel3.setText("From :");

        jLabel4.setText("To :");

        jDateChooser2.setDateFormatString("yyy-MM-dd");

        jDateChooser3.setDateFormatString("yyy-MM-dd");

        jButton2.setText("GO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(53, 53, 53))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jDialog1.setMinimumSize(new java.awt.Dimension(250, 100));

        jLabel5.setText("Enter Date : ");

        jDateChooser1.setDateFormatString("yyy-MM-dd");

        jButton1.setText("GO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Party Name", "GST No.", "Vch No.", "DATE", "Item", "HSN Code", "qty", "value", "total value", "IGST%", "Total Igst", "CGST%", "Total Cgst", "SGST%", "Total Sgst", "Net Amnt"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Object.class, java.lang.Double.class, java.lang.Object.class, java.lang.Double.class, java.lang.Object.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(200);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(14).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(15).setPreferredWidth(100);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("GST SALES  RETURN REPORT");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });

        jTextField3.setEditable(false);
        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jTextField4.setEditable(false);
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jTextField5.setEditable(false);
        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jTextField6.setEditable(false);
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jTextField8.setEditable(false);
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jTextField9.setEditable(false);
        jTextField9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField9KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(390, 390, 390)
                        .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(114, 114, 114)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(95, 95, 95)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(75, 75, 75)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(67, 67, 67)
                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1360, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();
        DefaultTableModel dtm;
        Date Date11 = jDateChooser2.getDate();
        String date1 = formatter.format(Date11);

        Date Date12 = jDateChooser3.getDate();
        String date2 = formatter.format(Date12);

        String date22 = formatter4.format(Date12);

        jTextField6.setText(date22);
        dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        double value = 0;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            ResultSet rs = st.executeQuery("select to_ledger,bill_refrence,description,sum(qnty),total,vatamnt,cgstamnt,sgstamnt,sum(vatamnt),sum(sgstamnt),sum(cgstamnt),sum(total),date from salesreturn where date between '" + date1 + "' and '" + date2 + "' group by bill_refrence,description,rate ORDER BY CAST(date as date) ASC");
            while (rs.next()) {
                String GSTno = "";
                String HSLcode = "";
                String IGSTaccount = "";
                String CGSTaccount = "";
                String SGSTaccount = "";

                String IGSTperc = "";
                String CGSTperc = "";
                String SGSTperc = "";
                ResultSet rshsncode = st2.executeQuery("select HSL_CODE,IGST,SGST,CGST from product where product_code='" + rs.getString("Description") + "'");
                if (rshsncode.next()) {
                    HSLcode = rshsncode.getString(1);
                    IGSTaccount = rshsncode.getString("IGST");
                    SGSTaccount = rshsncode.getString("SGST");
                    CGSTaccount = rshsncode.getString("CGST");
                }
                ResultSet rsgstno = st1.executeQuery("select tin_no from Party_sales where Party_name='" + rs.getString("to_ledger") + "'");
                if (rsgstno.next()) {
                    GSTno = rsgstno.getString(1);
                }

                ResultSet rsigstperc = st2.executeQuery("select percent from ledger where ledger_name='" + IGSTaccount + "'");
                if (rsigstperc.next()) {
                    IGSTperc = rsigstperc.getString(1);
                }

                ResultSet rssgstperc = st2.executeQuery("select percent from ledger where ledger_name='" + SGSTaccount + "'");
                if (rssgstperc.next()) {
                    SGSTperc = rssgstperc.getString(1);
                }

                ResultSet rscgstperc = st2.executeQuery("select percent from ledger where ledger_name='" + CGSTaccount + "'");
                if (rscgstperc.next()) {
                    CGSTperc = rscgstperc.getString(1);
                }

                if (rs.getDouble(9) == 0) {
                    IGSTperc = "0";
                }
                if (rs.getDouble(10) == 0) {
                    CGSTperc = "0";
                }
                if (rs.getDouble(11) == 0) {
                    SGSTperc = "0";
                }

                value = rs.getDouble(12) - rs.getDouble(9) - rs.getDouble(10) - rs.getDouble(11);
                value = value / rs.getDouble(4);

                Object o[] = {rs.getString("to_ledger"), GSTno, rs.getString("bill_refrence"), formatter4.format(formatter.parse(rs.getString("date"))), rs.getString("description"), HSLcode, rs.getDouble(4), value, rs.getDouble(4) * value, IGSTperc, rs.getDouble(9), CGSTperc, rs.getDouble(10), SGSTperc, rs.getDouble(11), rs.getDouble(12)};
                dtm.addRow(o);
            }
            totalcalculation();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        jDialog1.dispose();
        Date Date11 = jDateChooser1.getDate();
        String date1 = formatter.format(Date11);
        String date11 = formatter4.format(Date11);

        jTextField6.setText(date11);
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        double value = 0;
        try {

            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            ResultSet rs = st.executeQuery("select to_ledger,bill_refrence,description,sum(qnty),total,vatamnt,cgstamnt,sgstamnt,sum(vatamnt),sum(sgstamnt),sum(cgstamnt),sum(total),date from salesreturn where date='" + date1 + "' group by bill_refrence,description,rate ORDER BY CAST(date as date) ASC");
            while (rs.next()) {
                String HSLcode = "";
                String GSTno = "";
                String IGSTaccount = "";
                String CGSTaccount = "";
                String SGSTaccount = "";

                String IGSTperc = "";
                String CGSTperc = "";
                String SGSTperc = "";
                ResultSet rshsncode = st2.executeQuery("select HSL_CODE,IGST,SGST,CGST from product where product_code='" + rs.getString("Description") + "'");
                if (rshsncode.next()) {
                    HSLcode = rshsncode.getString(1);
                    IGSTaccount = rshsncode.getString("IGST");
                    SGSTaccount = rshsncode.getString("SGST");
                    CGSTaccount = rshsncode.getString("CGST");
                }
                ResultSet rsgstno = st1.executeQuery("select tin_no from Party_sales where Party_name='" + rs.getString("to_ledger") + "'");
                if (rsgstno.next()) {
                    GSTno = rsgstno.getString(1);
                }

                ResultSet rsigstperc = st2.executeQuery("select percent from ledger where ledger_name='" + IGSTaccount + "'");
                if (rsigstperc.next()) {
                    IGSTperc = rsigstperc.getString(1);
                }

                ResultSet rssgstperc = st2.executeQuery("select percent from ledger where ledger_name='" + SGSTaccount + "'");
                if (rssgstperc.next()) {
                    SGSTperc = rssgstperc.getString(1);
                }

                ResultSet rscgstperc = st2.executeQuery("select percent from ledger where ledger_name='" + CGSTaccount + "'");
                if (rscgstperc.next()) {
                    CGSTperc = rscgstperc.getString(1);
                }

                if (rs.getDouble(9) == 0) {
                    IGSTperc = "0";
                }
                if (rs.getDouble(10) == 0) {
                    CGSTperc = "0";
                }
                if (rs.getDouble(11) == 0) {
                    SGSTperc = "0";
                }

                value = rs.getDouble(12) - rs.getDouble(9) - rs.getDouble(10) - rs.getDouble(11);
                value = value / rs.getDouble(4);

                Object o[] = {rs.getString("to_ledger"), GSTno, rs.getString("bill_refrence"), formatter4.format(formatter.parse(rs.getString("date"))), rs.getString("description"), HSLcode, rs.getDouble(4), value, rs.getDouble(4) * value, IGSTperc, rs.getDouble(9), CGSTperc, rs.getDouble(10), SGSTperc, rs.getDouble(11), rs.getDouble(12)};
                dtm.addRow(o);
            }
            totalcalculation();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_formKeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jTextField9.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jTextField9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField9KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField9KeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);
        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jLabel1KeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField2KeyPressed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {

            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);

        }

        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(750, 50, 250, 150);
        }

        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(750, 50, 362, 104);
        }
    }//GEN-LAST:event_jTextField8KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(gstsalesreturnreport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(gstsalesreturnreport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(gstsalesreturnreport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(gstsalesreturnreport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new gstsalesreturnreport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
