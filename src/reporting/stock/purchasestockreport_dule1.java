package reporting.stock;

import connection.connection;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFPrintSetup;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Prateek
 */
public class purchasestockreport_dule1 extends javax.swing.JFrame{

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    ArrayList invoicenumber = new ArrayList();
    ArrayList invoicenumberdatewise = new ArrayList();
    ArrayList invoicenumberdatebetween = new ArrayList();

    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    String dateNow1 = formatter1.format(currentDate.getTime());
   XSSFWorkbook workbook = new XSSFWorkbook();
    /**
     * Creates new form purchasestockreport
     */
    private static purchasestockreport_dule1 obj = null;

    public purchasestockreport_dule1() {
        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        jTextField1.setText(dateNow1);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st3 = connect.createStatement();
            Statement st_amnt_5 = connect.createStatement();
            Statement st_amnt_12 = connect.createStatement();
            Statement qnty = connect.createStatement();

/* Stock table not present in aapson master            
            ResultSet rs_stock = st2.executeQuery("select * from stock group by Invoice_number order by Invoice_date ASC ");
            while (rs_stock.next()) {
                String GSTno = "";
                ResultSet rsgstno1 = st3.executeQuery("select tin_no from Party where Party_name='" + rs_stock.getString("Supplier_name") + "'");
                if (rsgstno1.next()) {
                    GSTno = rsgstno1.getString(1);
                }

                String igst_percent = rs_stock.getString("IGST_Tax_Account");
                String scgst_percent = rs_stock.getString("sgstaccount");
                if (rs_stock.getString("Invoice_date") == null) {
                } else {
                    if (igst_percent.equals("IGST 5%") || scgst_percent.equals("SGST 2.5%")) {
                        Object p[] = {formatter1.format(formatter.parse(rs_stock.getString("Invoice_date"))), rs_stock.getString("Invoice_number"), rs_stock.getString("supplier_name"), GSTno, rs_stock.getDouble("totalquantity"), rs_stock.getDouble("amount_after_discount"), 0, rs_stock.getDouble("IGST_Amount"), 0, rs_stock.getDouble("cgsttaxamount"), 0, rs_stock.getDouble("sgstamount"), 0, rs_stock.getDouble("Net_amount")};
                        dtm.addRow(p);
                    } else {
                        Object p[] = {formatter1.format(formatter.parse(rs_stock.getString("Invoice_date"))), rs_stock.getString("Invoice_number"), rs_stock.getString("supplier_name"), GSTno, rs_stock.getDouble("totalquantity"), 0, rs_stock.getDouble("amount_after_discount"), 0, rs_stock.getDouble("IGST_Amount"), 0, rs_stock.getDouble("cgsttaxamount"), 0, rs_stock.getDouble("sgstamount"), rs_stock.getDouble("Net_amount")};
                        dtm.addRow(p);
                    }
                }
            }
*/
            ResultSet rs = st.executeQuery("select * from stock1  group by Invoice_number order by Invoice_date ASC");
            while (rs.next()) {
                String GSTno = "";
                ResultSet rsgstno = st1.executeQuery("select gstin_no from party where Party_name='" + rs.getString("Supplier_name") + "'");
                if (rsgstno.next()) {
                    GSTno = rsgstno.getString(1);
                }
                String Invoice_number = rs.getString("Invoice_number");
                String Invoice_date = rs.getString("Invoice_date");
                String Supplier_name = rs.getString("supplier_name");
                double net_amnt = rs.getDouble("Net_amount");
                double amount5percent = 0;
                double amount12percent = 0;
                double total_qnty = 0;
                double igst_5 = 0;
                double sgst_25 = 0;
                double cgst_25 = 0;
                double igst_12 = 0;
                double sgst_6 = 0;
                double cgst_6 = 0;

                ResultSet rs5percentamount = st_amnt_5.executeQuery("select sum(Amount) as Amount, total_igst_amnt_5 ,total_sgst_amnt_25 ,total_cgst_amnt_25 from stock1 where by_ledger='PURCHASE 5%' and Invoice_number='" + Invoice_number + "' ");
                while (rs5percentamount.next()) {
                    amount5percent = rs5percentamount.getDouble("Amount");
                    igst_5 = rs5percentamount.getDouble("total_igst_amnt_5");
                    sgst_25 = rs5percentamount.getDouble("total_sgst_amnt_25");
                    cgst_25 = rs5percentamount.getDouble("total_cgst_amnt_25");

                }
                ResultSet rs12percentamount = st_amnt_12.executeQuery("select sum(Amount) as Amount, total_igst_amnt_12, total_sgst_amnt_6, total_cgst_amnt_6 from stock1 where by_ledger='PURCHASE 12%' and Invoice_number='" + Invoice_number + "' ");
                while (rs12percentamount.next()) {
                    amount12percent = rs12percentamount.getDouble("Amount");
                    igst_12 = rs12percentamount.getDouble("total_igst_amnt_12");
                    sgst_6 = rs12percentamount.getDouble("total_sgst_amnt_6");
                    cgst_6 = rs12percentamount.getDouble("total_cgst_amnt_6");

                }

                ResultSet rs_qnty = qnty.executeQuery("select sum(Qty) as Qty from stock1 where Invoice_number='" + Invoice_number + "' ");
                while (rs_qnty.next()) {
                    total_qnty = rs_qnty.getDouble("Qty");
                }

                if (Invoice_date == null) {
                } else {
                    Object o[] = {formatter1.format(formatter.parse(Invoice_date)), Invoice_number, Supplier_name, GSTno, total_qnty, amount5percent, amount12percent, igst_5, igst_12, cgst_25, cgst_6, sgst_25, sgst_6, net_amnt};
                    dtm.addRow(o);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        total_amnt();
    }

    public static purchasestockreport_dule1 getObj() {
        if (obj == null) {
            obj = new purchasestockreport_dule1();
        }
        return obj;
    }

    public void total_amnt() {
        int rowcount = jTable1.getRowCount();
        double qty = 0;
        double amnt = 0;
        double taxamnt_5 = 0;
        double taxamnt_12 = 0;
        double igst_5 = 0;
        double igst_12 = 0;
        double sgst_25 = 0;
        double sgst_6 = 0;
        double cgst_25 = 0;
        double cgst_6 = 0;
        double totalqty = 0;
        double totalamnt = 0;
        double totaltaxamnt_5 = 0;
        double totaltaxamnt_12 = 0;
        double total_igst_5 = 0;
        double total_igst_12 = 0;
        double total_sgst_25 = 0;
        double total_sgst_6 = 0;
        double total_cgst_25 = 0;
        double total_cgst_6 = 0;
        DecimalFormat df = new DecimalFormat("0.00");
        for (int i = 0; i < rowcount; i++) {
            qty = Double.parseDouble(jTable1.getValueAt(i, 4) + "");
            totalqty = qty + totalqty;

            amnt = Double.parseDouble(jTable1.getValueAt(i, 13) + "");
            totalamnt = amnt + totalamnt;

            taxamnt_5 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
            totaltaxamnt_5 = taxamnt_5 + totaltaxamnt_5;

            taxamnt_12 = Double.parseDouble(jTable1.getValueAt(i, 6) + "");
            totaltaxamnt_12 = taxamnt_12 + totaltaxamnt_12;

            igst_5 = Double.parseDouble(jTable1.getValueAt(i, 7) + "");
            total_igst_5 = igst_5 + total_igst_5;

            igst_12 = Double.parseDouble(jTable1.getValueAt(i, 8) + "");
            total_igst_12 = igst_12 + total_igst_12;

            cgst_25 = Double.parseDouble(jTable1.getValueAt(i, 9) + "");
            total_cgst_25 = cgst_25 + total_cgst_25;

            cgst_6 = Double.parseDouble(jTable1.getValueAt(i, 10) + "");
            total_cgst_6 = cgst_6 + total_cgst_6;

            sgst_25 = Double.parseDouble(jTable1.getValueAt(i, 11) + "");
            total_sgst_25 = sgst_25 + total_sgst_25;

            sgst_6 = Double.parseDouble(jTable1.getValueAt(i, 12) + "");
            total_sgst_6 = sgst_6 + total_sgst_6;

        }

        jTextField2.setText("" + df.format(totalqty));
        jTextField3.setText("" + df.format(totalamnt));
        jTextField4.setText("" + df.format(totaltaxamnt_5));
        jTextField11.setText("" + df.format(totaltaxamnt_12));
        jTextField5.setText("" + df.format(total_igst_5));
        jTextField6.setText("" + df.format(total_igst_12));
        jTextField7.setText("" + df.format(total_cgst_25));
        jTextField8.setText("" + df.format(total_cgst_6));
        jTextField9.setText("" + df.format(total_sgst_25));
        jTextField10.setText("" + df.format(total_sgst_6));
    }

    //---------Function to write export data into excel------------------------------------------------------------
    public void toExcel() throws FileNotFoundException, IOException {
        String Company_Name = "";
        String Company_No = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("Select * from cmp_detail");
            while(rs.next()){
            Company_Name = rs.getString("cmp_name");
            Company_No = rs.getString("phone_no");        
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        TableModel model = jTable1.getModel();
// Creating Sheet        
        XSSFSheet spreadsheet = workbook.createSheet();
// Creating Rows
        XSSFRow row = spreadsheet.createRow((short) 0);
        XSSFRow row1 = spreadsheet.createRow((short) 1);
        XSSFRow row2;
// Creating Cell        
        XSSFCell cell1 = (XSSFCell) row.createCell((short) 0);
// Creating Font        
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Verdana");
        font.setItalic(false);
        font.setBold(true);
        font.setColor(HSSFColor.AUTOMATIC.index);
// Creating Style        
        XSSFCellStyle style = workbook.createCellStyle();
        XSSFCellStyle style1 = workbook.createCellStyle();
        style1.setAlignment(HorizontalAlignment.RIGHT);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        style.setFont(font);
        style.setWrapText(true);
        row.setHeight((short) 700);
         cell1.setCellValue(Company_Name+" - "+Company_No + "\n" + "Party -");
//        cell1.setCellValue("Khabiya Cloth Store - 07412-492939" + "\n" + "Party -" + jComboBox1.getSelectedItem());
        cell1.setCellStyle(style);
// Marging the rows        
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 13));
// Setting Row header        
        for (int i = 0; i < model.getColumnCount(); i++) {
            XSSFCell cell = (XSSFCell) row1.createCell((short) i);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
// Getting Value for Each Column        
        for (int i = 0; i < model.getRowCount(); i++) {
// Creating row after header and row header            
            row = spreadsheet.createRow(i + 2);
            for (int j = 0; j < model.getColumnCount(); j++) {
// Creating Cell for puting values                
                XSSFCell cell2 = (XSSFCell) row.createCell((short) j);
                cell2.setCellValue((model.getValueAt(i, j)+""));
// Condition to check numeric value then right align                 
                if (j >= 3) {
                    cell2.setCellStyle(style1);
                }
// Condition to put table total after all rows                
                if (i == (model.getRowCount() - 1)) {
                    row2 = spreadsheet.createRow(i + 4);
                    XSSFCell c1 = (XSSFCell) row2.createCell((short) 2);
                    c1.setCellValue("Total");
                    c1.setCellStyle(style);
//                    spreadsheet.addMergedRegion(new CellRangeAddress(i + 4, i + 4, 0, 0));
                    c1 = (XSSFCell) row2.createCell((short) 4);
                    c1.setCellValue(jTextField2.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 5);
                    c1.setCellValue(jTextField4.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 6);
                    c1.setCellValue(jTextField11.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 7);
                    c1.setCellValue(jTextField5.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 8);
                    c1.setCellValue(jTextField6.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 9);
                    c1.setCellValue(jTextField7.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 10);
                    c1.setCellValue(jTextField8.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 11);
                    c1.setCellValue(jTextField9.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 12);
                    c1.setCellValue(jTextField10.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 13);
                    c1.setCellValue(jTextField3.getText());
                    c1.setCellStyle(style1);
                }
// Setting excel sheet area for printing                
                workbook.setPrintArea(0, 0, j, 0, i);
            }
        }
// Resizing cloumn
        for (int i = 0; i < model.getColumnCount(); i++) {
            spreadsheet.autoSizeColumn(i);
        }
// Setup for printing
        XSSFPrintSetup ps = (XSSFPrintSetup) spreadsheet.getPrintSetup();
//        ps.setLandscape(true);
        spreadsheet.setAutobreaks(true);
        spreadsheet.setFitToPage(true);
        ps.setFitWidth((short) 1);
        ps.setFitHeight((short) 0);
    }
//-------------------------------------------------------------------------------------------------------------
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel3 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel6 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jTextField4 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();

        jDialog1.setMinimumSize(new java.awt.Dimension(327, 115));

        jLabel3.setText("DATE: -");

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jButton1.setText("GO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(84, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(104, 104, 104))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(15, 15, 15))
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(420, 130));

        jLabel6.setText("Enter Date: -");

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jLabel7.setText("From: -");

        jLabel8.setText("To: -");

        jDateChooser3.setDateFormatString("dd-MM-yyy");

        jButton2.setText("OK");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(21, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(64, 64, 64))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "DATE", "BILL NO.", "PARTY NAME", "GST NO.", "QTY.", "Taxable_5%", "Taxable_12%", "IGST 5%", "IGST 12%", "CGST 2.5%", "CGST 6%", "SGST 2.5%", "SGST 6%", "TOTAL AMNT"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, false, true, true, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(80);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(1).setMinWidth(100);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(35);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(150);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(170);
            jTable1.getColumnModel().getColumn(3).setMinWidth(120);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(160);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(20);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel1.setText("GST BILLWISE PURCHASE REPORT");
        jLabel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel1KeyPressed(evt);
            }
        });

        jLabel2.setText("DATE :-");

        jTextField1.setEditable(false);
        jTextField1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField1MouseClicked(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTextField2.setEditable(false);
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jTextField3.setEditable(false);

        jLabel4.setText("AMNT: -");

        jLabel5.setText("QTY: -");

        jButton3.setText("Export");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });

        jLabel9.setText("Taxable 5%");

        jLabel10.setText("IGST 5%");

        jLabel11.setText("IGST 12%");

        jLabel12.setText("CGST 2.5%");

        jLabel13.setText("CGST 6%");

        jLabel14.setText("SGST 2.5%");

        jLabel15.setText("SGST 6%");

        jLabel16.setText("Total :--");

        jLabel17.setText("Taxable 12%");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jLabel5)
                        .addGap(32, 32, 32))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel10)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(29, 29, 29)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(516, 516, 516)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 425, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel15))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jLabel14))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)
                            .addComponent(jLabel4)
                            .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jLabel13))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jLabel12))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addComponent(jLabel11))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel16)
                                    .addComponent(jButton3))
                                .addGap(4, 4, 4)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(jLabel5)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)))
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jDialog1.dispose();
        Date date1 = jDateChooser1.getDate();
        String date = formatter.format(date1);
        jTextField1.setText(formatter1.format(date1));

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        System.out.println("date is" + date);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st3 = connect.createStatement();
            Statement st_amnt_5 = connect.createStatement();
            Statement st_amnt_12 = connect.createStatement();
            Statement qnty = connect.createStatement();

/* Stock table is not present in appson master            
            ResultSet rs_stock = st2.executeQuery("select * from stock where Invoice_date='" + date + "' group by Invoice_number order by Invoice_date ASC");
            while (rs_stock.next()) {
                String GSTno = "";
                ResultSet rsgstno1 = st3.executeQuery("select tin_no from Party where Party_name='" + rs_stock.getString("Supplier_name") + "'");
                if (rsgstno1.next()) {
                    GSTno = rsgstno1.getString(1);
                }

                String igst_percent = rs_stock.getString("IGST_Tax_Account");
                String scgst_percent = rs_stock.getString("sgstaccount");
                if (igst_percent.equals("IGST 5%") || scgst_percent.equals("SGST 2.5%")) {
                    Object p[] = {formatter1.format(formatter.parse(rs_stock.getString("Invoice_date"))), rs_stock.getString("Invoice_number"), rs_stock.getString("supplier_name"), GSTno, rs_stock.getDouble("totalquantity"), rs_stock.getDouble("amount_after_discount"), 0, rs_stock.getDouble("IGST_Amount"), 0, rs_stock.getDouble("cgsttaxamount"), 0, rs_stock.getDouble("sgstamount"), 0, rs_stock.getDouble("Net_amount")};
                    dtm.addRow(p);
                } else {
                    Object p[] = {formatter1.format(formatter.parse(rs_stock.getString("Invoice_date"))), rs_stock.getString("Invoice_number"), rs_stock.getString("supplier_name"), GSTno, rs_stock.getDouble("totalquantity"), 0, rs_stock.getDouble("amount_after_discount"), 0, rs_stock.getDouble("IGST_Amount"), 0, rs_stock.getDouble("cgsttaxamount"), 0, rs_stock.getDouble("sgstamount"), rs_stock.getDouble("Net_amount")};
                    dtm.addRow(p);
                }
            }
*/
            ResultSet rs = st.executeQuery("select * from stock1 where Invoice_date='" + date + "' group by Invoice_number order by Invoice_date ASC");
            while (rs.next()) {
                String GSTno = "";
                ResultSet rsgstno = st1.executeQuery("select gstin_no from party where Party_name='" + rs.getString("Supplier_name") + "'");
                if (rsgstno.next()) {
                    GSTno = rsgstno.getString(1);
                }
                String Invoice_number = rs.getString("Invoice_number");
                String Invoice_date = rs.getString("Invoice_date");
                String Supplier_name = rs.getString("supplier_name");
                double igst_5 = 0;
                double igst_12 = 0;
                double cgst_25 = 0;
                double cgst_6 = 0;
                double sgst_25 = 0;
                double sgst_6 = 0;
                double net_amnt = rs.getDouble("Net_amount");
                double amount5percent = 0;
                double amount12percent = 0;
                double total_qnty = 0;

                ResultSet rs5percentamount = st_amnt_5.executeQuery("select sum(Amount) as Amount,total_igst_amnt_5,total_sgst_amnt_25,total_cgst_amnt_25 from stock1 where by_ledger='PURCHASE 5%' and Invoice_number='" + Invoice_number + "' ");
                while (rs5percentamount.next()) {
                    amount5percent = rs5percentamount.getDouble("Amount");
                    igst_5 = rs5percentamount.getDouble("total_igst_amnt_5");
                    sgst_25 = rs5percentamount.getDouble("total_sgst_amnt_25");
                    cgst_25 = rs5percentamount.getDouble("total_cgst_amnt_25");

                }
                ResultSet rs12percentamount = st_amnt_12.executeQuery("select sum(Amount) as Amount,total_igst_amnt_12,total_sgst_amnt_6,total_cgst_amnt_6 from stock1 where by_ledger='PURCHASE 12%' and Invoice_number='" + Invoice_number + "' ");
                while (rs12percentamount.next()) {
                    amount12percent = rs12percentamount.getDouble("Amount");
                    igst_12 = rs12percentamount.getDouble("total_igst_amnt_12");
                    sgst_6 = rs12percentamount.getDouble("total_sgst_amnt_6");
                    cgst_6 = rs12percentamount.getDouble("total_cgst_amnt_6");
                }

                ResultSet rs_qnty = qnty.executeQuery("select sum(Qty) as Qty from stock1 where Invoice_number='" + Invoice_number + "' ");
                while (rs_qnty.next()) {
                    total_qnty = rs_qnty.getDouble("Qty");
                }

                if (Invoice_date == null) {
                } else {
                    Object o[] = {formatter1.format(formatter.parse(Invoice_date)), Invoice_number, Supplier_name, GSTno, total_qnty, amount5percent, amount12percent, igst_5, igst_12, cgst_25, cgst_6, sgst_25, sgst_6, net_amnt};
                    dtm.addRow(o);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        total_amnt();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(700, 40, 340, 130);
        }
        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(700, 40, 420, 140);
        }

    }//GEN-LAST:event_jTable1KeyPressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(700, 40, 340, 130);
        }
        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(700, 40, 420, 140);
        }

    }//GEN-LAST:event_formKeyPressed

    private void jLabel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(700, 40, 340, 130);
        }

        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(700, 40, 420, 140);
        }
    }//GEN-LAST:event_jLabel1KeyPressed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(700, 40, 340, 130);
        }
        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(700, 40, 420, 140);
        }
    }//GEN-LAST:event_jTextField1KeyPressed
//    int row = 0;
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        transaction.Pur_trans_dualGst pur = new transaction.Pur_trans_dualGst();
        pur.setVisible(true);
        pur.isUpdate = true;
        pur.jDateChooser2.setEnabled(true);
        pur.jTextField2.setEnabled(true);
        pur.jTextField3.setEnabled(false);
        pur.jTextField4.setEnabled(true);
        pur.jTextField5.setEnabled(false);
        pur.jTextField6.setEnabled(false);
        pur.jTextField7.setEnabled(false);
        pur.jTextField8.setEnabled(false);
        pur.jTextField9.setEnabled(false);
        pur.jTextField10.setEnabled(false);
        pur.jTextField11.setEnabled(true);
        pur.jTextField12.setEnabled(true);
        pur.jTextField13.setEnabled(true);
        pur.jTextField14.setEnabled(true);
        pur.jTextField15.setEnabled(true);
        pur.jTextField16.setEnabled(true);
        pur.jTextField40.setEditable(false);
//        pur.jTextField17.setEnabled(true);
        pur.jTextField23.setEnabled(true);
        pur.jRadioButton3.setEnabled(true);
        pur.jRadioButton4.setEnabled(true);
//        pur.jComboBox3.setEnabled(false);
        pur.jComboBox4.setEnabled(false);
        pur.jComboBox1.setEnabled(false);
        pur.jComboBox2.setEnabled(false);
        pur.jButton1.setVisible(false);
        pur.jButton5.setVisible(false);
        double igst5 = 0;
        double igst12 = 0;
        double sgst25 = 0;
        double sgst6 = 0;
        double cgst25 = 0;
        double cgst6 = 0;
        int row = jTable1.getSelectedRow();
        String purchase_account = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st3 = connect.createStatement();
            Statement st4 = connect.createStatement();
            Statement st5 = connect.createStatement();

            ResultSet rs = st.executeQuery("select * from stock1 where Invoice_number='" + jTable1.getValueAt(row, 1) + "" + "'");
            while (rs.next()) {
                pur.jTextField22.setText(rs.getString(1));
                pur.jTextField12.setText(rs.getString(2));
                pur.jDateChooser2.setDate(formatter.parse(rs.getString(3)));
                pur.jTextField2.setText(rs.getString(4));

                if (rs.getString("Purchase_type").equals("Credit")) {
                    pur.jRadioButton4.setSelected(true);
                    pur.purch = "Credit";
                } else {
                    pur.purch = "Cash";
                    pur.jRadioButton3.setSelected(true);
                }
                pur.jTextField4.setText(rs.getString("Refrence_number"));
                if (rs.getString("partypurchase_date").equals("")) {

                } else if (!rs.getString("partypurchase_date").equals("")) {
                    pur.jDateChooser1.setDate(formatter.parse(rs.getString("partypurchase_date")));
                }
                String byforgation = rs.getString("Byforgation");
                if (!byforgation.equals(null)) {
                    byforgation = "";
                }
                String articleno = rs.getString("articleno");
                if (!articleno.equals(null)) {
                    articleno = "";
                }

                DefaultTableModel dtm = (DefaultTableModel) pur.jTable1.getModel();

                Object o[] = {rs.getString("Sno"), rs.getString("by_ledger"), rs.getString("Product"), rs.getString("Qty"), rs.getString("Rate"), rs.getString("Amount"), rs.getString("Pdesc"), rs.getString("Retail"), rs.getString("Desc"), byforgation, articleno, rs.getString("samebarcode")};
                dtm.addRow(o);
                pur.rpurc_account.add(0, rs.getString("by_ledger"));
                pur.jTextField13.setText(rs.getString("Bill_amount"));
                pur.jTextField15.setText(rs.getString("amount_after_discount"));
                pur.jTextField14.setText(rs.getString("Other_Charges"));
                pur.jTextField16.setText(rs.getString("Net_amount"));
                
                double discount = Double.parseDouble(pur.jTextField13.getText()) - Double.parseDouble(pur.jTextField15.getText());

                pur.jTextField23.setText("" + discount);

                float dis_per;
                double dis_amnt = 0;
                double total_amnt = 0;

                dis_amnt = Double.parseDouble(pur.jTextField23.getText());
                total_amnt = Double.parseDouble(pur.jTextField13.getText());

                dis_per = (float) ((dis_amnt / total_amnt) * 100);

                DecimalFormat df1 = new DecimalFormat("0.00");
                String per = df1.format(dis_per);

                pur.jTextField11.setText(per + "");

                pur.sundrycreditorold = rs.getString("Supplier_name");
                pur.Purchaseaccount = rs.getString("by_ledger");
//                pur.rpurc_account.add(rs.getString("by_ledger"));
                pur.Party_cst = rs.getString("cgst_account");
                pur.other_charges = rs.getString("otherchargesaccount");
                pur.Party_vat = rs.getString("sgst_account");
                pur.Party_Igst = rs.getString("igst_account");
                pur.old_freight_gst = rs.getString("Freight_Gst");
                pur.freight_gst = rs.getDouble("Freight_Gst");
                pur.jTextField40.setText(""+rs.getString("Freight_Gst_Amount"));

                if (pur.other_charges.equals("null")) {
                    pur.other_charges = null;
                }

                if (pur.other_charges != null) {

                } else {
                    ResultSet rs4 = st4.executeQuery("select * from ledger where ledger_name='" + pur.other_charges + "'");
                    while (rs4.next()) {
                        pur.jLabel45.setText(rs4.getString(10));
                        pur.jLabel46.setText(rs4.getString(11));
                    }
                }

            }
            ResultSet rs_qnty = st1.executeQuery("select sum(Qty) as Qty from stock1 where Invoice_number='" + jTable1.getValueAt(row, 1) + "" + "'");
            while (rs_qnty.next()) {
                pur.jTextField28.setText(rs_qnty.getString("Qty"));
            }

            ResultSet rs_gst_5 = st2.executeQuery("select * from stock1 where Invoice_number='" + (jTable1.getValueAt(row, 1) + "") + "' AND by_ledger = 'PURCHASE 12%'");
            while (rs_gst_5.next()) {
                igst12 = rs_gst_5.getDouble("total_igst_amnt_12");
                sgst6 = rs_gst_5.getDouble("total_sgst_amnt_6");
                cgst6 = rs_gst_5.getDouble("total_cgst_amnt_6");
            }

            ResultSet rs_gst_12 = st2.executeQuery("select * from stock1 where Invoice_number='" + (jTable1.getValueAt(row, 1) + "") + "' AND by_ledger = 'Purchase 5%'");
            while (rs_gst_12.next()) {
                igst5 = rs_gst_12.getDouble("total_igst_amnt_5");
                sgst25 = rs_gst_12.getDouble("total_sgst_amnt_25");
                cgst25 = rs_gst_12.getDouble("total_cgst_amnt_25");
            }

            pur.jTextField17.setText("" + cgst25);
            pur.jTextField25.setText("" + cgst6);
            pur.old_cgst_25 = cgst25 + "";
            pur.old_cgst_6 = cgst6 + "";

            pur.jTextField24.setText("" + sgst25);
            pur.jTextField26.setText("" + sgst6);
            pur.old_sgst_25 = sgst25 + "";
            pur.old_sgst_6 = sgst6 + "";

            pur.jTextField1.setText("" + igst5);
            pur.jTextField27.setText("" + igst12);
            pur.old_igst_5 = igst5 + "";
            pur.old_igst_12 = igst12 + "";

            ResultSet g_rs = st.executeQuery("select * from party where party_code='" + pur.jTextField22.getText() + "" + "'");
            while (g_rs.next()) {
                if (g_rs.getBoolean("IGST") == true) {
                    pur.isIgstApplicable = true;
                    pur.jTextField17.setEnabled(false);
                    pur.jTextField24.setEnabled(false);
                    pur.jTextField25.setEnabled(false);
                    pur.jTextField26.setEnabled(false);
                } else if (g_rs.getBoolean("IGST") == false) {
                    pur.isIgstApplicable = false;
                    pur.jTextField1.setEnabled(false);
                    pur.jTextField27.setEnabled(false);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();
        Date date2 = jDateChooser2.getDate();
        String date = formatter.format(date2);

        Date date3 = jDateChooser3.getDate();
        String date1 = formatter.format(date3);
        jTextField1.setText(date1);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st3 = connect.createStatement();
            Statement st_amnt_5 = connect.createStatement();
            Statement st_amnt_12 = connect.createStatement();
            Statement qnty = connect.createStatement();
/*
            ResultSet rs_stock = st2.executeQuery("select * from stock where Invoice_date between '" + date + "' and '" + date1 + "' group by Invoice_number order by Invoice_date ASC");
            while (rs_stock.next()) {
                String GSTno = "";
                ResultSet rsgstno1 = st3.executeQuery("select tin_no from Party where Party_name='" + rs_stock.getString("Supplier_name") + "'");
                if (rsgstno1.next()) {
                    GSTno = rsgstno1.getString(1);
                }

                String igst_percent = rs_stock.getString("IGST_Tax_Account");
                String scgst_percent = rs_stock.getString("sgstaccount");
                if (igst_percent.equals("IGST 5%") || scgst_percent.equals("SGST 2.5%")) {
                    Object p[] = {formatter1.format(formatter.parse(rs_stock.getString("Invoice_date"))), rs_stock.getString("Invoice_number"), rs_stock.getString("supplier_name"), GSTno, rs_stock.getDouble("totalquantity"), rs_stock.getDouble("amount_after_discount"), 0, rs_stock.getDouble("IGST_Amount"), 0, rs_stock.getDouble("cgsttaxamount"), 0, rs_stock.getDouble("sgstamount"), 0, rs_stock.getDouble("Net_amount")};
                    dtm.addRow(p);
                } else {
                    Object p[] = {formatter1.format(formatter.parse(rs_stock.getString("Invoice_date"))), rs_stock.getString("Invoice_number"), rs_stock.getString("supplier_name"), GSTno, rs_stock.getDouble("totalquantity"), 0, rs_stock.getDouble("amount_after_discount"), 0, rs_stock.getDouble("IGST_Amount"), 0, rs_stock.getDouble("cgsttaxamount"), 0, rs_stock.getDouble("sgstamount"), rs_stock.getDouble("Net_amount")};
                    dtm.addRow(p);
                }
            }
*/
            ResultSet rs = st.executeQuery("select * from stock1 where Invoice_date between '" + date + "' and '" + date1 + "' group by Invoice_number order by Invoice_date ASC");
            while (rs.next()) {
                String GSTno = "";
                ResultSet rsgstno = st1.executeQuery("select gstin_no from party where Party_name='" + rs.getString("Supplier_name") + "'");
                if (rsgstno.next()) {
                    GSTno = rsgstno.getString(1);
                }
                String Invoice_number = rs.getString("Invoice_number");
                String Invoice_date = rs.getString("Invoice_date");
                String Supplier_name = rs.getString("supplier_name");
//                int total_qnty = rs.getInt("total_qnty");
                double igst_5 = 0;
                double igst_12 = 0;
                double cgst_25 = 0;
                double cgst_6 = 0;
                double sgst_25 = 0;
                double sgst_6 = 0;
                double net_amnt = rs.getDouble("Net_amount");
                double amount5percent = 0;
                double amount12percent = 0;
                double total_qnty = 0;

                ResultSet rs5percentamount = st_amnt_5.executeQuery("select sum(Amount) as Amount,total_igst_amnt_5,total_sgst_amnt_25,total_cgst_amnt_25 from stock1 where by_ledger='PURCHASE 5%' and Invoice_number='" + Invoice_number + "' ");
                while (rs5percentamount.next()) {
                    amount5percent = rs5percentamount.getDouble("Amount");
                    igst_5 = rs5percentamount.getDouble("total_igst_amnt_5");
                    sgst_25 = rs5percentamount.getDouble("total_sgst_amnt_25");
                    cgst_25 = rs5percentamount.getDouble("total_cgst_amnt_25");
                }
                ResultSet rs12percentamount = st_amnt_12.executeQuery("select sum(Amount) as Amount,total_igst_amnt_12,total_sgst_amnt_6,total_cgst_amnt_6 from stock1 where by_ledger='PURCHASE 12%' and Invoice_number='" + Invoice_number + "' ");
                while (rs12percentamount.next()) {
                    amount12percent = rs12percentamount.getDouble("Amount");
                    igst_12 = rs12percentamount.getDouble("total_igst_amnt_12");
                    sgst_6 = rs12percentamount.getDouble("total_sgst_amnt_6");
                    cgst_6 = rs12percentamount.getDouble("total_cgst_amnt_6");

                }

                ResultSet rs_qnty = qnty.executeQuery("select sum(Qty) as Qty from stock1 where Invoice_number='" + Invoice_number + "' ");
                while (rs_qnty.next()) {
                    total_qnty = rs_qnty.getDouble("Qty");
                }

                if (Invoice_date == null) {
                } else {
                    Object o[] = {formatter1.format(formatter.parse(Invoice_date)), Invoice_number, Supplier_name, GSTno, total_qnty, amount5percent, amount12percent, igst_5, igst_12, cgst_25, cgst_6, sgst_25, sgst_6, net_amnt};
                    dtm.addRow(o);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        total_amnt();

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
/*
        int tb1 = jTable1.getRowCount();
        if (tb1 > 0) {
            PrinterJob job = PrinterJob.getPrinterJob();
            Paper paper = new Paper();
            PageFormat pf = job.defaultPage();
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            job.setPrintable(this);
            boolean b = job.printDialog();
            try {
                if (b == true) {
                    job.print();
                }
            } catch (PrinterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "No Value To print...");
        }
*/
 
        JFileChooser fc = new JFileChooser();
        int option = fc.showSaveDialog(purchasestockreport_dule1.this);
        if (option == JFileChooser.APPROVE_OPTION) {
            String filename = fc.getSelectedFile().getName();
            String path = fc.getSelectedFile().getParentFile().getPath();

            int len = filename.length();
            String ext = "";
            String file = "";

            if (len > 4) {
                ext = filename.substring(len - 4, len);
            }

            if (ext.equals(".xlsx")) {
                file = path + "\\" + filename;
            } else {
                file = path + "\\" + filename + ".xlsx";
            }
            try {
                toExcel();
                FileOutputStream out = new FileOutputStream(new File(file));
                //write operation workbook using file out object 
                workbook.write(out);

//                Desktop desktop = Desktop.getDesktop();
//                try {
//                    desktop.print(new File(file));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                System.out.println("createworkbook.xlsx written successfully");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
   
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F3) {
            jDialog1.setVisible(true);
            jDialog1.setBounds(700, 40, 340, 130);
        }
        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
            jDialog2.setBounds(700, 40, 420, 140);
        }
    }//GEN-LAST:event_jButton3KeyPressed

    private void jTextField1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField1MouseClicked
        jDialog1.setVisible(true);
        jDialog1.setBounds(700, 40, 340, 130);
    }//GEN-LAST:event_jTextField1MouseClicked

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(purchasestockreport_dule1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(purchasestockreport_dule1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(purchasestockreport_dule1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(purchasestockreport_dule1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new purchasestockreport_dule1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
