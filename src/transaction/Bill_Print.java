package transaction;

import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Aashish
 */
public class Bill_Print {

    public void Khabiya_Bill(Graphics g, PageFormat pf, int page) {
        //      ndb.setVisible(true);
        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fontMetrics = g2d.getFontMetrics();

        Font font = new Font(" Bill NO - SL/1718/1", Font.BOLD, 12);
        g2d.setFont(font);
        g2d.drawString(" Khabiya Cloth Store ", 150, 50);

        Font font2 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 10);
        g2d.setFont(font2);

        g2d.drawString(" 37, New Road ", 150, 60);
        g2d.drawString(" Ratlam - 457001(M.P) ", 150, 70);
        g2d.drawString(" Phone No.- 07412-492939", 150, 80);

        // Horizontal Line            
        g2d.drawLine(0, 85, 420, 85);

        // Left Side box
        g2d.drawString("Invoice No  : ", 15, 100);
        g2d.drawString("Invoice Date : ", 15, 115);
        g2d.drawString("GSTIN : 23AMDPK9264H1ZZ", 15, 130);

        // Virtical Line
        g2d.drawLine(150, 85, 150, 140);

        // Right Side box
        g2d.drawString("Name  :  ", 160, 95);
        g2d.drawString("Address :  ", 160, 105);
        g2d.drawString("Mobile No :  ", 160, 125);
        g2d.drawString("GSTIN No :  ", 160, 135);

        // Horizontal Line            
        g2d.drawLine(0, 140, 420, 140);
        Font font3 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 8);
        g2d.setFont(font3);

        //Table Header View
        g2d.drawString("S No", 15, 150);
        g2d.drawString("  Particulars  ", 50, 150);
        g2d.drawString("Staff", 190, 150);
        g2d.drawString("HSN Code", 230, 150);
        g2d.drawString("Rate", 290, 150);
        g2d.drawString(" Amount", 360, 150);
        g2d.drawString("Total - ", 280, 450);

        g2d.drawString("Net Amount - ", 280, 490);

        // Draw Horizontal line after table
        g2d.drawLine(0, 160, 420, 160);
        g2d.drawLine(0, 440, 420, 440);
        g2d.drawLine(270, 440, 270, 500); // Verticle Line 
        g2d.drawLine(270, 480, 420, 480);
        g2d.drawLine(270, 500, 420, 500);

        // For terms and Condition 
        g2d.drawString("Terms and Condition - ", 15, 477);
        g2d.drawString("1) No Exchange No Claim", 15, 490);
        g2d.drawString("2) Subject to Ratlam jurisdiction", 15, 500);

        //For tax calculation
        g2d.drawString("Tax Calculation - ", 15, 520);
        g2d.drawString("IGST - ", 15, 535);
        g2d.drawString("SGST - ", 85, 535);
        g2d.drawString("CGST - ", 170, 535);

        // For Authority Signatury
        g2d.drawString("For Khabiya :", 320, 555);
        // Getting Data For printing 

        // For upper left side box   
        g2d.drawString(New_Dual_Billing.getObj().jTextField1.getText(), 75, 100);
        System.out.println("bill_count" + New_Dual_Billing.getObj().jTextField1.getText());
        String Date = New_Dual_Billing.getObj().formatter1.format(New_Dual_Billing.getObj().jDateChooser1.getDate());
        g2d.drawString(Date, 80, 115);

        // For Uper Right Side Box
        if (!New_Dual_Billing.getObj().jTextField4.getText().isEmpty()) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select * from party_sales where party_code='" + New_Dual_Billing.getObj().jTextField4.getText() + "'");
                while (rs.next()) {
                    g2d.drawString(rs.getString("party_name"), 210, 95);
                    g2d.drawString(rs.getString("address"), 210, 105);
                    g2d.drawString(rs.getString("mobileno"), 215, 125);
                    g2d.drawString(rs.getString("tin_no"), 215, 135);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!New_Dual_Billing.getObj().jTextField3.getText().isEmpty()) {
            g2d.drawString(New_Dual_Billing.getObj().jTextField3.getText(), 210, 100);
            g2d.drawString(New_Dual_Billing.getObj().jTextField37.getText(), 215, 125);
        }
        // Getting Table data
        DefaultTableModel dtm = (DefaultTableModel) New_Dual_Billing.getObj().jTable1.getModel();
        int rowcount = dtm.getRowCount();
        String total_discount = "";
        String total_after_tax = "";
        String total_igsttax = "";
        String total_sgsttax = "";
        String total_cgsttax = "";
        String By_Cash = "";
        String By_Debit = "";
        String By_Card = "";

        double sr_amt = 0;

        int yaxis = 200;

        int next = 0;

        DecimalFormat formatter2 = new DecimalFormat("0.00");

        for (int i = 0; i < rowcount; i++) {
            int j = 10;
            g2d.drawString(New_Dual_Billing.getObj().jTable1.getValueAt(i, 0) + "", 15, 170 + (j * i)); // For Serial No
            g2d.drawString(New_Dual_Billing.getObj().jTable1.getValueAt(i, 1) + "", 155, 170 + (j * i)); // For Stock No
            g2d.drawString(New_Dual_Billing.getObj().jTable1.getValueAt(i, 10) + "", 190, 170 + (j * i)); // For Staff
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(New_Dual_Billing.getObj().jTable1.getValueAt(i, 3) + ""))) + "", 280, 170 + (j * i));// For Rate
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(New_Dual_Billing.getObj().jTable1.getValueAt(i, 5) + ""))) + "", 340, 170 + (j * i));// For Amount

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                ResultSet rs2 = st1.executeQuery("select * from product where product_code='" + New_Dual_Billing.getObj().jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs2.next()) {
                    g2d.drawString(rs2.getString("HSL_CODE"), 230, 170 + (j * i)); // For HSN COde
                    g2d.drawString(rs2.getString("print_On"), 35, 170 + (j * i)); // Product Name
                    New_Dual_Billing.getObj().ismeter = rs2.getInt("ismeter");
                }
                if (New_Dual_Billing.getObj().ismeter == 1) {
                    g2d.drawString(New_Dual_Billing.getObj().jTable1.getValueAt(i, 4) + "" + " (M) ", 250, 170 + (j * i));
                }

                Statement st2 = connect.createStatement();
                ResultSet rs3 = st2.executeQuery("select * from billing where bill_refrence='" + New_Dual_Billing.getObj().jTextField1.getText() + "'");
                while (rs3.next()) {
                    total_igsttax = rs3.getString("total_igst_taxamnt");
                    total_sgsttax = rs3.getString("total_sgst_taxamnt");
                    total_cgsttax = rs3.getString("total_cgst_taxamnt");
                    total_discount = rs3.getString("total_disc_amnt");
                    total_after_tax = rs3.getString("total_value_after_tax");
                    By_Cash = rs3.getString("cashamount");
                    By_Debit = rs3.getString("sundrydebtorsamount");
                    By_Card = rs3.getString("cardamount");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
// For Sales Return
            String sr_ref = (String) New_Dual_Billing.getObj().jComboBox3.getSelectedItem();

            if (i == (rowcount - 1)) {
                next = i + 1;
                yaxis = i + 210 + (j * i);
                if (next > i) {

                    if (!New_Dual_Billing.getObj().jComboBox3.getSelectedItem().toString().equals("Select")) {
                        System.out.println("sr_ref" + sr_ref);
                        g2d.drawLine(0, yaxis, 420, yaxis);
                        g2d.drawString("Sales Return ", 180, yaxis + 10);
                        g2d.drawString(sr_ref, 50, yaxis + 10);
                        g2d.drawLine(0, yaxis + 20, 420, yaxis + 20);

                        String total_srigst = "";
                        String total_srsgst = "";
                        String total_srcgst = "";

                        for (int p = 0; p < rowcount; p++) {
                            try {
                                ArrayList serial_no = new ArrayList();
                                ArrayList Stock_no = new ArrayList();
                                ArrayList Stock_print = new ArrayList();
                                ArrayList hsl_code = new ArrayList();
                                ArrayList srate = new ArrayList();
                                ArrayList samount = new ArrayList();
                                connection c = new connection();
                                Connection connect = c.cone();
                                Statement st1 = connect.createStatement();

                                ResultSet rs1 = st1.executeQuery("Select * from salesreturn where bill_refrence = '" + sr_ref + "" + "' ");
                                while (rs1.next()) {
                                    serial_no.add(rs1.getString("sno"));
                                    Stock_no.add(rs1.getString("stock_no"));
                                    samount.add(rs1.getDouble("total"));
                                    srate.add(rs1.getDouble("value"));
                                    sr_amt = rs1.getDouble("total_amnt");

                                    total_srigst = rs1.getString("total_igst_taxamnt");
                                    total_srsgst = rs1.getString("total_sgst_taxamnt");
                                    total_srcgst = rs1.getString("total_cgst_taxamnt");

                                    Statement st2 = connect.createStatement();
                                    ResultSet rs2 = st2.executeQuery("select * from product where product_code='" + rs1.getString("description") + "' ");
                                    while (rs2.next()) {
                                        Stock_print.add(rs2.getString("print_on"));
                                        hsl_code.add(rs2.getString("HSL_CODE"));
                                    }
                                }

                                g2d.drawString(serial_no.get(p) + "", 5, yaxis + 30 + (j * p));
                                g2d.drawString(Stock_no.get(p) + "", 165, yaxis + 30 + (j * p));
                                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(srate.get(p) + ""))), 280, yaxis + 30 + (j * p));
                                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(samount.get(p) + ""))), 340, yaxis + 30 + (j * p));

                                g2d.drawString(Stock_print.get(p) + "", 35, yaxis + 30 + (j * p));
                                g2d.drawString(hsl_code.get(p) + "", 220, yaxis + 30 + (j * p));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        g2d.drawString("Sale Return - ", 280, 460);
                        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(sr_amt + ""))), 340, 460);

                        g2d.drawLine(0, yaxis + 55 + i, 420, yaxis + 55 + i);

//For Sales return tax calculation
                        g2d.drawString("Sale Return Tax - ", 15, 545);
                        g2d.drawString("IGST - ", 15, 555);
                        g2d.drawString("SGST - ", 85, 555);
                        g2d.drawString("CGST - ", 170, 555);

                        g2d.drawString(total_srigst + "", 40, 555);// For IGST amount
                        g2d.drawString(total_srsgst + "", 115, 555);// For SGST amount
                        g2d.drawString(total_srcgst + "", 200, 555);// For CGST amount

                    }
                }
                System.out.println("Value of next is " + next);
            }

        }

        if (!total_discount.equals("0")) {
            g2d.drawString("Discount     - ", 280, 470);
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(total_discount + ""))), 340, 470);// For Discount amount
        }

        // For Mode of payment
        g2d.drawString("Mode of Payment - ", 15, 450);

        if (!By_Cash.equals("0") && By_Debit.equals("0") && By_Card.equals("0")) {
            g2d.drawString("By Cash  - ", 15, 463);
        } else if (!By_Debit.equals("0") && By_Cash.equals("0") && By_Card.equals("0")) {
            g2d.drawString("By Debit  - ", 15, 463);
        } else if (!By_Card.equals("0") && By_Cash.equals("0") && By_Debit.equals("0")) {
            g2d.drawString("By Card  - ", 15, 463);
        } else {
            if (!By_Cash.equals("0")) {
                g2d.drawString("By Cash  - ", 15, 463);
                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Cash + ""))), 35, 463);// For By cash amount
            }

            if (!By_Debit.equals("0")) {
                g2d.drawString("By Debit  - ", 85, 463);
                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Debit + ""))), 105, 463);// For By debit amount
            }

            if (!By_Card.equals("0")) {
                g2d.drawString("By Card  - ", 170, 463);
                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Card + ""))), 190, 463);// For By Card amount
            }
        }
        double amnt_after_discount = Double.parseDouble(New_Dual_Billing.getObj().jTextField15.getText()) - Double.parseDouble(total_discount) - sr_amt;
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(New_Dual_Billing.getObj().jTextField15.getText() + ""))), 340, 450);// For Total amount
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(amnt_after_discount + ""))), 340, 490);

        g2d.drawString(total_igsttax + "", 40, 535);// For IGST amount
        g2d.drawString(total_sgsttax + "", 115, 535);// For SGST amount
        g2d.drawString(total_cgsttax + "", 200, 535);// For CGST amount

        System.out.println("printing");
    }

    /*    
    public void khabiya_salesReturn(Graphics g, PageFormat pf, int page) {
//            New_Dual_SalesReturn nds = new New_Dual_SalesReturn();
       New_Dual_SalesReturn nds  = New_Dual_SalesReturn.getObj();
        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fontMetrics = g2d.getFontMetrics();

        Font font = new Font(" Bill NO - SL/1718/1", Font.BOLD, 12);
        g2d.setFont(font);
        g2d.drawString(" Khabiya Cloth Store ", 150, 50);

        Font font2 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 10);
        g2d.setFont(font2);

        g2d.drawString(" 37, New Road ", 150, 60);
        g2d.drawString(" Ratlam - -457001(M.P) ", 150, 70);
        g2d.drawString(" Phone No.- 07412-492939", 150, 80);

        // Horizontal Line            
        g2d.drawLine(0, 85, 420, 85);

        // Left Side box
        g2d.drawString("Invoice No  : ", 15, 100);
        g2d.drawString("Invoice Date : ", 15, 115);
        g2d.drawString("GSTIN : 23AMDPK9264H1ZZ", 15, 130);

        // Virtical Line
        g2d.drawLine(150, 85, 150, 140);

        // Right Side box
        g2d.drawString("Name  :  ", 160, 95);
        g2d.drawString("Address :  ", 160, 105);
        g2d.drawString("Mobile No :  ", 160, 125);
        g2d.drawString("GSTIN No :  ", 160, 135);

        // Horizontal Line            
        g2d.drawLine(0, 140, 420, 140);
        Font font3 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 8);
        g2d.setFont(font3);

        //Table Header View
        g2d.drawString("S No", 15, 150);
        g2d.drawString("  Particulars  ", 50, 150);
        g2d.drawString("HSN Code", 220, 150);
        g2d.drawString("Rate", 290, 150);
        g2d.drawString(" Amount", 360, 150);
        g2d.drawString("Total - ", 280, 450);

        g2d.drawString("Net Amount - ", 280, 490);

        // Draw Horizontal line after table
        g2d.drawLine(0, 160, 420, 160);
        g2d.drawLine(0, 440, 420, 440);
        g2d.drawLine(270, 440, 270, 500); // Verticle Line 
        g2d.drawLine(270, 480, 420, 480);
        g2d.drawLine(270, 500, 420, 500);

        // For terms and Condition 
        g2d.drawString("Terms and Condition - ", 15, 477);
        g2d.drawString("1) No Exchange No Claim", 15, 490);
        g2d.drawString("2) Subject to Ratlam jurisdiction", 15, 500);

        //For tax calculation
        g2d.drawString("Tax Calculation - ", 15, 520);
        g2d.drawString("IGST - ", 15, 535);
        g2d.drawString("SGST - ", 85, 535);
        g2d.drawString("CGST - ", 170, 535);

        // For Authority Signatury
        g2d.drawString("For Khabiya :", 320, 555);
        // Getting Data For printing 

        // For upper left side box   
        g2d.drawString(nds.jTextField1.getText(), 75, 100);
        String Date = nds.formatter1.format(nds.jDateChooser1.getDate());
        g2d.drawString(Date, 80, 115);

        // For Uper Right Side Box
        if (!nds.jTextField4.getText().isEmpty()) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select * from party_sales where party_code='" + nds.jTextField4.getText() + "'");
                while (rs.next()) {
                    g2d.drawString(rs.getString("party_name"), 210, 95);
                    g2d.drawString(rs.getString("address1") + "" + rs.getString("address2"), 210, 105);
                    g2d.drawString(rs.getString("address3") + "" + rs.getString("address4"), 210, 105);
                    g2d.drawString(rs.getString("mobileno"), 215, 125);
                    g2d.drawString(rs.getString("gstin_no"), 215, 135);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!nds.jTextField3.getText().isEmpty()) {
            g2d.drawString(nds.jTextField3.getText(), 210, 100);
        }
        // Getting Table data
        DefaultTableModel dtm = (DefaultTableModel) nds.jTable1.getModel();
        int rowcount = dtm.getRowCount();
        String total_discount = "";
        String total_after_tax = "";
        String total_igsttax = "";
        String total_sgsttax = "";
        String total_cgsttax = "";
        String By_Cash = "";
        String By_Debit = "";
        String By_Card = "";

        DecimalFormat formatter2 = new DecimalFormat("0.00");

        for (int i = 0; i < rowcount; i++) {
            int j = 10;
            g2d.drawString(nds.jTable1.getValueAt(i, 0) + "", 15, 170 + (j * i)); // Serial No
            g2d.drawString(nds.jTable1.getValueAt(i, 1) + "", 165, 170 + (j * i)); // Stock No
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(nds.jTable1.getValueAt(i, 3) + ""))) + "", 280, 170 + (j * i));// For Rate
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(nds.jTable1.getValueAt(i, 5) + ""))) + "", 340, 170 + (j * i));// For Amount

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                ResultSet rs2 = st1.executeQuery("select * from product where product_code='" + nds.jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs2.next()) {
                    g2d.drawString(rs2.getString("HSL_CODE"), 220, 170 + (j * i)); // For HSN COde
                    g2d.drawString(rs2.getString("print_On"), 35, 170 + (j * i)); // Product Name
                    nds.ismeter = rs2.getInt("ismeter");
                }
                if (nds.ismeter == 1) {
                    g2d.drawString(nds.jTable1.getValueAt(i, 4) + "" + " (M) ", 250, 170 + (j * i));
                }

                Statement st2 = connect.createStatement();
                ResultSet rs3 = st2.executeQuery("Select * from salesreturn where bill_refrence='" + nds.jTextField1.getText() + "'");
                while (rs3.next()) {
                    total_igsttax = rs3.getString("total_igst_taxamnt");
                    total_sgsttax = rs3.getString("total_sgst_taxamnt");
                    total_cgsttax = rs3.getString("total_cgst_taxamnt");
                    total_discount = rs3.getString("total_disc_amnt");
                    total_after_tax = rs3.getString("total_value_after_tax");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!total_discount.equals("0.00")) {
            g2d.drawString("Discount     - ", 280, 470);
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(total_discount + ""))), 340, 470);// For Discount amount
        }

        // For Mode of payment
        g2d.drawString("Mode of Payment - ", 15, 450);

        double amnt_after_discount = Double.parseDouble(nds.jTextField15.getText()) - Double.parseDouble(total_discount);
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(nds.jTextField15.getText() + ""))), 340, 450);// For Total amount
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(amnt_after_discount + ""))), 340, 490);

        g2d.drawString(total_igsttax + "", 40, 535);// For IGST amount
        g2d.drawString(total_sgsttax + "", 115, 535);// For SGST amount
        g2d.drawString(total_cgsttax + "", 200, 535);// For CGST amount

        System.out.println("printing");
    }
     */
    public static void main(String[] args) {
        
    }
}
