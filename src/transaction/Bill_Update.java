package transaction;

import connection.connection;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Aashish
 */
public class Bill_Update extends javax.swing.JFrame {

    /**
     * Creates new form Bill_Update
     */
    private static Bill_Update obj = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    int row = 0;

    public Bill_Update() {
        initComponents();
        getData();
        this.setLocationRelativeTo(null);
        jDialog1.setLocationRelativeTo(null);
    }

    public static Bill_Update getObj() {
        if (obj == null) {
            obj = new Bill_Update();
        }
        return obj;
    }

//--------------------------Getting Data----------------------------------------
    public void getData() {
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.getDataVector().removeAllElements();
        dtm.fireTableStructureChanged();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = null;
            if (jDateChooser1.getDate() == null) {
                rs = st.executeQuery("Select * from billing where date = '" + dateNow + "' group by bill_refrence");
            } else if (jDateChooser1.getDate() != null) {
                rs = st.executeQuery("Select * from billing where date = '" + formatter.format(jDateChooser1.getDate()) + "' group by bill_refrence");
            }
            while (rs.next()) {
                Object o[] = {rs.getString("date"), rs.getString("bill_refrence"), rs.getString("total_qnty"), rs.getString("total_value_after_tax")};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------
//-------------------------Deleting the bill------------------------------------

    public void delete() {
        int index = jTable1.getSelectedRow();
        ArrayList old_sundrydebtorsaccountcurrbal = new ArrayList();
        ArrayList old_sundrydebtorsaccountcurrbaltype = new ArrayList();
        ArrayList old_sundrydebtorsaccountupdatecurrbaltype = new ArrayList();
        ArrayList old_sundrydebtorsaccountupdatecurrbal = new ArrayList();
        ArrayList old_cardaccountcurrbal = new ArrayList();
        ArrayList old_cardaccountcurrbaltype = new ArrayList();
        ArrayList old_cardaccountupdatecurrbal = new ArrayList();
        ArrayList old_cardaccountupdatecurrbaltype = new ArrayList();
        ArrayList old_cashaccountcurrbal = new ArrayList();
        ArrayList old_cashaccountcurrbaltype = new ArrayList();
        ArrayList old_cashaccountupdatecurrbal = new ArrayList();
        ArrayList old_cashaccountupdatecurrbaltype = new ArrayList();
        double discounamntt = 0;
        double cashamount = 0;
        String cashaccount = "";
        double debtoramount = 0;
        String debtoraccount = "";
        double cardamount = 0;
        String cardupdateaccount = "";
        double round1 = 0;
        double reducingtax = 0;
        String Party_ledger = "";
        ArrayList vat1 = new ArrayList();
        ArrayList ledgervat1 = new ArrayList();
        ArrayList sgst1 = new ArrayList();
        ArrayList ledgersgst1 = new ArrayList();
        ArrayList cgst1 = new ArrayList();
        ArrayList ledgercgst1 = new ArrayList();
        int ismeterupdate = 0;
        ArrayList vatcurrbal = new ArrayList();
        ArrayList vatcurrbaltype = new ArrayList();
        ArrayList updatevatcurrbal = new ArrayList();
        ArrayList updatevatcurrbaltype = new ArrayList();

        ArrayList sgstcurrbal = new ArrayList();
        ArrayList sgstcurrbaltype = new ArrayList();
        ArrayList updatesgstcurrbal = new ArrayList();
        ArrayList updatesgstcurrbaltype = new ArrayList();

        ArrayList cgstcurrbal = new ArrayList();
        ArrayList cgstcurrbaltype = new ArrayList();
        ArrayList updatecgstcurrbal = new ArrayList();
        ArrayList updatecgstcurrbaltype = new ArrayList();

        ArrayList salesaccount = new ArrayList();
        ArrayList Salesaccountcurrbal = new ArrayList();
        ArrayList Salesaccountcurrbaltype = new ArrayList();

        ArrayList Salesaccountupdatecurrbal = new ArrayList();
        ArrayList Salesaccountupdatecurrbaltype = new ArrayList();

        double vatamnt = 0;
        String igstamnt1_1;
        double sgstperc = 0;
        double sgstamnt = 0;
        String sgstamnt1_1;
        double cgstperc = 0;
        double cgstamnt = 0;
        String cgstamnt1_1;
        try {
            String paytype = "";
            connection c = new connection();
            Connection connect = c.cone();
            Statement st1 = connect.createStatement();
            ResultSet rs = st1.executeQuery("select * from billing where bill_refrence='" + jTable1.getValueAt(index, 1) + "'");
            while (rs.next()) {
                paytype = rs.getString(3);
                cashamount = Double.parseDouble(rs.getString("cashamount"));
                cashaccount = rs.getString("cashaccount");
                debtoramount = Double.parseDouble(rs.getString("sundrydebtorsamount"));
                debtoraccount = rs.getString("sundrydebtorsaccount");
                cardamount = Double.parseDouble(rs.getString("cardamount"));
                cardupdateaccount = rs.getString("cardaccount");
                ismeterupdate = rs.getInt("ismeter");
                if (rs.getString("Round_OFf_Value").equals("null") || rs.getString("Round_OFf_Value").equals("") || rs.getString("Round_OFf_Value") == null) {
                    round1 = 0;
                } else {
                    round1 = Double.parseDouble(rs.getString("Round_OFf_Value"));
                }

                Party_ledger = rs.getString("by_ledger");
                reducingtax = rs.getDouble("total_value_before_tax");
                discounamntt = Double.parseDouble(rs.getString("total_disc_amnt"));

                Statement stmt3 = connect.createStatement();

                if (ismeterupdate == 0) {
                    ResultSet rsstock = stmt3.executeQuery("select * from stockid where Stock_No='" + rs.getString(7) + "'");
                    while (rsstock.next()) {
                        if (rsstock.getString(2) != null) {
                            Statement stmt4 = connect.createStatement();
                            stmt4.executeUpdate("insert into stockid2 values('" + rsstock.getString(2) + "','" + rsstock.getString(3) + "','" + rsstock.getString(4) + "','" + rsstock.getString(5) + "','" + rsstock.getString(6) + "','" + rsstock.getString(7) + "','" + rsstock.getString(8) + "','" + rsstock.getString(11) + "','" + rsstock.getString(10) + "','" + rsstock.getString(9) + "','" + rsstock.getString(13) + "','" + ismeterupdate + "','1')");
                        }
                    }
                } else if (ismeterupdate == 1) {
                    double qntstock = 0;
                    double qnt = 0;
                    double totalqnty = 0;
                    ResultSet rsstock = stmt3.executeQuery("select qnt from stockid2 where Stock_No='" + rs.getString(7) + "'");
                    while (rsstock.next()) {
                        qntstock = rsstock.getDouble(1);

                    }
                    qnt = rs.getDouble("qnty");
                    totalqnty = qnt + qntstock;

                    stmt3.executeUpdate("update stockid2 set qnt='" + totalqnty + "" + "' where stock_no='" + rs.getString(8) + "'");

                }

                {
                    ledgervat1.add(rs.getString("vataccount"));
                    Statement stmt1 = connect.createStatement();
                    Statement stmtvataccount = connect.createStatement();
                    ResultSet rsvataccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString(24) + "' ");
                    while (rsvataccount.next()) {
                        vat1.add(rsvataccount.getString(6));
                        vatcurrbal.add(rsvataccount.getString(10));
                        vatcurrbaltype.add(rsvataccount.getString(11));

                        vatamnt = rs.getDouble("vatamnt");

                        DecimalFormat idf = new DecimalFormat("0.00");
                        igstamnt1_1 = idf.format(vatamnt);

                        String vatcurrbalance = (String) vatcurrbal.get(0);
                        String Curbaltypevat = (String) vatcurrbaltype.get(0);
                        double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                        double vatamnt1 = vatamnt;
                        double result;

                        if (Curbaltypevat.equals("CR")) {
                            result = vatcurrbalance1 - vatamnt1;
                            if (result >= 0) {
                                updatevatcurrbaltype.add(0, "CR");
                                updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                            } else {
                                updatevatcurrbaltype.add(0, "DR");
                                updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }
                        } else if (Curbaltypevat.equals("DR")) {

                            result = vatcurrbalance1 + vatamnt1;

                            updatevatcurrbaltype.add(0, "DR");
                            updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                        }

                        if (Integer.parseInt(vat1.get(0) + "") != 0) {
                            DecimalFormat upvat = new DecimalFormat("0.00");
                            String updavat = upvat.format(Double.parseDouble(updatevatcurrbal.get(0).toString()));
                            stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updavat + "" + "',currbal_type='" + updatevatcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgervat1.get(0) + "" + "'");
                        }
                        vat1.clear();
                        vatcurrbal.clear();
                        vatcurrbaltype.clear();
                        updatevatcurrbal.clear();
                        updatevatcurrbaltype.clear();
                    }
                }

// For SGST calculation
                {
                    ledgersgst1.add(rs.getString("Sgstaccount"));
                    Statement stmt_sgst = connect.createStatement();
                    Statement stmtsgstaccount = connect.createStatement();
                    ResultSet rssgstaccount = stmtsgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("Sgstaccount") + "' ");
                    while (rssgstaccount.next()) {
                        sgst1.add(rssgstaccount.getString(6));
                        sgstcurrbal.add(rssgstaccount.getString(10));
                        sgstcurrbaltype.add(rssgstaccount.getString(11));

                        sgstperc = Double.parseDouble((String) sgst1.get(0));

                        sgstamnt = rs.getDouble("sgstamnt");

                        DecimalFormat idf = new DecimalFormat("0.00");
                        sgstamnt1_1 = idf.format(sgstamnt);

                        String sgstcurrbalance = (String) sgstcurrbal.get(0);
                        String Curbaltypesgst = (String) sgstcurrbaltype.get(0);
                        double sgstcurrbalance1 = Double.parseDouble(sgstcurrbalance);
                        double sgstamnt1 = sgstamnt;
                        double result;

                        if (Curbaltypesgst.equals("CR")) {
                            result = sgstcurrbalance1 - sgstamnt1;
                            if (result >= 0) {
                                updatesgstcurrbaltype.add(0, "CR");
                                updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            } else {
                                updatesgstcurrbaltype.add(0, "DR");
                                updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }
                        } else if (Curbaltypesgst.equals("DR")) {

                            result = sgstcurrbalance1 + sgstamnt1;

                            updatesgstcurrbaltype.add(0, "DR");
                            updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                        }

                        if (Double.parseDouble(sgst1.get(0) + "") != 0) {
                            DecimalFormat upsgst = new DecimalFormat("0.00");
                            String updasgst = upsgst.format(Double.parseDouble(updatesgstcurrbal.get(0).toString()));
                            stmt_sgst.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgersgst1.get(0) + "" + "'");
                        }

                        sgst1.clear();
                        sgstcurrbal.clear();
                        sgstcurrbaltype.clear();
                        updatesgstcurrbal.clear();
                        updatesgstcurrbaltype.clear();

                    }
                }

// For CGST calculation
                {
                    ledgercgst1.add(rs.getString("Cgstaccount"));
                    Statement stmt_cgst = connect.createStatement();
                    Statement stmtcgstaccount = connect.createStatement();
                    ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("Cgstaccount") + "' ");
                    while (rscgstaccount.next()) {
                        cgst1.add(rscgstaccount.getString(6));
                        cgstcurrbal.add(rscgstaccount.getString(10));
                        cgstcurrbaltype.add(rscgstaccount.getString(11));

                        cgstamnt = rs.getDouble("cgstamnt");

                        DecimalFormat idf = new DecimalFormat("0.00");
                        cgstamnt1_1 = idf.format(cgstamnt);

                        String cgstcurrbalance = (String) cgstcurrbal.get(0);
                        String Curbaltypecgst = (String) cgstcurrbaltype.get(0);
                        double cgstcurrbalance1 = Double.parseDouble(cgstcurrbalance);
                        double cgstamnt1 = cgstamnt;
                        double result;

                        if (Curbaltypecgst.equals("CR")) {
                            result = cgstcurrbalance1 - cgstamnt1;
                            if (result >= 0) {
                                updatecgstcurrbaltype.add(0, "CR");
                                updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            } else {
                                updatecgstcurrbaltype.add(0, "DR");
                                updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }
                        } else if (Curbaltypecgst.equals("DR")) {

                            result = cgstcurrbalance1 + cgstamnt1;

                            updatecgstcurrbaltype.add(0, "DR");
                            updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                        }

                        if (Double.parseDouble(cgst1.get(0) + "") != 0) {

                            DecimalFormat upcgst = new DecimalFormat("0.00");
                            String updacgst = upcgst.format(Double.parseDouble(updatecgstcurrbal.get(0).toString()));
                            stmt_cgst.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgercgst1.get(0) + "" + "'");
                        }

                        cgst1.clear();
                        cgstcurrbal.clear();
                        cgstcurrbaltype.clear();
                        updatecgstcurrbal.clear();
                        updatecgstcurrbaltype.clear();
                    }
                }
            }
            Statement Salesaccount = connect.createStatement();

            ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + Party_ledger + "' ");
            while (rssales.next()) {
                Salesaccountcurrbal.add(rssales.getString(10));
                Salesaccountcurrbaltype.add(rssales.getString(11));
            }

            double totalamntreducingvat = reducingtax;

            String salesaccountamnt = (String) Salesaccountcurrbal.get(0);
            double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
            double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);

            if (Salesaccountcurrbaltype.get(0).equals("CR")) {
                if (salesaccountupdatedamount > 0) {
                    Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                    Salesaccountupdatecurrbaltype.add("CR");
                }
                if (salesaccountupdatedamount <= 0) {
                    Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                    Salesaccountupdatecurrbaltype.add("DR");
                }
            } else if (Salesaccountcurrbaltype.get(0).equals("DR")) {
                salesaccountupdatedamount = salesaccountamnt1 + totalamntreducingvat;
                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                Salesaccountupdatecurrbaltype.add("DR");
            }

            Statement stmt2 = connect.createStatement();
            DecimalFormat upsales = new DecimalFormat("0.00");
            String updasales = upsales.format(Double.parseDouble(Salesaccountupdatecurrbal.get(0).toString()));
            stmt2.executeUpdate("UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + Party_ledger + "'");

            Salesaccountcurrbal.clear();
            Salesaccountcurrbaltype.clear();
            Salesaccountupdatecurrbal.clear();
            Salesaccountupdatecurrbaltype.clear();

            if (cashamount != 0) {

                String c2 = "CASH";
                Statement Cashaccount = connect.createStatement();
                ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='" + cashaccount + "'");
                while (rscashaccount.next()) {
                    old_cashaccountcurrbal.add(rscashaccount.getString(10));
                    old_cashaccountcurrbaltype.add(rscashaccount.getString(11));
                }

                String cashaccountamount = (String) old_cashaccountcurrbal.get(0);
                double totalamnt = cashamount;
                double cashaccountamount1 = Double.parseDouble(cashaccountamount);

                if (old_cashaccountcurrbaltype.get(0).equals("DR")) {
                    double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                    if (cashaccountupdateamount > 0) {
                        old_cashaccountupdatecurrbaltype.add("DR");
                        old_cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                    if (cashaccountupdateamount <= 0) {
                        old_cashaccountupdatecurrbaltype.add("CR");
                        old_cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                }
                if (old_cashaccountcurrbaltype.get(0).equals("CR")) {
                    double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                    old_cashaccountupdatecurrbaltype.add("CR");
                    old_cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                }
                Statement stmt1 = connect.createStatement();
                DecimalFormat dfcash = new DecimalFormat("0.00");
                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfcash.format(old_cashaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + old_cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + c2 + "'");

//                    double disccurrbal = 0;
//                    String disccurrbaltype = "";
//                    try {
//
////                      Statement statement = connect.createStatement();
//                        Statement statement1 = connect.createStatement();
////                      Statement statement2 = connect.createStatement();
//                        Statement statement3 = connect.createStatement();
//
//                        ResultSet rs1 = statement1.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='DISCOUNT ACCOUNT'");
//
//                        while (rs1.next()) {
//                            disccurrbal = Double.parseDouble(rs1.getString(1));
//                            disccurrbaltype = rs1.getString(2);
//                        }
//
//                        if (disccurrbaltype.equals("DR")) {
//                            disccurrbal = disccurrbal - discounamntt;
//                            if (disccurrbal < 0) {
//                                disccurrbaltype = "CR";
//                                disccurrbal = Math.abs(disccurrbal);
//                            }
//                        } else if (disccurrbaltype.equals("CR")) {
//                            disccurrbal = disccurrbal + discounamntt;
//                        }
//
//                        disccurrbal = Math.abs(disccurrbal);
//                        statement3.executeUpdate("update ledger set curr_bal='" + disccurrbal + "',currbal_type='" + disccurrbaltype + "' where ledger_name = 'DISCOUNT ACCOUNT'");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
            }

            if (debtoramount != 0) {
                Statement sundrydebtorsaccount = connect.createStatement();
                ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + debtoraccount + "'");
                while (rssundrydebtors.next()) {
                    old_sundrydebtorsaccountcurrbal.add(rssundrydebtors.getString("curr_bal"));
                    old_sundrydebtorsaccountcurrbaltype.add(rssundrydebtors.getString("currbal_type"));
                }

                String sundrydebtorsamnt = (String) old_sundrydebtorsaccountcurrbal.get(0);
                double totalamnt = debtoramount;
                double sundrydebtorsamnt1 = Double.parseDouble(sundrydebtorsamnt);
                if (old_sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                    double sundrydebtorsccountupdateamount = sundrydebtorsamnt1 - totalamnt;

                    if (sundrydebtorsccountupdateamount > 0) {
                        old_sundrydebtorsaccountupdatecurrbaltype.add(0, "DR");
                        old_sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                    }
                    if (sundrydebtorsccountupdateamount <= 0) {
                        old_sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                        old_sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                    }
                }
                if (old_sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                    double cashaccountupdateamount = sundrydebtorsamnt1 + totalamnt;
                    old_sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                    old_sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(cashaccountupdateamount));
                }

                DecimalFormat dfsundry = new DecimalFormat("0.00");
                sundrydebtorsaccount.executeUpdate("UPDATE ledger SET curr_bal= '" + (dfsundry.format(old_sundrydebtorsaccountupdatecurrbal.get(0)) + "") + "', currbal_type = '" + (old_sundrydebtorsaccountupdatecurrbaltype.get(0) + "") + "' where ledger_name='" + debtoraccount + "'");

            }

            if (cardamount != 0) {
                Statement cardaccount = connect.createStatement();
                ResultSet rscard = cardaccount.executeQuery("select * from ledger where  ledger_name='" + cardupdateaccount + "'");
                while (rscard.next()) {
                    old_cardaccountcurrbal.add(rscard.getString(10));
                    old_cardaccountcurrbaltype.add(rscard.getString(11));
                }

                String cardamnt = (String) old_cardaccountcurrbal.get(0);
                double totalamnt = cardamount;
                double cardamnt1 = Double.parseDouble(cardamnt);
                if (old_cardaccountcurrbaltype.get(0).equals("DR")) {
                    double cardaccountupdateamount = cardamnt1 - totalamnt;

                    if (cardaccountupdateamount > 0) {
                        old_cardaccountupdatecurrbaltype.add(0, "DR");
                        old_cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                    }
                    if (cardaccountupdateamount <= 0) {
                        old_cardaccountupdatecurrbaltype.add(0, "CR");
                        old_cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                    }
                }
                if (old_cardaccountcurrbaltype.get(0).equals("CR")) {
                    double cardaccountupdateamount = cardamnt1 + totalamnt;
                    old_cardaccountupdatecurrbaltype.add(0, "CR");
                    old_cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                }
                DecimalFormat dfsundry = new DecimalFormat("0.00");
                cardaccount.executeUpdate("UPDATE ledger SET curr_bal='" + dfsundry.format(old_cardaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + old_cardaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + cardupdateaccount + "'");
            }

// Calculation for Round off
            String Round_currbal_type1 = "";
            double diff_total1 = 0;
            String diff_total_round1 = "";
            Statement stru = connect.createStatement();
            Statement round_st = connect.createStatement();
            ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
            while (round_rs.next()) {
                double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                Round_currbal_type1 = round_rs.getString("currbal_type");
                double round_total = round1;

                if (Round_currbal_type1.equals("DR") && round_total < 0) {
                    diff_total1 = Round_curr_bal - round_total;
                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }
                } else if (Round_currbal_type1.equals("DR") && round_total > 0) {
                    diff_total1 = Round_curr_bal + round_total;
                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }
                }

                if (Round_currbal_type1.equals("CR") && round_total < 0) {
                    diff_total1 = Round_curr_bal + round_total;

                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }

                } else if (Round_currbal_type1.equals("CR") && round_total > 0) {
                    diff_total1 = Round_curr_bal - round_total;

                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }
                }
                DecimalFormat dr1 = new DecimalFormat("0.00");
                diff_total_round1 = dr1.format(diff_total1);
            }

            //Updating round off ledger in ledger table   
            stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round1 + "', currbal_type = '" + Round_currbal_type1 + "' where ledger_name = 'Round Off'");

            Statement stmt3 = connect.createStatement();
            Statement stmt4 = connect.createStatement();
            Statement stmt5 = connect.createStatement();
            String insertbillingupdate = "\"insert into billing1 select * from billing where bill_refrence='" + jTable1.getValueAt(index, 1) + "'\"";
            stmt4.executeUpdate("insert into billing1 select * from billing where bill_refrence='" + jTable1.getValueAt(index, 1) + "'");
            stmt5.executeUpdate("insert into queries values(" + insertbillingupdate + ")");

            stmt3.executeUpdate("delete from billing where bill_refrence='" + jTable1.getValueAt(index, 1) + "'");

            Salesaccountcurrbal.clear();
            Salesaccountupdatecurrbal.clear();
            Salesaccountcurrbaltype.clear();
            Salesaccountupdatecurrbaltype.clear();
            salesaccount.clear();
            old_cashaccountcurrbal.clear();
            old_cashaccountupdatecurrbal.clear();
            old_cashaccountcurrbaltype.clear();
            old_cashaccountupdatecurrbaltype.clear();
            old_sundrydebtorsaccountcurrbal.clear();
            old_sundrydebtorsaccountcurrbaltype.clear();
            old_sundrydebtorsaccountupdatecurrbal.clear();
            old_sundrydebtorsaccountupdatecurrbaltype.clear();
            old_cardaccountcurrbal.clear();
            old_cardaccountupdatecurrbal.clear();
            old_cardaccountcurrbaltype.clear();
            old_cardaccountupdatecurrbaltype.clear();

            dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jDialog1.setSize(new java.awt.Dimension(298, 162));

        jLabel1.setText("Enter Date : ");

        jDateChooser1.setDateFormatString("yyy-MM-dd");

        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jButton1))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(34, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(58, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Bill Update");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill Number", "Total Qnty", "Total Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jDialog1.dispose();
        getData();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        int key = evt.getKeyCode();
        if (key == KeyEvent.VK_F3) {
            jDialog1.setVisible(true);
        }
    }//GEN-LAST:event_formKeyPressed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        transaction.New_Dual_Billing b1 = new transaction.New_Dual_Billing();
        b1.setVisible(true);
        row = jTable1.getSelectedRow();
        String type = null;
        String pn = "";
        String party_code = "";
        String sr_no = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
             Statement st_staff = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  * FROM Billing where bill_refrence='" + (jTable1.getValueAt(row, 1) + "") + "' ");
            while (rs.next()) {
                pn = rs.getString("customer_name");
                party_code = rs.getString("customer_account");

                b1.jTextField1.setText(rs.getString(1));
                b1.jDateChooser1.setDate(formatter.parse(rs.getString(2)));
                type = rs.getString(3);
                b1.jTextField3.setText(rs.getString(4));
                b1.jTextField14.setText(rs.getString(5));

                DefaultTableModel dtm = (DefaultTableModel) b1.jTable1.getModel();
                b1.sno = Integer.parseInt(rs.getString(6)) + 1;
                b1.jTextField14.setText(b1.sno + "");
                Object o[] = {rs.getString("sno"), rs.getString("stock_no"), rs.getString("description"), rs.getString("rate"), rs.getString("qnty"), rs.getString("value"), rs.getString("disc_Code"), rs.getString("disc%"), rs.getString("disc_amnt"), rs.getString("total"), rs.getString("staff")};
                dtm.addRow(o);         
                b1.jTextField15.setText(rs.getString(17));
                b1.jTextField16.setText(rs.getString(18));
                b1.jTextField17.setText(rs.getString(19));
                b1.jTextField18.setText(rs.getString(20));
                b1.jTextField19.setText(rs.getString(21));
                b1.jTextField20.setText(rs.getString(22));
                b1.jTextField19.setText(rs.getString("total_value_before_tax"));
                b1.jTextField30.setText(rs.getString("total_igst_taxamnt"));
                b1.jTextField31.setText(rs.getString("total_sgst_taxamnt"));
                b1.jTextField32.setText(rs.getString("total_cgst_taxamnt"));
                b1.jTextField38.setText(rs.getString("reference"));
                if (rs.getString("mobile_no").equals(null) || rs.getString("mobile_no").equals("null")) {
                    b1.jTextField37.setText("");
                } else {
                    b1.jTextField37.setText(rs.getString("mobile_no"));
                }

                if (rs.getString("Round_Off_Value").equals(null) || rs.getString("Round_Off_Value").equals("null")) {
                    b1.jTextField33.setText("0");
                } else {
                    b1.jTextField33.setText(rs.getString("Round_Off_Value"));
                }

                b1.tiv = rs.getDouble("total_igst_taxamnt");
                b1.tsv = rs.getDouble("total_sgst_taxamnt");
                b1.tcv = rs.getDouble("total_cgst_taxamnt");
                b1.iv = rs.getDouble("vatamnt");
                b1.sv = rs.getDouble("Sgstamnt");
                b1.cv = rs.getDouble("Sgstamnt");
                b1.sundrydebtors = rs.getString("sundrydebtorsaccount");

                b1.totalamount = Double.parseDouble(rs.getString(17));
                b1.totalquantity = Double.parseDouble(rs.getString(18));
                b1.totaldiscount = Double.parseDouble(rs.getString(19));
                b1.totalamountbeforetax = Double.parseDouble(rs.getString(21));
                b1.totalamountaftertax = Double.parseDouble(rs.getString(22));
            }

            ResultSet staff_rs = st_staff.executeQuery("SELECT  * FROM staff");
            while (staff_rs.next()) {
                b1.jComboBox5.addItem(staff_rs.getString("Emp_code"));
            }
            
            ResultSet sr_rs = st.executeQuery("SELECT Sr_Ref from billing where bill_refrence='" + jTable1.getValueAt(row, 1) + "" + "' group by bill_refrence ");
            while (sr_rs.next()) {
                sr_no = sr_rs.getString("SR_Ref");
                b1.jComboBox3.addItem(sr_no);
                b1.jComboBox3.setSelectedItem(sr_no);
            }

            ResultSet rs1 = st1.executeQuery("SELECT party_name, IGST FROM party_sales where party_name = '" + pn + "'");
            while (rs1.next()) {
                if (rs1.getBoolean("IGST") == true) {
                    b1.isIgstApplicable = true;
                } else if (rs1.getBoolean("IGST") == false) {
                    b1.isIgstApplicable = false;
                }
            }

            if (type.equals("Cash")) {
                b1.jRadioButton1.setSelected(true);
            }

            if (type.equals("Debit")) {
                b1.jRadioButton2.setSelected(true);

                if (party_code != null) {
                    b1.jLabel4.setVisible(true);
                    b1.jTextField4.setVisible(true);
                    b1.jTextField4.setText("" + party_code);
                }
            }
            
                        if (type.equals("Card payment")) {
                b1.jRadioButton5.setSelected(true);
            }
            
            
            b1.jButton1.setVisible(false);
            b1.jButton7.setVisible(true);
            b1.jButton5.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        int key = evt.getKeyCode();
        if (key == KeyEvent.VK_F3) {
            jDialog1.setVisible(true);
        } else if (key == KeyEvent.VK_DELETE) {
            int i2 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Delete");
            if (i2 == 0) {
                delete();
            }
        }
    }//GEN-LAST:event_jTable1KeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bill_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bill_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bill_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bill_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Bill_Update().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
