package transaction;

import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Aashish
 */
public class BsnlPurchase_Update extends javax.swing.JFrame {

    /**
     * Creates new form BsnlPurchase_Update
     */
      private static BsnlPurchase_Update obj = null;
    public BsnlPurchase_Update() {
        initComponents();
        getData();
    }
          public static BsnlPurchase_Update getObj() {
        if (obj == null) {
            obj = new BsnlPurchase_Update();
        }
        return obj;
    }
//----------------Method for default data---------------------------------------

    public void getData() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("Select * from bsnl_purchase group by Bill_Refrence_No order by length(Bill_Refrence_No), Bill_Refrence_No");
            while (rs.next()) {
                Object o[] = {rs.getString("Invoice_date"), rs.getString("Bill_Refrence_No"), rs.getString("Total_Qnty"), rs.getString("Total_Net_Amount")};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------ 
//----------------Method to move Data to another form---------------------------

    public void moveData() {
        Bsnl_Purchase bp = new Bsnl_Purchase();
        this.setVisible(false);
        bp.setVisible(true);
        bp.jButton1.setVisible(false);
        bp.jButton4.setVisible(true);
        DefaultTableModel bsnl_dtm = (DefaultTableModel) bp.jTable1.getModel();
        try {
            int row = jTable1.getSelectedRow();
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement sup_st = connect.createStatement();
            ResultSet rs = st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTable1.getValueAt(row, 1) + "'");
            while (rs.next()) {
                bp.jTextField1.setText("" + rs.getString("Supplier_Name"));
                bp.jTextField2.setText("" + rs.getString("Bill_Refrence_No"));
                bp.jDateChooser1.setDate(rs.getDate("Invoice_Date"));
                bp.jDateChooser2.setDate(rs.getDate("Party_Purchase_Date"));
                bp.jTextField3.setText("" + rs.getString("Purchase_Refrence_No"));
                bp.jLabel26.setText("" + rs.getString("to_ledger"));
                if (rs.getString("Purchase_Type").equals("Credit")) {
                    bp.jRadioButton2.setSelected(true);
                    bp.jLabel24.setText("" + rs.getString("Supplier_Id"));
                    ResultSet sup_rs = sup_st.executeQuery("select * from ledger where ledger_name='" + rs.getString("Supplier_Name") + "'");
                    while (sup_rs.next()) {
                        bp.jLabel27.setText("" + sup_rs.getString("curr_bal"));
                        bp.jLabel28.setText("" + sup_rs.getString("currbal_type"));

                        if (sup_rs.getBoolean("Gst_Applicable") == true) {
                            bp.isIgstApplicable = true;
                            bp.jTextField16.setEnabled(true);
                            bp.jTextField17.setEnabled(true);
                        } else if (sup_rs.getBoolean("Gst_Applicable") == false) {
                            bp.isIgstApplicable = false;
                            bp.jTextField12.setEnabled(true);
                            bp.jTextField13.setEnabled(true);
                            bp.jTextField14.setEnabled(true);
                            bp.jTextField15.setEnabled(true);
                        }
                    }
                } else {
                    bp.jRadioButton1.setSelected(true);
                }
                bp.Sno = rs.getString("Sno");
                int s_no = Integer.parseInt(bp.Sno) + 1;
                bp.jTextField4.setText("" + s_no);
                Object o[] = {rs.getString("Sno"), rs.getString("Qnty"), rs.getString("Product"), rs.getString("Gross_Amount"), rs.getString("Basic_Price")};
                bsnl_dtm.addRow(o);

                bp.jTextField8.setText("" + rs.getString("Total_Basic_Price"));
                bp.jTextField9.setText("" + rs.getString("Total_Qnty"));
                bp.jTextField10.setText("" + rs.getString("Total_Discount"));
                bp.jTextField11.setText("" + rs.getString("Total_Amnt_After_Disc"));
                bp.jTextField13.setText("" + rs.getString("Total_Cgst_Amount"));
                bp.jTextField15.setText("" + rs.getString("Total_Sgst_Amount"));
                bp.jTextField17.setText("" + rs.getString("Total_Igst_Amount"));
                bp.jTextField18.setText("" + rs.getString("Other_Charges"));
                bp.jTextField19.setText("" + rs.getString("Total_Net_Amount"));
                bp.jTextField21.setText("" + rs.getString("Round_Off"));

                String iacc = rs.getString("Igst_Account");
                bp.Old_Party_Igst = iacc;
                if (!iacc.equals("0")) {
                    bp.jTextField16.setText("" + iacc.substring(4, iacc.length() - 1));
                }
                String sacc = rs.getString("Sgst_Account");
                bp.Old_Party_sgst = sacc;
                if (!sacc.equals("0")) {
                    bp.jTextField14.setText("" + sacc.substring(4, sacc.length() - 1));
                }
                String cacc = rs.getString("Cgst_Account");
                bp.Old_Party_cgst = cacc;
                if (!cacc.equals("0")) {
                    bp.jTextField12.setText("" + cacc.substring(4, cacc.length() - 1));
                }

                bp.Old_other_charges = rs.getString("Other_Account");
                bp.sundrycreditorold = rs.getString("Supplier_Name");
                bp.Purchaseaccount = rs.getString("to_ledger");
                bp.total_basic_amount = rs.getDouble("Total_Basic_Price");
                bp.total_gross_amount = rs.getDouble("Total_Gross_Amount");
                bp.total_net_amount = rs.getDouble("Total_Net_Amount");
                bp.rev_igst_value = rs.getDouble("Total_Igst_Amount");
                bp.rev_sgst_value = rs.getDouble("Total_Sgst_Amount");
                bp.rev_cgst_value = rs.getDouble("Total_Cgst_Amount");
                bp.Qnty = rs.getDouble("Total_Qnty");
                bp.tiv = rs.getDouble("Total_Igst_Amount");
                bp.tcv = rs.getDouble("Total_Cgst_Amount");
                bp.tsv = rs.getDouble("Total_Sgst_Amount");
                bp.trv = rs.getDouble("Round_Off");
                bp.iv = rs.getDouble("Igst_Amount");
                bp.cv = rs.getDouble("Cgst_Amount");
                bp.sv = rs.getDouble("Sgst_Amount");

                bp.jRadioButton1.setEnabled(true);
                bp.jRadioButton2.setEnabled(true);
                bp.jDateChooser2.setEnabled(true);
                bp.jTextField3.setEnabled(true);
                bp.jTextField4.setEnabled(true);
                bp.jTextField5.setEnabled(true);
                bp.jTextField6.setEnabled(true);
                bp.jTextField7.setEnabled(true);
                bp.jTextField8.setEnabled(true);
                bp.jTextField9.setEnabled(true);
                bp.jTextField10.setEnabled(true);
                bp.jTextField11.setEnabled(true);
                bp.jTextField18.setEnabled(true);
                bp.jTextField19.setEnabled(true);
                bp.jTextField21.setEnabled(true);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------ 

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("BSNL PURCHASE UPDATE");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill Ref", "Total Qnaty", "Total Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setToolTipText("Bsnl Purchase Update");
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        moveData();
    }//GEN-LAST:event_jTable1MouseClicked

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BsnlPurchase_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BsnlPurchase_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BsnlPurchase_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BsnlPurchase_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BsnlPurchase_Update().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
