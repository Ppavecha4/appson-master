package transaction;

import connection.connection;
import java.awt.event.KeyEvent;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Aashish
 */
public class Bsnl_Billing_GST extends javax.swing.JFrame {

    /**
     * Creates new form Bsnl_Billing_GST
     */
//------------------------------------------------------------------------------
// Setting Date and Time    
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    Calendar currentDate1 = Calendar.getInstance();
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
    String dateNow1 = formatter1.format(currentDate1.getTime());
    Calendar currentDate2 = Calendar.getInstance();
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyy");
    String dateNow2 = formatter2.format(currentDate2.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    DecimalFormat df = new DecimalFormat("0.00");
     private static Bsnl_Billing_GST obj = null;
     String Year;
    //-----------------------------------------------------------------------------   

    public Bsnl_Billing_GST() {
        initComponents();
        this.setLocationRelativeTo(null);
        genrate_BillNo();
        fillCombo();
        jComboBox1.requestFocus();
        jButton6.setVisible(false);
        jRadioButton1.setSelected(true);
        jLabel3.setVisible(false);
        jTextField2.setVisible(false);
        jDialog1.setLocationRelativeTo(null);
        jDialog2.setLocationRelativeTo(null);
    }
    
      public static Bsnl_Billing_GST getObj() {
        if (obj == null) {
            obj = new Bsnl_Billing_GST();
        }
        return obj;
    }
//---------------Declaring Gloable Variable-------------------------------------
    String sundrydebtors = null;
    String to_ledger = null;
    String by_ledger = null;
    String paytype = null;
    public boolean isIgstapplicable = false;
    public int sno = 1;
    double Quntity = 0;
    double Gross_Amount = 0;
    double Net_Amount = 0;
    double Basic_Price = 0;
    double Igst_percent = 0;
    double Sgst_percent = 0;
    double Cgst_percent = 0;
    double Amnt_before_tax = 0;
    double Igst_Value = 0;
    double Sgst_Value = 0;
    double Cgst_Value = 0;
    double Round_off = 0;
    double total_igst_amnt = 0;
    double total_sgst_amnt = 0;
    double total_cgst_amnt = 0;
    String Round_currbal_type = "";
    double diff_total = 0;
    String diff_total_round = "";
    String old_igst_account = "";
    String old_sgst_account = "";
    String old_cgst_account = "";
//------------------------------------------------------------------------------
//-------------Declaring ArrayList----------------------------------------------
    ArrayList sundrydebtorsaccountcurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbaltype = new ArrayList();
    ArrayList sundrydebtorsaccountcurrbaltype = new ArrayList();
    ArrayList salesaccount = new ArrayList();
    ArrayList Salesaccountcurrbal = new ArrayList();
    ArrayList Salesaccountcurrbaltype = new ArrayList();
    ArrayList Salesaccountupdatecurrbal = new ArrayList();
    ArrayList Salesaccountupdatecurrbaltype = new ArrayList();
    ArrayList cashaccountcurrbal = new ArrayList();
    ArrayList cashaccountupdatecurrbal = new ArrayList();
    ArrayList cashaccountupdatecurrbaltype = new ArrayList();
    ArrayList cashaccountcurrbaltype = new ArrayList();
    ArrayList ledger_igst = new ArrayList();
    ArrayList updateigstcurrbal = new ArrayList();
    ArrayList updateigstcurrbaltype = new ArrayList();
    ArrayList igst_currbal = new ArrayList();
    ArrayList igst_currbal_type = new ArrayList();
    ArrayList ledger_sgst = new ArrayList();
    ArrayList updatesgstcurrbal = new ArrayList();
    ArrayList updatesgstcurrbaltype = new ArrayList();
    ArrayList sgst_currbal = new ArrayList();
    ArrayList sgst_currbal_type = new ArrayList();
    ArrayList ledger_cgst = new ArrayList();
    ArrayList updatecgstcurrbal = new ArrayList();
    ArrayList updatecgstcurrbaltype = new ArrayList();
    ArrayList cgst_currbal = new ArrayList();
    ArrayList cgst_currbal_type = new ArrayList();

    ArrayList snumber = new ArrayList();
    ArrayList total = new ArrayList();
    ArrayList product = new ArrayList();
    ArrayList qnty = new ArrayList();
    ArrayList Gross_amnt = new ArrayList();
    ArrayList Basic_amnt = new ArrayList();
    ArrayList Net_amnt = new ArrayList();
    ArrayList Ivalue = new ArrayList();
    ArrayList Svalue = new ArrayList();
    ArrayList Cvalue = new ArrayList();

//------------------------------------------------------------------------------    
//---------Genrating Bsnl_ Purchase Bill No and Date----------------------------
    public void genrate_BillNo() {
        String purchase = "BPR/";
        String slash = "/";
        String yearwise = Year.concat(slash);
//        String yearwise = (getyear.concat(nextyear)).concat(slash);
        String finalconcat = (purchase.concat(yearwise));
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement Bill_No_st = connect.createStatement();
            ResultSet Bill_No_rs = Bill_No_st.executeQuery("SELECT Bill_Refrence_No FROM bsnl_billing_gst ");
            String no = null;
            while (Bill_No_rs.next()) {
                no = Bill_No_rs.getString(1);
            }
            if (no == null) {
                String zero = "0";
                no = finalconcat.concat(zero);
            }
            String[] Part1 = no.split("/");
            int a1 = Integer.parseInt(Part1[2]);
            String s = finalconcat.concat(Integer.toString(a1 + 1));
            jTextField1.setText("" + s);
            jDateChooser1.setDate(formatter.parse(dateNow));
        } catch (Exception e) {
            System.out.println("Connection Faild");
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------
//--------Fill Combobox--------------------------------------------------------- 

    public void fillCombo() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement Combo_st = connect.createStatement();
            ResultSet Combo_rs = Combo_st.executeQuery("SELECT product_code FROM product");
            while (Combo_rs.next()) {
                String name = Combo_rs.getString("product_code");
                jComboBox1.addItem(name);
            }
            ResultSet rs = Combo_st.executeQuery("SELECT * FROM customer_details");
            while (rs.next()) {
                String name = rs.getString("Customer_name") + "/" + rs.getString("Mobile_no");
                jComboBox2.addItem(name);
                AutoCompleteDecorator.decorate(jComboBox2);
                jTextField3.requestFocus();
            }
          ResultSet rs_year = Combo_st.executeQuery("Select * from year");
            while(rs_year.next()){
            Year = rs_year.getString("year");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//--------------Save Cutomer Info-----------------------------------------------

    public void saveCustInfo() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement cust_info = connect.createStatement();
            cust_info.executeUpdate("Insert into customer_details values ('" + jTextField17.getText() + "','" + jTextField18.getText() + "')");
            jComboBox2.setSelectedItem(jTextField17.getText());
            jTextField3.setText("" + jTextField18.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//--------Select Party----------------------------------------------------------

    public void partyselect() {
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        int row = jTable2.getSelectedRow();
        sundrydebtors = (String) jTable2.getValueAt(row, 1);
        jTextField2.setText(jTable2.getValueAt(row, 0) + "");
        jComboBox2.setSelectedItem(jTable2.getValueAt(row, 1) + "/" + jTable2.getValueAt(row, 2) + "");
//        jTextField3.setText(jTable2.getValueAt(row, 2) + "");

        jDialog1.setVisible(false);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + sundrydebtors + "'");
            while (rs.next()) {
                sundrydebtorsaccountcurrbal.add(rs.getString(10));
                sundrydebtorsaccountcurrbaltype.add(rs.getString(11));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jDialog1.dispose();
        jTextField5.requestFocus();

        if (dtm.getValueAt(row, 7).toString().equals("False")) {
            isIgstapplicable = false;
        }
        if (dtm.getValueAt(row, 7).toString().equals("True")) {
            isIgstapplicable = true;
        }

    }
//------------------------------------------------------------------------------
//-----------------Calculate and Display Data-----------------------------------

    public void displayData() {
//Calulating amount before tax or basic price        
        try {
            String pro_igst = "";
            String pro_sgst = "";
            String pro_cgst = "";
            connection c = new connection();
            Connection connect = c.cone();
            Statement pro_st = connect.createStatement();
            ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox1.getSelectedItem() + "'");
            while (pro_rs.next()) {
                pro_igst = pro_rs.getString("IGST");
                pro_sgst = pro_rs.getString("SGST");
                pro_cgst = pro_rs.getString("CGST");
            }
            if (isIgstapplicable == true) {
                ResultSet igst_rs = pro_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_igst + "'");
                while (igst_rs.next()) {
                    Igst_percent = igst_rs.getDouble("percent");
                    double value = Double.parseDouble(jTextField8.getText());
                    Amnt_before_tax = (value / (100 + Igst_percent)) * 100;
                    Igst_Value = (Amnt_before_tax * Igst_percent) / 100;
                }
                jTextField7.setText("" + df.format(Amnt_before_tax));
            } else if (isIgstapplicable == false) {
                // For Sgst Percentage           
                ResultSet sgst_rs = pro_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_sgst + "'");
                while (sgst_rs.next()) {
                    Sgst_percent = sgst_rs.getDouble("percent");
                }
                // For Cgst Percentage
                ResultSet cgst_rs = pro_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_cgst + "'");
                while (cgst_rs.next()) {
                    Cgst_percent = cgst_rs.getDouble("percent");
                }
                double total_percent = Sgst_percent + Cgst_percent;
                double value = Double.parseDouble(jTextField8.getText());
                Amnt_before_tax = (value / (100 + total_percent)) * 100;

                Sgst_Value = (Amnt_before_tax * Sgst_percent) / 100;
                Cgst_Value = (Amnt_before_tax * Cgst_percent) / 100;

                jTextField7.setText("" + df.format(Amnt_before_tax));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Object o[] = {sno, (String) jComboBox1.getSelectedItem(), jTextField5.getText(), jTextField6.getText(), jTextField7.getText(), df.format(Cgst_Value), df.format(Sgst_Value), df.format(Igst_Value), jTextField8.getText()};
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.addRow(o);
// Showing data in respective text field
        sno = Integer.parseInt(jTextField4.getText());
        sno++;
        Quntity = Quntity + Double.parseDouble(jTextField5.getText());
        Basic_Price = Basic_Price + Double.parseDouble(jTextField7.getText());
        Gross_Amount = Gross_Amount + Double.parseDouble(jTextField6.getText());
        Net_Amount = Net_Amount + Double.parseDouble(jTextField8.getText());
        Igst_Value = Igst_Value + Double.parseDouble(jTextField12.getText());
        Sgst_Value = Sgst_Value + Double.parseDouble(jTextField13.getText());
        Cgst_Value = Cgst_Value + Double.parseDouble(jTextField14.getText());

        jTextField4.setText("" + sno);
        jTextField9.setText("" + Quntity);
        jTextField10.setText("" + df.format(Basic_Price));
        jTextField11.setText("" + df.format(Gross_Amount));
        jTextField15.setText("" + df.format(Net_Amount));
        jTextField12.setText("" + df.format(Igst_Value));
        jTextField13.setText("" + df.format(Sgst_Value));
        jTextField14.setText("" + df.format(Cgst_Value));

        Round_off = Double.parseDouble(jTextField15.getText()) - (Double.parseDouble(jTextField10.getText()) + Double.parseDouble(jTextField12.getText()) + Double.parseDouble(jTextField13.getText()) + Double.parseDouble(jTextField14.getText()));
        jTextField16.setText("" + df.format(Round_off));

        jTextField5.setText(null);
        jTextField6.setText(null);
        jTextField7.setText(null);
        jTextField8.setText(null);

        jComboBox1.requestFocus();
    }
//------------------------------------------------------------------------------   
//------------For Save Method---------------------------------------------------

    public void saveData() {
        if (jRadioButton2.isSelected() && (sundrydebtors == null || sundrydebtors.equals("") || sundrydebtors.equals("null"))) {
            JOptionPane.showMessageDialog(rootPane, "Please select the Sundry debtors");
            jDialog1.setVisible(true);
        } else {
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            int rowcount = jTable1.getRowCount();

            if (rowcount == 0) {
                JOptionPane.showMessageDialog(rootPane, "Please Enter the value ");
            } else {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stmt = connect.createStatement();
                    for (int i = 0; i < rowcount; i++) {
                        snumber.add(i, jTable1.getValueAt(i, 0));
                        product.add(i, jTable1.getValueAt(i, 1));
                        qnty.add(i, jTable1.getValueAt(i, 2));
                        Gross_amnt.add(i, jTable1.getValueAt(i, 3));
                        Basic_amnt.add(i, jTable1.getValueAt(i, 4));
                        Cvalue.add(i, jTable1.getValueAt(i, 5));
                        Svalue.add(i, jTable1.getValueAt(i, 6));
                        Ivalue.add(i, jTable1.getValueAt(i, 7));
                        Net_amnt.add(i, jTable1.getValueAt(i, 8));
                        total.add(i, jTable1.getValueAt(i, 8));
                        String Date = formatter.format(jDateChooser1.getDate());

                        try {
                            Statement Salesaccount = connect.createStatement();
                            Statement Cashaccount = connect.createStatement();
                            Statement sundrydebtorsaccount = connect.createStatement();
                            ResultSet rssalesaccount = Salesaccount.executeQuery("Select * from product where product_code ='" + (String) product.get(i) + "'  ");
                            while (rssalesaccount.next()) {
                                salesaccount.add(i, rssalesaccount.getString("sales_account"));
                            }
                            ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + (String) salesaccount.get(i) + "'");
                            while (rssales.next()) {
                                Salesaccountcurrbal.add(i, rssales.getString("curr_bal"));
                                Salesaccountcurrbaltype.add(i, rssales.getString("currbal_type"));
                            }
                            ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='BSNL CASH'");
                            while (rscashaccount.next()) {
                                cashaccountcurrbal.add(i, rscashaccount.getString("curr_bal"));
                                cashaccountcurrbaltype.add(i, rscashaccount.getString("currbal_type"));
                            }
                            ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + sundrydebtors + "'");
                            while (rssundrydebtors.next()) {
                                sundrydebtorsaccountcurrbal.add(rssundrydebtors.getString("curr_bal"));
                                sundrydebtorsaccountcurrbaltype.add(rssundrydebtors.getString("currbal_type"));
                            }

                            if ((String) product.get(i) != null) {
                                if (isIgstapplicable == true) {
                                    ResultSet rsigst = stmt.executeQuery("select IGST from product where product_code='" + (String) product.get(i) + "'");
                                    while (rsigst.next()) {
                                        ledger_igst.add(i, rsigst.getString("IGST"));
                                    }
                                    Statement st_igst = connect.createStatement();
                                    ResultSet rsvataccount = st_igst.executeQuery("Select *  from ledger where ledger_name='" + (String) ledger_igst.get(i) + "'");
                                    while (rsvataccount.next()) {
                                        igst_currbal.add(i, rsvataccount.getString("curr_bal"));
                                        igst_currbal_type.add(i, rsvataccount.getString("currbal_type"));
                                        Igst_percent = rsvataccount.getDouble("percent");
                                        
                                        String igstcurrbalance = (String) igst_currbal.get(i);
                                        String Curbaltypeigst = (String) igst_currbal_type.get(i);
                                        double igstcurrbalance1 = Double.parseDouble(igstcurrbalance);
//                                        double igstamnt1 = Igst_Value;
                                       double amountbeforesgst = (Double.parseDouble((String) total.get(i)) / (100 + Igst_percent)) * 100;
                                        double igstamnt1 = (Igst_percent * amountbeforesgst) / 100;

                                        double result;
                                        if (Curbaltypeigst.equals("DR")) {
                                            result = igstcurrbalance1 - igstamnt1;
                                            if (result >= 0) {
                                                updateigstcurrbaltype.add(i, "DR");
                                                updateigstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            } else {
                                                updateigstcurrbaltype.add(i, "CR");
                                                updateigstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            }

                                        } else if (Curbaltypeigst.equals("CR")) {

                                            result = igstcurrbalance1 + igstamnt1;

                                            updateigstcurrbaltype.add(i, "CR");
                                            updateigstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                        }
                                    }
                                    String updaigst = df.format(Double.parseDouble(updateigstcurrbal.get(i).toString()));
                                    stmt.executeUpdate("UPDATE ledger SET curr_bal='" + updaigst + "" + "',currbal_type='" + updateigstcurrbaltype.get(i) + "" + "' where ledger_name='" + (String) ledger_igst.get(i) + "" + "'");

                                    ledger_sgst.add("");
                                    ledger_cgst.add("");
                                } else if (isIgstapplicable == false) {
                                    // For Sgst              
                                    ResultSet rssgst = stmt.executeQuery("select SGST from product where product_code='" + (String) product.get(i) + "'");
                                    while (rssgst.next()) {
                                        ledger_sgst.add(i, rssgst.getString(1));
                                    }
                                    Statement stmtsgstaccount = connect.createStatement();
                                    ResultSet rssgstaccount = stmtsgstaccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledger_sgst.get(i) + "'");
                                    while (rssgstaccount.next()) {
                                        sgst_currbal.add(i, rssgstaccount.getString("curr_bal"));
                                        sgst_currbal_type.add(i, rssgstaccount.getString("currbal_type"));
                                        Sgst_percent = rssgstaccount.getDouble("percent");

                                        String vatcurrbalance = (String) sgst_currbal.get(i);
                                        String Curbaltypesgst = (String) sgst_currbal_type.get(i);
                                        double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
//                                        double sgstamnt1 = Sgst_Value;
                                       double amountbeforesgst = (Double.parseDouble((String) total.get(i)) / (100 + Sgst_percent + Sgst_percent)) * 100;
                                        double sgstamnt1 = (Sgst_percent * amountbeforesgst) / 100;
                                        
                                        double result;
                                        if (Curbaltypesgst.equals("DR")) {
                                            result = vatcurrbalance1 - sgstamnt1;
                                            if (result >= 0) {
                                                updatesgstcurrbaltype.add(i, "DR");
                                                updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            } else {
                                                updatesgstcurrbaltype.add(i, "CR");
                                                updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            }

                                        } else if (Curbaltypesgst.equals("CR")) {
                                            result = vatcurrbalance1 + sgstamnt1;
                                            updatesgstcurrbaltype.add(i, "CR");
                                            updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                        }
                                    }
                                    String updasgst = df.format(Double.parseDouble(updatesgstcurrbal.get(i).toString()));
                                    stmt.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledger_sgst.get(i) + "" + "' ");

                                    // For Cgst
                                    ResultSet rscgst = stmt.executeQuery("select CGST from product where product_code='" + (String) product.get(i) + "'");
                                    while (rscgst.next()) {
                                        ledger_cgst.add(i, rscgst.getString(1));
                                    }
                                    
                                    Statement stmtcgstaccount = connect.createStatement();
                                    ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledger_cgst.get(i) + "'");
                                    while (rscgstaccount.next()) {
                                        cgst_currbal.add(i, rscgstaccount.getString("curr_bal"));
                                        cgst_currbal_type.add(i, rscgstaccount.getString("currbal_type"));
                                        Cgst_percent = rscgstaccount.getDouble("percent");

                                        String cgstcurrbalance = (String) cgst_currbal.get(i);
                                        String Curbaltypecgst = (String) cgst_currbal_type.get(i);
                                        double vatcurrbalance2 = Double.parseDouble(cgstcurrbalance);
                                        double amountbeforesgst = (Double.parseDouble((String) total.get(i)) / (100 + Cgst_percent + Cgst_percent)) * 100;
                                        double cgstamnt1 = (Cgst_percent * amountbeforesgst) / 100;
//                                        double cgstamnt1 = Cgst_Value;
                                        double result1;
                                        if (Curbaltypecgst.equals("DR")) {
                                            result1 = vatcurrbalance2 - cgstamnt1;
                                            if (result1 >= 0) {
                                                updatecgstcurrbaltype.add(i, "DR");
                                                updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));

                                            } else {
                                                updatecgstcurrbaltype.add(i, "CR");
                                                updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                            }

                                        } else if (Curbaltypecgst.equals("CR")) {
                                            result1 = vatcurrbalance2 + cgstamnt1;
                                            updatecgstcurrbaltype.add(i, "CR");
                                            updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                        }
                                    }
                                    String updacgst = df.format(Double.parseDouble(updatecgstcurrbal.get(i).toString()));
                                    stmt.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledger_cgst.get(i) + "" + "' ");

                                    ledger_igst.add("");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Updating Sales Account                         
                        String salesaccountamnt = (String) Salesaccountcurrbal.get(i);
                        double totalamntreducingvat = Double.parseDouble(Basic_amnt.get(i) + "");;

                        double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
                        double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);
                        if (Salesaccountcurrbaltype.get(i).equals("DR")) {
                            if (salesaccountupdatedamount > 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("DR");

                            }
                            if (salesaccountupdatedamount <= 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("CR");
                            }
                        } else if (Salesaccountcurrbaltype.get(i).equals("CR")) {
                            salesaccountupdatedamount = salesaccountamnt1 + totalamntreducingvat;
                            Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                            Salesaccountupdatecurrbaltype.add("CR");
                        }
                        String updatesales = df.format(Double.parseDouble(Salesaccountupdatecurrbal.get(i).toString()));
                        stmt.executeUpdate("UPDATE ledger SET curr_bal='" + updatesales + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'");

                        if (jRadioButton1.isSelected()) {
                            paytype = "BSNL CASH";
                            to_ledger = "BSNL CASH";
                            by_ledger = (String) salesaccount.get(i);
                        } else if (jRadioButton2.isSelected()) {
                            paytype = "Debit";
                            to_ledger = sundrydebtors;
                            by_ledger = (String) salesaccount.get(i);
                        }

// Inserting data into bsnl_billing_gst table
                        String name[] = jComboBox2.getSelectedItem().toString().split("/");

                        stmt.executeUpdate("Insert into bsnl_billing_gst values ('" + jTextField1.getText() + "','" + Date + "','" + paytype + "','" + name[0].toString() + "','" + jTextField2.getText() + "','" + name[1].toString() + "','" + snumber.get(i) + "','" + product.get(i) + "','" + qnty.get(i) + "','" + Gross_amnt.get(i) + "','" + Basic_amnt.get(i) + "','" + Net_amnt.get(i) + "','" + jTextField9.getText() + "','" + jTextField11.getText() + "','" + jTextField10.getText() + "','" + jTextField15.getText() + "','" + ledger_igst.get(i) + "','" + Ivalue.get(i) + "','" + ledger_sgst.get(i) + "','" + Svalue.get(i) + "','" + ledger_cgst.get(i) + "','" + Cvalue.get(i) + "','" + jTextField12.getText() + "','" + jTextField13.getText() + "','" + jTextField14.getText() + "','" + jTextField16.getText() + "','" + to_ledger + "','" + by_ledger + "','')");
                    }

// Updating Cash Account and SundryDebitors Account  Balance
                    if (jRadioButton1.isSelected()) {
                        String cashaccountamount = (String) cashaccountcurrbal.get(0);
                        double totalamnt = Double.parseDouble(jTextField15.getText());
                        double cashaccountamount1 = Double.parseDouble(cashaccountamount);

                        if (cashaccountcurrbaltype.get(0).equals("CR")) {
                            double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                            if (cashaccountupdateamount > 0) {
                                cashaccountupdatecurrbaltype.add("CR");
                                cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                            } else if (cashaccountupdateamount <= 0) {
                                cashaccountupdatecurrbaltype.add("DR");
                                cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                            }
                        }
                        if (cashaccountcurrbaltype.get(0).equals("DR")) {
                            double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                            cashaccountupdatecurrbaltype.add("DR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }

                        stmt.executeUpdate("UPDATE ledger SET curr_bal='" + df.format(cashaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='BSNL CASH'");
                    } else if (jRadioButton2.isSelected()) {
                        String sundrydebtorsamnt = (String) sundrydebtorsaccountcurrbal.get(0);
                        double totalamnt = Double.parseDouble(jTextField15.getText());
                        double sundrydebtorsamnt1 = Double.parseDouble(sundrydebtorsamnt);
                        System.out.println("code is running for sundry debtors");

                        if (sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                            System.out.println("code is running for sundry debtors CR");
                            double sundrydebtorsccountupdateamount = sundrydebtorsamnt1 - totalamnt;
                            if (sundrydebtorsccountupdateamount > 0) {
                                sundrydebtorsaccountupdatecurrbaltype.add("CR");
                                sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsccountupdateamount));
                            }
                            if (sundrydebtorsccountupdateamount <= 0) {
                                sundrydebtorsaccountupdatecurrbaltype.add("DR");
                                sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsccountupdateamount));
                            }
                        } else if (sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                            System.out.println("code is running for sundry debtors DR");
                            double cashaccountupdateamount = sundrydebtorsamnt1 + totalamnt;
                            sundrydebtorsaccountupdatecurrbaltype.add("DR");
                            sundrydebtorsaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }

                        stmt.executeUpdate("UPDATE ledger SET curr_bal='" + df.format(sundrydebtorsaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + sundrydebtors + "'");
                    }

// Updating Round Off balance in ledger table
                    Statement round_st = connect.createStatement();
                    ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
                    while (round_rs.next()) {
                        double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                        Round_currbal_type = round_rs.getString("currbal_type");
                        double round_total = Double.parseDouble(jTextField15.getText()) - (Double.parseDouble(jTextField10.getText()) + Double.parseDouble(jTextField12.getText()) + Double.parseDouble(jTextField13.getText()) + Double.parseDouble(jTextField14.getText()));

                        if (Round_currbal_type.equals("DR") && round_total < 0) {
                            diff_total = Round_curr_bal + Math.abs(round_total);
                        } else if (Round_currbal_type.equals("DR") && round_total > 0) {
                            diff_total = Round_curr_bal - Math.abs(round_total);

                            if (diff_total > 0) {
                                Round_currbal_type = "CR";
                                diff_total = (Math.abs(diff_total));
                            } else if (diff_total < 0) {
                                Round_currbal_type = "DR";
                                diff_total = (Math.abs(diff_total));
                            }
                        }

                        if (Round_currbal_type.equals("CR") && round_total < 0) {
                            diff_total = Round_curr_bal - Math.abs(round_total);

                            if (diff_total > 0) {
                                Round_currbal_type = "DR";
                                diff_total = (Math.abs(diff_total));
                            } else if (diff_total < 0) {
                                Round_currbal_type = "CR";
                                diff_total = (Math.abs(diff_total));
                            }

                        } else if (Round_currbal_type.equals("CR") && round_total > 0) {
                            diff_total = Round_curr_bal + Math.abs(round_total);
                        }
//                    }

                        DecimalFormat dr1 = new DecimalFormat("0.00");
                        diff_total_round = dr1.format(diff_total);
                    }

                    round_st.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round + "', currbal_type = '" + Round_currbal_type + "' where ledger_name = 'Round Off' ");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
//------------------------------------------------------------------------------
//--------------------For Updating balance--------------------------------------

    public void updateBalance() {
        ArrayList igst_account = new ArrayList();
        ArrayList sgst_account = new ArrayList();
        ArrayList cgst_account = new ArrayList();
        ArrayList old_igst = new ArrayList();
        ArrayList old_sgst = new ArrayList();
        ArrayList old_cgst = new ArrayList();
        ArrayList old_igst_currbal = new ArrayList();
        ArrayList old_igst_currbal_type = new ArrayList();
        ArrayList old_updateigstcurrbal = new ArrayList();
        ArrayList old_updateigstcurrbaltype = new ArrayList();
        ArrayList old_sgst_currbal = new ArrayList();
        ArrayList old_sgst_currbal_type = new ArrayList();
        ArrayList old_updatesgstcurrbal = new ArrayList();
        ArrayList old_updatesgstcurrbaltype = new ArrayList();
        ArrayList old_cgst_currbal = new ArrayList();
        ArrayList old_cgst_currbal_type = new ArrayList();
        ArrayList old_updatecgstcurrbal = new ArrayList();
        ArrayList old_updatecgstcurrbaltype = new ArrayList();

        String Party_ledger = "";
        double reducingtax = 0;
        String Type = null;
        double c1 = 0;
        double round1 = 0;

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement up_st = connect.createStatement();
            ResultSet up_rs = up_st.executeQuery("Select * from bsnl_billing_gst where Bill_Refrence_No = '" + jTextField1.getText() + "'");
            while (up_rs.next()) {
                c1 = up_rs.getDouble("Total_Net_Amount");
                sundrydebtors = up_rs.getString("to_ledger");
                Party_ledger = up_rs.getString("by_ledger");
                reducingtax = up_rs.getDouble("Total_Basic_Amount");
                Type = up_rs.getString("Type");
                round1 = up_rs.getDouble("Round_Off");
// Reversing Balance for igst account                
                {
                    igst_account.add(up_rs.getString("Igst_Account"));
                    Statement igst_st = connect.createStatement();
                    Statement up_igst = connect.createStatement();
                    ResultSet igst_rs = igst_st.executeQuery("Select * from ledger where ledger_name = '" + up_rs.getString("Igst_Account") + "'");
                    while (igst_rs.next()) {
                        old_igst.add(igst_rs.getString("percent"));
                        old_igst_currbal.add(igst_rs.getString("curr_bal"));
                        old_igst_currbal_type.add(igst_rs.getString("currbal_type"));

                        double igstamnt = up_rs.getDouble("Igst_Amount");
                        String igstcurrbalance = (String) old_igst_currbal.get(0);
                        String Curbaltypeigst = (String) old_igst_currbal_type.get(0);
                        double igstcurrbalance1 = Double.parseDouble(igstcurrbalance);

                        double igstamnt1 = igstamnt;
                        double result;

                        if (Curbaltypeigst.equals("CR")) {
                            result = igstcurrbalance1 - igstamnt1;
                            if (result >= 0) {
                                old_updateigstcurrbaltype.add(0, "CR");
                                old_updateigstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            } else {
                                old_updateigstcurrbaltype.add(0, "DR");
                                old_updateigstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }
                        } else if (Curbaltypeigst.equals("DR")) {

                            result = igstcurrbalance1 + igstamnt1;

                            old_updateigstcurrbaltype.add(0, "DR");
                            old_updateigstcurrbal.add(0, String.valueOf(Math.abs(result)));
                        }

                        if (Integer.parseInt(old_igst.get(0) + "") != 0) {
                            String updaigst = df.format(Double.parseDouble(old_updateigstcurrbal.get(0).toString()));
                            up_igst.executeUpdate("UPDATE ledger SET curr_bal='" + updaigst + "" + "',currbal_type='" + old_updateigstcurrbaltype.get(0) + "" + "' where ledger_name='" + igst_account.get(0) + "" + "'");
                        }
                        old_igst.clear();
                        old_igst_currbal.clear();
                        old_igst_currbal_type.clear();
                        old_updateigstcurrbal.clear();
                        old_updateigstcurrbaltype.clear();

                    }
                }
// Reversing Balance For Sgst account
                {
                    sgst_account.add(up_rs.getString("Sgst_Account"));
                    Statement sgst_st = connect.createStatement();
                    Statement up_sgst = connect.createStatement();
                    ResultSet sgst_rs = sgst_st.executeQuery("Select * from ledger where ledger_name = '" + up_rs.getString("Sgst_Account") + "'");
                    while (sgst_rs.next()) {
                        old_sgst.add(sgst_rs.getString("percent"));
                        old_sgst_currbal.add(sgst_rs.getString("curr_bal"));
                        old_sgst_currbal_type.add(sgst_rs.getString("currbal_type"));

                        double sgstamnt = up_rs.getDouble("sgst_Amount");
                        String sgstcurrbalance = (String) old_sgst_currbal.get(0);
                        String Curbaltypesgst = (String) old_sgst_currbal_type.get(0);
                        double sgstcurrbalance1 = Double.parseDouble(sgstcurrbalance);

                        double sgstamnt1 = sgstamnt;
                        double result;
                        if (Curbaltypesgst.equals("CR")) {
                            result = sgstcurrbalance1 - sgstamnt1;
                            if (result >= 0) {
                                old_updatesgstcurrbaltype.add(0, "CR");
                                old_updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            } else {
                                old_updatesgstcurrbaltype.add(0, "DR");
                                old_updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }
                        } else if (Curbaltypesgst.equals("DR")) {
                            result = sgstcurrbalance1 + sgstamnt1;
                            old_updatesgstcurrbaltype.add(0, "DR");
                            old_updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                        }

                        if (Integer.parseInt(old_sgst.get(0) + "") != 0) {
                            String updasgst = df.format(Double.parseDouble(old_updatesgstcurrbal.get(0).toString()));
                            up_sgst.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + old_updatesgstcurrbaltype.get(0) + "" + "' where ledger_name='" + sgst_account.get(0) + "" + "'");
                        }
                        old_sgst.clear();
                        old_sgst_currbal.clear();
                        old_sgst_currbal_type.clear();
                        old_updatesgstcurrbal.clear();
                        old_updatesgstcurrbaltype.clear();

                    }
                }
// Reversing Balance for Cgst account
                {
                    cgst_account.add(up_rs.getString("Cgst_Account"));
                    Statement cgst_st = connect.createStatement();
                    Statement up_cgst = connect.createStatement();
                    ResultSet cgst_rs = cgst_st.executeQuery("Select * from ledger where ledger_name = '" + up_rs.getString("Cgst_Account") + "'");
                    while (cgst_rs.next()) {
                        old_cgst.add(cgst_rs.getString("percent"));
                        old_cgst_currbal.add(cgst_rs.getString("curr_bal"));
                        old_cgst_currbal_type.add(cgst_rs.getString("currbal_type"));

                        double cgstamnt = up_rs.getDouble("Cgst_Amount");
                        String cgstcurrbalance = (String) old_cgst_currbal.get(0);
                        String Curbaltypecgst = (String) old_cgst_currbal_type.get(0);
                        double cgstcurrbalance1 = Double.parseDouble(cgstcurrbalance);

                        double cgstamnt1 = cgstamnt;
                        double result;
                        if (Curbaltypecgst.equals("CR")) {
                            result = cgstcurrbalance1 - cgstamnt1;
                            if (result >= 0) {
                                old_updatecgstcurrbaltype.add(0, "CR");
                                old_updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            } else {
                                old_updatecgstcurrbaltype.add(0, "DR");
                                old_updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }
                        } else if (Curbaltypecgst.equals("DR")) {

                            result = cgstcurrbalance1 + cgstamnt1;

                            old_updatecgstcurrbaltype.add(0, "DR");
                            old_updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                        }

                        if (Integer.parseInt(old_cgst.get(0) + "") != 0) {
                            String updacgst = df.format(Double.parseDouble(old_updatecgstcurrbal.get(0).toString()));
                            up_cgst.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + old_updatecgstcurrbaltype.get(0) + "" + "' where ledger_name='" + cgst_account.get(0) + "" + "'");
                        }
                        old_cgst.clear();
                        old_cgst_currbal.clear();
                        old_cgst_currbal_type.clear();
                        old_updatecgstcurrbal.clear();
                        old_updatecgstcurrbaltype.clear();

                    }
                }
            }
// Reversing Balance For Sales Account
            Statement Salesaccount = connect.createStatement();
            ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + Party_ledger + "' ");
            while (rssales.next()) {
                Salesaccountcurrbal.add(rssales.getString("curr_bal"));
                Salesaccountcurrbaltype.add(rssales.getString("currbal_type"));
            }
            double totalamntreducingvat = reducingtax;
            String salesaccountamnt = (String) Salesaccountcurrbal.get(0);
            double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
            double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);

            if (Salesaccountcurrbaltype.get(0).equals("CR")) {
                if (salesaccountupdatedamount > 0) {
                    Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                    Salesaccountupdatecurrbaltype.add("CR");
                }
                if (salesaccountupdatedamount <= 0) {
                    Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                    Salesaccountupdatecurrbaltype.add("DR");
                }
            } else if (Salesaccountcurrbaltype.get(0).equals("DR")) {
                salesaccountupdatedamount = salesaccountamnt1 + totalamntreducingvat;
                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                Salesaccountupdatecurrbaltype.add("DR");
            }

            Statement stmt2 = connect.createStatement();
            String updasales = df.format(Double.parseDouble(Salesaccountupdatecurrbal.get(0).toString()));
            stmt2.executeUpdate("UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + Party_ledger + "'");

            Salesaccountcurrbal.clear();
            Salesaccountcurrbaltype.clear();
            Salesaccountupdatecurrbal.clear();
            Salesaccountupdatecurrbaltype.clear();

// Reversing Balance for Cash and Debit account
            if (Type.equals("BSNL CASH")) {
                Statement Cashaccount = connect.createStatement();
                ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='BSNL CASH'");
                while (rscashaccount.next()) {
                    cashaccountcurrbal.add(rscashaccount.getString(10));
                    cashaccountcurrbaltype.add(rscashaccount.getString(11));
                }
                System.out.println("CODE IS RUNNING FOR CASH UPDATE");
                jTextField4.setText(null);
                Type = jRadioButton1.getLabel();

                String cashaccountamount = (String) cashaccountcurrbal.get(0);
                double totalamnt = c1;
                double cashaccountamount1 = Double.parseDouble(cashaccountamount);

                if (cashaccountcurrbaltype.get(0).equals("DR")) {
                    double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                    if (cashaccountupdateamount > 0) {
                        cashaccountupdatecurrbaltype.add("DR");
                        cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                    if (cashaccountupdateamount <= 0) {
                        cashaccountupdatecurrbaltype.add("CR");
                        cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                }
                if (cashaccountcurrbaltype.get(0).equals("CR")) {
                    double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                    cashaccountupdatecurrbaltype.add("CR");
                    cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                }

                Statement stmt1 = connect.createStatement();
                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + df.format(cashaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='BSNL CASH'");

            } else if (Type.equals("Debit")) {
                Statement sundrydebtorsaccount = connect.createStatement();
                ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + sundrydebtors + "'");

                while (rssundrydebtors.next()) {
                    sundrydebtorsaccountcurrbal.add(rssundrydebtors.getString(10));
                    sundrydebtorsaccountcurrbaltype.add(rssundrydebtors.getString(11));
                }

                String sundrydebtorsamnt = (String) sundrydebtorsaccountcurrbal.get(0);
                double totalamnt = c1;
                double sundrydebtorsamnt1 = Double.parseDouble(sundrydebtorsamnt);

                if (sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                    double sundrydebtorsccountupdateamount = sundrydebtorsamnt1 - totalamnt;
                    if (sundrydebtorsccountupdateamount > 0) {
                        sundrydebtorsaccountupdatecurrbaltype.add(0, "DR");
                        sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                    }
                    if (sundrydebtorsccountupdateamount <= 0) {
                        sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                        sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                    }
                }
                if (sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                    double cashaccountupdateamount = sundrydebtorsamnt1 + totalamnt;
                    sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                    sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(cashaccountupdateamount));
                }

                sundrydebtorsaccount.executeUpdate("UPDATE ledger SET curr_bal='" + df.format(sundrydebtorsaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + sundrydebtors + "'");

            }
// Reversing Balance for Round off      
            String Round_currbal_type1 = "";
            double diff_total1 = 0;
            String diff_total_round1 = "";
            Statement stru = connect.createStatement();
            Statement round_st = connect.createStatement();
            ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
            while (round_rs.next()) {
                double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                Round_currbal_type1 = round_rs.getString("currbal_type");
                double round_total = round1;

                if (Round_currbal_type1.equals("DR") && round_total < 0) {
                    diff_total1 = Round_curr_bal - round_total;
                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }
                } else if (Round_currbal_type1.equals("DR") && round_total > 0) {
                    diff_total1 = Round_curr_bal + round_total;
                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }
                }

                if (Round_currbal_type1.equals("CR") && round_total < 0) {
                    diff_total1 = Round_curr_bal + round_total;

                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }

                } else if (Round_currbal_type1.equals("CR") && round_total > 0) {
                    diff_total1 = Round_curr_bal - round_total;

                    if (diff_total1 > 0) {
                        Round_currbal_type1 = "DR";
                        diff_total1 = (Math.abs(diff_total1));
                    } else if (diff_total1 < 0) {
                        Round_currbal_type1 = "CR";
                        diff_total1 = (Math.abs(diff_total1));
                    }
                }
                diff_total_round1 = df.format(diff_total1);
            }

            stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round1 + "', currbal_type = '" + Round_currbal_type1 + "' where ledger_name = 'Round Off'");

            Statement stmt4 = connect.createStatement();
            Statement stmt5 = connect.createStatement();
            stmt4.executeUpdate("insert into bsnl_billing_gst1 select * from bsnl_billing_gst where Bill_Refrence_No='" + jTextField1.getText() + "'");
            stmt5.executeUpdate("delete from bsnl_billing_gst where Bill_Refrence_No='" + jTextField1.getText() + "'");

            Salesaccountcurrbal.clear();
            Salesaccountupdatecurrbal.clear();
            Salesaccountcurrbaltype.clear();
            Salesaccountupdatecurrbaltype.clear();
            salesaccount.clear();
            cashaccountcurrbal.clear();
            cashaccountupdatecurrbal.clear();
            cashaccountcurrbaltype.clear();
            cashaccountupdatecurrbaltype.clear();
            sundrydebtorsaccountcurrbal.clear();
            sundrydebtorsaccountcurrbaltype.clear();
            sundrydebtorsaccountupdatecurrbal.clear();
            sundrydebtorsaccountupdatecurrbaltype.clear();

            int rowcount = jTable1.getRowCount();
            if (rowcount != 0) {
                jButton5.requestFocus();
                saveData();
            }
            dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//--------------------For Deleting Data from jtable-----------------------------

    public void deleteData() {
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int row = jTable1.getSelectedRow();
// Resetting quntity           
        double del_qnty = Double.parseDouble(jTable1.getValueAt(row, 2) + "");
        Quntity = Quntity - del_qnty;
        jTextField9.setText("" + Quntity);
// Resetting gross amount
        double del_gross_amnt = Double.parseDouble(jTable1.getValueAt(row, 3) + "");
        Gross_Amount = Gross_Amount - del_gross_amnt;
        jTextField11.setText("" + df.format(Gross_Amount));
// Resetting basic amount
        double del_basic_amnt = Double.parseDouble(jTable1.getValueAt(row, 4) + "");
        Basic_Price = Basic_Price - del_basic_amnt;
        jTextField10.setText("" + df.format(Basic_Price));
// Resetting Net amount 
        double del_net_amnt = Double.parseDouble(jTable1.getValueAt(row, 8) + "");
        Net_Amount = Net_Amount - del_net_amnt;
        jTextField15.setText("" + df.format(Net_Amount));
// Resetting Igst amount
        double del_igst_amnt = Double.parseDouble(jTable1.getValueAt(row, 7) + "");
        Igst_Value = Igst_Value - del_igst_amnt;
        jTextField12.setText("" + df.format(Igst_Value));
// Resetting Sgst amount
        double del_sgst_amnt = Double.parseDouble(jTable1.getValueAt(row, 6) + "");
        Sgst_Value = Sgst_Value - del_sgst_amnt;
        jTextField13.setText("" + df.format(Sgst_Value));
// Resetting Cgst amount
        double del_cgst_amnt = Double.parseDouble(jTable1.getValueAt(row, 5) + "");
        Cgst_Value = Cgst_Value - del_cgst_amnt;
        jTextField14.setText("" + df.format(Cgst_Value));
// Resetting Round off Value
        Round_off = Double.parseDouble(jTextField15.getText()) - (Double.parseDouble(jTextField10.getText()) + Double.parseDouble(jTextField12.getText()) + Double.parseDouble(jTextField13.getText()) + Double.parseDouble(jTextField14.getText()));
        jTextField16.setText("" + df.format(Round_off));
// Resetting Serial Number Field
        int a = Integer.parseInt(jTextField4.getText());
        a = a - 1;
        jTextField4.setText("" + a);
// Resetting Jtable
        int rowcount = dtm.getRowCount();
        for (int i = row + 1; i < rowcount; i++) {
            int row1 = row + 2;
            jTable1.getModel().setValueAt(--row1, i, 0);
            row++;
        }
        row = jTable1.getSelectedRow();
        dtm.removeRow(row);
// Resetting all value zero if jtable is empty
        if (rowcount == 0) {
            jTextField9.setText("0.00");
            jTextField10.setText("0.00");
            jTextField11.setText("0.00");
            jTextField12.setText("0.00");
            jTextField13.setText("0.00");
            jTextField14.setText("0.00");
            jTextField15.setText("0.00");
            jTextField16.setText("0.00");
        }

    }
//------------------------------------------------------------------------------    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jDialog1 = new javax.swing.JDialog();
        jLabel26 = new javax.swing.JLabel();
        jTextField20 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton4 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jLabel20 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jTextField18 = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel19 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        jLabel26.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(51, 102, 255));
        jLabel26.setText("Search");

        jTextField20.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField20CaretUpdate(evt);
            }
        });

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Party Code", "Party Name", "Mobile No", "City", "State", "Opening Balance", "Balance Type", "IGST Applicable"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable2);

        jButton4.setText("New Party");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel26)
                .addGap(18, 18, 18)
                .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton4)
                .addContainerGap())
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jDialog2.setTitle("Customer Info");
        jDialog2.setMinimumSize(new java.awt.Dimension(500, 200));
        jDialog2.setResizable(false);

        jLabel20.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel20.setText("Customer Name");

        jLabel21.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel21.setText("Mobile Number");

        jButton5.setText("Submit");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(128, 128, 128)
                        .addComponent(jButton5))
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21))
                        .addGap(64, 64, 64)
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField18, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                            .addComponent(jTextField17))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addGap(19, 19, 19))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("BSNL BILLING");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel1.setText("Bill Refrence No");

        jTextField1.setEditable(false);

        jLabel2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel2.setText("Date");

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jLabel3.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel3.setText("Customer A/C ");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Cash");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Debit");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel4.setText("Customer Name and No");

        jTextField3.setEnabled(false);
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel5.setText("S No");

        jTextField4.setText("1");

        jLabel6.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel6.setText("Product");

        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel7.setText("Quntity");

        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel8.setText("Gross Amount");

        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel9.setText("Basic Price");

        jLabel10.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel10.setText("Net Amount");

        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S No", "Product", "Quntity", "Gross Amount", "Basic Price", "CGST Amount", "SGST Amount", "IGST Amount", "Net Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel11.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel11.setText("Total Quntity");

        jTextField9.setEditable(false);
        jTextField9.setText("0");

        jLabel12.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel12.setText("Total Basic Price");

        jTextField10.setEditable(false);
        jTextField10.setText("0.00");

        jLabel13.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel13.setText("Total Gross Amnt");

        jTextField11.setEditable(false);
        jTextField11.setText("0.00");

        jLabel14.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel14.setText("Total IGST Amnt");

        jTextField12.setEditable(false);
        jTextField12.setText("0.00");

        jLabel15.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel15.setText("Total SGST Amnt");

        jTextField13.setEditable(false);
        jTextField13.setText("0.00");

        jLabel16.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel16.setText("Total CGST Amnt");

        jTextField14.setEditable(false);
        jTextField14.setText("0.00");

        jButton1.setText("Save");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancle");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel17.setText("Total Net Amnt");

        jTextField15.setEditable(false);
        jTextField15.setText("0.00");

        jLabel18.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel18.setText("Round Off");

        jTextField16.setEditable(false);
        jTextField16.setText("0.00");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox2KeyPressed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel19.setText("Mobile No");
        jLabel19.setEnabled(false);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Main/icons8-plus-24.png"))); // NOI18N
        jButton3.setToolTipText("Add New Customer");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton6.setText("Update");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jRadioButton1)
                                        .addGap(18, 18, 18)
                                        .addComponent(jRadioButton2))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel7))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel8))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel10))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel9)
                                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(jLabel12)
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGap(1, 1, 1)
                                                        .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel13)))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel11)
                                                            .addComponent(jLabel14))
                                                        .addGap(26, 26, 26)
                                                        .addComponent(jLabel15))
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGap(6, 6, 6)
                                                        .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel16)
                                                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel18)
                                                    .addComponent(jLabel17)
                                                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton6))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel3)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(30, 30, 30)
                                        .addComponent(jLabel19)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton2)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel13)
                                    .addComponent(jLabel17))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel18))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(19, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButton2)
                                    .addComponent(jButton1)
                                    .addComponent(jButton6))
                                .addContainerGap())))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField20CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField20CaretUpdate
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        dtm.getDataVector().removeAllElements();
        String code = jTextField20.getText();
        String name = jTextField20.getText();
        String city = jTextField20.getText();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  * FROM party_sales where party_name LIKE'%" + name + "%' OR party_code  LIKE '%" + code + "%' OR city LIKE '%" + city + "%'");
            while (rs.next()) {
                String igst = "";
                if (rs.getBoolean("IGST") == true) {
                    igst = "True";
                } else {
                    igst = "False";
                }
                Object o[] = {rs.getString(1), rs.getString(2), rs.getString("mobileno"), rs.getString(4), rs.getString(5), rs.getString(10), rs.getString(11), igst};
                dtm.addRow(o);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField20CaretUpdate

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        partyselect();
    }//GEN-LAST:event_jTable2MouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        master.customer.Add_Sales_Party addnew = new master.customer.Add_Sales_Party();
        addnew.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        if (jRadioButton2.isSelected()) {
            jLabel3.setVisible(true);
            jTextField2.setVisible(true);
            jButton3.setVisible(false);
            jDialog1.setVisible(true);
        }
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField3.requestFocus();
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().equals("") || jTextField5.getText().equals("0")) {
                JOptionPane.showMessageDialog(rootPane, "Quantity can not be blank or Zero");
                jTextField5.requestFocus();
            } else {
                jTextField6.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if ((jTextField8.getText().isEmpty() || jTextField8.getText().equals("0")) || ((jTextField5.getText().isEmpty() || jTextField5.getText().equals("0")))) {
                JOptionPane.showMessageDialog(rootPane, "Net Amount or quamtity or gross amount can not be blank or Zero");
                jTextField8.requestFocus();
            } else {
                //Display Data code goes here
                displayData();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField6.requestFocus();
        }
    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField6.getText().equals("")) {
                JOptionPane.showMessageDialog(rootPane, "Gross Amount can not be blank ");
                jTextField6.requestFocus();
            } else {
                jTextField8.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        int key = evt.getKeyCode();
        if (key == evt.VK_DELETE) {
            //Delete Data code goes here
            deleteData();
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int rowcount = jTable1.getRowCount();
        if (rowcount != 0) {
            int response = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
            if (response == 0) {
                saveData();
                JOptionPane.showMessageDialog(rootPane, "Data Save Successfully...");
                this.dispose();
                Bsnl_Billing_GST bs = new Bsnl_Billing_GST();
                bs.setVisible(true);
            }
        } else if (rowcount == 0) {
            JOptionPane.showMessageDialog(rootPane, "Please Feel the Data !!");
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        jLabel3.setVisible(false);
        jTextField2.setVisible(false);
        jButton3.setVisible(true);
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        jDialog2.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if (!jTextField17.getText().isEmpty() && !jTextField18.getText().isEmpty()) {
            if (!(jTextField18.getText().isEmpty())) {
                String m_no = jTextField18.getText();
                if ((!m_no.matches("[789]{1}[0-9]{9}"))) {
                    JOptionPane.showMessageDialog(null, "Please Enter Complete Mobile Number Only");
                    jTextField18.requestFocus();
                } else {
                    saveCustInfo();
                    JOptionPane.showMessageDialog(rootPane, "Data Submitted successfully");
                    jDialog2.dispose();
                    jComboBox1.requestFocus();
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Some Data");
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jComboBox2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox2KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_jComboBox2KeyPressed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        char key = evt.getKeyChar();
        if (key == KeyEvent.VK_ENTER) {
            jComboBox1.requestFocus();
        }
        if (key == KeyEvent.VK_ESCAPE) {
            jComboBox2.requestFocus();
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Update");
        if (i2 == 0) {
            updateBalance();
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       obj = null;
    }//GEN-LAST:event_formWindowClosed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Billing_GST.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Billing_GST.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Billing_GST.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Billing_GST.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Bsnl_Billing_GST().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    private javax.swing.JComboBox<String> jComboBox1;
    public javax.swing.JComboBox<String> jComboBox2;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JRadioButton jRadioButton1;
    public javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    public javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
