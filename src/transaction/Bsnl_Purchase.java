package transaction;

import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Aashish
 */
public class Bsnl_Purchase extends javax.swing.JFrame {

    /**
     * Creates new form bsnl_purchase
     */
//------------------------------------------------------------------------------
// Setting Date and Time    
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    Calendar currentDate1 = Calendar.getInstance();
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
    String dateNow1 = formatter1.format(currentDate1.getTime());
    Calendar currentDate2 = Calendar.getInstance();
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyy");
    String dateNow2 = formatter2.format(currentDate2.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    private static Bsnl_Purchase obj = null;
    String Year;
    //-----------------------------------------------------------------------------   

    public Bsnl_Purchase() {
        initComponents();
        this.setLocationRelativeTo(null);
        genrate_BillNo();
        fillCombo();
        jButton4.setVisible(false);
        jRadioButton1.setEnabled(false);
        jRadioButton2.setEnabled(false);
        jDateChooser2.setEnabled(false);
        jTextField3.setEnabled(false);
        jTextField4.setEnabled(false);
        jTextField5.setEnabled(false);
        jTextField6.setEnabled(false);
        jTextField7.setEnabled(false);
        jTextField8.setEnabled(false);
        jTextField9.setEnabled(false);
        jTextField10.setEnabled(false);
        jTextField11.setEnabled(false);
        jTextField12.setEnabled(false);
        jTextField13.setEnabled(false);
        jTextField14.setEnabled(false);
        jTextField15.setEnabled(false);
        jTextField16.setEnabled(false);
        jTextField17.setEnabled(false);
        jTextField18.setEnabled(false);
        jTextField19.setEnabled(false);
    }
    
      public static Bsnl_Purchase getObj() {
        if (obj == null) {
            obj = new Bsnl_Purchase();
        }
        return obj;
    }
//---------------Declaring Gloable Variable-------------------------------------
    DecimalFormat df = new DecimalFormat("0.00");
    public String Purchaseaccount = "";
    public static int s = 1;
    public String Sno = "";
    double Qnty = 0;
    String Product = "";
    double Gross_Amount = 0;
    double Basic_Amount = 0;
    double total_basic_amount = 0;
    double total_gross_amount = 0;
    double total_net_amount = 0;
    String pur_type = "";
    double Pur_Amnt = 0;
    double Disc_Amnt = 0;
    double Amnt_After_Disc = 0;
    double Other_Charges = 0;
    double Bill_Amnt = 0;
    public boolean isIgstApplicable = false;
    double iv = 0;
    double cv = 0;
    double sv = 0;
    double tiv = 0;
    double tcv = 0;
    double tsv = 0;
    double trv = 0;
    double sgstpercent = 0;
    double cgstpercent = 0;
    double igstpercent = 0;
    double totalpercent = 0;
    double discount_amount = 0;
    double other_charges_amount = 0;
    double qnty = 0;
    String Discountaccount = null;
    String updateddisccurrbal = null;
    String updateddisccurrbaltyoe = null;
    String Igst_BalType = null;
    double Igst_Bal = 0;
    String Sgst_BalType = null;
    double Sgst_Bal = 0;
    String Cgst_BalType = null;
    double Cgst_Bal = 0;
    boolean cashisselected = true;
    String other_charges_acc = null;
    double other_charges_bal = 0;
    String other_charges_bal_type = null;
    String other_BalType = null;
    double other_Bal = 0;
    String pur_BalType = null;
    double pur_Bal = 0;
    String creditor_BalType = null;
    double creditor_Bal = 0;
    String Cash_BalType = null;
    double Cash_Bal = 0;
    String Round_BalType = null;
    double Round_Bal = 0;
    String Update_Igst_BalType = null;
    double Update_Igst_Bal = 0;
    String Update_Sgst_BalType = null;
    double Update_Sgst_Bal = 0;
    String Update_Cgst_BalType = null;
    double Update_Cgst_Bal = 0;
    String Igst_Account = null;
    String Sgst_Account = null;
    String Cgst_Account = null;
//------------------------------------------------------------------------------    
//---------Genrating Bsnl_ Purchase Bill No and Date----------------------------

    public void genrate_BillNo() {
        String purchase = "BPP/";
        String slash = "/";
        String yearwise = Year.concat(slash);
//        String yearwise = (getyear.concat(nextyear)).concat(slash);
        String finalconcat = (purchase.concat(yearwise));
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement Bill_No_st = connect.createStatement();
            ResultSet Bill_No_rs = Bill_No_st.executeQuery("SELECT Bill_Refrence_No FROM bsnl_purchase ");
            int a = 0, b, d = 0;
            int i = 0;
            while (Bill_No_rs.next()) {
                if (i == 0) {
                    d = Bill_No_rs.getString(1).lastIndexOf("/");
                    i++;
                }
                b = Integer.parseInt(Bill_No_rs.getString(1).substring(d + 1));
                if (a < b) {
                    a = b;
                }
            }

            String s = finalconcat.concat(Integer.toString(a + 1));
            jTextField2.setText("" + s);
            jDateChooser1.setDate(formatter.parse(dateNow));
            jDateChooser2.setDate(formatter.parse(dateNow));
        } catch (Exception e) {
            System.out.println("Connection Faild");
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------
//--------Fill Combobox--------------------------------------------------------- 

    public void fillCombo() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement Combo_st = connect.createStatement();
            ResultSet Combo_rs = Combo_st.executeQuery("SELECT product_code FROM product");
            while (Combo_rs.next()) {
                String name = Combo_rs.getString("product_code");
                jComboBox1.addItem(name);
            }
             ResultSet rs_year = Combo_st.executeQuery("Select * from year");
            while(rs_year.next()){
            Year = rs_year.getString("year");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//--------Select Party----------------------------------------------------------

    public void partyselect() {
        String GST = "True";
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        int row = jTable2.getSelectedRow();
        jLabel24.setText("" + dtm.getValueAt(row, 0));
        jTextField1.setText("" + dtm.getValueAt(row, 1));
        jDialog1.setVisible(false);

        Purchaseaccount = (String) jTable2.getValueAt(row, 6);
        jLabel26.setText(Purchaseaccount);
        jRadioButton1.setEnabled(true);
        jRadioButton2.setSelected(true);

        if (Purchaseaccount == null) {
            JOptionPane.showMessageDialog(rootPane, "please select Purchase account");
        } else {
            jRadioButton1.setEnabled(true);
            jRadioButton2.setEnabled(true);
            jDateChooser2.setEnabled(true);
            jTextField3.setEnabled(true);
            jTextField4.setEnabled(true);
            jTextField5.setEnabled(true);
            jTextField6.setEnabled(true);
            jTextField7.setEnabled(true);
            jTextField8.setEnabled(true);
            jTextField9.setEnabled(true);
            jTextField10.setEnabled(true);
            jTextField11.setEnabled(true);
            jTextField12.setEnabled(true);
            jTextField13.setEnabled(true);
            jTextField14.setEnabled(true);
            jTextField15.setEnabled(true);
            jTextField16.setEnabled(true);
            jTextField17.setEnabled(true);
            jTextField18.setEnabled(true);
            jTextField19.setEnabled(true);
        }

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + (String) jTable2.getValueAt(row, 1) + "'");
            while (rs.next()) {
                jLabel27.setText(rs.getString(10));
                jLabel28.setText(rs.getString(11));
            }

            if (GST.equalsIgnoreCase((String) jTable2.getValueAt(row, 7))) {
                jTextField12.setEnabled(false);
                jTextField13.setEnabled(false);
                jTextField14.setEnabled(false);
                jTextField15.setEnabled(false);
                isIgstApplicable = true;

            } else if (!GST.equalsIgnoreCase((String) jTable2.getValueAt(row, 7))) {
                jTextField16.setEnabled(false);
                jTextField17.setEnabled(false);
                isIgstApplicable = false;
            }
            jTextField3.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------
//------ Calculate Data---------------------------------------------------------

    public void calculation() {

        double amnt = Double.parseDouble(jTextField8.getText());
        double discamnt = Double.parseDouble(jTextField10.getText());
        double igstamnt = Double.parseDouble(jTextField17.getText());
        double sgstamnt = Double.parseDouble(jTextField15.getText());
        double cgstamnt = Double.parseDouble(jTextField13.getText());
        double dam = amnt - discamnt;
        double oth = Double.parseDouble(jTextField18.getText());

        double tot = (dam + oth + igstamnt + sgstamnt + cgstamnt);
        double round_off = tot - (amnt + discamnt + oth + igstamnt + sgstamnt + cgstamnt);

        jTextField10.setText("" + discamnt);
        jTextField11.setText("" + dam);
        jTextField19.setText("" + tot);
        jTextField21.setText("" + df.format(round_off));
    }
//------------------------------------------------------------------------------

    public void updateBalance() {
//................For Discount..................................................
        String disc_currbal = null;
        String disc_currbaltype = null;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='DISCOUNT ACCOUNT'");
            while (rs.next()) {
                Discountaccount = rs.getString(1);
                disc_currbal = rs.getString(10);
                disc_currbaltype = rs.getString(11);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//   For Discount     
        {
            String disccurrbal = disc_currbal;
            String Curbaltypedisc = disc_currbaltype;
            double othercurrbal1 = Double.parseDouble(disccurrbal);
            double discamount = Double.parseDouble(jTextField10.getText());
            double adisc;

            if (Curbaltypedisc.equals("DR")) {
                adisc = discamount - othercurrbal1;
                if (adisc <= 0) {
                    updateddisccurrbaltyoe = "DR";
                    updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));

                } else {
                    updateddisccurrbaltyoe = ("CR");
                    updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
                }

            } else {
                if (Curbaltypedisc.equals("CR")) {
                    adisc = discamount + othercurrbal1;
                    if (adisc >= 0) {
                        updateddisccurrbaltyoe = ("CR");
                        updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
                    }
                }

            }
        }
//..............................................................................
//.............For IGST.........................................................
        if ((Double.parseDouble(jTextField17.getText()) == 0)) {

        }// else if ((Double.parseDouble(jTextField17.getText()) != 0)) {
        //          JOptionPane.showMessageDialog(rootPane, "Please choose Igst account");
        //       } 
        else {
            double igst_bal = 0;
            String igst_type = "";
            if (Igst_Account == null) {
                Igst_Account = Old_Party_Igst;
            }
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + Igst_Account + "'");
                while (rs.next()) {
                    igst_bal = rs.getDouble("curr_bal");
                    igst_type = rs.getString("currbal_type");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            double igstcurrbal = igst_bal;
            String Curbaltypeigst = igst_type;
            double igstcurrbal1 = igstcurrbal;
            double igstamount = Double.parseDouble(jTextField17.getText());
            double aigst;
            if (Curbaltypeigst.equals("CR")) {
                aigst = igstamount - igstcurrbal1;
                if (aigst < 0) {
                    Update_Igst_BalType = "CR";
                    Igst_Bal = Math.abs(aigst);
                } else {
                    Update_Igst_BalType = "DR";
                    Update_Igst_Bal = Math.abs(aigst);
                }
            } else {
                if (Curbaltypeigst.equals("DR")) {
                    aigst = igstamount + igstcurrbal1;
                    if (aigst > 0) {
                        Update_Igst_BalType = "DR";
                        Update_Igst_Bal = Math.abs(aigst);
                    }
                }
            }
        }
//..............................................................................
//...........For SGST...........................................................
        if ((Double.parseDouble(jTextField15.getText()) == 0)) {
        }// else if ((Double.parseDouble(jTextField15.getText()) != 0)) {
        //   JOptionPane.showMessageDialog(rootPane, "Please choose Sgst account");
        //     }
        else {
            double sgst_bal = 0;
            String sgst_type = "";
            if (Sgst_Account == null) {
                Sgst_Account = Old_Party_sgst;
            }
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + Sgst_Account + "'");
                while (rs.next()) {
                    sgst_bal = rs.getDouble("curr_bal");
                    sgst_type = rs.getString("currbal_type");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            double sgstcurrbal = sgst_bal;
            String Curbaltypesgst = sgst_type;
            double sgstcurrbal1 = sgstcurrbal;
            double sgstamount = Double.parseDouble(jTextField15.getText());
            double asgst;
            if (Curbaltypesgst.equals("CR")) {
                asgst = sgstamount - sgstcurrbal1;
                if (asgst < 0) {
                    Update_Sgst_BalType = "CR";
                    Update_Sgst_Bal = Math.abs(asgst);
                } else {
                    Update_Sgst_BalType = "DR";
                    Update_Sgst_Bal = Math.abs(asgst);
                }
            } else {
                if (Curbaltypesgst.equals("DR")) {
                    asgst = sgstamount + sgstcurrbal1;
                    if (asgst > 0) {
                        Update_Sgst_BalType = "DR";
                        Update_Sgst_Bal = Math.abs(asgst);
                    }
                }
            }
        }
//..............................................................................
//..............For CGST........................................................
        if ((Double.parseDouble(jTextField13.getText()) == 0)) {
        } // else if ((Double.parseDouble(jTextField13.getText()) != 0)) {
        //  JOptionPane.showMessageDialog(rootPane, "Please choose cst account");
        //        } 
        else {
            double cgst_bal = 0;
            String cgst_type = "";
            if (Cgst_Account == null) {
                Cgst_Account = Old_Party_cgst;
            }
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + Cgst_Account + "'");
                while (rs.next()) {
                    cgst_bal = rs.getDouble("curr_bal");
                    cgst_type = rs.getString("currbal_type");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            double cgstcurrbal = cgst_bal;
            String Curbaltypecgst = cgst_type;
            double cgstcurrbal1 = cgstcurrbal;
            double cgstamount = Double.parseDouble(jTextField13.getText());
            double acgst;
            if (Curbaltypecgst.equals("CR")) {
                acgst = cgstamount - cgstcurrbal1;
                if (acgst < 0) {
                    Update_Cgst_BalType = "CR";
                    Update_Cgst_Bal = Math.abs(acgst);
                } else {
                    Update_Cgst_BalType = "DR";
                    Update_Cgst_Bal = Math.abs(acgst);
                }
            } else {
                if (Curbaltypecgst.equals("DR")) {
                    acgst = cgstamount + cgstcurrbal1;
                    if (acgst > 0) {
                        Update_Cgst_BalType = "DR";
                        Update_Cgst_Bal = Math.abs(acgst);
                    }
                }
            }
        }
//..............For Other Charges...............................................
        if ((Double.parseDouble(jTextField18.getText()) == 0)) {
        } else if (jTextField18.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "Please choose other charges account");
        } else {
            double othercurrbal = other_charges_bal;
            String Curbaltypeother = other_charges_bal_type;
            double othercurrbal1 = othercurrbal;
            double otheramount = Double.parseDouble(jTextField18.getText());
            double aother;
            if (Curbaltypeother.equals("CR")) {
                aother = otheramount - othercurrbal1;
                if (aother < 0) {
                    other_BalType = "CR";
                    other_Bal = Math.abs(aother);
                } else {
                    other_BalType = "DR";
                    other_Bal = Math.abs(aother);
                }
            } else {
                if (Curbaltypeother.equals("DR")) {
                    aother = otheramount + othercurrbal1;
                    if (aother > 0) {
                        other_BalType = "DR";
                        other_Bal = Math.abs(aother);
                    }
                }
            }
        }
//.............................................................................. 
//...................For Round Off Value........................................
        String round_currbal = null;
        String round_currbaltype = null;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement round_st = connect.createStatement();
            ResultSet round_rs = round_st.executeQuery("select * from ledger where ledger_name='Round Off'");
            while (round_rs.next()) {
                round_currbal = round_rs.getString("curr_bal");
                round_currbaltype = round_rs.getString("currbal_type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        {
            String roundcurrbal = round_currbal;
            String Curbaltyperound = round_currbaltype;
            double roundcurrbal1 = Double.parseDouble(roundcurrbal);
            double roundamount = Double.parseDouble(jTextField21.getText());
            double amt_round;

            if (Curbaltyperound.equals("DR")) {
                amt_round = roundamount - roundcurrbal1;
                if (amt_round <= 0) {
                    Round_BalType = "DR";
                    Round_Bal = (Math.abs(amt_round));

                } else {
                    Round_BalType = ("CR");
                    Round_Bal = (Math.abs(amt_round));
                }

            } else {
                if (Curbaltyperound.equals("CR")) {
                    amt_round = roundamount + roundcurrbal1;
                    if (amt_round >= 0) {
                        Round_BalType = ("CR");
                        Round_Bal = (Math.abs(amt_round));
                    }
                }
            }
        }
//..............................................................................
//.............For Purchase account.............................................
        String pur_currbal = null;
        String pur_currbaltype = null;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + jLabel26.getText() + "'");
            while (rs.next()) {
                pur_currbal = rs.getString("curr_bal");
                pur_currbaltype = rs.getString("currbal_type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        {
            String purcurrbal = pur_currbal;
            String Curbaltypepur = pur_currbaltype;
            double purcurrbal1 = Double.parseDouble(purcurrbal);
            double puramount = Double.parseDouble(jTextField8.getText()) - Double.parseDouble(jTextField10.getText());
            double amt_pur;
            if (Curbaltypepur.equals("CR")) {
                amt_pur = puramount - purcurrbal1;
                if (amt_pur <= 0) {
                    pur_BalType = "CR";
                    pur_Bal = (Math.abs(amt_pur));

                } else {
                    pur_BalType = ("DR");
                    pur_Bal = (Math.abs(amt_pur));
                }

            } else {
                if (Curbaltypepur.equals("DR")) {
                    amt_pur = puramount + purcurrbal1;
                    if (amt_pur >= 0) {
                        pur_BalType = ("DR");
                        pur_Bal = (Math.abs(amt_pur));
                    }
                }
            }
        }
//..............................................................................
//..............For Cash account................................................
        String cash_currbal = null;
        String cash_currbaltype = null;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='CASH'");
            while (rs.next()) {
                cash_currbal = rs.getString("curr_bal");
                cash_currbaltype = rs.getString("currbal_type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        {
            String cashcurrbal = cash_currbal;
            String Curbaltypecash = cash_currbaltype;
            double cashcurrbal1 = Double.parseDouble(cashcurrbal);
            double cashamount = Double.parseDouble(jTextField19.getText());
            double amt_cash;

            if (Curbaltypecash.equals("DR")) {
                amt_cash = cashamount - cashcurrbal1;
                if (amt_cash <= 0) {
                    Cash_BalType = "DR";
                    Cash_Bal = (Math.abs(amt_cash));

                } else {
                    Cash_BalType = ("CR");
                    Cash_Bal = (Math.abs(amt_cash));
                }

            } else {
                if (Curbaltypecash.equals("CR")) {
                    amt_cash = cashamount + cashcurrbal1;
                    if (amt_cash >= 0) {
                        Cash_BalType = ("CR");
                        Cash_Bal = (Math.abs(amt_cash));
                    }
                }
            }
        }
//..............................................................................
//.............For Creditors account.............................................
        String creditor_currbal = null;
        String creditor_currbaltype = null;
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + jTextField1.getText() + "'");
            while (rs.next()) {
                creditor_currbal = rs.getString("curr_bal");
                creditor_currbaltype = rs.getString("currbal_type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        {
            String crecurrbal = creditor_currbal;
            String Curbaltypecre = creditor_currbaltype;
            double crecurrbal1 = Double.parseDouble(crecurrbal);
            double creamount = Double.parseDouble(jTextField19.getText());
            double amt_cre;

            if (Curbaltypecre.equals("DR")) {
                amt_cre = creamount - crecurrbal1;
                if (amt_cre <= 0) {
                    creditor_BalType = "DR";
                    creditor_Bal = (Math.abs(amt_cre));

                } else {
                    creditor_BalType = ("CR");
                    creditor_Bal = (Math.abs(amt_cre));
                }

            } else {
                if (Curbaltypecre.equals("CR")) {
                    amt_cre = creamount + crecurrbal1;
                    if (amt_cre >= 0) {
                        creditor_BalType = ("CR");
                        creditor_Bal = (Math.abs(amt_cre));
                    }
                }
            }
        }
//..............................................................................
    }
//------------------------------------------------------------------------------
//----------------Save Method---------------------------------------------------

    public void save() {
        updateBalance();
        int rowcount = jTable1.getRowCount();
        double gross_amount = 0;
        double total_gross_amnt = 0;
        for (int i = 0; i < rowcount; i++) {
            gross_amount = Double.parseDouble(jTable1.getValueAt(i, 3) + "");
            total_gross_amnt = gross_amount + total_gross_amnt;
        }
        for (int i = 0; i < rowcount; i++) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                String cgstaccount = "";
                String sgstaccount = "";
                Statement stcgst = connect.createStatement();
                ResultSet rs = st.executeQuery("select CGST,SGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                while (rs.next()) {
                    cgstaccount = rs.getString("CGST");
                    sgstaccount = rs.getString("SGST");
                }

                if (jRadioButton1.isSelected()) {
                    st.executeUpdate("insert into bsnl_purchase values ('" + jTextField2.getText() + "','" + jLabel24.getText() + "','" + jTextField1.getText() + "','" + formatter.format(jDateChooser1.getDate()) + "','" + pur_type + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField3.getText() + "','" + jTable1.getValueAt(i, 0) + "','" + jTable1.getValueAt(i, 1) + "','" + jTable1.getValueAt(i, 2) + "','" + jTable1.getValueAt(i, 3) + "','" + jTable1.getValueAt(i, 4) + "','" + jTextField10.getText() + "','" + jTextField11.getText() + "','" + total_gross_amnt + "','" + jTextField8.getText() + "','" + jTextField19.getText() + "','" + jTextField9.getText() + "','" + jTextField10.getText() + "','" + jTextField11.getText() + "','" + other_charges_acc + "','" + jTextField18.getText() + "','" + jTextField16.getText() + "','" + df.format(iv) + "','" + sgstaccount + "','" + df.format(sv) + "','" + cgstaccount + "','" + df.format(cv) + "','" + jTextField17.getText() + "','" + jTextField15.getText() + "','" + jTextField13.getText() + "','CASH','" + Purchaseaccount + "','','" + jTextField21.getText() + "')");
//                   st.executeUpdate("Update ledger SET curr_bal= '" + Cash_Bal + "',currbal_type='" + Cash_BalType + "' where ledger_name='CASH'");
                } else if (jRadioButton2.isSelected()) {
                    st.executeUpdate("insert into bsnl_purchase values ('" + jTextField2.getText() + "','" + jLabel24.getText() + "','" + jTextField1.getText() + "','" + formatter.format(jDateChooser1.getDate()) + "','" + pur_type + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField3.getText() + "','" + jTable1.getValueAt(i, 0) + "','" + jTable1.getValueAt(i, 1) + "','" + jTable1.getValueAt(i, 2) + "','" + jTable1.getValueAt(i, 3) + "','" + jTable1.getValueAt(i, 4) + "','" + jTextField10.getText() + "','" + jTextField11.getText() + "','" + total_gross_amnt + "','" + jTextField8.getText() + "','" + jTextField19.getText() + "','" + jTextField9.getText() + "','" + jTextField10.getText() + "','" + jTextField11.getText() + "','" + other_charges_acc + "','" + jTextField18.getText() + "','" + jTextField16.getText() + "','" + df.format(iv) + "','" + sgstaccount + "','" + df.format(sv) + "','" + cgstaccount + "','" + df.format(cv) + "','" + jTextField17.getText() + "','" + jTextField15.getText() + "','" + jTextField13.getText() + "','" + Purchaseaccount + "','" + jTextField1.getText() + "','','" + jTextField21.getText() + "')");
//                    st.executeUpdate("Update ledger SET curr_bal= '" + creditor_Bal + "',currbal_type='" + creditor_BalType + "' where ledger_name='" + jTextField1.getText() + "'");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
// Updating Ledger Account
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement up_st = connect.createStatement();
            System.out.println("Updating Ledger Accounts...");
// Updating Purchase account ledger
            up_st.executeUpdate("Update ledger SET curr_bal= '" + pur_Bal + "',currbal_type='" + pur_BalType + "' where ledger_name='" + jLabel26.getText() + "'");
            if (Igst_Account != null && Sgst_Account == null && Cgst_Account == null) {
// Updating Igst Ledger
                up_st.executeUpdate("Update ledger SET curr_bal= '" + Update_Igst_Bal + "',currbal_type='" + Update_Igst_BalType + "' where ledger_name='" + Igst_Account + "'");
            } else if (Sgst_Account != null && Cgst_Account != null && Igst_Account == null) {
// Updating Sgst Ledger
                up_st.executeUpdate("Update ledger SET curr_bal= '" + Update_Sgst_Bal + "',currbal_type='" + Update_Sgst_BalType + "' where ledger_name='" + Sgst_Account + "'");
// Updating Igst Ledger
                up_st.executeUpdate("Update ledger SET curr_bal= '" + Update_Cgst_Bal + "',currbal_type='" + Update_Cgst_BalType + "' where ledger_name='" + Cgst_Account + "'");
            }
// Updating Other Charges Account
            if (!jTextField18.equals("0") || !jTextField18.equals("")) {
                up_st.executeUpdate("Update ledger SET curr_bal= '" + other_Bal + "',currbal_type='" + other_BalType + "' where ledger_name='" + other_charges_acc + "'");
            }
// Updating Round Off Ledger
            if (!jTextField21.equals("0") || !jTextField21.equals("")) {
                up_st.executeUpdate("Update ledger SET curr_bal= '" + Round_Bal + "',currbal_type='" + Round_BalType + "' where ledger_name='Round Off '");
            }
// Updating Cash and Creditors Account Ledger
            if (jRadioButton1.isSelected()) {
                up_st.executeUpdate("Update ledger SET curr_bal= '" + Cash_Bal + "',currbal_type='" + Cash_BalType + "' where ledger_name='CASH'");
            } else if (jRadioButton2.isSelected()) {
                up_st.executeUpdate("Update ledger SET curr_bal= '" + creditor_Bal + "',currbal_type='" + creditor_BalType + "' where ledger_name='" + jTextField1.getText() + "'");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
//------------------------------------------------------------------------------    
//----------------------Method for Reversing Balance----------------------------
    //--------------------Reversing Balance-------------------------------------------------------------------------------------
    public String sundrycreditorold = "";
    String sundrycreditorcurrbalold = "";
    String sundrycreditorcurrbaltypeold = "";
    String rDiscountaccount = null;
    String rupdateddisccurrbal = null;
    String rupdateddisccurrbaltyoe = null;
    public String Old_Party_Igst = null;
    public String Old_Party_cgst = null;
    public String Old_Party_sgst = null;
    public String Old_other_charges = null;
    double rigst_value = 0;
    String rigst_type = "";
    double rsgst_value = 0;
    String rsgst_type = "";
    double rcgst_value = 0;
    String rcgst_type = "";
    double rupdate_igst_value = 0;
    String rupdate_igst_type = "";
    double rupdate_sgst_value = 0;
    String rupdate_sgst_type = "";
    double rupdate_cgst_value = 0;
    String rupdate_cgst_type = "";
    double rupdate_other_value = 0;
    String rupdate_other_type = "";
    double rupdate_cash_value = 0;
    String rupdate_cash_type = "";
    double rupdate_purc_amnt = 0;
    String rupdate_purc_type = "";
    public double rev_igst_value = 0;
    public double rev_sgst_value = 0;
    public double rev_cgst_value = 0;

    public void updatereversebalances() {
//---------------For Igst-----------------------------------------------------------------------------------
        if (rev_igst_value != 0.00 && (!Old_Party_Igst.equals("0"))) {
            try {
                double rigst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rigst_st = connect.createStatement();
                ResultSet rigst_rs = rigst_st.executeQuery("Select * from ledger where ledger_name = '" + Old_Party_Igst + "'");
                while (rigst_rs.next()) {
                    rigst_value = rigst_rs.getDouble("curr_bal");
                    rigst_type = rigst_rs.getString("currbal_type");
                }

                ResultSet rigst_rs1 = rigst_st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTextField2.getText() + "'");
                while (rigst_rs1.next()) {
                    rigst_amnt = rigst_rs1.getDouble("Total_Igst_Amount");
                }

                double riamnt;

                if (rigst_type.equals("DR")) {
                    riamnt = rigst_amnt - rigst_value;
                    if (riamnt < 0) {
                        rupdate_igst_type = "DR";
                        rupdate_igst_value = Math.abs(riamnt);
                    } else {
                        rupdate_igst_type = "CR";
                        rupdate_igst_value = Math.abs(riamnt);
                    }
                } else {
                    if (rigst_type.equals("CR")) {
                        riamnt = rigst_amnt + rigst_value;
                        if (riamnt > 0) {
                            rupdate_igst_type = "CR";
                            rupdate_igst_value = Math.abs(riamnt);
                        }
                    }
                }

                rigst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_igst_value + "',currbal_type='" + rupdate_igst_type + "' where ledger_name = '" + Old_Party_Igst + "'");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   

//---------------For Sgst-----------------------------------------------------------------------------------------------
        if (rev_sgst_value != 0.00 && (!Old_Party_sgst.equals("0"))) {
            try {
                double rsgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rsgst_st = connect.createStatement();
                ResultSet rsgst_rs = rsgst_st.executeQuery("Select * from ledger where ledger_name = '" + Old_Party_sgst + "'");
                while (rsgst_rs.next()) {
                    rsgst_value = rsgst_rs.getDouble("curr_bal");
                    rsgst_type = rsgst_rs.getString("currbal_type");
                }
                ResultSet rsgst_rs1 = rsgst_st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTextField2.getText() + "'");
                while (rsgst_rs1.next()) {
                    rsgst_amnt = rsgst_rs1.getDouble("Total_Sgst_Amount");
                }

                double rsamnt;

                if (rsgst_type.equals("DR")) {
                    rsamnt = rsgst_amnt - rsgst_value;
                    if (rsamnt < 0) {
                        rupdate_sgst_type = "DR";
                        rupdate_sgst_value = Math.abs(rsamnt);
                    } else {
                        rupdate_sgst_type = "CR";
                        rupdate_sgst_value = Math.abs(rsamnt);
                    }
                } else {
                    if (rsgst_type.equals("CR")) {
                        rsamnt = rsgst_amnt + rsgst_value;
                        if (rsamnt > 0) {
                            rupdate_sgst_type = "CR";
                            rupdate_sgst_value = Math.abs(rsamnt);
                        }
                    }
                }
                rsgst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_sgst_value + "',currbal_type='" + rupdate_sgst_type + "' where ledger_name = '" + Old_Party_sgst + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   
//---------------For Cgst-----------------------------------------------------------------------------------------------
        if (rev_cgst_value != 0.00 && (!Old_Party_cgst.equals("0"))) {
            try {
                double rcgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcgst_st = connect.createStatement();
                ResultSet rcgst_rs = rcgst_st.executeQuery("Select * from ledger where ledger_name = '" + Old_Party_cgst + "'");
                while (rcgst_rs.next()) {
                    rcgst_value = rcgst_rs.getDouble("curr_bal");
                    rcgst_type = rcgst_rs.getString("currbal_type");
                }

                ResultSet rcgst_rs1 = rcgst_st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTextField2.getText() + "'");
                while (rcgst_rs1.next()) {
                    rcgst_amnt = rcgst_rs1.getDouble("Total_Cgst_Amount");
                }

                double rcamnt;

                if (rcgst_type.equals("DR")) {
                    rcamnt = rcgst_amnt - rcgst_value;
                    if (rcamnt < 0) {
                        rupdate_cgst_type = "DR";
                        rupdate_cgst_value = Math.abs(rcamnt);
                    } else {
                        rupdate_cgst_type = "CR";
                        rupdate_cgst_value = Math.abs(rcamnt);
                    }
                } else {
                    if (rcgst_type.equals("CR")) {
                        rcamnt = rcgst_amnt + rcgst_value;
                        if (rcamnt > 0) {
                            rupdate_cgst_type = "CR";
                            rupdate_cgst_value = Math.abs(rcamnt);
                        }
                    }
                }
                rcgst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_cgst_value + "',currbal_type='" + rupdate_cgst_type + "' where ledger_name = '" + Old_Party_cgst + "'");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   
//------------------------For Other Charges--------------------------------------------------------------------------
        if ((Double.parseDouble(jTextField18.getText()) == 0) && (Old_other_charges == null)) {

        } else if ((Double.parseDouble(jTextField18.getText()) != 0) && (Old_other_charges == null)) {
            JOptionPane.showMessageDialog(rootPane, "Please choose other charges account");
            jTextField18.requestFocus();
        } else {
            try {
                double rotheramount = 0;
                double rothercurrbal = 0;
                String rCurbaltypeother = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement rother_st = connect.createStatement();
                ResultSet rother_rs = rother_st.executeQuery("Select * from ledger where ledger_name = '" + Old_other_charges + "'");

                while (rother_rs.next()) {
                    rothercurrbal = rother_rs.getDouble("curr_bal");
                    rCurbaltypeother = rother_rs.getString("currbal_type");
                }

                ResultSet rother_rs1 = rother_st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTextField2.getText() + "'");
                while (rother_rs1.next()) {
                    rotheramount = rother_rs1.getDouble("Other_Charges");
                }

                double raother;

                if (rCurbaltypeother.equals("DR")) {
                    raother = rotheramount - rothercurrbal;
                    if (raother < 0) {
                        rupdate_other_type = "DR";
                        rupdate_other_value = Math.abs(raother);

                    } else {
                        rupdate_other_type = "CR";
                        rupdate_other_value = Math.abs(raother);
                    }

                } else {
                    if (rCurbaltypeother.equals("CR")) {
                        raother = rotheramount + rothercurrbal;
                        if (raother > 0) {
                            rupdate_other_type = "CR";
                            rupdate_other_value = Math.abs(raother);
                        }
                    }
                }

                rother_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_other_value + "',currbal_type='" + rupdate_other_type + "' where ledger_name='" + Old_other_charges + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------
//--------------For Creaditor and Cash----------------------------------------------------------------------------------------------------------
        if (jRadioButton1.isSelected()) {
            try {
                double rcash_amount = 0;
                double rcashcurrbal = 0;
                String rCurbaltypecash = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcash_st = connect.createStatement();
                ResultSet rcash_rs = rcash_st.executeQuery("Select * from ledger where ledger_name = 'CASH'");

                while (rcash_rs.next()) {
                    rcashcurrbal = rcash_rs.getDouble("curr_bal");
                    rCurbaltypecash = rcash_rs.getString("currbal_type");
                }

                ResultSet rcash_rs1 = rcash_st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTextField2.getText() + "'");
                while (rcash_rs1.next()) {
                    rcash_amount = rcash_rs1.getDouble("Total_Net_Amount");
                }

                double rcash_amnt = 0;

                if (rCurbaltypecash.equals("DR")) {
                    rcash_amnt = rcashcurrbal + rcash_amount;
                    rupdate_cash_value = Math.abs(rcash_amnt);
                } else if (rCurbaltypecash.equals("CR")) {
                    if (rcash_amnt > rcash_amount) {
                        rcash_amnt = rcashcurrbal - rcash_amount;
                        rupdate_cash_value = Math.abs(rcash_amnt);
                    }
                    if (rcash_amnt < rcash_amount) {
                        rcash_amnt = rcash_amount - rcashcurrbal;
                        rupdate_cash_value = Math.abs(rcash_amnt);
                        rupdate_cash_type = "DR";
                    }
                }

                rcash_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_cash_value + "',currbal_type='" + rupdate_cash_type + "' where ledger_name='CASH'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (jRadioButton2.isSelected()) {
            try {
                double totalamount = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcraditor_st = connect.createStatement();
                ResultSet rs4 = rcraditor_st.executeQuery("select * from ledger where ledger_name='" + sundrycreditorold + "'");
                while (rs4.next()) {
                    sundrycreditorcurrbalold = rs4.getString("curr_bal");
                    sundrycreditorcurrbaltypeold = rs4.getString("currbal_type");
                }

                String creditoramount = sundrycreditorcurrbalold;
                System.out.println("sundrycreditorcurrbalold >" + creditoramount);

                ResultSet rs5 = rcraditor_st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTextField2.getText() + "'");
                while (rs5.next()) {
                    totalamount = rs5.getDouble("Total_Net_Amount");
                }

                double creditoramount1 = Double.parseDouble(creditoramount);
                if (sundrycreditorcurrbaltypeold.equals("DR")) {
                    creditoramount1 = creditoramount1 + totalamount;
                    sundrycreditorcurrbalold = ("" + creditoramount1);
                } else if (sundrycreditorcurrbaltypeold.equals("CR")) {
                    if (creditoramount1 > totalamount) {
                        creditoramount1 = creditoramount1 - totalamount;
                        sundrycreditorcurrbalold = ("" + creditoramount1);

                    } else if (creditoramount1 <= totalamount) {
                        creditoramount1 = totalamount - creditoramount1;
                        sundrycreditorcurrbalold = ("" + creditoramount1);
                        sundrycreditorcurrbaltypeold = ("DR");
                    }
                }

                rcraditor_st.executeUpdate("Update ledger SET curr_bal= '" + sundrycreditorcurrbalold + "',currbal_type='" + sundrycreditorcurrbaltypeold + "' where ledger_name='" + sundrycreditorold + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--------------For Purchase account----------------------------------------------------------------------------------------------------------------
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement rpur_st = connect.createStatement();
            String rpurc_type = "";
            String rpurc_amnt = "";
            String rpurc_currbal = "";
            ResultSet rpur_rs = rpur_st.executeQuery("Select * from ledger where ledger_name = '" + Purchaseaccount + "'");
            while (rpur_rs.next()) {
                rpurc_type = rpur_rs.getString("currbal_type");
                rpurc_amnt = rpur_rs.getString("curr_bal");
            }
            ResultSet rpur_rs1 = rpur_st.executeQuery("Select * from bsnl_purchase where Bill_Refrence_No = '" + jTextField2.getText() + "'");

            while (rpur_rs1.next()) {
                rpurc_currbal = rpur_rs1.getString("Total_Basic_Price");
            }

            double rpurchase_amnt = Double.parseDouble(rpurc_amnt);
            double rpurchase_currbal = Double.parseDouble(rpurc_currbal);
            double rpur_bal = 0;

            if (rpurc_type.equals("DR")) {
                rpur_bal = rpurchase_currbal - rpurchase_amnt;
                if (rpur_bal <= 0) {
                    rupdate_purc_type = "DR";
                    rupdate_purc_amnt = Math.abs(rpur_bal);
                } else {
                    rupdate_purc_type = "CR";
                    rupdate_purc_amnt = Math.abs(rpur_bal);
                }

            } else {
                if (rpurc_type.equals("CR")) {
                    rpur_bal = rpurchase_currbal + rpurchase_amnt;
                    if (rpur_bal >= 0) {
                        rupdate_purc_type = "CR";
                        rupdate_purc_amnt = Math.abs(rpur_bal);
                    }
                }
            }

            rpur_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_purc_amnt + "',currbal_type='" + rupdate_purc_type + "' where ledger_name='" + Purchaseaccount + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------
    }
//--------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jDialog1 = new javax.swing.JDialog();
        jLabel25 = new javax.swing.JLabel();
        jTextField20 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        list1 = new java.awt.List();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jTextField18 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jTextField19 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();

        jDialog1.setMinimumSize(new java.awt.Dimension(850, 500));

        jLabel25.setFont(new java.awt.Font("sansserif", 2, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(51, 102, 255));
        jLabel25.setText("Search");

        jTextField20.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField20CaretUpdate(evt);
            }
        });

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Party Code", "Party Name", "City", "State", "Opening Balance", "Balance Type", "Purchase Account", "IGST Applicable"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jButton3.setText("New Party");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel25)
                .addGap(18, 18, 18)
                .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton3)
                .addContainerGap())
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        list1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("BSNL PURCHASE");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel1.setText("Supplier Name");

        jTextField1.setEditable(false);
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel2.setText("Invoice Date");

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jLabel3.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel3.setText("Invoice No");

        jTextField2.setEditable(false);
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel4.setText("Purchase Type");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Cash");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Credit");

        jLabel5.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel5.setText("Party Pur Date");

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jLabel6.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel6.setText("Purchase Ref No");

        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jTextField4.setEditable(false);
        jTextField4.setText("1");
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel7.setText("S No");

        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel8.setText("Quntity");

        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel9.setText("Product");

        jLabel10.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel10.setText("Gross Amount");

        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel11.setText("Basic Price");

        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });
        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S No", "Quantity", "Product", "Gross Amount", "Basic Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel12.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel12.setText("Purchase Amount");

        jTextField8.setEditable(false);
        jTextField8.setText("0");

        jLabel13.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel13.setText("Total Quntity");

        jTextField9.setEditable(false);
        jTextField9.setText("0");

        jLabel14.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel14.setText("Discount");

        jTextField10.setText("0");
        jTextField10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField10FocusLost(evt);
            }
        });
        jTextField10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField10KeyPressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel15.setText("Amnt After Disc");

        jTextField11.setEditable(false);
        jTextField11.setText("0");

        jLabel16.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel16.setText("CGST %");

        jTextField12.setEditable(false);
        jTextField12.setText("0");

        jLabel17.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel17.setText("CGST");

        jTextField13.setEditable(false);
        jTextField13.setText("0");

        jLabel18.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel18.setText("SGST %");

        jTextField14.setEditable(false);
        jTextField14.setText("0");

        jLabel19.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel19.setText("SGST");

        jTextField15.setEditable(false);
        jTextField15.setText("0");

        jLabel20.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel20.setText("IGST %");

        jTextField16.setEditable(false);
        jTextField16.setText("0");
        jTextField16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField16KeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel21.setText("IGST");

        jTextField17.setEditable(false);
        jTextField17.setText("0");

        jLabel22.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel22.setText("Other Charges");

        jTextField18.setText("0");
        jTextField18.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField18FocusLost(evt);
            }
        });
        jTextField18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField18KeyPressed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel23.setText("Bill AMount");

        jTextField19.setEditable(false);
        jTextField19.setText("0");

        jButton1.setText("Save");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancle");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel24.setText("Supplier Id");

        jLabel26.setText("Purchase Account");

        jLabel27.setText("Balance");

        jLabel28.setText("Type");

        jLabel29.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel29.setText("Round Off ");

        jTextField21.setText("0");

        jButton4.setText("Update");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton2)
                                .addGap(40, 40, 40)
                                .addComponent(jLabel26))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel24)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel27)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(20, 20, 20)
                                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(39, 39, 39)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField2)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel13)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel15))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField8)
                                    .addComponent(jTextField9)
                                    .addComponent(jTextField10)
                                    .addComponent(jTextField11, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                                    .addComponent(jTextField21)))
                            .addComponent(jLabel29))
                        .addGap(48, 48, 48)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18)
                            .addComponent(jLabel19))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField12)
                            .addComponent(jTextField13, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField14, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField15, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
                        .addGap(51, 51, 51)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel20)
                                    .addComponent(jLabel21))
                                .addGap(58, 58, 58)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel22)
                                    .addComponent(jLabel23))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField19)
                                    .addComponent(jTextField18))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2))
                            .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel24)
                            .addComponent(jLabel27)
                            .addComponent(jLabel28))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jRadioButton1)
                            .addComponent(jRadioButton2)
                            .addComponent(jLabel26)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel17)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel18)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1)
                    .addComponent(jLabel29)
                    .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog1.setVisible(true);
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField20CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField20CaretUpdate
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        dtm.getDataVector().removeAllElements();
        String code = jTextField20.getText();
        String name = jTextField20.getText();
        String city = jTextField20.getText();
//        if (party.trim().length() > 0) {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT  * FROM Party where party_name LIKE'%" + name + "%' OR party_code  LIKE '%" + code + "%' OR city LIKE '%" + city + "%'");
            while (rs.next()) {
                String igst = "";
                if (rs.getBoolean("IGST") == true) {
                    igst = "True";
                } else {
                    igst = "False";
                }
                Object o[] = {rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(10), rs.getString(11), rs.getString("purchase_account"), igst};
                dtm.addRow(o);
            }

        } catch (Exception e) {
            e.printStackTrace();
//            }

        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField20CaretUpdate

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        master.customer.Add_Purchase_Party addnew = new master.customer.Add_Purchase_Party();
        addnew.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        partyselect();
    }//GEN-LAST:event_jTable2MouseClicked

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField4.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jDateChooser2.requestFocus();
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().isEmpty()) {
                jTextField10.requestFocus();
            } else {
                jComboBox1.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField3.requestFocus();
        }
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().equals("") || jTextField5.getText().equals("0")) {
                JOptionPane.showMessageDialog(rootPane, "Quntity can not be blank or Zero");
                jTextField5.requestFocus();
            } else {
                jTextField6.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField7.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField6.getText().equals("")) {
                JOptionPane.showMessageDialog(rootPane, "Gross Amount Can not be Empty");
                jTextField6.requestFocus();
            } else if (jTextField7.getText().equals("")) {
                JOptionPane.showMessageDialog(rootPane, "Basic Amount Can not be Empty");
                jTextField7.requestFocus();
            } else {
                Sno = jTextField4.getText();
                Product = (String) jComboBox1.getSelectedItem();
                Gross_Amount = Double.parseDouble(jTextField6.getText());
                Basic_Amount = Double.parseDouble(jTextField7.getText());
                if (jRadioButton1.isSelected()) {
                    pur_type = "Cash";
                } else if (jRadioButton2.isSelected()) {
                    pur_type = "Credit";
                }
//                double bill = Double.parseDouble(jTextField7.getText());
                qnty = Double.parseDouble(jTextField5.getText());
                Qnty = qnty + Qnty;
                jTextField9.setText("" + Qnty);

                Object o[] = {Sno, qnty, Product, df.format(Gross_Amount), df.format(Basic_Amount)};
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                dtm.addRow(o);
                ++s;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement sttt = connect.createStatement();
                    Statement stttt = connect.createStatement();
                    Statement pro_st = connect.createStatement();
                    Statement sgst_st = connect.createStatement();
                    Statement cgst_st = connect.createStatement();
                    ResultSet rssss, rs5, rs4;
                    if (isIgstApplicable == true) {
                        ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + Product + "'");
                        while (rsss.next()) {
                            Igst_Account = rsss.getString(3);
                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                            while (rssss.next()) {
                                Igst_Bal = rssss.getDouble("curr_bal");
                                Igst_BalType = rssss.getString("currbal_type");
                                igstpercent = Double.parseDouble(rssss.getString("percent"));
//                                double value1 = Double.parseDouble(jTextField9.getText());

                                iv = (Basic_Amount * igstpercent) / 100;
//                                    trv = (totalamountaftertax - (amntbeforetax+iv));
                            }
                        }
                        jTextField16.setText("" + igstpercent);

                    } else if (isIgstApplicable == false) {
                        ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + Product + "'");
                        while (pro_rs.next()) {
                            Sgst_Account = pro_rs.getString("SGST");
                            Cgst_Account = pro_rs.getString("CGST");
                            ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                            while (sgst_rs.next()) {
                                Sgst_Bal = sgst_rs.getDouble("curr_bal");
                                Sgst_BalType = sgst_rs.getString("currbal_type");
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }
                            jTextField14.setText("" + sgstpercent);
                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
                                Cgst_Bal = cgst_rs.getDouble("curr_bal");
                                Cgst_BalType = cgst_rs.getString("currbal_type");
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }
                            jTextField12.setText("" + cgstpercent);
                        }

                        totalpercent = sgstpercent + cgstpercent;
//                        double value1 = Double.parseDouble(jTextField9.getText());

                        sv = (Basic_Amount * sgstpercent) / 100;
                        cv = (Basic_Amount * cgstpercent) / 100;

                    }

                    tiv = iv + tiv;
                    tcv = cv + tcv;
                    tsv = sv + tsv;

                } catch (Exception e) {
                    e.printStackTrace();
                }
                discount_amount = Double.parseDouble(jTextField10.getText());

                total_basic_amount = Basic_Amount + total_basic_amount;
                total_gross_amount = Gross_Amount + total_gross_amount;
                total_net_amount = total_basic_amount + tiv + tcv + tsv + other_charges_amount + discount_amount;

                jTextField4.setText("" + s);

                jTextField5.setText(null);
                jTextField6.setText(null);
                jTextField7.setText(null);

                jTextField8.setText(df.format(total_basic_amount));
                jTextField11.setText(df.format(total_basic_amount));
                jTextField13.setText(df.format(tcv));
                jTextField15.setText(df.format(tsv));
                jTextField17.setText(df.format(tiv));
                jTextField19.setText(df.format(total_net_amount));
                trv = Double.parseDouble(jTextField19.getText()) - (Double.parseDouble(jTextField10.getText()) + Double.parseDouble(jTextField8.getText()) + Double.parseDouble(jTextField13.getText()) + Double.parseDouble(jTextField15.getText()) + Double.parseDouble(jTextField17.getText()) + Double.parseDouble(jTextField18.getText()));
                jTextField21.setText(df.format(trv));
                jTextField5.requestFocus();

            }

        }
        if (key == evt.VK_ESCAPE) {
            jTextField6.requestFocus();
        }
    }//GEN-LAST:event_jTextField7KeyPressed

    private void jTextField16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField16KeyPressed

    }//GEN-LAST:event_jTextField16KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int response = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save ?");
        if (response == 0) {
            save();
            JOptionPane.showMessageDialog(rootPane, "Data save successfully !!!");
            this.dispose();
            transaction.Bsnl_Purchase bsp = new transaction.Bsnl_Purchase();
            bsp.setVisible(true);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_DELETE) {
            iv = 0;
            sv = 0;
            cv = 0;
            int row = jTable1.getSelectedRow();
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            Basic_Amount = Double.parseDouble(jTable1.getValueAt(row, 4) + "");
            total_basic_amount = total_basic_amount - Basic_Amount;
            qnty = Double.parseDouble(jTable1.getValueAt(row, 1) + "");
            Qnty = Qnty - qnty;
            try {

                connection c = new connection();
                Connection connect = c.cone();
                Statement sttt = connect.createStatement();
                Statement stttt = connect.createStatement();
                Statement pro_st = connect.createStatement();
                Statement sgst_st = connect.createStatement();
                Statement cgst_st = connect.createStatement();
                ResultSet rssss, rs5, rs4;
                if (isIgstApplicable == true) {
                    ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "" + "'");
                    while (rsss.next()) {
                        rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                        while (rssss.next()) {
                            igstpercent = Double.parseDouble(rssss.getString("percent"));
                            iv = (Double.parseDouble(jTable1.getValueAt(row, 4) + "") * igstpercent) / 100;
                        }
                    }
                    jTextField16.setText("" + igstpercent);

                } else if (isIgstApplicable == false) {
                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "" + "'");
                    while (pro_rs.next()) {
                        ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                        while (sgst_rs.next()) {
                            sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                        }
//                        jTextField16.setText("" + igstpercent);
                        ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                        while (cgst_rs.next()) {
                            cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                        }
                    }
                    totalpercent = sgstpercent + cgstpercent;
                    sv = (Double.parseDouble(jTable1.getValueAt(row, 4) + "") * sgstpercent) / 100;
                    cv = (Double.parseDouble(jTable1.getValueAt(row, 4) + "") * cgstpercent) / 100;
                }

                jTextField12.setText("" + cgstpercent);
                jTextField14.setText("" + sgstpercent);

                tiv = tiv - iv;
                tcv = tcv - cv;
                tsv = tsv - sv;

            } catch (Exception e) {
                e.printStackTrace();
            }

            dtm.removeRow(row);
            dtm.fireTableDataChanged();
            --s;
            int a = Integer.parseInt(jTextField4.getText());
            a = a - 1;
            jTextField4.setText("" + a);
            total_net_amount = total_basic_amount + tiv + tcv + tsv - discount_amount + other_charges_amount;
            jTextField8.setText(df.format(total_basic_amount));
            jTextField13.setText(df.format(tcv));
            jTextField15.setText(df.format(tsv));
            jTextField17.setText(df.format(tiv));
            jTextField19.setText(df.format(total_net_amount));
            jTextField9.setText(df.format(Qnty));
            double dam = Double.parseDouble(jTextField8.getText()) - Double.parseDouble(jTextField10.getText());
            jTextField11.setText("" + dam);
            trv = Double.parseDouble(jTextField19.getText()) - (Double.parseDouble(jTextField10.getText()) + Double.parseDouble(jTextField8.getText()) + Double.parseDouble(jTextField13.getText()) + Double.parseDouble(jTextField15.getText()) + Double.parseDouble(jTextField17.getText()) + Double.parseDouble(jTextField18.getText()));
            jTextField21.setText(df.format(trv));
        }


    }//GEN-LAST:event_jTable1KeyPressed

    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jTextField10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField10FocusLost
        // TODO add your handling code here:
        double discount_amount = Double.parseDouble(jTextField10.getText());
        if (discount_amount == 0) {

        } else {

            total_net_amount = 0;
            total_basic_amount = 0;
            total_gross_amount = 0;
            tiv = 0;
            tcv = 0;
            tsv = 0;
            double totalcgstamnt = 0;
            double totaligstamnt = 0;
            double totalamountbeforetax = 0;
            double amountbeforetax = 0;
            int rowcount = jTable1.getRowCount();
            for (int i = 0; i < rowcount; i++) {

                double discountpercentage = (discount_amount / Double.parseDouble(jTextField8.getText())) * 100;

                double totalafterdiscountpercentage = (discountpercentage / 100) * Double.parseDouble(jTable1.getValueAt(i, 4) + "");
                Basic_Amount = Double.parseDouble(jTable1.getValueAt(i, 4) + "") - totalafterdiscountpercentage;

                jTable1.setValueAt(df.format(Basic_Amount), i, 4);

                String igstaccount = "";
                String sgstaccount = "";
                String cgstaccount = "";
                double gstperc = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stmt1 = connect.createStatement();

                    if (isIgstApplicable == true) {
                        ResultSet rsigst = stmt1.executeQuery("select IGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rsigst.next()) {
                            igstaccount = rsigst.getString(1);
                        }
                        ResultSet rsIGSTperc = stmt1.executeQuery("select percent from ledger where ledger_name ='" + igstaccount + "'");
                        while (rsIGSTperc.next()) {
                            gstperc = rsIGSTperc.getDouble(1);
                        }

                        iv = (gstperc * Basic_Amount) / 100;

                        tiv = iv + tiv;

                        total_basic_amount = Basic_Amount + total_basic_amount;

                    } else if (isIgstApplicable == false) {
                        ResultSet rssgst = stmt1.executeQuery("select SGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rssgst.next()) {
                            sgstaccount = rssgst.getString(1);
                        }
                        ResultSet rscgst = stmt1.executeQuery("select CGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rscgst.next()) {
                            cgstaccount = rscgst.getString(1);
                        }
                        ResultSet rsSGSTperc = stmt1.executeQuery("select percent from ledger where ledger_name ='" + sgstaccount + "'");
                        while (rsSGSTperc.next()) {
                            gstperc = rsSGSTperc.getDouble(1);
                        }

                        cv = (gstperc * Basic_Amount) / 100;

                        tcv = cv + tcv;
                        sv = cv;
                        tsv = tcv;
                        total_basic_amount = Basic_Amount + total_basic_amount;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            total_net_amount = total_basic_amount + tiv + tcv + tsv + other_charges_amount;
            //    DecimalFormat df=new DecimalFormat("0.00");
            //    jTextField8.setText(df.format(total_basic_amount));
            jTextField13.setText(df.format(tcv));
            jTextField15.setText(df.format(tsv));
            jTextField17.setText(df.format(tiv));
            jTextField19.setText(df.format(total_net_amount));

        }


    }//GEN-LAST:event_jTextField10FocusLost

    private void jTextField10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField10KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_ENTER) {
            calculation();
            jTextField18.requestFocus();
        }

    }//GEN-LAST:event_jTextField10KeyPressed

    private void jTextField18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField18KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (Double.parseDouble(jTextField18.getText()) != 0) {
                jDialog2.setVisible(true);
            } else {
                jTextField19.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField10.requestFocus();
        }
    }//GEN-LAST:event_jTextField18KeyPressed

    private void list1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list1ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();
        jTextField19.requestFocus();
        if (list1.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger create = new reporting.accounts.createLedger();
            create.setVisible(true);
            create.jComboBox1.setSelectedItem("DIRECT EXPENSES");
        } else {
            other_charges_acc = list1.getSelectedItem();
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet other_rs = st.executeQuery("SELECT * from ledger where ledger_name='" + other_charges_acc + "'");
                while (other_rs.next()) {
                    other_charges_bal = other_rs.getDouble("curr_bal");
                    other_charges_bal_type = other_rs.getString("currbal_type");
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
    }//GEN-LAST:event_list1ActionPerformed

    private void jTextField18FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField18FocusLost
        if (Double.parseDouble(jTextField18.getText()) == 0) {
            jTextField19.requestFocus();
        } else if (Double.parseDouble(jTextField18.getText()) != 0) {
            jDialog2.setVisible(true);
            calculation();
            list1.removeAll();
            list1.addItem("ADD NEW");
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                String duties = null;
                String Duteis = "DIRECT EXPENSES";
                ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
                while (rs1.next()) {
                    duties = rs1.getString(1);
                }
                ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "'");
                while (rs.next()) {
                    list1.add(rs.getString(1));
                }
            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }

    }//GEN-LAST:event_jTextField18FocusLost

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Update");
        if (i == 0) {
            updatereversebalances();
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement stmt = connect.createStatement();
                stmt.executeUpdate("delete from bsnl_purchase where Bill_Refrence_No='" + jTextField2.getText() + "'");
                save();

            } catch (Exception e) {
                e.printStackTrace();
            }
            JOptionPane.showMessageDialog(rootPane, "Your Purchase Entry is Updated Successfully");
            dispose();
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Purchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Purchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Purchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bsnl_Purchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Bsnl_Purchase().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboBox1;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    public com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    public javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    public javax.swing.JLabel jLabel26;
    public javax.swing.JLabel jLabel27;
    public javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JRadioButton jRadioButton1;
    public javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    public javax.swing.JTextField jTextField17;
    public javax.swing.JTextField jTextField18;
    public javax.swing.JTextField jTextField19;
    public javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    public javax.swing.JTextField jTextField21;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    public javax.swing.JTextField jTextField6;
    public javax.swing.JTextField jTextField7;
    public javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    private java.awt.List list1;
    // End of variables declaration//GEN-END:variables
}
