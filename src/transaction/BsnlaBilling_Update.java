package transaction;

import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Aashish
 */
public class BsnlaBilling_Update extends javax.swing.JFrame {

    /**
     * Creates new form BsnlaBilling_Update
     */
     private static BsnlaBilling_Update obj = null;
    public BsnlaBilling_Update() {
        initComponents();
        getData();
    }
      public static BsnlaBilling_Update getObj() {
        if (obj == null) {
            obj = new BsnlaBilling_Update();
        }
        return obj;
    }
//----------------Method for default data---------------------------------------

    public void getData() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("Select * from bsnl_billing_gst group by Bill_Refrence_No order by length(Bill_Refrence_No), Bill_Refrence_No");
            while (rs.next()) {
                Object o[] = {rs.getString("Bill_date"), rs.getString("Bill_Refrence_No"), rs.getString("Total_Quntity"), rs.getString("Total_Net_Amount")};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    
//----------------Method to move Data to another form---------------------------
    public void moveData() {
        Bsnl_Billing_GST bbg = new Bsnl_Billing_GST();
        this.setVisible(false);
        bbg.setVisible(true);
        bbg.jButton1.setVisible(false);
        bbg.jButton6.setVisible(true);
        DefaultTableModel bsnl_dtm = (DefaultTableModel) bbg.jTable1.getModel();
        try {
            int row = jTable1.getSelectedRow();
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("Select * from bsnl_billing_gst where Bill_Refrence_No = '" + jTable1.getValueAt(row, 1) + "'");
            while (rs.next()) {
                bbg.jTextField1.setText("" + rs.getString("Bill_Refrence_No"));
                bbg.jDateChooser1.setDate(rs.getDate("Bill_Date"));
                bbg.jComboBox2.setSelectedItem(rs.getString("Customer_Name") + "/" + rs.getString("Mobile_No"));
                if (rs.getString("Type").equals("BSNL CASH")) {
                    bbg.jRadioButton1.setSelected(true);
                } else {
                    bbg.jRadioButton2.setSelected(true);
                    bbg.jTextField2.setText("" + rs.getString("Customer_Account"));
                    bbg.jTextField3.setText("" + rs.getString("Mobile_No"));
                }
                bbg.sno = Integer.parseInt(rs.getString("Sno")) + 1;
                bbg.jTextField4.setText(""+bbg.sno);
                Object o[] = {rs.getString("sno"), rs.getString("Product"), rs.getString("Qnty"), rs.getString("Gross_Amount"), rs.getString("Basic_Amount"), rs.getString("Cgst_Amount"), rs.getString("Sgst_Amount"), rs.getString("Igst_Amount"), rs.getString("Net_Amount")};
                bsnl_dtm.addRow(o);

                bbg.jTextField9.setText("" + rs.getString("Total_Quntity"));
                bbg.jTextField10.setText("" + rs.getString("Total_Basic_Amount"));
                bbg.jTextField11.setText("" + rs.getString("Total_Gross_Amount"));
                bbg.jTextField15.setText("" + rs.getString("Total_Net_Amount"));
                bbg.jTextField12.setText("" + rs.getString("Total_Igst_Amount"));
                bbg.jTextField13.setText("" + rs.getString("Total_Sgst_Amount"));
                bbg.jTextField14.setText("" + rs.getString("Total_Cgst_Amount"));
                bbg.jTextField16.setText("" + rs.getString("Round_Off"));
                
                bbg.Quntity = rs.getDouble("Total_Quntity");
                bbg.Basic_Price = rs.getDouble("Total_Basic_Amount");
                bbg.Gross_Amount = rs.getDouble("Total_Gross_Amount");
                bbg.Net_Amount = rs.getDouble("Total_Net_Amount");
                bbg.Igst_Value =  rs.getDouble("Total_Igst_Amount");
                bbg.Sgst_Value =  rs.getDouble("Total_Sgst_Amount");
                bbg.Cgst_Value =  rs.getDouble("Total_Cgst_Amount");
                bbg.Round_off = rs.getDouble("Round_Off");
                bbg.old_igst_account = rs.getString("Igst_Account");
                bbg.old_sgst_account = rs.getString("Sgst_Account");
                bbg.old_cgst_account = rs.getString("Cgst_Account");
               
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Bsnl Billing Update");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill Ref", "Total Qunt", "Total Amount"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        moveData();
    }//GEN-LAST:event_jTable1MouseClicked

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
         obj = null;
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BsnlaBilling_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BsnlaBilling_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BsnlaBilling_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BsnlaBilling_Update.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BsnlaBilling_Update().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
