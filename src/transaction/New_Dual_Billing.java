package transaction;

//import Printing.Bill_Print;
import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.List;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Golu
 */
public class New_Dual_Billing extends javax.swing.JFrame implements Printable {

    public List list;
    public String xz[];
    String sundrydebtors = null;
    String cardpaymentaccount = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    public SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    public SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String Party_code;

    ArrayList party = new ArrayList();

    public boolean isIgstApplicable = false;
    public double diff_total = 0;
    String diff_total_round;
    double amntbeforetax = 0;
    double tiv = 0;
    double tsv = 0;
    double tcv = 0;
    double trv = 0;
    double iv = 0;
    double sv = 0;
    double cv = 0;
    public String Round_currbal_type = null;

    public String Sgst_Account = null;
    public String Cgst_Account = null;
    public double igst_amnt = 0;
    public double sgst_total_amnt = 0;
    public double cgst_total_amnt = 0;
    String tax_type = "Inclusive";
    String Barcode;
    String Rate;
    String Company;
    String Tax_Type_Status;
    String Year;

    private static New_Dual_Billing obj = null;

    public New_Dual_Billing() {
        try {
            combo();
            initComponents();
            this.setLocationRelativeTo(null);
            jDialog5.setLocationRelativeTo(null);
            jDialog6.setLocationRelativeTo(null);
            jRadioButton3.setSelected(true);
            jLabel51.setText("");
            jTextField5.requestFocus();
            jLabel23.setVisible(false);
            jLabel24.setVisible(false);
            jLabel49.setVisible(false);
            jTextField39.setVisible(false);
            jLabel27.setText("" + currentDate);
            jRadioButton1.setSelected(true);
            jButton5.setVisible(false);
            jButton6.setVisible(false);
            jButton7.setVisible(false);
            jTextField40.setEnabled(false);
            jDateChooser2.setEnabled(false);
            if (jRadioButton1.isSelected()) {
                String c = "CASH";
                try {
                    connection c1 = new connection();
                    Connection connect = c1.cone();
                    Statement st2 = connect.createStatement();
                    Statement st3 = connect.createStatement();
                    Statement st4 = connect.createStatement();
                    ResultSet rs2 = null;
                    rs2 = st2.executeQuery("select * from ledger where ledger_name ='" + c + "' ");
                    while (rs2.next()) {
                        String cash = rs2.getString(1);
                        jLabel23.setText(rs2.getString(10));
                        jLabel24.setText(rs2.getString(11));
                    }
                    ResultSet rs3 = st3.executeQuery("select * from ledger where groups='BANK ACCOUNTS'");
                    while (rs3.next()) {
                        jComboBox2.addItem(rs3.getString(1));
                    }
                    ResultSet pro_rs = st4.executeQuery("select * from product");
                    while (pro_rs.next()) {
                        jComboBox4.addItem(pro_rs.getString("product_code"));
                    }
                    ArrayList srref = new ArrayList();
                    ResultSet rssrref = st3.executeQuery("select sr_ref from billing ");
                    while (rssrref.next()) {
                        srref.add(rssrref.getString(1));
                    }
                    ResultSet rs4 = st3.executeQuery("SELECT Distinct(bill_refrence) FROM salesreturn  ");

                    while (rs4.next()) {

                        if (srref.contains(rs4.getString("bill_refrence"))) {

                        } else {
                            jComboBox3.addItem(rs4.getString("bill_refrence"));
                        }
                    }

                    ResultSet rs_year = st4.executeQuery("Select * from year");
                    while (rs_year.next()) {
                        Year = rs_year.getString("year");
                    }

                    ResultSet staff_rs = st4.executeQuery("SELECT  * FROM staff");
                    while (staff_rs.next()) {
                        jComboBox5.addItem(staff_rs.getString("Emp_code"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            jTextField3.setEnabled(true);
            Calendar currentDate = Calendar.getInstance();

            String dateNow = formatter.format(currentDate.getTime());
            jDateChooser1.setDate(formatter.parse(dateNow));
            try {
                java.util.Date dateafter = formatter.parse("20" + getyear + "-03-31");
                java.util.Date datenow = formatter.parse(dateNow);
                if (datenow.before(dateafter)) {
                    getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                    nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            String sales = "SL/";
            String slash = "/";
            String Branchno = "1/";
//            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String yearwise = Year.concat(slash);
            String finalconcat = ((sales.concat(yearwise)));
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();

                ResultSet rs = st.executeQuery("SELECT bill_refrence FROM billing where branchid = '0' ");
                int a = 0, b, d = 0;
                int i = 0;
                while (rs.next()) {
                    if (i == 0) {
                        d = rs.getString(1).lastIndexOf("/");
                        i++;
                    }
                    b = Integer.parseInt(rs.getString(1).substring(d + 1));
                    if (a < b) {
                        a = b;
                    }
                }
                String s = finalconcat.concat(Integer.toString(a + 1));
                jTextField1.setText("" + s);
            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement setting_st = connect.createStatement();
            ResultSet rs = st.executeQuery("select ledger_name from ledger where groups ='BANK ACCOUNTS'");
            while (rs.next()) {
                list1.addItem(rs.getString(1));
            }
            ResultSet setting_rs = setting_st.executeQuery("Select * from setting");
            while (setting_rs.next()) {
                Barcode = setting_rs.getString("Barcode");
                Rate = setting_rs.getString("Rate");
                Company = setting_rs.getString("Company");
                Tax_Type_Status = setting_rs.getString("Tax_Type");
            }
            if (Barcode.equals("Enable")) {
                jComboBox4.setEnabled(false);
            } else if (Barcode.equals("Disable")) {
                jTextField5.setEnabled(false);
            }
            if (Tax_Type_Status.equals("Enable")) {
                jRadioButton3.setEnabled(true);
                jRadioButton4.setEnabled(true);
            } else if (Tax_Type_Status.equals("Disable")) {
                jRadioButton3.setEnabled(false);
                jRadioButton4.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static New_Dual_Billing getObj() {
        if (obj == null) {
            obj = new New_Dual_Billing();
        }
        return obj;
    }

    private Object makeObj(final String item) {
        return new Object() {
            @Override
            public String toString() {
                return item;
            }
        };
    }
    //      rs = st.executeQuery("SELECT party_code from Party");

    public void combo() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st1 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("SELECT * from Staff");
            int a1 = 0;
            while (rs1.next()) {
                a1++;
            }
            xz = new String[a1];
            rs1 = st1.executeQuery("SELECT * from Staff");
            a1 = 0;
            while (rs1.next()) {
                xz[a1] = rs1.getString(1);
                a1++;
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jDialog1 = new javax.swing.JDialog();
        jLabel26 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jTextField24 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel33 = new javax.swing.JLabel();
        jTextField25 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jDialog2 = new javax.swing.JDialog();
        list1 = new java.awt.List();
        jDialog3 = new javax.swing.JDialog();
        jTextField26 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jTextField27 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jTextField28 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jTextField29 = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton8 = new javax.swing.JButton();
        jDialog4 = new javax.swing.JDialog();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jTextField34 = new javax.swing.JTextField();
        jTextField35 = new javax.swing.JTextField();
        jTextField36 = new javax.swing.JTextField();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jTextField39 = new javax.swing.JTextField();
        jButton12 = new javax.swing.JButton();
        buttonGroup3 = new javax.swing.ButtonGroup();
        jComboBox5 = new javax.swing.JComboBox<>();
        jDialog5 = new javax.swing.JDialog();
        jLabel54 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jButton13 = new javax.swing.JButton();
        jLabel56 = new javax.swing.JLabel();
        jRadioButton6 = new javax.swing.JRadioButton();
        jRadioButton7 = new javax.swing.JRadioButton();
        jLabel57 = new javax.swing.JLabel();
        jTextField40 = new javax.swing.JTextField();
        jLabel58 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel59 = new javax.swing.JLabel();
        jTextField41 = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        jTextField42 = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jLabel63 = new javax.swing.JLabel();
        jTextField44 = new javax.swing.JTextField();
        jLabel64 = new javax.swing.JLabel();
        jTextField45 = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        jComboBox6 = new javax.swing.JComboBox<>();
        jTextField46 = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        jTextField43 = new javax.swing.JTextField();
        buttonGroup4 = new javax.swing.ButtonGroup();
        jDialog6 = new javax.swing.JDialog();
        jLabel67 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel68 = new javax.swing.JLabel();
        jTextField47 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jTextField13 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jTextField16 = new javax.swing.JTextField();
        jTextField17 = new javax.swing.JTextField();
        jTextField18 = new javax.swing.JTextField();
        jTextField19 = new javax.swing.JTextField();
        jTextField20 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox(xz);
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jRadioButton5 = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jTextField2 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jTextField30 = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jTextField31 = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        jTextField32 = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jTextField33 = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jTextField37 = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        jTextField38 = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel51 = new javax.swing.JLabel();
        jComboBox4 = new javax.swing.JComboBox<>();
        jLabel52 = new javax.swing.JLabel();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel53 = new javax.swing.JLabel();

        jDialog1.setMinimumSize(new java.awt.Dimension(451, 396));

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel26.setText("Cheque Entry");

        jLabel22.setText("Date: -");

        jLabel27.setText("jLabel27");

        jLabel28.setText("Name: -");

        jLabel29.setText("Cheque No. : -");

        jLabel30.setText("Payee Bank: -");

        jLabel31.setText("Amount: -");

        jLabel32.setText("To Account :-");

        jLabel33.setText("Date of Cheque: -");

        jButton4.setText("CANCEL");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton3.setText("OK");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30)
                            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jDialog1Layout.createSequentialGroup()
                                    .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel29)
                                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jDialog1Layout.createSequentialGroup()
                                            .addGap(56, 56, 56)
                                            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jTextField23)
                                                .addComponent(jTextField24)
                                                .addComponent(jComboBox2, 0, 117, Short.MAX_VALUE)
                                                .addComponent(jTextField25))))))))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel33)))
                .addContainerGap(186, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addGap(77, 77, 77)
                        .addComponent(jButton4)
                        .addGap(154, 154, 154))))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jLabel27))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(jTextField25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32))
                .addGap(27, 27, 27)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4)
                    .addComponent(jButton3))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(200, 400));

        list1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );

        jDialog3.setMinimumSize(new java.awt.Dimension(682, 580));

        jTextField26.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField26CaretUpdate(evt);
            }
        });
        jTextField26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField26ActionPerformed(evt);
            }
        });
        jTextField26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField26KeyPressed(evt);
            }
        });

        jLabel34.setText("Enter Party code :");

        jTextField27.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField27CaretUpdate(evt);
            }
        });
        jTextField27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField27ActionPerformed(evt);
            }
        });
        jTextField27.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField27KeyPressed(evt);
            }
        });

        jLabel35.setText("Enter Party Name :");

        jTextField28.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField28CaretUpdate(evt);
            }
        });
        jTextField28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField28KeyPressed(evt);
            }
        });

        jLabel36.setText("Enter city :");

        jLabel37.setText("Enter State :");

        jTextField29.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField29CaretUpdate(evt);
            }
        });
        jTextField29.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField29KeyPressed(evt);
            }
        });

        jScrollPane5.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "party_code", "party_name", "city", "state", "opening_bal", "Balance Type", "IGST Applicable"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTable3);

        jScrollPane5.setViewportView(jScrollPane4);

        jButton8.setText("ADD NEW PARTY");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 711, Short.MAX_VALUE)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addComponent(jLabel37)
                        .addGap(44, 44, 44)
                        .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jDialog3Layout.createSequentialGroup()
                            .addComponent(jLabel36)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog3Layout.createSequentialGroup()
                                .addComponent(jLabel35)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog3Layout.createSequentialGroup()
                                .addComponent(jLabel34)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34))
                .addGap(23, 23, 23)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addGap(22, 22, 22)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jDialog4.setMinimumSize(new java.awt.Dimension(500, 300));

        jLabel42.setText("Amount by Cash");

        jLabel43.setText("Amount by Debit");

        jLabel44.setText("Amount by Card");

        jTextField34.setText("0");
        jTextField34.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField34KeyPressed(evt);
            }
        });

        jTextField35.setText("0");
        jTextField35.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField35KeyPressed(evt);
            }
        });

        jTextField36.setText("0");
        jTextField36.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField36KeyPressed(evt);
            }
        });

        jButton9.setText("SAVE");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jButton9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton9KeyPressed(evt);
            }
        });

        jButton10.setText("SELECT DEBTORS");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setText("SELECT BANK");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jLabel47.setText("TOTAL AMNT : ");

        jLabel49.setText("Invoice No. : -");

        jButton12.setText("UPDATE");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog4Layout = new javax.swing.GroupLayout(jDialog4.getContentPane());
        jDialog4.getContentPane().setLayout(jDialog4Layout);
        jDialog4Layout.setHorizontalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog4Layout.createSequentialGroup()
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField34)
                            .addComponent(jTextField35, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
                    .addGroup(jDialog4Layout.createSequentialGroup()
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton12)
                            .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextField36, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jDialog4Layout.createSequentialGroup()
                                    .addComponent(jTextField39)
                                    .addGap(10, 10, 10))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                        .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel48)
                        .addGap(80, 80, 80))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                        .addComponent(jButton9)
                        .addGap(163, 163, 163))))
        );
        jDialog4Layout.setVerticalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog4Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(jLabel48))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42))
                .addGap(18, 18, 18)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel43)
                    .addComponent(jTextField35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton10))
                .addGap(23, 23, 23)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44)
                    .addComponent(jTextField36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(jTextField39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton9)
                    .addComponent(jButton12))
                .addContainerGap())
        );

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));

        jDialog5.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jDialog5.setTitle("Customer Details");
        jDialog5.setPreferredSize(new java.awt.Dimension(670, 395));
        jDialog5.setSize(new java.awt.Dimension(670, 395));

        jLabel54.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel54.setText("Customer Details - ");

        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel55.setText("Customer Name -");

        jButton13.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButton13.setText("Submit");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });
        jButton13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton13KeyPressed(evt);
            }
        });

        jLabel56.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel56.setText("Is Married -");

        buttonGroup4.add(jRadioButton6);
        jRadioButton6.setText("Yes");
        jRadioButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton6ActionPerformed(evt);
            }
        });
        jRadioButton6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jRadioButton6KeyPressed(evt);
            }
        });

        buttonGroup4.add(jRadioButton7);
        jRadioButton7.setText("No");
        jRadioButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton7ActionPerformed(evt);
            }
        });
        jRadioButton7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jRadioButton7KeyPressed(evt);
            }
        });

        jLabel57.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel57.setText("Spouse Name -");

        jTextField40.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField40KeyPressed(evt);
            }
        });

        jLabel58.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel58.setText("Anniversary -");

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jLabel59.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel59.setText("Mobile No -");

        jTextField41.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField41FocusLost(evt);
            }
        });
        jTextField41.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField41KeyPressed(evt);
            }
        });

        jLabel60.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel60.setText("Whatsapp No -");

        jTextField42.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField42FocusLost(evt);
            }
        });
        jTextField42.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField42KeyPressed(evt);
            }
        });

        jLabel61.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel61.setText("Email Id -");

        jLabel62.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel62.setText("Date of Birth -");

        jDateChooser3.setDateFormatString("dd-MM-yyy");

        jLabel63.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel63.setText("Address 1 -");

        jTextField44.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField44KeyPressed(evt);
            }
        });

        jLabel64.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel64.setText("Address 2 -");

        jTextField45.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField45KeyPressed(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel65.setText("City -");

        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " ", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar ", "Goa", "Gujarat ", "Haryana", "Himachal Pradesh ", "Jammu & Kashmir ", "Karnataka ", "Kerala ", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya ", "Mizoram ", "Nagaland", "Orissa", "Punjab", "Rajasthan", "Sikkim ", "Tamil Nadu", "Tripura", "Uttar Pradesh", "West Bengal", "Chhattisgarh", "Uttarakhand", "Jharkhand", "Telangana" }));
        jComboBox6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox6KeyPressed(evt);
            }
        });

        jTextField46.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField46KeyPressed(evt);
            }
        });

        jLabel66.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel66.setText("State -");

        jTextField43.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField43KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jDialog5Layout = new javax.swing.GroupLayout(jDialog5.getContentPane());
        jDialog5.getContentPane().setLayout(jDialog5Layout);
        jDialog5Layout.setHorizontalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2)
            .addGroup(jDialog5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addComponent(jLabel59)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                        .addComponent(jTextField41, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel55)
                            .addComponent(jLabel57))
                        .addGap(18, 18, 18)
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField6, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                            .addComponent(jTextField40)))
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel61)
                            .addComponent(jLabel63)
                            .addComponent(jLabel65))
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jDialog5Layout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField44)
                                    .addComponent(jTextField46)))
                            .addGroup(jDialog5Layout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(jTextField43)))))
                .addGap(21, 21, 21)
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel58)
                            .addComponent(jLabel56))
                        .addGap(27, 27, 27)
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jDialog5Layout.createSequentialGroup()
                                .addComponent(jDateChooser2, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                                .addGap(11, 11, 11))
                            .addGroup(jDialog5Layout.createSequentialGroup()
                                .addComponent(jRadioButton6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jRadioButton7)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel60)
                            .addComponent(jLabel62)
                            .addComponent(jLabel64)
                            .addComponent(jLabel66))
                        .addGap(18, 18, 18)
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox6, 0, 219, Short.MAX_VALUE)
                            .addGroup(jDialog5Layout.createSequentialGroup()
                                .addComponent(jDateChooser3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(5, 5, 5))
                            .addComponent(jTextField42)
                            .addComponent(jTextField45))
                        .addContainerGap())))
            .addGroup(jDialog5Layout.createSequentialGroup()
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel54))
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGap(273, 273, 273)
                        .addComponent(jButton13)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDialog5Layout.setVerticalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel54)
                .addGap(2, 2, 2)
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jDialog5Layout.createSequentialGroup()
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel55)
                                    .addComponent(jLabel56)
                                    .addComponent(jRadioButton6)
                                    .addComponent(jRadioButton7))
                                .addGap(18, 18, 18)
                                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel57)
                                    .addComponent(jTextField40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel58)))
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel59)
                            .addComponent(jTextField41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel60)
                            .addComponent(jTextField42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel61)
                            .addComponent(jLabel62)
                            .addComponent(jTextField43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel63)
                    .addComponent(jTextField44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel64)
                    .addComponent(jTextField45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel65)
                    .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel66))
                .addGap(27, 27, 27)
                .addComponent(jButton13)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jDialog6.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jDialog6.setTitle("Customer Info");
        jDialog6.setSize(new java.awt.Dimension(529, 401));

        jLabel67.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel67.setText("Customer Info -");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Customer Name", "Mobile  Number", "Whatsapp Number", "City"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jLabel68.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel68.setText("Search -");

        jTextField47.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField47CaretUpdate(evt);
            }
        });

        javax.swing.GroupLayout jDialog6Layout = new javax.swing.GroupLayout(jDialog6.getContentPane());
        jDialog6.getContentPane().setLayout(jDialog6Layout);
        jDialog6Layout.setHorizontalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel67)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel68)
                .addGap(18, 18, 18)
                .addComponent(jTextField47, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jSeparator3)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
        );
        jDialog6Layout.setVerticalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel67)
                    .addComponent(jLabel68)
                    .addComponent(jTextField47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : BILLING");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setText("Bill Reference :");

        jLabel2.setText("Date :");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Cash");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Debit");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jLabel3.setText("Customer Name :");

        jLabel4.setText("Customer A/C No. :");

        jTextField1.setEditable(false);
        jTextField1.setText("1");
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTextField3.setEnabled(false);
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jTextField4.setEditable(false);
        jTextField4.setEnabled(false);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel5.setText("Stock No.");

        jLabel6.setText("Product");

        jLabel7.setText("Rate");

        jLabel8.setText("Quantity");

        jLabel9.setText("Value");

        jLabel10.setText("Description");

        jLabel11.setText("Disc %");

        jLabel12.setText("Disc Amt");

        jLabel13.setText("Total");

        jLabel14.setText("Staff: -");

        jTextField5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField5FocusLost(evt);
            }
        });
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });
        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
        });

        jTextField8.setText("1");
        jTextField8.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField8FocusLost(evt);
            }
        });
        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jTextField9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField9ActionPerformed(evt);
            }
        });
        jTextField9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField9KeyPressed(evt);
            }
        });

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });
        jTextField10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField10KeyPressed(evt);
            }
        });

        jTextField11.setEditable(false);
        jTextField11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField11KeyPressed(evt);
            }
        });

        jTextField12.setEditable(false);
        jTextField12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField12FocusLost(evt);
            }
        });
        jTextField12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField12ActionPerformed(evt);
            }
        });
        jTextField12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField12KeyPressed(evt);
            }
        });

        jTextField13.setEditable(false);
        jTextField13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField13FocusGained(evt);
            }
        });
        jTextField13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField13KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SNo.", "Stock No.", "Product", "Rate", "Quantity", "Value", "Description ", "Disc %", "Disc Amt", "Total", "Staff"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setResizable(false);
            jTable1.getColumnModel().getColumn(8).setResizable(false);
            jTable1.getColumnModel().getColumn(9).setResizable(false);
            jTable1.getColumnModel().getColumn(10).setCellEditor(new DefaultCellEditor(jComboBox5));
        }

        jLabel15.setText("Total Amount");

        jLabel16.setText("Quantity");

        jLabel17.setText("Disc Amount");

        jLabel18.setText("Amount After Discount");

        jLabel19.setText("Value Before Tax");

        jLabel20.setText("Net Amount");

        jTextField15.setEditable(false);
        jTextField15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField15ActionPerformed(evt);
            }
        });
        jTextField15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField15KeyPressed(evt);
            }
        });

        jTextField16.setEditable(false);
        jTextField16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField16KeyPressed(evt);
            }
        });

        jTextField17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField17FocusLost(evt);
            }
        });
        jTextField17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField17ActionPerformed(evt);
            }
        });
        jTextField17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField17KeyPressed(evt);
            }
        });

        jTextField18.setEditable(false);
        jTextField18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField18ActionPerformed(evt);
            }
        });
        jTextField18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField18KeyPressed(evt);
            }
        });

        jTextField19.setEditable(false);
        jTextField19.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField19FocusLost(evt);
            }
        });
        jTextField19.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField19KeyPressed(evt);
            }
        });

        jTextField20.setEditable(false);
        jTextField20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField20FocusLost(evt);
            }
        });
        jTextField20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField20ActionPerformed(evt);
            }
        });
        jTextField20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField20KeyPressed(evt);
            }
        });

        jComboBox1.setSelectedItem(null);
        jComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBox1FocusGained(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel23.setText("jLabel23");

        jLabel24.setText("jLabel24");

        jTextField14.setEditable(false);
        jTextField14.setText("1");
        jTextField14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField14ActionPerformed(evt);
            }
        });
        jTextField14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField14KeyPressed(evt);
            }
        });

        jLabel25.setText("S No.");

        buttonGroup1.add(jRadioButton5);
        jRadioButton5.setText("Card Payment");
        jRadioButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton5ActionPerformed(evt);
            }
        });

        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        jButton5.setText("REPRINT");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("BILL NEW");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("UPDATE");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jTextField2.setEnabled(false);
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });

        jLabel21.setText("Disc %");

        jLabel38.setText("Igst Amount");

        jTextField30.setEditable(false);

        jLabel39.setText("Sgst Amount");

        jTextField31.setEditable(false);

        jLabel40.setText("Cgst Amount");

        jTextField32.setEditable(false);

        jLabel41.setText("Round Off");

        jTextField33.setEditable(false);

        jLabel45.setText("Mobile No.: -");

        jLabel46.setText("REF No.:");

        jLabel50.setText("Sales Return Ref");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select" }));
        jComboBox3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox3ItemStateChanged(evt);
            }
        });
        jComboBox3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox3KeyPressed(evt);
            }
        });

        jLabel51.setText("jLabel51");

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox4KeyPressed(evt);
            }
        });

        jLabel52.setText("Tax Type -");

        buttonGroup3.add(jRadioButton3);
        jRadioButton3.setText("Inclusive");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });

        buttonGroup3.add(jRadioButton4);
        jRadioButton4.setText("Exclusive");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });

        jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Main/icons8-plus-24.png"))); // NOI18N
        jLabel53.setToolTipText("Add New Customer");
        jLabel53.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel53.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel53MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25)
                            .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(0, 37, Short.MAX_VALUE))
                            .addComponent(jTextField9))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addGap(77, 77, 77))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel16))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel21))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel17))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel18)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addComponent(jLabel38))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField30, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField31, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel39))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField32, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel40))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField33, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel41))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel20)
                                    .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton5)
                            .addComponent(jButton6)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jRadioButton1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioButton2))
                                    .addComponent(jLabel3))
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jRadioButton5)
                                        .addGap(29, 29, 29)
                                        .addComponent(jLabel23)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel24))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jTextField3, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                            .addComponent(jTextField4))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel45)
                                            .addComponent(jLabel52))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jRadioButton3)
                                                .addGap(12, 12, 12)
                                                .addComponent(jRadioButton4))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jTextField37, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabel53))))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 222, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel46))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addComponent(jTextField38, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
                            .addGroup(layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(jLabel50)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel51)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2)
                    .addComponent(jLabel23)
                    .addComponent(jRadioButton5)
                    .addComponent(jLabel24)
                    .addComponent(jLabel46)
                    .addComponent(jTextField38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel52)
                        .addComponent(jRadioButton3)
                        .addComponent(jRadioButton4))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel50)
                            .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel45)
                            .addComponent(jTextField37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel51))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13)
                            .addComponent(jLabel25))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel18)
                                    .addComponent(jLabel21))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel19)
                                        .addComponent(jLabel39)
                                        .addComponent(jLabel40)
                                        .addComponent(jLabel41)
                                        .addComponent(jLabel20)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 20, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButton2)
                                    .addComponent(jButton1)
                                    .addComponent(jButton7))
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel53)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        setSize(new java.awt.Dimension(995, 549));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField9ActionPerformed

    private void jTextField15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField15ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField15ActionPerformed

    private void jTextField20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField20ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField20ActionPerformed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDialog6.setVisible(true);
            cust_Data();
        } else if (key == evt.VK_ENTER) {
            jTextField38.setVisible(true);
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField4KeyPressed
    static String disc_type = null;
    double sgstpercent = 0;
    double cgstpercent = 0;
    double totalpercent = 0;
    private void jTextField5FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField5FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5FocusLost
    String barcode1 = null;
    public boolean islist = false;
    String barcode = null;

    public void partyselect() {
        DefaultTableModel dtm = null;
        dtm = (DefaultTableModel) jTable3.getModel();
        row = jTable3.getSelectedRow();
        int coloum = jTable3.getSelectedColumn();
        sundrydebtors = (String) jTable3.getValueAt(row, 1);
        jDialog2.setVisible(false);
        jTextField4.setText(jTable3.getValueAt(row, 0) + "");
        jTextField3.setText(jTable3.getValueAt(row, 1) + "");
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + sundrydebtors + "'");
            while (rs.next()) {
                sundrydebtorsaccountcurrbal.add(rs.getString(10));
                sundrydebtorsaccountcurrbaltype.add(rs.getString(11));
            }

            if (jTable3.getValueAt(row, 6).equals("True")) {
                isIgstApplicable = true;
            } else if (jTable3.getValueAt(row, 6).equals("False")) {
                isIgstApplicable = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jDialog3.dispose();

        jTextField5.requestFocus();
    }
    public boolean isstockfinish = false;

    public int ismeter = 0;
    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            if (jTextField5.getText().isEmpty()) {
                jButton1.requestFocus();
            } else {
                isstockfinish = false;
                islist = false;
                jTextField13.requestFocus();
                barcode = jTextField5.getText().trim().toUpperCase();

                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st1 = connect.createStatement();
                    ResultSet rs = st1.executeQuery("select * from Stockid2 where Stock_No='" + barcode + "'");
                    while (rs.next()) {
                        barcode1 = rs.getString(1);
                        System.out.println("barcode1" + barcode1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                int rowcount = jTable1.getRowCount();
                int count = 0;
                int count1 = 0;

                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st1 = connect.createStatement();
                    int meter = 0;
                    ResultSet rs = st1.executeQuery("select count(stock_no),ismeter from stockid2 where stock_no='" + barcode1 + "'");

                    while (rs.next()) {
                        count = rs.getInt(1);
                        meter = rs.getInt(2);
                    }
                    if (meter == 1) {
                        isstockfinish = false;
                    } else if (meter == 0) {
                        for (int i = 0; i < rowcount; i++) {
                            if (jTable1.getValueAt(i, 1).toString().equals(barcode1)) {
                                count1++;
                            }
                        }
                        if (count1 >= count) {
                            isstockfinish = true;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (jTextField5.getText().isEmpty()) {
                    jComboBox4.requestFocus();

                } else if (barcode1 == null || isstockfinish == true) {
                    JOptionPane.showMessageDialog(rootPane, "Item is not presented in stock or Already in the List");
                    jTextField5.setText(null);
                    jTextField5.requestFocus();

                } else if (barcode1 != null) {
                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement st = connect.createStatement();

                        Statement stt2 = connect.createStatement();
                        ResultSet rss = stt2.executeQuery("SELECT  * FROM Stockid2 where Stock_No='" + barcode + "'");

                        System.out.println("" + barcode);
                        while (rss.next()) {
                            jTextField7.setText("" + rss.getString(3));
                            jComboBox4.setSelectedItem("" + rss.getString(7));
                            //    party.add(rss.getString(10));  
                            ismeter = rss.getInt("ismeter");

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (ismeter == 0 && Rate.equals("Disable")) {
                        if (!jTextField7.getText().isEmpty()) {
                            String ra_te = jTextField7.getText();
                            String qun_tity = null;
                            if (jTextField18.getText() == null) {
                                qun_tity = "0";
                            } else {
                                qun_tity = jTextField8.getText();
                            }

                            double p = Double.parseDouble(ra_te);
                            double q = Double.parseDouble(qun_tity);
                            double amou_nt = Math.round(p * q);
                            //	String t = Integer.toString(amou_nt);
                            jTextField9.setText("" + String.valueOf(amou_nt));
                        }

                        // focusgained
                        double disc_amt = 0;
                        double value = 0;
                        if (!jTextField12.getText().isEmpty()) {
                            disc_amt = Double.parseDouble(jTextField12.getText());
                        } else {
                            disc_amt = 0;
                        }
                        if (!jTextField9.getText().isEmpty()) {
                            value = Double.parseDouble(jTextField9.getText());
                        } else {
                            value = 0;
                        }

                        double total = value - disc_amt;
                        jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                        jTextField5.requestFocus();
                        st = (String) jComboBox1.getSelectedItem();
                        sno = Integer.parseInt(jTextField14.getText());
                        totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                        DecimalFormat df = new DecimalFormat("0.00");
//String totalamount1 = df.format((int)totalamount);
                        jTextField15.setText("" + (int) totalamount);
                        jTextField18.setText("" + (int) totalamount);
                        totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                        String totalquantity1 = df.format(totalquantity);
                        jTextField16.setText("" + totalquantity1);

                        double disc = 0;
                        if (!jTextField12.getText().isEmpty()) {
                            disc = Double.parseDouble(jTextField12.getText());
                        } else {
                            disc = 0;
                        }
                        totaldiscount = (totaldiscount + disc);
                        jTextField17.setText("" + (int) totaldiscount);
//                        totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
//                        jTextField20.setText("" + (int) totalamountaftertax);

                        try {
                            connection c = new connection();
                            Connection connect = c.cone();
                            Statement sttt = connect.createStatement();
                            Statement stttt = connect.createStatement();
                            Statement pro_st = connect.createStatement();
                            Statement sgst_st = connect.createStatement();
                            Statement cgst_st = connect.createStatement();
                            ResultSet rssss, rs5, rs4;
                            if (isIgstApplicable == true) {
                                ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                                while (rsss.next()) {
                                    rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                                    while (rssss.next()) {
                                        double vat = Double.parseDouble(rssss.getString("percent"));
                                        double value1 = Double.parseDouble(jTextField9.getText());

                                        if (jRadioButton3.isSelected()) {
                                            amntbeforetax = (value1 / (100 + vat)) * 100;
                                        } else if (jRadioButton4.isSelected()) {
                                            amntbeforetax = (value1);
                                        }
                                        iv = (amntbeforetax * vat) / 100;
//                                    trv = (totalamountaftertax - (amntbeforetax+iv));
                                    }
                                }

                            } else if (isIgstApplicable == false) {
                                ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                                while (pro_rs.next()) {
                                    ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                    while (sgst_rs.next()) {
                                        sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                    }

                                    ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                    while (cgst_rs.next()) {
//                                    Cgst_Account = cgst_rs.getString("ledger_name");
                                        cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                    }
                                }

                                totalpercent = sgstpercent + cgstpercent;
                                double value1 = Double.parseDouble(jTextField9.getText());

                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = (value1);
                                }

                                sv = (amntbeforetax * sgstpercent) / 100;
                                cv = ((amntbeforetax * cgstpercent) / 100);

                                double round_total = (totalamountaftertax - (amntbeforetax + sv + cv));
                                double round_figure = Math.round(round_total);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //    totalamountbeforetax=fino;
                        totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                        if (jRadioButton3.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                            jTextField20.setText("" + (int) totalamountaftertax);
                        } else if (jRadioButton4.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText())) + iv + sv + cv;
                            jTextField20.setText("" + (int) totalamountaftertax);
                        }
                        tiv = (tiv + iv);
                        tsv = (tsv + sv);
                        tcv = (tcv + cv);
                        DecimalFormat df1 = new DecimalFormat("0.00");

                        jTextField19.setText("" + df1.format(totalamountbeforetax));
                        jTextField30.setText("" + df1.format(tiv));
                        jTextField31.setText("" + df1.format(tsv));
                        jTextField32.setText("" + df1.format(tcv));

                        trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText()))));

                        jTextField33.setText("" + df1.format(trv));

                        if (jTextField5.getText().isEmpty()) {
                            barcode = "N/A";
                        }

                        DefaultTableModel dtm_ = (DefaultTableModel) jTable1.getModel();
                        st = (String) jComboBox1.getSelectedItem();
                        Object o[] = {sno, barcode, jComboBox4.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                            jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                            st, (String) jComboBox3.getSelectedItem()};
                        dtm_.addRow(o);
                        count++;
                        sno = Integer.parseInt(jTextField14.getText());
                        sno++;
                        jTextField14.setText("" + sno);
                        jTextField5.setText(null);
                        jComboBox4.setSelectedIndex(0);
                        jTextField7.setText(null);
                        jTextField8.setText("1");
                        jTextField9.setText(null);
                        jTextField10.setText(null);
                        jTextField11.setText("0");
                        jTextField12.setText("0");
                        jTextField13.setText(null);
                        jComboBox1.setSelectedItem(st);
                        jTextField5.requestFocus();
                    } else {
                        jTextField7.requestFocus();
                    }
                }
            }
        }

        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField9.requestFocus();
            if (ismeter == 0) {
                if (!jTextField7.getText().isEmpty()) {
                    String ra_te = jTextField7.getText();
                    String qun_tity = null;
                    if (jTextField18.getText() == null) {
                        qun_tity = "0";
                    } else {
                        qun_tity = jTextField8.getText();
                    }

                    double p = Double.parseDouble(ra_te);
                    double q = Double.parseDouble(qun_tity);
                    double amou_nt = Math.round(p * q);
                    //	String t = Integer.toString(amou_nt);
                    jTextField9.setText("" + String.valueOf(amou_nt));
                }

                // focusgained
                double disc_amt = 0;
                double value = 0;
                if (!jTextField12.getText().isEmpty()) {
                    disc_amt = Double.parseDouble(jTextField12.getText());
                } else {
                    disc_amt = 0;
                }
                if (!jTextField9.getText().isEmpty()) {
                    value = Double.parseDouble(jTextField9.getText());
                } else {
                    value = 0;
                }

                double total = value - disc_amt;
                jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                jTextField5.requestFocus();
                st = (String) jComboBox1.getSelectedItem();
                sno = Integer.parseInt(jTextField14.getText());
                totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                DecimalFormat df = new DecimalFormat("0.00");
//String totalamount1 = df.format((int)totalamount);
                jTextField15.setText("" + (int) totalamount);
                jTextField18.setText("" + (int) totalamount);
                if (!jTextField7.getText().isEmpty()) {
                    totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                    String totalquantity1 = df.format(totalquantity);
                    jTextField16.setText("" + totalquantity1);
                }

                double disc = 0;
                if (!jTextField12.getText().isEmpty()) {
                    disc = Double.parseDouble(jTextField12.getText());
                } else {
                    disc = 0;
                }
                totaldiscount = (totaldiscount + disc);
                jTextField17.setText("" + (int) totaldiscount);
//                totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
//                jTextField20.setText("" + (int) totalamountaftertax);

                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement sttt = connect.createStatement();
                    Statement stttt = connect.createStatement();
                    Statement pro_st = connect.createStatement();
                    Statement sgst_st = connect.createStatement();
                    Statement cgst_st = connect.createStatement();
                    ResultSet rssss, rs5, rs4;
                    if (isIgstApplicable == true) {
                        ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                        while (rsss.next()) {
                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                            while (rssss.next()) {
                                double vat = Double.parseDouble(rssss.getString("percent"));
                                double value1 = Double.parseDouble(jTextField9.getText());

                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = (value1);
                                }

                                iv = (amntbeforetax * vat) / 100;
//                                    trv = (totalamountaftertax - (amntbeforetax+iv));
                            }
                        }

                    } else if (isIgstApplicable == false) {
                        ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                        while (pro_rs.next()) {
                            ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                            while (sgst_rs.next()) {
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }

                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
//                                    Cgst_Account = cgst_rs.getString("ledger_name");
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }
                        }

                        totalpercent = sgstpercent + cgstpercent;
                        double value1 = Double.parseDouble(jTextField9.getText());

                        if (jRadioButton3.isSelected()) {
                            amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amntbeforetax = (value1);
                        }

                        sv = (amntbeforetax * sgstpercent) / 100;
                        cv = ((amntbeforetax * cgstpercent) / 100);

                        double round_total = (totalamountaftertax - (amntbeforetax + sv + cv));
                        double round_figure = Math.round(round_total);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                //    totalamountbeforetax=fino;
                totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                if (jRadioButton3.isSelected()) {
                    totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                    jTextField20.setText("" + (int) totalamountaftertax);
                } else if (jRadioButton4.isSelected()) {
                    totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText())) + iv + sv + cv;
                    jTextField20.setText("" + (int) totalamountaftertax);
                }
                tiv = (tiv + iv);
                tsv = (tsv + sv);
                tcv = (tcv + cv);
                DecimalFormat df1 = new DecimalFormat("0.00");

                jTextField19.setText("" + df1.format(totalamountbeforetax));
                jTextField30.setText("" + df1.format(tiv));
                jTextField31.setText("" + df1.format(tsv));
                jTextField32.setText("" + df1.format(tcv));

                trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText()))));

                jTextField33.setText("" + df1.format(trv));

                if (jTextField5.getText().isEmpty()) {
                    barcode = "N/A";
                }

                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                st = (String) jComboBox1.getSelectedItem();
                Object o[] = {sno, barcode, jComboBox4.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                    jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                    st, (String) jComboBox3.getSelectedItem()};
                dtm.addRow(o);
                count++;
                sno = Integer.parseInt(jTextField14.getText());
                sno++;
                jTextField14.setText("" + sno);
                jTextField5.setText(null);
                jComboBox4.setSelectedIndex(0);
                jTextField7.setText(null);
                jTextField8.setText("1");
                jTextField9.setText(null);
                jTextField10.setText(null);
                jTextField11.setText("0");
                jTextField12.setText("0");
                jTextField13.setText(null);
                jComboBox1.setSelectedItem(st);
                jTextField5.requestFocus();
            } else if (ismeter == 1) {
                double quantityindatabse = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();

                    ResultSet rs = st.executeQuery("select qnt from stockid2 where stock_no='" + jTextField5.getText() + "' ");
                    while (rs.next()) {
                        quantityindatabse = rs.getDouble("qnt");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (quantityindatabse < Double.parseDouble(jTextField8.getText())) {
                    JOptionPane.showMessageDialog(rootPane, "The Selected Quantity is greater then the Quantity Avaialble !! Please Check Again");
                    jTextField8.requestFocus();
                } else {
                    if (jTextField5.getText().equals("OTH")) {
                        jTextField10.requestFocus();
                    } else {
                        if (!jTextField7.getText().isEmpty()) {
                            String ra_te = jTextField7.getText();
                            String qun_tity = null;
                            if (jTextField18.getText() == null) {
                                qun_tity = "0";
                            } else {
                                qun_tity = jTextField8.getText();
                            }

                            double p = Double.parseDouble(ra_te);
                            double q = Double.parseDouble(qun_tity);
                            double amou_nt = Math.round(p * q);
                            //	String t = Integer.toString(amou_nt);
                            jTextField9.setText("" + String.valueOf(amou_nt));
                        }

                        // focusgained
                        double disc_amt = 0;
                        double value = 0;
                        if (!jTextField12.getText().isEmpty()) {
                            disc_amt = Double.parseDouble(jTextField12.getText());
                        } else {
                            disc_amt = 0;
                        }
                        if (!jTextField9.getText().isEmpty()) {
                            value = Double.parseDouble(jTextField9.getText());
                        } else {
                            value = 0;
                        }

                        double total = value - disc_amt;
                        jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                        jTextField5.requestFocus();
                        st = (String) jComboBox1.getSelectedItem();
                        sno = Integer.parseInt(jTextField14.getText());
                        totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                        DecimalFormat df = new DecimalFormat("0.00");
//String totalamount1 = df.format(totalamount);
                        jTextField15.setText("" + totalamount);
                        //                jTextField18.setText("" + (int) totalamount);
                        totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
//String totalquantity1 = df.format((int)totalquantity);
                        jTextField16.setText("" + totalquantity);

                        double disc = 0;
                        if (!jTextField12.getText().isEmpty()) {
                            disc = Double.parseDouble(jTextField12.getText());
                        } else {
                            disc = 0;
                        }
                        totaldiscount = (totaldiscount + disc);
//String totaldiscount1 = df.format(totaldiscount);
                        jTextField17.setText("" + (int) totaldiscount);
//                        totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
////String totalamountaftertax1 = df.format((int)totalamountaftertax);
//                        jTextField20.setText("" + (int) totalamountaftertax);
//                    double amntbeforetax = 0;
                        try {
                            connection c = new connection();
                            Connection connect = c.cone();
                            Statement sttt = connect.createStatement();
                            Statement stttt = connect.createStatement();
                            Statement pro_st = connect.createStatement();
                            Statement sgst_st = connect.createStatement();
                            Statement cgst_st = connect.createStatement();
                            ResultSet rssss, rs5, rs4;
                            if (isIgstApplicable == true) {
                                ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                                while (rsss.next()) {

                                    rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                                    while (rssss.next()) {
                                        double vat = Double.parseDouble(rssss.getString("percent"));
                                        double value1 = Double.parseDouble(jTextField9.getText());

                                        if (jRadioButton3.isSelected()) {
                                            amntbeforetax = (value1 / (100 + vat)) * 100;
                                        } else if (jRadioButton4.isSelected()) {
                                            amntbeforetax = (value1);
                                        }

                                        iv = (amntbeforetax * vat) / 100;
//                                    trv = (totalamountaftertax - (amntbeforetax+iv));
                                    }

                                }

                            } else if (isIgstApplicable == false) {
                                ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                                while (pro_rs.next()) {
                                    ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                    while (sgst_rs.next()) {
                                        sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                    }

                                    ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                    while (cgst_rs.next()) {
//                                    Cgst_Account = cgst_rs.getString("ledger_name");
                                        cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                    }
                                }

                                totalpercent = sgstpercent + cgstpercent;
                                double value1 = Double.parseDouble(jTextField9.getText());

                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = (value1);
                                }
                                sv = (amntbeforetax * sgstpercent) / 100;
                                cv = ((amntbeforetax * cgstpercent) / 100);

                                double round_total = (totalamountaftertax - (amntbeforetax + sv + cv));
                                double round_figure = Math.round(round_total);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //    totalamountbeforetax=fino;
                        totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                        if (jRadioButton3.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                            jTextField20.setText("" + (int) totalamountaftertax);
                        } else if (jRadioButton4.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText())) + iv + sv + cv;
                            jTextField20.setText("" + (int) totalamountaftertax);
                        }
                        tiv = (tiv + iv);
                        tsv = (tsv + sv);
                        tcv = (tcv + cv);
                        DecimalFormat df1 = new DecimalFormat("0.00");

                        jTextField19.setText("" + df1.format(totalamountbeforetax));
                        jTextField30.setText("" + df1.format(tiv));
                        jTextField31.setText("" + df1.format(tsv));
                        jTextField32.setText("" + df1.format(tcv));

                        trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText()))));

                        jTextField33.setText("" + df1.format(trv));

                        if (jTextField5.getText().isEmpty()) {
                            barcode = "N/A";
                        }

                        st = (String) jComboBox1.getSelectedItem();
                        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                        Object o[] = {sno, barcode, jComboBox4.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                            jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                            st};
                        dtm.addRow(o);
                        count++;
                        sno = Integer.parseInt(jTextField14.getText());
                        sno++;
                        jTextField14.setText("" + sno);
                        jTextField5.setText(null);
                        jComboBox4.setSelectedIndex(0);
                        jTextField7.setText(null);
                        jTextField8.setText("1");
                        jTextField9.setText(null);
                        jTextField10.setText(null);
                        jTextField11.setText("0");
                        jTextField12.setText("0");
                        jTextField13.setText(null);
                        jComboBox1.setSelectedItem(st);
                        jTextField5.requestFocus();

                    }
                }
            }

        }
        if (key == evt.VK_ESCAPE) {
            jTextField7.requestFocus();
        }
    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().isEmpty()) {
                jTextField8.requestFocus();
            } else if (!jTextField5.getText().isEmpty()) {
                if (ismeter == 0) {
                    if (!jTextField7.getText().isEmpty()) {
                        String ra_te = jTextField7.getText();
                        String qun_tity = null;
                        if (jTextField18.getText() == null) {
                            qun_tity = "0";
                        } else {
                            qun_tity = jTextField8.getText();
                        }

                        double p = Double.parseDouble(ra_te);
                        double q = Double.parseDouble(qun_tity);
                        double amou_nt = Math.round(p * q);
                        //	String t = Integer.toString(amou_nt);
                        jTextField9.setText("" + String.valueOf(amou_nt));
                    }

                    // focusgained
                    double disc_amt = 0;
                    double value = 0;
                    if (!jTextField12.getText().isEmpty()) {
                        disc_amt = Double.parseDouble(jTextField12.getText());
                    } else {
                        disc_amt = 0;
                    }
                    if (!jTextField9.getText().isEmpty()) {
                        value = Double.parseDouble(jTextField9.getText());
                    } else {
                        value = 0;
                    }

                    double total = value - disc_amt;
                    jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                    jTextField5.requestFocus();
                    st = (String) jComboBox1.getSelectedItem();
                    sno = Integer.parseInt(jTextField14.getText());
                    totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                    DecimalFormat df = new DecimalFormat("0.00");
//String totalamount1 = df.format((int)totalamount);
                    jTextField15.setText("" + (int) totalamount);
                    jTextField18.setText("" + (int) totalamount);
                    totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                    String totalquantity1 = df.format(totalquantity);
                    jTextField16.setText("" + totalquantity1);

                    double disc = 0;
                    if (!jTextField12.getText().isEmpty()) {
                        disc = Double.parseDouble(jTextField12.getText());
                    } else {
                        disc = 0;
                    }
                    totaldiscount = (totaldiscount + disc);
                    jTextField17.setText("" + (int) totaldiscount);
//                    totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
//                    jTextField20.setText("" + (int) totalamountaftertax);

                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement sttt = connect.createStatement();
                        Statement stttt = connect.createStatement();
                        Statement pro_st = connect.createStatement();
                        Statement sgst_st = connect.createStatement();
                        Statement cgst_st = connect.createStatement();
                        ResultSet rssss, rs5, rs4;
                        if (isIgstApplicable == true) {
                            ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                            while (rsss.next()) {
                                rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                                while (rssss.next()) {
                                    double vat = Double.parseDouble(rssss.getString("percent"));
                                    double value1 = Double.parseDouble(jTextField9.getText());

                                    if (jRadioButton3.isSelected()) {
                                        amntbeforetax = (value1 / (100 + vat)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amntbeforetax = (value1);
                                    }
                                    iv = (amntbeforetax * vat) / 100;
//                                    trv = (totalamountaftertax - (amntbeforetax+iv));
                                }
                            }

                        } else if (isIgstApplicable == false) {
                            ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                            while (pro_rs.next()) {
                                ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                while (sgst_rs.next()) {
                                    sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                }

                                ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                while (cgst_rs.next()) {
//                                    Cgst_Account = cgst_rs.getString("ledger_name");
                                    cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                }
                            }

                            totalpercent = sgstpercent + cgstpercent;
                            double value1 = Double.parseDouble(jTextField9.getText());

                            if (jRadioButton3.isSelected()) {
                                amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                            } else if (jRadioButton4.isSelected()) {
                                amntbeforetax = (value1);
                            }

                            sv = (amntbeforetax * sgstpercent) / 100;
                            cv = ((amntbeforetax * cgstpercent) / 100);

                            double round_total = (totalamountaftertax - (amntbeforetax + sv + cv));
                            double round_figure = Math.round(round_total);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //    totalamountbeforetax=fino;
                    if (jRadioButton3.isSelected()) {
                        totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                        jTextField20.setText("" + (int) totalamountaftertax);
                    } else if (jRadioButton4.isSelected()) {
                        totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText())) + iv + sv + cv;
                        jTextField20.setText("" + (int) totalamountaftertax);
                    }
                    totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                    tiv = (tiv + iv);
                    tsv = (tsv + sv);
                    tcv = (tcv + cv);
                    DecimalFormat df1 = new DecimalFormat("0.00");

                    jTextField19.setText("" + df1.format(totalamountbeforetax));
                    jTextField30.setText("" + df1.format(tiv));
                    jTextField31.setText("" + df1.format(tsv));
                    jTextField32.setText("" + df1.format(tcv));

                    trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText()))));

                    jTextField33.setText("" + df1.format(trv));

                    if (jTextField5.getText().isEmpty()) {
                        barcode = "N/A";
                    }

                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    st = (String) jComboBox1.getSelectedItem();
                    Object o[] = {sno, barcode, jComboBox4.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                        jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                        st, (String) jComboBox3.getSelectedItem()};
                    dtm.addRow(o);
                    count++;
                    sno = Integer.parseInt(jTextField14.getText());
                    sno++;
                    jTextField14.setText("" + sno);
                    jTextField5.setText(null);
                    jComboBox4.setSelectedIndex(0);
                    jTextField7.setText(null);
                    jTextField8.setText("1");
                    jTextField9.setText(null);
                    jTextField10.setText(null);
                    jTextField11.setText("0");
                    jTextField12.setText("0");
                    jTextField13.setText(null);
                    jComboBox1.setSelectedItem(st);
                    jTextField5.requestFocus();
                } else if (ismeter == 1) {
                    jTextField8.requestFocus();
                }
            }
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox4.requestFocus();
        }
    }//GEN-LAST:event_jTextField7KeyPressed

    private void jTextField10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField10KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().equals("OTH")) {
                if (!jTextField7.getText().isEmpty()) {
                    String ra_te = jTextField7.getText();
                    String qun_tity = null;
                    if (jTextField18.getText() == null) {
                        qun_tity = "0";
                    } else {
                        qun_tity = jTextField8.getText();
                    }

                    double p = Double.parseDouble(ra_te);
                    double q = Double.parseDouble(qun_tity);
                    double amou_nt = (p * q);
                    //	String t = Integer.toString(amou_nt);
                    jTextField9.setText("" + String.valueOf(amou_nt));
                }

                // focusgained
                double disc_amt = 0;
                double value = 0;
                if (!jTextField12.getText().isEmpty()) {
                    disc_amt = Double.parseDouble(jTextField12.getText());
                } else {
                    disc_amt = 0;
                }
                if (!jTextField9.getText().isEmpty()) {
                    value = Double.parseDouble(jTextField9.getText());
                } else {
                    value = 0;
                }

                double total = value - disc_amt;
                jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                jTextField5.requestFocus();
                st = (String) jComboBox1.getSelectedItem();
                sno = Integer.parseInt(jTextField14.getText());
                totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                DecimalFormat df = new DecimalFormat("0.00");
//String totalamount1 = df.format((int)totalamount);
                jTextField15.setText("" + (int) totalamount);
                jTextField18.setText("" + (int) totalamount);
                totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
//String totalquantity1 = df.format((int)totalquantity);
                jTextField16.setText("" + totalquantity);

                double disc = 0;
                if (!jTextField12.getText().isEmpty()) {
                    disc = Double.parseDouble(jTextField12.getText());
                } else {
                    disc = 0;
                }
                totaldiscount = (totaldiscount + disc);
//String totaldiscount1 = df.format(totaldiscount);
                jTextField17.setText("" + (int) totaldiscount);
//                totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
////String totalamountaftertax1 = df.format((int)totalamountaftertax);
//                jTextField20.setText("" + (int) totalamountaftertax);
//                    double amntbeforetax = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement sttt = connect.createStatement();
                    Statement stttt = connect.createStatement();
                    Statement pro_st = connect.createStatement();
                    Statement sgst_st = connect.createStatement();
                    Statement cgst_st = connect.createStatement();
                    ResultSet rssss, rs5, rs4;
                    if (isIgstApplicable == true) {
                        ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                        while (rsss.next()) {

                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                            while (rssss.next()) {
                                double vat = Double.parseDouble(rssss.getString("percent"));
                                double value1 = Double.parseDouble(jTextField9.getText());

                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = (value1);
                                }
                                iv = (amntbeforetax * vat) / 100;
//                                    trv = (totalamountaftertax - (amntbeforetax+iv));
                            }

                        }

                    } else if (isIgstApplicable == false) {
                        ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox4.getSelectedItem() + "'");
                        while (pro_rs.next()) {
                            ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                            while (sgst_rs.next()) {
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }

                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
//                                    Cgst_Account = cgst_rs.getString("ledger_name");
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }
                        }

                        totalpercent = sgstpercent + cgstpercent;
                        double value1 = Double.parseDouble(jTextField9.getText());

                        if (jRadioButton3.isSelected()) {
                            amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amntbeforetax = (value1);
                        }
                        sv = (amntbeforetax * sgstpercent) / 100;
                        cv = ((amntbeforetax * cgstpercent) / 100);

                        double round_total = (totalamountaftertax - (amntbeforetax + sv + cv));
                        double round_figure = Math.round(round_total);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                //    totalamountbeforetax=fino;
                totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                if (jRadioButton3.isSelected()) {
                    totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                    jTextField20.setText("" + (int) totalamountaftertax);
                } else if (jRadioButton4.isSelected()) {
                    totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText())) + iv + sv + cv;
                    jTextField20.setText("" + (int) totalamountaftertax);
                }
                tiv = (tiv + iv);
                tsv = (tsv + sv);
                tcv = (tcv + cv);
                DecimalFormat df1 = new DecimalFormat("0.00");

                jTextField19.setText("" + df1.format(totalamountbeforetax));
                jTextField30.setText("" + df1.format(tiv));
                jTextField31.setText("" + df1.format(tsv));
                jTextField32.setText("" + df1.format(tcv));

                trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText()))));

                jTextField33.setText("" + df1.format(Math.abs(trv)));

                st = (String) jComboBox1.getSelectedItem();

                if (jTextField5.getText().isEmpty()) {
                    barcode = "N/A";
                }

                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                Object o[] = {sno, barcode, jComboBox4.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                    jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                    st};
                dtm.addRow(o);
                count++;
                sno = Integer.parseInt(jTextField14.getText());
                sno++;
                jTextField14.setText("" + sno);
                jTextField5.setText(null);
                jComboBox4.setSelectedIndex(0);
                jTextField7.setText(null);
                jTextField8.setText("1");
                jTextField9.setText(null);
                jTextField10.setText(null);
                jTextField11.setText("0");
                jTextField12.setText("0");
                jTextField13.setText(null);
                jComboBox1.setSelectedItem(st);
                jTextField5.requestFocus();
            }
        }
    }//GEN-LAST:event_jTextField10KeyPressed
    ArrayList snumber = new ArrayList();
    String st = null;
    ArrayList stock = new ArrayList();
    ArrayList prod = new ArrayList();
    ArrayList rate = new ArrayList();
    ArrayList qnty = new ArrayList();
    ArrayList value = new ArrayList();
    ArrayList disc_code = new ArrayList();
    ArrayList discper = new ArrayList();
    ArrayList discamt = new ArrayList();
    ArrayList total = new ArrayList();
    ArrayList staff = new ArrayList();
    double per[] = new double[1000];
    int count = 0;
    public int sno = 1;
    double totalamount;
    double totalquantity;
    double totaldiscount;
    double totalamountaftertax;
    double totalamountbeforetax = 0;
    double perc = 0;
    int q = 1;
    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jTextField20KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField20KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ESCAPE) {
            jTextField19.requestFocus();
        }
    }//GEN-LAST:event_jTextField20KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        int key1 = evt.getKeyCode();
        if (key == evt.VK_ENTER) {
            if (issave == false) {
                jButton12.setVisible(false);
//                 jButton5.requestFocus();
                if (issave == false) {

                    jDialog4.setVisible(true);
                    jLabel48.setText(jTextField20.getText());
                    if (jRadioButton1.isSelected()) {
                        jTextField34.setText(jTextField20.getText());
                    } else if (jRadioButton2.isSelected()) {
                        jTextField35.setText(jTextField20.getText());
                    } else if (jRadioButton5.isSelected()) {
                        jTextField36.setText(jTextField20.getText());
                    }

                }
            }
        }
        if (key1 == evt.VK_RIGHT) {
            jButton2.requestFocus();
        }
    }//GEN-LAST:event_jButton1KeyPressed
    ArrayList vat = new ArrayList();
    ArrayList ledgervat = new ArrayList();
    ArrayList ledgersgst = new ArrayList();
    ArrayList ledgervat2 = new ArrayList();
    ArrayList sgst = new ArrayList();
    ArrayList sgstledger = new ArrayList();
    ArrayList cgst = new ArrayList();
    ArrayList cgstledger = new ArrayList();
    ArrayList vatamount = new ArrayList();
    ArrayList vatcurrbal = new ArrayList();
    ArrayList vatcurrbaltype = new ArrayList();
    ArrayList updatevatcurrbal = new ArrayList();
    ArrayList updatevatcurrbaltype = new ArrayList();

    ArrayList sgstcurrbal = new ArrayList();
    ArrayList sgstcurrbaltype = new ArrayList();
    ArrayList updatesgstcurrbal = new ArrayList();
    ArrayList updatesgstcurrbaltype = new ArrayList();

    ArrayList cgstcurrbal = new ArrayList();
    ArrayList cgstcurrbaltype = new ArrayList();
    ArrayList updatecgstcurrbal = new ArrayList();
    ArrayList updatecgstcurrbaltype = new ArrayList();

    ArrayList salesaccount = new ArrayList();
    ArrayList salesaccount1 = new ArrayList();

    ArrayList Salesaccountcurrbal = new ArrayList();
    ArrayList Salesaccountcurrbaltype = new ArrayList();
    ArrayList Salesaccountcurrbal1 = new ArrayList();
    ArrayList Salesaccountcurrbaltype1 = new ArrayList();

    ArrayList Salesaccountupdatecurrbal = new ArrayList();
    ArrayList Salesaccountupdatecurrbaltype = new ArrayList();
    ArrayList cashaccountcurrbal = new ArrayList();
    ArrayList cashaccountcurrbaltype = new ArrayList();
    ArrayList cashaccountupdatecurrbal = new ArrayList();
    ArrayList cashaccountupdatecurrbaltype = new ArrayList();
    ArrayList sundrydebtorsaccountcurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountcurrbaltype = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbaltype = new ArrayList();
    ArrayList cardaccountcurrbal = new ArrayList();
    ArrayList cardaccountcurrbaltype = new ArrayList();
    ArrayList cardaccountupdatecurrbal = new ArrayList();
    ArrayList cardaccountupdatecurrbaltype = new ArrayList();
    ArrayList bankaccountcurrbal = new ArrayList();
    ArrayList bankaccountcurrbaltype = new ArrayList();
    ArrayList bankaccountupdatecurrbal = new ArrayList();
    ArrayList bankaccountupdatecurrbaltype = new ArrayList();
    double vatperc = 0;
    double vatamnt = 0;
    String igstamnt1_1;
    double amountbeforevat = 0;

    double total_igst_taxamnt = 0;
    double total_sgst_taxamnt = 0;
    double total_cgst_taxamnt = 0;

    double sgstperc = 0;
    double sgstamnt = 0;
    String sgstamnt1_1;
    double amountbeforesgst = 0;

    double cgstperc = 0;
    double cgstamnt = 0;
    String cgstamnt1_1;
    double amountbeforecgst = 0;

    public boolean issave = false;

    public void save() {
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        if (rowcount == 0) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter the Stock NO value has been entered");
        } else {
            issave = true;
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement Cashaccount = connect.createStatement();
                Statement cardaccount = connect.createStatement();
                Statement sundrydebtorsaccount = connect.createStatement();
                String c1 = "CASH";
                ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='" + c1 + "'");
                while (rscashaccount.next()) {
                    cashaccountcurrbal.add(0, rscashaccount.getString(10));
                    cashaccountcurrbaltype.add(0, rscashaccount.getString(11));
                }

                ResultSet rscard = cardaccount.executeQuery("select * from ledger where ledger_name='" + cardpaymentaccount + "'");
                while (rscard.next()) {
                    cardaccountcurrbal.add(0, rscard.getString(10));
                    cardaccountcurrbaltype.add(0, rscard.getString(11));
                }

                ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + sundrydebtors + "'");
                while (rssundrydebtors.next()) {
                    sundrydebtorsaccountcurrbal.add(0, rssundrydebtors.getString(10));
                    sundrydebtorsaccountcurrbaltype.add(0, rssundrydebtors.getString(11));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            for (int i = 0; i < rowcount; i++) {
                try {
                    snumber.add(i, jTable1.getValueAt(i, 0));
                    stock.add(i, jTable1.getValueAt(i, 1));
                    prod.add(i, jTable1.getValueAt(i, 2));
                    rate.add(i, jTable1.getValueAt(i, 3));
                    qnty.add(i, jTable1.getValueAt(i, 4));
                    value.add(i, jTable1.getValueAt(i, 5));
                    disc_code.add(i, jTable1.getValueAt(i, 6));
                    discper.add(i, jTable1.getValueAt(i, 7));
                    discamt.add(i, jTable1.getValueAt(i, 8));
                    total.add(i, jTable1.getValueAt(i, 9));
                    staff.add(i, jTable1.getValueAt(i, 10));

                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = null;
                    ResultSet rs = null;
                    PreparedStatement pstm = null;
                    int j = 0;
                    int check = 0;
                    String HELLO = "HELLO";
                    String paytype = null;
                    String to_ledger = null;
                    String by_ledger = null;
                    try {
                        Statement stin = connect.createStatement();
                        Statement del = connect.createStatement();
                        Statement stmt1 = connect.createStatement();
                        Statement stmtvataccount = connect.createStatement();
                        Statement stru = connect.createStatement();
                        Statement stru1 = connect.createStatement();

                        Statement stmt2 = connect.createStatement();
                        Statement stmtcgstaccount = connect.createStatement();

                        if ((String) prod.get(i) != null) {
                            if (isIgstApplicable == true) {
                                ResultSet rsvat = stmt1.executeQuery("select IGST from product where product_code='" + (String) prod.get(i) + "'");
                                while (rsvat.next()) {
                                    ledgervat.add(i, rsvat.getString(1));
                                }

                                ResultSet rsvataccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgervat.get(i) + "'");
                                while (rsvataccount.next()) {
                                    vat.add(i, rsvataccount.getString(6));
                                    vatcurrbal.add(i, rsvataccount.getString(10));
                                    vatcurrbaltype.add(i, rsvataccount.getString(11));

                                    vatperc = Double.parseDouble((String) vat.get(i));

                                    if (jRadioButton3.isSelected()) {
                                        amountbeforevat = (Double.parseDouble((String) total.get(i)) / (100 + vatperc)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amountbeforevat = (Double.parseDouble((String) total.get(i)));
                                    }
                                    vatamnt = (vatperc * amountbeforevat) / 100;

                                    DecimalFormat idf = new DecimalFormat("0.00");
                                    igstamnt1_1 = idf.format(vatamnt);

                                    String vatcurrbalance = (String) vatcurrbal.get(i);
                                    String Curbaltypevat = (String) vatcurrbaltype.get(i);
                                    double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                                    double vatamnt1 = Math.round(vatamnt);
                                    double result;
                                    if (Curbaltypevat.equals("DR")) {
                                        result = vatcurrbalance1 - vatamnt1;
                                        if (result >= 0) {
                                            updatevatcurrbaltype.add(i, "DR");
                                            updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));
                                        } else {
                                            updatevatcurrbaltype.add(i, "CR");
                                            updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));
                                        }

                                    } else if (Curbaltypevat.equals("CR")) {

                                        result = vatcurrbalance1 + vatamnt1;

                                        updatevatcurrbaltype.add(i, "CR");
                                        updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));

                                    }

                                    total_igst_taxamnt = total_igst_taxamnt + vatamnt;
                                }
                                sgstperc = 0;
                                sgstamnt = 0;
                                cgstperc = 0;
                                cgstamnt = 0;

                            } else if (isIgstApplicable == false) {
                                ResultSet rssgst = stmt1.executeQuery("select SGST from product where product_code='" + (String) prod.get(i) + "'");
                                while (rssgst.next()) {
                                    ledgersgst.add(i, rssgst.getString(1));
                                }
// For SGST                                
                                ResultSet rssgstaccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgersgst.get(i) + "'");
                                while (rssgstaccount.next()) {
                                    sgst.add(i, rssgstaccount.getString(6));
                                    sgstcurrbal.add(i, rssgstaccount.getString(10));
                                    sgstcurrbaltype.add(i, rssgstaccount.getString(11));
                                    sgstperc = Double.parseDouble((String) sgst.get(i));

// For CGST                                   
                                    ResultSet rscgst = stmt2.executeQuery("select CGST from product where product_code='" + (String) prod.get(i) + "'");
                                    while (rscgst.next()) {
                                        ledgervat2.add(i, rscgst.getString(1));
                                    }
                                    ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgervat2.get(i) + "'");
                                    while (rscgstaccount.next()) {
                                        cgst.add(i, rscgstaccount.getString(6));
                                        cgstcurrbal.add(i, rscgstaccount.getString(10));
                                        cgstcurrbaltype.add(i, rscgstaccount.getString(11));
                                        cgstperc = Double.parseDouble((String) cgst.get(i));

// SGST Caluclation                           
                                        if (jRadioButton3.isSelected()) {
                                            amountbeforesgst = (Double.parseDouble((String) total.get(i)) / (100 + sgstperc + cgstperc)) * 100;
                                        } else if (jRadioButton4.isSelected()) {
                                            amountbeforesgst = (Double.parseDouble((String) total.get(i)));
                                        }
                                        sgstamnt = (sgstperc * amountbeforesgst) / 100;
                                        DecimalFormat sdf = new DecimalFormat("0.00");
                                        sgstamnt1_1 = sdf.format(sgstamnt);

                                        String vatcurrbalance = (String) sgstcurrbal.get(i);
                                        String Curbaltypesgst = (String) sgstcurrbaltype.get(i);
                                        double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                                        double sgstamnt1 = sgstamnt;
                                        double result;
                                        if (Curbaltypesgst.equals("DR")) {
                                            result = vatcurrbalance1 - sgstamnt1;
                                            if (result >= 0) {
                                                updatesgstcurrbaltype.add(i, "DR");
                                                updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            } else {
                                                updatesgstcurrbaltype.add(i, "CR");
                                                updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            }

                                        } else if (Curbaltypesgst.equals("CR")) {

                                            result = vatcurrbalance1 + sgstamnt1;
                                            updatesgstcurrbaltype.add(i, "CR");
                                            updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));

                                        }
//CGST Calculation                     
                                        if (jRadioButton3.isSelected()) {
                                            amountbeforecgst = (Double.parseDouble((String) total.get(i)) / (100 + cgstperc + sgstperc)) * 100;
                                        } else if (jRadioButton4.isSelected()) {
                                            amountbeforecgst = (Double.parseDouble((String) total.get(i)));
                                        }
                                        cgstamnt = (cgstperc * amountbeforecgst) / 100;
                                        DecimalFormat cdf = new DecimalFormat("0.00");
                                        cgstamnt1_1 = cdf.format(cgstamnt);

                                        String cgstcurrbalance = (String) cgstcurrbal.get(i);
                                        String Curbaltypecgst = (String) cgstcurrbaltype.get(i);
                                        double vatcurrbalance2 = Double.parseDouble(cgstcurrbalance);
                                        double cgstamnt1 = cgstamnt;
                                        double result1;
                                        if (Curbaltypecgst.equals("DR")) {
                                            result1 = vatcurrbalance2 - cgstamnt1;
                                            if (result1 >= 0) {
                                                updatecgstcurrbaltype.add(i, "DR");
                                                updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                            } else {
                                                updatecgstcurrbaltype.add(i, "CR");
                                                updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                            }

                                        } else if (Curbaltypecgst.equals("CR")) {

                                            result1 = vatcurrbalance2 + cgstamnt1;
                                            System.out.println("cgst result" + result1);

                                            updatecgstcurrbaltype.add(i, "CR");
                                            updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));

                                        }
                                    }

                                }
                                vatperc = 0;
                                vatamnt = 0;
                                total_sgst_taxamnt = total_sgst_taxamnt + sgstamnt;
                                total_cgst_taxamnt = total_cgst_taxamnt + cgstamnt;

                            }
                        } else if (prod.get(i) == null) {
                            vatperc = 0;
                            vatamnt = 0;
                            sgstperc = 0;
                            sgstamnt = 0;
                            cgstperc = 0;
                            cgstamnt = 0;
                        }
                        Statement Salesaccount = connect.createStatement();
                        Statement Salesaccount2 = connect.createStatement();
                        String Sales = "SALES ACCOUNTS";
                        String Salesgrp = null;
                        if (isIgstApplicable == true) {
                            ResultSet rssalesaccount = Salesaccount.executeQuery("Select Sales_account from product where product_code='" + prod.get(i) + "' ");
                            while (rssalesaccount.next()) {
                                salesaccount.add(i, rssalesaccount.getString(1));
                            }
                            ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + (String) salesaccount.get(i) + "'");
                            while (rssales.next()) {
                                Salesaccountcurrbal.add(i, rssales.getString(10));
                                Salesaccountcurrbaltype.add(i, rssales.getString(11));
                            }
                        } else if (isIgstApplicable == false) {
                            ResultSet rssalesaccount = Salesaccount.executeQuery("Select Sales_account from product where product_code='" + prod.get(i) + "' ");
                            while (rssalesaccount.next()) {
                                salesaccount.add(i, rssalesaccount.getString(1));
                            }

                            ResultSet rssales = Salesaccount2.executeQuery("select * from ledger where ledger_name='" + (String) salesaccount.get(i) + "'");
                            while (rssales.next()) {
                                Salesaccountcurrbal.add(i, rssales.getString(10));
                                Salesaccountcurrbaltype.add(i, rssales.getString(11));
                            }
                        }
                        if (jRadioButton1.isSelected()) {
                            paytype = jRadioButton1.getLabel();
                        }
                        if (jRadioButton2.isSelected()) {
                            paytype = jRadioButton2.getLabel();
                        }
                        if (jRadioButton5.isSelected()) {
                            paytype = jRadioButton5.getLabel();
                        }
                        String salesaccountamnt = (String) Salesaccountcurrbal.get(i);

                        double totalamnt = Double.parseDouble((String) total.get(i));

                        double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);

                        double salesaccountupdatedamount = (salesaccountamnt1 - (totalamnt - (vatamnt + sgstamnt + cgstamnt)));
                        if (Salesaccountcurrbaltype.get(i).equals("DR")) {
                            if (salesaccountupdatedamount > 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("DR");

                            }
                            if (salesaccountupdatedamount <= 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("CR");
                            }
                        } else if (Salesaccountcurrbaltype.get(i).equals("CR")) {
                            salesaccountupdatedamount = salesaccountamnt1 + (totalamnt - (vatamnt + sgstamnt + cgstamnt));
                            Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                            Salesaccountupdatecurrbaltype.add("CR");
                        }

                        by_ledger = (String) salesaccount.get(i);

                        if (!(jTable1.getValueAt(i, 1) + "").equals("")) {
                            party.add("N/A");
                        } else {
                            try {
                                ResultSet rsparty = stmt1.executeQuery("Select supplier_name from stockid2 where stock_no='" + jTable1.getValueAt(i, 1) + "" + "'");
                                while (rsparty.next()) {
                                    party.add(rsparty.getString(1));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        int meterapplicable = 0;
                        double quantity = 0;
                        double updatedquantity = 0;
                        try {
                            ResultSet rsmeter = stmt1.executeQuery("select ismeter,qnt from stockid2 where stock_no='" + stock.get(i) + "" + "'");
                            while (rsmeter.next()) {
                                meterapplicable = rsmeter.getInt("ismeter");
                                quantity = rsmeter.getDouble("qnt");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String cashaccount = "";
                        if (Double.parseDouble(jTextField34.getText()) != 0) {
                            cashaccount = "CASH";
                        }

                        String Date = formatter.format(jDateChooser1.getDate());

                        if (isIgstApplicable == true) {
                            stmt1.executeUpdate("insert into billing values('" + jTextField1.getText() + "','" + Date + "',"
                                    + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                    + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                    + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                    + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                    + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','" + igstamnt1_1 + "','" + (String) ledgervat.get(i) + "','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','0' , 'SGST 0%' ,'0','CGST 0%','" + jTextField33.getText() + "','" + jTextField30.getText() + "','" + jTextField31.getText() + "','" + jTextField32.getText() + "'," + meterapplicable + ",'" + jTextField37.getText() + "','" + jTextField34.getText() + "','" + cashaccount + "','" + jTextField35.getText() + "','" + sundrydebtors + "','" + jTextField36.getText() + "','" + cardpaymentaccount + "','" + jTextField38.getText() + "','" + jTextField37.getText() + "','" + (String) jComboBox3.getSelectedItem() + "','" + tax_type + "')");

                        } else if (isIgstApplicable == false) {
                            stmt1.executeUpdate("insert into billing values('" + jTextField1.getText() + "','" + Date + "',"
                                    + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                    + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                    + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                    + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                    + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','0','IGST 0%','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','" + sgstamnt1_1 + "','" + (String) ledgersgst.get(i) + "','" + cgstamnt1_1 + "','" + (String) ledgervat2.get(i) + "','" + jTextField33.getText() + "','" + jTextField30.getText() + "','" + jTextField31.getText() + "','" + jTextField32.getText() + "','" + meterapplicable + "','" + jTextField37.getText() + "' ,'" + jTextField34.getText() + "','" + cashaccount + "','" + jTextField35.getText() + "','" + sundrydebtors + "','" + jTextField36.getText() + "','" + cardpaymentaccount + "','" + jTextField38.getText() + "','" + jTextField37.getText() + "','" + (String) jComboBox3.getSelectedItem() + "','" + tax_type + "')");
                        }

                        if (meterapplicable == 1) {
                            updatedquantity = quantity - Double.parseDouble(qnty.get(i) + "");
                            stmt1.executeUpdate("update Stockid2 set qnt=" + updatedquantity + " where Stock_No='" + (stock.get(i) + "").trim() + "'");
                        } else if (meterapplicable == 0) {
                            stmt1.executeUpdate("DELETE from Stockid2 where Stock_No='" + (stock.get(i) + "").trim() + "' LIMIT 1");
                        }
                        if (isIgstApplicable == true) {
                            if (Integer.parseInt(vat.get(i) + "") != 0) {
                                DecimalFormat upft = new DecimalFormat("0.00");
                                String updavat = upft.format(Double.parseDouble(updatevatcurrbal.get(i).toString()));
                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updavat + "" + "',currbal_type='" + updatevatcurrbaltype.get(i) + "" + "' where ledger_name='" + (String) ledgervat.get(i) + "" + "'");
                            }
                        } else if (isIgstApplicable == false) {

                            if (sgst.get(i) != null && cgst.get(i) != null) {

                                DecimalFormat upf = new DecimalFormat("0.00");
                                String updasgst = upf.format(Double.parseDouble(updatesgstcurrbal.get(i).toString()));

                                String updacgst = upf.format(Double.parseDouble(updatecgstcurrbal.get(i).toString()));

                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledgersgst.get(i) + "" + "' ");
                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledgervat2.get(i) + "" + "' ");

                            }
                        }

                        DecimalFormat upf = new DecimalFormat("0.00");
                        String updasales = upf.format(Double.parseDouble(Salesaccountupdatecurrbal.get(i).toString()));

                        stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'");
                        String updatesales = "\"UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'\"";

                        stmt1.executeUpdate("insert into queries values(" + updatesales + ") ");

                        stmt1.executeUpdate("insert into queries1 values(" + updatesales + ") ");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (jTextField34.equals("")) {
                jTextField34.setText("0");
            }

            if (Double.parseDouble(jTextField34.getText()) != 0) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    String cashaccountamount = (String) cashaccountcurrbal.get(0);
                    double cashaccountamount1 = Double.parseDouble(cashaccountamount);
                    double totalamnt = Double.parseDouble(jTextField34.getText());
                    if (cashaccountcurrbaltype.get(0).equals("CR")) {
                        double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                        if (cashaccountupdateamount > 0) {
                            cashaccountupdatecurrbaltype.add("CR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                        if (cashaccountupdateamount <= 0) {
                            cashaccountupdatecurrbaltype.add("DR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                    }
                    if (cashaccountcurrbaltype.get(0).equals("DR")) {
                        double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                        cashaccountupdatecurrbaltype.add("DR");
                        cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                    st.executeUpdate("update ledger set curr_bal='" + cashaccountupdatecurrbal.get(0) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='CASH'");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (jTextField35.getText().equals("")) {
                jTextField35.setText("0");
            }

            if (Double.parseDouble(jTextField35.getText()) != 0) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    String sundrydebtorsaccountamount = (String) sundrydebtorsaccountcurrbal.get(0);
                    double sundrydebtorsaccountamount1 = Double.parseDouble(sundrydebtorsaccountamount);
                    double totalamnt = Double.parseDouble(jTextField35.getText());
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                        double sundrydebtorsaccountupdateamount = sundrydebtorsaccountamount1 - totalamnt;
                        if (sundrydebtorsaccountupdateamount > 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add("CR");
                            sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsaccountupdateamount));
                        }
                        if (sundrydebtorsaccountupdateamount <= 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add("DR");
                            sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsaccountupdateamount));
                        }
                    }
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                        double sundrydebtorsaccountupdateamount = sundrydebtorsaccountamount1 + totalamnt;
                        sundrydebtorsaccountupdatecurrbaltype.add("DR");
                        sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsaccountupdateamount));
                    }
                    st.executeUpdate("update ledger set curr_bal='" + sundrydebtorsaccountupdatecurrbal.get(0) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + sundrydebtors + "'");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (jTextField36.getText().equals("")) {
                jTextField36.setText("0");
            }
            if (Double.parseDouble(jTextField36.getText()) != 0) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    String cardaccountamount = (String) cardaccountcurrbal.get(0);
                    double cardaccountamount1 = Double.parseDouble(cardaccountamount);
                    double totalamnt = Double.parseDouble(jTextField36.getText());
                    if (cardaccountcurrbaltype.get(0).equals("CR")) {
                        double cardaccountupdateamount = cardaccountamount1 - totalamnt;
                        if (cardaccountupdateamount > 0) {
                            cardaccountupdatecurrbaltype.add("CR");
                            cardaccountupdatecurrbal.add(Math.abs(cardaccountupdateamount));
                        }
                        if (cardaccountupdateamount <= 0) {
                            cardaccountupdatecurrbaltype.add("DR");
                            cardaccountupdatecurrbal.add(Math.abs(cardaccountupdateamount));
                        }
                    }
                    if (cardaccountcurrbaltype.get(0).equals("DR")) {
                        double cardaccountupdateamount = cardaccountamount1 + totalamnt;
                        cardaccountupdatecurrbaltype.add("DR");
                        cardaccountupdatecurrbal.add(Math.abs(cardaccountupdateamount));
                    }
                    st.executeUpdate("update ledger set curr_bal='" + cardaccountupdatecurrbal.get(0) + "" + "',currbal_type='" + cardaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + cardpaymentaccount + "'");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            try {
//calculating round of value     
                connection c = new connection();
                Connection connect = c.cone();
                Statement stru = connect.createStatement();
                Statement round_st = connect.createStatement();
                ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
                while (round_rs.next()) {
                    double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                    Round_currbal_type = round_rs.getString("currbal_type");
                    double round_total = (Double.parseDouble(jTextField20.getText()) - ((Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText())) + Double.parseDouble(jTextField31.getText()) + Double.parseDouble(jTextField32.getText())));
                    if (Round_currbal_type.equals("DR") && round_total < 0) {
                        diff_total = Round_curr_bal + Math.abs(round_total);
                    } else if (Round_currbal_type.equals("DR") && round_total > 0) {
                        diff_total = Round_curr_bal - Math.abs(round_total);

                        if (diff_total > 0) {
                            Round_currbal_type = "CR";
                            diff_total = (Math.abs(diff_total));
                        } else if (diff_total < 0) {
                            Round_currbal_type = "DR";
                            diff_total = (Math.abs(diff_total));
                        }
                    }

                    if (Round_currbal_type.equals("CR") && round_total < 0) {
                        diff_total = Round_curr_bal - Math.abs(round_total);

                        if (diff_total > 0) {
                            Round_currbal_type = "DR";
                            diff_total = (Math.abs(diff_total));
                        } else if (diff_total < 0) {
                            Round_currbal_type = "CR";
                            diff_total = (Math.abs(diff_total));
                        }

                    } else if (Round_currbal_type.equals("CR") && round_total > 0) {
                        diff_total = Round_curr_bal + Math.abs(round_total);
                    }

                    DecimalFormat dr1 = new DecimalFormat("0.00");
                    diff_total_round = dr1.format(diff_total);
                }
                System.out.println("When saving diff_total_round" + diff_total_round);
                stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round + "', currbal_type = '" + Round_currbal_type + "' where ledger_name = 'Round Off' ");

            } catch (Exception e) {
                e.printStackTrace();
            }

//            if (Double.parseDouble(jTextField17.getText()) != 0) {
//                double disccurrbal = 0;
//                String disccurrbaltype = "";
//                double discounamntt = Double.parseDouble(jTextField17.getText());
//                try {
//                    connection c = new connection();
//                    Connection connect = c.cone();
//                    Statement statement1 = connect.createStatement();
//                    Statement statement3 = connect.createStatement();
//                    ResultSet rs1 = statement1.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='DISCOUNT ACCOUNT'");
//                    while (rs1.next()) {
//                        disccurrbal = Double.parseDouble(rs1.getString(1));
//                        disccurrbaltype = rs1.getString(2);
//                    }
//
//                    switch (disccurrbaltype) {
//                        case "CR":
//                            disccurrbal = disccurrbal - discounamntt;
//                            if (disccurrbal < 0) {
//                                disccurrbaltype = "DR";
//                                disccurrbal = Math.abs(disccurrbal);
//                                System.out.println("condition one for DR discount is running ");
//                            } else {
//                                disccurrbaltype = "CR";
//                            }
//                            break;
//                        case "DR":
//                            disccurrbal = disccurrbal + discounamntt;
//                            break;
//                    }
//                    disccurrbal = Math.abs(disccurrbal);
//                    statement3.executeUpdate("update ledger set curr_bal='" + disccurrbal + "',currbal_type='" + disccurrbaltype + "' where ledger_name='DISCOUNT ACCOUNT'");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
            if (jRadioButton2.isSelected()) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    st.executeUpdate("insert into againstpayment values('" + jTextField1.getText() + "','','" + formatter.format(jDateChooser1.getDate()) + "','" + sundrydebtors + "','SALES','" + jTextField20.getText() + "','Sales','')");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
            if (i2 == 0) {
                PrinterJob job = PrinterJob.getPrinterJob();
                PageFormat pf = new PageFormat();
                Paper paper = new Paper();

                if (Company.equals("Khabiya")) {
                    paper.setSize(5.8d * 72, 8.3d * 72);
                    double margin = 40;
                    paper.setImageableArea(margin, margin, (paper.getWidth() - 0) * 1, paper.getHeight() - margin * 2);
                } else if (Company.equals("Mahavir")) {
                    double margin = 50;
                    paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
                } else if (Company.equals("Anupam")) {
                    double margin = 50;
                    paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
                } else if (Company.equals("Muskan")) {
                    double margin = 50;
                    paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
                } else if (Company.equals("Rishabh")) {
                    double margin = 50;
                    paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
                }
                pf.setPaper(paper);
                job.setPrintable(this, pf);
                PrintService service = PrintServiceLookup.lookupDefaultPrintService();

                try {
                    job.setPrintService(service);
                    if (Company.equals("Khabiya")) {
                        job.setCopies(2);
                    } else if (Company.equals("Mahavir")) {
                        job.setCopies(1);
                    } else if (Company.equals("Anupam")) {
                        job.setCopies(1);
                    } else if (Company.equals("Muskan")) {
                        job.setCopies(1);
                    } else if (Company.equals("Rishabh")) {
                        job.setCopies(1);
                    }
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                    ex.printStackTrace();
                }
            }
            jDialog1.dispose();


            jTextField5.setEnabled(false);
            jButton1.setVisible(false);
            jButton2.setVisible(false);
            jButton5.setVisible(true);
            jButton6.setVisible(true);
        }
//Conform.dispose();
        this.dispose();
    }

    /* Printing Fromate code for all client */
//---------------------------For Khabiya Printing-------------------------------
    public void khabiyaPrint(Graphics g, PageFormat pf, int page) {
        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fontMetrics = g2d.getFontMetrics();

        Font font = new Font(" Bill NO - SL/1718/1", Font.BOLD, 12);
        g2d.setFont(font);
        g2d.drawString(" Khabiya Cloth Store ", 150, 50);

        Font font2 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 10);
        g2d.setFont(font2);

        g2d.drawString(" 37, New Road ", 150, 60);
        g2d.drawString(" Ratlam - 457001(M.P) ", 150, 70);
        g2d.drawString(" Phone No.- 07412-492939", 150, 80);

        // Horizontal Line            
        g2d.drawLine(0, 85, 420, 85);

        // Left Side box
        g2d.drawString("Invoice No  : ", 15, 100);
        g2d.drawString("Invoice Date : ", 15, 115);
        g2d.drawString("GSTIN : 23AMDPK9264H1ZZ", 15, 130);

        // Virtical Line
        g2d.drawLine(150, 85, 150, 140);

        // Right Side box
        g2d.drawString("Name  :  ", 160, 95);
        g2d.drawString("Address :  ", 160, 105);
        g2d.drawString("Mobile No :  ", 160, 125);
        g2d.drawString("GSTIN No :  ", 160, 135);

        // Horizontal Line            
        g2d.drawLine(0, 140, 420, 140);
        Font font3 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 8);
        g2d.setFont(font3);

        //Table Header View
        g2d.drawString("S No", 15, 150);
        g2d.drawString("  Particulars  ", 50, 150);
        g2d.drawString("Staff", 190, 150);
        g2d.drawString("HSN Code", 230, 150);
        g2d.drawString("Rate", 290, 150);
        g2d.drawString(" Amount", 360, 150);
        g2d.drawString("Total - ", 280, 450);

        g2d.drawString("Net Amount - ", 280, 490);

        // Draw Horizontal line after table
        g2d.drawLine(0, 160, 420, 160);
        g2d.drawLine(0, 440, 420, 440);
        g2d.drawLine(270, 440, 270, 500); // Verticle Line 
        g2d.drawLine(270, 480, 420, 480);
        g2d.drawLine(270, 500, 420, 500);

        // For terms and Condition 
        g2d.drawString("Terms and Condition - ", 15, 477);
        g2d.drawString("1) No Exchange No Claim", 15, 490);
        g2d.drawString("2) Subject to Ratlam jurisdiction", 15, 500);

        //For tax calculation
        g2d.drawString("Tax Calculation - ", 15, 520);
        g2d.drawString("IGST - ", 15, 535);
        g2d.drawString("SGST - ", 85, 535);
        g2d.drawString("CGST - ", 170, 535);

        // For Authority Signatury
        g2d.drawString("For Khabiya :", 320, 555);
        // Getting Data For printing 

        // For upper left side box   
        g2d.drawString(jTextField1.getText(), 75, 100);
        String Date = formatter1.format(jDateChooser1.getDate());
        g2d.drawString(Date, 80, 115);

        // For Uper Right Side Box
        if (!jTextField4.getText().isEmpty()) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select * from party_sales where party_code='" + jTextField4.getText() + "'");
                while (rs.next()) {
                    g2d.drawString(rs.getString("party_name"), 210, 95);
                    g2d.drawString(rs.getString("address1") + "" + rs.getString("address2"), 210, 105);
                    g2d.drawString(rs.getString("address3") + "" + rs.getString("address4"), 210, 115);
                    g2d.drawString(rs.getString("mobile_no"), 215, 125);
                    g2d.drawString(rs.getString("gstin_no"), 215, 135);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!jTextField3.getText().isEmpty()) {
            g2d.drawString(jTextField3.getText(), 210, 100);
            g2d.drawString(jTextField37.getText(), 215, 125);
        }
        // Getting Table data
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = dtm.getRowCount();
        String total_discount = "";
        String total_after_tax = "";
        String total_igsttax = "";
        String total_sgsttax = "";
        String total_cgsttax = "";
        String By_Cash = "";
        String By_Debit = "";
        String By_Card = "";

        double sr_amt = 0;

        int yaxis = 200;

        int next = 0;

        DecimalFormat formatter2 = new DecimalFormat("0.00");

        for (int i = 0; i < rowcount; i++) {
            int j = 10;
            g2d.drawString(jTable1.getValueAt(i, 0) + "", 15, 170 + (j * i)); // For Serial No
            g2d.drawString(jTable1.getValueAt(i, 1) + "", 155, 170 + (j * i)); // For Stock No
            g2d.drawString(jTable1.getValueAt(i, 10) + "", 190, 170 + (j * i)); // For Staff
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(jTable1.getValueAt(i, 3) + ""))) + "", 280, 170 + (j * i));// For Rate
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(jTable1.getValueAt(i, 5) + ""))) + "", 340, 170 + (j * i));// For Amount

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                ResultSet rs2 = st1.executeQuery("select * from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs2.next()) {
                    g2d.drawString(rs2.getString("HSL_CODE"), 230, 170 + (j * i)); // For HSN COde
                    g2d.drawString(rs2.getString("print_On"), 35, 170 + (j * i)); // Product Name
                    ismeter = rs2.getInt("ismeter");
                }
                if (ismeter == 1) {
                    g2d.drawString(jTable1.getValueAt(i, 4) + "" + " (M) ", 250, 170 + (j * i));
                }

                Statement st2 = connect.createStatement();
                ResultSet rs3 = st2.executeQuery("select * from billing where bill_refrence='" + jTextField1.getText() + "'");
                while (rs3.next()) {
                    total_igsttax = rs3.getString("total_igst_taxamnt");
                    total_sgsttax = rs3.getString("total_sgst_taxamnt");
                    total_cgsttax = rs3.getString("total_cgst_taxamnt");
                    total_discount = rs3.getString("total_disc_amnt");
                    total_after_tax = rs3.getString("total_value_after_tax");
                    By_Cash = rs3.getString("cashamount");
                    By_Debit = rs3.getString("sundrydebtorsamount");
                    By_Card = rs3.getString("cardamount");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
// For Sales Return
            String sr_ref = (String) jComboBox3.getSelectedItem();

            if (i == (rowcount - 1)) {
                next = i + 1;
                yaxis = i + 210 + (j * i);
                if (next > i) {

                    if (!jComboBox3.getSelectedItem().toString().equals("Select")) {
                        System.out.println("sr_ref" + sr_ref);
                        g2d.drawLine(0, yaxis, 420, yaxis);
                        g2d.drawString("Sales Return ", 180, yaxis + 10);
                        g2d.drawString(sr_ref, 50, yaxis + 10);
                        g2d.drawLine(0, yaxis + 20, 420, yaxis + 20);

                        String total_srigst = "";
                        String total_srsgst = "";
                        String total_srcgst = "";

                        for (int p = 0; p < rowcount; p++) {
                            try {
                                ArrayList serial_no = new ArrayList();
                                ArrayList Stock_no = new ArrayList();
                                ArrayList Stock_print = new ArrayList();
                                ArrayList hsl_code = new ArrayList();
                                ArrayList srate = new ArrayList();
                                ArrayList samount = new ArrayList();
                                connection c = new connection();
                                Connection connect = c.cone();
                                Statement st1 = connect.createStatement();

                                ResultSet rs1 = st1.executeQuery("Select * from salesreturn where bill_refrence = '" + sr_ref + "" + "' ");
                                while (rs1.next()) {
                                    serial_no.add(rs1.getString("sno"));
                                    Stock_no.add(rs1.getString("stock_no"));
                                    samount.add(rs1.getDouble("total"));
                                    srate.add(rs1.getDouble("value"));
                                    sr_amt = rs1.getDouble("total_amnt");

                                    total_srigst = rs1.getString("total_igst_taxamnt");
                                    total_srsgst = rs1.getString("total_sgst_taxamnt");
                                    total_srcgst = rs1.getString("total_cgst_taxamnt");

                                    Statement st2 = connect.createStatement();
                                    ResultSet rs2 = st2.executeQuery("select * from product where product_code='" + rs1.getString("description") + "' ");
                                    while (rs2.next()) {
                                        Stock_print.add(rs2.getString("print_on"));
                                        hsl_code.add(rs2.getString("HSL_CODE"));
                                    }
                                }

                                g2d.drawString(serial_no.get(p) + "", 5, yaxis + 30 + (j * p));
                                g2d.drawString(Stock_no.get(p) + "", 165, yaxis + 30 + (j * p));
                                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(srate.get(p) + ""))), 280, yaxis + 30 + (j * p));
                                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(samount.get(p) + ""))), 340, yaxis + 30 + (j * p));

                                g2d.drawString(Stock_print.get(p) + "", 35, yaxis + 30 + (j * p));
                                g2d.drawString(hsl_code.get(p) + "", 220, yaxis + 30 + (j * p));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        g2d.drawString("Sale Return - ", 280, 460);
                        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(sr_amt + ""))), 340, 460);

                        g2d.drawLine(0, yaxis + 55 + i, 420, yaxis + 55 + i);

//For Sales return tax calculation
                        g2d.drawString("Sale Return Tax - ", 15, 545);
                        g2d.drawString("IGST - ", 15, 555);
                        g2d.drawString("SGST - ", 85, 555);
                        g2d.drawString("CGST - ", 170, 555);

                        g2d.drawString(total_srigst + "", 40, 555);// For IGST amount
                        g2d.drawString(total_srsgst + "", 115, 555);// For SGST amount
                        g2d.drawString(total_srcgst + "", 200, 555);// For CGST amount

                    }
                }
                System.out.println("Value of next is " + next);
            }
        }
        if (!total_discount.equals("0")) {
            g2d.drawString("Discount     - ", 280, 470);
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(total_discount + ""))), 340, 470);// For Discount amount
        }
        // For Mode of payment
        g2d.drawString("Mode of Payment - ", 15, 450);

        if (!By_Cash.equals("0") && By_Debit.equals("0") && By_Card.equals("0")) {
            g2d.drawString("By Cash  - ", 15, 463);
        } else if (!By_Debit.equals("0") && By_Cash.equals("0") && By_Card.equals("0")) {
            g2d.drawString("By Debit  - ", 15, 463);
        } else if (!By_Card.equals("0") && By_Cash.equals("0") && By_Debit.equals("0")) {
            g2d.drawString("By Card  - ", 15, 463);
        } else {
            if (!By_Cash.equals("0")) {
                g2d.drawString("By Cash  - ", 15, 463);
                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Cash + ""))), 35, 463);// For By cash amount
            }

            if (!By_Debit.equals("0")) {
                g2d.drawString("By Debit  - ", 85, 463);
                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Debit + ""))), 105, 463);// For By debit amount
            }

            if (!By_Card.equals("0")) {
                g2d.drawString("By Card  - ", 170, 463);
                g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Card + ""))), 190, 463);// For By Card amount
            }
        }
        double amnt_after_discount = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(total_discount) - sr_amt;
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(jTextField15.getText() + ""))), 340, 450);// For Total amount
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(amnt_after_discount + ""))), 340, 490);

        g2d.drawString(total_igsttax + "", 40, 535);// For IGST amount
        g2d.drawString(total_sgsttax + "", 115, 535);// For SGST amount
        g2d.drawString(total_cgsttax + "", 200, 535);// For CGST amount

        System.out.println("printing");
    }
//------------------------------------------------------------------------------ 
//-------------------------------For Mahavir Printing---------------------------

    public void mahavirPrint(Graphics g, PageFormat pf, int page) {
        Paper paper = new Paper();
        pf = PrinterJob.getPrinterJob().defaultPage();
        pf.getPaper();
        pf.setPaper(paper);
        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fontMetrics = g2d.getFontMetrics();

        Font font = new Font(" Bill NO - SL/1718/1", Font.BOLD, 14);
        g2d.setFont(font);
        g2d.drawString(" Mahaveer Cloth Emporium ", 215, 80);

        Font font2 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 12);
        g2d.setFont(font2);

        g2d.drawString(" 60, Ghass Bazar ", 215, 90);
        g2d.drawString(" Ratlam (MP) -- 457001 ", 215, 100);
        g2d.drawString(" Phone No.- 07412-471719 / 9424603460 / 9424503001", 215, 110);

// Horizontal Line            
        g2d.drawLine(0, 115, 600, 115);
        Font font3 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 8);
        g2d.setFont(font3);

// Left Side box
        g2d.drawString("Invoice No    : ", 70, 125);
        g2d.drawString("Invoice Date  : ", 70, 140);
        g2d.drawString("GSTIN : 23ABCPJ3364P1ZI", 70, 155);
// Virtical Line
        g2d.drawLine(270, 115, 270, 175);
// Right Side box
        g2d.drawString("Details of Receiver / Billed to:", 350, 125);
        g2d.drawString("Name :  ", 275, 135);
        g2d.drawString("Address : ", 275, 145);
        g2d.drawString("Mobile No  :  ", 275, 160);
        g2d.drawString("GSTIN  :  ", 275, 170);

// Horizontal Line            
        g2d.drawLine(0, 175, 600, 175);

//Table Header View
        g2d.drawString("S No", 50, 190);
        g2d.drawString("Product", 120, 190);
        g2d.drawString("HSN Code", 220, 190);
        g2d.drawString("Quantity", 300, 190);
        g2d.drawString("Rate", 390, 190);
        g2d.drawString("Amount", 490, 190);

        g2d.drawString("Total", 157, 590);

// Draw Varticle line on table
        g2d.drawLine(72, 175, 72, 575);
        g2d.drawLine(200, 175, 200, 575);
        g2d.drawLine(290, 175, 290, 600);
        g2d.drawLine(350, 175, 350, 600);
        g2d.drawLine(465, 175, 465, 600);

// Draw Horizontal inside table            
        g2d.drawLine(0, 205, 600, 205);
        g2d.drawLine(0, 575, 600, 575);
        g2d.drawLine(0, 600, 600, 600);

// GST Related Data info
        g2d.drawString("Total Amount Before Tax :", 355, 615);
        g2d.drawString("Add : CGST :", 355, 625);
        g2d.drawString("Add : SGST :", 355, 635);
        g2d.drawString("Add : IGST :", 355, 645);
//            g2d.drawString("Tax Amount : GST :", 355, 655);
        g2d.drawString("Total Amount After Tax :", 355, 665);

//Horizontal and vertical line for GST Related data info and bank related data info    
        g2d.drawLine(0, 605, 600, 605);
        g2d.drawLine(347, 670, 600, 670);
        g2d.drawLine(347, 605, 347, 670);
        g2d.drawLine(452, 605, 452, 670);

// Bank Related Data Info and horizontal line for this
        g2d.drawString("Bank Details :-", 75, 615);
        g2d.drawString("Allahabad Bank Account No : 50131840198", 75, 630);
        g2d.drawString("SBI Bank Account No : 32757868468", 75, 645);
        g2d.drawString("ICICI Bank Account No : 658205110891", 75, 660);
        g2d.drawLine(0, 665, 347, 665);

// For Authorized Signature
        g2d.drawString("Customer Signature :", 452, 710);

//For Instructions          
        g2d.drawString("INSTRUCTIONS - ", 75, 695);
        g2d.drawString("1) Goods once sold will not be taken back", 75, 705);
        g2d.drawString("2) No cash refunds", 75, 715);

// Getting Data for printing    
// For upper left side box   
        g2d.drawString(jTextField1.getText(), 130, 125);
        String Date = formatter1.format(jDateChooser1.getDate());
        g2d.drawString(Date, 130, 140);

// For table data
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        int total_qunty = 0;
        double total_rate = 0;
        double total_amount = 0;
        double gst = 0;
        String total_before_tax = "";
        String total_after_tax = "";
        String total_igsttax = "";
        String total_scgsttax = "";
        String total_cgsttax = "";
        DecimalFormat df_print = new DecimalFormat("0.00");
        for (int i = 0; i < rowcount; i++) {

            int a = (int) (Double.parseDouble(jTable1.getValueAt(i, 4) + ""));
            int b = (int) (Double.parseDouble(jTextField20.getText()));

            try {
                int j = 10;
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select HSL_CODE from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs.next()) {
                    g2d.drawString(rs.getString("HSL_CODE"), 220, 215 + (j * i));
                }
                Statement st1 = connect.createStatement();
                ResultSet rs1 = st1.executeQuery("select * from billing where bill_refrence='" + jTextField1.getText() + "'");
                while (rs1.next()) {

                    total_before_tax = rs1.getString("total_value_before_tax");
                    total_igsttax = rs1.getString("total_igst_taxamnt");
                    total_scgsttax = rs1.getString("total_sgst_taxamnt");
                    total_cgsttax = rs1.getString("total_cgst_taxamnt");
                    total_after_tax = rs1.getString("total_value_after_tax");
//                        gst = rs1.getDouble("total_cgst_taxamnt") + rs1.getDouble("total_sgst_taxamnt") + rs1.getDouble("total_igst_taxamnt");
                }
//For Upper Right Side box Data
                if (!jTextField4.getText().isEmpty()) {
                    Statement st2 = connect.createStatement();
                    ResultSet rs2 = st2.executeQuery("select * from party_sales where party_code='" + jTextField4.getText() + "'");
                    while (rs2.next()) {
                        g2d.drawString(rs2.getString("party_name"), 325, 135);
                        g2d.drawString(rs2.getString("address1") + "" + rs2.getString("address2") + "" + rs2.getString("city"), 325, 145);
                        g2d.drawString(rs2.getString("mobile_no"), 325, 160);
                        g2d.drawString(rs2.getString("gstin_no"), 325, 170);
                    }
                } else if (!jTextField3.getText().isEmpty()) {
                    Font font5 = new Font(" Bill NO - SL/1718", Font.BOLD, 8);
                    g2d.setFont(font5);
                    g2d.drawString(jTextField3.getText(), 325, 135);
                }

                g2d.drawString(jTable1.getValueAt(i, 0) + "", 50, 215 + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", 100, 215 + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 4) + "", 335 - fontMetrics.stringWidth(jTable1.getValueAt(i, 4).toString()), 215 + (j * i));
                g2d.drawString(df_print.format(Double.parseDouble(jTable1.getValueAt(i, 3) + "")) + "", 425 - fontMetrics.stringWidth(jTable1.getValueAt(i, 3).toString()), 215 + (j * i));
                g2d.drawString(df_print.format(Double.parseDouble(jTable1.getValueAt(i, 9) + "")) + "", 535 - fontMetrics.stringWidth(jTable1.getValueAt(i, 9).toString()), 215 + (j * i));

// For Lower right side box 
                g2d.drawString(total_before_tax, 530 - fontMetrics.stringWidth(total_before_tax), 615);
                g2d.drawString(total_cgsttax, 528 - fontMetrics.stringWidth(total_cgsttax), 625);
                g2d.drawString(total_scgsttax, 528 - fontMetrics.stringWidth(total_scgsttax), 635);
                g2d.drawString(total_igsttax, 528 - fontMetrics.stringWidth(total_igsttax), 645);
//                    g2d.drawString(gst + "", 465, 655);
                g2d.drawString(total_after_tax, 530 - fontMetrics.stringWidth(total_after_tax), 665);
// For table total Data
                total_qunty = total_qunty + Integer.parseInt(jTable1.getValueAt(i, 4).toString());
                total_rate = total_rate + Double.parseDouble(jTable1.getValueAt(i, 3).toString());
                total_amount = total_amount + Double.parseDouble(jTable1.getValueAt(i, 9).toString());

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        String tr = Double.toString(total_rate);
        String ta = Double.toString(total_amount);
// For printing table total Data 
        g2d.drawString(Integer.toString(total_qunty), 335 - fontMetrics.stringWidth(Integer.toString(total_qunty)), 590);
        g2d.drawString(df_print.format(Double.parseDouble(tr)), 440 - fontMetrics.stringWidth(df_print.format(Double.parseDouble(tr))), 590);
        g2d.drawString(df_print.format(Double.parseDouble(ta)), 550 - fontMetrics.stringWidth(df_print.format(Double.parseDouble(ta))), 590);

        System.out.println("printing");
    }
//------------------------------------------------------------------------------
//--------------------------For Anupam Printing---------------------------------

    public void anupamPrint(Graphics g, PageFormat pf, int page) {
        Paper paper = new Paper();
        paper.setSize(8.67d * 72, 5.71d * 72);
        pf.getPaper();
        Graphics2D g2d = (Graphics2D) g;
//    g2d.translate(pf.getImageableX(), pf.getImageableY());
        Font font = new Font(jTextField1.getText(), Font.PLAIN, 12);
        g2d.setFont(font);
        g2d.drawString("     Name:- ", 135, 85);
//            g2d.drawString("36, Dalu Modi Bazar, Ratlam Ph.:233051 ", 265, 81);
        g2d.drawString(jTextField3.getText(), 230, 85);
        g2d.drawString(jTextField1.getText(), 240, 125);
        String Date = formatter1.format(jDateChooser1.getDate());
        g2d.drawString(Date, 230, 105);

        FontMetrics fm3 = g2d.getFontMetrics(font);
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        for (int i = 0; i < rowcount; i++) {
            int a = (int) (Double.parseDouble(jTable1.getValueAt(i, 9) + ""));
            int b = (int) (Double.parseDouble(jTextField20.getText()));
            try {
                int j = 15;
                g2d.drawString(jTable1.getValueAt(i, 0) + "", 165, 180 + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", 230, 180 + (j * i));
                g2d.drawString(a + "", 435 - fm3.stringWidth(a + ""), 180 + (j * i));
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select print_on from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs.next()) {
                    g2d.drawString(rs.getString(1), 290, 180 + (j * i));
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            g2d.drawString(b + "", 440 - fm3.stringWidth(b + ""), 505);
        }
        System.out.println("printing");
    }
//------------------------------------------------------------------------------ 
//-------------------For Muskan Gst Printing------------------------------------

    public void muskanPrint(Graphics g, PageFormat pf, int page) {
        Paper paper = new Paper();
        paper.setSize(8.67d * 72, 5.71d * 72);
        pf.getPaper();
        Graphics2D g2d = (Graphics2D) g;

//    g2d.translate(pf.getImageableX(), pf.getImageableY());
        Font font = new Font(jTextField1.getText(), Font.PLAIN, 10);
        g2d.setFont(font);
        g2d.drawString("SALES INVOICE ", 280, 135);
        g2d.drawString("Name :" + jTextField3.getText(), 165, 155);
        g2d.drawString("Bill No :" + jTextField1.getText(), 165, 145);

        String Date = formatter1.format(jDateChooser1.getDate());
        g2d.drawString("Date :" + Date, 330, 145);

        g2d.drawLine(165, 165, 550, 165);
        g2d.drawString("SNO", 165, 175);
        g2d.drawString("PARTICULAR", 230, 175);
        g2d.drawString("AMOUNT", 425, 175);
        g2d.drawLine(165, 185, 550, 185);

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        for (int i = 0; i < rowcount; i++) {
            int a = (int) (Double.parseDouble(jTable1.getValueAt(i, 9) + ""));
            int b = (int) (Double.parseDouble(jTextField20.getText()));
            try {
                int j = 15;
                g2d.drawString(jTable1.getValueAt(i, 0) + "", 165, 210 + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", 230, 210 + (j * i));
                g2d.drawString(a + "", 435, 210 + (j * i));
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select print_on from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs.next()) {
                    g2d.drawString(rs.getString(1), 290, 210 + (j * i));
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            g2d.drawString("Total Amount :" + jTextField15.getText() + "", 400, 505);
            if (Integer.parseInt(jTextField17.getText()) != 0) {
                g2d.drawString("SPL OFFER :" + jTextField17.getText(), 400, 515);
            }
            g2d.drawString("Net Amount :" + b + "", 400, 525);
        }
    }
//------------------------------------------------------------------------------    
//---------------------For Rishabh Printing-------------------------------------

    public void rishabhPrint(Graphics g, PageFormat pf, int page) {
        Paper paper = new Paper();
        paper.setSize(8.67d * 72, 5.71d * 72);
        pf.getPaper();
        Graphics2D g2d = (Graphics2D) g;

//    g2d.translate(pf.getImageableX(), pf.getImageableY());
        Font font = new Font(jTextField1.getText(), Font.PLAIN, 10);
        g2d.setFont(font);
        g2d.drawString("SALES INVOICE ", 280, 135);
        g2d.drawString("Name :" + jTextField3.getText(), 165, 155);
        g2d.drawString("Bill No :" + jTextField1.getText(), 165, 145);

        String Date = formatter1.format(jDateChooser1.getDate());
        g2d.drawString("Date :" + Date, 330, 145);

        g2d.drawLine(165, 165, 550, 165);
        g2d.drawString("SNO", 165, 175);
        g2d.drawString("PARTICULAR", 230, 175);
        g2d.drawString("AMOUNT", 425, 175);
        g2d.drawLine(165, 185, 550, 185);

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        for (int i = 0; i < rowcount; i++) {
            int a = (int) (Double.parseDouble(jTable1.getValueAt(i, 9) + ""));
            int b = (int) (Double.parseDouble(jTextField20.getText()));
            try {
                int j = 15;
                g2d.drawString(jTable1.getValueAt(i, 0) + "", 165, 210 + (j * i));
                g2d.drawString(jTable1.getValueAt(i, 1) + "", 230, 210 + (j * i));
                g2d.drawString(a + "", 435, 210 + (j * i));
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select print_on from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs.next()) {
                    g2d.drawString(rs.getString(1), 290, 210 + (j * i));
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            g2d.drawString("Total Amount :" + jTextField15.getText() + "", 400, 505);
            if (Integer.parseInt(jTextField17.getText()) != 0) {
                g2d.drawString("SPL OFFER :" + jTextField17.getText(), 400, 515);
            }
            g2d.drawString("Net Amount :" + b + "", 400, 525);
        }
    }
//------------------------------------------------------------------------------    

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (issave == false) {

            jDialog4.setVisible(true);
            jLabel48.setText(jTextField20.getText());
            jButton12.setVisible(false);
            if (jRadioButton1.isSelected()) {
                jTextField34.setText(jTextField20.getText());
            } else if (jRadioButton2.isSelected()) {
                jTextField35.setText(jTextField20.getText());
            } else if (jRadioButton5.isSelected()) {
                jTextField36.setText(jTextField20.getText());
            }

        }
    }//GEN-LAST:event_jButton1ActionPerformed
    @Override
    public int print(Graphics g, PageFormat pf, int page)
            throws PrinterException {
        if (page > 0) {
            return NO_SUCH_PAGE;
        } else {
            if (Company.equals("Khabiya")) {
                khabiyaPrint(g, pf, page);
            } else if (Company.equals("Mahavir")) {
                mahavirPrint(g, pf, page);
            } else if (Company.equals("Anupam")) {
                anupamPrint(g, pf, page);
            } else if (Company.equals("Muskan")) {
                muskanPrint(g, pf, page);
            } else if (Company.equals("Rishabh")) {
                rishabhPrint(g, pf, page);
            }
            return 0;
        }
    }
    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // TODO add your handling code here:
        if (jRadioButton1.isSelected()) {
            isIgstApplicable = false;
            jTextField3.setEnabled(true);
            jTextField4.setEnabled(false);
            jTextField4.setText("");
            int rowcount = jTable1.getRowCount();
            if (rowcount != 0) {
                party_update();
            }
        }
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // TODO add your handling code here:
        if (jRadioButton2.isSelected()) {
//            jTextField3.setEnabled(true);
            jLabel4.setVisible(true);
            jTextField4.setVisible(true);
            jTextField4.setEnabled(true);

            jDialog3.setVisible(true);
        }
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void list1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list1ActionPerformed
        // TODO add your handling code here:
        cardpaymentaccount = list1.getSelectedItem();
        jDialog2.setVisible(false);
        jLabel49.setVisible(true);
        jTextField39.setVisible(true);
    }//GEN-LAST:event_list1ActionPerformed

    private void jTextField20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField20FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField20FocusLost

    private void jTextField8FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField8FocusLost
        // TODO add your handling code here:

//        String ra_te = jTextField7.getText();
//        String qun_tity = jTextField8.getText();
//        double p = Double.parseDouble(ra_te);
//        double q = Double.parseDouble(qun_tity);
//        double amou_nt = (p * q);
//        //	String t = Integer.toString(amou_nt);
//        jTextField9.setText("" + String.valueOf(amou_nt));
    }//GEN-LAST:event_jTextField8FocusLost

    private void jTextField12FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField12FocusLost
        // TODO add your handling code here:
        double disc_amt = Double.parseDouble(jTextField12.getText());
        double value = Double.parseDouble(jTextField9.getText());
        double total = value - disc_amt;
        jTextField13.setText("" + String.valueOf(total));
    }//GEN-LAST:event_jTextField12FocusLost


    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jTextField14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField14ActionPerformed

    private void jTextField14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField14KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_jTextField14KeyPressed

    private void jTextField15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField15KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField16.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jTextField15KeyPressed

    private void jTextField16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField16KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField17.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField15.requestFocus();
        }
    }//GEN-LAST:event_jTextField16KeyPressed

    private void jTextField17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField17KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField18.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField16.requestFocus();
        }
    }//GEN-LAST:event_jTextField17KeyPressed

    private void jTextField18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField18KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField19.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jTextField18KeyPressed

    private void jTextField19KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField19KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField20.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField18.requestFocus();
        }
    }//GEN-LAST:event_jTextField19KeyPressed

    private void jTextField13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField13KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox1.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField12.requestFocus();
        }
    }//GEN-LAST:event_jTextField13KeyPressed

    private void jTextField18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField18ActionPerformed

    private void jTextField13FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField13FocusGained
//        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField13FocusGained

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField12KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField13.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField12KeyPressed

    private void jTextField11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField11KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField12.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField9.requestFocus();
        }
    }//GEN-LAST:event_jTextField11KeyPressed

    private void jTextField9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField9KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField8.requestFocus();
        }
    }//GEN-LAST:event_jTextField9KeyPressed

    private void jTextField17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField17FocusLost
        // TODO add your handling code here:
        double discount_amount = Double.parseDouble(jTextField17.getText());

        if (discount_amount == 0) {

        } else {
            DecimalFormat df = new DecimalFormat("0.00");
            double totalcgstamnt = 0;
            double totaligstamnt = 0;
            double totalamountbeforetax = 0;
            double amountbeforetax = 0;
            int rowcount = jTable1.getRowCount();
            for (int i = 0; i < rowcount; i++) {
                double totalamntafterbill = Double.parseDouble(jTextField15.getText());
                double discountpercentage = (discount_amount / totalamntafterbill) * 100;

                double totalafterdiscountpercentage = (discountpercentage / 100) * Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                double totalfinal = Double.parseDouble(jTable1.getValueAt(i, 5) + "") - totalafterdiscountpercentage;

                jTable1.setValueAt(df.format(totalfinal) + "", i, 9);

                String igstaccount = "";
                String sgstaccount = "";
                String cgstaccount = "";
                double gstperc = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stmt1 = connect.createStatement();

                    if (isIgstApplicable == true) {
                        ResultSet rsigst = stmt1.executeQuery("select IGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rsigst.next()) {
                            igstaccount = rsigst.getString(1);
                        }
                        ResultSet rsIGSTperc = stmt1.executeQuery("select percent from ledger where ledger_name ='" + igstaccount + "'");
                        while (rsIGSTperc.next()) {
                            gstperc = rsIGSTperc.getDouble(1);
                        }

//                        amountbeforetax = (totalfinal / (100 + gstperc)) * 100;
                        if (jRadioButton3.isSelected()) {
                            amountbeforetax = (totalfinal / (100 + gstperc)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amountbeforetax = (totalfinal);
                        }
                        double igstamnt = (gstperc * amountbeforetax) / 100;

                        totaligstamnt = igstamnt + totaligstamnt;

                        totalamountbeforetax = amountbeforetax + totalamountbeforetax;

                    } else if (isIgstApplicable == false) {
                        ResultSet rssgst = stmt1.executeQuery("select SGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rssgst.next()) {
                            sgstaccount = rssgst.getString(1);
                        }
                        ResultSet rscgst = stmt1.executeQuery("select CGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rscgst.next()) {
                            cgstaccount = rscgst.getString(1);
                        }
                        ResultSet rsSGSTperc = stmt1.executeQuery("select percent from ledger where ledger_name ='" + sgstaccount + "'");
                        while (rsSGSTperc.next()) {
                            gstperc = rsSGSTperc.getDouble(1);
                        }
                        //                       amountbeforetax = (totalfinal / (100 + gstperc + gstperc)) * 100;
                        if (jRadioButton3.isSelected()) {
                            amountbeforetax = (totalfinal / (100 + gstperc + gstperc)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amountbeforetax = (totalfinal);
                        }
                        double cgstamnt = (gstperc * amountbeforetax) / 100;

                        totalcgstamnt = cgstamnt + totalcgstamnt;

                        totalamountbeforetax = amountbeforetax + totalamountbeforetax;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            jTextField31.setText(df.format(totalcgstamnt) + "");
            jTextField32.setText(df.format(totalcgstamnt) + "");
            jTextField19.setText(df.format(totalamountbeforetax) + "");
            jTextField20.setText((Double.parseDouble(jTextField15.getText()) - (discount_amount)) + "");
            jTextField18.setText(df.format(Double.parseDouble(jTextField15.getText()) - (discount_amount)) + "");
            jTextField33.setText(df.format(Math.abs(Double.parseDouble(jTextField20.getText()) - Double.parseDouble(jTextField19.getText()) - Double.parseDouble(jTextField30.getText()) - Double.parseDouble(jTextField31.getText()) - Double.parseDouble(jTextField32.getText()))) + "");
            jTextField30.setText(df.format(totaligstamnt) + "");
        }
    }//GEN-LAST:event_jTextField17FocusLost
    int row = 0;
    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_DELETE) {
            DefaultTableModel dtm = null;
            dtm = (DefaultTableModel) jTable1.getModel();
            row = jTable1.getSelectedRow();
            double totalamt1 = Double.parseDouble((String) jTable1.getValueAt(row, 5));
            double nettotal = Double.parseDouble(jTextField15.getText());
            double nettotal1 = nettotal - totalamt1;
            jTextField15.setText("" + nettotal1);
            totalamount = totalamount - totalamt1;
            totalamountaftertax = totalamountaftertax - totalamt1;

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement sttt = connect.createStatement();
                Statement sttt1 = connect.createStatement();
                Statement pro_st = connect.createStatement();
                Statement sgst_st = connect.createStatement();
                Statement cgst_st = connect.createStatement();

                if (isIgstApplicable == true) {
                    ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "" + "'");
                    while (rsss.next()) {
                        ResultSet rssss = sttt1.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");

                        while (rssss.next()) {
                            double vat = Double.parseDouble(rssss.getString("percent"));
                            double value1 = Double.parseDouble(jTable1.getValueAt(row, 5) + "");

                            if (jRadioButton3.isSelected()) {
                                amntbeforetax = (value1 / (100 + vat)) * 100;
                            } else if (jRadioButton4.isSelected()) {
                                amntbeforetax = (value1);
                            }
                            iv = (amntbeforetax * vat) / 100;
                        }
                    }

                    totalamountbeforetax = (totalamountbeforetax - amntbeforetax);
                    tiv = (tiv - iv);
                    DecimalFormat df1 = new DecimalFormat("0.00");

                    jTextField19.setText("" + df1.format(totalamountbeforetax));
                    jTextField30.setText("" + df1.format(tiv));
                    jTextField31.setText("0.00");
                    jTextField32.setText("0.00");

                } else if (isIgstApplicable == false) {
                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "'");
                    while (pro_rs.next()) {
                        ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                        while (sgst_rs.next()) {
                            sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                        }

                        ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                        while (cgst_rs.next()) {
                            cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                        }
                    }

                    totalpercent = sgstpercent + cgstpercent;
                    double value1 = Double.parseDouble(jTable1.getValueAt(row, 5) + "");

                    if (jRadioButton3.isSelected()) {
                        amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                    } else if (jRadioButton4.isSelected()) {
                        amntbeforetax = (value1);
                    }
                    sv = (amntbeforetax * sgstpercent) / 100;
                    cv = ((amntbeforetax * cgstpercent) / 100);

                    totalamountbeforetax = (totalamountbeforetax - amntbeforetax);
                    tsv = (tsv - sv);
                    tcv = (tcv - cv);
                    DecimalFormat df1 = new DecimalFormat("0.00");

                    jTextField19.setText("" + df1.format(totalamountbeforetax));
                    jTextField30.setText("0.00");
                    jTextField31.setText("" + df1.format(tsv));
                    jTextField32.setText("" + df1.format(tcv));

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            double discamt = 0;
            double disctotal = 0;

            if (jTable1.getValueAt(row, 8).toString().isEmpty()) {

            } else {
                discamt = Double.parseDouble((String) jTable1.getValueAt(row, 8));
            }
            disctotal = Double.parseDouble(jTextField17.getText());
            disctotal = disctotal - discamt;
            totaldiscount = totaldiscount - discamt;
            jTextField17.setText("" + disctotal);
            double netamttotal = 0;
            netamttotal = nettotal1 - disctotal;
            jTextField18.setText("" + netamttotal);
            jTextField20.setText("" + netamttotal);
            double qnty = 0;
            double qntytotal = 0;
            qnty = Double.parseDouble((String) jTable1.getValueAt(row, 4));
            qntytotal = Double.parseDouble(jTextField16.getText());
            qntytotal = qntytotal - qnty;
            totalquantity = totalquantity - qnty;
            jTextField16.setText("" + qntytotal);

// Round off 
            DecimalFormat df2 = new DecimalFormat("0.00");
            trv = Double.parseDouble(jTextField20.getText()) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText())));
            jTextField33.setText("" + df2.format(trv));

            int rowcount = dtm.getRowCount();

            for (int i = row + 1; i < rowcount; i++) {
                int row1 = row + 2;
                jTable1.getModel().setValueAt(--row1, i, 0);
                row++;
            }
            int a = Integer.parseInt(jTextField14.getText());
            a = a - 1;
            jTextField14.setText("" + a);

            row = jTable1.getSelectedRow();
            dtm.removeRow(row);
            dtm.fireTableDataChanged();

            int rowcount1 = jTable1.getRowCount();
            if (rowcount1 == 0) {
                jTextField17.setText("0");
                jTextField15.setText("0");
                jTextField18.setText("0");
                jTextField20.setText("0");
                jTextField19.setText("0");
                jTextField30.setText("0.00");
                jTextField31.setText("0.00");
                jTextField32.setText("0.00");
                jTextField33.setText("0.00");
            }
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTextField19FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField19FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField19FocusLost

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jRadioButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton5ActionPerformed
        // TODO add your handling code here:
        jDialog2.setVisible(true);
        if (jRadioButton5.isSelected()) {
            isIgstApplicable = false;
            jTextField3.setVisible(true);
            int rowcount = jTable1.getRowCount();
            if (rowcount != 0) {
                party_update();
            }
        }
    }//GEN-LAST:event_jRadioButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        jDialog1.hide();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        jDialog1.dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jComboBox1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1FocusGained

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jComboBox1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        // TODO add your handling code here:
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_RIGHT) {
            jButton1.requestFocus();
        }
    }//GEN-LAST:event_jButton2KeyPressed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        dispose();
        new New_Dual_Billing().setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
        if (i2 == 0) {
            PrinterJob job = PrinterJob.getPrinterJob();
            PageFormat pf = new PageFormat();
            Paper paper = new Paper();
//            paper.setSize(5.8d * 72, 8.3d * 72);
//            double margin = 40;
//            paper.setImageableArea(margin, margin, paper.getWidth() - 0 * 2, paper.getHeight() - margin * 2);
            if (Company.equals("Khabiya")) {
                paper.setSize(5.8d * 72, 8.3d * 72);
                double margin = 40;
                paper.setImageableArea(margin, margin, (paper.getWidth() - 0) * 1, paper.getHeight() - margin * 2);
            } else if (Company.equals("Mahavir")) {
                double margin = 50;
                paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
            } else if (Company.equals("Anupam")) {
                double margin = 50;
                paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
            } else if (Company.equals("Muskan")) {
                double margin = 50;
                paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
            } else if (Company.equals("Rishabh")) {
                double margin = 50;
                paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
            }
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            PrintService service = PrintServiceLookup.lookupDefaultPrintService();
            try {
                job.setPrintService(service);
                job.setCopies(1);
                job.print();
            } catch (PrinterException ex) {
                /* The job did not successfully complete */
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    double s1 = 0;
    double cashamount = 0;
    String cashaccount = "";
    double debtoramount = 0;
    String debtoraccount = "";
    double cardamount = 0;
    String cardupdateaccount = "";
    double v1 = 0;
    double round1 = 0;
    double reducingtax = 0;
    String Party_ledger = "";
    ArrayList stock1 = new ArrayList();
    ArrayList vat1 = new ArrayList();
    ArrayList ledgervat1 = new ArrayList();
    ArrayList sgst1 = new ArrayList();
    ArrayList ledgersgst1 = new ArrayList();
    ArrayList cgst1 = new ArrayList();
    ArrayList ledgercgst1 = new ArrayList();
    int ismeterupdate = 0;
    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        jDialog4.setVisible(true);
        jButton9.setVisible(false);
        jButton12.setVisible(true);
        jLabel48.setText(jTextField20.getText());
        if (jRadioButton1.isSelected()) {
            jTextField34.setText(jTextField20.getText());
        } else if (jRadioButton2.isSelected()) {
            jTextField35.setText(jTextField20.getText());
        } else if (jRadioButton5.isSelected()) {
            jTextField36.setText(jTextField20.getText());
        }

    }//GEN-LAST:event_jButton7ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2KeyPressed

    private void jTextField17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField17ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField17ActionPerformed

    private void jTextField26CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField26CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();

        String party = jTextField26.getText();
        if (party.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party_sales where party_code  LIKE '%" + party + "%'");

                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};

                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField26CaretUpdate

    private void jTextField26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField26ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField26ActionPerformed

    private void jTextField26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField26KeyPressed

    }//GEN-LAST:event_jTextField26KeyPressed

    private void jTextField27CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField27CaretUpdate
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String partyName = jTextField27.getText();
        if (partyName.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party_sales where party_name LIKE'%" + partyName + "%'");
                while (rs.next()) {

                    String igst = "";
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField27CaretUpdate

    private void jTextField27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField27ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField27ActionPerformed

    private void jTextField27KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField27KeyPressed
        int key = evt.getKeyCode();
        if (key == evt.VK_DOWN) {
            jTable3.requestFocus();
        }
    }//GEN-LAST:event_jTextField27KeyPressed

    private void jTextField28CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField28CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String city = jTextField28.getText();
        if (city.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party_sales where city LIKE '%" + city + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField28CaretUpdate

    private void jTextField28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField28KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField28KeyPressed

    private void jTextField29CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField29CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String state = jTextField29.getText();
        System.out.println("state is" + state);
        if (state.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party_sales where state LIKE '%" + state + "%'");
                while (rs.next()) {
                    String igst;
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }
                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }
            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField29CaretUpdate

    private void jTextField29KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField29KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField29KeyPressed

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        partyselect();
        int rowcount = jTable1.getRowCount();
        if (rowcount != 0) {
            party_update();
        }
    }//GEN-LAST:event_jTable3MouseClicked

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            partyselect();
        }

        if (key == evt.VK_BACK_SPACE) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_LEFT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_RIGHT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_SPACE) {
            jTextField18.requestFocus();
        }
    }//GEN-LAST:event_jTable3KeyPressed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        master.customer.Add_Sales_Party addnew = new master.customer.Add_Sales_Party();
        addnew.setVisible(true);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jTextField35KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField35KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog3.setVisible(true);
        }
       
        if (key == evt.VK_ENTER) {
            double netamount = Double.parseDouble(jTextField20.getText());
            double cashamount = Double.parseDouble(jTextField34.getText());
            double sundrydebtorsamount=Double.parseDouble(jTextField35.getText());
            double cardamount=Double.parseDouble(jTextField36.getText());
            double netcalculatedamount=cashamount+sundrydebtorsamount+cardamount;
            
            if (netcalculatedamount < netamount) {
                jTextField36.setText("" +(netamount - netcalculatedamount));
                jTextField36.requestFocus();
            } else if (netcalculatedamount == netamount) {
                jButton9.requestFocus();
            }
        }
    }//GEN-LAST:event_jTextField35KeyPressed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        if ((Double.parseDouble(jTextField35.getText())) != 0 && sundrydebtors == null) {
            JOptionPane.showMessageDialog(rootPane, "lease select sundry debtors account");
        } else if ((Double.parseDouble(jTextField36.getText())) != 0 && cardpaymentaccount == null) {
            JOptionPane.showMessageDialog(rootPane, "Please select Bank Account");
        } else if ((Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) < Double.parseDouble(jTextField20.getText()) || (Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) > Double.parseDouble(jTextField20.getText())) {
            JOptionPane.showMessageDialog(rootPane, "The Amount Distributed is not equal to net amount please rectify ");
        } else {
            jDialog4.dispose();
            save();
        }
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jTextField36KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField36KeyPressed
        // TODO add your handling code here:
          int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
        }
          if (key == evt.VK_ENTER) {
            double netamount = Double.parseDouble(jTextField20.getText());
            double cashamount1 = Double.parseDouble(jTextField34.getText());
            double sundrydebtorsamount=Double.parseDouble(jTextField35.getText());
            double cardamount1=Double.parseDouble(jTextField36.getText());
            double netcalculatedamount=cashamount1+sundrydebtorsamount+cardamount1;
            
            if (netcalculatedamount < netamount) {
                jTextField36.setText("" +(netamount - netcalculatedamount));
                jTextField36.requestFocus();
            } else if (netcalculatedamount == netamount) {
                jButton9.requestFocus();
            }
        }
    }//GEN-LAST:event_jTextField36KeyPressed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        jDialog2.setVisible(true);
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jTextField34KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField34KeyPressed
        // TODO add your handling code here:
         int key = evt.getKeyCode();
        if (key == evt.VK_ENTER) {
            double netamount = Double.parseDouble(jTextField20.getText());
            double cashamount = Double.parseDouble(jTextField34.getText());
            double sundrydebtorsamount=Double.parseDouble(jTextField35.getText());
            double cardamount=Double.parseDouble(jTextField36.getText());
            double netcalculatedamount=cashamount+sundrydebtorsamount+cardamount;
            
            if (netcalculatedamount < netamount) {
                jTextField35.setText("" +(netamount - netcalculatedamount));
                jTextField35.requestFocus();
            } else if (netcalculatedamount == netamount) {
                jButton9.requestFocus();
            }
        }
    }//GEN-LAST:event_jTextField34KeyPressed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        jDialog3.setVisible(true);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton9KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            if ((Double.parseDouble(jTextField35.getText())) != 0 && sundrydebtors == null) {
                JOptionPane.showMessageDialog(rootPane, "lease select sundry debtors account");
            } else if ((Double.parseDouble(jTextField36.getText())) != 0 && cardpaymentaccount == null) {
                JOptionPane.showMessageDialog(rootPane, "Please select Bank Account");
            } else if ((Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) < Double.parseDouble(jTextField20.getText()) || (Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) > Double.parseDouble(jTextField20.getText())) {
                JOptionPane.showMessageDialog(rootPane, "The Amount Distributed is not equal to net amount please rectify ");
            } else {
                jDialog4.dispose();
                save();
            }

        }

    }//GEN-LAST:event_jButton9KeyPressed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Update");
        if (i2 == 0) {
            if ((Double.parseDouble(jTextField35.getText())) != 0 && sundrydebtors == null) {
                JOptionPane.showMessageDialog(rootPane, "lease select sundry debtors account");
            } else if ((Double.parseDouble(jTextField36.getText())) != 0 && cardpaymentaccount == null) {
                JOptionPane.showMessageDialog(rootPane, "Please select Bank Account");
            } else if ((Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) < Double.parseDouble(jTextField20.getText()) || (Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) > Double.parseDouble(jTextField20.getText())) {
                JOptionPane.showMessageDialog(rootPane, "The Amount Distributed is not equal to net amount please rectify ");
            } else {
            
            ArrayList old_sundrydebtorsaccountcurrbal = new ArrayList();
            ArrayList old_sundrydebtorsaccountcurrbaltype = new ArrayList();
            ArrayList old_sundrydebtorsaccountupdatecurrbaltype = new ArrayList();
            ArrayList old_sundrydebtorsaccountupdatecurrbal = new ArrayList();
            ArrayList old_cardaccountcurrbal = new ArrayList();
            ArrayList old_cardaccountcurrbaltype = new ArrayList();
            ArrayList old_cardaccountupdatecurrbal = new ArrayList();
            ArrayList old_cardaccountupdatecurrbaltype = new ArrayList();
            ArrayList old_cashaccountcurrbal = new ArrayList();
            ArrayList old_cashaccountcurrbaltype = new ArrayList();
            ArrayList old_cashaccountupdatecurrbal = new ArrayList();
            ArrayList old_cashaccountupdatecurrbaltype = new ArrayList();
            double discounamntt = 0;
            try {
                String paytype = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                ResultSet rs = st1.executeQuery("select * from billing where bill_refrence='" + jTextField1.getText() + "'");
                while (rs.next()) {
                    paytype = rs.getString(3);
//                    c1 = Double.parseDouble(rs.getString("Net_Total"));
                    cashamount = Double.parseDouble(rs.getString("cashamount"));
                    cashaccount = rs.getString("cashaccount");
                    debtoramount = Double.parseDouble(rs.getString("sundrydebtorsamount"));
                    debtoraccount = rs.getString("sundrydebtorsaccount");
                    cardamount = Double.parseDouble(rs.getString("cardamount"));
                    cardupdateaccount = rs.getString("cardaccount");
                    ismeterupdate = rs.getInt("ismeter");
                    if (rs.getString("Round_OFf_Value").equals("null") || rs.getString("Round_OFf_Value").equals("") || rs.getString("Round_OFf_Value") == null) {
                        round1 = 0;
                    } else {
                        round1 = Double.parseDouble(rs.getString("Round_OFf_Value"));
                    }

                    Party_ledger = rs.getString("by_ledger");
                    reducingtax = rs.getDouble("total_value_before_tax");
                    discounamntt = Double.parseDouble(rs.getString("total_disc_amnt"));

                    Statement stmt3 = connect.createStatement();

                    if (ismeterupdate == 0) {
                        ResultSet rsstock = stmt3.executeQuery("select * from stockid where Stock_No='" + rs.getString(7) + "'");
                        while (rsstock.next()) {
                            if (rsstock.getString(2) != null) {
                                Statement stmt4 = connect.createStatement();
                                stmt4.executeUpdate("insert into stockid2 values('" + rsstock.getString(2) + "','" + rsstock.getString(3) + "','" + rsstock.getString(4) + "','" + rsstock.getString(5) + "','" + rsstock.getString(6) + "','" + rsstock.getString(7) + "','" + rsstock.getString(8) + "','" + rsstock.getString(11) + "','" + rsstock.getString(10) + "','" + rsstock.getString(9) + "','" + rsstock.getString(13) + "','" + ismeterupdate + "','1')");
                            }
                        }
                    } else if (ismeterupdate == 1) {
                        double qntstock = 0;
                        double qnt = 0;
                        double totalqnty = 0;
                        ResultSet rsstock = stmt3.executeQuery("select qnt from stockid2 where Stock_No='" + rs.getString(7) + "'");
                        while (rsstock.next()) {
                            qntstock = rsstock.getDouble(1);

                        }
                        qnt = rs.getDouble("qnty");
                        totalqnty = qnt + qntstock;

                        stmt3.executeUpdate("update stockid2 set qnt='" + totalqnty + "" + "' where stock_no='" + rs.getString(8) + "'");

                    }

                    {
                        ledgervat1.add(rs.getString("vataccount"));
                        Statement stmt1 = connect.createStatement();
                        Statement stmtvataccount = connect.createStatement();
                        ResultSet rsvataccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString(24) + "' ");
                        while (rsvataccount.next()) {
                            vat1.add(rsvataccount.getString(6));
                            vatcurrbal.add(rsvataccount.getString(10));
                            vatcurrbaltype.add(rsvataccount.getString(11));

                            vatamnt = rs.getDouble("vatamnt");

                            DecimalFormat idf = new DecimalFormat("0.00");
                            igstamnt1_1 = idf.format(vatamnt);

                            String vatcurrbalance = (String) vatcurrbal.get(0);
                            String Curbaltypevat = (String) vatcurrbaltype.get(0);
                            double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                            double vatamnt1 = vatamnt;
                            double result;

                            if (Curbaltypevat.equals("CR")) {
                                result = vatcurrbalance1 - vatamnt1;
                                if (result >= 0) {
                                    updatevatcurrbaltype.add(0, "CR");
                                    updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatevatcurrbaltype.add(0, "DR");
                                    updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypevat.equals("DR")) {

                                result = vatcurrbalance1 + vatamnt1;

                                updatevatcurrbaltype.add(0, "DR");
                                updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Integer.parseInt(vat1.get(0) + "") != 0) {
                                DecimalFormat upvat = new DecimalFormat("0.00");
                                String updavat = upvat.format(Double.parseDouble(updatevatcurrbal.get(0).toString()));
                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updavat + "" + "',currbal_type='" + updatevatcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgervat1.get(0) + "" + "'");
                            }
                            vat1.clear();
                            vatcurrbal.clear();
                            vatcurrbaltype.clear();
                            updatevatcurrbal.clear();
                            updatevatcurrbaltype.clear();
                        }
                    }

// For SGST calculation
                    {
                        ledgersgst1.add(rs.getString("Sgstaccount"));
                        Statement stmt_sgst = connect.createStatement();
                        Statement stmtsgstaccount = connect.createStatement();
                        ResultSet rssgstaccount = stmtsgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("Sgstaccount") + "' ");
                        while (rssgstaccount.next()) {
                            sgst1.add(rssgstaccount.getString(6));
                            sgstcurrbal.add(rssgstaccount.getString(10));
                            sgstcurrbaltype.add(rssgstaccount.getString(11));

                            sgstperc = Double.parseDouble((String) sgst1.get(0));

                            sgstamnt = rs.getDouble("sgstamnt");

                            DecimalFormat idf = new DecimalFormat("0.00");
                            sgstamnt1_1 = idf.format(sgstamnt);

                            String sgstcurrbalance = (String) sgstcurrbal.get(0);
                            String Curbaltypesgst = (String) sgstcurrbaltype.get(0);
                            double sgstcurrbalance1 = Double.parseDouble(sgstcurrbalance);
                            double sgstamnt1 = sgstamnt;
                            double result;

                            if (Curbaltypesgst.equals("CR")) {
                                result = sgstcurrbalance1 - sgstamnt1;
                                if (result >= 0) {
                                    updatesgstcurrbaltype.add(0, "CR");
                                    updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatesgstcurrbaltype.add(0, "DR");
                                    updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypesgst.equals("DR")) {

                                result = sgstcurrbalance1 + sgstamnt1;

                                updatesgstcurrbaltype.add(0, "DR");
                                updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Double.parseDouble(sgst1.get(0) + "") != 0) {
                                DecimalFormat upsgst = new DecimalFormat("0.00");
                                String updasgst = upsgst.format(Double.parseDouble(updatesgstcurrbal.get(0).toString()));
                                stmt_sgst.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgersgst1.get(0) + "" + "'");
                            }

                            sgst1.clear();
                            sgstcurrbal.clear();
                            sgstcurrbaltype.clear();
                            updatesgstcurrbal.clear();
                            updatesgstcurrbaltype.clear();

                        }
                    }

// For CGST calculation
                    {
                        ledgercgst1.add(rs.getString("Cgstaccount"));
                        Statement stmt_cgst = connect.createStatement();
                        Statement stmtcgstaccount = connect.createStatement();
                        ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("Cgstaccount") + "' ");
                        while (rscgstaccount.next()) {
                            cgst1.add(rscgstaccount.getString(6));
                            cgstcurrbal.add(rscgstaccount.getString(10));
                            cgstcurrbaltype.add(rscgstaccount.getString(11));

                            cgstamnt = rs.getDouble("cgstamnt");

                            DecimalFormat idf = new DecimalFormat("0.00");
                            cgstamnt1_1 = idf.format(cgstamnt);

                            String cgstcurrbalance = (String) cgstcurrbal.get(0);
                            String Curbaltypecgst = (String) cgstcurrbaltype.get(0);
                            double cgstcurrbalance1 = Double.parseDouble(cgstcurrbalance);
                            double cgstamnt1 = cgstamnt;
                            double result;

                            if (Curbaltypecgst.equals("CR")) {
                                result = cgstcurrbalance1 - cgstamnt1;
                                if (result >= 0) {
                                    updatecgstcurrbaltype.add(0, "CR");
                                    updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatecgstcurrbaltype.add(0, "DR");
                                    updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypecgst.equals("DR")) {

                                result = cgstcurrbalance1 + cgstamnt1;

                                updatecgstcurrbaltype.add(0, "DR");
                                updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Double.parseDouble(cgst1.get(0) + "") != 0) {

                                DecimalFormat upcgst = new DecimalFormat("0.00");
                                String updacgst = upcgst.format(Double.parseDouble(updatecgstcurrbal.get(0).toString()));
                                stmt_cgst.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgercgst1.get(0) + "" + "'");
                            }

                            cgst1.clear();
                            cgstcurrbal.clear();
                            cgstcurrbaltype.clear();
                            updatecgstcurrbal.clear();
                            updatecgstcurrbaltype.clear();
                        }
                    }
                }
                Statement Salesaccount = connect.createStatement();

                ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + Party_ledger + "' ");
                while (rssales.next()) {
                    Salesaccountcurrbal.add(rssales.getString(10));
                    Salesaccountcurrbaltype.add(rssales.getString(11));
                }

                double totalamntreducingvat = reducingtax;

                String salesaccountamnt = (String) Salesaccountcurrbal.get(0);
                double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
                double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);

                if (Salesaccountcurrbaltype.get(0).equals("CR")) {
                    if (salesaccountupdatedamount > 0) {
                        Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                        Salesaccountupdatecurrbaltype.add("CR");
                    }
                    if (salesaccountupdatedamount <= 0) {
                        Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                        Salesaccountupdatecurrbaltype.add("DR");
                    }
                } else if (Salesaccountcurrbaltype.get(0).equals("DR")) {
                    salesaccountupdatedamount = salesaccountamnt1 + totalamntreducingvat;
                    Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                    Salesaccountupdatecurrbaltype.add("DR");
                }

                Statement stmt2 = connect.createStatement();
                DecimalFormat upsales = new DecimalFormat("0.00");
                String updasales = upsales.format(Double.parseDouble(Salesaccountupdatecurrbal.get(0).toString()));
                stmt2.executeUpdate("UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + Party_ledger + "'");

                Salesaccountcurrbal.clear();
                Salesaccountcurrbaltype.clear();
                Salesaccountupdatecurrbal.clear();
                Salesaccountupdatecurrbaltype.clear();
//                }

                if (cashamount != 0) {

                    String c2 = "CASH";
                    Statement Cashaccount = connect.createStatement();
                    ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='" + cashaccount + "'");
                    while (rscashaccount.next()) {
                        old_cashaccountcurrbal.add(rscashaccount.getString(10));
                        old_cashaccountcurrbaltype.add(rscashaccount.getString(11));
                    }

                    jTextField4.setText(null);
                    if (jRadioButton1.isSelected()) {
                        paytype = jRadioButton1.getLabel();
                    } else if (jRadioButton2.isSelected()) {
                        paytype = jRadioButton2.getLabel();
                    } else if (jRadioButton5.isSelected()) {
                        paytype = jRadioButton5.getLabel();
                    }

                    String cashaccountamount = (String) old_cashaccountcurrbal.get(0);
                    double totalamnt = cashamount;
                    double cashaccountamount1 = Double.parseDouble(cashaccountamount);

                    if (old_cashaccountcurrbaltype.get(0).equals("DR")) {
                        double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                        if (cashaccountupdateamount > 0) {
                            old_cashaccountupdatecurrbaltype.add("DR");
                            old_cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                        if (cashaccountupdateamount <= 0) {
                            old_cashaccountupdatecurrbaltype.add("CR");
                            old_cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                    }
                    if (old_cashaccountcurrbaltype.get(0).equals("CR")) {
                        double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                        old_cashaccountupdatecurrbaltype.add("CR");
                        old_cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                    Statement stmt1 = connect.createStatement();
                    DecimalFormat dfcash = new DecimalFormat("0.00");
                    stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfcash.format(old_cashaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + old_cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + c2 + "'");

//                    double disccurrbal = 0;
//                    String disccurrbaltype = "";
//                    try {
//
////                      Statement statement = connect.createStatement();
//                        Statement statement1 = connect.createStatement();
////                      Statement statement2 = connect.createStatement();
//                        Statement statement3 = connect.createStatement();
//
//                        ResultSet rs1 = statement1.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='DISCOUNT ACCOUNT'");
//
//                        while (rs1.next()) {
//                            disccurrbal = Double.parseDouble(rs1.getString(1));
//                            disccurrbaltype = rs1.getString(2);
//                        }
//
//                        if (disccurrbaltype.equals("DR")) {
//                            disccurrbal = disccurrbal - discounamntt;
//                            if (disccurrbal < 0) {
//                                disccurrbaltype = "CR";
//                                disccurrbal = Math.abs(disccurrbal);
//                            }
//                        } else if (disccurrbaltype.equals("CR")) {
//                            disccurrbal = disccurrbal + discounamntt;
//                        }
//
//                        disccurrbal = Math.abs(disccurrbal);
//                        statement3.executeUpdate("update ledger set curr_bal='" + disccurrbal + "',currbal_type='" + disccurrbaltype + "' where ledger_name = 'DISCOUNT ACCOUNT'");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }

                if (debtoramount != 0) {
                    Statement sundrydebtorsaccount = connect.createStatement();
                    ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + debtoraccount + "'");
                    while (rssundrydebtors.next()) {
                        old_sundrydebtorsaccountcurrbal.add(rssundrydebtors.getString("curr_bal"));
                        old_sundrydebtorsaccountcurrbaltype.add(rssundrydebtors.getString("currbal_type"));
                    }

                    String sundrydebtorsamnt = (String) old_sundrydebtorsaccountcurrbal.get(0);
                    double totalamnt = debtoramount;
                    double sundrydebtorsamnt1 = Double.parseDouble(sundrydebtorsamnt);
                    if (old_sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                        double sundrydebtorsccountupdateamount = sundrydebtorsamnt1 - totalamnt;

                        if (sundrydebtorsccountupdateamount > 0) {
                            old_sundrydebtorsaccountupdatecurrbaltype.add(0, "DR");
                            old_sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                        }
                        if (sundrydebtorsccountupdateamount <= 0) {
                            old_sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                            old_sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                        }
                    }
                    if (old_sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                        double cashaccountupdateamount = sundrydebtorsamnt1 + totalamnt;
                        old_sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                        old_sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(cashaccountupdateamount));
                    }

                    DecimalFormat dfsundry = new DecimalFormat("0.00");
                    sundrydebtorsaccount.executeUpdate("UPDATE ledger SET curr_bal= '" + (dfsundry.format(old_sundrydebtorsaccountupdatecurrbal.get(0)) + "") + "', currbal_type = '" + (old_sundrydebtorsaccountupdatecurrbaltype.get(0) + "") + "' where ledger_name='" + debtoraccount + "'");

                }

                if (cardamount != 0) {
                    Statement cardaccount = connect.createStatement();
                    ResultSet rscard = cardaccount.executeQuery("select * from ledger where  ledger_name='" + cardupdateaccount + "'");
                    while (rscard.next()) {
                        old_cardaccountcurrbal.add(rscard.getString(10));
                        old_cardaccountcurrbaltype.add(rscard.getString(11));
                    }

                    String cardamnt = (String) old_cardaccountcurrbal.get(0);
                    double totalamnt = cardamount;
//                    double totalamntreducingvat = totalamnt - vatamnt;
                    double cardamnt1 = Double.parseDouble(cardamnt);
                    if (old_cardaccountcurrbaltype.get(0).equals("DR")) {
                        double cardaccountupdateamount = cardamnt1 - totalamnt;

                        if (cardaccountupdateamount > 0) {
                            old_cardaccountupdatecurrbaltype.add(0, "DR");
                            old_cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                        }
                        if (cardaccountupdateamount <= 0) {
                            old_cardaccountupdatecurrbaltype.add(0, "CR");
                            old_cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                        }
                    }
                    if (old_cardaccountcurrbaltype.get(0).equals("CR")) {
                        double cardaccountupdateamount = cardamnt1 + totalamnt;
                        old_cardaccountupdatecurrbaltype.add(0, "CR");
                        old_cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                    }

//                    Statement stmt2 = connect.createStatement();
                    DecimalFormat dfsundry = new DecimalFormat("0.00");
                    cardaccount.executeUpdate("UPDATE ledger SET curr_bal='" + dfsundry.format(old_cardaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + old_cardaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + cardupdateaccount + "'");
                }

// Calculation for Round off
                String Round_currbal_type1 = "";
                double diff_total1 = 0;
                String diff_total_round1 = "";
                Statement stru = connect.createStatement();
                Statement round_st = connect.createStatement();
                ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
                while (round_rs.next()) {
                    double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                    Round_currbal_type1 = round_rs.getString("currbal_type");
                    double round_total = round1;

                    if (Round_currbal_type1.equals("DR") && round_total < 0) {
                        diff_total1 = Round_curr_bal - round_total;
                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    } else if (Round_currbal_type1.equals("DR") && round_total > 0) {
                        diff_total1 = Round_curr_bal + round_total;
                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    }

                    if (Round_currbal_type1.equals("CR") && round_total < 0) {
                        diff_total1 = Round_curr_bal + round_total;

                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }

                    } else if (Round_currbal_type1.equals("CR") && round_total > 0) {
                        diff_total1 = Round_curr_bal - round_total;

                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    }
                    DecimalFormat dr1 = new DecimalFormat("0.00");
                    diff_total_round1 = dr1.format(diff_total1);
                }

                //Updating round off ledger in ledger table   
                stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round1 + "', currbal_type = '" + Round_currbal_type1 + "' where ledger_name = 'Round Off'");

                Statement stmt3 = connect.createStatement();
                Statement stmt4 = connect.createStatement();
                Statement stmt5 = connect.createStatement();
                String insertbillingupdate = "\"insert into billing1 select * from billing where bill_refrence='" + jTextField1.getText() + "'\"";
                stmt4.executeUpdate("insert into billing1 select * from billing where bill_refrence='" + jTextField1.getText() + "'");
                stmt5.executeUpdate("insert into queries values(" + insertbillingupdate + ")");

                stmt3.executeUpdate("delete from billing where bill_refrence='" + jTextField1.getText() + "'");

                Salesaccountcurrbal.clear();
                Salesaccountupdatecurrbal.clear();
                Salesaccountcurrbaltype.clear();
                Salesaccountupdatecurrbaltype.clear();
                salesaccount.clear();
                old_cashaccountcurrbal.clear();
                old_cashaccountupdatecurrbal.clear();
                old_cashaccountcurrbaltype.clear();
                old_cashaccountupdatecurrbaltype.clear();
                old_sundrydebtorsaccountcurrbal.clear();
                old_sundrydebtorsaccountcurrbaltype.clear();
                old_sundrydebtorsaccountupdatecurrbal.clear();
                old_sundrydebtorsaccountupdatecurrbaltype.clear();
                old_cardaccountcurrbal.clear();
                old_cardaccountupdatecurrbal.clear();
                old_cardaccountcurrbaltype.clear();
                old_cardaccountupdatecurrbaltype.clear();

                int rowcount = jTable1.getRowCount();

                if (rowcount != 0) {
                    if (issave == false) {
                        jButton5.requestFocus();
                        save();
                    }
                } else {
                    String date = formatter.format(jDateChooser1.getDate());
                    Statement stmtinsert = connect.createStatement();
                    stmtinsert.executeUpdate("insert into billing1(bill_refrence,date,type,customer_name,customer_account,sno,stock_no,rate,qnty,value,total,total_amnt,total_disc_amnt,total_amnt_after_disc,total_value_before_tax,total_value_after_tax,vatamnt,vataccount,to_ledger,by_ledger,branchid) values ('" + jTextField1.getText() + "','" + date + "','" + paytype + "','cancelled bill','','0','','0','0','0','0','0','0','0','0','0','0','VAT OUTPUT 0%','CASH','SALES','0') ");
                    JOptionPane.showMessageDialog(rootPane, "Entry deleted succesfully");
                }
                jDialog4.dispose();
                dispose();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }       
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jComboBox3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox3KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }
    }//GEN-LAST:event_jComboBox3KeyPressed
    public void party_update() {
        int rowcount = jTable1.getRowCount();
        jTextField30.setText("0.00");
        jTextField31.setText("0.00");
        jTextField32.setText("0.00");
        trv = 0;
        tiv = 0;
        tsv = 0;
        tcv = 0;
        iv = 0;
        sv = 0;
        cv = 0;
        amntbeforetax = 0;
        DecimalFormat df1 = new DecimalFormat("0.00");
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement sttt = connect.createStatement();
            Statement stttt = connect.createStatement();
            Statement pro_st = connect.createStatement();
            Statement sgst_st = connect.createStatement();
            Statement cgst_st = connect.createStatement();
            for (int i = 0; i < rowcount; i++) {
                if (isIgstApplicable == true) {
                    ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(i, 2) + "'");
                    while (rsss.next()) {

                        ResultSet rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                        while (rssss.next()) {
                            double vat = Double.parseDouble(rssss.getString("percent"));
                            double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");

                            if (jRadioButton3.isSelected()) {
                                amntbeforetax = (value1 / (100 + vat)) * 100;
                            } else if (jRadioButton4.isSelected()) {
                                amntbeforetax = (value1);
                            }
                            iv = (amntbeforetax * vat) / 100;
                        }

                    }
                    tiv = (tiv + iv);
                    jTextField30.setText("" + df1.format(tiv));

                } else if (isIgstApplicable == false) {
                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(i, 2) + "'");
                    while (pro_rs.next()) {
                        ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                        while (sgst_rs.next()) {
                            sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                        }

                        ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                        while (cgst_rs.next()) {
                            cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                        }
                    }

                    totalpercent = sgstpercent + cgstpercent;
                    double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");

                    if (jRadioButton3.isSelected()) {
                        amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                    } else if (jRadioButton4.isSelected()) {
                        amntbeforetax = (value1);
                    }
                    sv = (amntbeforetax * sgstpercent) / 100;
                    cv = ((amntbeforetax * cgstpercent) / 100);
                }

                tsv = (tsv + sv);
                tcv = (tcv + cv);

                jTextField31.setText("" + df1.format(tsv));
                jTextField32.setText("" + df1.format(tcv));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText()))));

        jTextField33.setText("" + df1.format(trv));

    }

    private void jComboBox3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox3ItemStateChanged
        try {
            String sr_amount = "";
            connection c = new connection();
            Connection connect = c.cone();
            Statement sr_st = connect.createStatement();
            ResultSet sr_rs = sr_st.executeQuery("Select * from salesreturn where bill_refrence = '" + (String) jComboBox3.getSelectedItem() + "'");
            while (sr_rs.next()) {
                sr_amount = sr_rs.getString("total_amnt");
            }
            jLabel51.setText(sr_amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jComboBox3ItemStateChanged

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    private void jComboBox4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox4KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField7.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jComboBox4KeyPressed

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        if (jRadioButton3.isSelected()) {
            tax_type = "Inclusive";
        }
    }//GEN-LAST:event_jRadioButton3ActionPerformed

    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        if (jRadioButton4.isSelected()) {
            tax_type = "Exclusive";
        }
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void jLabel53MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel53MouseClicked
        jDialog5.setVisible(true);
    }//GEN-LAST:event_jLabel53MouseClicked
    boolean married;
    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jRadioButton6.requestFocus();
        }
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jRadioButton6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jRadioButton6KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jRadioButton7.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField6.requestFocus();
        }
    }//GEN-LAST:event_jRadioButton6KeyPressed

    private void jRadioButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton6ActionPerformed
        if (jRadioButton6.isSelected()) {
            married = true;
            jTextField40.setEnabled(true);
            jDateChooser2.setEnabled(true);
        }
    }//GEN-LAST:event_jRadioButton6ActionPerformed

    private void jRadioButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton7ActionPerformed
        if (jRadioButton7.isSelected()) {
            married = false;
            jTextField40.setEnabled(false);
            jDateChooser2.setEnabled(false);
        }
    }//GEN-LAST:event_jRadioButton7ActionPerformed

    private void jRadioButton7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jRadioButton7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jRadioButton6.isSelected()) {
                jTextField40.requestFocus();
            } else {
                jTextField41.requestFocus();
            }
        } else if (key == evt.VK_ESCAPE) {
            jRadioButton6.requestFocus();
        }
    }//GEN-LAST:event_jRadioButton7KeyPressed

    private void jTextField41KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField41KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField41.getText().isEmpty()) {
                String mobile = jTextField41.getText();
                if (!(mobile.matches("^[0-9 ]*+$") && (mobile.length() >= 10) && (mobile.length() < 15))) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Valide Mobile Number");
                    jTextField41.requestFocus();
                } else {
                    checkNumber();
                }
            } else if (jTextField41.getText().isEmpty()) {
                jTextField42.requestFocus();
            }
        } else if (key == evt.VK_ESCAPE) {
            if (jRadioButton6.isSelected()) {
                jDateChooser2.requestFocus();
            } else {
                jRadioButton7.requestFocus();
            }
        }
    }//GEN-LAST:event_jTextField41KeyPressed

    private void jTextField40KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField40KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDateChooser2.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jRadioButton6.requestFocus();
        }
    }//GEN-LAST:event_jTextField40KeyPressed

    private void jTextField42KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField42KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField42.getText().isEmpty()) {
                String mobile = jTextField42.getText();
                if (!(mobile.matches("^[0-9 ]*+$") && (mobile.length() >= 10) && (mobile.length() < 12))) {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Valide Mobile Number");
                    jTextField42.requestFocus();
                } else {
                    checkNumber();
                }
            } else if (jTextField42.getText().isEmpty()) {
                jTextField43.requestFocus();
            }
        } else if (key == evt.VK_ESCAPE) {
            jTextField41.requestFocus();
        }
    }//GEN-LAST:event_jTextField42KeyPressed

    private void jTextField43KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField43KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDateChooser3.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField42.requestFocus();
        }
    }//GEN-LAST:event_jTextField43KeyPressed

    private void jTextField44KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField44KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField45.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jDateChooser3.requestFocus();
        }
    }//GEN-LAST:event_jTextField44KeyPressed

    private void jTextField45KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField45KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField46.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField44.requestFocus();
        }
    }//GEN-LAST:event_jTextField45KeyPressed

    private void jTextField46KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField46KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox6.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField45.requestFocus();
        }
    }//GEN-LAST:event_jTextField46KeyPressed

    private void jComboBox6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox6KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton13.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField46.requestFocus();
        }
    }//GEN-LAST:event_jComboBox6KeyPressed

    private void jButton13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton13KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField41.getText().isEmpty()) {
                JOptionPane.showMessageDialog(rootPane, "Please Enter Mobile Number");
                jTextField41.requestFocus();
            } else if (!jTextField41.getText().isEmpty()) {
                saveCustomerInfo();
            }
        } else if (key == evt.VK_ESCAPE) {
            jComboBox6.requestFocus();
        }
    }//GEN-LAST:event_jButton13KeyPressed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        if (jTextField41.getText().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Mobile Number");
            jTextField41.requestFocus();
        } else if (!jTextField41.getText().isEmpty()) {
            saveCustomerInfo();
        }
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jTextField47CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField47CaretUpdate
        cust_serch();
        if (jTextField47.getText().isEmpty()) {
            cust_Data();
        }
    }//GEN-LAST:event_jTextField47CaretUpdate

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        int sel_row = jTable2.getSelectedRow();
        jTextField3.setText("" + jTable2.getValueAt(sel_row, 0));
        jTextField37.setText("" + jTable2.getValueAt(sel_row, 1));
        jDialog6.dispose();
        jTextField5.requestFocus();
    }//GEN-LAST:event_jTable2MouseClicked

    private void jTextField41FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField41FocusLost
//        checkNumber();
    }//GEN-LAST:event_jTextField41FocusLost

    private void jTextField42FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField42FocusLost
//        checkNumber();
    }//GEN-LAST:event_jTextField42FocusLost
    public void saveCustomerInfo() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement cust_info = connect.createStatement();
            String Ani_date = "";
            String Dob = "";
            if (jDateChooser2.getDate() != null) {
                Ani_date = formatter.format(jDateChooser2.getDate());
            }
            if (jDateChooser3.getDate() != null) {
                Dob = formatter.format(jDateChooser3.getDate());
            }
            cust_info.executeUpdate("Insert into billing_customer values ('" + jTextField6.getText() + "'," + married + ",'" + jTextField40.getText() + "','" + Ani_date + "','" + jTextField41.getText() + "','" + jTextField42.getText() + "','" + jTextField43.getText() + "','" + Dob + "','" + jTextField44.getText() + "','" + jTextField45.getText() + "','" + jTextField46.getText() + "','" + jComboBox6.getSelectedItem() + "')");
            jTextField3.setText(jTextField6.getText());
            jTextField37.setText("" + jTextField41.getText());
            jDialog5.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------

    public void cust_serch() {
        try {
            DefaultTableModel dtm;
            dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            connection c = new connection();
            Connection connect = c.cone();
            Statement cust_st = connect.createStatement();
            ResultSet cust_rs = cust_st.executeQuery("Select * from billing_customer where Customer_Name Like '" + jTextField47.getText() + "%' OR Mobile_No Like '" + jTextField47.getText() + "%' OR Whatsapp_No Like '" + jTextField47.getText() + "%'");
            while (cust_rs.next()) {
                Object o[] = {cust_rs.getString("Customer_Name"), cust_rs.getString("Mobile_No"), cust_rs.getString("Whatsapp_No"), cust_rs.getString("City")};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------

    public void cust_Data() {
        try {
            DefaultTableModel dtm;
            dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            connection c = new connection();
            Connection connect = c.cone();
            Statement cust_st = connect.createStatement();
            ResultSet cust_rs = cust_st.executeQuery("Select * from billing_customer");
            while (cust_rs.next()) {
                Object o[] = {cust_rs.getString("Customer_Name"), cust_rs.getString("Mobile_No"), cust_rs.getString("Whatsapp_No"), cust_rs.getString("City")};
                dtm.addRow(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //---------Function For Checking Party Number exist or not------------------------

    public void checkNumber() {
        ArrayList mobile_no = new ArrayList();
        ArrayList whatsapp_no = new ArrayList();
        try {
            connection c = new connection();
            Connection conn = c.cone();
            Statement number_st = conn.createStatement();
            ResultSet number_rs = number_st.executeQuery("Select Mobile_No,Whatsapp_No from billing_customer");
            while (number_rs.next()) {
                mobile_no.add(number_rs.getString("Mobile_No"));
                whatsapp_no.add(number_rs.getString("Whatsapp_No"));

            }
            if (!jTextField41.getText().isEmpty()) {
                if (mobile_no.contains(jTextField41.getText())) {
                    JOptionPane.showMessageDialog(rootPane, "Mobile Number all ready exist");
                    jTextField41.requestFocus();
                } else {
                    jTextField42.requestFocus();
                }
            }
            if (!jTextField42.getText().isEmpty()) {
                if (whatsapp_no.contains(jTextField42.getText())) {
                    JOptionPane.showMessageDialog(rootPane, "Whatsapp Number all ready exist");
                    jTextField42.requestFocus();
                } else {
                    jTextField43.requestFocus();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------ 

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //final JButton printButton = new JButton("Print");
        // UIManager.put("swing.boldMetal", Boolean.TRUE);
        //JFrame f = new JFrame("GatePass");
//        Billing_1.addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent e) {
//                System.exit(0);
//            }
//        });
//        Font f1 = new Font("Times New Roman", Font.BOLD, 12);
        //JBut.setVisible(true);
//        JLabel label1 = new JLabel("Vision Techno Solutions", JLabel.CENTER);
//        label1.setBounds(30, 30, 200, 40);
//        JLabel label2 = new JLabel("Batch No:");
//        label2.setBounds(10, 70, 80, 20);
//        JLabel label3 = new JLabel("Date and Time:");
//        label3.setBounds(10, 100, 120, 20);
//        JLabel label4 = new JLabel("Visitor's Name:");
//        label4.setBounds(10, 130, 120, 20);
//        JLabel label5 = new JLabel("Concern Person:");
//        label5.setBounds(10, 160, 120, 20);
//        JLabel label6 = new JLabel("Purpose of Visit:");
//        label6.setBounds(10, 190, 120, 20);
//        JLabel label7 = new JLabel("Manager's Sign");
//        label7.setBounds(140, 230, 120, 20);
//        JLabel label8 = new JLabel("Visitor's Sign");
//        label8.setBounds(10, 230, 120, 20);
/////      /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new New_Dual_Billing().setVisible(true);
            }
        });
    }
    String snumbers[] = new String[5000];

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    public javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    public javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    public javax.swing.JComboBox<String> jComboBox5;
    private javax.swing.JComboBox<String> jComboBox6;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    public com.toedter.calendar.JDateChooser jDateChooser2;
    public com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JDialog jDialog1;
    public javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JDialog jDialog4;
    private javax.swing.JDialog jDialog5;
    private javax.swing.JDialog jDialog6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    public javax.swing.JLabel jLabel15;
    public javax.swing.JLabel jLabel16;
    public javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel18;
    public javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    public javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    public javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JRadioButton jRadioButton1;
    public javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    public javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JRadioButton jRadioButton6;
    private javax.swing.JRadioButton jRadioButton7;
    public javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    public javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    public javax.swing.JTextField jTextField17;
    public javax.swing.JTextField jTextField18;
    public javax.swing.JTextField jTextField19;
    public javax.swing.JTextField jTextField2;
    public javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    private javax.swing.JTextField jTextField22;
    private javax.swing.JTextField jTextField23;
    private javax.swing.JTextField jTextField24;
    private javax.swing.JTextField jTextField25;
    private javax.swing.JTextField jTextField26;
    private javax.swing.JTextField jTextField27;
    private javax.swing.JTextField jTextField28;
    private javax.swing.JTextField jTextField29;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField30;
    public javax.swing.JTextField jTextField31;
    public javax.swing.JTextField jTextField32;
    public javax.swing.JTextField jTextField33;
    public javax.swing.JTextField jTextField34;
    public javax.swing.JTextField jTextField35;
    public javax.swing.JTextField jTextField36;
    public javax.swing.JTextField jTextField37;
    public javax.swing.JTextField jTextField38;
    public javax.swing.JTextField jTextField39;
    public javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField40;
    private javax.swing.JTextField jTextField41;
    private javax.swing.JTextField jTextField42;
    private javax.swing.JTextField jTextField43;
    private javax.swing.JTextField jTextField44;
    private javax.swing.JTextField jTextField45;
    private javax.swing.JTextField jTextField46;
    private javax.swing.JTextField jTextField47;
    public javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    public javax.swing.JTextField jTextField7;
    public javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    private java.awt.List list1;
    // End of variables declaration//GEN-END:variables
}
