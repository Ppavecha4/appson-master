package transaction;

import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.List;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Golu
 */
public class New_Dual_PurchaseReturn extends javax.swing.JFrame implements Printable {

    public List list;
    public String xz[];
    String sundrydebtors = null;
    String cardpaymentaccount = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String Party_code;
    String Year;

    ArrayList party = new ArrayList();
    int no_of_copies = 1;

    public boolean isIgstApplicable = false;
    public double diff_total = 0;
    String diff_total_round;
    String Fc_currbal_type = "";
    double FC_total = 0;
    String Fre_currbal_type = "";
    double Fre_total = 0;
    double amntbeforetax = 0;
    double tiv = 0;
    double tsv = 0;
    double tcv = 0;
    double tiv1 = 0;
    double tsv1 = 0;
    double tcv1 = 0;
    double trv = 0;
    double iv = 0;
    double sv = 0;
    double cv = 0;
    double iv1 = 0;
    double sv1 = 0;
    double cv1 = 0;
    double freight_gst = 0;
    public String Round_currbal_type = null;

    public String Sgst_Account = null;
    public String Cgst_Account = null;
    public double igst_amnt = 0;
    public double sgst_total_amnt = 0;
    public double cgst_total_amnt = 0;
    String tax_type = "Exclusive";
    String Barcode;
    String Rate;

    private static New_Dual_PurchaseReturn obj = null;

    public New_Dual_PurchaseReturn() {
        this.setLocationRelativeTo(null);
        try {
            combo();
            initComponents();
            jDialog7.setLocationRelativeTo(null);
            jDialog5.setLocationRelativeTo(null);
            jDialog6.setLocationRelativeTo(null);
            jTextField5.requestFocus();
            jRadioButton4.setSelected(true);
            jRadioButton4.setEnabled(false);
            jRadioButton3.setEnabled(false);
            jLabel23.setVisible(false);
            jLabel24.setVisible(false);
            jLabel49.setVisible(false);
            jTextField39.setVisible(false);
            jLabel27.setText("" + currentDate);
            jRadioButton1.setSelected(true);
            jButton5.setVisible(false);
            jButton6.setVisible(false);
            jButton7.setVisible(false);
            if (jRadioButton1.isSelected()) {
                String c = "CASH";
                try {
                    connection c1 = new connection();
                    Connection connect = c1.cone();
                    Statement st2 = connect.createStatement();
                    Statement st3 = connect.createStatement();
                    Statement st4 = connect.createStatement();
                    ResultSet rs2 = null;
                    rs2 = st2.executeQuery("select * from ledger where ledger_name ='" + c + "' ");
                    while (rs2.next()) {
                        String cash = rs2.getString(1);
                        jLabel23.setText(rs2.getString(10));
                        jLabel24.setText(rs2.getString(11));
                    }
                    ResultSet rs3 = st3.executeQuery("select * from ledger where groups='BANK ACCOUNTS'");
                    while (rs3.next()) {
                        jComboBox2.addItem(rs3.getString(1));
                    }
                    ResultSet pro_rs = st4.executeQuery("select DISTINCT(Pro_duct) from stockid2");
                    while (pro_rs.next()) {
                        jComboBox3.addItem(pro_rs.getString("Pro_duct"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            jLabel4.setVisible(false);
            jTextField4.setVisible(false);
            jTextField3.setEnabled(true);
            Calendar currentDate = Calendar.getInstance();

            String dateNow = formatter.format(currentDate.getTime());
            jDateChooser1.setDate(formatter.parse(dateNow));
            try {
                java.util.Date dateafter = formatter.parse("20" + getyear + "-03-31");
                java.util.Date datenow = formatter.parse(dateNow);
                if (datenow.before(dateafter)) {
                    getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                    nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            String sales = "PRR/";
            String slash = "/";
            String Branchno = "1/";
            String yearwise = Year.concat(slash);
//            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String finalconcat = ((sales.concat(yearwise)));
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();

                ResultSet rs = st.executeQuery("SELECT bill_refrence FROM purchasereturndualgst where branchid = '0' ");
                int a = 0, b, d = 0;
                int i = 0;
                while (rs.next()) {
                    if (i == 0) {
                        d = rs.getString(1).lastIndexOf("/");
                        i++;
                    }
                    b = Integer.parseInt(rs.getString(1).substring(d + 1));
                    if (a < b) {
                        a = b;
                    }
                }
                String s = finalconcat.concat(Integer.toString(a + 1));
                jTextField1.setText("" + s);
            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement setting_st = connect.createStatement();
            ResultSet rs = st.executeQuery("select ledger_name from ledger where groups ='BANK ACCOUNTS'");
            while (rs.next()) {
                list1.addItem(rs.getString(1));
            }
            ResultSet setting_rs = setting_st.executeQuery("Select Barcode, Rate from setting");
            while (setting_rs.next()) {
                Barcode = setting_rs.getString("Barcode");
                Rate = setting_rs.getString("Rate");
            }
            if (Barcode.equals("Enable")) {
                jComboBox3.setEnabled(false);
            } else if (Barcode.equals("Disable")) {
                jTextField5.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static New_Dual_PurchaseReturn getObj() {
        if (obj == null) {
            obj = new New_Dual_PurchaseReturn();
        }
        return obj;
    }

    private Object makeObj(final String item) {
        return new Object() {
            @Override
            public String toString() {
                return item;
            }
        };
    }
    //      rs = st.executeQuery("SELECT party_code from Party");

    public void combo() {

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("SELECT * from Staff");
            int a1 = 0;
            while (rs1.next()) {
                a1++;
            }
            xz = new String[a1];
            rs1 = st1.executeQuery("SELECT * from Staff");
            a1 = 0;
            while (rs1.next()) {
                xz[a1] = rs1.getString(1);
                a1++;
            }
            ResultSet rs_year = st2.executeQuery("Select * from year");
            while (rs_year.next()) {
                Year = rs_year.getString("year");
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
    }
//-------------------------------Freight with Gst------------------------------------

    public void freightGst() {
        double igst_amnt = 0;
        double sgst_amnt = 0;
        double cgst_amnt = 0;
        try {
            String fr_gst = (String) jComboBox11.getSelectedItem();
            double f_gst = Double.parseDouble(fr_gst);
            double f_amnt = Double.parseDouble(jTextField45.getText());

            if (isIgstApplicable == true) {
                igst_amnt = (f_amnt * f_gst) / 100;
                if (f_gst == 5) {
                    freight_gst = f_gst;
                    tiv = tiv + igst_amnt;
                } else if (f_gst == 12) {
                    freight_gst = f_gst;
                    tiv1 = tiv1 + igst_amnt;
                }
                jTextField30.setText("" + tiv);
                jTextField40.setText("" + tiv1);
            } else if (isIgstApplicable == false) {

                sgst_amnt = (f_amnt * (f_gst) / 2) / 100;
                cgst_amnt = (f_amnt * (f_gst) / 2) / 100;

                if (f_gst == 5) {
                    freight_gst = (f_gst / 2);
                    tsv = tsv + sgst_amnt;
                    tcv = tcv + cgst_amnt;
                } else if (f_gst == 12) {
                    freight_gst = (f_gst / 2);
                    tsv1 = tsv1 + sgst_amnt;
                    tcv1 = tcv1 + cgst_amnt;
                }
                jTextField31.setText("" + tsv);
                jTextField32.setText("" + tcv);
                jTextField41.setText("" + tsv1);
                jTextField42.setText("" + tcv1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        calculationall();
    }
//----------------------------------------------------------------------------------- 
//---------------------Method for Recalculation--------------------------------------

    public void calculationall() {
        DecimalFormat df = new DecimalFormat("0.00");

        double amnt = Double.parseDouble(jTextField15.getText());
        double discamnt = Double.parseDouble(jTextField17.getText());
        double dam = amnt - discamnt;

        double igst_5 = Double.parseDouble(jTextField30.getText());
        double igst_12 = Double.parseDouble(jTextField40.getText());

        double sgst_5 = Double.parseDouble(jTextField31.getText());
        double sgst_12 = Double.parseDouble(jTextField41.getText());

        double cgst_5 = Double.parseDouble(jTextField32.getText());
        double cgst_12 = Double.parseDouble(jTextField42.getText());

        double oth = Double.parseDouble(jTextField44.getText());

        double freight = Double.parseDouble(jTextField45.getText());

        double tot = cgst_5 + cgst_12 + sgst_5 + sgst_12 + igst_5 + igst_12 + oth + dam + freight;

        jTextField18.setText("" + df.format(dam));
        jTextField20.setText("" + df.format(tot));
    }
//-----------------------------------------------------------------------------------    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jDialog1 = new javax.swing.JDialog();
        jLabel26 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jTextField24 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel33 = new javax.swing.JLabel();
        jTextField25 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jDialog2 = new javax.swing.JDialog();
        list1 = new java.awt.List();
        jDialog3 = new javax.swing.JDialog();
        jTextField26 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jTextField27 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jTextField28 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jTextField29 = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton8 = new javax.swing.JButton();
        jDialog4 = new javax.swing.JDialog();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jTextField34 = new javax.swing.JTextField();
        jTextField35 = new javax.swing.JTextField();
        jTextField36 = new javax.swing.JTextField();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jTextField39 = new javax.swing.JTextField();
        jButton12 = new javax.swing.JButton();
        jDialog5 = new javax.swing.JDialog();
        jLabel53 = new javax.swing.JLabel();
        jTextField43 = new javax.swing.JTextField();
        jButton13 = new javax.swing.JButton();
        jDialog6 = new javax.swing.JDialog();
        list2 = new java.awt.List();
        jDialog7 = new javax.swing.JDialog();
        jLabel70 = new javax.swing.JLabel();
        jComboBox11 = new javax.swing.JComboBox<String>();
        jButton14 = new javax.swing.JButton();
        buttonGroup3 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jTextField13 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jTextField16 = new javax.swing.JTextField();
        jTextField17 = new javax.swing.JTextField();
        jTextField18 = new javax.swing.JTextField();
        jTextField19 = new javax.swing.JTextField();
        jTextField20 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox(xz);
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jRadioButton5 = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jTextField2 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jTextField30 = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jTextField31 = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        jTextField32 = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jTextField33 = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jTextField37 = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        jTextField38 = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jTextField40 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jTextField41 = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        jTextField42 = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        jTextField44 = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        jTextField45 = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jComboBox3 = new javax.swing.JComboBox<String>();

        jDialog1.setMinimumSize(new java.awt.Dimension(451, 396));

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel26.setText("Cheque Entry");

        jLabel22.setText("Date: -");

        jLabel27.setText("jLabel27");

        jLabel28.setText("Name: -");

        jLabel29.setText("Cheque No. : -");

        jLabel30.setText("Payee Bank: -");

        jLabel31.setText("Amount: -");

        jLabel32.setText("To Account :-");

        jLabel33.setText("Date of Cheque: -");

        jButton4.setText("CANCEL");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton3.setText("OK");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30)
                            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jDialog1Layout.createSequentialGroup()
                                    .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel29)
                                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jDialog1Layout.createSequentialGroup()
                                            .addGap(56, 56, 56)
                                            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jTextField23)
                                                .addComponent(jTextField24)
                                                .addComponent(jComboBox2, 0, 117, Short.MAX_VALUE)
                                                .addComponent(jTextField25))))))))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel33)))
                .addContainerGap(186, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addGap(77, 77, 77)
                        .addComponent(jButton4)
                        .addGap(154, 154, 154))))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jLabel27))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(jTextField25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32))
                .addGap(27, 27, 27)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4)
                    .addComponent(jButton3))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(200, 400));

        list1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );

        jDialog3.setMinimumSize(new java.awt.Dimension(682, 580));

        jTextField26.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField26CaretUpdate(evt);
            }
        });
        jTextField26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField26ActionPerformed(evt);
            }
        });
        jTextField26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField26KeyPressed(evt);
            }
        });

        jLabel34.setText("Enter Party code :");

        jTextField27.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField27CaretUpdate(evt);
            }
        });
        jTextField27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField27ActionPerformed(evt);
            }
        });
        jTextField27.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField27KeyPressed(evt);
            }
        });

        jLabel35.setText("Enter Party Name :");

        jTextField28.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField28CaretUpdate(evt);
            }
        });
        jTextField28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField28KeyPressed(evt);
            }
        });

        jLabel36.setText("Enter city :");

        jLabel37.setText("Enter State :");

        jTextField29.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField29CaretUpdate(evt);
            }
        });
        jTextField29.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField29KeyPressed(evt);
            }
        });

        jScrollPane5.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "party_code", "party_name", "city", "state", "opening_bal", "Balance Type", "IGST Applicable"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTable3);

        jScrollPane5.setViewportView(jScrollPane4);

        jButton8.setText("ADD NEW PARTY");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 711, Short.MAX_VALUE)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addComponent(jLabel37)
                        .addGap(44, 44, 44)
                        .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jDialog3Layout.createSequentialGroup()
                            .addComponent(jLabel36)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog3Layout.createSequentialGroup()
                                .addComponent(jLabel35)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog3Layout.createSequentialGroup()
                                .addComponent(jLabel34)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34))
                .addGap(23, 23, 23)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addGap(22, 22, 22)
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addGap(34, 34, 34)
                .addComponent(jButton8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jDialog4.setMinimumSize(new java.awt.Dimension(500, 300));

        jLabel42.setText("Amount by Cash");

        jLabel43.setText("Amount by Debit");

        jLabel44.setText("Amount by Card");

        jTextField34.setText("0");
        jTextField34.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField34KeyPressed(evt);
            }
        });

        jTextField35.setText("0");
        jTextField35.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField35KeyPressed(evt);
            }
        });

        jTextField36.setText("0");
        jTextField36.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField36KeyPressed(evt);
            }
        });

        jButton9.setText("SAVE");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jButton9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton9KeyPressed(evt);
            }
        });

        jButton10.setText("SELECT DEBTORS");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setText("SELECT BANK");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jLabel47.setText("TOTAL AMNT : ");

        jLabel49.setText("Invoice No. : -");

        jButton12.setText("UPDATE");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog4Layout = new javax.swing.GroupLayout(jDialog4.getContentPane());
        jDialog4.getContentPane().setLayout(jDialog4Layout);
        jDialog4Layout.setHorizontalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog4Layout.createSequentialGroup()
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField34)
                            .addComponent(jTextField35, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
                    .addGroup(jDialog4Layout.createSequentialGroup()
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton12)
                            .addComponent(jTextField36, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                            .addComponent(jTextField39))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addComponent(jButton11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                        .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel48)
                        .addGap(80, 80, 80))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                        .addComponent(jButton9)
                        .addGap(163, 163, 163))))
        );
        jDialog4Layout.setVerticalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog4Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(jLabel48))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42))
                .addGap(18, 18, 18)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel43)
                    .addComponent(jTextField35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton10))
                .addGap(23, 23, 23)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44)
                    .addComponent(jTextField36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(jTextField39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton9)
                    .addComponent(jButton12))
                .addContainerGap())
        );

        jDialog5.setMinimumSize(new java.awt.Dimension(250, 120));

        jLabel53.setText("No of Copies - ");

        jTextField43.setText("1");
        jTextField43.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField43KeyPressed(evt);
            }
        });

        jButton13.setText("Print");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });
        jButton13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton13KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jDialog5Layout = new javax.swing.GroupLayout(jDialog5.getContentPane());
        jDialog5.getContentPane().setLayout(jDialog5Layout);
        jDialog5Layout.setHorizontalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog5Layout.createSequentialGroup()
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jLabel53)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField43, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog5Layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addComponent(jButton13)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jDialog5Layout.setVerticalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog5Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel53)
                    .addComponent(jTextField43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton13)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        jDialog6.setTitle("Freight Account");

        list2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog6Layout = new javax.swing.GroupLayout(jDialog6.getContentPane());
        jDialog6.getContentPane().setLayout(jDialog6Layout);
        jDialog6Layout.setHorizontalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list2, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
        );
        jDialog6Layout.setVerticalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list2, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
        );

        jDialog7.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jDialog7.setTitle("Freight Gst Percentage ");

        jLabel70.setText("Select GST Percentage - ");

        jComboBox11.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "5", "12", "18", "28" }));
        jComboBox11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox11KeyPressed(evt);
            }
        });

        jButton14.setText("Submit");
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });
        jButton14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton14KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jDialog7Layout = new javax.swing.GroupLayout(jDialog7.getContentPane());
        jDialog7.getContentPane().setLayout(jDialog7Layout);
        jDialog7Layout.setHorizontalGroup(
            jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog7Layout.createSequentialGroup()
                .addGroup(jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel70)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox11, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog7Layout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addComponent(jButton14)))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        jDialog7Layout.setVerticalGroup(
            jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog7Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel70)
                    .addComponent(jComboBox11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(jButton14)
                .addGap(31, 31, 31))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Purchase Return");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setText("Bill Reference :");

        jLabel2.setText("Date :");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Cash");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Debit");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jLabel3.setText("Customer Name :");

        jLabel4.setText("Customer A/C No. :");

        jTextField1.setEditable(false);
        jTextField1.setText("1");
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTextField3.setEnabled(false);
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jTextField4.setEnabled(false);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel5.setText("Stock No.");

        jLabel6.setText("Product");

        jLabel7.setText("Rate");

        jLabel8.setText("Quantity");

        jLabel9.setText("Value");

        jLabel10.setText("Description");

        jLabel11.setText("Disc %");

        jLabel12.setText("Disc Amt");

        jLabel13.setText("Total");

        jLabel14.setText("Staff: -");

        jTextField5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField5FocusLost(evt);
            }
        });
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });
        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField7KeyReleased(evt);
            }
        });

        jTextField8.setText("1");
        jTextField8.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField8FocusLost(evt);
            }
        });
        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jTextField9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField9ActionPerformed(evt);
            }
        });
        jTextField9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField9KeyPressed(evt);
            }
        });

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });
        jTextField10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField10KeyPressed(evt);
            }
        });

        jTextField11.setEditable(false);
        jTextField11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField11KeyPressed(evt);
            }
        });

        jTextField12.setEditable(false);
        jTextField12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField12FocusLost(evt);
            }
        });
        jTextField12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField12ActionPerformed(evt);
            }
        });
        jTextField12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField12KeyPressed(evt);
            }
        });

        jTextField13.setEditable(false);
        jTextField13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField13FocusGained(evt);
            }
        });
        jTextField13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField13KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SNo.", "Stock No.", "Product", "Rate", "Quantity", "Value", "Description ", "Disc %", "Disc Amt", "Total", "Staff"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setResizable(false);
            jTable1.getColumnModel().getColumn(8).setResizable(false);
            jTable1.getColumnModel().getColumn(9).setResizable(false);
            jTable1.getColumnModel().getColumn(10).setResizable(false);
        }

        jLabel15.setText("Total Amount");

        jLabel16.setText("Quantity");

        jLabel17.setText("Disc Amount");

        jLabel18.setText("Amount After Dis");

        jLabel19.setText("Value Before Tax");

        jLabel20.setText("Net Amount");

        jTextField15.setEditable(false);
        jTextField15.setText("0");
        jTextField15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField15ActionPerformed(evt);
            }
        });
        jTextField15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField15KeyPressed(evt);
            }
        });

        jTextField16.setEditable(false);
        jTextField16.setText("0");
        jTextField16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField16KeyPressed(evt);
            }
        });

        jTextField17.setText("0");
        jTextField17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField17FocusLost(evt);
            }
        });
        jTextField17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField17ActionPerformed(evt);
            }
        });
        jTextField17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField17KeyPressed(evt);
            }
        });

        jTextField18.setEditable(false);
        jTextField18.setText("0");
        jTextField18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField18ActionPerformed(evt);
            }
        });
        jTextField18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField18KeyPressed(evt);
            }
        });

        jTextField19.setEditable(false);
        jTextField19.setText("0");
        jTextField19.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField19FocusLost(evt);
            }
        });
        jTextField19.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField19KeyPressed(evt);
            }
        });

        jTextField20.setEditable(false);
        jTextField20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField20FocusLost(evt);
            }
        });
        jTextField20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField20ActionPerformed(evt);
            }
        });
        jTextField20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField20KeyPressed(evt);
            }
        });

        jComboBox1.setSelectedItem(null);
        jComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBox1FocusGained(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel23.setText("jLabel23");

        jLabel24.setText("jLabel24");

        jTextField14.setEditable(false);
        jTextField14.setText("1");
        jTextField14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField14ActionPerformed(evt);
            }
        });
        jTextField14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField14KeyPressed(evt);
            }
        });

        jLabel25.setText("S No.");

        buttonGroup1.add(jRadioButton5);
        jRadioButton5.setText("Card Payment");
        jRadioButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton5ActionPerformed(evt);
            }
        });

        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        jButton5.setText("REPRINT");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("BILL NEW");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("UPDATE");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jTextField2.setEnabled(false);
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });

        jLabel21.setText("Disc %");

        jLabel38.setText("IGST 5%");

        jTextField30.setEditable(false);
        jTextField30.setText("0");

        jLabel39.setText("SGST 2.5%");

        jTextField31.setEditable(false);
        jTextField31.setText("0");

        jLabel40.setText("CGST 2.5%");

        jTextField32.setEditable(false);
        jTextField32.setText("0");

        jLabel41.setText("Round Off");

        jTextField33.setText("0");
        jTextField33.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField33FocusLost(evt);
            }
        });
        jTextField33.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField33KeyPressed(evt);
            }
        });

        jLabel45.setText("Mobile No.: -");

        jLabel46.setText("REF No.:");

        jLabel50.setText("IGST 12%");

        jTextField40.setText("0");

        jLabel51.setText("SGST 6%");

        jTextField41.setText("0");

        jLabel52.setText("CGST 6%");

        jTextField42.setText("0");

        jLabel54.setText("Other Charges");

        jTextField44.setText("0");
        jTextField44.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField44KeyPressed(evt);
            }
        });

        jLabel55.setText("Fre. With Gst");

        jTextField45.setText("0");
        jTextField45.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField45KeyPressed(evt);
            }
        });

        jLabel56.setText("Tax Type -");

        buttonGroup3.add(jRadioButton3);
        jRadioButton3.setText("Inclusive");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });

        buttonGroup3.add(jRadioButton4);
        jRadioButton4.setText("Exclusive");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        jComboBox3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox3KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel38)
                            .addComponent(jTextField30, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel21))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel17)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(jLabel51)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel18)
                                    .addComponent(jLabel40)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField40, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField31, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(22, 22, 22)
                                        .addComponent(jLabel50)
                                        .addGap(51, 51, 51)
                                        .addComponent(jLabel39)))
                                .addGap(18, 18, 18)
                                .addComponent(jTextField41, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField32, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addComponent(jLabel52))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jTextField42, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel25)
                                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11)
                                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField33, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel19)
                                        .addGap(34, 34, 34)
                                        .addComponent(jLabel41)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton6))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel54)
                                            .addComponent(jTextField44, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel20))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel55)
                                            .addComponent(jTextField45, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 133, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jButton7)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButton1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jButton5, javax.swing.GroupLayout.Alignment.TRAILING)))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel4)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jRadioButton1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jRadioButton2))
                                            .addComponent(jLabel3))
                                        .addGap(6, 6, 6)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jRadioButton5)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel23)
                                                .addGap(35, 35, 35)
                                                .addComponent(jLabel24))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextField37, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(59, 59, 59)
                                                .addComponent(jLabel56)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jRadioButton3)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jRadioButton4))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel46)
                                    .addComponent(jLabel2))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                                    .addComponent(jTextField38)
                                    .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2)
                    .addComponent(jLabel23)
                    .addComponent(jRadioButton5)
                    .addComponent(jLabel24)
                    .addComponent(jLabel46)
                    .addComponent(jTextField38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel56)
                    .addComponent(jRadioButton3)
                    .addComponent(jRadioButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel45)
                    .addComponent(jTextField37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jLabel21))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel39)
                            .addComponent(jLabel40)
                            .addComponent(jLabel38)
                            .addComponent(jLabel50)
                            .addComponent(jLabel51)
                            .addComponent(jLabel52))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(jLabel41)
                            .addComponent(jLabel20)
                            .addComponent(jLabel54)
                            .addComponent(jLabel55))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(jButton1)
                            .addComponent(jButton7))
                        .addGap(9, 9, 9))))
        );

        setSize(new java.awt.Dimension(1018, 566));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField9ActionPerformed

    private void jTextField15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField15ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField15ActionPerformed

    private void jTextField20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField20ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField20ActionPerformed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField4.isEnabled()) {
                jTextField4.requestFocus();
            } else {
                jTextField5.requestFocus();
            }
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField4KeyPressed
    static String disc_type = null;
    double sgstpercent = 0;
    double cgstpercent = 0;
    double totalpercent = 0;
    private void jTextField5FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField5FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5FocusLost
    String barcode1 = null;
    public boolean islist = false;
    String barcode = null;

    public void partyselect() {
        DefaultTableModel dtm = null;
        dtm = (DefaultTableModel) jTable3.getModel();
        row = jTable3.getSelectedRow();
        int coloum = jTable3.getSelectedColumn();
        sundrydebtors = (String) jTable3.getValueAt(row, 1);
        jDialog2.setVisible(false);
        jTextField4.setText(jTable3.getValueAt(row, 0) + "");
        jTextField3.setText(jTable3.getValueAt(row, 1) + "");
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + sundrydebtors + "'");
            while (rs.next()) {
                sundrydebtorsaccountcurrbal.add(rs.getString(10));
                sundrydebtorsaccountcurrbaltype.add(rs.getString(11));
            }

            if (jTable3.getValueAt(row, 6).equals("True")) {
                isIgstApplicable = true;
            } else if (jTable3.getValueAt(row, 6).equals("False")) {
                isIgstApplicable = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jDialog3.dispose();

        jTextField5.requestFocus();
    }
    public boolean isstockfinish = false;

    int ismeter = 0;
    int iscalculationapplicable = 0;
    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            if (jTextField5.getText().isEmpty()) {
                jComboBox3.requestFocus();
            } else {
                isstockfinish = false;
                islist = false;
                jTextField13.requestFocus();
                barcode = jTextField5.getText().trim().toUpperCase();

                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st1 = connect.createStatement();
                    ResultSet rs = st1.executeQuery("select * from Stockid2 where Stock_No='" + barcode + "'");
                    while (rs.next()) {
                        barcode1 = rs.getString(1);
                        System.out.println("barcode1" + barcode1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                int rowcount = jTable1.getRowCount();
                int count = 0;
                int count1 = 0;

                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st1 = connect.createStatement();
                    int meter = 0;
                    ResultSet rs = st1.executeQuery("select count(stock_no),ismeter from stockid2 where stock_no='" + barcode1 + "'");

                    while (rs.next()) {
                        count = rs.getInt(1);
                        meter = rs.getInt(2);
                    }
                    if (meter == 1) {
                        isstockfinish = false;
                    } else if (meter == 0) {
                        for (int i = 0; i < rowcount; i++) {
                            if (jTable1.getValueAt(i, 1).toString().equals(barcode1)) {
                                count1++;
                            }
                        }
                        if (count1 >= count) {
                            isstockfinish = true;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (jTextField5.getText().isEmpty()) {
                    jButton3.requestFocus();

                } else if (barcode1 == null || isstockfinish == true) {
                    JOptionPane.showMessageDialog(rootPane, "Item is not presented in stock or Already in the List");
                    jTextField5.setText(null);
                    jTextField5.requestFocus();

                } else if (barcode1 != null) {
                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement st = connect.createStatement();

                        Statement stt2 = connect.createStatement();
                        ResultSet rss = stt2.executeQuery("SELECT  * FROM Stockid2 where Stock_No='" + barcode + "'");

                        System.out.println("" + barcode);
                        while (rss.next()) {
                            jTextField7.setText("" + rss.getString("ra_te"));
                            jComboBox3.setSelectedItem("" + rss.getString(7));
                            //    party.add(rss.getString(10));  
                            ismeter = rss.getInt("ismeter");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (ismeter == 0 && Rate.equals("Disable")) {
                        if (!jTextField7.getText().isEmpty()) {
                            String ra_te = jTextField7.getText();
                            String qun_tity = null;
                            if (jTextField18.getText() == null) {
                                qun_tity = "0";
                            } else {
                                qun_tity = jTextField8.getText();
                            }

                            double p = Double.parseDouble(ra_te);
                            double q = Double.parseDouble(qun_tity);
                            double amou_nt = Math.round(p * q);
                            jTextField9.setText("" + String.valueOf(amou_nt));
                        }

                        // focusgained
                        double disc_amt = 0;
                        double value = 0;
                        if (!jTextField12.getText().isEmpty()) {
                            disc_amt = Double.parseDouble(jTextField12.getText());
                        } else {
                            disc_amt = 0;
                        }
                        if (!jTextField9.getText().isEmpty()) {
                            value = Double.parseDouble(jTextField9.getText());
                        } else {
                            value = 0;
                        }

                        double total = value - disc_amt;
                        jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                        iv1 = 0;
                        iv = 0;
                        cv = 0;
                        cv1 = 0;
                        sv = 0;
                        sv1 = 0;
                        st = (String) jComboBox1.getSelectedItem();
                        sno = Integer.parseInt(jTextField14.getText());
                        totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));

                        DecimalFormat df = new DecimalFormat("0.00");
                        jTextField15.setText("" + totalamount);

                        totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                        String totalquantity1 = df.format(totalquantity);
                        jTextField16.setText("" + totalquantity1);

                        try {
                            connection c = new connection();
                            Connection connect = c.cone();
                            Statement sttt = connect.createStatement();
                            Statement stttt = connect.createStatement();
                            Statement pro_st = connect.createStatement();
                            Statement sgst_st = connect.createStatement();
                            Statement cgst_st = connect.createStatement();
                            ResultSet rssss, rs5, rs4;
                            if (isIgstApplicable == true) {
                                ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                                while (rsss.next()) {

                                    iscalculationapplicable = rsss.getInt("iscalculationapplicable");
                                    if (iscalculationapplicable == 0) {
                                        rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                        while (rssss.next()) {
                                            double vat = Double.parseDouble(rssss.getString("percent"));
                                            double value1 = Double.parseDouble(jTextField9.getText());
                                            if (jRadioButton3.isSelected()) {
                                                amntbeforetax = (value1 / (100 + vat)) * 100;
                                            } else if (jRadioButton4.isSelected()) {
                                                amntbeforetax = value1;
                                            }
                                            if (vat == 5) {
                                                iv = (value1 * vat) / 100;
                                            } else if (vat == 12) {
                                                iv1 = (value1 * vat) / 100;
                                            }
                                        }
                                    } else if (iscalculationapplicable == 1) {
                                        if (Double.parseDouble(jTextField7.getText()) > 1000) {
                                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                            while (rssss.next()) {
                                                double vat = Double.parseDouble(rssss.getString("percent"));
                                                double value1 = Double.parseDouble(jTextField9.getText());
                                                if (jRadioButton3.isSelected()) {
                                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                                } else if (jRadioButton4.isSelected()) {
                                                    amntbeforetax = value1;
                                                }
                                                if (vat == 5) {
                                                    iv = (value1 * vat) / 100;
                                                } else if (vat == 12) {
                                                    iv1 = (value1 * vat) / 100;
                                                }
                                            }

                                        } else if (Double.parseDouble(jTextField7.getText()) <= 1000) {
                                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                            while (rssss.next()) {
                                                double vat = 5;
                                                double value1 = Double.parseDouble(jTextField9.getText());
                                                if (jRadioButton3.isSelected()) {
                                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                                } else if (jRadioButton4.isSelected()) {
                                                    amntbeforetax = value1;
                                                }
                                                iv = (value1 * vat) / 100;
                                            }
                                        }

                                    }

                                }

                            } else if (isIgstApplicable == false) {
                                ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                                while (pro_rs.next()) {
                                    iscalculationapplicable = pro_rs.getInt("iscalculationapplicable");
                                    ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                    if ((iscalculationapplicable == 0) || ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) > 1000))) {
                                        while (sgst_rs.next()) {
                                            sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                        }

                                        ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                        while (cgst_rs.next()) {
                                            cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                        }
                                    } else if ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) < 1000)) {
                                        sgstpercent = 2.5;
                                        cgstpercent = 2.5;
                                    }

                                }

                                totalpercent = sgstpercent + cgstpercent;
                                double value1 = Double.parseDouble(jTextField9.getText());
                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = value1;
                                }
                                if (totalpercent == 5) {
                                    sv = (amntbeforetax * sgstpercent) / 100;
                                    cv = ((amntbeforetax * cgstpercent) / 100);
                                } else if (totalpercent == 12) {
                                    sv1 = (amntbeforetax * sgstpercent) / 100;
                                    cv1 = ((amntbeforetax * cgstpercent) / 100);
                                }

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                        tiv = (tiv + iv);
                        tsv = (tsv + sv);
                        tcv = (tcv + cv);
                        tiv1 = (tiv1 + iv1);
                        tsv1 = (tsv1 + sv1);
                        tcv1 = (tcv1 + cv1);
                        DecimalFormat df1 = new DecimalFormat("0.00");
                        totalamountaftertax = (totalamountbeforetax + tiv + tsv + tcv + tiv1 + tsv1 + tcv1 + Double.parseDouble(jTextField44.getText()));

                        jTextField20.setText("" + df1.format(totalamountaftertax));
                        double value_before_tax = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText());
                        jTextField19.setText("" + df.format(value_before_tax));
                        jTextField30.setText("" + df1.format(tiv));
                        jTextField40.setText("" + df1.format(tiv1));
                        jTextField31.setText("" + df1.format(tsv));
                        jTextField41.setText("" + df1.format(tsv1));
                        jTextField32.setText("" + df1.format(tcv));
                        jTextField42.setText("" + df1.format(tcv1));
                        totalafterdiscount = (Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText()));
                        jTextField18.setText("" + df.format(totalafterdiscount));
                        jTextField33.setText("" + df1.format(Math.abs(trv)));
                        DefaultTableModel dtm_ = (DefaultTableModel) jTable1.getModel();
                        if (jTextField5.getText().isEmpty()) {
                            barcode = "N/A";
                        }
                        st = (String) jComboBox1.getSelectedItem();
                        Object o[] = {sno, barcode, jComboBox3.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                            jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                            st};
                        dtm_.addRow(o);
                        count++;
                        sno = Integer.parseInt(jTextField14.getText());
                        sno++;
                        jTextField14.setText("" + sno);
                        jTextField5.setText(null);
                        jComboBox3.setSelectedIndex(0);
                        jTextField7.setText(null);
                        jTextField8.setText("1");
                        jTextField9.setText(null);
                        jTextField10.setText(null);
                        jTextField11.setText("0");
                        jTextField12.setText("0");
                        jTextField13.setText(null);
                        jComboBox1.setSelectedItem(st);
                        jTextField5.requestFocus();
                    } else if (ismeter != 0 && !Rate.equals("Disable")) {
                        jTextField7.requestFocus();
                    }
                }
            }
        }

        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField7.getText().isEmpty()) {
                if (jTextField5.getText().isEmpty()) {
                    if (ismeter == 0) {
                        if (!jTextField7.getText().isEmpty()) {
                            String ra_te = jTextField7.getText();
                            String qun_tity = null;
                            if (jTextField18.getText() == null) {
                                qun_tity = "0";
                            } else {
                                qun_tity = jTextField8.getText();
                            }

                            double p = Double.parseDouble(ra_te);
                            double q = Double.parseDouble(qun_tity);
                            double amou_nt = Math.round(p * q);
                            jTextField9.setText("" + String.valueOf(amou_nt));
                        }

                        // focusgained
                        double disc_amt = 0;
                        double value = 0;
                        if (!jTextField12.getText().isEmpty()) {
                            disc_amt = Double.parseDouble(jTextField12.getText());
                        } else {
                            disc_amt = 0;
                        }
                        if (!jTextField9.getText().isEmpty()) {
                            value = Double.parseDouble(jTextField9.getText());
                        } else {
                            value = 0;
                        }

                        double total = value - disc_amt;
                        jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                        iv1 = 0;
                        iv = 0;
                        cv = 0;
                        cv1 = 0;
                        sv = 0;
                        sv1 = 0;
                        st = (String) jComboBox1.getSelectedItem();
                        sno = Integer.parseInt(jTextField14.getText());
                        totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));

                        DecimalFormat df = new DecimalFormat("0.00");
                        jTextField15.setText("" + totalamount);

                        totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                        String totalquantity1 = df.format(totalquantity);
                        jTextField16.setText("" + totalquantity1);

                        try {
                            connection c = new connection();
                            Connection connect = c.cone();
                            Statement sttt = connect.createStatement();
                            Statement stttt = connect.createStatement();
                            Statement pro_st = connect.createStatement();
                            Statement sgst_st = connect.createStatement();
                            Statement cgst_st = connect.createStatement();
                            ResultSet rssss, rs5, rs4;
                            if (isIgstApplicable == true) {
                                ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                                while (rsss.next()) {

                                    iscalculationapplicable = rsss.getInt("iscalculationapplicable");
                                    if (iscalculationapplicable == 0) {
                                        rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                        while (rssss.next()) {
                                            double vat = Double.parseDouble(rssss.getString("percent"));
                                            double value1 = Double.parseDouble(jTextField9.getText());
                                            if (jRadioButton3.isSelected()) {
                                                amntbeforetax = (value1 / (100 + vat)) * 100;
                                            } else if (jRadioButton4.isSelected()) {
                                                amntbeforetax = value1;
                                            }
                                            if (vat == 5) {
                                                iv = (value1 * vat) / 100;
//                                
                                            } else if (vat == 12) {
                                                iv1 = (value1 * vat) / 100;
                                            }
                                        }
                                    } else if (iscalculationapplicable == 1) {
                                        if (Double.parseDouble(jTextField7.getText()) > 1000) {
                                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                            while (rssss.next()) {
                                                double vat = Double.parseDouble(rssss.getString("percent"));
                                                double value1 = Double.parseDouble(jTextField9.getText());

                                                if (jRadioButton3.isSelected()) {
                                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                                } else if (jRadioButton4.isSelected()) {
                                                    amntbeforetax = value1;
                                                }
                                                if (vat == 5) {
                                                    iv = (value1 * vat) / 100;
                                                } else if (vat == 12) {
                                                    iv1 = (value1 * vat) / 100;
                                                }

                                            }

                                        } else if (Double.parseDouble(jTextField7.getText()) <= 1000) {
                                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                            while (rssss.next()) {
                                                double vat = 5;
                                                double value1 = Double.parseDouble(jTextField9.getText());
                                                if (jRadioButton3.isSelected()) {
                                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                                } else if (jRadioButton4.isSelected()) {
                                                    amntbeforetax = value1;
                                                }
                                                iv = (value1 * vat) / 100;
                                            }
                                        }

                                    }

                                }

                            } else if (isIgstApplicable == false) {
                                ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                                while (pro_rs.next()) {
                                    iscalculationapplicable = pro_rs.getInt("iscalculationapplicable");
                                    ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                    if ((iscalculationapplicable == 0) || ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) > 1000))) {
                                        while (sgst_rs.next()) {
                                            sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                        }

                                        ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                        while (cgst_rs.next()) {
                                            cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                        }
                                    } else if ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) < 1000)) {
                                        sgstpercent = 2.5;
                                        cgstpercent = 2.5;
                                    }

                                }

                                totalpercent = sgstpercent + cgstpercent;
                                double value1 = Double.parseDouble(jTextField9.getText());
                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = value1;
                                }
                                if (totalpercent == 5) {
                                    sv = (amntbeforetax * sgstpercent) / 100;
                                    cv = ((amntbeforetax * cgstpercent) / 100);
                                } else if (totalpercent == 12) {
                                    sv1 = (amntbeforetax * sgstpercent) / 100;
                                    cv1 = ((amntbeforetax * cgstpercent) / 100);
                                }

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                        tiv = (tiv + iv);
                        tsv = (tsv + sv);
                        tcv = (tcv + cv);
                        tiv1 = (tiv1 + iv1);
                        tsv1 = (tsv1 + sv1);
                        tcv1 = (tcv1 + cv1);
                        DecimalFormat df1 = new DecimalFormat("0.00");
                        totalamountaftertax = (totalamountbeforetax + tiv + tsv + tcv + tiv1 + tsv1 + tcv1 + Double.parseDouble(jTextField44.getText()));

                        jTextField20.setText("" + df1.format(totalamountaftertax));
                        double value_before_tax = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText());
                        jTextField19.setText("" + df.format(value_before_tax));
                        jTextField30.setText("" + df1.format(tiv));
                        jTextField40.setText("" + df1.format(tiv1));
                        jTextField31.setText("" + df1.format(tsv));
                        jTextField41.setText("" + df1.format(tsv1));
                        jTextField32.setText("" + df1.format(tcv));
                        jTextField42.setText("" + df1.format(tcv1));
                        totalafterdiscount = (Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText()));
                        jTextField18.setText("" + df.format(totalafterdiscount));
                        jTextField33.setText("" + df1.format(Math.abs(trv)));
                        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                        if (jTextField5.getText().isEmpty()) {
                            barcode = "N/A";
                        }
                        st = (String) jComboBox1.getSelectedItem();
                        Object o[] = {sno, barcode, jComboBox3.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                            jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                            st};
                        dtm.addRow(o);
                        count++;
                        sno = Integer.parseInt(jTextField14.getText());
                        sno++;
                        jTextField14.setText("" + sno);
                        jTextField5.setText(null);
                        jComboBox3.setSelectedIndex(0);
                        jTextField7.setText(null);
                        jTextField8.setText("1");
                        jTextField9.setText(null);
                        jTextField10.setText(null);
                        jTextField11.setText("0");
                        jTextField12.setText("0");
                        jTextField13.setText(null);
                        jComboBox1.setSelectedItem(st);
                        jTextField5.requestFocus();
                    }
                }
                if (ismeter == 1) {
                    double quantityindatabse = 0;
                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement st = connect.createStatement();

                        ResultSet rs = st.executeQuery("select qnt from stockid2 where stock_no='" + jTextField5.getText() + "' ");
                        while (rs.next()) {
                            quantityindatabse = rs.getDouble("qnt");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (quantityindatabse < Double.parseDouble(jTextField8.getText())) {
                        JOptionPane.showMessageDialog(rootPane, "The Selected Quantity is greater then the Quantity Avaialble !! Please Check Again");
                        jTextField8.requestFocus();
                    } else {
                        if (jTextField5.getText().equals("OTH")) {
                            jTextField10.requestFocus();
                        } else {
                            if (!jTextField7.getText().isEmpty()) {
                                String ra_te = jTextField7.getText();
                                String qun_tity = null;
                                if (jTextField18.getText() == null) {
                                    qun_tity = "0";
                                } else {
                                    qun_tity = jTextField8.getText();
                                }

                                double p = Double.parseDouble(ra_te);
                                double q = Double.parseDouble(qun_tity);
                                double amou_nt = Math.round(p * q);
                                jTextField9.setText("" + String.valueOf(amou_nt));
                            }

                            // focusgained
                            double disc_amt = 0;
                            double value = 0;
                            if (!jTextField12.getText().isEmpty()) {
                                disc_amt = Double.parseDouble(jTextField12.getText());
                            } else {
                                disc_amt = 0;
                            }
                            if (!jTextField9.getText().isEmpty()) {
                                value = Double.parseDouble(jTextField9.getText());
                            } else {
                                value = 0;
                            }

                            double total = value - disc_amt;
                            jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                            st = (String) jComboBox1.getSelectedItem();
                            sno = Integer.parseInt(jTextField14.getText());
                            totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                            DecimalFormat df = new DecimalFormat("0.00");
                            jTextField15.setText("" + totalamount);
                            totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                            jTextField16.setText("" + totalquantity);

                            try {
                                connection c = new connection();
                                Connection connect = c.cone();
                                Statement sttt = connect.createStatement();
                                Statement stttt = connect.createStatement();
                                Statement pro_st = connect.createStatement();
                                Statement sgst_st = connect.createStatement();
                                Statement cgst_st = connect.createStatement();
                                ResultSet rssss, rs5, rs4;
                                if (isIgstApplicable == true) {
                                    ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                                    while (rsss.next()) {

                                        iscalculationapplicable = rsss.getInt("iscalculationapplicable");
                                        if (iscalculationapplicable == 0) {
                                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                            while (rssss.next()) {
                                                double vat = Double.parseDouble(rssss.getString("percent"));
                                                double value1 = Double.parseDouble(jTextField9.getText());
                                                if (jRadioButton3.isSelected()) {
                                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                                } else if (jRadioButton4.isSelected()) {
                                                    amntbeforetax = value1;
                                                }
                                                iv = (value1 * vat) / 100;
                                                tiv = tiv + iv;
                                                jTextField30.setText("" + df.format(tiv));
                                            }

                                        } else if (iscalculationapplicable == 1) {
                                            if (Double.parseDouble(jTextField7.getText()) > 1000) {
                                                rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                                double igst_percent = 0;
                                                while (rssss.next()) {
                                                    igst_percent = Double.parseDouble(rssss.getString("percent"));
                                                    double value1 = Double.parseDouble(jTextField9.getText());
                                                    if (jRadioButton3.isSelected()) {
                                                        amntbeforetax = (value1 / (100 + igst_percent)) * 100;
                                                    } else if (jRadioButton4.isSelected()) {
                                                        amntbeforetax = value1;
                                                    }
                                                }
                                                if (igst_percent == 5) {
                                                    iv = (amntbeforetax * igst_percent) / 100;
                                                    tiv = tiv + iv;
                                                    jTextField30.setText("" + df.format(tiv));
                                                } else if (igst_percent == 12) {
                                                    iv1 = (amntbeforetax * igst_percent) / 100;
                                                    tiv1 = tiv1 + iv1;
                                                    jTextField40.setText("" + df.format(tiv1));
                                                }

                                            } else if (Double.parseDouble(jTextField7.getText()) <= 1000) {
                                                rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                                while (rssss.next()) {
                                                    double vat = 5;
                                                    double value1 = Double.parseDouble(jTextField9.getText());
                                                    if (jRadioButton3.isSelected()) {
                                                        amntbeforetax = (value1 / (100 + vat)) * 100;
                                                    } else if (jRadioButton4.isSelected()) {
                                                        amntbeforetax = value1;
                                                    }
                                                    iv = (value1 * vat) / 100;
                                                    tiv = tiv + iv;
                                                    jTextField30.setText("" + df.format(tiv));
                                                }

                                            }

                                        }

                                    }

                                } else if (isIgstApplicable == false) {
                                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                                    while (pro_rs.next()) {
                                        iscalculationapplicable = pro_rs.getInt("iscalculationapplicable");
                                        ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                        if ((iscalculationapplicable == 0) || ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) > 1000))) {

                                            while (sgst_rs.next()) {
                                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                            }

                                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                            while (cgst_rs.next()) {
                                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                            }

                                        } else if ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) < 1000)) {

                                            sgstpercent = 2.5;
                                            cgstpercent = 2.5;
                                        }

                                    }

                                    totalpercent = sgstpercent + cgstpercent;
                                    double value1 = Double.parseDouble(jTextField9.getText());
                                    if (jRadioButton3.isSelected()) {
                                        amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amntbeforetax = value1;
                                    }
                                    if (totalpercent == 5) {

                                        sv = (amntbeforetax * sgstpercent) / 100;
                                        cv = ((amntbeforetax * cgstpercent) / 100);

                                        tsv = tsv + sv;
                                        tcv = tcv + cv;

                                        jTextField31.setText("" + df.format(tsv));
                                        jTextField32.setText("" + df.format(tcv));

                                    } else if (totalpercent == 12) {

                                        sv1 = (amntbeforetax * sgstpercent) / 100;
                                        cv1 = ((amntbeforetax * cgstpercent) / 100);

                                        tsv1 = tsv1 + sv1;
                                        tcv1 = tcv1 + cv1;

                                        jTextField41.setText("" + df.format(tsv1));
                                        jTextField42.setText("" + df.format(tcv1));
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            totalamountbeforetax = (totalamountbeforetax + amntbeforetax);

                            DecimalFormat df1 = new DecimalFormat("0.00");
                            totalamountaftertax = (totalamountbeforetax + tiv + tsv + tcv + tiv1 + tsv1 + tcv1 + Double.parseDouble(jTextField44.getText()));
                            jTextField20.setText("" + df1.format(totalamountaftertax));
                            double discount = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText());
                            jTextField19.setText("" + df.format(discount));
                            totalafterdiscount = (Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText()));
                            jTextField18.setText("" + df.format(totalafterdiscount));
                            if (jTextField5.getText().isEmpty()) {
                                barcode = "N/A";
                            }
                            st = (String) jComboBox1.getSelectedItem();
                            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                            Object o[] = {sno, barcode, jComboBox3.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                                jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                                st};
                            dtm.addRow(o);
                            count++;
                            sno = Integer.parseInt(jTextField14.getText());
                            sno++;
                            jTextField14.setText("" + sno);
                            jTextField5.setText(null);
                            jComboBox3.setSelectedIndex(0);
                            jTextField7.setText(null);
                            jTextField8.setText("1");
                            jTextField9.setText(null);
                            jTextField10.setText(null);
                            jTextField11.setText("0");
                            jTextField12.setText("0");
                            jTextField13.setText(null);
                            jComboBox1.setSelectedItem(st);
                            jTextField5.requestFocus();

                        }

                    }
                }

                if (Double.parseDouble(jTextField45.getText()) != 0) {
                    freightGst();
                }
            } else if (jTextField7.getText().isEmpty()) {
                jTextField5.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField7.requestFocus();
        }
    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().isEmpty()) {
                jTextField8.requestFocus();
            } else if (!jTextField5.getText().isEmpty()) {
                if (ismeter == 0) {
                    if (!jTextField7.getText().isEmpty()) {
                        String ra_te = jTextField7.getText();
                        String qun_tity = null;
                        if (jTextField18.getText() == null) {
                            qun_tity = "0";
                        } else {
                            qun_tity = jTextField8.getText();
                        }

                        double p = Double.parseDouble(ra_te);
                        double q = Double.parseDouble(qun_tity);
                        double amou_nt = Math.round(p * q);
                        jTextField9.setText("" + String.valueOf(amou_nt));
                    }

                    // focusgained
                    double disc_amt = 0;
                    double value = 0;
                    if (!jTextField12.getText().isEmpty()) {
                        disc_amt = Double.parseDouble(jTextField12.getText());
                    } else {
                        disc_amt = 0;
                    }
                    if (!jTextField9.getText().isEmpty()) {
                        value = Double.parseDouble(jTextField9.getText());
                    } else {
                        value = 0;
                    }

                    double total = value - disc_amt;
                    jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                    iv1 = 0;
                    iv = 0;
                    cv = 0;
                    cv1 = 0;
                    sv = 0;
                    sv1 = 0;
                    st = (String) jComboBox1.getSelectedItem();
                    sno = Integer.parseInt(jTextField14.getText());
                    totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));

                    DecimalFormat df = new DecimalFormat("0.00");
                    jTextField15.setText("" + totalamount);

                    totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                    String totalquantity1 = df.format(totalquantity);
                    jTextField16.setText("" + totalquantity1);

                    try {
                        connection c = new connection();
                        Connection connect = c.cone();
                        Statement sttt = connect.createStatement();
                        Statement stttt = connect.createStatement();
                        Statement pro_st = connect.createStatement();
                        Statement sgst_st = connect.createStatement();
                        Statement cgst_st = connect.createStatement();
                        ResultSet rssss, rs5, rs4;
                        if (isIgstApplicable == true) {
                            ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                            while (rsss.next()) {

                                iscalculationapplicable = rsss.getInt("iscalculationapplicable");
                                if (iscalculationapplicable == 0) {
                                    rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                    while (rssss.next()) {
                                        double vat = Double.parseDouble(rssss.getString("percent"));
                                        double value1 = Double.parseDouble(jTextField9.getText());
                                        if (jRadioButton3.isSelected()) {
                                            amntbeforetax = (value1 / (100 + vat)) * 100;
                                        } else if (jRadioButton4.isSelected()) {
                                            amntbeforetax = value1;
                                        }
                                        if (vat == 5) {
                                            iv = (value1 * vat) / 100;
                                        } else if (vat == 12) {
                                            iv1 = (value1 * vat) / 100;
                                        }
                                    }
                                } else if (iscalculationapplicable == 1) {
                                    if (Double.parseDouble(jTextField7.getText()) > 1000) {
                                        rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                        while (rssss.next()) {
                                            double vat = Double.parseDouble(rssss.getString("percent"));
                                            double value1 = Double.parseDouble(jTextField9.getText());
                                            if (jRadioButton3.isSelected()) {
                                                amntbeforetax = (value1 / (100 + vat)) * 100;
                                            } else if (jRadioButton4.isSelected()) {
                                                amntbeforetax = value1;
                                            }
                                            if (vat == 5) {
                                                iv = (value1 * vat) / 100;
                                            } else if (vat == 12) {
                                                iv1 = (value1 * vat) / 100;
                                            }
                                        }

                                    } else if (Double.parseDouble(jTextField7.getText()) <= 1000) {
                                        rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                        while (rssss.next()) {
                                            double vat = 5;
                                            double value1 = Double.parseDouble(jTextField9.getText());
                                            if (jRadioButton3.isSelected()) {
                                                amntbeforetax = (value1 / (100 + vat)) * 100;
                                            } else if (jRadioButton4.isSelected()) {
                                                amntbeforetax = value1;
                                            }
                                            iv = (value1 * vat) / 100;
                                        }
                                    }

                                }

                            }

                        } else if (isIgstApplicable == false) {
                            ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                            while (pro_rs.next()) {
                                iscalculationapplicable = pro_rs.getInt("iscalculationapplicable");
                                ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                if ((iscalculationapplicable == 0) || ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) > 1000))) {
                                    while (sgst_rs.next()) {
                                        sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                    }

                                    ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                    while (cgst_rs.next()) {
                                        cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                    }
                                } else if ((iscalculationapplicable == 1) && (Double.parseDouble(jTextField7.getText()) < 1000)) {
                                    sgstpercent = 2.5;
                                    cgstpercent = 2.5;
                                }

                            }

                            totalpercent = sgstpercent + cgstpercent;
                            double value1 = Double.parseDouble(jTextField9.getText());
                            if (jRadioButton3.isSelected()) {
                                amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                            } else if (jRadioButton4.isSelected()) {
                                amntbeforetax = value1;
                            }
                            if (totalpercent == 5) {
                                sv = (amntbeforetax * sgstpercent) / 100;
                                cv = ((amntbeforetax * cgstpercent) / 100);
                            } else if (totalpercent == 12) {
                                sv1 = (amntbeforetax * sgstpercent) / 100;
                                cv1 = ((amntbeforetax * cgstpercent) / 100);
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                    tiv = (tiv + iv);
                    tsv = (tsv + sv);
                    tcv = (tcv + cv);
                    tiv1 = (tiv1 + iv1);
                    tsv1 = (tsv1 + sv1);
                    tcv1 = (tcv1 + cv1);
                    DecimalFormat df1 = new DecimalFormat("0.00");
                    totalamountaftertax = (totalamountbeforetax + tiv + tsv + tcv + tiv1 + tsv1 + tcv1 + Double.parseDouble(jTextField44.getText()));

                    jTextField20.setText("" + df1.format(totalamountaftertax));
                    double value_before_tax = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText());
                    jTextField19.setText("" + df.format(value_before_tax));
                    jTextField30.setText("" + df1.format(tiv));
                    jTextField40.setText("" + df1.format(tiv1));
                    jTextField31.setText("" + df1.format(tsv));
                    jTextField41.setText("" + df1.format(tsv1));
                    jTextField32.setText("" + df1.format(tcv));
                    jTextField42.setText("" + df1.format(tcv1));
                    totalafterdiscount = (Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText()));
                    jTextField18.setText("" + df.format(totalafterdiscount));
                    jTextField33.setText("" + df1.format(Math.abs(trv)));
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    if (jTextField5.getText().isEmpty()) {
                        barcode = "N/A";
                    }
                    st = (String) jComboBox1.getSelectedItem();
                    Object o[] = {sno, barcode, jComboBox3.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                        jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                        st};
                    dtm.addRow(o);
                    count++;
                    sno = Integer.parseInt(jTextField14.getText());
                    sno++;
                    jTextField14.setText("" + sno);
                    jTextField5.setText(null);
                    jComboBox3.setSelectedIndex(0);
                    jTextField7.setText(null);
                    jTextField8.setText("1");
                    jTextField9.setText(null);
                    jTextField10.setText(null);
                    jTextField11.setText("0");
                    jTextField12.setText("0");
                    jTextField13.setText(null);
                    jComboBox1.setSelectedItem(st);
                    jTextField5.requestFocus();
                } else if (ismeter == 1) {
                    jTextField8.requestFocus();
                }
                if (Double.parseDouble(jTextField45.getText()) != 0) {
                    freightGst();
                }
            }
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox3.requestFocus();
        }
    }//GEN-LAST:event_jTextField7KeyPressed

    private void jTextField10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField10KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().equals("OTH")) {
                if (!jTextField7.getText().isEmpty()) {
                    String ra_te = jTextField7.getText();
                    String qun_tity = null;
                    if (jTextField18.getText() == null) {
                        qun_tity = "0";
                    } else {
                        qun_tity = jTextField8.getText();
                    }

                    double p = Double.parseDouble(ra_te);
                    double q = Double.parseDouble(qun_tity);
                    double amou_nt = (p * q);
                    jTextField9.setText("" + String.valueOf(amou_nt));
                }

                // focusgained
                double disc_amt = 0;
                double value = 0;
                if (!jTextField12.getText().isEmpty()) {
                    disc_amt = Double.parseDouble(jTextField12.getText());
                } else {
                    disc_amt = 0;
                }
                if (!jTextField9.getText().isEmpty()) {
                    value = Double.parseDouble(jTextField9.getText());
                } else {
                    value = 0;
                }

                double total = value - disc_amt;
                jTextField13.setText("" + String.valueOf(total));

// combobox key pressed
                st = (String) jComboBox1.getSelectedItem();
                sno = Integer.parseInt(jTextField14.getText());
                totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                DecimalFormat df = new DecimalFormat("0.00");
                jTextField15.setText("" + (int) totalamount);
                totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));
                jTextField16.setText("" + totalquantity);

                double disc = 0;
                if (!jTextField12.getText().isEmpty()) {
                    disc = Double.parseDouble(jTextField12.getText());
                } else {
                    disc = 0;
                }

                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement sttt = connect.createStatement();
                    Statement stttt = connect.createStatement();
                    Statement pro_st = connect.createStatement();
                    Statement sgst_st = connect.createStatement();
                    Statement cgst_st = connect.createStatement();
                    ResultSet rssss, rs5, rs4;
                    if (isIgstApplicable == true) {
                        ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                        while (rsss.next()) {

                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                            while (rssss.next()) {
                                double vat = Double.parseDouble(rssss.getString("percent"));
                                double value1 = Double.parseDouble(jTextField9.getText());
                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = (value1);
                                }
                                iv = (amntbeforetax * vat) / 100;
                            }

                        }

                    } else if (isIgstApplicable == false) {
                        ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox3.getSelectedItem() + "'");
                        while (pro_rs.next()) {
                            ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                            while (sgst_rs.next()) {
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }

                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }
                        }

                        totalpercent = sgstpercent + cgstpercent;
                        double value1 = Double.parseDouble(jTextField9.getText());

                        if (jRadioButton3.isSelected()) {
                            amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amntbeforetax = value1;
                        }

                        sv = (amntbeforetax * sgstpercent) / 100;
                        cv = ((amntbeforetax * cgstpercent) / 100);

                        double round_total = (totalamountaftertax - (amntbeforetax + sv + cv));
                        double round_figure = Math.round(round_total);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                tiv = (tiv + iv);
                tsv = (tsv + sv);
                tcv = (tcv + cv);
                DecimalFormat df1 = new DecimalFormat("0.00");

                totalamountaftertax = (totalamountbeforetax + tiv + tsv + tcv + tiv1 + tsv1 + tcv1 + Double.parseDouble(jTextField44.getText()));
                jTextField20.setText("" + df1.format(totalamountaftertax));
                double discount = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText());
                jTextField19.setText("" + df.format(discount));
                jTextField30.setText("" + df1.format(tiv));
                jTextField31.setText("" + df1.format(tsv));
                jTextField32.setText("" + df1.format(tcv));
                totalafterdiscount = (Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText()));
                jTextField18.setText("" + df.format(totalafterdiscount));
                trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + (Double.parseDouble(jTextField32.getText()))));

                jTextField33.setText("" + df1.format(Math.abs(trv)));

                if (jTextField5.getText().isEmpty()) {
                    barcode = "N/A";
                }

                st = (String) jComboBox1.getSelectedItem();
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                Object o[] = {sno, barcode, jComboBox3.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                    jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                    st};
                dtm.addRow(o);
                count++;
                sno = Integer.parseInt(jTextField14.getText());
                sno++;
                jTextField14.setText("" + sno);
                jTextField5.setText(null);
                jComboBox3.setSelectedIndex(0);
                jTextField7.setText(null);
                jTextField8.setText("1");
                jTextField9.setText(null);
                jTextField10.setText(null);
                jTextField11.setText("0");
                jTextField12.setText("0");
                jTextField13.setText(null);
                jComboBox1.setSelectedItem(st);
                jTextField17.requestFocus();
            }
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jTextField10KeyPressed
    ArrayList snumber = new ArrayList();
    String st = null;
    ArrayList stock = new ArrayList();
    ArrayList prod = new ArrayList();
    ArrayList rate = new ArrayList();
    ArrayList qnty = new ArrayList();
    ArrayList value = new ArrayList();
    ArrayList disc_code = new ArrayList();
    ArrayList discper = new ArrayList();
    ArrayList discamt = new ArrayList();
    ArrayList total = new ArrayList();
    ArrayList staff = new ArrayList();
    double per[] = new double[1000];
    int count = 0;
    public int sno = 1;
    double totalamount = 0;
    double totalafterdiscount = 0;
    double totalquantity = 0;
    double totaldiscount = 0;
    double totalamountaftertax = 0;
    double totalamountbeforetax = 0;
    double perc = 0;
    int q = 1;
    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jTextField20KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField20KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ESCAPE) {
            jTextField19.requestFocus();
        }
    }//GEN-LAST:event_jTextField20KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        int key1 = evt.getKeyCode();
        if (key == evt.VK_ENTER) {
            if (issave == false) {
                jButton12.setVisible(false);
                if (issave == false) {

                    jDialog4.setVisible(true);
                    jLabel48.setText(jTextField20.getText());
                    if (jRadioButton1.isSelected()) {
                        jTextField34.setText(jTextField20.getText());
                    } else if (jRadioButton2.isSelected()) {
                        jTextField35.setText(jTextField20.getText());
                    } else if (jRadioButton5.isSelected()) {
                        jTextField36.setText(jTextField20.getText());
                    }

                }
            }
        }
        if (key1 == evt.VK_RIGHT) {
            jButton2.requestFocus();
        }
    }//GEN-LAST:event_jButton1KeyPressed
    ArrayList vat = new ArrayList();
    ArrayList ledgervat = new ArrayList();
    ArrayList ledgersgst = new ArrayList();
    ArrayList ledgervat2 = new ArrayList();
    ArrayList sgst = new ArrayList();
    ArrayList sgstledger = new ArrayList();
    ArrayList cgst = new ArrayList();
    ArrayList cgstledger = new ArrayList();
    ArrayList vatamount = new ArrayList();
    ArrayList vatcurrbal = new ArrayList();
    ArrayList vatcurrbaltype = new ArrayList();
    ArrayList updatevatcurrbal = new ArrayList();
    ArrayList updatevatcurrbaltype = new ArrayList();

    ArrayList sgstcurrbal = new ArrayList();
    ArrayList sgstcurrbaltype = new ArrayList();
    ArrayList updatesgstcurrbal = new ArrayList();
    ArrayList updatesgstcurrbaltype = new ArrayList();

    ArrayList cgstcurrbal = new ArrayList();
    ArrayList cgstcurrbaltype = new ArrayList();
    ArrayList updatecgstcurrbal = new ArrayList();
    ArrayList updatecgstcurrbaltype = new ArrayList();

    ArrayList salesaccount = new ArrayList();
    ArrayList salesaccount1 = new ArrayList();

    ArrayList Salesaccountcurrbal = new ArrayList();
    ArrayList Salesaccountcurrbaltype = new ArrayList();
    ArrayList Salesaccountcurrbal1 = new ArrayList();
    ArrayList Salesaccountcurrbaltype1 = new ArrayList();

    ArrayList Salesaccountupdatecurrbal = new ArrayList();
    ArrayList Salesaccountupdatecurrbaltype = new ArrayList();
    ArrayList cashaccountcurrbal = new ArrayList();
    ArrayList cashaccountcurrbaltype = new ArrayList();
    ArrayList cashaccountupdatecurrbal = new ArrayList();
    ArrayList cashaccountupdatecurrbaltype = new ArrayList();
    ArrayList sundrydebtorsaccountcurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountcurrbaltype = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbaltype = new ArrayList();
    ArrayList cardaccountcurrbal = new ArrayList();
    ArrayList cardaccountcurrbaltype = new ArrayList();
    ArrayList cardaccountupdatecurrbal = new ArrayList();
    ArrayList cardaccountupdatecurrbaltype = new ArrayList();
    ArrayList bankaccountcurrbal = new ArrayList();
    ArrayList bankaccountcurrbaltype = new ArrayList();
    ArrayList bankaccountupdatecurrbal = new ArrayList();
    ArrayList bankaccountupdatecurrbaltype = new ArrayList();
    double vatperc = 0;
    double vatamnt = 0;
    String igstamnt1_1;
    double amountbeforevat = 0;

    double total_igst_taxamnt = 0;
    double total_sgst_taxamnt = 0;
    double total_cgst_taxamnt = 0;

    double sgstperc = 0;
    double sgstamnt = 0;
    String sgstamnt1_1;
    double amountbeforesgst = 0;

    double cgstperc = 0;
    double cgstamnt = 0;
    String cgstamnt1_1;
    double amountbeforecgst = 0;

    public boolean issave = false;

    public void save() {

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = jTable1.getRowCount();
        if (rowcount == 0) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter the stock NO value has been entered");
        } else {
            issave = true;
            double discount = Double.parseDouble(jTextField17.getText());
            double totalamntafterbill = Double.parseDouble(jTextField15.getText());
            double discountpercentage = (discount / totalamntafterbill) * 100;

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement Cashaccount = connect.createStatement();
                Statement cardaccount = connect.createStatement();
                Statement sundrydebtorsaccount = connect.createStatement();
                String c1 = "CASH";
                ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='" + c1 + "'");
                while (rscashaccount.next()) {
                    cashaccountcurrbal.add(0, rscashaccount.getString(10));
                    cashaccountcurrbaltype.add(0, rscashaccount.getString(11));
                }

                ResultSet rscard = cardaccount.executeQuery("select * from ledger where ledger_name='" + cardpaymentaccount + "'");
                while (rscard.next()) {
                    cardaccountcurrbal.add(0, rscard.getString(10));
                    cardaccountcurrbaltype.add(0, rscard.getString(11));
                }

                ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + sundrydebtors + "'");
                while (rssundrydebtors.next()) {
                    sundrydebtorsaccountcurrbal.add(0, rssundrydebtors.getString(10));
                    sundrydebtorsaccountcurrbaltype.add(0, rssundrydebtors.getString(11));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            for (int i = 0; i < rowcount; i++) {
                try {
                    double totalafterdiscountpercentage = (discountpercentage / 100) * Double.parseDouble(jTable1.getValueAt(i, 9) + "");
                    double totalfinal = Double.parseDouble(jTable1.getValueAt(i, 9) + "") - totalafterdiscountpercentage;

                    snumber.add(i, jTable1.getValueAt(i, 0));
                    stock.add(i, jTable1.getValueAt(i, 1));
                    prod.add(i, jTable1.getValueAt(i, 2));
                    rate.add(i, jTable1.getValueAt(i, 3));
                    qnty.add(i, jTable1.getValueAt(i, 4));
                    value.add(i, jTable1.getValueAt(i, 5));
                    disc_code.add(i, jTable1.getValueAt(i, 6));
                    discper.add(i, jTable1.getValueAt(i, 7));
                    discamt.add(i, jTable1.getValueAt(i, 8));
                    total.add(i, jTable1.getValueAt(i, 9));
                    staff.add(i, jTable1.getValueAt(i, 10));

                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = null;
                    ResultSet rs = null;
                    PreparedStatement pstm = null;
                    int j = 0;
                    int check = 0;
                    String HELLO = "HELLO";
                    String paytype = null;
                    String to_ledger = null;
                    String by_ledger = null;
                    try {
                        Statement stin = connect.createStatement();
                        Statement del = connect.createStatement();
                        Statement stmt1 = connect.createStatement();
                        Statement stmtvataccount = connect.createStatement();
                        Statement stru = connect.createStatement();
                        Statement stru1 = connect.createStatement();

                        Statement stmt2 = connect.createStatement();
                        Statement stmtcgstaccount = connect.createStatement();
                        Statement stmtiscalculation = connect.createStatement();

                        ResultSet rsiscalculation = stmtiscalculation.executeQuery("select iscalculationapplicable from product where product_code='" + prod.get(i) + "" + "'");
                        while (rsiscalculation.next()) {
                            iscalculationapplicable = rsiscalculation.getInt(1);
                        }

                        if ((String) prod.get(i) != null) {
                            if (isIgstApplicable == true) {

                                if ((iscalculationapplicable == 0) || (iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") > 1000)) {
                                    ResultSet rsvat = stmt1.executeQuery("select IGST from product where product_code='" + (String) prod.get(i) + "'");
                                    while (rsvat.next()) {
                                        ledgervat.add(i, rsvat.getString(1));
                                    }
                                } else if (iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") < 1000) {
                                    ResultSet rsvat = stmt1.executeQuery("select ledger_name from ledger where percent='5' and type='IGST'");
                                    while (rsvat.next()) {
                                        ledgervat.add(i, rsvat.getString(1));
                                    }
                                }
                                ResultSet rsvataccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgervat.get(i) + "'");
                                while (rsvataccount.next()) {
                                    vat.add(i, rsvataccount.getString(6));
                                    vatcurrbal.add(i, rsvataccount.getString(10));
                                    vatcurrbaltype.add(i, rsvataccount.getString(11));

                                    vatperc = Double.parseDouble((String) vat.get(i));
                                    if (jRadioButton3.isSelected()) {
                                        amountbeforevat = (Double.parseDouble((String) total.get(i)) / (100 + vatperc)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amountbeforevat = Double.parseDouble(total.get(i) + "");
                                    }
                                    vatamnt = (vatperc * amountbeforevat) / 100;

                                    DecimalFormat idf = new DecimalFormat("0.00");
                                    igstamnt1_1 = idf.format(vatamnt);

                                    String vatcurrbalance = (String) vatcurrbal.get(i);
                                    String Curbaltypevat = (String) vatcurrbaltype.get(i);
                                    double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                                    double vatamnt1 = Math.round(vatamnt);
                                    double result;
                                    if (Curbaltypevat.equals("DR")) {
                                        result = vatcurrbalance1 - vatamnt1;
                                        if (result >= 0) {
                                            updatevatcurrbaltype.add(i, "DR");
                                            updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));
                                        } else {
                                            updatevatcurrbaltype.add(i, "CR");
                                            updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));
                                        }

                                    } else if (Curbaltypevat.equals("CR")) {

                                        result = vatcurrbalance1 + vatamnt1;

                                        updatevatcurrbaltype.add(i, "CR");
                                        updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));

                                    }

                                    total_igst_taxamnt = total_igst_taxamnt + vatamnt;
                                }
                                sgstperc = 0;
                                sgstamnt = 0;
                                cgstperc = 0;
                                cgstamnt = 0;

                            } else if (isIgstApplicable == false) {

                                if ((iscalculationapplicable == 0) || (iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") > 1000)) {
                                    ResultSet rssgst = stmt1.executeQuery("select SGST from product where product_code='" + (String) prod.get(i) + "'");
                                    while (rssgst.next()) {
                                        ledgersgst.add(i, rssgst.getString(1));
                                    }
                                    System.out.println("condition 1 is running for sgst");
                                } else if ((iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") < 1000)) {

                                    ResultSet rssgst = stmt1.executeQuery("select ledger_name from ledger where percent='2.5' and type='SGST'");
                                    while (rssgst.next()) {
                                        ledgersgst.add(i, rssgst.getString(1));
                                    }
                                    System.out.println("condition 2 is running for sgst");
                                }

// For SGST                      
                                ResultSet rssgstaccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgersgst.get(i) + "'");
                                while (rssgstaccount.next()) {
                                    sgst.add(i, rssgstaccount.getString(6));
                                    sgstcurrbal.add(i, rssgstaccount.getString(10));
                                    sgstcurrbaltype.add(i, rssgstaccount.getString(11));
                                    sgstperc = Double.parseDouble((String) sgst.get(i));

// SGST Caluclation                      
                                    if (jRadioButton3.isSelected()) {
                                        amountbeforesgst = (Double.parseDouble((String) total.get(i)) / (100 + sgstperc + sgstperc)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amountbeforesgst = Double.parseDouble(total.get(i) + "");
                                    }
                                    sgstamnt = (sgstperc * amountbeforesgst) / 100;

                                    DecimalFormat sdf = new DecimalFormat("0.00");
                                    sgstamnt1_1 = sdf.format(sgstamnt);

                                    String vatcurrbalance = (String) sgstcurrbal.get(i);
                                    System.out.println("current balance is" + vatcurrbalance);
                                    String Curbaltypesgst = (String) sgstcurrbaltype.get(i);
                                    double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                                    double sgstamnt1 = sgstamnt;
                                    System.out.println("SGST amount is" + sgstamnt);
                                    double result;
                                    if (Curbaltypesgst.equals("DR")) {
                                        result = vatcurrbalance1 - sgstamnt1;
                                        if (result >= 0) {
                                            updatesgstcurrbaltype.add(i, "DR");
                                            updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            System.out.println("result condition 1 is is" + result);
                                        } else {
                                            updatesgstcurrbaltype.add(i, "CR");
                                            updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                            System.out.println("result condition 2 is is" + result);
                                        }

                                    } else if (Curbaltypesgst.equals("CR")) {

                                        result = vatcurrbalance1 + sgstamnt1;

                                        updatesgstcurrbaltype.add(i, "CR");
                                        updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                        System.out.println("result condition 3 is" + result);
                                    }
//CGST Calculation            
                                }

                                // For CGST                      
                                if ((iscalculationapplicable == 0) || (iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") > 1000)) {
                                    ResultSet rscgst = stmt2.executeQuery("select CGST from product where product_code='" + (String) prod.get(i) + "'");
                                    while (rscgst.next()) {
                                        ledgervat2.add(i, rscgst.getString(1));
                                    }
                                } else if (iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") < 1000) {
                                    ResultSet rscgst = stmt2.executeQuery("select ledger_name from ledger where percent='2.5' and type='CGST'");
                                    while (rscgst.next()) {
                                        ledgervat2.add(i, rscgst.getString(1));
                                    }
                                }

                                ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgervat2.get(i) + "'");
                                while (rscgstaccount.next()) {
                                    cgst.add(i, rscgstaccount.getString(6));
                                    cgstcurrbal.add(i, rscgstaccount.getString(10));
                                    cgstcurrbaltype.add(i, rscgstaccount.getString(11));
                                    cgstperc = Double.parseDouble((String) cgst.get(i));
                                    if (jRadioButton3.isSelected()) {
                                        amountbeforecgst = (Double.parseDouble((String) total.get(i)) / (100 + cgstperc + sgstperc)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amountbeforecgst = Double.parseDouble((String) total.get(i));
                                    }
                                    cgstamnt = (cgstperc * amountbeforecgst) / 100;

                                    DecimalFormat cdf = new DecimalFormat("0.00");
                                    cgstamnt1_1 = cdf.format(cgstamnt);

                                    String cgstcurrbalance = (String) cgstcurrbal.get(i);
                                    String Curbaltypecgst = (String) cgstcurrbaltype.get(i);
                                    double vatcurrbalance2 = Double.parseDouble(cgstcurrbalance);
                                    double cgstamnt1 = cgstamnt;
                                    double result1;
                                    if (Curbaltypecgst.equals("DR")) {
                                        result1 = vatcurrbalance2 - cgstamnt1;
                                        if (result1 >= 0) {
                                            updatecgstcurrbaltype.add(i, "DR");
                                            updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                        } else {
                                            updatecgstcurrbaltype.add(i, "CR");
                                            updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                        }

                                    } else if (Curbaltypecgst.equals("CR")) {

                                        result1 = vatcurrbalance2 + cgstamnt1;

                                        updatecgstcurrbaltype.add(i, "CR");
                                        updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));

                                    }
                                }

                                vatperc = 0;
                                vatamnt = 0;
                                total_sgst_taxamnt = total_sgst_taxamnt + sgstamnt;
                                total_cgst_taxamnt = total_cgst_taxamnt + cgstamnt;

                            }
                        } else if (prod.get(i) == null) {
                            vatperc = 0;
                            vatamnt = 0;
                            sgstperc = 0;
                            sgstamnt = 0;
                            cgstperc = 0;
                            cgstamnt = 0;
                        }
                        Statement Salesaccount = connect.createStatement();
                        Statement Salesaccount2 = connect.createStatement();
                        String Sales = "SALES ACCOUNTS";
                        String Salesgrp = null;
                        if ((iscalculationapplicable == 0) || (iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") > 1000)) {
                            ResultSet rssalesaccount = Salesaccount.executeQuery("Select salestopurchase from product where product_code='" + prod.get(i) + "' ");
                            while (rssalesaccount.next()) {
                                salesaccount.add(i, rssalesaccount.getString(1));
                            }
                            ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + (String) salesaccount.get(i) + "'");
                            while (rssales.next()) {
                                System.out.println("value os salescurrbal type is " + rssales.getString(11));
                                Salesaccountcurrbal.add(i, rssales.getString(10));
                                Salesaccountcurrbaltype.add(i, rssales.getString(11));
                            }
                        } else if (iscalculationapplicable == 1 && Double.parseDouble(rate.get(i) + "") < 1000) {
                            ResultSet rssalesaccount = Salesaccount.executeQuery("Select ledger_name from ledger where percent='5' and groups='SALES ACCOUNTS' ");
                            while (rssalesaccount.next()) {
                                salesaccount.add(i, rssalesaccount.getString(1));
                            }

                            ResultSet rssales = Salesaccount2.executeQuery("select * from ledger where ledger_name='" + (String) salesaccount.get(i) + "'");
                            while (rssales.next()) {
                                Salesaccountcurrbal.add(i, rssales.getString(10));
                                Salesaccountcurrbaltype.add(i, rssales.getString(11));
                            }
                        }
                        if (jRadioButton1.isSelected()) {
                            paytype = jRadioButton1.getLabel();
                        }
                        if (jRadioButton2.isSelected()) {
                            paytype = jRadioButton2.getLabel();
                        }
                        if (jRadioButton5.isSelected()) {
                            paytype = jRadioButton5.getLabel();
                        }
                        String salesaccountamnt = (String) Salesaccountcurrbal.get(i);

                        double totalamnt = Double.parseDouble((String) total.get(i));

                        double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);

                        double salesaccountupdatedamount = (salesaccountamnt1 - (totalamnt));
                        if (Salesaccountcurrbaltype.get(i).equals("DR")) {
                            if (salesaccountupdatedamount >= 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("DR");

                            }
                            if (salesaccountupdatedamount <= 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("CR");
                            }
                        } else if (Salesaccountcurrbaltype.get(i).equals("CR")) {
                            salesaccountupdatedamount = salesaccountamnt1 + (totalamnt);
                            Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                            Salesaccountupdatecurrbaltype.add("CR");
                            System.out.println("" + Salesaccountupdatecurrbal.get(i));
                            System.out.println("" + Salesaccountupdatecurrbal.get(i));
                        }

                        by_ledger = (String) salesaccount.get(i);
                        if (!(jTable1.getValueAt(i, 1) + "").equals("N/A")) {
                            try {
                                ResultSet rsparty = stmt1.executeQuery("Select supplier_name from stockid2 where stock_no='" + jTable1.getValueAt(i, 1) + "" + "'");
                                while (rsparty.next()) {
                                    party.add(rsparty.getString(1));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if ((jTable1.getValueAt(i, 1) + "").equals("N/A")) {
                            party.add("N/A");
                        }

                        int meterapplicable = 0;
                        double quantity = 0;
                        double updatedquantity = 0;
                        try {
                            ResultSet rsmeter = stmt1.executeQuery("select ismeter,qnt from stockid2 where stock_no='" + stock.get(i) + "" + "'");
                            while (rsmeter.next()) {
                                meterapplicable = rsmeter.getInt("ismeter");
                                quantity = rsmeter.getDouble("qnt");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String cashaccount = "";
                        if (Double.parseDouble(jTextField34.getText()) != 0) {
                            cashaccount = "CASH";
                        }

                        String Date = formatter.format(jDateChooser1.getDate());

                        if (isIgstApplicable == true) {

                            if ("IGST 5%".equals(ledgervat.get(i))) {
                                stmt1.executeUpdate("insert into purchasereturndualgst values('" + jTextField1.getText() + "','" + Date + "',"
                                        + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                        + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                        + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                        + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                        + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','" + igstamnt1_1 + "','0','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','0' , '0' ,'0','0','" + jTextField33.getText() + "','" + jTextField30.getText() + "','" + jTextField31.getText() + "','" + jTextField32.getText() + "'," + meterapplicable + ",'" + jTextField37.getText() + "','" + jTextField34.getText() + "','" + cashaccount + "','" + jTextField35.getText() + "','" + sundrydebtors + "','" + jTextField36.getText() + "','" + cardpaymentaccount + "','" + jTextField38.getText() + "','" + jTextField37.getText() + "','" + (String) ledgervat.get(i) + "', 'SGST 0%','CGST 0%','" + jTextField40.getText() + "','" + jTextField41.getText() + "','" + jTextField42.getText() + "','" + jTextField44.getText() + "','" + freight_account + "','" + freight_gst + "','" + jTextField45.getText() + "','" + tax_type + "')");
                            } else if ("IGST 12%".equals(ledgervat.get(i))) {

                                stmt1.executeUpdate("insert into purchasereturndualgst values('" + jTextField1.getText() + "','" + Date + "',"
                                        + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                        + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                        + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                        + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                        + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','0','" + igstamnt1_1 + "','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','0' , '0' ,'0','0','" + jTextField33.getText() + "','" + jTextField30.getText() + "','" + jTextField31.getText() + "','" + jTextField32.getText() + "'," + meterapplicable + ",'" + jTextField37.getText() + "','" + jTextField34.getText() + "','" + cashaccount + "','" + jTextField35.getText() + "','" + sundrydebtors + "','" + jTextField36.getText() + "','" + cardpaymentaccount + "','" + jTextField38.getText() + "','" + jTextField37.getText() + "','" + (String) ledgervat.get(i) + "', 'SGST 0%','CGST 0%','" + jTextField40.getText() + "','" + jTextField41.getText() + "','" + jTextField42.getText() + "','" + jTextField44.getText() + "','" + freight_account + "','" + freight_gst + "','" + jTextField45.getText() + "','" + tax_type + "')");

                            }
                        } else if (isIgstApplicable == false) {

                            if ("SGST 2.5%".equals(ledgersgst.get(i))) {
                                stmt1.executeUpdate("insert into purchasereturndualgst values('" + jTextField1.getText() + "','" + Date + "',"
                                        + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                        + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                        + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                        + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                        + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','0','0','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','" + sgstamnt1_1 + "','0','" + cgstamnt1_1 + "','0','" + jTextField33.getText() + "','" + jTextField30.getText() + "','" + jTextField31.getText() + "','" + jTextField32.getText() + "','" + meterapplicable + "','" + jTextField37.getText() + "' ,'" + jTextField34.getText() + "','" + cashaccount + "','" + jTextField35.getText() + "','" + sundrydebtors + "','" + jTextField36.getText() + "','" + cardpaymentaccount + "','" + jTextField38.getText() + "','" + jTextField37.getText() + "','IGST 0%','" + (String) ledgersgst.get(i) + "','" + (String) ledgervat2.get(i) + "','" + jTextField40.getText() + "','" + jTextField41.getText() + "','" + jTextField42.getText() + "','" + jTextField44.getText() + "','" + freight_account + "','" + freight_gst + "','" + jTextField45.getText() + "','" + tax_type + "')");
                            } else if ("SGST 6%".equals(ledgersgst.get(i))) {

                                stmt1.executeUpdate("insert into purchasereturndualgst values('" + jTextField1.getText() + "','" + Date + "',"
                                        + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                        + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                        + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                        + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                        + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','0','0','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','0','" + sgstamnt1_1 + "','0','" + cgstamnt1_1 + "','" + jTextField33.getText() + "','" + jTextField30.getText() + "','" + jTextField31.getText() + "','" + jTextField32.getText() + "','" + meterapplicable + "','" + jTextField37.getText() + "' ,'" + jTextField34.getText() + "','" + cashaccount + "','" + jTextField35.getText() + "','" + sundrydebtors + "','" + jTextField36.getText() + "','" + cardpaymentaccount + "','" + jTextField38.getText() + "','" + jTextField37.getText() + "','IGST 0%','" + (String) ledgersgst.get(i) + "','" + (String) ledgervat2.get(i) + "','" + jTextField40.getText() + "','" + jTextField41.getText() + "','" + jTextField42.getText() + "','" + jTextField44.getText() + "','" + freight_account + "','" + freight_gst + "','" + jTextField45.getText() + "','" + tax_type + "')");

                            }
                        }

                        if (meterapplicable == 1) {
                            updatedquantity = quantity - Double.parseDouble(qnty.get(i) + "");
                            System.out.println("updated quantity is" + updatedquantity);

                            stmt1.executeUpdate("update Stockid2 set qnt=" + updatedquantity + " where Stock_No='" + (stock.get(i) + "").trim() + "'");
                        } else if (meterapplicable == 0) {
                            stmt1.executeUpdate("DELETE from Stockid2 where Stock_No='" + (stock.get(i) + "").trim() + "' LIMIT 1");
                        }
                        if (isIgstApplicable == true) {
                            if (Integer.parseInt(vat.get(i) + "") != 0) {
                                DecimalFormat upft = new DecimalFormat("0.00");
                                String updavat = upft.format(Double.parseDouble(updatevatcurrbal.get(i).toString()));

                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updavat + "" + "',currbal_type='" + updatevatcurrbaltype.get(i) + "" + "' where ledger_name='" + (String) ledgervat.get(i) + "" + "'");
                                System.out.println("igst update query" + "UPDATE ledger SET curr_bal='" + updavat + "" + "',currbal_type='" + updatevatcurrbaltype.get(i) + "" + "' where ledger_name='" + (String) ledgervat.get(i) + "" + "'");
                            }
                        } else if (isIgstApplicable == false) {

                            if (sgst.get(i) != null && cgst.get(i) != null) {

                                DecimalFormat upf = new DecimalFormat("0.00");
                                String updasgst = upf.format(Double.parseDouble(updatesgstcurrbal.get(i).toString()));

                                String updacgst = upf.format(Double.parseDouble(updatecgstcurrbal.get(i).toString()));
                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledgersgst.get(i) + "" + "' ");
                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledgervat2.get(i) + "" + "' ");
                                System.out.println(" update sgst" + "UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledgersgst.get(i) + "" + "' ");
                            }
                        }

                        DecimalFormat upf = new DecimalFormat("0.00");
                        String updasales = upf.format(Double.parseDouble(Salesaccountupdatecurrbal.get(i).toString()));

                        String updatesales = "\"UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'\"";

                        stmt1.executeUpdate("update ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'");
                        System.out.println(" sales query is" + "update ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'");
                        stmt1.executeUpdate("insert into queries values(" + updatesales + ") ");

                        stmt1.executeUpdate("insert into queries1 values(" + updatesales + ") ");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (jTextField34.equals("")) {
                jTextField34.setText("0");
            }

            if (Double.parseDouble(jTextField34.getText()) != 0) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    String cashaccountamount = (String) cashaccountcurrbal.get(0);
                    double cashaccountamount1 = Double.parseDouble(cashaccountamount);
                    double totalamnt = Double.parseDouble(jTextField34.getText());
                    if (cashaccountcurrbaltype.get(0).equals("CR")) {
                        double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                        if (cashaccountupdateamount > 0) {
                            cashaccountupdatecurrbaltype.add("CR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                        if (cashaccountupdateamount <= 0) {
                            cashaccountupdatecurrbaltype.add("DR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                    }
                    if (cashaccountcurrbaltype.get(0).equals("DR")) {
                        double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                        cashaccountupdatecurrbaltype.add("DR");
                        cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                    st.executeUpdate("update ledger set curr_bal='" + cashaccountupdatecurrbal.get(0) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='CASH'");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (jTextField35.getText().equals("")) {
                jTextField35.setText("0");
            }

            if (Double.parseDouble(jTextField35.getText()) != 0) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    String sundrydebtorsaccountamount = (String) sundrydebtorsaccountcurrbal.get(0);
                    double sundrydebtorsaccountamount1 = Double.parseDouble(sundrydebtorsaccountamount);
                    double totalamnt = Double.parseDouble(jTextField35.getText());
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                        double sundrydebtorsaccountupdateamount = sundrydebtorsaccountamount1 - totalamnt;
                        if (sundrydebtorsaccountupdateamount > 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add("CR");
                            sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsaccountupdateamount));
                        }
                        if (sundrydebtorsaccountupdateamount <= 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add("DR");
                            sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsaccountupdateamount));
                        }
                    }
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                        double sundrydebtorsaccountupdateamount = sundrydebtorsaccountamount1 + totalamnt;
                        sundrydebtorsaccountupdatecurrbaltype.add("DR");
                        sundrydebtorsaccountupdatecurrbal.add(Math.abs(sundrydebtorsaccountupdateamount));
                    }
                    st.executeUpdate("update ledger set curr_bal='" + sundrydebtorsaccountupdatecurrbal.get(0) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + sundrydebtors + "'");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (jTextField36.getText().equals("")) {
                jTextField36.setText("0");
            }
            if (Double.parseDouble(jTextField36.getText()) != 0) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    String cardaccountamount = (String) cardaccountcurrbal.get(0);
                    double cardaccountamount1 = Double.parseDouble(cardaccountamount);
                    double totalamnt = Double.parseDouble(jTextField36.getText());
                    if (cardaccountcurrbaltype.get(0).equals("CR")) {
                        double cardaccountupdateamount = cardaccountamount1 - totalamnt;
                        if (cardaccountupdateamount > 0) {
                            cardaccountupdatecurrbaltype.add("CR");
                            cardaccountupdatecurrbal.add(Math.abs(cardaccountupdateamount));
                        }
                        if (cardaccountupdateamount <= 0) {
                            cardaccountupdatecurrbaltype.add("DR");
                            cardaccountupdatecurrbal.add(Math.abs(cardaccountupdateamount));
                        }
                    }
                    if (cardaccountcurrbaltype.get(0).equals("DR")) {
                        double cardaccountupdateamount = cardaccountamount1 + totalamnt;
                        cardaccountupdatecurrbaltype.add("DR");
                        cardaccountupdatecurrbal.add(Math.abs(cardaccountupdateamount));
                    }
                    st.executeUpdate("update ledger set curr_bal='" + cardaccountupdatecurrbal.get(0) + "" + "',currbal_type='" + cardaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + cardpaymentaccount + "'");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (Double.parseDouble(jTextField33.getText()) != 0) {
                try {
//calculating round of value     
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stru = connect.createStatement();
                    Statement round_st = connect.createStatement();
                    ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
                    while (round_rs.next()) {
                        double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                        Round_currbal_type = round_rs.getString("currbal_type");
                        double round_total = (Double.parseDouble(jTextField33.getText()));
                        if (Round_currbal_type.equals("DR") && round_total < 0) {
                            diff_total = Round_curr_bal + Math.abs(round_total);
                        } else if (Round_currbal_type.equals("DR") && round_total > 0) {
                            diff_total = Round_curr_bal - Math.abs(round_total);

                            if (diff_total > 0) {
                                Round_currbal_type = "CR";
                                diff_total = (Math.abs(diff_total));
                            } else if (diff_total < 0) {
                                Round_currbal_type = "DR";
                                diff_total = (Math.abs(diff_total));
                            }
                        }

                        if (Round_currbal_type.equals("CR") && round_total < 0) {
                            diff_total = Round_curr_bal - Math.abs(round_total);

                            if (diff_total > 0) {
                                Round_currbal_type = "DR";
                                diff_total = (Math.abs(diff_total));
                            } else if (diff_total < 0) {
                                Round_currbal_type = "CR";
                                diff_total = (Math.abs(diff_total));
                            }

                        } else if (Round_currbal_type.equals("CR") && round_total > 0) {
                            diff_total = Round_curr_bal + Math.abs(round_total);
                        }
                        DecimalFormat dr1 = new DecimalFormat("0.00");
                        diff_total_round = dr1.format(diff_total);
                    }

                    stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round + "', currbal_type = '" + Round_currbal_type + "' where ledger_name = 'Round Off' ");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

//calculating freight charges         
            if (Double.parseDouble(jTextField44.getText()) != 0) {
                try {
                    String fc_total_round = "";
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stru = connect.createStatement();
                    Statement freight_st = connect.createStatement();
                    ResultSet freight_rs = freight_st.executeQuery("Select * from ledger where ledger_name = '" + freight_account + "'");
                    while (freight_rs.next()) {
                        double FC_curr_bal = Double.parseDouble(freight_rs.getString("curr_bal"));
                        Fc_currbal_type = freight_rs.getString("currbal_type");

                        double fc_total1 = Double.parseDouble(jTextField44.getText());

                        if (Fc_currbal_type.equals("DR")) {
                            FC_total = FC_curr_bal - fc_total1;

                            if (FC_total < 0) {
                                Fc_currbal_type = "CR";
                                FC_total = (Math.abs(FC_total));
                            } else if (FC_total > 0) {
                                Fc_currbal_type = "DR";
                                FC_total = (Math.abs(FC_total));
                            }
                        } else if (Fc_currbal_type.equals("CR")) {
                            FC_total = FC_curr_bal + fc_total1;

                            if (FC_total < 0) {
                                Fc_currbal_type = "DR";
                                FC_total = (Math.abs(FC_total));
                            } else if (FC_total > 0) {
                                Fc_currbal_type = "CR";
                                FC_total = (Math.abs(FC_total));
                            }
                        }
                        DecimalFormat dr1 = new DecimalFormat("0.00");
                        fc_total_round = dr1.format(FC_total);
                    }

                    stru.executeUpdate("Update ledger Set curr_bal = '" + fc_total_round + "', currbal_type = '" + Fc_currbal_type + "' where ledger_name = '" + freight_account + "'");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

//-------------------------------Freight With Gst-----------------------------------------
            if (Double.parseDouble(jTextField45.getText()) != 0) {
                try {
                    String fre_total_round = "";
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement fre_st = connect.createStatement();
                    ResultSet fre_rs = fre_st.executeQuery("Select * from ledger where ledger_name = 'Freight Charges'");
                    while (fre_rs.next()) {
                        double Fre_curr_bal = Double.parseDouble(fre_rs.getString("curr_bal"));
                        Fre_currbal_type = fre_rs.getString("currbal_type");

                        double frec_total1 = Double.parseDouble(jTextField45.getText());

                        if (Fre_currbal_type.equals("DR")) {
                            Fre_total = Fre_curr_bal - frec_total1;

                            if (Fre_total < 0) {
                                Fre_currbal_type = "CR";
                                Fre_total = (Math.abs(Fre_total));
                            } else if (Fre_total > 0) {
                                Fre_currbal_type = "DR";
                                Fre_total = (Math.abs(Fre_total));
                            }
                        } else if (Fre_currbal_type.equals("CR")) {
                            Fre_total = Fre_curr_bal + frec_total1;

                            if (Fre_total < 0) {
                                Fre_currbal_type = "DR";
                                Fre_total = (Math.abs(Fre_total));
                            } else if (Fre_total > 0) {
                                Fre_currbal_type = "CR";
                                Fre_total = (Math.abs(Fre_total));
                            }
                        }
                        DecimalFormat dr1 = new DecimalFormat("0.00");
                        fre_total_round = dr1.format(Fre_total);
                    }

                    fre_st.executeUpdate("Update ledger Set curr_bal = '" + fre_total_round + "', currbal_type = '" + Fre_currbal_type + "' where ledger_name = 'Freight Charges'");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//----------------------------------------------------------------------------------------

//            if (Double.parseDouble(jTextField17.getText()) != 0) {
//                double cashcurrbal = 0;
//                String Cashcurbaltype = "";
//                double disccurrbal = 0;
//                String disccurrbaltype = "";
//                double discounamntt = Double.parseDouble(jTextField17.getText());
//                try {
//                    connection c = new connection();
//                    Connection connect = c.cone();
//                    Statement statement = connect.createStatement();
//                    Statement statement1 = connect.createStatement();
//                    Statement statement2 = connect.createStatement();
//                    Statement statement3 = connect.createStatement();
//                    ResultSet rs = statement.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='CASH'");
//                    while (rs.next()) {
//                        cashcurrbal = Double.parseDouble(rs.getString(1));
//                        Cashcurbaltype = rs.getString(2);
//                    }
//                    ResultSet rs1 = statement1.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='DISCOUNT ACCOUNT'");
//                    while (rs1.next()) {
//                        disccurrbal = Double.parseDouble(rs1.getString(1));
//                        disccurrbaltype = rs1.getString(2);
//                    }
//                    switch (Cashcurbaltype) {
//                        case "DR":
//                            cashcurrbal = cashcurrbal - discounamntt;
//                            if (cashcurrbal < 0) {
//                                Cashcurbaltype = "CR";
//                                cashcurrbal = Math.abs(cashcurrbal);
//                            }
//                            break;
//                        case "CR":
//                            cashcurrbal = cashcurrbal + discounamntt;
//                            break;
//                    }
//                    switch (disccurrbaltype) {
//                        case "CR":
//                            disccurrbal = disccurrbal - discounamntt;
//                            if (disccurrbal < 0) {
//                                disccurrbaltype = "DR";
//                                disccurrbal = Math.abs(disccurrbal);
//                                System.out.println("condition one for DR discount is running ");
//                            } else {
//                                disccurrbaltype = "CR";
//                            }
//                            break;
//                        case "DR":
//                            disccurrbal = disccurrbal + discounamntt;
//                            break;
//                    }
//                    disccurrbal = Math.abs(disccurrbal);
//                    cashcurrbal = Math.abs(cashcurrbal);
//                    statement2.executeUpdate("update ledger set curr_bal='" + cashcurrbal + "',currbal_type='" + Cashcurbaltype + "' where ledger_name='CASH'");
//                    statement3.executeUpdate("update ledger set curr_bal='" + disccurrbal + "',currbal_type='" + disccurrbaltype + "' where ledger_name='DISCOUNT ACCOUNT'");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
            if (jRadioButton2.isSelected()) {
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    st.executeUpdate("insert into againstpayment values('" + jTextField1.getText() + "','','" + formatter.format(jDateChooser1.getDate()) + "','" + sundrydebtors + "','SALES','" + jTextField20.getText() + "','Sales','')");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            JOptionPane.showMessageDialog(rootPane, "Data saved successfully !!");
            int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
            if (i2 == 0) {
                jDialog5.setVisible(true);
                jTextField43.selectAll();
            }

            jDialog1.dispose();
            this.dispose();
        }
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (issave == false) {

            jDialog4.setVisible(true);
            jLabel48.setText(jTextField20.getText());
            jButton12.setVisible(false);
            if (jRadioButton1.isSelected()) {
                jTextField34.setText(jTextField20.getText());
            } else if (jRadioButton2.isSelected()) {
                jTextField35.setText(jTextField20.getText());
            } else if (jRadioButton5.isSelected()) {
                jTextField36.setText(jTextField20.getText());
            }

        }
    }//GEN-LAST:event_jButton1ActionPerformed
    public int print(Graphics g, PageFormat pf, int page)
            throws PrinterException {
        if (page > 0) {
            return NO_SUCH_PAGE;
        } else {

            Graphics2D g2d = (Graphics2D) g;
            FontMetrics fontMetrics = g2d.getFontMetrics();

            Font font = new Font(" Bill NO - SL/1718/1", Font.BOLD, 12);
            g2d.setFont(font);
          //  g2d.drawString(" ANUPAM ", 150, 50);

            Font font2 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 10);
            g2d.setFont(font2);

       //     g2d.drawString(" 36,Dalumodi Bazaar ", 150, 60);
      //      g2d.drawString(" Ratlam - -457001(M.P) ", 150, 70);
      //      g2d.drawString(" Phone No.- 07412-233051", 150, 80);

// Horizontal Line            
            g2d.drawLine(0, 85, 420, 85);

// Left Side box
            g2d.drawString("Invoice No :", 15, 100);
            g2d.drawString("Invoice Date :", 15, 120);
     //       g2d.drawString("GSTIN : 23AMDPK9264H1ZZ", 15, 140);

// Virtical Line
            g2d.drawLine(150, 85, 150, 170);

// Right Side box
            g2d.drawString("Name  :  ", 160, 100);
            g2d.drawString("Address :  ", 160, 115);
            g2d.drawString("City : ", 160, 141);
             g2d.drawString("GSTIN : ", 160, 153);
            g2d.drawString("Mobile No :  ", 160, 165);

// Horizontal Line            
            g2d.drawLine(0, 170, 420, 170);
            Font font3 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 8);
            g2d.setFont(font3);

//Table Header View
            g2d.drawString("S No", 15, 180);
//            g2d.drawString("  Particulars  ", 50, 150);
//            g2d.drawString(" Code ", 165, 150);
            g2d.drawString("HSN Code", 60, 180);
            g2d.drawString("Quantity", 165, 180);
            g2d.drawString("Rate", 270, 180);
            g2d.drawString(" Amount", 360, 180);
            g2d.drawString("Total - ", 260, 470);

            g2d.drawString("Net Amount - ", 260, 548);

// Draw Horizontal line after table
            g2d.drawLine(0, 190, 420, 190);
            g2d.drawLine(0, 460, 420, 460);
            g2d.drawLine(250, 460, 250, 555); // Verticle Line 
            g2d.drawLine(250, 538, 420, 538);
            g2d.drawLine(250, 555, 420, 555);

//For tax calculation
            g2d.drawString("GST Amount - ", 15, 470);

// For terms and Condition 
            g2d.drawString("Terms and Condition - ", 15, 515);
            g2d.drawString("1) No Exchange No Claim", 15, 525);
            g2d.drawString("2) Subject to Ratlam jurisdiction", 15, 535);
// For Authority Signatury
//            g2d.drawString("For ANUPAM :", 40, 555);
// Getting Data For printing 

// For upper left side box   
            g2d.drawString(jTextField1.getText(), 70, 100);
            String Date = formatter1.format(jDateChooser1.getDate());
            g2d.drawString(Date, 75, 120);

// For Uper Right Side Box
            if (!jTextField4.getText().isEmpty()) {
                try {
                    String address = "";
                    String mobile_no = "";
                    String city = "";
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    
                     Statement stcmpdetails = connect.createStatement();
ResultSet rscmpdetails=stcmpdetails.executeQuery("select * from cmp_detail");
while(rscmpdetails.next())
{
    g2d.setFont(font);
    
            g2d.drawString(rscmpdetails.getString("cmp_name"), 150, 50);
         g2d.setFont(font2);
            g2d.drawString(rscmpdetails.getString("add1"), 150, 60);
            g2d.drawString(rscmpdetails.getString("city")+rscmpdetails.getString("pincode"), 150, 70);
            g2d.drawString("Phone No.- "+rscmpdetails.getString("phone_no")+"", 150, 80);
            
            g2d.drawString("From : "+rscmpdetails.getString("cmp_name"), 40, 555);
            
              g2d.drawString("GSTIN : "+rscmpdetails.getString("gst_no")+"", 15, 140);
            
}
                   
                    String gstin="";
                    String address2="";
                    String address3="";
                    String address4="";
                    ResultSet rs = st.executeQuery("select * from party where party_code='" + jTextField4.getText() + "'");
                    while (rs.next()) {
                        g2d.drawString(rs.getString("party_name"), 210, 100);
                        if (rs.getString("mobile_no") != null) {
                            g2d.drawString(rs.getString("mobile_no"), 215, 165);
                        } else {
                            g2d.drawString("", 215, 165);
                        }
                        if (rs.getString("address1") != null || rs.getString("address1") !="null") {
                            address = rs.getString("address1");
                        } else {
                            address = "";
                        }
                        if (rs.getString("address2") != null|| !rs.getString("address2").equalsIgnoreCase("null")) {
                            address2 = rs.getString("address2");
                        } else {
                            address2 = "";
                        }
                        if (rs.getString("address3") != null|| !rs.getString("address3").equalsIgnoreCase("null")) {
                            address3 = rs.getString("address3");
                        } else {
                            address3 = "";
                        }
                        if (rs.getString("address4") != null|| !rs.getString("address4").equalsIgnoreCase("null")) {
                            address4 = rs.getString("address4");
                        } else {
                            address4 = "";
                        }
                        
                        
                        if (rs.getString("city") != null) {
                            city = rs.getString("city");
                        } else {
                            city = "";
                        }
                         if(rs.getString("gstin_no") !=null)
                        {
                            gstin=rs.getString("gstin_no");
                        }
                        else
                        {
                            gstin="";
                        }
                    }
                    g2d.drawString(city, 210, 141);
                    g2d.drawString(gstin, 210, 153);

                        g2d.drawString(address, 210, 115);
                   

                   
                        g2d.drawString(address2, 210, 125);
                   

                        g2d.drawString(address3, 210, 135);
                  

                    
                    //    g2d.drawString(address4, 210, 145);
                    
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (!jTextField3.getText().isEmpty()) {
                g2d.drawString(jTextField3.getText(), 210, 100);
            }
// Getting Table data
            String total_discount = jTextField17.getText();
            String net_amount = jTextField20.getText();
            String igst_first_amt = "";
            String igst_second_amt = "";
            String sgst_first_amt = "";
            String sgst_second_amt = "";
            String cgst_first_amt = "";
            String cgst_second_amt = "";
            String freight_amt = "";
            String freight_acc = "";
            try {
                int j = 10;
                int i = 1;
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                Statement st2 = connect.createStatement();
                ResultSet rs1 = st1.executeQuery("SELECT rate as rate, SUM(qnty) as qnty,(rate * SUM(qnty)) AS total,total_igst_first_amnt,total_igst_second_amnt,total_sgst_first_amnt,total_sgst_second_amnt,total_cgst_first_amnt,total_cgst_second_amnt ,description, freight_amount,freight_account FROM purchasereturndualgst where bill_refrence = '" + jTextField1.getText() + "' GROUP BY description ,rate ");
                while (rs1.next()) {

                    igst_first_amt = rs1.getString("total_igst_first_amnt");
                    igst_second_amt = rs1.getString("total_igst_second_amnt");
                    sgst_first_amt = rs1.getString("total_sgst_first_amnt");
                    sgst_second_amt = rs1.getString("total_sgst_second_amnt");
                    cgst_first_amt = rs1.getString("total_cgst_first_amnt");
                    cgst_second_amt = rs1.getString("total_cgst_second_amnt");
                    freight_amt = rs1.getString("freight_amount");
                    freight_acc = rs1.getString("freight_account");

                    ResultSet rs2 = st2.executeQuery("select * from product where product_code = '" + rs1.getString("description") + "'");
                    while (rs2.next()) {
                        g2d.drawString(rs2.getString("HSL_CODE"), 60, 195 + (j * i)); // For HSN COde
                        ismeter = rs2.getInt("ismeter");
                    }
                    g2d.drawString(i + "", 15, 195 + (j * i));  // For Serial No                          
                    g2d.drawString(rs1.getString("qnty"), 165, 195 + (j * i)); // For qunatity
                    g2d.drawString(rs1.getString("rate"), 270, 195 + (j * i)); // For rate
                    g2d.drawString(rs1.getString("total"), 360, 195 + (j * i)); // For total amount    
                    i++;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            g2d.drawString(jTextField15.getText(), 380 - fontMetrics.stringWidth(jTextField15.getText()), 470);// For Total amount
            if (!total_discount.equals("0")) {
                g2d.drawString("Discount - ", 260, 480);
                g2d.drawString(total_discount + "", 380 - fontMetrics.stringWidth(total_discount), 480);// For Discount amount
            }
            g2d.drawString(net_amount + "", 380 - fontMetrics.stringWidth(net_amount), 548);// For net amount

            if (isIgstApplicable == true) {
                if (!jTextField30.getText().equals("0")) {
                    g2d.drawString("IGST(5%) - ", 15, 485);
                    g2d.drawString(igst_first_amt + "", 60, 485);// For Total_IGST_First_Amount
                }
                if (!jTextField40.getText().equals("0")) {
                    g2d.drawString("IGST(12%) - ", 15, 500);
                    g2d.drawString(igst_second_amt + "", 60, 500);// For Total_IGST_Second_Amount
                }
                g2d.drawString("Total Igst", 260, 490);
                double total_igst = Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField40.getText());
                g2d.drawString(total_igst + "", 380 - fontMetrics.stringWidth(total_igst + ""), 490);
            } else if (isIgstApplicable == false) {
                if (!jTextField31.getText().equals("0") && !jTextField32.getText().equals("0")) {
                    g2d.drawString("SGST(2.5%) - ", 15, 485);
                    g2d.drawString("CGST(2.5%) - ", 110, 485);
                    g2d.drawString(sgst_first_amt + "", 70, 485);// For Total_SGST_First_Amount
                    g2d.drawString(cgst_first_amt + "", 170, 485);// For Total_CGST_First_Amount
                }
                if (!jTextField41.getText().equals("0") && !jTextField42.getText().equals("0")) {
                    g2d.drawString("SGST(6%) - ", 15, 500);
                    g2d.drawString("CGST(6%) - ", 110, 500);
                    g2d.drawString(sgst_second_amt + "", 70, 500);// For Total_SGST_Second_Amount
                    g2d.drawString(cgst_second_amt + "", 170, 500);// For Total_CGST_Second_Amount
                }
                g2d.drawString("Total Sgst", 260, 490);
                double total_sgst = Double.parseDouble(jTextField31.getText()) + Double.parseDouble(jTextField41.getText());
                g2d.drawString(total_sgst + "", 380 - fontMetrics.stringWidth(total_sgst + ""), 490);
                g2d.drawString("Total Cgst", 260, 500);
                double total_cgst = Double.parseDouble(jTextField32.getText()) + Double.parseDouble(jTextField42.getText());
                g2d.drawString(total_cgst + "", 380 - fontMetrics.stringWidth(total_cgst + ""), 500);
            }
            if (!jTextField33.getText().equals("0.00")) {
                g2d.drawString("Round Off", 260, 510);
                g2d.drawString(jTextField33.getText() + "", 380 - fontMetrics.stringWidth(jTextField33.getText() + ""), 510);
            }
            if (!freight_amt.equals("0")) {
                g2d.drawString("Other Amount", 260, 520);
                g2d.drawString(freight_amt + "", 380 - fontMetrics.stringWidth(freight_amt + ""), 520);
                g2d.drawString("(" + freight_acc + ")", 260, 530);
            }
            System.out.println("printing");
            return 0;
        }
    }
    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // TODO add your handling code here:
        if (jRadioButton2.isSelected()) {
            jLabel4.setVisible(true);
            jTextField4.setVisible(true);
            jTextField4.setEnabled(true);
            jDialog3.setVisible(true);
        }
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void list1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list1ActionPerformed
        // TODO add your handling code here:
        cardpaymentaccount = list1.getSelectedItem();
        jDialog2.setVisible(false);
        jLabel49.setVisible(true);
        jTextField39.setVisible(true);
    }//GEN-LAST:event_list1ActionPerformed

    private void jTextField20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField20FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField20FocusLost

    private void jTextField8FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField8FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8FocusLost

    private void jTextField12FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField12FocusLost
        // TODO add your handling code here:
        double disc_amt = Double.parseDouble(jTextField12.getText());
        double value = Double.parseDouble(jTextField9.getText());
        double total = value - disc_amt;
        jTextField13.setText("" + String.valueOf(total));
    }//GEN-LAST:event_jTextField12FocusLost


    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jTextField14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField14ActionPerformed

    private void jTextField14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField14KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_jTextField14KeyPressed

    private void jTextField15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField15KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField16.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }
    }//GEN-LAST:event_jTextField15KeyPressed

    private void jTextField16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField16KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField17.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField15.requestFocus();
        }
    }//GEN-LAST:event_jTextField16KeyPressed

    private void jTextField17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField17KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField33.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField16.requestFocus();
        }
    }//GEN-LAST:event_jTextField17KeyPressed

    private void jTextField18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField18KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField19.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jTextField18KeyPressed

    private void jTextField19KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField19KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField20.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField18.requestFocus();
        }
    }//GEN-LAST:event_jTextField19KeyPressed

    private void jTextField13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField13KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox1.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField12.requestFocus();
        }
    }//GEN-LAST:event_jTextField13KeyPressed

    private void jTextField18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField18ActionPerformed

    private void jTextField13FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField13FocusGained
//        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField13FocusGained

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField12KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField13.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField12KeyPressed

    private void jTextField11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField11KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField12.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField9.requestFocus();
        }
    }//GEN-LAST:event_jTextField11KeyPressed

    private void jTextField9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField9KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField8.requestFocus();
        }
    }//GEN-LAST:event_jTextField9KeyPressed

    private void jTextField17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField17FocusLost
        // TODO add your handling code here:
        double discount_amount = Double.parseDouble(jTextField17.getText());

        if (discount_amount == 0) {

        } else {
            DecimalFormat df = new DecimalFormat("0.00");
            double totalcgstamnt = 0;
            double totalsgstamnt = 0;
            double totalcgstamnt1 = 0;
            double totalsgstamnt1 = 0;
            double totaligstamnt = 0;
            double totaligstamnt1 = 0;
            double totalamountbeforetax = 0;
            double amountbeforetax = 0;

            double rate = 0;
            int rowcount = jTable1.getRowCount();
            for (int i = 0; i < rowcount; i++) {
                double totalamntafterbill = Double.parseDouble(jTextField15.getText());
                double discountpercentage = (discount_amount / totalamntafterbill) * 100;
                rate = Double.parseDouble(jTable1.getValueAt(i, 3) + "");
                double totalafterdiscountpercentage = (discountpercentage / 100) * Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                double totalfinal = Double.parseDouble(jTable1.getValueAt(i, 5) + "") - totalafterdiscountpercentage;

                jTable1.setValueAt(df.format(totalfinal) + "", i, 9);

                int iscalculationapplicable1 = 0;
                String igstaccount = "";
                String sgstaccount = "";
                String cgstaccount = "";
                double gstperc = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stmt1 = connect.createStatement();
                    Statement stmt2 = connect.createStatement();
                    Statement stmt3 = connect.createStatement();
                    Statement stmt4 = connect.createStatement();
                    if (isIgstApplicable == true) {
                        ResultSet rsigst = stmt1.executeQuery("select * from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rsigst.next()) {
                            igstaccount = rsigst.getString("IGST");
                            iscalculationapplicable1 = rsigst.getInt("iscalculationapplicable");
                        }
                        if (iscalculationapplicable1 == 0 || (iscalculationapplicable1 == 1 && rate > 1000)) {
                            ResultSet rs_igst = stmt2.executeQuery("SELECT  * FROM ledger where ledger_name='" + igstaccount + "'");
                            while (rs_igst.next()) {
                                gstperc = rs_igst.getDouble("percent");
                            }
                            if (jRadioButton3.isSelected()) {
                                amountbeforetax = (totalfinal / (100 + gstperc)) * 100;
                            } else if (jRadioButton4.isSelected()) {
                                amountbeforetax = (totalfinal);
                            }

                            if (gstperc == 5) {
                                double igstamnt = (gstperc * amountbeforetax) / 100;
                                totaligstamnt = igstamnt + totaligstamnt;
                                totalamountbeforetax = amountbeforetax + totalamountbeforetax;
                                System.out.println("total igst percent" + totaligstamnt);
                                jTextField30.setText(df.format(totaligstamnt) + "");
                            } else if (gstperc == 12) {
                                double igstamnt1 = (gstperc * amountbeforetax) / 100;
                                totaligstamnt1 = igstamnt1 + totaligstamnt1;
                                totalamountbeforetax = amountbeforetax + totalamountbeforetax;
                                jTextField40.setText(df.format(totaligstamnt1) + "");
                            }

                        } else if (iscalculationapplicable1 == 1) {
                            if (rate < 1000) {

                                double igst_percent = 5;
                                if (jRadioButton3.isSelected()) {
                                    amountbeforetax = (totalfinal / (100 + igst_percent)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amountbeforetax = (totalfinal);
                                }
                                double igstamnt = amountbeforetax * igst_percent / 100;
                                totaligstamnt = igstamnt + totaligstamnt;

                                jTextField30.setText(df.format(totaligstamnt) + "");
                                totalamountbeforetax = amountbeforetax + totalamountbeforetax;

                            }

                        }

                    } else if (isIgstApplicable == false) {
                        ResultSet rs_cal = stmt2.executeQuery("select * from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rs_cal.next()) {
                            iscalculationapplicable1 = rs_cal.getInt("iscalculationapplicable");
                            if ((iscalculationapplicable1 == 0) || ((iscalculationapplicable1 == 1) && (totalfinal > 1000))) {
                                ResultSet rssgst = stmt1.executeQuery("select SGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                                while (rssgst.next()) {
                                    sgstaccount = rssgst.getString(1);
                                }
                                ResultSet rscgst = stmt3.executeQuery("select CGST from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                                while (rscgst.next()) {
                                    cgstaccount = rscgst.getString(1);
                                }
                                ResultSet rsSGSTperc = stmt4.executeQuery("select percent from ledger where ledger_name ='" + sgstaccount + "'");
                                while (rsSGSTperc.next()) {
                                    gstperc = rsSGSTperc.getDouble(1);
                                }
                            } else if ((iscalculationapplicable1 == 1) && (rate) < 1000) {
                                gstperc = 2.5;
                            }
                        }
                        if (jRadioButton3.isSelected()) {
                            amountbeforetax = (totalfinal / (100 + gstperc + gstperc)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amountbeforetax = (totalfinal);
                        }
                        if (gstperc == 2.5) {

                            double sgst_amt = (amountbeforetax * gstperc) / 100;
                            double cgst_amt = ((amountbeforetax * gstperc) / 100);

                            totalsgstamnt = totalsgstamnt + sgst_amt;
                            totalcgstamnt = totalcgstamnt + cgst_amt;
                            totalamountbeforetax = amountbeforetax + totalamountbeforetax;
                            jTextField31.setText("" + df.format(totalsgstamnt));
                            jTextField32.setText("" + df.format(totalcgstamnt));

                        } else if (gstperc == 6) {

                            double sgst_amnt1 = (amountbeforetax * gstperc) / 100;
                            double cgst_amnt1 = ((amountbeforetax * gstperc) / 100);

                            totalsgstamnt1 = totalsgstamnt1 + sgst_amnt1;
                            totalcgstamnt1 = totalcgstamnt1 + cgst_amnt1;
                            totalamountbeforetax = amountbeforetax + totalamountbeforetax;
                            jTextField41.setText("" + df.format(totalsgstamnt1));
                            jTextField42.setText("" + df.format(totalcgstamnt1));
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            jTextField19.setText(df.format(totalamountbeforetax) + "");
            jTextField20.setText(df.format(Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField40.getText()) + Double.parseDouble(jTextField31.getText()) + Double.parseDouble(jTextField41.getText()) + Double.parseDouble(jTextField32.getText()) + Double.parseDouble(jTextField42.getText()) + Double.parseDouble(jTextField33.getText()) + Double.parseDouble(jTextField44.getText())));
            jTextField18.setText(df.format(Double.parseDouble(jTextField15.getText()) - (discount_amount)) + "");
        }


    }//GEN-LAST:event_jTextField17FocusLost
    int row = 0;
    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_DELETE) {
            DefaultTableModel dtm = null;
            dtm = (DefaultTableModel) jTable1.getModel();
            row = jTable1.getSelectedRow();
            double totalamt1 = Double.parseDouble((String) jTable1.getValueAt(row, 5));
            double nettotal = Double.parseDouble(jTextField15.getText());
            double nettotal1 = nettotal - totalamt1;
            jTextField15.setText("" + nettotal1);
            totalamount = totalamount - totalamt1;
            totalamountaftertax = totalamountaftertax - totalamt1;
            iv = 0;
            iv1 = 0;
            sv = 0;
            sv1 = 0;
            cv = 0;
            cv1 = 0;
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement sttt = connect.createStatement();
                Statement sttt1 = connect.createStatement();
                Statement pro_st = connect.createStatement();
                Statement sgst_st = connect.createStatement();
                Statement cgst_st = connect.createStatement();

                if (isIgstApplicable == true) {
                    ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "" + "'");
                    while (rsss.next()) {
                        iscalculationapplicable = rsss.getInt("iscalculationapplicable");
                        double vat = 0;
                        double igst_percent = 0;
                        double value1 = 0;

                        if ((iscalculationapplicable == 0) || (iscalculationapplicable == 1 && Double.parseDouble(jTable1.getValueAt(row, 3) + "") >= 1000)) {
                            ResultSet rssss = sttt1.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");

                            while (rssss.next()) {
                                igst_percent = Double.parseDouble(rssss.getString("percent"));
                            }
                        } else if ((iscalculationapplicable == 1 && Double.parseDouble(jTable1.getValueAt(row, 3) + "") < 1000)) {
                            igst_percent = 5;
                        }
                        value1 = Double.parseDouble(jTable1.getValueAt(row, 5) + "");
                        if (jRadioButton3.isSelected()) {
                            amntbeforetax = (value1 / (100 + igst_percent)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amntbeforetax = value1;
                        }
                        DecimalFormat df = new DecimalFormat("0.00");
                        if (igst_percent == 5) {
                            iv = (amntbeforetax * igst_percent) / 100;
                            tiv = tiv - iv;
                            jTextField30.setText("" + df.format(tiv));
                        } else if (igst_percent == 12) {
                            iv1 = (amntbeforetax * igst_percent) / 100;
                            tiv1 = tiv1 - iv1;
                            jTextField40.setText("" + df.format(tiv1));
                        }

                    }

                    totalamountbeforetax = (totalamountbeforetax - amntbeforetax);

                    DecimalFormat df1 = new DecimalFormat("0.00");
                    totalamountaftertax = totalamountbeforetax + tiv + tiv1 + Double.parseDouble(jTextField44.getText());
                    jTextField20.setText("" + df1.format(totalamountaftertax));
                    double value_before_tax = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText());
                    jTextField19.setText("" + df1.format(value_before_tax));

                } else if (isIgstApplicable == false) {
                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "'");
                    while (pro_rs.next()) {
                        iscalculationapplicable = pro_rs.getInt("iscalculationapplicable");
                        if ((iscalculationapplicable == 0) || (iscalculationapplicable == 1 && Double.parseDouble(jTable1.getValueAt(row, 3) + "") >= 1000)) {
                            ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                            while (sgst_rs.next()) {
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }

                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }
                        } else if (iscalculationapplicable == 1 && Double.parseDouble(jTable1.getValueAt(row, 3) + "") < 1000) {
                            sgstpercent = 2.5;
                            cgstpercent = 2.5;
                        }

                        DecimalFormat df = new DecimalFormat("0.00");
                        totalpercent = sgstpercent + cgstpercent;
                        if (jRadioButton3.isSelected()) {
                            amntbeforetax = (Double.parseDouble(jTable1.getValueAt(row, 5) + "") / (100 + totalpercent)) * 100;
                        } else if (jRadioButton4.isSelected()) {
                            amntbeforetax = Double.parseDouble(jTable1.getValueAt(row, 5) + "");
                        }
                        if (totalpercent == 5) {

                            sv = (amntbeforetax * sgstpercent) / 100;
                            cv = ((amntbeforetax * cgstpercent) / 100);

                            tsv = tsv - sv;
                            tcv = tcv - cv;

                            jTextField31.setText("" + df.format(tsv));
                            jTextField32.setText("" + df.format(tcv));

                        } else if (totalpercent == 12) {

                            sv1 = (amntbeforetax * sgstpercent) / 100;
                            cv1 = ((amntbeforetax * cgstpercent) / 100);

                            tsv1 = tsv1 - sv1;
                            tcv1 = tcv1 - cv1;

                            jTextField41.setText("" + df.format(tsv1));
                            jTextField42.setText("" + df.format(tcv1));
                        }
                    }

                    totalamountbeforetax = (totalamountbeforetax - amntbeforetax);

                    DecimalFormat df1 = new DecimalFormat("0.00");
                    totalamountaftertax = totalamountbeforetax + tsv + tcv + tsv1 + tcv1 + Double.parseDouble(jTextField44.getText());
                    jTextField20.setText("" + df1.format(totalamountaftertax));
                    double value_before_tax = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText());
                    jTextField19.setText("" + df1.format(value_before_tax));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            double qnty = 0;
            double qntytotal = 0;
            qnty = Double.parseDouble((String) jTable1.getValueAt(row, 4));
            qntytotal = Double.parseDouble(jTextField16.getText());
            qntytotal = qntytotal - qnty;
            totalquantity = totalquantity - qnty;
            jTextField16.setText("" + qntytotal);

            jTextField18.setText((Double.parseDouble(jTextField15.getText()) - Double.parseDouble(jTextField17.getText())) + "");

// Net amount after Freight Charges            
            double netamnt = Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField40.getText()) + Double.parseDouble(jTextField31.getText()) + Double.parseDouble(jTextField41.getText()) + Double.parseDouble(jTextField32.getText()) + Double.parseDouble(jTextField42.getText());
            double total_netamnt = netamnt + Double.parseDouble(jTextField44.getText());
            jTextField20.setText("" + total_netamnt);
            int rowcount = dtm.getRowCount();

            for (int i = row + 1; i < rowcount; i++) {
                int row1 = row + 2;
                jTable1.getModel().setValueAt(--row1, i, 0);
                row++;
            }
            int a = Integer.parseInt(jTextField14.getText());
            a = a - 1;
            jTextField14.setText("" + a);

            row = jTable1.getSelectedRow();
            dtm.removeRow(row);
            dtm.fireTableDataChanged();

            int rowcount1 = jTable1.getRowCount();
            if (rowcount1 == 0) {
                jTextField17.setText("0");
                jTextField15.setText("0");
                jTextField18.setText("0");
                jTextField20.setText("0");
                jTextField19.setText("0");
                jTextField30.setText("0.00");
                jTextField31.setText("0.00");
                jTextField32.setText("0.00");
                jTextField33.setText("0.00");
                jTextField40.setText("0.00");
                jTextField41.setText("0.00");
                jTextField42.setText("0.00");
            }
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTextField19FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField19FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField19FocusLost

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        jDialog1.hide();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        jDialog1.dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jComboBox1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1FocusGained

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jComboBox1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        // TODO add your handling code here:
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_RIGHT) {
            jButton1.requestFocus();
        }
    }//GEN-LAST:event_jButton2KeyPressed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        dispose();
        new New_Dual_PurchaseReturn().setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
        if (i2 == 0) {
            jDialog5.setVisible(true);
            jTextField43.selectAll();
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    double s1 = 0;
    double cashamount = 0;
    String cashaccount = "";
    double debtoramount = 0;
    String debtoraccount = "";
    double cardamount = 0;
    String cardupdateaccount = "";
    double freightamount = 0;
    double old_freight = 0;
    double v1 = 0;
    double round1 = 0;
    double reducingtax = 0;
    String Party_ledger = "";
    ArrayList stock1 = new ArrayList();
    ArrayList vat1 = new ArrayList();
    ArrayList ledgervat1 = new ArrayList();
    ArrayList sgst1 = new ArrayList();
    ArrayList ledgersgst1 = new ArrayList();
    ArrayList cgst1 = new ArrayList();
    ArrayList ledgercgst1 = new ArrayList();
    int ismeterupdate = 0;
    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        jDialog4.setVisible(true);
        jButton9.setVisible(false);
        jButton12.setVisible(true);
        jLabel48.setText(jTextField20.getText());
        if (jRadioButton1.isSelected()) {
            jTextField34.setText(jTextField20.getText());
        } else if (jRadioButton2.isSelected()) {
            jTextField35.setText(jTextField20.getText());
        } else if (jRadioButton5.isSelected()) {
            jTextField36.setText(jTextField20.getText());
        }

    }//GEN-LAST:event_jButton7ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2KeyPressed

    private void jTextField17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField17ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField17ActionPerformed

    private void jTextField26CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField26CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();

        String party = jTextField26.getText();
        if (party.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party where party_code  LIKE '%" + party + "%'");

                while (rs.next()) {

                    String igst = "";
                    if (rs.getInt("igst") == 1) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};

                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);

            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField26CaretUpdate

    private void jTextField26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField26ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField26ActionPerformed

    private void jTextField26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField26KeyPressed

    }//GEN-LAST:event_jTextField26KeyPressed

    private void jTextField27CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField27CaretUpdate
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String partyName = jTextField27.getText();
        if (partyName.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party where party_name LIKE'%" + partyName + "%'");
                while (rs.next()) {

                    String igst = "";
                    if (rs.getInt("igst") == 1) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }

        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField27CaretUpdate

    private void jTextField27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField27ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField27ActionPerformed

    private void jTextField27KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField27KeyPressed

        int key = evt.getKeyCode();

        if (key == evt.VK_DOWN) {
            jTable3.requestFocus();

        }
    }//GEN-LAST:event_jTextField27KeyPressed

    private void jTextField28CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField28CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String city = jTextField28.getText();
        if (city.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party where city LIKE '%" + city + "%'");
                while (rs.next()) {

                    String igst = "";
                    if (rs.getInt("igst") == 1) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);
                sqe.printStackTrace();

            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField28CaretUpdate

    private void jTextField28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField28KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField28KeyPressed

    private void jTextField29CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField29CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String state = jTextField29.getText();
        System.out.println("state is" + state);
        if (state.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party where state LIKE '%" + state + "%'");
                while (rs.next()) {
                    String igst = "";
                    if (rs.getInt("igst") == 1) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }
                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }
            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField29CaretUpdate

    private void jTextField29KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField29KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField29KeyPressed

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        partyselect();
        partyChange();
    }//GEN-LAST:event_jTable3MouseClicked

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            partyselect();
        }

        if (key == evt.VK_BACK_SPACE) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_LEFT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_RIGHT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_SPACE) {
            jTextField18.requestFocus();
        }
    }//GEN-LAST:event_jTable3KeyPressed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        master.customer.Add_Purchase_Party addnew = new master.customer.Add_Purchase_Party();
        addnew.setVisible(true);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jTextField35KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField35KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog3.setVisible(true);
        }

        if (key == evt.VK_ENTER) {
            double netamount = Double.parseDouble(jTextField20.getText());
            double cashamount = Double.parseDouble(jTextField34.getText());
            double creditamount = Double.parseDouble(jTextField35.getText());
            if ((cashamount + creditamount) < netamount) {
                jTextField36.setText("" + (netamount - (cashamount + creditamount)));
                jTextField36.requestFocus();
            } else if ((cashamount + creditamount) == netamount) {
                jButton9.requestFocus();
            }

        }

    }//GEN-LAST:event_jTextField35KeyPressed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        if ((Double.parseDouble(jTextField35.getText())) != 0 && sundrydebtors == null) {
            JOptionPane.showMessageDialog(rootPane, "lease select sundry debtors account");
        } else if ((Double.parseDouble(jTextField36.getText())) != 0 && cardpaymentaccount == null) {
            JOptionPane.showMessageDialog(rootPane, "Please select Bank Account");
        } else if ((Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) < Double.parseDouble(jTextField20.getText()) || (Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) > Double.parseDouble(jTextField20.getText())) {
            JOptionPane.showMessageDialog(rootPane, "The Amount Distributed is not equal to net amount please rectify ");
        } else {
            jDialog4.dispose();
            save();
        }


    }//GEN-LAST:event_jButton9ActionPerformed

    private void jTextField36KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField36KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog2.setVisible(true);
        }

    }//GEN-LAST:event_jTextField36KeyPressed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        jDialog2.setVisible(true);
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jTextField34KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField34KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_ENTER) {
            double netamount = Double.parseDouble(jTextField20.getText());
            double cashamount = Double.parseDouble(jTextField34.getText());
            if (cashamount < netamount) {
                jTextField35.setText("" + (netamount - cashamount));
                jTextField35.requestFocus();
            } else if (cashamount == netamount) {
                jButton9.requestFocus();
            }

        }
    }//GEN-LAST:event_jTextField34KeyPressed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        jDialog3.setVisible(true);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton9KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            if ((Double.parseDouble(jTextField35.getText())) != 0 && sundrydebtors == null) {
                JOptionPane.showMessageDialog(rootPane, "lease select sundry debtors account");
            } else if ((Double.parseDouble(jTextField36.getText())) != 0 && cardpaymentaccount == null) {
                JOptionPane.showMessageDialog(rootPane, "Please select Bank Account");
            } else if ((Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) < Double.parseDouble(jTextField20.getText()) || (Double.parseDouble(jTextField34.getText()) + Double.parseDouble(jTextField35.getText()) + Double.parseDouble(jTextField36.getText())) > Double.parseDouble(jTextField20.getText())) {
                JOptionPane.showMessageDialog(rootPane, "The Amount Distributed is not equal to net amount please rectify ");
            } else {
                jDialog4.dispose();
                save();
            }

        }

    }//GEN-LAST:event_jButton9KeyPressed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Update");
        if (i2 == 0) {

            double discounamntt = 0;
            try {
                String paytype = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                ResultSet rs = st1.executeQuery("select * from purchasereturndualgst where bill_refrence='" + jTextField1.getText() + "'");
                while (rs.next()) {
                    paytype = rs.getString(3);
//                    c1 = Double.parseDouble(rs.getString("Net_Total"));
                    cashamount = Double.parseDouble(rs.getString("cashamount"));
                    cashaccount = rs.getString("cashaccount");
                    debtoramount = Double.parseDouble(rs.getString("sundrydebtorsamount"));
                    debtoraccount = rs.getString("sundrydebtorsaccount");
                    cardamount = Double.parseDouble(rs.getString("cardamount"));
                    cardupdateaccount = rs.getString("cardaccount");
                    ismeterupdate = rs.getInt("ismeter");
                    freightamount = rs.getDouble("freight_amount");

                    if (rs.getString("Round_OFf_Value").equals("null") || rs.getString("Round_OFf_Value").equals("") || rs.getString("Round_OFf_Value") == null) {
                        round1 = 0;
                    } else {
                        round1 = Double.parseDouble(rs.getString("Round_OFf_Value"));
                    }

                    Party_ledger = rs.getString("by_ledger");
                    reducingtax = rs.getDouble("total_value_before_tax");
                    discounamntt = Double.parseDouble(rs.getString("total_disc_amnt"));

                    Statement stmt3 = connect.createStatement();

                    if (ismeterupdate == 0) {
                        ResultSet rsstock = stmt3.executeQuery("select * from stockid where Stock_No='" + rs.getString(7) + "'");
                        while (rsstock.next()) {
                            if (rsstock.getString(2) != null) {
                                Statement stmt4 = connect.createStatement();
                                stmt4.executeUpdate("insert into stockid2 values('" + rsstock.getString(2) + "','" + rsstock.getString(3) + "','" + rsstock.getString(4) + "','" + rsstock.getString(5) + "','" + rsstock.getString(6) + "','" + rsstock.getString(7) + "','" + rsstock.getString(8) + "','" + rsstock.getString(11) + "','" + rsstock.getString(10) + "','" + rsstock.getString(9) + "','" + rsstock.getString(13) + "','" + ismeterupdate + "','1')");
                            }
                        }
                    } else if (ismeterupdate == 1) {
                        double qntstock = 0;
                        double qnt = 0;
                        double totalqnty = 0;
                        ResultSet rsstock = stmt3.executeQuery("select qnt from stockid2 where Stock_No='" + rs.getString(7) + "'");
                        while (rsstock.next()) {
                            qntstock = rsstock.getDouble(1);

                        }
                        qnt = rs.getDouble("qnty");
                        totalqnty = qnt + qntstock;

                        stmt3.executeUpdate("update stockid2 set qnt='" + totalqnty + "" + "' where stock_no='" + rs.getString(8) + "'");

                    }

                    {
                        ledgervat1.add(rs.getString("igst_account"));
                        Statement stmt1 = connect.createStatement();
                        Statement stmtvataccount = connect.createStatement();
                        ResultSet rsvataccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("igst_account") + "' ");
                        while (rsvataccount.next()) {
                            vat1.add(rsvataccount.getString(6));
                            vatcurrbal.add(rsvataccount.getString(10));
                            vatcurrbaltype.add(rsvataccount.getString(11));

                            if (ledgervat1.get(0).equals("IGST 5%")) {
                                vatamnt = rs.getDouble("igst_first");
                            } else if (ledgervat1.get(0).equals("IGST 12%")) {
                                vatamnt = rs.getDouble("igst_second");
                            }

                            System.out.println("igst vatamnt" + vatamnt);

                            DecimalFormat idf = new DecimalFormat("0.00");
                            igstamnt1_1 = idf.format(vatamnt);

                            String vatcurrbalance = (String) vatcurrbal.get(0);
                            String Curbaltypevat = (String) vatcurrbaltype.get(0);
                            double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                            double vatamnt1 = vatamnt;
                            double result;

                            if (Curbaltypevat.equals("CR")) {
                                result = vatcurrbalance1 - vatamnt1;
                                if (result >= 0) {
                                    updatevatcurrbaltype.add(0, "CR");
                                    updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatevatcurrbaltype.add(0, "DR");
                                    updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypevat.equals("DR")) {

                                result = vatcurrbalance1 + vatamnt1;

                                updatevatcurrbaltype.add(0, "DR");
                                updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Double.parseDouble(vat1.get(0) + "") != 0) {
                                DecimalFormat upvat = new DecimalFormat("0.00");
                                String updavat = upvat.format(Double.parseDouble(updatevatcurrbal.get(0).toString()));
                                System.out.println("Igst reverse balance-- " + updavat);
                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updavat + "" + "',currbal_type='" + updatevatcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgervat1.get(0) + "" + "'");
                            }
                            vat1.clear();
                            vatcurrbal.clear();
                            vatcurrbaltype.clear();
                            updatevatcurrbal.clear();
                            updatevatcurrbaltype.clear();
                        }
                    }

// For SGST calculation
                    {
                        ledgersgst1.add(rs.getString("sgst_account"));
                        Statement stmt_sgst = connect.createStatement();
                        Statement stmtsgstaccount = connect.createStatement();
                        ResultSet rssgstaccount = stmtsgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("sgst_account") + "' ");
                        while (rssgstaccount.next()) {
                            sgst1.add(rssgstaccount.getString(6));
                            sgstcurrbal.add(rssgstaccount.getString(10));
                            sgstcurrbaltype.add(rssgstaccount.getString(11));

                            sgstperc = Double.parseDouble((String) sgst1.get(0));

                            if (ledgersgst1.get(0).equals("SGST 2.5%")) {
                                sgstamnt = rs.getDouble("sgst_first");
                            } else if (ledgersgst1.get(0).equals("SGST 5%")) {
                                sgstamnt = rs.getDouble("sgst_second");
                            }
                            DecimalFormat idf = new DecimalFormat("0.00");
                            sgstamnt1_1 = idf.format(sgstamnt);

                            String sgstcurrbalance = (String) sgstcurrbal.get(0);
                            String Curbaltypesgst = (String) sgstcurrbaltype.get(0);
                            double sgstcurrbalance1 = Double.parseDouble(sgstcurrbalance);
                            double sgstamnt1 = sgstamnt;
                            double result;

                            if (Curbaltypesgst.equals("CR")) {
                                result = sgstcurrbalance1 - sgstamnt1;
                                if (result >= 0) {
                                    updatesgstcurrbaltype.add(0, "CR");
                                    updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatesgstcurrbaltype.add(0, "DR");
                                    updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypesgst.equals("DR")) {

                                result = sgstcurrbalance1 + sgstamnt1;

                                updatesgstcurrbaltype.add(0, "DR");
                                updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Double.parseDouble(sgst1.get(0) + "") != 0) {
                                DecimalFormat upsgst = new DecimalFormat("0.00");
                                String updasgst = upsgst.format(Double.parseDouble(updatesgstcurrbal.get(0).toString()));
                                stmt_sgst.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgersgst1.get(0) + "" + "'");
                            }

                            sgst1.clear();
                            sgstcurrbal.clear();
                            sgstcurrbaltype.clear();
                            updatesgstcurrbal.clear();
                            updatesgstcurrbaltype.clear();

                        }
                    }

// For CGST calculation
                    {
                        ledgercgst1.add(rs.getString("cgst_account"));
                        Statement stmt_cgst = connect.createStatement();
                        Statement stmtcgstaccount = connect.createStatement();
                        ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("cgst_account") + "' ");
                        while (rscgstaccount.next()) {
                            cgst1.add(rscgstaccount.getString(6));
                            cgstcurrbal.add(rscgstaccount.getString(10));
                            cgstcurrbaltype.add(rscgstaccount.getString(11));

                            if (ledgercgst1.get(0).equals("CGST 2.5%")) {
                                cgstamnt = rs.getDouble("cgst_first");
                            } else if (ledgercgst1.get(0).equals("CGST 5%")) {
                                cgstamnt = rs.getDouble("cgst_first");
                            }

                            DecimalFormat idf = new DecimalFormat("0.00");
                            cgstamnt1_1 = idf.format(cgstamnt);

                            String cgstcurrbalance = (String) cgstcurrbal.get(0);
                            String Curbaltypecgst = (String) cgstcurrbaltype.get(0);
                            double cgstcurrbalance1 = Double.parseDouble(cgstcurrbalance);
                            double cgstamnt1 = cgstamnt;
                            double result;

                            if (Curbaltypecgst.equals("CR")) {
                                result = cgstcurrbalance1 - cgstamnt1;
                                if (result >= 0) {
                                    updatecgstcurrbaltype.add(0, "CR");
                                    updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatecgstcurrbaltype.add(0, "DR");
                                    updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypecgst.equals("DR")) {

                                result = cgstcurrbalance1 + cgstamnt1;

                                updatecgstcurrbaltype.add(0, "DR");
                                updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Double.parseDouble(cgst1.get(0) + "") != 0) {

                                DecimalFormat upcgst = new DecimalFormat("0.00");
                                String updacgst = upcgst.format(Double.parseDouble(updatecgstcurrbal.get(0).toString()));
                                stmt_cgst.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgercgst1.get(0) + "" + "'");
                            }

                            cgst1.clear();
                            cgstcurrbal.clear();
                            cgstcurrbaltype.clear();
                            updatecgstcurrbal.clear();
                            updatecgstcurrbaltype.clear();
                        }
                    }
                }
                Statement Salesaccount = connect.createStatement();

                ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + Party_ledger + "' ");
                while (rssales.next()) {
                    Salesaccountcurrbal.add(rssales.getString(10));
                    Salesaccountcurrbaltype.add(rssales.getString(11));
                }

                double totalamntreducingvat = reducingtax;

                String salesaccountamnt = (String) Salesaccountcurrbal.get(0);
                double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
                double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);

                if (Salesaccountcurrbaltype.get(0).equals("CR")) {
                    if (salesaccountupdatedamount > 0) {
                        Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                        Salesaccountupdatecurrbaltype.add("CR");
                    }
                    if (salesaccountupdatedamount <= 0) {
                        Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                        Salesaccountupdatecurrbaltype.add("DR");
                    }
                } else if (Salesaccountcurrbaltype.get(0).equals("DR")) {
                    salesaccountupdatedamount = salesaccountamnt1 + totalamntreducingvat;
                    Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                    Salesaccountupdatecurrbaltype.add("DR");
                }

                Statement stmt2 = connect.createStatement();
                DecimalFormat upsales = new DecimalFormat("0.00");
                String updasales = upsales.format(Double.parseDouble(Salesaccountupdatecurrbal.get(0).toString()));
                stmt2.executeUpdate("UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + Party_ledger + "'");

                Salesaccountcurrbal.clear();
                Salesaccountcurrbaltype.clear();
                Salesaccountupdatecurrbal.clear();
                Salesaccountupdatecurrbaltype.clear();
//                }

                if (cashamount != 0) {

                    String c2 = "CASH";
                    Statement Cashaccount = connect.createStatement();
                    ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='" + cashaccount + "'");
                    while (rscashaccount.next()) {
                        cashaccountcurrbal.add(rscashaccount.getString(10));
                        cashaccountcurrbaltype.add(rscashaccount.getString(11));
                    }

                    System.out.println("CODE IS RUNNING FOR CASH AND SALE UPDATE");

                    jTextField4.setText(null);
                    if (jRadioButton1.isSelected()) {
                        paytype = jRadioButton1.getLabel();
                    } else if (jRadioButton2.isSelected()) {
                        paytype = jRadioButton2.getLabel();
                    } else if (jRadioButton5.isSelected()) {
                        paytype = jRadioButton5.getLabel();
                    }

                    String cashaccountamount = (String) cashaccountcurrbal.get(0);
                    double totalamnt = cashamount;
                    double cashaccountamount1 = Double.parseDouble(cashaccountamount);

                    if (cashaccountcurrbaltype.get(0).equals("DR")) {
                        double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                        if (cashaccountupdateamount > 0) {
                            cashaccountupdatecurrbaltype.add("DR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                        if (cashaccountupdateamount <= 0) {
                            cashaccountupdatecurrbaltype.add("CR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                    }
                    if (cashaccountcurrbaltype.get(0).equals("CR")) {
                        double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                        cashaccountupdatecurrbaltype.add("CR");
                        cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                    Statement stmt1 = connect.createStatement();
                    DecimalFormat dfcash = new DecimalFormat("0.00");
                    stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfcash.format(cashaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + c2 + "'");

                    double disccurrbal = 0;
                    String disccurrbaltype = "";

                    try {
                        Statement statement1 = connect.createStatement();
                        Statement statement3 = connect.createStatement();

                        ResultSet rs1 = statement1.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='DISCOUNT ACCOUNT'");

                        while (rs1.next()) {
                            disccurrbal = Double.parseDouble(rs1.getString(1));
                            disccurrbaltype = rs1.getString(2);
                        }

                        if (disccurrbaltype.equals("DR")) {
                            disccurrbal = disccurrbal - discounamntt;
                            if (disccurrbal < 0) {
                                disccurrbaltype = "CR";
                                disccurrbal = Math.abs(disccurrbal);
                            }
                        } else if (disccurrbaltype.equals("CR")) {
                            disccurrbal = disccurrbal + discounamntt;
                        }

                        disccurrbal = Math.abs(disccurrbal);
                        statement3.executeUpdate("update ledger set curr_bal='" + disccurrbal + "',currbal_type='" + disccurrbaltype + "' where ledger_name = 'DISCOUNT ACCOUNT'");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                if (debtoramount != 0) {
                    Statement sundrydebtorsaccount = connect.createStatement();
                    ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + debtoraccount + "'");
                    while (rssundrydebtors.next()) {
                        sundrydebtorsaccountcurrbal.add(rssundrydebtors.getString(10));
                        sundrydebtorsaccountcurrbaltype.add(rssundrydebtors.getString(11));
                    }

                    String sundrydebtorsamnt = (String) sundrydebtorsaccountcurrbal.get(0);
                    double totalamnt = debtoramount;
//                    double totalamntreducingvat = totalamnt - vatamnt;
                    double sundrydebtorsamnt1 = Double.parseDouble(sundrydebtorsamnt);
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                        double sundrydebtorsccountupdateamount = sundrydebtorsamnt1 - totalamnt;

                        if (sundrydebtorsccountupdateamount > 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add(0, "DR");
                            sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                        }
                        if (sundrydebtorsccountupdateamount <= 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                            sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                        }
                    }
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                        double cashaccountupdateamount = sundrydebtorsamnt1 + totalamnt;
                        sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                        sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(cashaccountupdateamount));
                    }

                    DecimalFormat dfsundry = new DecimalFormat("0.00");
                    sundrydebtorsaccount.executeUpdate("UPDATE ledger SET curr_bal='" + dfsundry.format(sundrydebtorsaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + debtoraccount + "'");

                }

                if (cardamount != 0) {
                    Statement cardaccount = connect.createStatement();
                    ResultSet rscard = cardaccount.executeQuery("select * from ledger where  ledger_name='" + cardupdateaccount + "'");
                    while (rscard.next()) {
                        cardaccountcurrbal.add(rscard.getString(10));
                        cardaccountcurrbaltype.add(rscard.getString(11));
                    }

                    String cardamnt = (String) cardaccountcurrbal.get(0);
                    double totalamnt = cardamount;
//                    double totalamntreducingvat = totalamnt - vatamnt;
                    double cardamnt1 = Double.parseDouble(cardamnt);
                    if (cardaccountcurrbaltype.get(0).equals("DR")) {
                        double cardaccountupdateamount = cardamnt1 - totalamnt;

                        if (cardaccountupdateamount > 0) {
                            cardaccountupdatecurrbaltype.add(0, "DR");
                            cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                        }
                        if (cardaccountupdateamount <= 0) {
                            cardaccountupdatecurrbaltype.add(0, "CR");
                            cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                        }
                    }
                    if (cardaccountcurrbaltype.get(0).equals("CR")) {
                        double cardaccountupdateamount = cardamnt1 + totalamnt;
                        cardaccountupdatecurrbaltype.add(0, "CR");
                        cardaccountupdatecurrbal.add(0, Math.abs(cardaccountupdateamount));
                    }

//                    Statement stmt2 = connect.createStatement();
                    DecimalFormat dfsundry = new DecimalFormat("0.00");
                    cardaccount.executeUpdate("UPDATE ledger SET curr_bal='" + dfsundry.format(cardaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + cardaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + cardupdateaccount + "'");
                }

// Calculation for Round off
                String Round_currbal_type1 = "";
                double diff_total1 = 0;
                String diff_total_round1 = "";
                Statement stru = connect.createStatement();
                Statement round_st = connect.createStatement();
                ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
                while (round_rs.next()) {
                    double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                    Round_currbal_type1 = round_rs.getString("currbal_type");
                    double round_total = round1;

                    if (Round_currbal_type1.equals("DR") && round_total < 0) {
                        diff_total1 = Round_curr_bal - round_total;
                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    } else if (Round_currbal_type1.equals("DR") && round_total > 0) {
                        diff_total1 = Round_curr_bal + round_total;
                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    }

                    if (Round_currbal_type1.equals("CR") && round_total < 0) {
                        diff_total1 = Round_curr_bal + round_total;

                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }

                    } else if (Round_currbal_type1.equals("CR") && round_total > 0) {
                        diff_total1 = Round_curr_bal - round_total;

                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    }
                    DecimalFormat dr1 = new DecimalFormat("0.00");
                    diff_total_round1 = dr1.format(diff_total1);
                }

                //Updating round off ledger in ledger table       
                stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round1 + "', currbal_type = '" + Round_currbal_type1 + "' where ledger_name = 'Round Off'");

// Updating Freight account
                if (freightamount != 0) {
                    String fc_total_round = "";
                    String Fc_currbal_type1 = "";
                    double FC_total1 = 0;
                    Statement freight_charges = connect.createStatement();
                    ResultSet fc_rs = freight_charges.executeQuery("Select * From ledger where ledger_name = '" + freight_account + "'");
                    while (fc_rs.next()) {
                        double FC_curr_bal = Double.parseDouble(fc_rs.getString("curr_bal"));
                        Fc_currbal_type1 = fc_rs.getString("currbal_type");

                        double fc_total1 = Double.parseDouble(freightamount + "");

                        if (Fc_currbal_type1.equals("DR")) {
                            FC_total1 = FC_curr_bal + fc_total1;

                            if (FC_total1 <= 0) {
                                Fc_currbal_type1 = "CR";
                                FC_total1 = (Math.abs(FC_total1));
                            } else if (FC_total1 > 0) {
                                Fc_currbal_type1 = "DR";
                                FC_total1 = (Math.abs(FC_total1));
                            }
                        }

                        if (Fc_currbal_type1.equals("CR")) {
                            FC_total1 = FC_curr_bal - fc_total1;

                            if (FC_total1 <= 0) {
                                Fc_currbal_type1 = "DR";
                                FC_total1 = (Math.abs(FC_total1));
                            } else if (FC_total1 > 0) {
                                Fc_currbal_type1 = "CR";
                                FC_total1 = (Math.abs(FC_total1));
                            }
                        }
                        DecimalFormat dr1 = new DecimalFormat("0.00");
                        fc_total_round = dr1.format(FC_total1);
                    }
                    freight_charges.executeUpdate("Update ledger Set curr_bal = '" + fc_total_round + "', currbal_type = '" + Fc_currbal_type1 + "' where ledger_name = '" + freight_account + "'");

                }
//---------------------Reversing Freight With Gst-----------------------------------------------------
                if (old_freight != 0) {
                    String fre_total_round = "";
                    String Fre_currbal_type1 = "";
                    double Fre_total1 = 0;
                    Statement fre_charges = connect.createStatement();
                    ResultSet fre_rs = fre_charges.executeQuery("Select * From ledger where ledger_name = 'Freight Charges'");
                    while (fre_rs.next()) {
                        double Fre_curr_bal = Double.parseDouble(fre_rs.getString("curr_bal"));
                        Fre_currbal_type1 = fre_rs.getString("currbal_type");

                        double fre_total1 = Double.parseDouble(old_freight + "");

                        if (Fre_currbal_type1.equals("DR")) {
                            Fre_total1 = Fre_curr_bal + fre_total1;

                            if (Fre_total1 <= 0) {
                                Fre_currbal_type1 = "CR";
                                Fre_total1 = (Math.abs(Fre_total1));
                            } else if (Fre_total1 > 0) {
                                Fre_currbal_type1 = "DR";
                                Fre_total1 = (Math.abs(Fre_total1));
                            }
                        }

                        if (Fre_currbal_type1.equals("CR")) {
                            Fre_total1 = Fre_curr_bal - fre_total1;

                            if (Fre_total1 <= 0) {
                                Fre_currbal_type1 = "DR";
                                Fre_total1 = (Math.abs(Fre_total1));
                            } else if (Fre_total1 > 0) {
                                Fre_currbal_type1 = "CR";
                                Fre_total1 = (Math.abs(Fre_total1));
                            }
                        }
                        DecimalFormat dr1 = new DecimalFormat("0.00");
                        fre_total_round = dr1.format(Fre_total1);
                    }
                    fre_charges.executeUpdate("Update ledger Set curr_bal = '" + fre_total_round + "', currbal_type = '" + Fre_currbal_type1 + "' where ledger_name = 'Freight Charges'");
                }
//----------------------------------------------------------------------------------------------------
                Statement stmt3 = connect.createStatement();
//                Statement stmt4 = connect.createStatement();
//                Statement stmt5 = connect.createStatement();
//                String insertbillingupdate = "\"insert into purchasereturngst1 select * from purchasereturngst where bill_refrence='" + jTextField1.getText() + "'\"";
//                stmt4.executeUpdate("insert into purchasereturngst1 select * from purchasereturngst where bill_refrence='" + jTextField1.getText() + "'");
//                stmt5.executeUpdate("insert into queries values(" + insertbillingupdate + ")");

                Salesaccountcurrbal.clear();
                Salesaccountupdatecurrbal.clear();
                Salesaccountcurrbaltype.clear();
                Salesaccountupdatecurrbaltype.clear();
                salesaccount.clear();
                cashaccountcurrbal.clear();
                cashaccountupdatecurrbal.clear();
                cashaccountcurrbaltype.clear();
                cashaccountupdatecurrbaltype.clear();
                sundrydebtorsaccountcurrbal.clear();
                sundrydebtorsaccountcurrbaltype.clear();
                sundrydebtorsaccountupdatecurrbal.clear();
                sundrydebtorsaccountupdatecurrbaltype.clear();
                cardaccountcurrbal.clear();
                cardaccountupdatecurrbal.clear();
                cardaccountcurrbaltype.clear();
                cardaccountupdatecurrbaltype.clear();

                int rowcount = jTable1.getRowCount();

                if (rowcount != 0) {
                    if (issave == false) {
                        jButton5.requestFocus();
                        stmt3.executeUpdate("delete from purchasereturndualgst where bill_refrence='" + jTextField1.getText() + "'");
                        save();
                    }
                } else {
                    String date = formatter.format(jDateChooser1.getDate());
                    Statement stmtinsert = connect.createStatement();
                    stmtinsert.executeUpdate("insert into purchasereturndualgst1 select * from purchasereturndualgst where bill_refrence = '" + jTextField1.getText() + "'");
                    stmt3.executeUpdate("delete from purchasereturndualgst where bill_refrence='" + jTextField1.getText() + "'");
                    JOptionPane.showMessageDialog(rootPane, "Entry deleted succesfully");
                }
                jDialog4.dispose();
                dispose();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }//GEN-LAST:event_jButton12ActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // TODO add your handling code here:
        if (jRadioButton1.isSelected()) {
            isIgstApplicable = false;
            jTextField3.setEnabled(true);
            jTextField4.setEnabled(false);
            partyChange();
        }
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jTextField33FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField33FocusLost

        DecimalFormat df = new DecimalFormat("0.00");
        jTextField20.setText(df.format((Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField40.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField31.getText()) + Double.parseDouble(jTextField41.getText()) + Double.parseDouble(jTextField32.getText()) + Double.parseDouble(jTextField42.getText()) + Double.parseDouble(jTextField33.getText()) + Double.parseDouble(jTextField44.getText()))) + "");
        //     jTextField20.setText("" + totalamountaftertax);
    }//GEN-LAST:event_jTextField33FocusLost

    private void jRadioButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton5ActionPerformed
        // TODO add your handling code here:
        jDialog2.setVisible(true);
        if (jRadioButton5.isSelected()) {
            isIgstApplicable = false;
            jTextField3.setVisible(true);
            partyChange();
        }
    }//GEN-LAST:event_jRadioButton5ActionPerformed

    private void jTextField43KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField43KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            jButton13.requestFocus();
        }
    }//GEN-LAST:event_jTextField43KeyPressed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        noOfCopies();
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton13KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            noOfCopies();
        } else if (key == evt.VK_ESCAPE) {
            jTextField43.requestFocus();
        }
    }//GEN-LAST:event_jButton13KeyPressed

    private void jTextField44KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField44KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (Double.parseDouble(jTextField44.getText()) == 0) {
                jButton7.requestFocus();
                jButton1.requestFocus();
            } else if (Double.parseDouble(jTextField44.getText()) != 0) {
                jDialog6.setVisible(true);
                list2.removeAll();
                list2.addItem("ADD NEW");
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement freight_st = connect.createStatement();
                    ResultSet freight_rs = freight_st.executeQuery("Select * from ledger where groups = 'INDIRECT EXPENSES'");
                    while (freight_rs.next()) {
                        list2.add(freight_rs.getString("ledger_name"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField44KeyPressed
    public String freight_account = "";
    private void list2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list2ActionPerformed
        jDialog6.dispose();
        if (list2.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger cr = new reporting.accounts.createLedger();
            cr.setVisible(true);
            cr.jComboBox1.setSelectedItem("INDIRECT EXPENSES");
            cr.jComboBox5.setSelectedItem("OTHER");
        } else {
            freight_account = list2.getSelectedItem();
            if (jTextField44.getText().isEmpty()) {
                JOptionPane.showMessageDialog(rootPane, "Freight Charges can not be empty");
                jTextField44.setText("0.00");
                jTextField44.requestFocus();
            } else {
                DecimalFormat frd = new DecimalFormat("0.00");
                double netamnt = Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField30.getText()) + Double.parseDouble(jTextField40.getText()) + Double.parseDouble(jTextField31.getText()) + Double.parseDouble(jTextField41.getText()) + Double.parseDouble(jTextField32.getText()) + Double.parseDouble(jTextField42.getText());
                double total_netamnt = netamnt + Double.parseDouble(jTextField44.getText());
                jTextField20.setText("" + frd.format(total_netamnt));
                jButton7.requestFocus();
                jButton1.requestFocus();
            }
        }
    }//GEN-LAST:event_list2ActionPerformed

    private void jTextField7KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7KeyReleased

    private void jTextField33KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField33KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_ENTER) {
            jTextField44.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }

    }//GEN-LAST:event_jTextField33KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        freightGst();
        jDialog7.dispose();
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton14KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            freightGst();
            jDialog7.dispose();
        }
    }//GEN-LAST:event_jButton14KeyPressed

    private void jTextField45KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField45KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDialog7.setVisible(true);
        }
    }//GEN-LAST:event_jTextField45KeyPressed

    private void jComboBox11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox11KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton14.requestFocus();
        }
    }//GEN-LAST:event_jComboBox11KeyPressed

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        if (jRadioButton3.isSelected()) {
            tax_type = "Inclusive";
        }
    }//GEN-LAST:event_jRadioButton3ActionPerformed

    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        if (jRadioButton4.isSelected()) {
            tax_type = "Exclusive";
        }
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void jComboBox3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox3KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField7.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jComboBox3KeyPressed
    public void partyChange() {
        tiv = 0;
        tsv = 0;
        tcv = 0;
        tiv1 = 0;
        tsv1 = 0;
        tcv1 = 0;
        iv = 0;
        sv = 0;
        cv = 0;
        iv1 = 0;
        sv1 = 0;
        cv1 = 0;
        jTextField30.setText("0");
        jTextField40.setText("0");
        jTextField31.setText("0");
        jTextField41.setText("0");
        jTextField32.setText("0");
        jTextField42.setText("0");
        int row_count = jTable1.getRowCount();
        for (int i = 0; i < row_count; i++) {
            DecimalFormat df1 = new DecimalFormat("0.00");
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement sttt = connect.createStatement();
                Statement stttt = connect.createStatement();
                Statement pro_st = connect.createStatement();
                Statement sgst_st = connect.createStatement();
                Statement cgst_st = connect.createStatement();
                ResultSet rssss, rs5, rs4;
                if (isIgstApplicable == true) {
                    ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + (jTable1.getValueAt(i, 2) + "") + "'");
                    while (rsss.next()) {
                        iscalculationapplicable = rsss.getInt("iscalculationapplicable");
                        if (iscalculationapplicable == 0) {
                            rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                            while (rssss.next()) {
                                double vat = Double.parseDouble(rssss.getString("percent"));
                                double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                                if (jRadioButton3.isSelected()) {
                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                } else if (jRadioButton4.isSelected()) {
                                    amntbeforetax = value1;
                                }
                                if (vat == 5) {
                                    iv = (value1 * vat) / 100;
                                } else if (vat == 12) {
                                    iv1 = (value1 * vat) / 100;
                                }
                            }
                        } else if (iscalculationapplicable == 1) {
                            if (Double.parseDouble(jTable1.getValueAt(i, 3) + "") > 1000) {
                                rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                                while (rssss.next()) {
                                    double vat = Double.parseDouble(rssss.getString("percent"));
                                    double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                                    if (jRadioButton3.isSelected()) {
                                        amntbeforetax = (value1 / (100 + vat)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amntbeforetax = value1;
                                    }
                                    if (vat == 5) {
                                        iv = (value1 * vat) / 100;
//                               
                                    } else if (vat == 12) {
                                        iv1 = (value1 * vat) / 100;
                                    }

                                }

                            } else if (Double.parseDouble(jTable1.getValueAt(i, 3) + "") <= 1000) {
                                rssss = stttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");
                                while (rssss.next()) {
                                    double vat = 5;
                                    double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                                    if (jRadioButton3.isSelected()) {
                                        amntbeforetax = (value1 / (100 + vat)) * 100;
                                    } else if (jRadioButton4.isSelected()) {
                                        amntbeforetax = value1;
                                    }
                                    iv = (value1 * vat) / 100;
                                }
                            }

                        }

                    }

                    tiv = (tiv + iv);
                    tiv1 = (tiv1 + iv1);

                    jTextField30.setText("" + df1.format(tiv));
                    jTextField40.setText("" + df1.format(tiv1));

                } else if (isIgstApplicable == false) {
                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + (jTable1.getValueAt(i, 2) + "") + "'");
                    while (pro_rs.next()) {
                        iscalculationapplicable = pro_rs.getInt("iscalculationapplicable");
                        ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                        if ((iscalculationapplicable == 0) || ((iscalculationapplicable == 1) && (Double.parseDouble(jTable1.getValueAt(i, 3) + "")) > 1000)) {
                            while (sgst_rs.next()) {
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }

                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }
                        } else if ((iscalculationapplicable == 1) && (Double.parseDouble(jTable1.getValueAt(i, 3) + "") < 1000)) {
                            sgstpercent = 2.5;
                            cgstpercent = 2.5;
                        }

                    }

                    totalpercent = sgstpercent + cgstpercent;
                    double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                    if (jRadioButton3.isSelected()) {
                        amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                    } else if (jRadioButton4.isSelected()) {
                        amntbeforetax = value1;
                    }
                    if (totalpercent == 5) {
                        sv = (amntbeforetax * sgstpercent) / 100;
                        cv = ((amntbeforetax * cgstpercent) / 100);

                        tsv = (tsv + sv);
                        tcv = (tcv + cv);
                    } else if (totalpercent == 12) {
                        sv1 = (amntbeforetax * sgstpercent) / 100;
                        cv1 = ((amntbeforetax * cgstpercent) / 100);

                        tsv1 = (tsv1 + sv1);
                        tcv1 = (tcv1 + cv1);
                    }
                    jTextField31.setText("" + df1.format(tsv));
                    jTextField41.setText("" + df1.format(tsv1));
                    jTextField32.setText("" + df1.format(tcv));
                    jTextField42.setText("" + df1.format(tcv1));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void noOfCopies() {
        no_of_copies = Integer.parseInt(jTextField43.getText());
        jDialog5.setVisible(false);
        PrinterJob job = PrinterJob.getPrinterJob();
        PageFormat pf = new PageFormat();
        Paper paper = new Paper();
        paper.setSize(5.8d * 72, 8.3d * 72);

        double margin = 40;
        paper.setImageableArea(margin, margin, (paper.getWidth() - 0) * 1, paper.getHeight() - margin * 2);

        pf.setPaper(paper);
        job.setPrintable(this, pf);
        PrintService service = PrintServiceLookup.lookupDefaultPrintService();

        try {
            job.setPrintService(service);
            job.setCopies(no_of_copies);
            job.print();
        } catch (PrinterException ex) {
            /* The job did not successfully complete */
            ex.printStackTrace();
        }
        this.dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(New_Dual_PurchaseReturn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(New_Dual_PurchaseReturn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(New_Dual_PurchaseReturn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(New_Dual_PurchaseReturn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //final JButton printButton = new JButton("Print");
        // UIManager.put("swing.boldMetal", Boolean.TRUE);
        //JFrame f = new JFrame("GatePass");
//        Billing_1.addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent e) {
//                System.exit(0);
//            }
//        });
//        Font f1 = new Font("Times New Roman", Font.BOLD, 12);
        //JBut.setVisible(true);
//        JLabel label1 = new JLabel("Vision Techno Solutions", JLabel.CENTER);
//        label1.setBounds(30, 30, 200, 40);
//        JLabel label2 = new JLabel("Batch No:");
//        label2.setBounds(10, 70, 80, 20);
//        JLabel label3 = new JLabel("Date and Time:");
//        label3.setBounds(10, 100, 120, 20);
//        JLabel label4 = new JLabel("Visitor's Name:");
//        label4.setBounds(10, 130, 120, 20);
//        JLabel label5 = new JLabel("Concern Person:");
//        label5.setBounds(10, 160, 120, 20);
//        JLabel label6 = new JLabel("Purpose of Visit:");
//        label6.setBounds(10, 190, 120, 20);
//        JLabel label7 = new JLabel("Manager's Sign");
//        label7.setBounds(140, 230, 120, 20);
//        JLabel label8 = new JLabel("Visitor's Sign");
//        label8.setBounds(10, 230, 120, 20);
/////      /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new New_Dual_PurchaseReturn().setVisible(true);
            }
        });
    }
    String snumbers[] = new String[5000];

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    public javax.swing.JComboBox jComboBox1;
    public javax.swing.JComboBox<String> jComboBox11;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JDialog jDialog1;
    public javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JDialog jDialog4;
    private javax.swing.JDialog jDialog5;
    private javax.swing.JDialog jDialog6;
    private javax.swing.JDialog jDialog7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    public javax.swing.JLabel jLabel15;
    public javax.swing.JLabel jLabel16;
    public javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel18;
    public javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    public javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    public javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public final javax.swing.JRadioButton jRadioButton1 = new javax.swing.JRadioButton();
    public javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    public javax.swing.JRadioButton jRadioButton5;
    public javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTable jTable1;
    private javax.swing.JTable jTable3;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    public javax.swing.JTextField jTextField17;
    public javax.swing.JTextField jTextField18;
    public javax.swing.JTextField jTextField19;
    public javax.swing.JTextField jTextField2;
    public javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    private javax.swing.JTextField jTextField22;
    private javax.swing.JTextField jTextField23;
    private javax.swing.JTextField jTextField24;
    private javax.swing.JTextField jTextField25;
    private javax.swing.JTextField jTextField26;
    private javax.swing.JTextField jTextField27;
    private javax.swing.JTextField jTextField28;
    private javax.swing.JTextField jTextField29;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField30;
    public javax.swing.JTextField jTextField31;
    public javax.swing.JTextField jTextField32;
    public javax.swing.JTextField jTextField33;
    public javax.swing.JTextField jTextField34;
    public javax.swing.JTextField jTextField35;
    public javax.swing.JTextField jTextField36;
    public javax.swing.JTextField jTextField37;
    public javax.swing.JTextField jTextField38;
    public javax.swing.JTextField jTextField39;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField40;
    public javax.swing.JTextField jTextField41;
    public javax.swing.JTextField jTextField42;
    private javax.swing.JTextField jTextField43;
    public javax.swing.JTextField jTextField44;
    public javax.swing.JTextField jTextField45;
    public javax.swing.JTextField jTextField5;
    public javax.swing.JTextField jTextField7;
    public javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    private java.awt.List list1;
    private java.awt.List list2;
    // End of variables declaration//GEN-END:variables
}
