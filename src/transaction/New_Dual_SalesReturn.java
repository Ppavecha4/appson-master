package transaction;

import connection.connection;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.List;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Golu
 */
public class New_Dual_SalesReturn extends javax.swing.JFrame implements Printable {

    public List list;
    public String xz[];
    String sundrydebtors = null;
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    public SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    public boolean isstockid = false;
    public String branch = null;
    ArrayList party = new ArrayList();

    public boolean isIgstApplicable = false;
    public double diff_total = 0;
    public boolean issave = false;
    String diff_total_round;
    String igstamnt1_1;
    String sgstamnt1_1;
    String cgstamnt1_1;
    String Round_currbal_type = "";
    double amntbeforetax = 0;

    double tiv = 0;
    double tsv = 0;
    double tcv = 0;
    double trv = 0;
    double iv = 0;
    double sv = 0;
    double cv = 0;

    double sgstpercent = 0;
    double cgstpercent = 0;
    double totalpercent = 0;
    String tax_type = "Inclusive";
    String Barcode;
    String Rate;
    String Company;
    String Tax_Type_Status;
    String Year;
    private static New_Dual_SalesReturn obj = null;

    public New_Dual_SalesReturn() {
        try {
            combo();
            initComponents();
            this.setLocationRelativeTo(null);
            jDialog4.setLocationRelativeTo(null);
            jRadioButton5.setSelected(true);
            jButton6.setVisible(false);
            jButton7.setVisible(false);
            jLabel23.setVisible(false);
            jLabel24.setVisible(false);
            jRadioButton1.setSelected(true);

            if (jRadioButton1.isSelected()) {
                String c = "CASH";
                try {
                    connection c1 = new connection();
                    Connection connect = c1.cone();
                    Statement st2 = connect.createStatement();
                    ResultSet rs2 = null;
                    rs2 = st2.executeQuery("select * from ledger where ledger_name ='" + c + "' ");
                    while (rs2.next()) {
                        String cash = rs2.getString(1);
                        jLabel23.setText(rs2.getString(10));
                        jLabel24.setText(rs2.getString(11));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            jLabel4.setVisible(false);
            jTextField4.setVisible(false);
            jTextField3.setEnabled(true);
            Calendar currentDate = Calendar.getInstance();
            String dateNow = formatter.format(currentDate.getTime());
            jDateChooser1.setDate(formatter.parse(dateNow));
            try {
                java.util.Date dateafter = formatter.parse("20" + getyear + "-03-31");
                java.util.Date datenow = formatter.parse(dateNow);
                if (datenow.before(dateafter)) {
                    getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                    nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            String salesreturn = "SR/";
            String slash = "/";
            String yearwise = Year.concat(slash);
//            String yearwise = (getyear.concat(nextyear)).concat(slash);

            String finalconcat = ((salesreturn.concat(yearwise)));
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st4 = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT (bill_refrence) FROM salesreturn where branchid='0'");
                int a = 0, b, d = 0;
                int i = 0;

                while (rs.next()) {
                    if (i == 0) {
                        d = rs.getString(1).lastIndexOf("/");
                        i++;
                    }
                    b = Integer.parseInt(rs.getString(1).substring(d + 1));
                    if (a < b) {
                        a = b;
                    }
                }
                String s = finalconcat.concat(Integer.toString(a + 1));
                jTextField1.setText("" + s);

                ResultSet pro_rs = st4.executeQuery("select distinct(description) from billing");
                while (pro_rs.next()) {
                    jComboBox2.addItem(pro_rs.getString("description"));
                }
                ResultSet pro_rs1 = st4.executeQuery("select distinct(description) from billing_old");
                while (pro_rs1.next()) {
                    jComboBox2.addItem(pro_rs1.getString("description"));
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
            try {
                connection c = new connection();
                Connection conn = c.cone();
                Statement setting_st = conn.createStatement();
                Statement st = conn.createStatement();
                Statement st1 = conn.createStatement();
                Statement st2 = conn.createStatement();
                ResultSet rs2 = st2.executeQuery("select ledger_name from ledger where groups IN('SUNDRY DEBITORS')");
                while (rs2.next()) {
                    list1.add(rs2.getString(1));
                }

                ResultSet rs = st.executeQuery("select Name from Group_create where Under IN('SUNDRY DEBITORS')");
                while (rs.next()) {
                    String gname = rs.getString(1);
                    ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups='" + gname + "' ");
                    while (rs1.next()) {
                        list1.add(rs1.getString(1));
                    }
                }
                ResultSet setting_rs = setting_st.executeQuery("Select * from setting");
                while (setting_rs.next()) {
                    Barcode = setting_rs.getString("Barcode");
                    Rate = setting_rs.getString("Rate");
                    Company = setting_rs.getString("Company");
                    Tax_Type_Status = setting_rs.getString("Tax_Type");
                }
                if (Barcode.equals("Enable")) {
                    jComboBox2.setEnabled(false);
                } else if (Barcode.equals("Disable")) {
                    jTextField5.setEnabled(false);
                }
                if (Tax_Type_Status.equals("Enable")) {
                    jRadioButton5.setEnabled(true);
                    jRadioButton6.setEnabled(true);
                } else if (Tax_Type_Status.equals("Disable")) {
                    jRadioButton5.setEnabled(false);
                    jRadioButton6.setEnabled(false);
                }

                st.close();
                st1.close();
                st2.close();
                conn.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

    }

    public static New_Dual_SalesReturn getObj() {
        if (obj == null) {
            obj = new New_Dual_SalesReturn();
        }
        return obj;
    }

    private Object makeObj(final String item) {
        return new Object() {
            @Override
            public String toString() {
                return item;
            }
        };
    }
    //      rs = st.executeQuery("SELECT party_code from Party");

    public void combo() {

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st1 = connect.createStatement();
            Statement st4 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("SELECT * from Staff");
            int a1 = 0;
            while (rs1.next()) {
                a1++;
            }
            xz = new String[a1];
            rs1 = st1.executeQuery("SELECT * from Staff");
            a1 = 0;
            while (rs1.next()) {
                xz[a1] = rs1.getString(1);
                a1++;
            }
            ResultSet rs_year = st4.executeQuery("Select * from year");
            while (rs_year.next()) {
                Year = rs_year.getString("year");
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        Conform = new javax.swing.JDialog();
        jLabel21 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jDialog1 = new javax.swing.JDialog();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel22 = new javax.swing.JLabel();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jDialog2 = new javax.swing.JDialog();
        list1 = new java.awt.List();
        jButton3 = new javax.swing.JButton();
        jDialog3 = new javax.swing.JDialog();
        jTextField26 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jTextField27 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jTextField28 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jTextField29 = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        jButton8 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jDialog4 = new javax.swing.JDialog();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel30 = new javax.swing.JLabel();
        buttonGroup3 = new javax.swing.ButtonGroup();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jTextField13 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jTextField16 = new javax.swing.JTextField();
        jTextField17 = new javax.swing.JTextField();
        jTextField18 = new javax.swing.JTextField();
        jTextField19 = new javax.swing.JTextField();
        jTextField20 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox(xz);
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel26 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        jRadioButton5 = new javax.swing.JRadioButton();
        jRadioButton6 = new javax.swing.JRadioButton();
        jComboBox2 = new javax.swing.JComboBox<>();

        Conform.setMinimumSize(new java.awt.Dimension(400, 400));

        jLabel21.setText("WOULD YOU LIKE TO SAVE ");

        jButton2.setText("NO");

        jButton5.setText("Print");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ConformLayout = new javax.swing.GroupLayout(Conform.getContentPane());
        Conform.getContentPane().setLayout(ConformLayout);
        ConformLayout.setHorizontalGroup(
            ConformLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConformLayout.createSequentialGroup()
                .addGap(95, 95, 95)
                .addGroup(ConformLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ConformLayout.createSequentialGroup()
                        .addComponent(jButton5)
                        .addGap(52, 52, 52)
                        .addComponent(jButton2))
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(132, Short.MAX_VALUE))
        );
        ConformLayout.setVerticalGroup(
            ConformLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConformLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(ConformLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton5))
                .addGap(20, 20, 20))
        );

        jDialog1.setMinimumSize(new java.awt.Dimension(300, 150));

        buttonGroup2.add(jRadioButton3);
        jRadioButton3.setText("Existing Customer");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });

        buttonGroup2.add(jRadioButton4);
        jRadioButton4.setText("New Customer");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });

        jLabel22.setText("Select Customer Type?");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRadioButton3)
                    .addComponent(jRadioButton4)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel22)
                .addGap(18, 18, 18)
                .addComponent(jRadioButton3)
                .addGap(18, 18, 18)
                .addComponent(jRadioButton4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(400, 600));

        list1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list1ActionPerformed(evt);
            }
        });

        jButton3.setText("ADD NEW");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addComponent(list1, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(jButton3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 526, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addContainerGap(330, Short.MAX_VALUE)
                .addComponent(jButton3)
                .addGap(247, 247, 247))
        );

        jDialog3.setMinimumSize(new java.awt.Dimension(682, 580));

        jTextField26.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField26CaretUpdate(evt);
            }
        });
        jTextField26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField26ActionPerformed(evt);
            }
        });
        jTextField26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField26KeyPressed(evt);
            }
        });

        jLabel34.setText("Enter Party code :");

        jTextField27.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField27CaretUpdate(evt);
            }
        });
        jTextField27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField27ActionPerformed(evt);
            }
        });
        jTextField27.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField27KeyPressed(evt);
            }
        });

        jLabel35.setText("Enter Party Name :");

        jTextField28.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField28CaretUpdate(evt);
            }
        });
        jTextField28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField28KeyPressed(evt);
            }
        });

        jLabel36.setText("Enter city :");

        jLabel37.setText("Enter State :");

        jTextField29.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField29CaretUpdate(evt);
            }
        });
        jTextField29.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField29KeyPressed(evt);
            }
        });

        jScrollPane5.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jButton8.setText("ADD NEW PARTY");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "party_code", "party_name", "city", "state", "opening_bal", "Balance Type", "IGST Applicable"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTable3);

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 661, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jDialog3Layout.createSequentialGroup()
                                .addComponent(jLabel37)
                                .addGap(44, 44, 44)
                                .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jDialog3Layout.createSequentialGroup()
                                    .addComponent(jLabel36)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog3Layout.createSequentialGroup()
                                        .addComponent(jLabel35)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog3Layout.createSequentialGroup()
                                        .addComponent(jLabel34)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(29, 29, 29)
                        .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel35))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel34))
                        .addGap(23, 23, 23)
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel36))
                        .addGap(22, 22, 22)
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel37)))
                    .addGroup(jDialog3Layout.createSequentialGroup()
                        .addGap(180, 180, 180)
                        .addComponent(jButton8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane5)
                            .addComponent(jScrollPane4))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jDialog4.setSize(new java.awt.Dimension(486, 420));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bill Number", "Quantity ", "Rate", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jLabel30.setFont(new java.awt.Font("sansserif", 3, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(51, 52, 53));
        jLabel30.setText("Please Select Bill No...");

        javax.swing.GroupLayout jDialog4Layout = new javax.swing.GroupLayout(jDialog4.getContentPane());
        jDialog4.getContentPane().setLayout(jDialog4Layout);
        jDialog4Layout.setHorizontalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
            .addGroup(jDialog4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDialog4Layout.setVerticalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE))
        );

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : SALES RETURN");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setText("Bill Reference :");

        jLabel2.setText("Date :");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Cash");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Credit");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jLabel3.setText("Customer Name :");

        jLabel4.setText("Customer A/C No. :");

        jTextField1.setEditable(false);
        jTextField1.setText("1");
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jTextField3.setEnabled(false);
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jTextField4.setEnabled(false);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel5.setText("Stock No.");

        jLabel6.setText("Product");

        jLabel7.setText("Rate");

        jLabel8.setText("Quantity");

        jLabel9.setText("Value");

        jLabel10.setText("Disc. Code");

        jLabel11.setText("Disc %");

        jLabel12.setText("Disc Amt");

        jLabel13.setText("Total");

        jLabel14.setText("Staff :");

        jTextField5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField5FocusLost(evt);
            }
        });
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField5KeyReleased(evt);
            }
        });

        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });
        jTextField7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField7KeyPressed(evt);
            }
        });

        jTextField8.setText("1");
        jTextField8.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField8FocusLost(evt);
            }
        });
        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jTextField9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField9ActionPerformed(evt);
            }
        });
        jTextField9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField9KeyPressed(evt);
            }
        });

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });
        jTextField10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField10KeyPressed(evt);
            }
        });

        jTextField11.setText("0");
        jTextField11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField11KeyPressed(evt);
            }
        });

        jTextField12.setText("0");
        jTextField12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField12FocusLost(evt);
            }
        });
        jTextField12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField12ActionPerformed(evt);
            }
        });
        jTextField12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField12KeyPressed(evt);
            }
        });

        jTextField13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField13FocusGained(evt);
            }
        });
        jTextField13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField13KeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SNo.", "Stock No.", "Product", "Rate", "Quantity", "Value", "Disc Code", "Disc %", "Disc Amt", "Total", "Staff", "Bill Number"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(50);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(55);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setResizable(false);
            jTable1.getColumnModel().getColumn(8).setResizable(false);
            jTable1.getColumnModel().getColumn(9).setResizable(false);
            jTable1.getColumnModel().getColumn(10).setCellEditor(new DefaultCellEditor(jComboBox3));
        }

        jLabel15.setText("Total Amount");

        jLabel16.setText("Quantity");

        jLabel17.setText("Disc Amount");

        jLabel18.setText("Amount After Dis");

        jLabel19.setText("Value Before Tax");

        jLabel20.setText("Net Amount");

        jTextField15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField15ActionPerformed(evt);
            }
        });
        jTextField15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField15KeyPressed(evt);
            }
        });

        jTextField16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField16KeyPressed(evt);
            }
        });

        jTextField17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField17FocusLost(evt);
            }
        });
        jTextField17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField17KeyPressed(evt);
            }
        });

        jTextField18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField18ActionPerformed(evt);
            }
        });
        jTextField18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField18KeyPressed(evt);
            }
        });

        jTextField19.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField19FocusLost(evt);
            }
        });
        jTextField19.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField19KeyPressed(evt);
            }
        });

        jTextField20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField20FocusLost(evt);
            }
        });
        jTextField20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField20ActionPerformed(evt);
            }
        });
        jTextField20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField20KeyPressed(evt);
            }
        });

        jComboBox1.setSelectedItem(null);
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel23.setText("jLabel23");

        jLabel24.setText("jLabel24");

        jTextField14.setEditable(false);
        jTextField14.setText("1");
        jTextField14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField14ActionPerformed(evt);
            }
        });
        jTextField14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField14KeyPressed(evt);
            }
        });

        jLabel25.setText("S No.");

        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton4.setText("CANCEL");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");
        jDateChooser1.setEnabled(false);

        jLabel26.setText("Igst Amount");

        jLabel27.setText("Sgst Amount");

        jLabel28.setText("Cgst Amount");

        jLabel29.setText("Round Off");

        jButton6.setText("Update");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("REPRINT");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jLabel31.setText("Tax Type -");

        buttonGroup3.add(jRadioButton5);
        jRadioButton5.setText("Inclusive");
        jRadioButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton5ActionPerformed(evt);
            }
        });

        buttonGroup3.add(jRadioButton6);
        jRadioButton6.setText("Exclusive");
        jRadioButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton6ActionPerformed(evt);
            }
        });

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jRadioButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jRadioButton2)
                                .addGap(165, 165, 165)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel23)
                                    .addComponent(jLabel24))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 414, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel15)
                                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(30, 30, 30)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel16)
                                            .addComponent(jLabel26)
                                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLabel19))
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel17))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel28)
                                            .addComponent(jLabel18))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel20)
                                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLabel27))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel29)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton7))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton6)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton1)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton4)))))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(29, 29, 29)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jLabel31)
                        .addGap(18, 18, 18)
                        .addComponent(jRadioButton5)
                        .addGap(18, 18, 18)
                        .addComponent(jRadioButton6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel14)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(704, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25)
                            .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addGap(29, 29, 29))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel31)
                    .addComponent(jRadioButton5)
                    .addComponent(jRadioButton6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel19)
                                .addComponent(jLabel27)
                                .addComponent(jLabel28)
                                .addComponent(jLabel29)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(12, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton4)
                            .addComponent(jButton6))
                        .addContainerGap())))
        );

        setSize(new java.awt.Dimension(949, 553));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField9ActionPerformed

    private void jTextField15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField15ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField15ActionPerformed

    private void jTextField20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField20ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField20ActionPerformed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:

        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField4.isEnabled()) {
                jTextField4.requestFocus();
            } else {
                jTextField5.requestFocus();
            }

        }

    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }

    }//GEN-LAST:event_jTextField4KeyPressed
    static String disc_type = null;
    private void jTextField5FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField5FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5FocusLost
    boolean islist = false;
    public int ismeter = 0;
    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().isEmpty()) {
                jComboBox2.requestFocus();
            } else {

                try {
                    DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
                    dtm.getDataVector().removeAllElements();
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st_bill = connect.createStatement();
                    Statement st_salesreturn = connect.createStatement();
                    ResultSet rs_bill = null;
                    ResultSet rs_salesreturn = null;
                    rs_bill = st_bill.executeQuery("Select * from billing where stock_no = '" + jTextField5.getText() + "' and bill_refrence NOT IN(select bill_no from salesreturn WHERE bill_no = bill_refrence)");
                    if (!rs_bill.isBeforeFirst()) {
                        rs_bill = st_bill.executeQuery("Select * from billing_old where stock_no = '" + jTextField5.getText() + "' and bill_refrence NOT IN(select bill_no from salesreturn_old WHERE bill_no = bill_refrence)");
                    }
                    if (!rs_bill.isBeforeFirst()) {
                        JOptionPane.showMessageDialog(rootPane, "Item has not sold");
                        jTextField5.requestFocus();
                    } else {
                        while (rs_bill.next()) {
                            if (rs_bill.getInt("ismeter") == 0) {
                                rs_salesreturn = st_salesreturn.executeQuery("select * from salesreturn where stock_no='" + jTextField5.getText() + "' and bill_no='" + rs_bill.getString("bill_refrence") + "'");
                                if (!rs_salesreturn.isBeforeFirst()) {
                                    rs_salesreturn = st_salesreturn.executeQuery("select * from salesreturn_old where stock_no='" + jTextField5.getText() + "' and bill_no='" + rs_bill.getString("bill_refrence") + "'");
                                }
                                if (rs_salesreturn.next()) {
                                    JOptionPane.showMessageDialog(rootPane, "Item has not sold");
                                } else {
                                    Object o[] = {rs_bill.getString("bill_refrence"), rs_bill.getString("qnty"), rs_bill.getString("rate"), rs_bill.getString("total")};
                                    dtm.addRow(o);
                                    jDialog4.setVisible(true);
                                }
                            } else {
                                Object o[] = {rs_bill.getString("bill_refrence"), rs_bill.getString("qnty"), rs_bill.getString("rate"), rs_bill.getString("total")};
                                dtm.addRow(o);
                                jDialog4.setVisible(true);
                            }

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        if (key == evt.VK_TAB) {
            jTextField13.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (jTextField5.getText().isEmpty()) {
                if (ismeter == 0) {
                    System.out.println("is meter zero code is running");

                    if (!jTextField7.getText().isEmpty()) {
                        String ra_te = jTextField7.getText();
                        String qun_tity = jTextField8.getText();
                        double p = Double.parseDouble(ra_te);
                        double q = Double.parseDouble(qun_tity);
                        double amou_nt = (p * q);
                        jTextField9.setText("" + String.valueOf(amou_nt));
                    }

//focus gained
                    if (!jTextField12.getText().isEmpty() && !jTextField9.getText().isEmpty()) {
                        double disc_amt = Double.parseDouble(jTextField12.getText());
                        double value = Double.parseDouble(jTextField9.getText());
                        double total = value - disc_amt;
                        jTextField13.setText("" + String.valueOf(total));
                    }

// keypressed
                    jTextField5.requestFocus();
                    String st1 = (String) jComboBox1.getSelectedItem();
                    sno = Integer.parseInt(jTextField14.getText());
                    totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                    DecimalFormat df = new DecimalFormat("0.00");
                    String totalamount1 = df.format(totalamount);
                    jTextField15.setText("" + totalamount1);
                    jTextField18.setText("" + totalamount1);
                    totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));

                    String totalquantity1 = df.format(totalquantity);
                    jTextField16.setText("" + totalquantity1);
                    totaldiscount = (totaldiscount + Double.parseDouble(jTextField12.getText()));
                    String totaldiscount1 = df.format(totaldiscount);
                    jTextField17.setText("" + totaldiscount1);
//                    totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
//                    String totalamountaftertax1 = df.format(totalamountaftertax);
//                    jTextField20.setText("" + totalamountaftertax1);

                    if (jTextField5.getText().isEmpty()) {
                        jTextField5.setText("N/A");
                    }

                    Object o[] = {sno, jTextField5.getText().toUpperCase(), jComboBox2.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                        jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                        st1, ""};
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.addRow(o);
                    try {
                        connection c5 = new connection();
                        Connection connect1 = c5.cone();
                        Statement sttt = connect1.createStatement();
                        Statement st3 = connect1.createStatement();
                        Statement pro_st = connect1.createStatement();
                        Statement sgst_st = connect1.createStatement();
                        Statement cgst_st = connect1.createStatement();
                        if (isIgstApplicable == true) {
                            ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox2.getSelectedItem() + "'");
                            while (rsss.next()) {

                                ResultSet rssss = st3.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                while (rssss.next()) {
                                    double vat = Double.parseDouble(rssss.getString("percent"));
                                    double value1 = Double.parseDouble(jTextField9.getText());

                                    if (jRadioButton5.isSelected()) {
                                        amntbeforetax = (value1 / (100 + vat)) * 100;
                                    } else if (jRadioButton6.isSelected()) {
                                        amntbeforetax = (value1);
                                    }
                                    iv = (amntbeforetax * vat) / 100;
                                }
                            }
                        } else if (isIgstApplicable == false) {

                            ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox2.getSelectedItem() + "'");
                            while (pro_rs.next()) {
                                ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                while (sgst_rs.next()) {
                                    sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                }

                                ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                while (cgst_rs.next()) {
                                    cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                }
                            }

                            totalpercent = sgstpercent + cgstpercent;
                            double value1 = Double.parseDouble(jTextField9.getText());

                            if (jRadioButton5.isSelected()) {
                                amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                            } else if (jRadioButton6.isSelected()) {
                                amntbeforetax = (value1);
                            }
                            sv = (amntbeforetax * sgstpercent) / 100;
                            cv = ((amntbeforetax * cgstpercent) / 100);

                        }

                        totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                        if (jRadioButton5.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                            String totalamountaftertax1 = df.format(totalamountaftertax);
                            jTextField20.setText("" + totalamountaftertax1);
                        } else if (jRadioButton6.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()) + iv + sv + cv);
                            String totalamountaftertax1 = df.format(totalamountaftertax);
                            jTextField20.setText("" + totalamountaftertax1);
                        }
                        tiv = (tiv + iv);
                        tsv = (tsv + sv);
                        tcv = (tcv + cv);
                        DecimalFormat df1 = new DecimalFormat("0.00");

                        jTextField19.setText("" + df1.format(totalamountbeforetax));
                        jTextField2.setText("" + df1.format(tiv));
                        jTextField21.setText("" + df1.format(tsv));
                        jTextField22.setText("" + df1.format(tcv));

                        trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField2.getText()) + Double.parseDouble(jTextField21.getText()) + (Double.parseDouble(jTextField22.getText()))));

                        jTextField23.setText("" + df1.format(trv));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    sno = Integer.parseInt(jTextField14.getText());
                    sno++;
                    jTextField14.setText("" + sno);
                    jTextField5.setText(null);
                    jComboBox2.setSelectedIndex(0);
                    jTextField7.setText("0");
                    jTextField8.setText("1");
                    jTextField9.setText(null);
                    jTextField10.setText(null);
                    jTextField11.setText("0");
                    jTextField12.setText("0");
                    jTextField13.setText(null);
                    jComboBox1.setSelectedItem(st1);
                }
            } else if (!jTextField5.getText().isEmpty()) {
                if (ismeter == 1) {
                    int billl_row = jTable2.getSelectedRow();
                    if (!jTextField7.getText().isEmpty()) {
                        String ra_te = jTextField7.getText();
                        String qun_tity = jTextField8.getText();
                        double p = Double.parseDouble(ra_te);
                        double q = Double.parseDouble(qun_tity);
                        double amou_nt = (p * q);
                        //	String t = Integer.toString(amou_nt);
                        jTextField9.setText("" + String.valueOf(amou_nt));
                    }

//focus gained
                    if (!jTextField12.getText().isEmpty() && !jTextField9.getText().isEmpty()) {
                        double disc_amt = Double.parseDouble(jTextField12.getText());
                        double value = Double.parseDouble(jTextField9.getText());
                        double total = value - disc_amt;
                        jTextField13.setText("" + String.valueOf(total));
                    }

                    // keypressed
//                jTextField5.requestFocus();
                    String st1 = (String) jComboBox1.getSelectedItem();
                    sno = Integer.parseInt(jTextField14.getText());
                    totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                    DecimalFormat df = new DecimalFormat("0.00");
                    String totalamount1 = df.format(totalamount);
                    jTextField15.setText("" + totalamount1);
                    jTextField18.setText("" + totalamount1);
                    totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));

                    String totalquantity1 = df.format(totalquantity);
                    jTextField16.setText("" + totalquantity1);
                    totaldiscount = (totaldiscount + Double.parseDouble(jTextField12.getText()));
                    String totaldiscount1 = df.format(totaldiscount);
                    jTextField17.setText("" + totaldiscount1);
//                    totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
//                    String totalamountaftertax1 = df.format(totalamountaftertax);
//                    jTextField20.setText("" + totalamountaftertax1);

                    if (jTextField5.getText().isEmpty()) {
                        jTextField5.setText("N/A");
                    }

                    Object o[] = {sno, jTextField5.getText().toUpperCase(), jComboBox2.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                        jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                        st1, jTable2.getValueAt(billl_row, 0)};
                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.addRow(o);
                    try {
                        connection c5 = new connection();
                        Connection connect1 = c5.cone();
                        Statement sttt = connect1.createStatement();
                        Statement st3 = connect1.createStatement();
                        Statement pro_st = connect1.createStatement();
                        Statement sgst_st = connect1.createStatement();
                        Statement cgst_st = connect1.createStatement();
                        if (isIgstApplicable == true) {
                            ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jComboBox2.getSelectedItem() + "'");
                            while (rsss.next()) {

                                ResultSet rssss = st3.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                                while (rssss.next()) {
                                    double vat = Double.parseDouble(rssss.getString("percent"));
                                    double value1 = Double.parseDouble(jTextField9.getText());
                                    if (jRadioButton5.isSelected()) {
                                        amntbeforetax = (value1 / (100 + vat)) * 100;
                                    } else if (jRadioButton6.isSelected()) {
                                        amntbeforetax = (value1);
                                    }
                                    iv = (amntbeforetax * vat) / 100;
                                }
                            }
                        } else if (isIgstApplicable == false) {
                            ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jComboBox2.getSelectedItem() + "'");
                            while (pro_rs.next()) {
                                ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                                while (sgst_rs.next()) {
                                    sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                                }

                                ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                                while (cgst_rs.next()) {
                                    cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                                }
                            }

                            totalpercent = sgstpercent + cgstpercent;
                            double value1 = Double.parseDouble(jTextField9.getText());
                            if (jRadioButton5.isSelected()) {
                                amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                            } else if (jRadioButton6.isSelected()) {
                                amntbeforetax = (value1);
                            }
                            sv = (amntbeforetax * sgstpercent) / 100;
                            cv = ((amntbeforetax * cgstpercent) / 100);

                        }

                        totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                        if (jRadioButton5.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                            String totalamountaftertax1 = df.format(totalamountaftertax);
                            jTextField20.setText("" + totalamountaftertax1);
                        } else if (jRadioButton6.isSelected()) {
                            totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()) + iv + sv + cv);
                            String totalamountaftertax1 = df.format(totalamountaftertax);
                            jTextField20.setText("" + totalamountaftertax1);
                        }
                        tiv = (tiv + iv);
                        tsv = (tsv + sv);
                        tcv = (tcv + cv);
                        DecimalFormat df1 = new DecimalFormat("0.00");

                        jTextField19.setText("" + df1.format(totalamountbeforetax));
                        jTextField2.setText("" + df1.format(tiv));
                        jTextField21.setText("" + df1.format(tsv));
                        jTextField22.setText("" + df1.format(tcv));

                        trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField2.getText()) + Double.parseDouble(jTextField21.getText()) + (Double.parseDouble(jTextField22.getText()))));

                        jTextField23.setText("" + df1.format(trv));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    sno = Integer.parseInt(jTextField14.getText());
                    sno++;
                    jTextField14.setText("" + sno);
                    jTextField5.setText(null);
                    jComboBox2.setSelectedIndex(0);
                    jTextField7.setText("0");
                    jTextField8.setText("1");
                    jTextField9.setText(null);
                    jTextField10.setText(null);
                    jTextField11.setText("0");
                    jTextField12.setText("0");
                    jTextField13.setText(null);
                    jComboBox1.setSelectedItem(st1);
                }
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField7.requestFocus();
        }

    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTextField7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField8.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox2.requestFocus();
        }
    }//GEN-LAST:event_jTextField7KeyPressed

    private void jTextField10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField10KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement sttt = connect.createStatement();
                ResultSet rsss = sttt.executeQuery("SELECT  * FROM Discount_type where Discounttype='" + disc_type + "'");
                while (rsss.next()) {
                    String method = rsss.getString(2);
                    if (method.equals("BUY-GET")) {
                        int getfree = Integer.parseInt("" + rsss.getString(5));

                    }
                    if (method.equals("PERCENTAGE")) {
                        double dis_per = Double.parseDouble("" + rsss.getString(3));
                        jTextField11.setText("" + dis_per);
                        double values = Double.parseDouble("" + jTextField9.getText());
                        jTextField12.setText("" + ((values * dis_per) / 100));

                        jTextField13.setText("" + (Double.parseDouble("" + jTextField9.getText()) - Double.parseDouble("" + jTextField12.getText())));
                    }
                    if (method.equals("MRP")) {
                        double disamnt = Double.parseDouble("" + rsss.getString(6)) - Double.parseDouble("" + rsss.getString(7));
                        jTextField12.setText("" + disamnt);
                        jTextField13.setText("" + (Double.parseDouble(jTextField9.getText()) - disamnt));
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jTextField10KeyPressed
    ArrayList snumber = new ArrayList();
    String st = null;
    ArrayList stock = new ArrayList();
    ArrayList prod = new ArrayList();
    ArrayList rate = new ArrayList();
    ArrayList qnty = new ArrayList();
    ArrayList value = new ArrayList();
    ArrayList disc_code = new ArrayList();
    ArrayList discper = new ArrayList();
    ArrayList discamt = new ArrayList();
    ArrayList total = new ArrayList();
    ArrayList staff = new ArrayList();
    ArrayList bill = new ArrayList();
    double per[] = new double[1000];
    int count = 0;
    public int sno = 1;
    double totalamount;
    double totalquantity;
    double totaldiscount;
    double totalamountaftertax;
    double totalamountbeforetax;
    double perc = 0;
    int q = 1;
    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        // TODO add your handling code here:

        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jTextField5.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jTextField20KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField20KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            Conform.setVisible(true);
        }
        if (key == evt.VK_ESCAPE) {
            jTextField19.requestFocus();
        }
    }//GEN-LAST:event_jTextField20KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            save();
        }

    }//GEN-LAST:event_jButton1KeyPressed
    ArrayList vat = new ArrayList();
    ArrayList ledgervat = new ArrayList();
    ArrayList ledgersgst = new ArrayList();
    ArrayList ledgervat2 = new ArrayList();
    ArrayList sgst = new ArrayList();
    ArrayList sgstledger = new ArrayList();
    ArrayList cgst = new ArrayList();
    ArrayList cgstledger = new ArrayList();
    ArrayList vatamount = new ArrayList();
    ArrayList vatcurrbal = new ArrayList();
    ArrayList vatcurrbaltype = new ArrayList();
    ArrayList updatevatcurrbal = new ArrayList();
    ArrayList updatevatcurrbaltype = new ArrayList();

    ArrayList sgstcurrbal = new ArrayList();
    ArrayList sgstcurrbaltype = new ArrayList();
    ArrayList updatesgstcurrbal = new ArrayList();
    ArrayList updatesgstcurrbaltype = new ArrayList();

    ArrayList cgstcurrbal = new ArrayList();
    ArrayList cgstcurrbaltype = new ArrayList();
    ArrayList updatecgstcurrbal = new ArrayList();
    ArrayList updatecgstcurrbaltype = new ArrayList();

    ArrayList salesaccount = new ArrayList();
    ArrayList salesaccount1 = new ArrayList();

    ArrayList Salesaccountcurrbal = new ArrayList();
    ArrayList Salesaccountcurrbaltype = new ArrayList();
    ArrayList Salesaccountcurrbal1 = new ArrayList();
    ArrayList Salesaccountcurrbaltype1 = new ArrayList();

    ArrayList Salesaccountupdatecurrbal = new ArrayList();
    ArrayList Salesaccountupdatecurrbaltype = new ArrayList();
    ArrayList cashaccountcurrbal = new ArrayList();
    ArrayList cashaccountcurrbaltype = new ArrayList();
    ArrayList cashaccountupdatecurrbal = new ArrayList();
    ArrayList cashaccountupdatecurrbaltype = new ArrayList();
    ArrayList sundrydebtorsaccountcurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountcurrbaltype = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbal = new ArrayList();
    ArrayList sundrydebtorsaccountupdatecurrbaltype = new ArrayList();

    double vatperc = 0;
    double vatamnt = 0;
    double amountbeforevat = 0;

    double sgstperc = 0;
    double sgstamnt = 0;
    double amountbeforesgst = 0;

    double cgstperc = 0;
    double cgstamnt = 0;
    double amountbeforecgst = 0;

    public void save() {
        int i1 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i1 == 0) {

            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            int rowcount = jTable1.getRowCount();
            for (int i = 0; i < rowcount; i++) {
                snumber.add(i, jTable1.getValueAt(i, 0));
                stock.add(i, jTable1.getValueAt(i, 1));
                prod.add(i, jTable1.getValueAt(i, 2));
                rate.add(i, jTable1.getValueAt(i, 3));
                qnty.add(i, jTable1.getValueAt(i, 4));
                value.add(i, jTable1.getValueAt(i, 5));
                disc_code.add(i, jTable1.getValueAt(i, 6));
                discper.add(i, jTable1.getValueAt(i, 7));
                discamt.add(i, jTable1.getValueAt(i, 8));
                total.add(i, jTable1.getValueAt(i, 9));
                staff.add(i, jTable1.getValueAt(i, 10));
                bill.add(i, jTable1.getValueAt(i, 11));

                Statement st = null;
                ResultSet rs = null;
                PreparedStatement pstm = null;
                int j = 0;
                int check = 0;
                String HELLO = "HELLO";
                String paytype = null;
                String to_ledger = null;
                String by_ledger = null;
                try {
                    connection c1 = new connection();
                    Connection connect = c1.cone();
                    Statement stin = connect.createStatement();
                    Statement del = connect.createStatement();
                    Statement del_1 = connect.createStatement();
                    Statement stmt1 = connect.createStatement();
                    Statement stmtvataccount = connect.createStatement();
                    Statement stru = connect.createStatement();

                    Statement stmt2 = connect.createStatement();
                    Statement stmtcgstaccount = connect.createStatement();

                    if (isIgstApplicable == true) {
                        ResultSet rsvat = stmt1.executeQuery("select IGST,ismeter from product where product_code='" + (String) prod.get(i) + "'");
                        while (rsvat.next()) {
                            ledgervat.add(i, rsvat.getString(1));
                            ismeter = rsvat.getInt("ismeter");

                        }
//                    String typeoftax = "VAT OUTPUT";
                        ResultSet rsvataccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgervat.get(i) + "' ");
                        while (rsvataccount.next()) {
                            vat.add(i, rsvataccount.getString(6));
                            vatcurrbal.add(i, rsvataccount.getString(10));
                            vatcurrbaltype.add(i, rsvataccount.getString(11));

                            vatperc = Double.parseDouble((String) vat.get(i));
                            if (jRadioButton5.isSelected()) {
                                amountbeforevat = (Double.parseDouble((String) total.get(i)) / (100 + vatperc)) * 100;
                            } else if (jRadioButton6.isSelected()) {
                                amountbeforevat = (Double.parseDouble((String) total.get(i)));
                            }
                            vatamnt = (vatperc * amountbeforevat) / 100;

                            DecimalFormat idf = new DecimalFormat("0.00");
                            igstamnt1_1 = idf.format(vatamnt);

                            String vatcurrbalance = (String) vatcurrbal.get(i);
                            String Curbaltypevat = (String) vatcurrbaltype.get(i);
                            double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                            double vatamnt1 = vatamnt;
                            double result;

                            if (Curbaltypevat.equals("CR")) {
                                result = vatcurrbalance1 - vatamnt1;
                                if (result >= 0) {
                                    updatevatcurrbaltype.add(i, "CR");
                                    updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));
                                } else {
                                    updatevatcurrbaltype.add(i, "DR");
                                    updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));
                                }

                            } else if (Curbaltypevat.equals("DR")) {

                                result = vatcurrbalance1 + vatamnt1;

                                updatevatcurrbaltype.add(i, "DR");
                                updatevatcurrbal.add(i, String.valueOf(Math.abs(result)));
                            }
                        }
                        sgstperc = 0;
                        sgstamnt = 0;
                        cgstperc = 0;
                        cgstamnt = 0;

                    } else if (isIgstApplicable == false) {
// For SGST                        
                        ResultSet rssgst = stmt1.executeQuery("select SGST,ismeter from product where product_code='" + (String) prod.get(i) + "'");
                        while (rssgst.next()) {
                            ledgersgst.add(i, rssgst.getString(1));
                            ismeter = rssgst.getInt("ismeter");
                            System.out.println(" is Meter status ---" + ismeter);
                        }
                        System.out.println("(String) ledgersgst.get(i)" + (String) ledgersgst.get(i));
                        ResultSet rssgstaccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgersgst.get(i) + "'");
                        while (rssgstaccount.next()) {
                            sgst.add(i, rssgstaccount.getString(6));
                            sgstcurrbal.add(i, rssgstaccount.getString(10));
                            sgstcurrbaltype.add(i, rssgstaccount.getString(11));
                            sgstperc = Double.parseDouble((String) sgst.get(i));

// For CGST                             
                            ResultSet rscgst = stmt2.executeQuery("select CGST from product where product_code='" + (String) prod.get(i) + "'");
                            while (rscgst.next()) {
                                ledgervat2.add(i, rscgst.getString(1));
                            }
                            System.out.println("Kuch Bhi Cgst" + (String) ledgervat2.get(i));
                            ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + (String) ledgervat2.get(i) + "'");
                            while (rscgstaccount.next()) {
                                cgst.add(i, rscgstaccount.getString(6));
                                cgstcurrbal.add(i, rscgstaccount.getString(10));
                                cgstcurrbaltype.add(i, rscgstaccount.getString(11));
                                cgstperc = Double.parseDouble((String) cgst.get(i));

// SGST Calculation                         
                                if (jRadioButton5.isSelected()) {
                                    amountbeforesgst = (Double.parseDouble((String) total.get(i)) / (100 + sgstperc + cgstperc)) * 100;
                                } else if (jRadioButton6.isSelected()) {
                                    amountbeforesgst = (Double.parseDouble((String) total.get(i)));
                                }
                                sgstamnt = (sgstperc * amountbeforesgst) / 100;

                                DecimalFormat sdf = new DecimalFormat("0.00");
                                sgstamnt1_1 = sdf.format(sgstamnt);

                                String vatcurrbalance = (String) sgstcurrbal.get(i);
                                String Curbaltypesgst = (String) sgstcurrbaltype.get(i);
                                double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                                double sgstamnt1 = sgstamnt;
                                double result;
                                if (Curbaltypesgst.equals("DR")) {
                                    result = vatcurrbalance1 + sgstamnt1;
                                    if (result >= 0) {
                                        updatesgstcurrbaltype.add(i, "DR");
                                        updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                    } else {
                                        updatesgstcurrbaltype.add(i, "CR");
                                        updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));
                                    }

                                } else if (Curbaltypesgst.equals("CR")) {

                                    result = vatcurrbalance1 - sgstamnt1;

                                    updatesgstcurrbaltype.add(i, "CR");
                                    updatesgstcurrbal.add(i, String.valueOf(Math.abs(result)));

                                }

//CGST Calculation            
                                if (jRadioButton5.isSelected()) {
                                    amountbeforecgst = (Double.parseDouble((String) total.get(i)) / (100 + sgstperc + cgstperc)) * 100;
                                } else if (jRadioButton6.isSelected()) {
                                    amountbeforecgst = (Double.parseDouble((String) total.get(i)));
                                }
                                cgstamnt = (cgstperc * amountbeforecgst) / 100;

                                DecimalFormat cdf = new DecimalFormat("0.00");
                                cgstamnt1_1 = cdf.format(cgstamnt);

                                String cgstcurrbalance = (String) cgstcurrbal.get(i);
                                String Curbaltypecgst = (String) cgstcurrbaltype.get(i);
                                double vatcurrbalance2 = Double.parseDouble(cgstcurrbalance);
                                double cgstamnt1 = cgstamnt;
                                double result1;
                                if (Curbaltypecgst.equals("DR")) {
                                    result1 = vatcurrbalance2 + cgstamnt1;
                                    if (result1 >= 0) {
                                        updatecgstcurrbaltype.add(i, "DR");
                                        updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                    } else {
                                        updatecgstcurrbaltype.add(i, "CR");
                                        updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));
                                    }

                                } else if (Curbaltypecgst.equals("CR")) {

                                    result1 = vatcurrbalance2 - cgstamnt1;

                                    updatecgstcurrbaltype.add(i, "CR");
                                    updatecgstcurrbal.add(i, String.valueOf(Math.abs(result1)));

                                }
                            }

                        }

                        vatperc = 0;
                        vatamnt = 0;

                    }

                    Statement Salesaccount = connect.createStatement();
                    Statement Salesaccount2 = connect.createStatement();
                    Statement Cashaccount = connect.createStatement();
                    Statement sundrydebtorsaccount = connect.createStatement();
                    String Sales = "SALES ACCOUNTS";
                    String Salesgrp = null;

                    if (isIgstApplicable == true) {
                        ResultSet rssalesaccount = Salesaccount.executeQuery("Select Sales_account from product where product_code='" + prod.get(i) + "' ");
                        while (rssalesaccount.next()) {
                            salesaccount.add(i, rssalesaccount.getString(1));
//                            System.out.println("" + salesaccount.get(i));
                        }
                        ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + (String) salesaccount.get(i) + "'");
                        while (rssales.next()) {
                            Salesaccountcurrbal.add(i, rssales.getString(10));
                            Salesaccountcurrbaltype.add(i, rssales.getString(11));
                        }
                    } else if (isIgstApplicable == false) {
                        ResultSet rssalesaccount = Salesaccount.executeQuery("Select Sales_account from product where product_code='" + prod.get(i) + "' ");
                        while (rssalesaccount.next()) {
                            salesaccount.add(i, rssalesaccount.getString(1));
                        }
                        ResultSet rssales = Salesaccount2.executeQuery("select * from ledger where ledger_name='" + (String) salesaccount.get(i) + "'");
                        while (rssales.next()) {
                            System.out.println("value os salescurrbal type is " + rssales.getString(11));
                            Salesaccountcurrbal.add(i, rssales.getString(10));
                            Salesaccountcurrbaltype.add(i, rssales.getString(11));
                        }
                    }

                    String c = "CASH";
                    ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='" + c + "'");
                    while (rscashaccount.next()) {
                        cashaccountcurrbal.add(i, rscashaccount.getString(10));
                        cashaccountcurrbaltype.add(i, rscashaccount.getString(11));
                    }
                    ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + sundrydebtors + "'");
                    while (rssundrydebtors.next()) {
                        sundrydebtorsaccountcurrbal.add(i, rssundrydebtors.getString(10));
                        sundrydebtorsaccountcurrbaltype.add(i, rssundrydebtors.getString(11));
                    }
                    if (jRadioButton1.isSelected()) {
                        jTextField4.setText(null);
                        paytype = jRadioButton1.getLabel();
                        String salesaccountamnt = (String) Salesaccountcurrbal.get(i);
                        String cashaccountamount = (String) cashaccountcurrbal.get(i);
                        double totalamnt = Double.parseDouble((String) total.get(i));
                        double totalamntreducingvat = totalamnt - (vatamnt + sgstamnt + cgstamnt);
                        double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
                        double cashaccountamount1 = Double.parseDouble(cashaccountamount);
                        double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);
                        if (Salesaccountcurrbaltype.get(i).equals("CR")) {
                            if (salesaccountupdatedamount > 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("CR");

                            }
                            if (salesaccountupdatedamount <= 0) {
                                Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add("DR");
                            }
                        } else if (Salesaccountcurrbaltype.get(i).equals("DR")) {
                            salesaccountupdatedamount = salesaccountamnt1 + totalamntreducingvat;
                            Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                            Salesaccountupdatecurrbaltype.add("DR");
                            System.out.println("" + Salesaccountupdatecurrbal.get(i));
                            System.out.println("" + Salesaccountupdatecurrbal.get(i));
                        }
                        if (cashaccountcurrbaltype.get(i).equals("DR")) {
                            double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                            if (cashaccountupdateamount > 0) {
                                cashaccountupdatecurrbaltype.add("DR");
                                cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                            }
                            if (cashaccountupdateamount <= 0) {
                                cashaccountupdatecurrbaltype.add("CR");
                                cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                            }
                        }
                        if (cashaccountcurrbaltype.get(i).equals("CR")) {
                            double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                            cashaccountupdatecurrbaltype.add("CR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }

                        to_ledger = c;
                        by_ledger = (String) salesaccount.get(i);

                    }
                    if (jRadioButton2.isSelected()) {
                        paytype = jRadioButton2.getLabel();
                        String salesaccountamnt = (String) Salesaccountcurrbal.get(i);
                        String sundrydebtorsamnt = (String) sundrydebtorsaccountcurrbal.get(i);
                        double totalamnt = Double.parseDouble((String) total.get(i));
                        double totalamntreducingvat = totalamnt - (vatamnt + sgstamnt + cgstamnt);
                        double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
                        double sundrydebtorsamnt1 = Double.parseDouble(sundrydebtorsamnt);
                        double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);
                        if (Salesaccountcurrbaltype.get(i) == "DR") {
                            if (salesaccountupdatedamount > 0) {
                                Salesaccountupdatecurrbal.add(i, Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add(i, "DR");
                            }
                            if (salesaccountupdatedamount <= 0) {
                                Salesaccountupdatecurrbal.add(i, Math.abs(salesaccountupdatedamount));
                                Salesaccountupdatecurrbaltype.add(i, "CR");
                            }
                        }
                        if (Salesaccountcurrbaltype.get(i).equals("CR")) {
                            salesaccountupdatedamount = salesaccountamnt1 - totalamntreducingvat;
                            Salesaccountupdatecurrbal.add(i, Math.abs(salesaccountupdatedamount));
                            Salesaccountupdatecurrbaltype.add(i, "CR");
                        }
                        if (sundrydebtorsaccountcurrbaltype.get(i).equals("CR")) {
                            double sundrydebtorsccountupdateamount = sundrydebtorsamnt1 + totalamnt;
                            if (sundrydebtorsccountupdateamount > 0) {
                                sundrydebtorsaccountupdatecurrbaltype.add(i, "CR");
                                sundrydebtorsaccountupdatecurrbal.add(i, Math.abs(sundrydebtorsccountupdateamount));
                            }
                            if (sundrydebtorsccountupdateamount <= 0) {
                                sundrydebtorsaccountupdatecurrbaltype.add(i, "DR");
                                sundrydebtorsaccountupdatecurrbal.add(i, Math.abs(sundrydebtorsccountupdateamount));
                            }
                        }
                        if (sundrydebtorsaccountcurrbaltype.get(i).equals("DR")) {
                            double cashaccountupdateamount = sundrydebtorsamnt1 - totalamnt;
                            sundrydebtorsaccountupdatecurrbaltype.add(i, "DR");
                            sundrydebtorsaccountupdatecurrbal.add(i, Math.abs(cashaccountupdateamount));
                        }
                        to_ledger = sundrydebtors;
                        by_ledger = (String) salesaccount.get(i);
                        System.out.println("sundry debtors is " + sundrydebtors);
                    }
                    try {
                        ResultSet rsparty = null;
                        rsparty = stmt1.executeQuery("Select party_code from billing where stock_no='" + jTable1.getValueAt(i, 1) + "" + "'");
                        if (!rsparty.isBeforeFirst()) {
                            rsparty = stmt1.executeQuery("Select party_code from billing_old where stock_no='" + jTable1.getValueAt(i, 1) + "" + "'");
                        }
                        while (rsparty.next()) {
                            party.add(rsparty.getString(1));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String date;
                    date = formatter.format(jDateChooser1.getDate());

                    if (isIgstApplicable == true) {
                        stmt1.executeUpdate("insert into salesreturn values('" + (jTextField1.getText()) + "','" + date + "',"
                                + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','" + igstamnt1_1 + "','" + (String) ledgervat.get(i) + "','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','0' , 'SGST 0%' ,'0','CGST 0%','" + jTextField23.getText() + "','" + jTextField2.getText() + "','" + jTextField21.getText() + "','" + jTextField22.getText() + "','" + bill.get(i) + "','" + tax_type + "')");

                        String q = "\"insert into salesreturn values('" + (jTextField1.getText()) + "','" + date + "',"
                                + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','" + igstamnt1_1 + "','" + (String) ledgervat.get(i) + "','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "',' '0' , 'SGST 0%' ,'0','CGST 0%','" + jTextField23.getText() + "','" + jTextField2.getText() + "','" + jTextField21.getText() + "','" + jTextField22.getText() + "','" + bill.get(i) + "','" + tax_type + "')\"";

                        stmt1.executeUpdate("insert into queries values(" + q + ")");
                        stmt1.executeUpdate("insert into queries1 values(" + q + ")");

                    } else if (isIgstApplicable == false) {
                        stmt1.executeUpdate("insert into salesreturn values('" + (jTextField1.getText()) + "','" + date + "',"
                                + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','0','IGST 0%','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','" + sgstamnt1_1 + "','" + (String) ledgersgst.get(i) + "','" + cgstamnt1_1 + "','" + (String) ledgervat2.get(i) + "','" + jTextField23.getText() + "','" + jTextField2.getText() + "','" + jTextField21.getText() + "','" + jTextField22.getText() + "','" + bill.get(i) + "','" + tax_type + "')");

                        String q = "\"insert into salesreturn values('" + (jTextField1.getText()) + "','" + date + "',"
                                + " '" + paytype + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + snumber.get(i) + "',"
                                + " '" + ((String) stock.get(i)) + "','" + prod.get(i) + "','" + rate.get(i) + "','" + qnty.get(i) + "','" + value.get(i) + "','" + disc_code.get(i) + "',"
                                + "'" + discper.get(i) + "','" + discamt.get(i) + "','" + total.get(i) + "','" + staff.get(i) + "','" + jTextField15.getText() + "',"
                                + "'" + jTextField16.getText() + "','" + jTextField17.getText() + "','" + jTextField18.getText() + "',"
                                + "'" + jTextField19.getText() + "','" + jTextField20.getText() + "','0','IGST 0%','" + to_ledger + "','" + by_ledger + "','0','" + party.get(i) + "','" + sgstamnt1_1 + "','" + (String) ledgersgst.get(i) + "','" + cgstamnt1_1 + "','" + (String) ledgervat2.get(i) + "','" + jTextField23.getText() + "','" + jTextField2.getText() + "','" + jTextField21.getText() + "','" + jTextField22.getText() + "','" + bill.get(i) + "','" + tax_type + "')\"";

                        stmt1.executeUpdate("insert into queries values(" + q + ")");
                        stmt1.executeUpdate("insert into queries1 values(" + q + ")");

                    }

                    String stockno = null;

                    {
                        ResultSet rs9 = del.executeQuery("select * from stockrecieved where Stock_no='" + ((String) stock.get(i)) + "'");
                        while (rs9.next()) {

                            stockno = rs9.getString(1);

                            stmt1.executeUpdate("insert into Stockid2(Stock_No,Ra_te,Re_tail,De_sc,P_Desc,byforgation,Pro_duct,articleno,sto_date,supplier_name,branchid) values('" + rs9.getString("Stock_no") + "','" + rs9.getString("Ra_te") + "','" + rs9.getString("ret_ail") + "','" + rs9.getString("desc_ription") + "','" + rs9.getString("prod_desc") + "','" + rs9.getString("Byforgation") + "','" + rs9.getString("Produ_ct") + "','','" + rs9.getString("St_Date") + "','" + party.get(i) + "" + "','" + rs9.getString("vsn") + "') ");

                            String insertvalue = "\"insert into Stockid2(Stock_No,Ra_te,Re_tail,De_sc,P_Desc,byforgation,Pro_duct,articleno,sto_date,supplier_name,branchid) values('" + rs9.getString("Stock_no") + "','" + rs9.getString("Ra_te") + "','" + rs9.getString("ret_ail") + "','" + rs9.getString("desc_ription") + "','" + rs9.getString("prod_desc") + "','" + rs9.getString("Byforgation") + "','" + rs9.getString("Produ_ct") + "','','" + rs9.getString("St_Date") + "','" + party.get(i) + "" + "','" + rs9.getString("vsn") + "') \"";
                            stmt1.executeUpdate("insert into queries values(" + insertvalue + ")");
                            stmt1.executeUpdate("insert into queries1 values(" + insertvalue + ")");

                        }

                    }
                    if (stockno == null) {
                        ResultSet rs10 = null;
                        if (ismeter == 0) {
                            rs10 = del_1.executeQuery("select * from Stockid where Stock_No='" + ((String) stock.get(i)) + "'");
                            if (!rs10.isBeforeFirst()) {
                                rs10 = del_1.executeQuery("select * from Stockid_old where Stock_No='" + ((String) stock.get(i)) + "'");
                            }
                            while (rs10.next()) {
                                stmt1.executeUpdate("insert into Stockid2(Stock_No,Ra_te,Re_tail,De_sc,P_Desc,byforgation,Pro_duct,articleno,sto_date,supplier_name,branchid,ismeter,qnt) values('" + rs10.getString("Stock_No") + "','" + rs10.getString("Ra_te") + "','" + rs10.getString("Re_tail") + "','" + rs10.getString("De_sc") + "','" + rs10.getString("P_Desc") + "','" + rs10.getString("byforgation") + "','" + rs10.getString("Pro_duct") + "','" + rs10.getString("articleno") + "','" + rs10.getString("Da_te") + "','" + rs10.getString("Party") + "','" + rs10.getString("branchid") + "',"+ismeter+",'"+(String)jTable1.getValueAt(i, 4)+"') ");
                                String insertvalue = "\"insert into Stockid2(Stock_No,Ra_te,Re_tail,De_sc,P_Desc,byforgation,Pro_duct,articleno,sto_date,supplier_name,branchid,ismeter,qnt) values('" + rs10.getString("Stock_No") + "','" + rs10.getString("Ra_te") + "','" + rs10.getString("Re_tail") + "','" + rs10.getString("De_sc") + "','" + rs10.getString("P_Desc") + "','" + rs10.getString("byforgation") + "','" + rs10.getString("Pro_duct") + "','" + rs10.getString("articleno") + "','" + rs10.getString("da_te") + "','" + rs10.getString("Party") + "','" + rs10.getString("branchid") + "',"+ismeter+",'"+(String)jTable1.getValueAt(i, 4)+"') \"";
                                stmt1.executeUpdate("insert into queries values(" + insertvalue + ")");
                                stmt1.executeUpdate("insert into queries1 values(" + insertvalue + ")");
                            }
                        }
                        if (ismeter == 1) {
                            int sel_row = jTable2.getSelectedRow();
                            double qnty_ = 0;
                            double sale_qnty = 0;
                            Statement bill_st = connect.createStatement();
                            ResultSet bill_rs = null;
                            bill_rs = bill_st.executeQuery("Select * from billing where Stock_No = '" + ((String) stock.get(i)) + "' and bill_refrence = '" + (jTable2.getValueAt(sel_row, 0) + "") + "'");
                            if (!bill_rs.isBeforeFirst()) {
                                bill_st.executeQuery("Select * from billing_old where Stock_No = '" + ((String) stock.get(i)) + "' and bill_refrence = '" + (jTable2.getValueAt(sel_row, 0) + "") + "'");
                            }
                            while (bill_rs.next()) {
                                sale_qnty = bill_rs.getDouble("qnty");
                            }
                            Statement meter_st = connect.createStatement();
                            ResultSet meter_rs = meter_st.executeQuery("Select * from stockid2 where Stock_No = '" + ((String) stock.get(i)) + "'");
                            while (meter_rs.next()) {
                                qnty_ = meter_rs.getDouble("qnt");
                            }
                            double rev_qnty = qnty_ + sale_qnty;
                            Statement stmt4 = connect.createStatement();
                            stmt4.executeUpdate("Update stockid2 set qnt = '" + rev_qnty + "' where Stock_No = '" + ((String) stock.get(i)) + "'");
                        }

                    }

                    if (isIgstApplicable == true) {
                        if (Integer.parseInt(vat.get(i) + "") != 0) {
                            DecimalFormat dfv = new DecimalFormat("0.00");
                            String dfvat = dfv.format(Double.parseDouble(updatevatcurrbal.get(i).toString()));

                            stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfvat + "',currbal_type='" + (String) updatevatcurrbaltype.get(i) + "' where ledger_name='" + ledgervat.get(i) + "'");

                            String q1 = "\"Update ledger SET curr_bal='" + igstamnt1_1 + "',currbal_type='DR' where ledger_name='" + ledgervat.get(i) + "'\"";
                            stmt1.executeUpdate("insert into queries values(" + q1 + ")");
                            stmt1.executeUpdate("insert into queries1 values(" + q1 + ")");

                        }
                    } else if (isIgstApplicable == false) {

                        if (sgst.get(i) != null && cgst.get(i) != null) {

                            DecimalFormat df = new DecimalFormat("0.00");
                            String dfsgst = df.format(Double.parseDouble(updatesgstcurrbal.get(i).toString()));
                            String dfcgst = df.format(Double.parseDouble(updatecgstcurrbal.get(i).toString()));

                            stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfsgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledgersgst.get(i) + "" + "' ");
                            stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfcgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(i) + "" + "' where ledger_name = '" + (String) ledgervat2.get(i) + "" + "' ");

                            String q1 = "\"Update ledger SET curr_bal='" + sgstamnt1_1 + "',currbal_type='DR' where ledger_name='" + ledgersgst.get(i) + "'\"";
                            stmt1.executeUpdate("insert into queries values(" + q1 + ")");
                            stmt1.executeUpdate("insert into queries1 values(" + q1 + ")");

                            String q_1 = "\"Update ledger SET curr_bal='" + cgstamnt1_1 + "',currbal_type='DR' where ledger_name='" + ledgervat2.get(i) + "'\"";
                            stin.executeUpdate("insert into queries values(" + q_1 + ")");
                            stin.executeUpdate("insert into queries1 values(" + q_1 + ")");

                        }
                    }

                    System.out.println("vatcurrbaltype" + updatevatcurrbaltype);
                    DecimalFormat df1 = new DecimalFormat("0.00");
                    String dfsales = df1.format(Double.parseDouble(Salesaccountupdatecurrbal.get(i).toString()));
//                    String dfcgst = df.format(Double.parseDouble(updatecgstcurrbal.get(i).toString()));

                    stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfsales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'");

                    String q2 = "\"UPDATE ledger SET curr_bal='" + dfsales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + salesaccount.get(i) + "" + "'\"";

                    stmt1.executeUpdate("insert into queries values(" + q2 + ")");
                    stmt1.executeUpdate("insert into queries1 values(" + q2 + ")");

                    if (jRadioButton1.isSelected()) {
                        stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + cashaccountupdatecurrbal.get(i) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + c + "'");
                        String q3 = "\"UPDATE ledger SET curr_bal='" + cashaccountupdatecurrbal.get(i) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + c + "'\"";
                        stmt1.executeUpdate("insert into queries values(" + q3 + ")");
                        stmt1.executeUpdate("insert into queries1 values(" + q3 + ")");
                    }

                    if (jRadioButton2.isSelected()) {
//1
                        stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + sundrydebtorsaccountupdatecurrbal.get(i) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + sundrydebtors + "'");
                        String q4 = "\"UPDATE ledger SET curr_bal='" + sundrydebtorsaccountupdatecurrbal.get(i) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(i) + "" + "' where ledger_name='" + sundrydebtors + "'\"";
                        stmt1.executeUpdate("insert into queries values(" + q4 + ")");
                        stmt1.executeUpdate("insert into queries1 values(" + q4 + ")");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            try {
//calculating round of value  
                connection c = new connection();
                Connection connect = c.cone();
                Statement stru = connect.createStatement();
                Statement round_st = connect.createStatement();
                ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
                while (round_rs.next()) {
                    double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                    Round_currbal_type = round_rs.getString("currbal_type");
                    double round_total = (Double.parseDouble(jTextField15.getText()) - ((Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField2.getText())) + Double.parseDouble(jTextField21.getText()) + Double.parseDouble(jTextField22.getText())));

                    if (Round_currbal_type.equals("DR") && round_total < 0) {
                        diff_total = Round_curr_bal - Math.abs(round_total);
                        if (diff_total > 0) {
                            Round_currbal_type = "DR";
                            diff_total = (Math.abs(diff_total));
                        } else if (diff_total < 0) {
                            Round_currbal_type = "CR";
                            diff_total = (Math.abs(diff_total));
                        }
                    } else if (Round_currbal_type.equals("DR") && round_total > 0) {
                        diff_total = Round_curr_bal + Math.abs(round_total);

                    }
                    if (Round_currbal_type.equals("CR") && round_total < 0) {
                        diff_total = Round_curr_bal + Math.abs(round_total);

                    } else if (Round_currbal_type.equals("CR") && round_total > 0) {
                        diff_total = Round_curr_bal - Math.abs(round_total);

                        if (diff_total > 0) {
                            Round_currbal_type = "DR";
                            diff_total = (Math.abs(diff_total));
                        } else if (diff_total < 0) {
                            Round_currbal_type = "CR";
                            diff_total = (Math.abs(diff_total));
                        }
                    }
                    DecimalFormat dr1 = new DecimalFormat("0.00");
                    diff_total_round = dr1.format(diff_total);
                }

                //Updating round off ledger in ledger table       
                stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round + "', currbal_type = '" + Round_currbal_type + "' where ledger_name = 'Round Off'");

            } catch (Exception e) {
                e.printStackTrace();
            }

            int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
            if (i2 == 0) {
                PrinterJob job = PrinterJob.getPrinterJob();
                PageFormat pf = new PageFormat();
                Paper paper = new Paper();
                if (Company.equals("Khabiya")) {
                    paper.setSize(5.8d * 72, 8.3d * 72);
                    double margin = 40;
                    paper.setImageableArea(margin, margin, (paper.getWidth() - 0) * 1, paper.getHeight() - margin * 2);
                } else if (Company.equals("Mahavir")) {
                    paper.setSize(5.8d * 72, 8.3d * 72);
                    double margin = 40;
                    paper.setImageableArea(margin, margin, (paper.getWidth() - 0) * 1, paper.getHeight() - margin * 2);
                }
                pf.setPaper(paper);
                job.setPrintable((Printable) this, pf);
                PrintService service = PrintServiceLookup.lookupDefaultPrintService();

                try {
                    job.setPrintService(service);
                    if (Company.equals("Khabiya")) {
                        job.setCopies(2);
                    } else if (Company.equals("Mahavir")) {
                        job.setCopies(1);
                    }
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                    ex.printStackTrace();
                }
            }

            Conform.dispose();
            jDialog2.dispose();
            this.dispose();
            new New_Dual_SalesReturn().setVisible(true);
        }
    }

    /*Printing Format Code For all Client*/
//----------------------For Khabiya---------------------------------------------
    public void khabiyaPrint(Graphics g, PageFormat pf, int page) {
        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fontMetrics = g2d.getFontMetrics();

        Font font = new Font(" Bill NO - SL/1718/1", Font.BOLD, 12);
        g2d.setFont(font);
        g2d.drawString(" Khabiya Cloth Store ", 150, 50);

        Font font2 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 10);
        g2d.setFont(font2);

        g2d.drawString(" 37, New Road ", 150, 60);
        g2d.drawString(" Ratlam - -457001(M.P) ", 150, 70);
        g2d.drawString(" Phone No.- 07412-492939", 150, 80);

        // Horizontal Line            
        g2d.drawLine(0, 85, 420, 85);

        // Left Side box
        g2d.drawString("Invoice No  : ", 15, 100);
        g2d.drawString("Invoice Date : ", 15, 115);
        g2d.drawString("GSTIN : 23AMDPK9264H1ZZ", 15, 130);

        // Virtical Line
        g2d.drawLine(150, 85, 150, 140);

        // Right Side box
        g2d.drawString("Name  :  ", 160, 95);
        g2d.drawString("Address :  ", 160, 105);
        g2d.drawString("Mobile No :  ", 160, 125);
        g2d.drawString("GSTIN No :  ", 160, 135);

        // Horizontal Line            
        g2d.drawLine(0, 140, 420, 140);
        Font font3 = new Font(" Bill NO - SL/1718/1", Font.PLAIN, 8);
        g2d.setFont(font3);

        //Table Header View
        g2d.drawString("S No", 15, 150);
        g2d.drawString("  Particulars  ", 50, 150);
        //            g2d.drawString(" Code ", 165, 150);
        g2d.drawString("HSN Code", 220, 150);
        g2d.drawString("Rate", 290, 150);
        g2d.drawString(" Amount", 360, 150);
        g2d.drawString("Total - ", 280, 450);

        g2d.drawString("Net Amount - ", 280, 490);

        // Draw Horizontal line after table
        g2d.drawLine(0, 160, 420, 160);
        g2d.drawLine(0, 440, 420, 440);
        g2d.drawLine(270, 440, 270, 500); // Verticle Line 
        g2d.drawLine(270, 480, 420, 480);
        g2d.drawLine(270, 500, 420, 500);

        // For terms and Condition 
        g2d.drawString("Terms and Condition - ", 15, 477);
        g2d.drawString("1) No Exchange No Claim", 15, 490);
        g2d.drawString("2) Subject to Ratlam jurisdiction", 15, 500);

        //For tax calculation
        g2d.drawString("Tax Calculation - ", 15, 520);
        g2d.drawString("IGST - ", 15, 535);
        g2d.drawString("SGST - ", 85, 535);
        g2d.drawString("CGST - ", 170, 535);

        // For Authority Signatury
        g2d.drawString("For Khabiya :", 320, 555);
        // Getting Data For printing 

        // For upper left side box   
        g2d.drawString(jTextField1.getText(), 75, 100);
        String Date = formatter1.format(jDateChooser1.getDate());
        g2d.drawString(Date, 80, 115);

        // For Uper Right Side Box
        if (!jTextField4.getText().isEmpty()) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select * from party_sales where party_code='" + jTextField4.getText() + "'");
                while (rs.next()) {
                    g2d.drawString(rs.getString("party_name"), 210, 95);
                    g2d.drawString(rs.getString("address1") + "" + rs.getString("address2"), 210, 105);
                    g2d.drawString(rs.getString("address3") + "" + rs.getString("address4"), 210, 105);
                    g2d.drawString(rs.getString("mobileno"), 215, 125);
                    g2d.drawString(rs.getString("gstin_no"), 215, 135);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!jTextField3.getText().isEmpty()) {
            g2d.drawString(jTextField3.getText(), 210, 100);
            //               g2d.drawString(jTextField37.getText(), 215, 125);
        }
        // Getting Table data
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        int rowcount = dtm.getRowCount();
        String total_discount = "";
        String total_after_tax = "";
        String total_igsttax = "";
        String total_sgsttax = "";
        String total_cgsttax = "";
        String By_Cash = "";
        String By_Debit = "";
        String By_Card = "";

//            double sr_amt = 0;
//
//            int yaxis = 200;
//
//            int next = 0;
        DecimalFormat formatter2 = new DecimalFormat("0.00");

        for (int i = 0; i < rowcount; i++) {
            int j = 10;
            g2d.drawString(jTable1.getValueAt(i, 0) + "", 15, 170 + (j * i)); // Serial No
            g2d.drawString(jTable1.getValueAt(i, 1) + "", 165, 170 + (j * i)); // Stock No
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(jTable1.getValueAt(i, 3) + ""))) + "", 280, 170 + (j * i));// For Rate
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(jTable1.getValueAt(i, 5) + ""))) + "", 340, 170 + (j * i));// For Amount

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                ResultSet rs2 = st1.executeQuery("select * from product where product_code='" + jTable1.getValueAt(i, 2) + "" + "' ");
                while (rs2.next()) {
                    g2d.drawString(rs2.getString("HSL_CODE"), 220, 170 + (j * i)); // For HSN COde
                    g2d.drawString(rs2.getString("print_On"), 35, 170 + (j * i)); // Product Name
                    ismeter = rs2.getInt("ismeter");
                }
                if (ismeter == 1) {
                    g2d.drawString(jTable1.getValueAt(i, 4) + "" + " (M) ", 250, 170 + (j * i));
                }

                Statement st2 = connect.createStatement();
                ResultSet rs3 = st2.executeQuery("Select * from salesreturn where bill_refrence='" + jTextField1.getText() + "'");
                while (rs3.next()) {
                    total_igsttax = rs3.getString("total_igst_taxamnt");
                    total_sgsttax = rs3.getString("total_sgst_taxamnt");
                    total_cgsttax = rs3.getString("total_cgst_taxamnt");
                    total_discount = rs3.getString("total_disc_amnt");
                    total_after_tax = rs3.getString("total_value_after_tax");
//                        By_Cash = rs3.getString("cashamount");
//                        By_Debit = rs3.getString("sundrydebtorsamount");
//                        By_Card = rs3.getString("cardamount");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
// For Sales Return
//                String sr_ref = (String) jComboBox3.getSelectedItem();
//
//                if (i == (rowcount - 1)) {
//                    next = i + 1;
//                    yaxis = i + 210 + (j * i);
//                    if (next > i) {
//
//                        if (!jComboBox3.getSelectedItem().toString().equals("Select")) {
//                            System.out.println("sr_ref" + sr_ref);
//                            g2d.drawLine(0, yaxis, 420, yaxis);
//                            g2d.drawString("Sales Return ", 180, yaxis + 10);
//                            g2d.drawString(sr_ref, 50, yaxis + 10);
//                            g2d.drawLine(0, yaxis + 20, 420, yaxis + 20);
//
//                            String total_srigst = "";
//                            String total_srsgst = "";
//                            String total_srcgst = "";
//
//                            for (int p = 0; p < rowcount; p++) {
//                                try {
//                                    ArrayList serial_no = new ArrayList();
//                                    ArrayList Stock_no = new ArrayList();
//                                    ArrayList Stock_print = new ArrayList();
//                                    ArrayList hsl_code = new ArrayList();
//                                    ArrayList srate = new ArrayList();
//                                    ArrayList samount = new ArrayList();
//                                    connection c = new connection();
//                                    Connection connect = c.cone();
//                                    Statement st1 = connect.createStatement();
//
//                                    ResultSet rs1 = st1.executeQuery("Select * from salesreturn where bill_refrence = '" + sr_ref + "" + "' ");
//                                    while (rs1.next()) {
//                                        serial_no.add(rs1.getString("sno"));
//                                        Stock_no.add(rs1.getString("stock_no"));
//                                        samount.add(rs1.getDouble("total"));
//                                        srate.add(rs1.getDouble("value"));
//                                        sr_amt = rs1.getDouble("total_amnt");
//
//                                        total_srigst = rs1.getString("total_igst_taxamnt");
//                                        total_srsgst = rs1.getString("total_sgst_taxamnt");
//                                        total_srcgst = rs1.getString("total_cgst_taxamnt");
//
//                                        Statement st2 = connect.createStatement();
//                                        ResultSet rs2 = st2.executeQuery("select * from product where product_code='" + rs1.getString("description") + "' ");
//                                        while (rs2.next()) {
//                                            Stock_print.add(rs2.getString("print_on"));
//                                            hsl_code.add(rs2.getString("HSL_CODE"));
//                                        }
//                                    }
//
//                                    g2d.drawString(serial_no.get(p) + "", 5, yaxis + 30 + (j * p));
//                                    g2d.drawString(Stock_no.get(p) + "", 165, yaxis + 30 + (j * p));
//                                    g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(srate.get(p) + ""))), 280, yaxis + 30 + (j * p));
//                                    g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(samount.get(p) + ""))), 340, yaxis + 30 + (j * p));
//
//                                    g2d.drawString(Stock_print.get(p) + "", 35, yaxis + 30 + (j * p));
//                                    g2d.drawString(hsl_code.get(p) + "", 220, yaxis + 30 + (j * p));
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            g2d.drawString("Sale Return - ", 280, 460);
//                            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(sr_amt + ""))), 340, 460);
//
//                            g2d.drawLine(0, yaxis + 55 + i, 420, yaxis + 55 + i);
//
////For Sales return tax calculation
//                            g2d.drawString("Sale Return Tax - ", 15, 545);
//                            g2d.drawString("IGST - ", 15, 555);
//                            g2d.drawString("SGST - ", 85, 555);
//                            g2d.drawString("CGST - ", 170, 555);
//
//                            g2d.drawString(total_srigst + "", 40, 555);// For IGST amount
//                            g2d.drawString(total_srsgst + "", 115, 555);// For SGST amount
//                            g2d.drawString(total_srcgst + "", 200, 555);// For CGST amount
//
//                        }
//                    }
//                    System.out.println("Value of next is " + next);
//                }

        }

        if (!total_discount.equals("0.00")) {
            g2d.drawString("Discount     - ", 280, 470);
            g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(total_discount + ""))), 340, 470);// For Discount amount
        }

        // For Mode of payment
        g2d.drawString("Mode of Payment - ", 15, 450);

//            if (!By_Cash.equals("0") && By_Debit.equals("0") && By_Card.equals("0")) {
//                g2d.drawString("By Cash  - ", 15, 463);
//            } else if (!By_Debit.equals("0") && By_Cash.equals("0") && By_Card.equals("0")) {
//                g2d.drawString("By Debit  - ", 15, 463);
//            } else if (!By_Card.equals("0") && By_Cash.equals("0") && By_Debit.equals("0")) {
//                g2d.drawString("By Card  - ", 15, 463);
//            } else {
//                if (!By_Cash.equals("0")) {
//                    g2d.drawString("By Cash  - ", 15, 463);
//                    g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Cash + ""))), 35, 463);// For By cash amount
//                }
//
//                if (!By_Debit.equals("0")) {
//                    g2d.drawString("By Debit  - ", 85, 463);
//                    g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Debit + ""))), 105, 463);// For By debit amount
//                }
//
//                if (!By_Card.equals("0")) {
//                    g2d.drawString("By Card  - ", 170, 463);
//                    g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(By_Card + ""))), 190, 463);// For By Card amount
//                }
//            }
        double amnt_after_discount = Double.parseDouble(jTextField15.getText()) - Double.parseDouble(total_discount);
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(jTextField15.getText() + ""))), 340, 450);// For Total amount
        g2d.drawString(String.format("%15s", formatter2.format(Double.parseDouble(amnt_after_discount + ""))), 340, 490);

        g2d.drawString(total_igsttax + "", 40, 535);// For IGST amount
        g2d.drawString(total_sgsttax + "", 115, 535);// For SGST amount
        g2d.drawString(total_cgsttax + "", 200, 535);// For CGST amount

        System.out.println("printing");
    }
//------------------------------------------------------------------------------ 
//-----------------------For Mahavir Printing-----------------------------------

    public void mahavirPrint(Graphics g, PageFormat pf, int page) {
    }
//------------------------------------------------------------------------------    

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        save();
    }//GEN-LAST:event_jButton1ActionPerformed
    @Override
    public int print(Graphics g, PageFormat pf, int page) throws PrinterException {
        if (page > 0) {
            return NO_SUCH_PAGE;
        } else {

            if (Company.equals("Khabiya")) {
                khabiyaPrint(g, pf, page);
            } else if (Company.equals("Mahavir")) {
                mahavirPrint(g, pf, page);
            }

            return 0;
        }
    }
    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // TODO add your handling code here:
        if (jRadioButton1.isSelected()) {
            jTextField3.setEnabled(true);
            jTextField4.setEnabled(false);
        }
        jTextField21.setText("0.00");
        jTextField22.setText("0.00");
        int rowcount = jTable1.getRowCount();
        if (rowcount != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement pro_st = connect.createStatement();
                Statement sgst_st = connect.createStatement();
                Statement cgst_st = connect.createStatement();
                for (int i = 0; i < rowcount; i++) {
                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(i, 2) + "'");
                    while (pro_rs.next()) {
                        ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                        while (sgst_rs.next()) {
                            sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                        }

                        ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                        while (cgst_rs.next()) {
                            cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                        }
                    }
                    totalpercent = sgstpercent + cgstpercent;
                    double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                    if (jRadioButton5.isSelected()) {
                        amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                    } else if (jRadioButton6.isSelected()) {
                        amntbeforetax = value1;
                    }
                    sv = (amntbeforetax * sgstpercent) / 100;
                    cv = ((amntbeforetax * cgstpercent) / 100);

                    tsv = (sv + Double.parseDouble(jTextField21.getText()));
                    tcv = (cv + Double.parseDouble(jTextField22.getText()));
                    DecimalFormat df1 = new DecimalFormat("0.00");

                    jTextField2.setText("0.00");
                    jTextField21.setText("" + df1.format(tsv));
                    jTextField22.setText("" + df1.format(tcv));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // TODO add your handling code here:
        if (jRadioButton2.isSelected()) {
            jLabel4.setVisible(true);
            jTextField4.setVisible(true);
            jTextField4.setEnabled(true);
            jDialog3.setVisible(true);
        }
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        // TODO add your handling code here:
        jDialog2.setVisible(true);
    }//GEN-LAST:event_jRadioButton3ActionPerformed

    private void list1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list1ActionPerformed
        // TODO add your handling code here:
        sundrydebtors = list1.getSelectedItem();
        jDialog2.dispose();
    }//GEN-LAST:event_list1ActionPerformed

    private void jTextField20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField20FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField20FocusLost

    private void jTextField8FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField8FocusLost
        // TODO add your handling code here:
        String ra_te = jTextField7.getText();
        String qun_tity = jTextField8.getText();
        double p = Double.parseDouble(ra_te);
        double q = Double.parseDouble(qun_tity);
        double amou_nt = (p * q);
        jTextField9.setText("" + String.valueOf(amou_nt));

    }//GEN-LAST:event_jTextField8FocusLost

    private void jTextField12FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField12FocusLost
        // TODO add your handling code here:
        double disc_amt = Double.parseDouble(jTextField12.getText());
        double value = Double.parseDouble(jTextField9.getText());
        double total = value - disc_amt;
        jTextField13.setText("" + String.valueOf(total));

    }//GEN-LAST:event_jTextField12FocusLost


    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jTextField14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField14ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jTextField14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField14KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jDateChooser1.requestFocus();
        }

    }//GEN-LAST:event_jTextField14KeyPressed

    private void jTextField15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField15KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField16.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jComboBox1.requestFocus();
        }

    }//GEN-LAST:event_jTextField15KeyPressed

    private void jTextField16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField16KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField17.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField15.requestFocus();
        }

    }//GEN-LAST:event_jTextField16KeyPressed

    private void jTextField17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField17KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_ESCAPE) {
            jTextField16.requestFocus();
        }

    }//GEN-LAST:event_jTextField17KeyPressed

    private void jTextField18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField18KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField19.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jTextField18KeyPressed

    private void jTextField19KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField19KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField20.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField18.requestFocus();
        }
    }//GEN-LAST:event_jTextField19KeyPressed

    private void jTextField13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField13KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField12.requestFocus();
        }
    }//GEN-LAST:event_jTextField13KeyPressed

    private void jTextField18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField18ActionPerformed

    private void jTextField13FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField13FocusGained
//        // TODO add your handling code here
    }//GEN-LAST:event_jTextField13FocusGained

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField12KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField13.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField12KeyPressed

    private void jTextField11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField11KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField12.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField9.requestFocus();
        }
    }//GEN-LAST:event_jTextField11KeyPressed

    private void jTextField9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField9KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField8.requestFocus();
        }
    }//GEN-LAST:event_jTextField9KeyPressed

    private void jTextField17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField17FocusLost
        // TODO add your handling code here:
        double totamount = Double.parseDouble(jTextField15.getText());
        double Discamount = Double.parseDouble(jTextField17.getText());
        double todiscamnt = totamount - Discamount;
        DecimalFormat df = new DecimalFormat("0.00");
        String totaldiscamount1 = df.format(todiscamnt);
        jTextField18.setText(totaldiscamount1);
        jTextField20.setText(totaldiscamount1);

    }//GEN-LAST:event_jTextField17FocusLost
    int row = 0;
    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_DELETE) {
            jTextField20.requestFocus();
            DefaultTableModel dtm = null;
            dtm = (DefaultTableModel) jTable1.getModel();
            row = jTable1.getSelectedRow();
            double totalamt1 = Double.parseDouble((String) jTable1.getValueAt(row, 5));
            double nettotal = Double.parseDouble(jTextField15.getText());
            double nettotal1 = nettotal - totalamt1;
            jTextField15.setText("" + nettotal1);
            totalamount = totalamount - totalamt1;
            totalamountaftertax = totalamountaftertax - totalamt1;

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement sttt = connect.createStatement();
                Statement pro_st = connect.createStatement();
                Statement sgst_st = connect.createStatement();
                Statement cgst_st = connect.createStatement();

                if (isIgstApplicable == true) {
                    ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "" + "'");
                    while (rsss.next()) {
                        ResultSet rssss = sttt.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");

                        while (rssss.next()) {
                            double vat = Double.parseDouble(rssss.getString("percent"));
                            double value1 = Double.parseDouble(jTable1.getValueAt(row, 5) + "");
                            if (jRadioButton5.isSelected()) {
                                amntbeforetax = (value1 / (100 + vat)) * 100;
                            } else if (jRadioButton6.isSelected()) {
                                amntbeforetax = (value1);
                            }
                            iv = (amntbeforetax * vat) / 100;
                        }
                    }
                    totalamountbeforetax = (totalamountbeforetax - amntbeforetax);
                    tiv = (tiv - iv);
                    DecimalFormat df1 = new DecimalFormat("0.00");

                    jTextField19.setText("" + df1.format(totalamountbeforetax));
                    jTextField2.setText("" + df1.format(tiv));
                    jTextField21.setText("0.00");
                    jTextField22.setText("0.00");
                } else if (isIgstApplicable == false) {
                    ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(row, 2) + "'");
                    while (pro_rs.next()) {
                        ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                        while (sgst_rs.next()) {
                            sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                        }

                        ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                        while (cgst_rs.next()) {
                            cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                        }
                    }
                    totalpercent = sgstpercent + cgstpercent;
                    double value1 = Double.parseDouble(jTable1.getValueAt(row, 5) + "");
                    if (jRadioButton5.isSelected()) {
                        amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                    } else if (jRadioButton6.isSelected()) {
                        amntbeforetax = (value1);
                    }
                    sv = (amntbeforetax * sgstpercent) / 100;
                    cv = ((amntbeforetax * cgstpercent) / 100);

                    totalamountbeforetax = (totalamountbeforetax - amntbeforetax);
                    tsv = (tsv - sv);
                    tcv = (tcv - cv);
                    DecimalFormat df1 = new DecimalFormat("0.00");

                    jTextField19.setText("" + df1.format(totalamountbeforetax));
                    jTextField2.setText("0.00");
                    jTextField21.setText("" + df1.format(tsv));
                    jTextField22.setText("" + df1.format(tcv));

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            double netamttotal = nettotal1;
            jTextField18.setText("" + netamttotal);

            DecimalFormat df2 = new DecimalFormat("0.00");
            jTextField20.setText("" + df2.format(totalamountaftertax));

            double qnty = Double.parseDouble((String) jTable1.getValueAt(row, 4));
            double qntytotal = Double.parseDouble(jTextField16.getText());
            qntytotal = qntytotal - qnty;
            totalquantity = totalquantity - qnty;
            jTextField16.setText("" + qntytotal);

// Round off Value
            DecimalFormat df3 = new DecimalFormat("0.00");
            trv = Double.parseDouble(jTextField20.getText()) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField2.getText()) + Double.parseDouble(jTextField21.getText()) + (Double.parseDouble(jTextField22.getText())));
            jTextField23.setText("" + df3.format(trv));

            int rowcount = dtm.getRowCount();

            for (int i = row + 1; i < rowcount; i++) {
                int row1 = row + 2;
                jTable1.getModel().setValueAt(--row1, i, 0);
                row++;
            }
            int a = Integer.parseInt(jTextField14.getText());
            a = a - 1;
            jTextField14.setText("" + a);
            row = jTable1.getSelectedRow();
            dtm.removeRow(row);
            int rowcount1 = jTable1.getRowCount();
            if (rowcount1 == 0) {
                jTextField15.setText("0");
                jTextField16.setText("0");
                jTextField17.setText("0");
                jTextField18.setText("0");
                jTextField19.setText("0");
                jTextField20.setText("0");
                jTextField2.setText("0.00");
                jTextField21.setText("0.00");
                jTextField22.setText("0.00");
                jTextField23.setText("0.00");
            }
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTextField19FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField19FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField19FocusLost

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();
        master.customer.Add_Purchase_Party party = new master.customer.Add_Purchase_Party();
        party.setVisible(true);
//        party.jTextField2.setVisible(false);
//        party.jLabel2.setVisible(false);
//        party.jButton4.setVisible(false);
//        party.jButton5.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jComboBox1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    String barcode1 = null;
    String barcode = null;

    public void partyselect() {
        DefaultTableModel dtm = null;
        dtm = (DefaultTableModel) jTable3.getModel();
        row = jTable3.getSelectedRow();
        int coloum = jTable3.getSelectedColumn();
        sundrydebtors = (String) jTable3.getValueAt(row, 1);
        System.out.println("sundry debtors on party select is" + sundrydebtors);
        jDialog2.setVisible(false);
        jTextField4.setText(jTable3.getValueAt(row, 0) + "");
        jTextField3.setText(jTable3.getValueAt(row, 1) + "");
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + sundrydebtors + "'");
            while (rs.next()) {
                sundrydebtorsaccountcurrbal.add(rs.getString(10));
                sundrydebtorsaccountcurrbaltype.add(rs.getString(11));
            }

            if (jTable3.getValueAt(row, 6).equals("True")) {
                isIgstApplicable = true;
            } else if (jTable3.getValueAt(row, 6).equals("False")) {
                isIgstApplicable = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jDialog3.dispose();

        jTextField5.requestFocus();
    }
    public boolean isstockfinish = false;

    private void jTextField26CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField26CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();

        String party = jTextField26.getText();
        if (party.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party_sales where party_code  LIKE '%" + party + "%'");

                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};

                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField26CaretUpdate

    private void jTextField26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField26ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField26ActionPerformed

    private void jTextField26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField26KeyPressed

    }//GEN-LAST:event_jTextField26KeyPressed

    private void jTextField27CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField27CaretUpdate
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String partyName = jTextField27.getText();
        if (partyName.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party_sales where party_name LIKE'%" + partyName + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }

        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField27CaretUpdate

    private void jTextField27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField27ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField27ActionPerformed

    private void jTextField27KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField27KeyPressed

        int key = evt.getKeyCode();

        if (key == evt.VK_DOWN) {
            jTable3.requestFocus();

        }
    }//GEN-LAST:event_jTextField27KeyPressed

    private void jTextField28CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField28CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String city = jTextField28.getText();
        if (city.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party_sales where city LIKE '%" + city + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField28CaretUpdate

    private void jTextField28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField28KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField28KeyPressed

    private void jTextField29CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField29CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String state = jTextField29.getText();
        System.out.println("state is" + state);
        if (state.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party_sales where state LIKE '%" + state + "%'");
                while (rs.next()) {
                    String igst;
                    if (rs.getBoolean("igst") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }
                    Object o[] = {rs.getString("party_code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }
            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField29CaretUpdate

    private void jTextField29KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField29KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField29KeyPressed

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        partyselect();
        jTextField2.setText("0.00");
        jTextField21.setText("0.00");
        jTextField22.setText("0.00");
        int rowcount = jTable1.getRowCount();
        if (rowcount != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                Statement pro_st = connect.createStatement();
                Statement sgst_st = connect.createStatement();
                Statement cgst_st = connect.createStatement();
                for (int i = 0; i < rowcount; i++) {
                    if (isIgstApplicable == true) {
                        ResultSet rsss = st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(i, 2) + "" + "'");
                        while (rsss.next()) {
                            ResultSet rssss = st1.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString("IGST") + "'");

                            while (rssss.next()) {
                                double vat = Double.parseDouble(rssss.getString("percent"));
                                double value1 = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
                                if (jRadioButton5.isSelected()) {
                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                } else if (jRadioButton6.isSelected()) {
                                    amntbeforetax = value1;
                                }
                                iv = (amntbeforetax * vat) / 100;
                            }
                        }

                        tiv = (iv + Double.parseDouble(jTextField2.getText()));
                        DecimalFormat df1 = new DecimalFormat("0.00");

                        jTextField2.setText("" + df1.format(tiv));
                        jTextField21.setText("0.00");
                        jTextField22.setText("0.00");

                    } else if (isIgstApplicable == false) {

                        ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + jTable1.getValueAt(i, 2) + "'");
                        while (pro_rs.next()) {
                            ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                            while (sgst_rs.next()) {
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }

                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }

                        }

                        totalpercent = sgstpercent + cgstpercent;
                        double value1 = Double.parseDouble(jTable1.getValueAt(row, 5) + "");
                        if (jRadioButton5.isSelected()) {
                            amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                        } else if (jRadioButton6.isSelected()) {
                            amntbeforetax = value1;
                        }
                        sv = (amntbeforetax * sgstpercent) / 100;
                        cv = ((amntbeforetax * cgstpercent) / 100);

                        tsv = (sv + Double.parseDouble(jTextField21.getText()));
                        tcv = (cv + Double.parseDouble(jTextField22.getText()));
                        DecimalFormat df1 = new DecimalFormat("0.00");

                        jTextField2.setText("0.00");
                        jTextField21.setText("" + df1.format(tsv));
                        jTextField22.setText("" + df1.format(tcv));

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jTable3MouseClicked

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            partyselect();
        }

        if (key == evt.VK_BACK_SPACE) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_LEFT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_RIGHT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_SPACE) {
            jTextField18.requestFocus();
        }
    }//GEN-LAST:event_jTable3KeyPressed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        master.customer.Add_Sales_Party addnew = new master.customer.Add_Sales_Party();
        addnew.setVisible(true);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField5KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5KeyReleased

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        String stock_no = "";
        int is_meter1 = 0;
        String bill_refrence = "";
        try {
            int stock_row = jTable2.getSelectedRow();
            connection c = new connection();
            Connection connect = c.cone();
            Statement st_stock = connect.createStatement();
            ResultSet rs_stock = null;
            rs_stock = st_stock.executeQuery("Select bill_refrence, stock_no, ismeter from billing where bill_refrence = '" + (jTable2.getValueAt(stock_row, 0)) + "' and stock_no = '" + jTextField5.getText() + "'");
            if (!rs_stock.isBeforeFirst()) {
                rs_stock = st_stock.executeQuery("Select bill_refrence, stock_no, ismeter from billing_old where bill_refrence = '" + (jTable2.getValueAt(stock_row, 0)) + "' and stock_no = '" + jTextField5.getText() + "'");
            }
            while (rs_stock.next()) {
                stock_no = rs_stock.getString("stock_no");
                is_meter1 = rs_stock.getInt("ismeter");
                bill_refrence = rs_stock.getString("bill_refrence");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int rowcount = jTable1.getRowCount();
        ArrayList stockno = new ArrayList();
        ArrayList bill_ref = new ArrayList();
        for (int i = 0; i < rowcount; i++) {
            stockno.add(jTable1.getValueAt(i, 1));
            bill_ref.add(jTable1.getValueAt(i, 11));
        }
        if (is_meter1 == 0) {
            if (stockno.contains(stock_no)) {
                JOptionPane.showMessageDialog(rootPane, "Item is already present in the list");
                jTextField5.requestFocus();
                jDialog4.dispose();
            } else {
                setData();
                jDialog4.dispose();
            }
        } else if (is_meter1 == 1) {
            if (bill_ref.contains(bill_refrence)) {
                JOptionPane.showMessageDialog(rootPane, "Item is already present in the list");
                jTextField5.requestFocus();
                jDialog4.dispose();
            } else {
                setData();
                jDialog4.dispose();
            }
        }
    }//GEN-LAST:event_jTable2MouseClicked
    double s1 = 0;
    double c1 = 0;
    double v1 = 0;
    double round1 = 0;
    double reducingtax = 0;
    int is_meter = 0;
    int sales_qnty = 0;
    String discr = "";
    String Party_ledger = "";
    ArrayList stock1 = new ArrayList();
    ArrayList vat1 = new ArrayList();
    ArrayList ledgervat1 = new ArrayList();
    ArrayList sgst1 = new ArrayList();
    ArrayList ledgersgst1 = new ArrayList();
    ArrayList cgst1 = new ArrayList();
    ArrayList ledgercgst1 = new ArrayList();
    public String sundrydebtorsold = "";
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Update");
        if (i2 == 0) {
            double discounamntt = 0;
            try {
                String paytype = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement st1 = connect.createStatement();
                ResultSet rs = null;
                rs = st1.executeQuery("select * from salesreturn where bill_refrence='" + jTextField1.getText() + "'");
                if (!rs.isBeforeFirst()) {
                    rs = st1.executeQuery("select * from salesreturn_old where bill_refrence='" + jTextField1.getText() + "'");
                }
                while (rs.next()) {
                    paytype = rs.getString(3);
                    discr = rs.getString("description");
                    sales_qnty = rs.getInt("qnty");
//                    c1 = Double.parseDouble(rs.getString("Net_Total"));
                    c1 = Double.parseDouble(rs.getString("total_value_after_tax"));

                    round1 = Double.parseDouble(rs.getString("Round_Off_Value"));

                    if (rs.getString("type").equals("Credit")) {
                        sundrydebtorsold = rs.getString("to_ledger");
                    }

                    Party_ledger = rs.getString("by_ledger");
                    reducingtax = rs.getDouble("total_value_before_tax");
                    discounamntt = Double.parseDouble(rs.getString("total_disc_amnt"));

                    Statement st_meter = connect.createStatement();
                    ResultSet rs_meter = st_meter.executeQuery("Select * from product where product_code = '" + discr + "'");
                    while (rs_meter.next()) {
                        is_meter = rs_meter.getInt("ismeter");
                    }

                    if (is_meter == 0) {
                        Statement stmt3 = connect.createStatement();
                        ResultSet rsstock = null;
                        rsstock = stmt3.executeQuery("select * from stockid where Stock_No='" + rs.getString("stock_no") + "'");
                        if (!rs.isBeforeFirst()) {
                            rsstock = stmt3.executeQuery("select * from stockid_old where Stock_No='" + rs.getString("stock_no") + "'");
                        }
                        while (rsstock.next()) {
                            if (rsstock.getString(2) != null) {
                                Statement stmt4 = connect.createStatement();
                                stmt4.executeUpdate("Delete from Stockid2 where Stock_No = '" + rs.getString("stock_no") + "'");
                            }
                        }
                    } else if (is_meter == 1) {
                        int qnty_ = 0;
                        Statement meter_st = connect.createStatement();
                        ResultSet meter_rs = meter_st.executeQuery("Select * from stockid2 where Stock_No = '" + rs.getString("stock_no") + "'");
                        while (meter_rs.next()) {
                            qnty_ = meter_rs.getInt("qnt");
                        }
                        int rev_qnty = qnty_ - sales_qnty;
                        Statement stmt4 = connect.createStatement();
                        stmt4.executeUpdate("Update stockid2 set qnt = '" + rev_qnty + "' where Stock_No = '" + rs.getString("stock_no") + "'");
                    }

                    {
                        ledgervat1.add(rs.getString("vataccount"));
                        Statement stmt1 = connect.createStatement();
                        Statement stmtvataccount = connect.createStatement();
                        ResultSet rsvataccount = stmtvataccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString(24) + "' ");
                        while (rsvataccount.next()) {
                            vat1.add(rsvataccount.getString(6));
                            vatcurrbal.add(rsvataccount.getString(10));
                            vatcurrbaltype.add(rsvataccount.getString(11));

                            vatamnt = rs.getDouble("vatamnt");

                            DecimalFormat idf = new DecimalFormat("0.00");
                            igstamnt1_1 = idf.format(vatamnt);

                            String vatcurrbalance = (String) vatcurrbal.get(0);
                            String Curbaltypevat = (String) vatcurrbaltype.get(0);
                            double vatcurrbalance1 = Double.parseDouble(vatcurrbalance);
                            double vatamnt1 = vatamnt;
                            double result;

                            if (Curbaltypevat.equals("DR")) {
                                result = vatcurrbalance1 - vatamnt1;
                                if (result >= 0) {
                                    updatevatcurrbaltype.add(0, "DR");
                                    updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatevatcurrbaltype.add(0, "CR");
                                    updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypevat.equals("CR")) {

                                result = vatcurrbalance1 + vatamnt1;

                                updatevatcurrbaltype.add(0, "CR");
                                updatevatcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Integer.parseInt(vat1.get(0) + "") != 0) {
                                DecimalFormat upvat = new DecimalFormat("0.00");
                                String updavat = upvat.format(Double.parseDouble(updatevatcurrbal.get(0).toString()));
                                stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + updavat + "" + "',currbal_type='" + updatevatcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgervat1.get(0) + "" + "'");
                            }
                            vat1.clear();
                            vatcurrbal.clear();
                            vatcurrbaltype.clear();
                            updatevatcurrbal.clear();
                            updatevatcurrbaltype.clear();
                        }
                    }

// For SGST calculation
                    {
                        ledgersgst1.add(rs.getString("Sgstaccount"));
                        Statement stmt_sgst = connect.createStatement();
                        Statement stmtsgstaccount = connect.createStatement();
                        ResultSet rssgstaccount = stmtsgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("Sgstaccount") + "' ");
                        while (rssgstaccount.next()) {
                            sgst1.add(rssgstaccount.getString(6));
                            sgstcurrbal.add(rssgstaccount.getString(10));
                            sgstcurrbaltype.add(rssgstaccount.getString(11));

                            sgstperc = Double.parseDouble((String) sgst1.get(0));

                            sgstamnt = rs.getDouble("sgstamnt");

                            DecimalFormat idf = new DecimalFormat("0.00");
                            sgstamnt1_1 = idf.format(sgstamnt);

                            String sgstcurrbalance = (String) sgstcurrbal.get(0);
                            String Curbaltypesgst = (String) sgstcurrbaltype.get(0);
                            double sgstcurrbalance1 = Double.parseDouble(sgstcurrbalance);
                            double sgstamnt1 = sgstamnt;
                            double result;

                            if (Curbaltypesgst.equals("DR")) {
                                result = sgstcurrbalance1 - sgstamnt1;
                                if (result >= 0) {
                                    updatesgstcurrbaltype.add(0, "DR");
                                    updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatesgstcurrbaltype.add(0, "CR");
                                    updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypesgst.equals("CR")) {

                                result = sgstcurrbalance1 + sgstamnt1;

                                updatesgstcurrbaltype.add(0, "CR");
                                updatesgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Double.parseDouble(sgst1.get(0) + "") != 0) {
                                DecimalFormat upsgst = new DecimalFormat("0.00");
                                String updasgst = upsgst.format(Double.parseDouble(updatesgstcurrbal.get(0).toString()));
                                stmt_sgst.executeUpdate("UPDATE ledger SET curr_bal='" + updasgst + "" + "',currbal_type='" + updatesgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgersgst1.get(0) + "" + "'");
                            }

                            sgst1.clear();
                            sgstcurrbal.clear();
                            sgstcurrbaltype.clear();
                            updatesgstcurrbal.clear();
                            updatesgstcurrbaltype.clear();

                        }
                    }

// For CGST calculation
                    {
                        ledgercgst1.add(rs.getString("Cgstaccount"));
                        Statement stmt_cgst = connect.createStatement();
                        Statement stmtcgstaccount = connect.createStatement();
                        ResultSet rscgstaccount = stmtcgstaccount.executeQuery("Select *  from ledger where ledger_name='" + rs.getString("Cgstaccount") + "' ");
                        while (rscgstaccount.next()) {
                            cgst1.add(rscgstaccount.getString(6));
                            cgstcurrbal.add(rscgstaccount.getString(10));
                            cgstcurrbaltype.add(rscgstaccount.getString(11));

                            cgstamnt = rs.getDouble("cgstamnt");

                            DecimalFormat idf = new DecimalFormat("0.00");
                            cgstamnt1_1 = idf.format(cgstamnt);

                            String cgstcurrbalance = (String) cgstcurrbal.get(0);
                            String Curbaltypecgst = (String) cgstcurrbaltype.get(0);
                            double cgstcurrbalance1 = Double.parseDouble(cgstcurrbalance);
                            double cgstamnt1 = cgstamnt;
                            double result;

                            if (Curbaltypecgst.equals("DR")) {
                                result = cgstcurrbalance1 - cgstamnt1;
                                if (result >= 0) {
                                    updatecgstcurrbaltype.add(0, "DR");
                                    updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                } else {
                                    updatecgstcurrbaltype.add(0, "CR");
                                    updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                                }
                            } else if (Curbaltypecgst.equals("CR")) {

                                result = cgstcurrbalance1 + cgstamnt1;

                                updatecgstcurrbaltype.add(0, "CR");
                                updatecgstcurrbal.add(0, String.valueOf(Math.abs(result)));
                            }

                            if (Double.parseDouble(cgst1.get(0) + "") != 0) {

                                DecimalFormat upcgst = new DecimalFormat("0.00");
                                String updacgst = upcgst.format(Double.parseDouble(updatecgstcurrbal.get(0).toString()));
                                stmt_cgst.executeUpdate("UPDATE ledger SET curr_bal='" + updacgst + "" + "',currbal_type='" + updatecgstcurrbaltype.get(0) + "" + "' where ledger_name='" + ledgercgst1.get(0) + "" + "'");
                            }

                            cgst1.clear();
                            cgstcurrbal.clear();
                            cgstcurrbaltype.clear();
                            updatecgstcurrbal.clear();
                            updatecgstcurrbaltype.clear();
                        }
                    }
                }
                Statement Salesaccount = connect.createStatement();

                ResultSet rssales = Salesaccount.executeQuery("select * from ledger where ledger_name='" + Party_ledger + "' ");
                while (rssales.next()) {
                    Salesaccountcurrbal.add(rssales.getString(10));
                    Salesaccountcurrbaltype.add(rssales.getString(11));
                }

                double totalamntreducingvat = reducingtax;

                String salesaccountamnt = (String) Salesaccountcurrbal.get(0);
                double salesaccountamnt1 = Double.parseDouble(salesaccountamnt);
                double salesaccountupdatedamount = (salesaccountamnt1 - totalamntreducingvat);

                if (Salesaccountcurrbaltype.get(0).equals("DR")) {
                    if (salesaccountupdatedamount > 0) {
                        Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                        Salesaccountupdatecurrbaltype.add("DR");

                    }
                    if (salesaccountupdatedamount <= 0) {
                        Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                        Salesaccountupdatecurrbaltype.add("CR");
                    }
                } else if (Salesaccountcurrbaltype.get(0).equals("CR")) {
                    salesaccountupdatedamount = salesaccountamnt1 + totalamntreducingvat;
                    Salesaccountupdatecurrbal.add(Math.abs(salesaccountupdatedamount));
                    Salesaccountupdatecurrbaltype.add("CR");
                }

                Statement stmt2 = connect.createStatement();
                DecimalFormat upsales = new DecimalFormat("0.00");
                String updasales = upsales.format(Double.parseDouble(Salesaccountupdatecurrbal.get(0).toString()));
                stmt2.executeUpdate("UPDATE ledger SET curr_bal='" + updasales + "" + "',currbal_type='" + Salesaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + Party_ledger + "'");

                Salesaccountcurrbal.clear();
                Salesaccountcurrbaltype.clear();
                Salesaccountupdatecurrbal.clear();
                Salesaccountupdatecurrbaltype.clear();

                System.out.println("cod is running for update ");

                if (paytype.equals("Cash")) {

                    String c2 = "CASH";
                    Statement Cashaccount = connect.createStatement();
                    ResultSet rscashaccount = Cashaccount.executeQuery("select * from ledger where ledger_name='" + c2 + "'");
                    while (rscashaccount.next()) {
                        cashaccountcurrbal.add(rscashaccount.getString(10));
                        cashaccountcurrbaltype.add(rscashaccount.getString(11));
                    }

                    System.out.println("CODE IS RUNNING FOR CASH AND SALE UPDATE");

                    jTextField4.setText(null);
                    paytype = jRadioButton1.getLabel();

                    String cashaccountamount = (String) cashaccountcurrbal.get(0);
                    double totalamnt = c1;
                    double cashaccountamount1 = Double.parseDouble(cashaccountamount);

                    if (cashaccountcurrbaltype.get(0).equals("CR")) {
                        double cashaccountupdateamount = cashaccountamount1 - totalamnt;
                        if (cashaccountupdateamount > 0) {
                            cashaccountupdatecurrbaltype.add("CR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                        if (cashaccountupdateamount <= 0) {
                            cashaccountupdatecurrbaltype.add("DR");
                            cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                        }
                    }
                    if (cashaccountcurrbaltype.get(0).equals("DR")) {
                        double cashaccountupdateamount = cashaccountamount1 + totalamnt;
                        cashaccountupdatecurrbaltype.add("DR");
                        cashaccountupdatecurrbal.add(Math.abs(cashaccountupdateamount));
                    }
                    Statement stmt1 = connect.createStatement();
                    DecimalFormat dfcash = new DecimalFormat("0.00");
                    stmt1.executeUpdate("UPDATE ledger SET curr_bal='" + dfcash.format(cashaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + cashaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + c2 + "'");

                    double disccurrbal = 0;
                    String disccurrbaltype = "";

                    try {
                        Statement statement1 = connect.createStatement();
                        Statement statement3 = connect.createStatement();

                        ResultSet rs1 = statement1.executeQuery("select curr_bal,currbal_type from ledger where ledger_name='DISCOUNT ACCOUNT'");

                        while (rs1.next()) {
                            disccurrbal = Double.parseDouble(rs1.getString(1));
                            disccurrbaltype = rs1.getString(2);
                        }

                        if (disccurrbaltype.equals("CR")) {
                            disccurrbal = disccurrbal - discounamntt;
                            if (disccurrbal < 0) {
                                disccurrbaltype = "DR";
                                disccurrbal = Math.abs(disccurrbal);
                            }
                        } else if (disccurrbaltype.equals("DR")) {
                            disccurrbal = disccurrbal + discounamntt;
                        }

                        disccurrbal = Math.abs(disccurrbal);
                        statement3.executeUpdate("update ledger set curr_bal='" + disccurrbal + "',currbal_type='" + disccurrbaltype + "' where ledger_name = 'DISCOUNT ACCOUNT'");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                if (paytype.equals("Credit")) {
                    Statement sundrydebtorsaccount = connect.createStatement();
                    ResultSet rssundrydebtors = sundrydebtorsaccount.executeQuery("select * from ledger where  ledger_name='" + sundrydebtorsold + "'");
                    while (rssundrydebtors.next()) {
                        sundrydebtorsaccountcurrbal.add(rssundrydebtors.getString(10));
                        sundrydebtorsaccountcurrbaltype.add(rssundrydebtors.getString(11));
                    }

                    String sundrydebtorsamnt = (String) sundrydebtorsaccountcurrbal.get(0);
                    double totalamnt = c1;
//                    double totalamntreducingvat = totalamnt - vatamnt;
                    double sundrydebtorsamnt1 = Double.parseDouble(sundrydebtorsamnt);
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("CR")) {
                        double sundrydebtorsccountupdateamount = sundrydebtorsamnt1 - totalamnt;

                        if (sundrydebtorsccountupdateamount > 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add(0, "CR");
                            sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                        }
                        if (sundrydebtorsccountupdateamount <= 0) {
                            sundrydebtorsaccountupdatecurrbaltype.add(0, "DR");
                            sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(sundrydebtorsccountupdateamount));
                        }
                    }
                    if (sundrydebtorsaccountcurrbaltype.get(0).equals("DR")) {
                        double cashaccountupdateamount = sundrydebtorsamnt1 + totalamnt;
                        sundrydebtorsaccountupdatecurrbaltype.add(0, "DR");
                        sundrydebtorsaccountupdatecurrbal.add(0, Math.abs(cashaccountupdateamount));
                    }

//                    Statement stmt2 = connect.createStatement();
                    DecimalFormat dfsundry = new DecimalFormat("0.00");
                    sundrydebtorsaccount.executeUpdate("UPDATE ledger SET curr_bal='" + dfsundry.format(sundrydebtorsaccountupdatecurrbal.get(0)) + "" + "',currbal_type='" + sundrydebtorsaccountupdatecurrbaltype.get(0) + "" + "' where ledger_name='" + sundrydebtorsold + "'");

                }

// Calculation for Round off
                String Round_currbal_type1 = "";
                double diff_total1 = 0;
                String diff_total_round1 = "";
                Statement stru = connect.createStatement();
                Statement round_st = connect.createStatement();
                ResultSet round_rs = round_st.executeQuery("Select * from ledger where ledger_name = 'Round Off'");
                while (round_rs.next()) {
                    double Round_curr_bal = Double.parseDouble(round_rs.getString("curr_bal"));
                    Round_currbal_type1 = round_rs.getString("currbal_type");
                    double round_total = round1;

                    if (Round_currbal_type1.equals("CR") && round_total < 0) {
                        diff_total1 = Round_curr_bal - round_total;
                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    } else if (Round_currbal_type1.equals("CR") && round_total > 0) {
                        diff_total1 = Round_curr_bal + round_total;
                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    }

                    if (Round_currbal_type1.equals("DR") && round_total < 0) {
                        diff_total1 = Round_curr_bal + round_total;

                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        }

                    } else if (Round_currbal_type1.equals("DR") && round_total > 0) {
                        diff_total1 = Round_curr_bal - round_total;

                        if (diff_total1 > 0) {
                            Round_currbal_type1 = "CR";
                            diff_total1 = (Math.abs(diff_total1));
                        } else if (diff_total1 < 0) {
                            Round_currbal_type1 = "DR";
                            diff_total1 = (Math.abs(diff_total1));
                        }
                    }
                    DecimalFormat dr1 = new DecimalFormat("0.00");
                    diff_total_round1 = dr1.format(diff_total1);
                }

                //Updating round off ledger in ledger table       
                stru.executeUpdate("Update ledger Set curr_bal = '" + diff_total_round1 + "', currbal_type = '" + Round_currbal_type1 + "' where ledger_name = 'Round Off'");

                Statement stmt3 = connect.createStatement();
                Statement stmt4 = connect.createStatement();
                Statement stmt5 = connect.createStatement();
                String insertbillingupdate = "\"insert into salesreturn1 select * from salesreturn where bill_refrence='" + jTextField1.getText() + "'\"";
                String insertbillingupdate1 = "\"insert into salesreturn1 select * from salesreturn_old where bill_refrence='" + jTextField1.getText() + "'\"";
                stmt4.executeUpdate("insert into salesreturn1 select * from salesreturn where bill_refrence='" + jTextField1.getText() + "'");
                stmt4.executeUpdate("insert into salesreturn1 select * from salesreturn_old where bill_refrence='" + jTextField1.getText() + "'");
                stmt5.executeUpdate("insert into queries values(" + insertbillingupdate + ")");
                stmt5.executeUpdate("insert into queries values(" + insertbillingupdate1 + ")");

                stmt3.executeUpdate("delete from salesreturn where bill_refrence='" + jTextField1.getText() + "'");
                stmt3.executeUpdate("delete from salesreturn_old where bill_refrence='" + jTextField1.getText() + "'");

                Salesaccountcurrbal.clear();
                Salesaccountupdatecurrbal.clear();
                Salesaccountcurrbaltype.clear();
                Salesaccountupdatecurrbaltype.clear();
                salesaccount.clear();
                cashaccountcurrbal.clear();
                cashaccountupdatecurrbal.clear();
                cashaccountcurrbaltype.clear();
                cashaccountupdatecurrbaltype.clear();
                sundrydebtorsaccountcurrbal.clear();
                sundrydebtorsaccountcurrbaltype.clear();
                sundrydebtorsaccountupdatecurrbal.clear();
                sundrydebtorsaccountupdatecurrbaltype.clear();

                int rowcount = jTable1.getRowCount();

                if (rowcount != 0) {
                    if (issave == false) {
                        jButton5.requestFocus();
                        save();
                    }
                } else {
                    String date = formatter.format(jDateChooser1.getDate());
                    Statement stmtinsert = connect.createStatement();
                    stmtinsert.executeUpdate("insert into salesreturn1(bill_refrence,date,type,customer_name,customer_account,sno,stock_no,rate,qnty,value,total,total_amnt,total_disc_amnt,total_amnt_after_disc,total_value_before_tax,total_value_after_tax,vatamnt,vataccount,to_ledger,by_ledger,branchid,party_code,Sgstamnt,Sgstaccount,Cgstamnt,Cgstaccount,Round_Off_Value,total_igst_taxamnt,total_sgst_taxamnt,total_cgst_taxamnt,bill_no) values ('" + jTextField1.getText() + "','" + date + "','" + paytype + "','cancelled bill','','0','','0','0','0','0','0','0','0','0','0','0','VAT OUTPUT 0%','CASH','SALES','0','0','0','0','0','0','0','0','0','0','0') ");
                    JOptionPane.showMessageDialog(rootPane, "Entry deleted succesfully");
                }
//                dispose();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        int i2 = JOptionPane.showConfirmDialog(rootPane, "Do you want to Print");
        if (i2 == 0) {
            PrinterJob job = PrinterJob.getPrinterJob();
            PageFormat pf = new PageFormat();
            Paper paper = new Paper();
            if (Company.equals("Khabiya")) {
                paper.setSize(5.8d * 72, 8.3d * 72);
                double margin = 40;
                paper.setImageableArea(margin, margin, (paper.getWidth() - 0) * 1, paper.getHeight() - margin * 2);
            } else if (Company.equals("Mahavir")) {
                paper.setSize(5.8d * 72, 8.3d * 72);
                double margin = 40;
                paper.setImageableArea(margin, margin, (paper.getWidth() - 0) * 1, paper.getHeight() - margin * 2);
            }
            pf.setPaper(paper);
            job.setPrintable(this, pf);
            PrintService service = PrintServiceLookup.lookupDefaultPrintService();

            try {
                job.setPrintService(service);
                if (Company.equals("Khabiya")) {
                    job.setCopies(2);
                } else if (Company.equals("Mahavir")) {
                    job.setCopies(1);
                }
                job.print();
            } catch (PrinterException ex) {
                /* The job did not successfully complete */
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    private void jRadioButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton5ActionPerformed
        if (jRadioButton5.isSelected()) {
            tax_type = "Inclusive";
        }
    }//GEN-LAST:event_jRadioButton5ActionPerformed

    private void jRadioButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton6ActionPerformed
        if (jRadioButton6.isSelected()) {
            tax_type = "Exclusive";
        }
    }//GEN-LAST:event_jRadioButton6ActionPerformed

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jComboBox2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox2KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField7.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jComboBox2KeyPressed
    public void setData() {
        islist = false;
        String barcode = (jTextField5.getText().trim().toUpperCase());
        int bill_row = jTable2.getSelectedRow();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st_data = connect.createStatement();
            Statement st_staff = connect.createStatement();
            Statement stt = connect.createStatement();
            ResultSet rsstock = null;
            rsstock = st_data.executeQuery("select Stock_No,ismeter from Stockid where Stock_No='" + barcode + "'");
            if (!rsstock.isBeforeFirst()) {
                rsstock = st_data.executeQuery("select Stock_No,ismeter from Stockid_old where Stock_No='" + barcode + "'");
            }
            while (rsstock.next()) {
                ismeter = rsstock.getInt("ismeter");
            }
            System.out.println("is meter status" + ismeter);
            if (ismeter == 0) {
                String product = null;
                String Retail = null;
                String barcode1 = null;
                System.out.println("is meter zero code is running");
                ResultSet rss = null;
                rss = stt.executeQuery("SELECT  * FROM Stockid where Stock_no='" + barcode + "'");
                if (!rss.isBeforeFirst()) {
                    rss = stt.executeQuery("SELECT  * FROM Stockid_old where Stock_no='" + barcode + "'");
                }
                while (rss.next()) {
                    barcode1 = rss.getString(1);
                    product = rss.getString(8);
                }

                Retail = jTable2.getValueAt(bill_row, 3) + "";

                jTextField7.setText("" + Retail);
                jComboBox2.setSelectedItem("" + product);

                if (!jTextField7.getText().isEmpty()) {
                    String ra_te = jTextField7.getText();
                    String qun_tity = jTextField8.getText();
                    double p = Double.parseDouble(ra_te);
                    double q = Double.parseDouble(qun_tity);
                    double amou_nt = (p * q);
                    jTextField9.setText("" + String.valueOf(amou_nt));
                }

//focus gained
                if (!jTextField12.getText().isEmpty() && !jTextField9.getText().isEmpty()) {
                    double disc_amt = Double.parseDouble(jTextField12.getText());
                    double value = Double.parseDouble(jTextField9.getText());
                    double total = value - disc_amt;
                    jTextField13.setText("" + String.valueOf(total));
                }

// keypressed
                jTextField5.requestFocus();
                String st1 = (String) jComboBox1.getSelectedItem();
                sno = Integer.parseInt(jTextField14.getText());
                totalamount = (totalamount + Double.parseDouble(jTextField13.getText()));
                DecimalFormat df = new DecimalFormat("0.00");
                String totalamount1 = df.format(totalamount);
                jTextField15.setText("" + totalamount1);
                jTextField18.setText("" + totalamount1);
                totalquantity = (totalquantity + Double.parseDouble(jTextField8.getText()));

                String totalquantity1 = df.format(totalquantity);
                jTextField16.setText("" + totalquantity1);
                totaldiscount = (totaldiscount + Double.parseDouble(jTextField12.getText()));
                String totaldiscount1 = df.format(totaldiscount);
                jTextField17.setText("" + totaldiscount1);
//                totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
//                String totalamountaftertax1 = df.format(totalamountaftertax);
//                jTextField20.setText("" + totalamountaftertax1);

                if (jTextField5.getText().isEmpty()) {
                    jTextField5.setText("N/A");
                }

                Object o[] = {sno, jTextField5.getText().toUpperCase(), (String) jComboBox2.getSelectedItem(), jTextField7.getText(), jTextField8.getText(),
                    jTextField9.getText(), jTextField10.getText(), jTextField11.getText(), jTextField12.getText(), jTextField13.getText(),
                    st1, jTable2.getValueAt(bill_row, 0)};
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                dtm.addRow(o);

                try {
                    connection c5 = new connection();
                    Connection connect1 = c5.cone();
                    Statement sttt = connect1.createStatement();
                    Statement st3 = connect1.createStatement();
                    Statement pro_st = connect.createStatement();
                    Statement sgst_st = connect.createStatement();
                    Statement cgst_st = connect.createStatement();
                    if (isIgstApplicable == true) {
                        ResultSet rsss = sttt.executeQuery("SELECT  * FROM product where product_code='" + (String) jComboBox2.getSelectedItem() + "'");
                        while (rsss.next()) {

                            ResultSet rssss = st3.executeQuery("SELECT  * FROM ledger where ledger_name='" + rsss.getString(3) + "'");
                            while (rssss.next()) {
                                double vat = Double.parseDouble(rssss.getString("percent"));
                                double value1 = Double.parseDouble(jTextField9.getText());
                                if (jRadioButton5.isSelected()) {
                                    amntbeforetax = (value1 / (100 + vat)) * 100;
                                } else if (jRadioButton6.isSelected()) {
                                    amntbeforetax = (value1);
                                }
                                iv = (amntbeforetax * vat) / 100;
                            }
                        }
                    } else if (isIgstApplicable == false) {

                        ResultSet pro_rs = pro_st.executeQuery("SELECT  * FROM product where product_code='" + (String) jComboBox2.getSelectedItem() + "'");
                        while (pro_rs.next()) {
                            ResultSet sgst_rs = sgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("SGST") + "'");
// Sgst value calculation  
                            while (sgst_rs.next()) {
                                sgstpercent = Double.parseDouble(sgst_rs.getString("percent"));
                            }

                            ResultSet cgst_rs = cgst_st.executeQuery("SELECT  * FROM ledger where ledger_name='" + pro_rs.getString("CGST") + "'");
// Cgst Value Calculation                            
                            while (cgst_rs.next()) {
                                cgstpercent = Double.parseDouble(cgst_rs.getString("percent"));
                            }
                        }

                        totalpercent = sgstpercent + cgstpercent;
                        double value1 = Double.parseDouble(jTextField9.getText());
                        if (jRadioButton5.isSelected()) {
                            amntbeforetax = (value1 / (100 + totalpercent)) * 100;
                        } else if (jRadioButton6.isSelected()) {
                            amntbeforetax = (value1);
                        }
                        sv = (amntbeforetax * sgstpercent) / 100;
                        cv = ((amntbeforetax * cgstpercent) / 100);

                    }

                    totalamountbeforetax = (totalamountbeforetax + amntbeforetax);
                    if (jRadioButton5.isSelected()) {
                        totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()));
                        String totalamountaftertax1 = df.format(totalamountaftertax);
                        jTextField20.setText("" + totalamountaftertax1);
                    } else if (jRadioButton6.isSelected()) {
                        totalamountaftertax = (totalamountaftertax + Double.parseDouble(jTextField9.getText()) + iv + sv + cv);
                        String totalamountaftertax1 = df.format(totalamountaftertax);
                        jTextField20.setText("" + totalamountaftertax1);
                    }
                    tiv = (tiv + iv);
                    tsv = (tsv + sv);
                    tcv = (tcv + cv);
                    DecimalFormat df1 = new DecimalFormat("0.00");

                    jTextField19.setText("" + df1.format(totalamountbeforetax));
                    jTextField2.setText("" + df1.format(tiv));
                    jTextField21.setText("" + df1.format(tsv));
                    jTextField22.setText("" + df1.format(tcv));

                    trv = ((Double.parseDouble(jTextField20.getText())) - (Double.parseDouble(jTextField19.getText()) + Double.parseDouble(jTextField2.getText()) + Double.parseDouble(jTextField21.getText()) + (Double.parseDouble(jTextField22.getText()))));

                    jTextField23.setText("" + df1.format(trv));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                sno = Integer.parseInt(jTextField14.getText());
                sno++;
                jTextField14.setText("" + sno);
                jTextField5.setText(null);
                jComboBox2.setSelectedIndex(0);
                jTextField7.setText("0");
                jTextField8.setText("1");
                jTextField9.setText(null);
                jTextField10.setText(null);
                jTextField11.setText("0");
                jTextField12.setText("0");
                jTextField13.setText(null);
                jComboBox1.setSelectedItem(st1);
            } else if (ismeter == 1) {
                String product = null;
                double Retail = 0;
                String barcode1 = null;
                System.out.println("is meter one code is running");
                ResultSet rss = null;
                rss = stt.executeQuery("SELECT  * FROM Stockid where Stock_no='" + barcode + "'");
                if (!rss.isBeforeFirst()) {
                    rss = stt.executeQuery("SELECT  * FROM Stockid_old where Stock_no='" + barcode + "'");
                }
                while (rss.next()) {
                    barcode1 = rss.getString(1);
                    product = rss.getString(8);
                }

                Retail = Double.parseDouble(jTable2.getValueAt(bill_row, 3) + "") / Double.parseDouble(jTable2.getValueAt(bill_row, 1) + "");
                DecimalFormat df = new DecimalFormat("0.00");
                jTextField7.setText("" + df.format(Retail));
                jComboBox2.setSelectedItem("" + product);
                jTextField8.requestFocus();
            }

            ResultSet staff_rs = st_staff.executeQuery("SELECT  * FROM staff");
            while (staff_rs.next()) {
                jComboBox3.addItem(staff_rs.getString("Emp_code"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(New_Dual_Billing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //final JButton printButton = new JButton("Print");
        // UIManager.put("swing.boldMetal", Boolean.TRUE);
        //JFrame f = new JFrame("GatePass");
//        Billing_1.addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent e) {
//                System.exit(0);
//            }
//        });
//        Font f1 = new Font("Times New Roman", Font.BOLD, 12);
        //JBut.setVisible(true);
//        JLabel label1 = new JLabel("Vision Techno Solutions", JLabel.CENTER);
//        label1.setBounds(30, 30, 200, 40);
//        JLabel label2 = new JLabel("Batch No:");
//        label2.setBounds(10, 70, 80, 20);
//        JLabel label3 = new JLabel("Date and Time:");
//        label3.setBounds(10, 100, 120, 20);
//        JLabel label4 = new JLabel("Visitor's Name:");
//        label4.setBounds(10, 130, 120, 20);
//        JLabel label5 = new JLabel("Concern Person:");
//        label5.setBounds(10, 160, 120, 20);
//        JLabel label6 = new JLabel("Purpose of Visit:");
//        label6.setBounds(10, 190, 120, 20);
//        JLabel label7 = new JLabel("Manager's Sign");
//        label7.setBounds(140, 230, 120, 20);
//        JLabel label8 = new JLabel("Visitor's Sign");
//        label8.setBounds(10, 230, 120, 20);
/////      /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new New_Dual_SalesReturn().setVisible(true);
            }
        });
    }
    String snumbers[] = new String[5000];

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JDialog Conform;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    public static javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    public javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    public javax.swing.JComboBox<String> jComboBox3;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JDialog jDialog1;
    public javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JDialog jDialog4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    public javax.swing.JLabel jLabel15;
    public javax.swing.JLabel jLabel16;
    public javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel18;
    public javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    public javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    public javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JRadioButton jRadioButton1;
    public javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JRadioButton jRadioButton6;
    public javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    public javax.swing.JTextField jTextField17;
    public javax.swing.JTextField jTextField18;
    public javax.swing.JTextField jTextField19;
    public javax.swing.JTextField jTextField2;
    public javax.swing.JTextField jTextField20;
    public javax.swing.JTextField jTextField21;
    public javax.swing.JTextField jTextField22;
    public javax.swing.JTextField jTextField23;
    private javax.swing.JTextField jTextField26;
    private javax.swing.JTextField jTextField27;
    private javax.swing.JTextField jTextField28;
    private javax.swing.JTextField jTextField29;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    public javax.swing.JTextField jTextField7;
    public javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    private java.awt.List list1;
    // End of variables declaration//GEN-END:variables
}
