package transaction;

import Printing.printbarcode;
import connection.connection;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

/**
 *
 * @author My Pc
 */
public class Pur_trans_dualGst extends javax.swing.JFrame {

    public String choice = null;
    public String xz[];
    public String x[];
    public String y[];
    public String z[];
    static int count = 0;
    private LinkedHashMap SerialNoDataVectorMap = new LinkedHashMap();

    /**
     * Creates new form Pur_trans_dualGst
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    Calendar currentDate1 = Calendar.getInstance();
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
    String dateNow1 = formatter1.format(currentDate1.getTime());
    Calendar currentDate2 = Calendar.getInstance();
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyy");
    String dateNow2 = formatter2.format(currentDate2.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
    boolean isquantity;

    public String Party_cst = null;
    public boolean isIgstApplicable = false;
    double amntbeforetax = 0;
    double igst_percent = 0;
    double scgst_percent = 0;
    double iv = 0;
    double sv = 0;
    double cv = 0;
    double tiv = 0;
    double tsv = 0;
    double tcv = 0;
    double tiv1 = 0;
    double tsv1 = 0;
    double tcv1 = 0;
    public double freight_gst = 0;
    String Year;
    public String Barcode;
public String Company=""; 
    private static Pur_trans_dualGst obj = null;

    public Pur_trans_dualGst() {
        combo();
        initComponents();
        jDialog12.setLocationRelativeTo(null);
        this.setLocationRelativeTo(null);
        jDialog10.setLocationRelativeTo(null);
        jDialog11.setLocationRelativeTo(null);
        //dynamicbyforgation();

        
        
try
{
    
    connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement setting_st = connect.createStatement();
          ResultSet setting_rs = setting_st.executeQuery("Select * from setting");
            while (setting_rs.next()) {

                Company = setting_rs.getString("Company");
                System.out.println("company is"+Company);
            }
}
catch(Exception e)
{
    e.printStackTrace();
}
        
        
        
        try {
            jDateChooser2.setDate(formatter.parse(dateNow));
            java.util.Date dateafter = formatter.parse("20" + getyear + "-03-31");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jDateChooser2.setEnabled(false);
        jTextField2.setEnabled(false);
        jTextField3.setEnabled(false);
        jTextField4.setEnabled(false);
        jTextField5.setEnabled(false);
        jTextField6.setEnabled(false);
        jTextField7.setEnabled(false);
        jTextField8.setEnabled(false);
        jTextField9.setEnabled(false);
        jTextField10.setEnabled(false);
        jTextField11.setEnabled(false);
        jTextField12.setEnabled(false);
        jTextField13.setEnabled(false);
        jTextField14.setEnabled(false);
        jTextField15.setEnabled(false);
        jTextField16.setEnabled(false);
//        jTextField17.setEnabled(false);
        jTextField23.setEnabled(false);
        jRadioButton3.setEnabled(false);
        jRadioButton4.setEnabled(false);
        jComboBox3.setEnabled(false);
        jComboBox4.setEnabled(false);
        jComboBox1.setEnabled(false);
        jLabel29.setVisible(false);
        jLabel30.setVisible(false);
        jLabel31.setVisible(false);
        jLabel32.setVisible(false);
        jLabel43.setVisible(false);
        jLabel44.setVisible(false);
        jLabel45.setVisible(false);
        jLabel46.setVisible(false);
        jLabel38.setVisible(false);
        jLabel39.setVisible(false);
        jLabel15.setVisible(false);
        jTextField22.setEnabled(false);
        jTextField12.setEnabled(true);
        jButton5.setVisible(false);

        jLabel51.setVisible(false);
        jLabel52.setVisible(false);

        jComboBox4.addItem("ADD NEW");
        jComboBox9.addItem("ADD NEW");
        list1.addItem("ADD NEW");
        list2.addItem("ADD NEW");
        jLabel48.setText("" + Purchaseaccount);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement setting_st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from article_no ");
            while (rs.next()) {
                jComboBox1.addItem(rs.getString(1));
                jComboBox10.addItem(rs.getString(1));
            }
            ResultSet setting_rs = setting_st.executeQuery("Select Quantity from setting");
            while (setting_rs.next()) {
                Barcode = setting_rs.getString("Barcode");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from product ");
            while (rs.next()) {
                jComboBox3.addItem(rs.getString(1));
                jComboBox8.addItem(rs.getString(1));
                isquantity = rs.getBoolean("isquantityapplicable");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st1 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("select ledger_name from ledger where groups = 'PURCHASE ACCOUNTS' ");
            while (rs1.next()) {
                jComboBox5.addItem(rs1.getString(1));
                jComboBox6.addItem(rs1.getString(1));
                jComboBox7.addItem(rs1.getString(1));
//                AutoCompleteDecorator.decorate(jComboBox6);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        jComboBox1.addItem(null);

        jComboBox1.setSelectedItem(null);
        {
            jTextField3.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {

                }
            });
            jDialog4.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent e) {
                    jDialog4.dispose();
                    show();
                    jTextField26.requestFocus();

                }
            });
            jDialog5.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jDialog5.dispose();
                    show();
                    jTextField25.requestFocus();

                }
            });

            jDialog6.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    if (Purchaseaccount == null) {
                        JOptionPane.showMessageDialog(rootPane, "Please select Purchase Account");

                    }

                }
            });
            String purchase = "PR/";
            String slash = "/";
            String yearwise = Year.concat(slash);
//            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String finalconcat = (purchase.concat(yearwise));
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT Invoice_number FROM Stock1 where branchid='0'");
                int a = 0, b, d = 0;
                int i = 0;
                while (rs.next()) {
                    if (i == 0) {
                        d = rs.getString(1).lastIndexOf("/");
                        i++;
                    }
                    b = Integer.parseInt(rs.getString(1).substring(d + 1));
                    if (a < b) {
                        a = b;
                    }
                }
                String s = finalconcat.concat(Integer.toString(a + 1));
                jTextField2.setText("" + s);

            } catch (SQLException sqe) {
                sqe.printStackTrace();
            }

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGap(0, 643, Short.MAX_VALUE));
            layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGap(0, 517, Short.MAX_VALUE));

            pack();
        }// </editor-fold>     

    }

    public static Pur_trans_dualGst getObj() {
        if (obj == null) {
            obj = new Pur_trans_dualGst();
        }
        return obj;
    }

    private Object makeObj(final String item) {
        return new Object() {
            @Override
            public String toString() {
                return item;
            }
        };
    }
    //      rs = st.executeQuery("SELECT party_code from Party");

    public void combo() {

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT party_code from party");
            int a = 0;
            while (rs.next()) {
                a++;
            }
            x = new String[a];
            rs = st.executeQuery("SELECT party_code from party");
            a = 0;
            while (rs.next()) {

//          System.out.print(x[a]);
                x[a] = rs.getString("party_code");
                a++;
            }

            Statement st1 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("SELECT product_code from product");
            int a1 = 0;
            while (rs1.next()) {
                a1++;
            }
            xz = new String[a1];
            rs1 = st1.executeQuery("SELECT product_code from product");
            a1 = 0;
            while (rs1.next()) {
                xz[a1] = rs1.getString("product_code");
                a1++;
            }

            Statement st12 = connect.createStatement();
            ResultSet rs12 = st12.executeQuery("SELECT * from byforgation");
            int a12 = 0;
            while (rs12.next()) {
//                jComboBox4.addItem(rs12.getString(1));
                a12++;
            }
            y = new String[a12];
            rs12 = st12.executeQuery("SELECT * from byforgation");
            a12 = 0;
            while (rs12.next()) {
                y[a12] = rs12.getString(1);
                a12++;
            }
           Statement Combo_st = connect.createStatement();
          ResultSet rs_year = Combo_st.executeQuery("Select * from year");
            while(rs_year.next()){
            Year = rs_year.getString("year");
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        //jComboBox4.addItem("ADD NEW");
    }

//-----------After Discount Calculation-----------------------------------------
    public void reCalculate() {
        DecimalFormat df = new DecimalFormat("0.00");
        double discount_amount = Double.parseDouble(jTextField23.getText());
        double after_disc = 0;

        tiv = 0;
        tiv1 = 0;
        iv = 0;

        tsv = 0;
        tsv1 = 0;
        sv = 0;

        tcv = 0;
        tcv1 = 0;
        cv = 0;

        int rowcount = jTable1.getRowCount();
        double bill_amt = 0;
        for (int i = 0; i < rowcount; i++) {
            double totalamntafterbill = Double.parseDouble(jTextField13.getText());
            double discountpercentage = (discount_amount / totalamntafterbill) * 100;
            double amount = Double.parseDouble(jTable1.getValueAt(i, 3) + "") * Double.parseDouble(jTable1.getValueAt(i, 4) + "");
            double totalafterdiscountpercentage = (discountpercentage / 100) * amount;
            double totalfinal = amount - totalafterdiscountpercentage;
            jTable1.setValueAt(df.format(totalfinal), i, 5);

            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement igst_st = connect.createStatement();
                Statement scgst_st = connect.createStatement();
                if (isIgstApplicable == true) {
                    ResultSet igst_rs = igst_st.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(i, 1) + "") + "'");
                    while (igst_rs.next()) {
                        igst_percent = igst_rs.getDouble("percent");
                        double value = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
//                            amntbeforetax = (value / (100 + gst_percent)) * 100;
                        iv = (value * igst_percent) / 100;
                    }

                    i_value.add(iv);
                    percent.add(igst_percent);

                    if (igst_percent == 5) {
                        tiv = tiv + iv;
                        jTextField1.setText("" + df.format(tiv));
                    } else if (igst_percent == 12) {
                        tiv1 = tiv1 + iv;
                        jTextField27.setText("" + df.format(tiv1));
                    }
                } else if (isIgstApplicable == false) {
                    ResultSet scgst_rs = scgst_st.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(i, 1) + "") + "'");
                    while (scgst_rs.next()) {
                        scgst_percent = scgst_rs.getDouble("percent");
                        double value = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
//                            amntbeforetax = (value / (100 + gst_percent)) * 100;
                        sv = (value * (scgst_percent) / 2) / 100;
                        cv = (value * (scgst_percent) / 2) / 100;
                    }

                    s_value.add(sv);
                    c_value.add(cv);
                    percent.add((scgst_percent / 2));

                    if (scgst_percent == 5) {
                        tsv = tsv + sv;
                        tcv = tcv + cv;
                        jTextField17.setText("" + df.format(tcv));
                        jTextField24.setText("" + df.format(tsv));
                    } else if (scgst_percent == 12) {
                        tsv1 = tsv1 + sv;
                        tcv1 = tcv1 + cv;
                        jTextField25.setText("" + df.format(tcv1));
                        jTextField26.setText("" + df.format(tsv1));
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            after_disc = after_disc + Double.parseDouble(jTable1.getValueAt(i, 5) + "");
            jTextField15.setText("" + df.format(after_disc));

        }
        percentage();
        calculationall();
    }

//-----------------Get Discount Amount from Discount percentage----------------------
    public double amount() {
        double amt = 0;
        double to_amt = 0;
        double d_per = 0;

        d_per = Double.parseDouble(jTextField11.getText());
        to_amt = Double.parseDouble(jTextField13.getText());

        amt = (to_amt / 100) * d_per;

        jTextField23.setText("" + amt);

        return amt;
    }
//-----------------------------------------------------------------------------------    
//-------------Get Discount in Percentage from Discount value------------------------

    public double percentage() {

        float dis_per;
        double dis_amnt = 0;
        double total_amnt = 0;

        dis_amnt = Double.parseDouble(jTextField23.getText());
        total_amnt = Double.parseDouble(jTextField13.getText());

        dis_per = (float) ((dis_amnt / total_amnt) * 100);

        DecimalFormat df1 = new DecimalFormat("0.00");
        String per = df1.format(dis_per);

        jTextField11.setText(per + "%");
        return Double.parseDouble(per);
    }
//-----------------------------------------------------------------------------------    
//-------------------------------Freight with Gst------------------------------------

    public void freightGst() {
        DecimalFormat df = new DecimalFormat("0.00");
        double igst_amnt = 0;
        double sgst_amnt = 0;
        double cgst_amnt = 0;
        try {
            String fr_gst = (String) jComboBox11.getSelectedItem();
            double f_gst = Double.parseDouble(fr_gst);
            double f_amnt = Double.parseDouble(jTextField40.getText());

            if (isIgstApplicable == true) {
                igst_amnt = (f_amnt * f_gst) / 100;
                if (f_gst == 5) {
                    freight_gst = f_gst;
                    tiv = tiv + igst_amnt;
                } else if (f_gst == 12) {
                    freight_gst = f_gst;
                    tiv1 = tiv1 + igst_amnt;
                }
                jTextField1.setText("" + df.format(tiv));
                jTextField27.setText("" + df.format(tiv1));
            } else if (isIgstApplicable == false) {

                sgst_amnt = (f_amnt * (f_gst) / 2) / 100;
                cgst_amnt = (f_amnt * (f_gst) / 2) / 100;

                if (f_gst == 5) {
                    freight_gst = (f_gst / 2);
                    tsv = tsv + sgst_amnt;
                    tcv = tcv + cgst_amnt;
                } else if (f_gst == 12) {
                    freight_gst = (f_gst / 2);
                    tsv1 = tsv1 + sgst_amnt;
                    tcv1 = tcv1 + cgst_amnt;
                }
                jTextField17.setText("" + df.format(tsv));
                jTextField24.setText("" + df.format(tcv));
                jTextField25.setText("" + df.format(tsv1));
                jTextField26.setText("" + df.format(tcv1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        calculationall();
    }
//-----------------------------------------------------------------------------------    

    /**
     * This 00 is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jTextField19 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jTextField18 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jTextField20 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jDialog3 = new javax.swing.JDialog();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jCheckBox5 = new javax.swing.JCheckBox();
        jLabel35 = new javax.swing.JLabel();
        jDialog4 = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        list1 = new java.awt.List();
        jDialog6 = new javax.swing.JDialog();
        list2 = new java.awt.List();
        jDialog5 = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        list3 = new java.awt.List();
        jDialog7 = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        list4 = new java.awt.List();
        jDialog8 = new javax.swing.JDialog();
        list5 = new java.awt.List();
        jDialog9 = new javax.swing.JDialog();
        jPanel5 = new javax.swing.JPanel();
        list6 = new java.awt.List();
        jDialog10 = new javax.swing.JDialog();
        jPanel6 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jComboBox6 = new javax.swing.JComboBox<>();
        jLabel54 = new javax.swing.JLabel();
        jTextField29 = new javax.swing.JTextField();
        jButton8 = new javax.swing.JButton();
        jLabel55 = new javax.swing.JLabel();
        jTextField30 = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        jTextField31 = new javax.swing.JTextField();
        jLabel71 = new javax.swing.JLabel();
        jTextField41 = new javax.swing.JTextField();
        jLabel72 = new javax.swing.JLabel();
        jComboBox12 = new javax.swing.JComboBox<>();
        jDialog11 = new javax.swing.JDialog();
        jLabel58 = new javax.swing.JLabel();
        jComboBox7 = new javax.swing.JComboBox<>();
        jLabel59 = new javax.swing.JLabel();
        jComboBox8 = new javax.swing.JComboBox<>();
        jLabel60 = new javax.swing.JLabel();
        jTextField33 = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        jTextField34 = new javax.swing.JTextField();
        jLabel62 = new javax.swing.JLabel();
        jTextField35 = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        jTextField36 = new javax.swing.JTextField();
        jLabel64 = new javax.swing.JLabel();
        jTextField37 = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        jTextField38 = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        jComboBox9 = new javax.swing.JComboBox<>();
        jLabel67 = new javax.swing.JLabel();
        jComboBox10 = new javax.swing.JComboBox<>();
        jButton9 = new javax.swing.JButton();
        jLabel68 = new javax.swing.JLabel();
        jTextField39 = new javax.swing.JTextField();
        jDialog12 = new javax.swing.JDialog();
        jLabel70 = new javax.swing.JLabel();
        jComboBox11 = new javax.swing.JComboBox<>();
        jButton10 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jComboBox4 = new javax.swing.JComboBox(y);
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel28 = new javax.swing.JLabel();
        jTextField24 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jTextField25 = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jTextField26 = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jTextField28 = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel34 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jComboBox3 = new javax.swing.JComboBox();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel49 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jTextField27 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jComboBox5 = new javax.swing.JComboBox<>();
        jLabel57 = new javax.swing.JLabel();
        jTextField32 = new javax.swing.JTextField();
        jTextField40 = new javax.swing.JTextField();
        jLabel69 = new javax.swing.JLabel();

        jDialog1.setMinimumSize(new java.awt.Dimension(1000, 550));
        jDialog1.setUndecorated(true);

        jPanel1.setMinimumSize(new java.awt.Dimension(1000, 550));
        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 550));

        jScrollPane3.setAutoscrolls(true);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sno.", "Stock No", "PRate", "Retail", "P Net Rate", "byforgation", "Product", "Article no.", "ismeter", "qty", "tagprint", "Desc"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, true, true, true, true, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
        }

        jButton3.setText("OK");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });

        jButton7.setText("update");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton4.setText("Cancel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton7)
                .addGap(33, 33, 33)
                .addComponent(jButton3)
                .addGap(18, 18, 18)
                .addComponent(jButton4)
                .addGap(86, 86, 86))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 924, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 76, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton7)
                    .addComponent(jButton4))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 950, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(682, 580));

        jTextField19.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField19CaretUpdate(evt);
            }
        });
        jTextField19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField19ActionPerformed(evt);
            }
        });
        jTextField19.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField19KeyPressed(evt);
            }
        });

        jLabel24.setText("Enter Party code :");

        jTextField18.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField18CaretUpdate(evt);
            }
        });
        jTextField18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField18ActionPerformed(evt);
            }
        });
        jTextField18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField18KeyPressed(evt);
            }
        });

        jLabel25.setText("Enter Party Name :");

        jTextField20.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField20CaretUpdate(evt);
            }
        });
        jTextField20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField20KeyPressed(evt);
            }
        });

        jLabel26.setText("Enter city :");

        jLabel27.setText("Enter State :");

        jTextField21.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField21CaretUpdate(evt);
            }
        });
        jTextField21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField21KeyPressed(evt);
            }
        });

        jScrollPane5.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "party_code", "party_name", "city", "state", "opening_bal", "Balance Type", "Mark UP", "Whatsapp No", "IGST Applicable"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTable3);

        jScrollPane5.setViewportView(jScrollPane4);

        jButton6.setText("ADD NEW PARTY");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 699, Short.MAX_VALUE)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addGap(44, 44, 44)
                        .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jDialog2Layout.createSequentialGroup()
                            .addComponent(jLabel26)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog2Layout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog2Layout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addGap(23, 23, 23)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addGap(22, 22, 22)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jCheckBox1.setText("MRP");

        jCheckBox2.setText("Desc");

        jCheckBox3.setText("PDesc");

        jCheckBox4.setText("Byforgation");

        jCheckBox5.setText("Product");

        jLabel35.setText("Select for Printing ?");

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox4)
                    .addComponent(jCheckBox5)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(164, Short.MAX_VALUE))
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel35)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox5)
                .addContainerGap(153, Short.MAX_VALUE))
        );

        jDialog4.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog4.setMinimumSize(new java.awt.Dimension(200, 400));

        list1.setMaximumSize(new java.awt.Dimension(200, 400));
        list1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(list1, javax.swing.GroupLayout.PREFERRED_SIZE, 521, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog4Layout = new javax.swing.GroupLayout(jDialog4.getContentPane());
        jDialog4.getContentPane().setLayout(jDialog4Layout);
        jDialog4Layout.setHorizontalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog4Layout.setVerticalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog6.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog6.setTitle("SELECT PURCHASE ACCOUNT");
        jDialog6.setMinimumSize(new java.awt.Dimension(200, 400));

        list2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog6Layout = new javax.swing.GroupLayout(jDialog6.getContentPane());
        jDialog6.getContentPane().setLayout(jDialog6Layout);
        jDialog6Layout.setHorizontalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
        );
        jDialog6Layout.setVerticalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog6Layout.createSequentialGroup()
                .addComponent(list2, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jDialog5.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog5.setMinimumSize(new java.awt.Dimension(200, 400));

        list3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(list3, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list3, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog5Layout = new javax.swing.GroupLayout(jDialog5.getContentPane());
        jDialog5.getContentPane().setLayout(jDialog5Layout);
        jDialog5Layout.setHorizontalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog5Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );
        jDialog5Layout.setVerticalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog7.setMinimumSize(new java.awt.Dimension(200, 400));

        list4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list4, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list4, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog7Layout = new javax.swing.GroupLayout(jDialog7.getContentPane());
        jDialog7.getContentPane().setLayout(jDialog7Layout);
        jDialog7Layout.setHorizontalGroup(
            jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog7Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jDialog7Layout.setVerticalGroup(
            jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog8.setMinimumSize(new java.awt.Dimension(200, 431));

        list5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog8Layout = new javax.swing.GroupLayout(jDialog8.getContentPane());
        jDialog8.getContentPane().setLayout(jDialog8Layout);
        jDialog8Layout.setHorizontalGroup(
            jDialog8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list5, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
        );
        jDialog8Layout.setVerticalGroup(
            jDialog8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list5, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
        );

        jDialog9.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog9.setMinimumSize(new java.awt.Dimension(200, 400));

        list6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(list6, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list6, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog9Layout = new javax.swing.GroupLayout(jDialog9.getContentPane());
        jDialog9.getContentPane().setLayout(jDialog9Layout);
        jDialog9Layout.setHorizontalGroup(
            jDialog9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog9Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );
        jDialog9Layout.setVerticalGroup(
            jDialog9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog10.setSize(new java.awt.Dimension(339, 350));

        jLabel8.setText("Purchase Account");

        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox6KeyPressed(evt);
            }
        });

        jLabel54.setText("Quntity");

        jTextField29.setEditable(false);

        jButton8.setText("Update");
        jButton8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton8KeyPressed(evt);
            }
        });

        jLabel55.setText("Rate");

        jTextField30.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField30KeyPressed(evt);
            }
        });

        jLabel56.setText("Total Amount");

        jTextField31.setEditable(false);

        jLabel71.setText("Fre With Gst");

        jTextField41.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField41KeyPressed(evt);
            }
        });

        jLabel72.setText("Fre Gst Per.");

        jComboBox12.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "5", "12", "18", "28" }));
        jComboBox12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox12KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel54)
                    .addComponent(jLabel55)
                    .addComponent(jLabel56)
                    .addComponent(jLabel72)
                    .addComponent(jLabel71))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jComboBox12, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox6, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField29, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField30, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField31, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                    .addComponent(jTextField41))
                .addGap(15, 15, 15))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(jButton8)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel54)
                    .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel55)
                    .addComponent(jTextField30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel56)
                    .addComponent(jTextField31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel72)
                    .addComponent(jComboBox12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel71)
                    .addComponent(jTextField41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton8)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog10Layout = new javax.swing.GroupLayout(jDialog10.getContentPane());
        jDialog10.getContentPane().setLayout(jDialog10Layout);
        jDialog10Layout.setHorizontalGroup(
            jDialog10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog10Layout.setVerticalGroup(
            jDialog10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog11.setTitle("Purchase ReEntry");
        jDialog11.setSize(new java.awt.Dimension(443, 525));

        jLabel58.setText("Purchase Account - ");

        jComboBox7.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox7KeyPressed(evt);
            }
        });

        jLabel59.setText("Product - ");

        jComboBox8.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox8KeyPressed(evt);
            }
        });

        jLabel60.setText("Quantity - ");

        jTextField33.setEditable(false);

        jLabel61.setText("Rate - ");

        jTextField34.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField34KeyPressed(evt);
            }
        });

        jLabel62.setText("Amount - ");

        jTextField35.setEditable(false);

        jLabel63.setText("P Desc - ");

        jTextField36.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField36KeyPressed(evt);
            }
        });

        jLabel64.setText("Retail -");

        jTextField37.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField37KeyPressed(evt);
            }
        });

        jLabel65.setText("Desc - ");

        jTextField38.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField38KeyPressed(evt);
            }
        });

        jLabel66.setText("By Forgation - ");

        jComboBox9.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox9KeyPressed(evt);
            }
        });

        jLabel67.setText("Article No - ");

        jComboBox10.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBox10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox10KeyPressed(evt);
            }
        });

        jButton9.setText("Submit");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jButton9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton9KeyPressed(evt);
            }
        });

        jLabel68.setText("No Of Tag -");

        jTextField39.setText("1");

        javax.swing.GroupLayout jDialog11Layout = new javax.swing.GroupLayout(jDialog11.getContentPane());
        jDialog11.getContentPane().setLayout(jDialog11Layout);
        jDialog11Layout.setHorizontalGroup(
            jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog11Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel58)
                    .addComponent(jLabel59)
                    .addComponent(jLabel60)
                    .addComponent(jLabel61)
                    .addComponent(jLabel62)
                    .addComponent(jLabel63)
                    .addComponent(jLabel64)
                    .addComponent(jLabel65)
                    .addComponent(jLabel66)
                    .addComponent(jLabel67)
                    .addComponent(jLabel68))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog11Layout.createSequentialGroup()
                        .addComponent(jTextField39, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton9))
                    .addGroup(jDialog11Layout.createSequentialGroup()
                        .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboBox7, 0, 188, Short.MAX_VALUE)
                            .addComponent(jComboBox8, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField33)
                            .addComponent(jTextField34)
                            .addComponent(jTextField35)
                            .addComponent(jTextField36)
                            .addComponent(jTextField37)
                            .addComponent(jTextField38)
                            .addComponent(jComboBox9, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox10, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 87, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jDialog11Layout.setVerticalGroup(
            jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog11Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel58)
                    .addComponent(jComboBox7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel59))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel60)
                    .addComponent(jTextField33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel61))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel62)
                    .addComponent(jTextField35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel63)
                    .addComponent(jTextField36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel64)
                    .addComponent(jTextField37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel65)
                    .addComponent(jTextField38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel66)
                    .addComponent(jComboBox9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel67)
                    .addComponent(jComboBox10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton9)
                    .addComponent(jLabel68)
                    .addComponent(jTextField39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        jDialog12.setTitle("Freight Gst");
        jDialog12.setSize(new java.awt.Dimension(324, 141));

        jLabel70.setText("Select GST Percentage - ");

        jComboBox11.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "5", "12", "18", "28" }));
        jComboBox11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox11KeyPressed(evt);
            }
        });

        jButton10.setText("Submit");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jButton10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton10KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jDialog12Layout = new javax.swing.GroupLayout(jDialog12.getContentPane());
        jDialog12.getContentPane().setLayout(jDialog12Layout);
        jDialog12Layout.setHorizontalGroup(
            jDialog12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog12Layout.createSequentialGroup()
                .addGroup(jDialog12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog12Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel70)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox11, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog12Layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(jButton10)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jDialog12Layout.setVerticalGroup(
            jDialog12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog12Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jDialog12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel70)
                    .addComponent(jComboBox11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(jButton10)
                .addGap(21, 21, 21))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : PURCHASE");
        setMinimumSize(new java.awt.Dimension(1165, 529));
        setResizable(false);
        setSize(new java.awt.Dimension(1165, 529));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jLabel1.setText("Supplier ID :");

        jLabel2.setText("Invoice Date :");

        jLabel3.setText("Invoice No. :");

        jTextField2.setText("1");
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel4.setText("Purchase Type :");

        jLabel5.setText("Purchase Reference No. :");

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel6.setText("S No.");

        jTextField5.setEditable(false);
        jTextField5.setText("1");
        jTextField5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField5FocusGained(evt);
            }
        });
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jLabel7.setText("Qty.");

        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });
        jTextField6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField6FocusLost(evt);
            }
        });
        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel9.setText("Amount");

        jTextField7.setEditable(false);
        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });

        jLabel10.setText("P Desc.");

        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jLabel11.setText("Retail");

        jTextField9.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField9FocusLost(evt);
            }
        });
        jTextField9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField9KeyPressed(evt);
            }
        });

        jLabel12.setText("Desc.");

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });
        jTextField10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField10KeyPressed(evt);
            }
        });

        jLabel13.setText("Product");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SNO.", "PUR ACCOUNT", "PRODUCT", "QTY.", "Rate", "AMOUNT", "P DESC", "RETAIL", "DESC.", "BYFORGATION", "ARTICLE NO.", "No of Tag"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(50);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(1).setMinWidth(130);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(180);
            jTable1.getColumnModel().getColumn(2).setMinWidth(120);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(150);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
        }

        jLabel14.setText("Supplier Name :");

        jTextField12.setEditable(false);
        jTextField12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField12KeyPressed(evt);
            }
        });

        jLabel16.setText("Bill Amount :");

        jTextField13.setEditable(false);
        jTextField13.setText("0");
        jTextField13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField13KeyPressed(evt);
            }
        });

        jLabel17.setText("Other Charges ");

        jTextField14.setText("0");
        jTextField14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField14FocusLost(evt);
            }
        });
        jTextField14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField14ActionPerformed(evt);
            }
        });
        jTextField14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField14KeyPressed(evt);
            }
        });

        jLabel19.setText("Net Amount ");

        jTextField16.setEditable(false);
        jTextField16.setText("0");
        jTextField16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField16FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField16FocusLost(evt);
            }
        });
        jTextField16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField16KeyPressed(evt);
            }
        });

        jLabel20.setText("Rate");

        jTextField3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField3FocusLost(evt);
            }
        });
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        jLabel21.setText("Discount (%) :");

        jTextField11.setText("0");
        jTextField11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField11FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField11FocusLost(evt);
            }
        });
        jTextField11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField11KeyPressed(evt);
            }
        });

        jLabel18.setText("Amount After Dis. :");

        jTextField15.setEditable(false);
        jTextField15.setText("0");
        jTextField15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField15FocusGained(evt);
            }
        });
        jTextField15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField15KeyPressed(evt);
            }
        });

        jLabel22.setText("CGST (2.5) ");

        jTextField17.setText("0");
        jTextField17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField17FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField17FocusLost(evt);
            }
        });
        jTextField17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField17KeyPressed(evt);
            }
        });

        jLabel23.setText("Byforgation");

        jComboBox4.setSelectedItem(null);
        jComboBox4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jComboBox4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox4ItemStateChanged(evt);
            }
        });
        jComboBox4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBox4FocusGained(evt);
            }
        });
        jComboBox4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboBox4MouseClicked(evt);
            }
        });
        jComboBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox4ActionPerformed(evt);
            }
        });
        jComboBox4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox4KeyPressed(evt);
            }
        });

        jLabel29.setText("jLabel29");

        jLabel30.setText("jLabel30");

        jLabel31.setText("jLabel31");
        jLabel31.setEnabled(false);

        jLabel32.setText("jLabel32");
        jLabel32.setEnabled(false);

        jTextField22.setEditable(false);
        jTextField22.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField22FocusLost(evt);
            }
        });
        jTextField22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField22ActionPerformed(evt);
            }
        });
        jTextField22.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField22KeyPressed(evt);
            }
        });

        jLabel33.setText("Discount Rs. :");

        jTextField23.setText("0");
        jTextField23.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField23FocusLost(evt);
            }
        });
        jTextField23.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField23KeyPressed(evt);
            }
        });

        buttonGroup1.add(jRadioButton3);
        jRadioButton3.setText("Cash");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton4);
        jRadioButton4.setText("Credit");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });

        jLabel28.setText("SGST (2.5%) ");

        jTextField24.setText("0");
        jTextField24.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField24FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField24FocusLost(evt);
            }
        });
        jTextField24.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField24KeyPressed(evt);
            }
        });

        jLabel36.setText("CGST  (6%) ");

        jTextField25.setText("0");
        jTextField25.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField25FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField25FocusLost(evt);
            }
        });
        jTextField25.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField25KeyPressed(evt);
            }
        });

        jLabel37.setText("SGST (6%)");

        jTextField26.setText("0");
        jTextField26.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField26FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField26FocusLost(evt);
            }
        });
        jTextField26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField26ActionPerformed(evt);
            }
        });
        jTextField26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField26KeyPressed(evt);
            }
        });

        jLabel38.setText("jLabel38");
        jLabel38.setEnabled(false);

        jLabel15.setText("jLabel15");
        jLabel15.setEnabled(false);

        jLabel39.setText("jLabel39");
        jLabel39.setEnabled(false);

        jLabel40.setText("Party Purchase Date :");

        jLabel41.setText("Article No.");

        jComboBox1.setSelectedItem(null);
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBox1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jComboBox1FocusLost(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jTextField28.setText("0");
        jTextField28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField28KeyPressed(evt);
            }
        });

        jLabel42.setText("Total quantity :");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" }));
        jComboBox2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jComboBox2FocusLost(evt);
            }
        });
        jComboBox2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox2KeyPressed(evt);
            }
        });

        jLabel34.setText("No of Tag");

        jLabel43.setText("jLabel43");
        jLabel43.setEnabled(false);

        jLabel44.setText("jLabel44");
        jLabel44.setEnabled(false);

        jLabel45.setText("jLabel45");
        jLabel45.setEnabled(false);

        jLabel46.setText("jLabel46");
        jLabel46.setEnabled(false);

        jLabel47.setText("Purchase account :");

        jLabel48.setText("jLabel48");

        jButton5.setText("UPDATE");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jComboBox3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox3KeyPressed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");
        jDateChooser1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jDateChooser1KeyPressed(evt);
            }
        });

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jLabel49.setText("IGST (5%) ");

        jTextField1.setText("0");
        jTextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField1FocusLost(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel50.setText("IGST (12%)");

        jTextField27.setText("0");
        jTextField27.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField27KeyPressed(evt);
            }
        });

        jLabel51.setText("jLabel51");
        jLabel51.setEnabled(false);

        jLabel52.setText("jLabel52");
        jLabel52.setEnabled(false);

        jLabel53.setText("Pur Account");

        jComboBox5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox5KeyPressed(evt);
            }
        });

        jLabel57.setText("Discount %: -");

        jTextField32.setText("0");
        jTextField32.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField32KeyPressed(evt);
            }
        });

        jTextField40.setText("0");
        jTextField40.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField40KeyPressed(evt);
            }
        });

        jLabel69.setText("Fre. With Gst");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel33)
                                .addGap(41, 41, 41)
                                .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)
                                .addComponent(jLabel37))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel18)
                                    .addGap(18, 18, 18)
                                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(24, 24, 24)
                                    .addComponent(jLabel49)
                                    .addGap(48, 48, 48)
                                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jComboBox5, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel53)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel20)
                                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel23)
                                    .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel41)
                                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel34)
                                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(40, 40, 40)
                                .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel29)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel30)
                                .addGap(127, 127, 127)
                                .addComponent(jLabel2)
                                .addGap(73, 73, 73)
                                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(158, 158, 158)
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField2))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel4))
                                .addGap(17, 17, 17)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jRadioButton3)
                                        .addGap(18, 18, 18)
                                        .addComponent(jRadioButton4)))
                                .addGap(25, 25, 25)
                                .addComponent(jLabel47)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel57)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField32, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(64, 64, 64)
                                        .addComponent(jLabel5)
                                        .addGap(10, 10, 10)
                                        .addComponent(jTextField4)))))))
                .addGap(6, 6, 6))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addGap(50, 50, 50)
                                .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)
                                .addComponent(jLabel22))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel21)
                                        .addGap(197, 197, 197))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(24, 24, 24)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel36)
                                    .addComponent(jLabel28)))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel42)
                                .addGap(173, 173, 173)))))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField25, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel50)
                        .addGap(48, 48, 48)
                        .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel69))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField40)
                            .addComponent(jTextField14))))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel31)
                        .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(jLabel51))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel38))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel52)
                            .addComponent(jLabel39))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel43, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel44, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel45, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel46, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(0, 99, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel29)
                            .addComponent(jLabel30)
                            .addComponent(jLabel2))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel40))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(12, 12, 12)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel5)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(12, 12, 12)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel47)
                                    .addComponent(jLabel48))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel57)
                                    .addComponent(jTextField32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(12, 12, 12)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jRadioButton3)
                                    .addComponent(jLabel4))
                                .addComponent(jRadioButton4)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel6)
                                                .addComponent(jLabel53)
                                                .addComponent(jLabel13))
                                            .addComponent(jLabel9)
                                            .addComponent(jLabel10)
                                            .addComponent(jLabel11)
                                            .addComponent(jLabel12))
                                        .addGap(12, 12, 12)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel41)
                                            .addGap(13, 13, 13)
                                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel34)
                                            .addGap(13, 13, 13)
                                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel20)
                                        .addGap(12, 12, 12)
                                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addGap(12, 12, 12)
                                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(12, 12, 12)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel22))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel50)
                                        .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(jLabel16)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabel42)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel21))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel52)
                                                        .addComponent(jLabel51))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel31)
                                                        .addComponent(jLabel39)))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel43)
                                                    .addGap(4, 4, 4)
                                                    .addComponent(jLabel44)))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel15)
                                                .addComponent(jLabel32)
                                                .addComponent(jLabel38)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel36)))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGap(6, 6, 6)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jTextField25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel17)
                                                        .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel28)
                                                .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTextField40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel69))))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel45)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel46)))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel19)
                                .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel33)
                                    .addComponent(jLabel37))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButton2)
                                .addComponent(jButton5)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel18)
                                    .addComponent(jLabel49))))
                        .addContainerGap(12, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addGap(13, 13, 13)
                        .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    }//GEN-LAST:event_formKeyPressed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
    }//GEN-LAST:event_formKeyReleased

    private void jTextField12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField12KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jRadioButton3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jDateChooser2.requestFocus();
        }
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_F2) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField12KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jTextField32.requestFocus();

        }
        if (key == evt.VK_ESCAPE) {
            jRadioButton3.requestFocus();
        }
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField6.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            if (jTextField6.getText().isEmpty()) {
                jTextField23.requestFocus();
            } else {
                jTextField3.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField8.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField6.requestFocus();
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField9.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField3.requestFocus();
        }
    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTextField9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField9KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField10.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField8.requestFocus();
        }
    }//GEN-LAST:event_jTextField9KeyPressed

    private void jTextField10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField10KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox4.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField9.requestFocus();
        }

    }//GEN-LAST:event_jTextField10KeyPressed
    static int f = 0;
    static int s = 1;
    String stoke = null;
    static String sn;
    static String qnt;
    static String rate, amount, pds, rt, ds, pd, fr, articleno, samebarcode;
    public static String sup, invod, invon, purch, ref;
    static String bil, otherc, va, namoun;
    static String zero = "000000";
    static String date1;
    static int first, second = 0;
    int gg = 0;
    int ggg = 0;
    int incr = 0;
    String supid[] = new String[100];
    String supnameid[] = new String[100];
    String invoiced[] = new String[100];
    String invoiceno[] = new String[100];
    String purchast[] = new String[100];
    String refrence[] = new String[100];
    String sno[] = new String[100];
    String qty[] = new String[100];
    String rat[] = new String[100];
    String amoun[] = new String[100];
    String pdes[] = new String[100];
    String retail[] = new String[100];
    String desc[] = new String[100];
    String prod[] = new String[100];
    String ftr[] = new String[100];
    String bill[] = new String[100];
    String ocharge[] = new String[100];
    String tax[] = new String[100];
    String discount[] = new String[100];
    String amountafter[] = new String[100];
    String namount[] = new String[100];
    String articlenum[] = new String[100];
    String samebarcode1[] = new String[100];
    static double famount = 0;
    static double mount = 0;
    double fvat = 0;
    static double ffvat = 0;
    String supname = null;
    ArrayList stockno = new ArrayList();
    int ismeter = 0;

    public void stocknumbers() {
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yy");
        String year = formatter.format(currentDate.getTime());
        SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
        String month = formatter1.format(currentDate.getTime());
        int sto = 0;
        String totalbarcode = null;
        String sto1 = null;
        String start = "";
//        date1 = year.concat(month);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement stmeter = connect.createStatement();
            ResultSet rs = st.executeQuery("select max(Stock_No) from Stockid where branchid = '0' and Da_te>'2018-03-31' ");
            while (rs.next()) {
                sto1 = (rs.getString(1));

                if (rs.getString(1) != null) {
                    start = (rs.getString(1).substring(0, 2));
                }
            }
            ResultSet rsmeter = stmeter.executeQuery("select ismeter from product where product_code='" + jComboBox3.getSelectedItem() + "" + "'");
            while (rsmeter.next()) {
                ismeter = rsmeter.getInt(1);
            }
           
            if(Company.equals("Muskan"))
            {
            year="20";
           zero="0000";
                if (sto1 == null) {

                stoke = year.concat(zero);
                 sto = Integer.parseInt(stoke);
            } else {
                sto = Integer.parseInt(sto1);

            }
                
            }
            else
            {
            if (sto1 == null || Integer.parseInt(start) != Integer.parseInt(year)) {

                stoke = year.concat(zero);

                sto = Integer.parseInt(stoke);
            } else {
                sto = Integer.parseInt(sto1);

            }
                
                
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        jDialog1.setVisible(true);
        int j = 1;
        double q = Double.parseDouble(qnt);

        if (ismeter == 0) {
            if (SerialNoDataVectorMap != null && SerialNoDataVectorMap.size() > 0) {
                Vector aDataVector = null;
                Iterator aIterator = SerialNoDataVectorMap.keySet().iterator();
                while (aIterator.hasNext()) {
                    aDataVector = (Vector) SerialNoDataVectorMap.get(aIterator.next());
                    if (aDataVector != null && aDataVector.size() > 0) {
                        sto = sto + aDataVector.size();
                    }
                }

            }

            DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            for (int i = 0; i < q; i++) {
                sto = sto + 1;
                Object d[] = {1, sto, rate, rt, pds, fr, pd, articleno, ismeter, 1, (String) jComboBox2.getSelectedItem() , ds};
                dtm = (DefaultTableModel) jTable2.getModel();
                dtm.addRow(d);
            }

        } else if (ismeter == 1) {

            if (SerialNoDataVectorMap != null && SerialNoDataVectorMap.size() > 0) {
                Vector aDataVector = null;
                Iterator aIterator = SerialNoDataVectorMap.keySet().iterator();
                while (aIterator.hasNext()) {
                    aDataVector = (Vector) SerialNoDataVectorMap.get(aIterator.next());
                    if (aDataVector != null && aDataVector.size() > 0) {
                        sto = sto + aDataVector.size();
                    }
                }

            }

            sto = sto + 1;
            DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            Object d[] = {1, sto, rate, rt, pds, fr, pd, articleno, ismeter, qnt, (String) jComboBox2.getSelectedItem(), ds};
            dtm = (DefaultTableModel) jTable2.getModel();
            dtm.addRow(d);
        }

    }

//    public void rigidcalculation() {
//        double tot = 0.0;
//        double amnt = Double.parseDouble(jTextField13.getText());
//        double disc = Double.parseDouble(jTextField23.getText());
//        double dam = amnt - disc;
//        jTextField15.setText("" + dam);
//        double taxamt = Double.parseDouble(jTextField25.getText());
//        double oth = Double.parseDouble(jTextField14.getText());
//        double Vatamt = Double.parseDouble(jTextField26.getText());
//        double igstamnt = Double.parseDouble(jTextField27.getText());
//
//        tot = tot + Vatamt + dam + taxamt + oth + igstamnt;
//        jTextField16.setText("" + tot);
//    }
    public void calculationall() {
        DecimalFormat df = new DecimalFormat("0.00");
        double amnt = Double.parseDouble(jTextField13.getText());
//        double discpercc = Double.parseDouble(jTextField11.getText());
        double discamnt = Double.parseDouble(jTextField23.getText());
        double dam = amnt - discamnt;

        double cgst_5 = Double.parseDouble(jTextField17.getText());
        double cgst_12 = Double.parseDouble(jTextField25.getText());

        double sgst_5 = Double.parseDouble(jTextField24.getText());
        double sgst_12 = Double.parseDouble(jTextField26.getText());

        double igst_5 = Double.parseDouble(jTextField1.getText());
        double igst_12 = Double.parseDouble(jTextField27.getText());

        double oth = Double.parseDouble(jTextField14.getText());

        double freight = Double.parseDouble(jTextField40.getText());

        double tot = cgst_5 + cgst_12 + sgst_5 + sgst_12 + igst_5 + igst_12 + oth + dam + freight;

//        jTextField23.setText("" + discamnt);
        jTextField15.setText("" + df.format(dam));
        jTextField16.setText("" + df.format(tot));
    }

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed
    ArrayList Prate = new ArrayList();
    ArrayList Retail = new ArrayList();
    ArrayList rdesc = new ArrayList();
    ArrayList byforgation = new ArrayList();
    ArrayList product = new ArrayList();

    public void insert() {
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        Vector aDataVector = new Vector(dtm.getDataVector());
        int aNo = Integer.parseInt(jTextField5.getText());
        aNo--;

        SerialNoDataVectorMap.put(aNo + "", aDataVector);

        // Pur_trans_dualGst pt=new Pur_trans_dualGst();
        jDialog1.setVisible(false);
        jTextField6.requestFocus();
    }
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        insert();

    }//GEN-LAST:event_jButton3ActionPerformed
    int p = 0;

    String Discountaccount = "";
    double updateddisccurrbal = 0;
    String updateddisccurrbaltype = "";
    double disc_value = 0;
    String disc_type = "";
    double igst_value = 0;
    String igst_type = "";
    double sgst_value = 0;
    String sgst_type = "";
    double cgst_value = 0;
    String cgst_type = "";
    double igst_value1 = 0;
    String igst_type1 = "";
    double sgst_value1 = 0;
    String sgst_type1 = "";
    double cgst_value1 = 0;
    String cgst_type1 = "";
    double update_igst_value = 0;
    String update_igst_type = "";
    double update_sgst_value = 0;
    String update_sgst_type = "";
    double update_cgst_value = 0;
    String update_cgst_type = "";
    double update_igst_value1 = 0;
    String update_igst_type1 = "";
    double update_sgst_value1 = 0;
    String update_sgst_type1 = "";
    double update_cgst_value1 = 0;
    String update_cgst_type1 = "";
    double update_other_value = 0;
    String update_other_type = "";
    double update_freight_value = 0;
    String update_freight_type = "";
    double update_creditor_value = 0;
    String update_creditor_type = "";
    double update_cash_value = 0;
    String update_cash_type = "";
    ArrayList purc_account = new ArrayList();
    ArrayList purc_currbal = new ArrayList();
    ArrayList purc_amnt = new ArrayList();
    ArrayList purc_type = new ArrayList();
    ArrayList update_purc_amnt = new ArrayList();
    ArrayList update_purc_type = new ArrayList();

    public void updatebalances() {
//---------------For Igst-----------------------------------------------------------------------------------
        if (Double.parseDouble(jTextField1.getText()) != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement igst_st = connect.createStatement();
                ResultSet igst_rs = igst_st.executeQuery("Select * from ledger where ledger_name = 'IGST 5%'");
                while (igst_rs.next()) {
                    igst_value = igst_rs.getDouble("curr_bal");
                    igst_type = igst_rs.getString("currbal_type");
                }

                double igst_amnt = Double.parseDouble(jTextField1.getText());
                double iamnt;

                if (igst_type.equals("CR")) {
                    iamnt = igst_amnt - igst_value;
                    if (iamnt < 0) {
                        update_igst_type = "CR";
                        update_igst_value = Math.abs(iamnt);
                    } else {
                        update_igst_type = "DR";
                        update_igst_value = Math.abs(iamnt);
                    }
                } else {
                    if (igst_type.equals("DR")) {
                        iamnt = igst_amnt + igst_value;
                        if (iamnt >= 0) {
                            update_igst_type = "DR";
                            update_igst_value = Math.abs(iamnt);
                        }
                    }
                }
                igst_st.executeUpdate("Update ledger SET curr_bal= '" + update_igst_value + "',currbal_type='" + update_igst_type + "' where ledger_name = 'IGST 5%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Double.parseDouble(jTextField27.getText()) != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement igst_st1 = connect.createStatement();
                ResultSet igst_rs1 = igst_st1.executeQuery("Select * from ledger where ledger_name = 'IGST 12%'");
                while (igst_rs1.next()) {
                    igst_value1 = igst_rs1.getDouble("curr_bal");
                    igst_type1 = igst_rs1.getString("currbal_type");
                }

                double igst_amnt = Double.parseDouble(jTextField27.getText());
                double iamnt;

                if (igst_type1.equals("CR")) {
                    iamnt = igst_amnt - igst_value1;
                    if (iamnt < 0) {
                        update_igst_type1 = "CR";
                        update_igst_value1 = Math.abs(iamnt);
                    } else {
                        update_igst_type1 = "DR";
                        update_igst_value1 = Math.abs(iamnt);
                    }
                } else {
                    if (igst_type1.equals("DR")) {
                        iamnt = igst_amnt + igst_value1;
                        if (iamnt >= 0) {
                            update_igst_type1 = "DR";
                            update_igst_value1 = Math.abs(iamnt);
                        }
                    }
                }
                igst_st1.executeUpdate("Update ledger SET curr_bal= '" + update_igst_value1 + "',currbal_type='" + update_igst_type1 + "' where ledger_name = 'IGST 12%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   

//------------------For Sgst-------------------------------------------------------------------------------------------------
        if (Double.parseDouble(jTextField24.getText()) != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement sgst_st = connect.createStatement();
                ResultSet sgst_rs = sgst_st.executeQuery("Select * from ledger where ledger_name = 'SGST 2.5%'");
                while (sgst_rs.next()) {
                    sgst_value = sgst_rs.getDouble("curr_bal");
                    sgst_type = sgst_rs.getString("currbal_type");
                }

                double sgst_amnt = Double.parseDouble(jTextField24.getText());
                double samnt;

                if (sgst_type.equals("CR")) {
                    samnt = sgst_amnt - sgst_value;
                    if (samnt < 0) {
                        update_sgst_type = "CR";
                        update_sgst_value = Math.abs(samnt);
                    } else {
                        update_sgst_type = "DR";
                        update_sgst_value = Math.abs(samnt);
                    }
                } else {
                    if (sgst_type.equals("DR")) {
                        samnt = sgst_amnt + sgst_value;
                        if (samnt >= 0) {
                            update_sgst_type = "DR";
                            update_sgst_value = Math.abs(samnt);
                        }
                    }
                }
                sgst_st.executeUpdate("Update ledger SET curr_bal= '" + update_sgst_value + "',currbal_type='" + update_sgst_type + "' where ledger_name = 'SGST 2.5%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (Double.parseDouble(jTextField26.getText()) != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement sgst_st1 = connect.createStatement();
                ResultSet sgst_rs1 = sgst_st1.executeQuery("Select * from ledger where ledger_name = 'SGST 6%'");
                while (sgst_rs1.next()) {
                    sgst_value1 = sgst_rs1.getDouble("curr_bal");
                    sgst_type1 = sgst_rs1.getString("currbal_type");
                }

                double sgst_amnt = Double.parseDouble(jTextField26.getText());
                double samnt;

                if (sgst_type1.equals("CR")) {
                    samnt = sgst_amnt - sgst_value1;
                    if (samnt < 0) {
                        update_sgst_type1 = "CR";
                        update_sgst_value1 = Math.abs(samnt);
                    } else {
                        update_sgst_type1 = "DR";
                        update_sgst_value1 = Math.abs(samnt);
                    }
                } else {
                    if (sgst_type1.equals("DR")) {
                        samnt = sgst_amnt + sgst_value1;
                        if (samnt >= 0) {
                            update_sgst_type1 = "DR";
                            update_sgst_value1 = Math.abs(samnt);
                        }
                    }
                }
                sgst_st1.executeUpdate("Update ledger SET curr_bal= '" + update_sgst_value1 + "',currbal_type='" + update_sgst_type1 + "' where ledger_name = 'SGST 6%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   

//-----------------For Cgst--------------------------------------------------------------------------------------------------
        if (Double.parseDouble(jTextField17.getText()) != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement cgst_st = connect.createStatement();
                ResultSet cgst_rs = cgst_st.executeQuery("Select * from ledger where ledger_name = 'CGST 2.5%'");
                while (cgst_rs.next()) {
                    cgst_value = cgst_rs.getDouble("curr_bal");
                    cgst_type = cgst_rs.getString("currbal_type");
                }

                double cgst_amnt = Double.parseDouble(jTextField17.getText());
                double camnt;

                if (cgst_type.equals("CR")) {
                    camnt = cgst_amnt - cgst_value;
                    if (camnt < 0) {
                        update_cgst_type = "CR";
                        update_cgst_value = Math.abs(camnt);
                    } else {
                        update_cgst_type = "DR";
                        update_cgst_value = Math.abs(camnt);
                    }
                } else {
                    if (cgst_type.equals("DR")) {
                        camnt = cgst_amnt + cgst_value;
                        if (camnt >= 0) {
                            update_cgst_type = "DR";
                            update_cgst_value = Math.abs(camnt);
                        }
                    }
                }
                cgst_st.executeUpdate("Update ledger SET curr_bal= '" + update_cgst_value + "',currbal_type='" + update_cgst_type + "' where ledger_name = 'CGST 2.5%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (Double.parseDouble(jTextField25.getText()) != 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement cgst_st1 = connect.createStatement();
                ResultSet cgst_rs1 = cgst_st1.executeQuery("Select * from ledger where ledger_name = 'CGST 6%'");
                while (cgst_rs1.next()) {
                    cgst_value1 = cgst_rs1.getDouble("curr_bal");
                    cgst_type1 = cgst_rs1.getString("currbal_type");
                }

                double cgst_amnt = Double.parseDouble(jTextField25.getText());
                double camnt;

                if (cgst_type1.equals("CR")) {
                    camnt = cgst_amnt - cgst_value1;
                    if (camnt < 0) {
                        update_cgst_type1 = "CR";
                        update_cgst_value1 = Math.abs(camnt);
                    } else {
                        update_cgst_type1 = "DR";
                        update_cgst_value1 = Math.abs(camnt);
                    }
                } else {
                    if (cgst_type1.equals("DR")) {
                        camnt = cgst_amnt + cgst_value1;
                        if (camnt >= 0) {
                            update_cgst_type1 = "DR";
                            update_cgst_value1 = Math.abs(camnt);
                        }
                    }
                }
                cgst_st1.executeUpdate("Update ledger SET curr_bal= '" + update_cgst_value1 + "',currbal_type='" + update_cgst_type1 + "' where ledger_name = 'CGST 6%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------

//------------------------For Discount Account--------------------------------------------------------------------------------
//        if (Double.parseDouble(jTextField23.getText()) != 0) {
//            try {
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement disc_st = connect.createStatement();
//                ResultSet disc_rs = disc_st.executeQuery("Select * from ledger where ledger_name = 'Discount Account'");
//                while (disc_rs.next()) {
//                    disc_value = disc_rs.getDouble("curr_bal");
//                    disc_type = disc_rs.getString("currbal_type");
//                }
//
//                double disc_amnt = Double.parseDouble(jTextField23.getText());
//                double damnt;
//
//                if (disc_type.equals("CR")) {
//                    damnt = disc_amnt - disc_value;
//                    if (damnt < 0) {
//                        updateddisccurrbaltype = "CR";
//                        updateddisccurrbal = Math.abs(damnt);
//                    } else {
//                        updateddisccurrbaltype = "DR";
//                        updateddisccurrbal = Math.abs(damnt);
//                    }
//                } else {
//                    if (disc_type.equals("DR")) {
//                        damnt = disc_amnt + disc_value;
//                        if (damnt > 0) {
//                            updateddisccurrbaltype = "DR";
//                            updateddisccurrbal = Math.abs(damnt);
//                        }
//                    }
//                }
//
//                
//                disc_st.executeUpdate("Update ledger SET curr_bal= '" + updateddisccurrbal + "',currbal_type='" + updateddisccurrbaltype + "' where ledger_name = 'Discount Account'");
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//---------------------------------------------------------------------------------------------------------------------------
//-----------------------------For Other Charges--------------------------------------------------------------------------
        if ((Double.parseDouble(jTextField14.getText()) == 0) && (other_charges == null)) {

        } else if ((Double.parseDouble(jTextField14.getText()) != 0) && (other_charges == null)) {
            JOptionPane.showMessageDialog(rootPane, "Please choose Direct expenses account");
            jTextField14.requestFocus();
        } else {
            try {
                double othercurrbal = 0;
                String Curbaltypeother = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement other_st = connect.createStatement();
                ResultSet other_rs = other_st.executeQuery("Select * from ledger where ledger_name = '" + other_charges + "'");

                while (other_rs.next()) {
                    othercurrbal = other_rs.getDouble("curr_bal");
                    Curbaltypeother = other_rs.getString("currbal_type");
                }
                double otheramount = Double.parseDouble(jTextField14.getText());
                double aother;

                if (Curbaltypeother.equals("CR")) {
                    aother = otheramount - othercurrbal;
                    if (aother < 0) {
                        update_other_type = "CR";
                        update_other_value = Math.abs(aother);

                    } else {
                        update_other_type = "DR";
                        update_other_value = Math.abs(aother);
                    }

                } else {
                    if (Curbaltypeother.equals("DR")) {
                        aother = otheramount + othercurrbal;
                        if (aother >= 0) {
                            update_other_type = "DR";
                            update_other_value = Math.abs(aother);
                        }
                    }
                }

                other_st.executeUpdate("Update ledger SET curr_bal= '" + update_other_value + "',currbal_type='" + update_other_type + "' where ledger_name='" + other_charges + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------
//--------------For Creaditor and Cash---------------------------------------------------------------------------------------
        if (jRadioButton3.isSelected()) {
            try {
                double cashcurrbal = 0;
                String Curbaltypecash = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement cash_st = connect.createStatement();
                ResultSet cash_rs = cash_st.executeQuery("Select * from ledger where ledger_name = 'CASH'");

                while (cash_rs.next()) {
                    cashcurrbal = cash_rs.getDouble("curr_bal");
                    Curbaltypecash = cash_rs.getString("currbal_type");
                }
                double cash_amount = Double.parseDouble(jTextField16.getText());
                double cash_amnt = 0;

                if (Curbaltypecash.equals("CR")) {
                    cash_amnt = cashcurrbal + cash_amount;
                    update_cash_value = Math.abs(cash_amnt);
                } else if (Curbaltypecash.equals("DR")) {
                    if (cash_amnt > cash_amount) {
                        cash_amnt = cashcurrbal - cash_amount;
                        update_cash_value = Math.abs(cash_amnt);
                    }
                    if (cash_amnt < cash_amount) {
                        cash_amnt = cash_amount - cashcurrbal;
                        update_cash_value = Math.abs(cash_amnt);
                        update_cash_type = "CR";
                    }
                }

                cash_st.executeUpdate("Update ledger SET curr_bal= '" + update_cash_value + "',currbal_type='" + update_cash_type + "' where ledger_name='CASH'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (jRadioButton4.isSelected()) {
            try {
                double creditcurrbal = 0;
                String Curbaltypecradit = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement craditor_st = connect.createStatement();

                ResultSet cred_rs = craditor_st.executeQuery("Select * from ledger where ledger_name = '" + jTextField12.getText() + "'");

                while (cred_rs.next()) {
                    creditcurrbal = cred_rs.getDouble("curr_bal");
                    Curbaltypecradit = cred_rs.getString("currbal_type");
                }
                double creditamount = Double.parseDouble(jTextField16.getText());
                double acredit;

                if (Curbaltypecradit.equals("DR")) {
                    acredit = creditamount - creditcurrbal;
                    if (acredit <= 0) {
                        update_creditor_type = "DR";
                        update_creditor_value = Math.abs(acredit);
                    } else {
                        update_creditor_type = "CR";
                        update_creditor_value = Math.abs(acredit);
                    }

                } else {
                    if (Curbaltypecradit.equals("CR")) {
                        acredit = creditamount + creditcurrbal;
                        if (acredit >= 0) {
                            update_creditor_type = "CR";
                            update_creditor_value = Math.abs(acredit);
                        }
                    }
                }
                craditor_st.executeUpdate("Update ledger SET curr_bal= '" + update_creditor_value + "',currbal_type='" + update_creditor_type + "' where ledger_name='" + jTextField12.getText() + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--------------For Purchase account----------------------------------------------------------------------------------------------------------------
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement pur_st = connect.createStatement();
            int rowcount = jTable1.getRowCount();
            for (int i = 0; i < rowcount; i++) {
                purc_account.add(i, jTable1.getValueAt(i, 1) + "");
                purc_currbal.add(i, jTable1.getValueAt(i, 5) + "");

                ResultSet pur_rs = pur_st.executeQuery("Select * from ledger where ledger_name = '" + (purc_account.get(i) + "") + "'");
                while (pur_rs.next()) {
                    purc_type.add(i, pur_rs.getString("currbal_type"));
                    purc_amnt.add(i, pur_rs.getString("curr_bal"));
                }

                double purchase_amnt = Double.parseDouble(purc_amnt.get(i) + "");
                double purchase_currbal = Double.parseDouble(purc_currbal.get(i) + "");
                double pur_bal = 0;

                if (purc_type.get(i).equals("CR")) {
                    pur_bal = purchase_currbal - purchase_amnt;
                    if (pur_bal <= 0) {
                        update_purc_type.add(i, "CR");
                        update_purc_amnt.add(i, Math.abs(pur_bal));
                    } else {
                        update_purc_type.add(i, "DR");
                        update_purc_amnt.add(i, Math.abs(pur_bal));
                    }

                } else {
                    if (purc_type.get(i).equals("DR")) {
                        pur_bal = purchase_currbal + purchase_amnt;
                        if (pur_bal >= 0) {
                            update_purc_type.add(i, "DR");
                            update_purc_amnt.add(i, Math.abs(pur_bal));
                        }
                    }
                }
                pur_st.executeUpdate("Update ledger SET curr_bal= '" + update_purc_amnt.get(i) + "',currbal_type='" + update_purc_type.get(i) + "' where ledger_name='" + purc_account.get(i) + "'");
            }
            update_purc_amnt.clear();
            update_purc_type.clear();

        } catch (Exception e) {
            e.printStackTrace();
        }
//------------------------------------------------------------------------------------------------------------------------------
//---------------------------------Freight With Gst--------------------------------------------------------------------------
        if (freight_gst != 0 && Double.parseDouble(jTextField40.getText()) != 0) {
            try {
                double freightcurrbal = 0;
                String Curbaltypefreight = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement freight_st = connect.createStatement();
                ResultSet freight_rs = freight_st.executeQuery("Select * from ledger where ledger_name = 'Freight Charges'");

                while (freight_rs.next()) {
                    freightcurrbal = freight_rs.getDouble("curr_bal");
                    Curbaltypefreight = freight_rs.getString("currbal_type");
                }
                double freightamount = Double.parseDouble(jTextField40.getText());
                double af;

                if (Curbaltypefreight.equals("CR")) {
                    af = freightamount - freightcurrbal;
                    if (af < 0) {
                        update_freight_type = "CR";
                        update_freight_value = Math.abs(af);

                    } else {
                        update_freight_type = "DR";
                        update_freight_value = Math.abs(af);
                    }

                } else {
                    if (Curbaltypefreight.equals("DR")) {
                        af = freightamount + freightcurrbal;
                        if (af >= 0) {
                            update_freight_type = "DR";
                            update_freight_value = Math.abs(af);
                        }
                    }
                }

                freight_st.executeUpdate("Update ledger SET curr_bal= '" + update_freight_value + "',currbal_type='" + update_freight_type + "' where ledger_name='Freight Charges'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
    }

    boolean issave = false;

    public void save() {
        issave = true;
        updatebalances();
        s = 0;
        try {

            Statement st = null;
            Statement st1 = null;
            ResultSet rs = null;
            PreparedStatement pstm = null;
            int check = 0;
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            String partypurchasedate = "";
            if (jDateChooser1.getDate() == null) {

            } else {
                partypurchasedate = formatter.format(jDateChooser1.getDate());
            }

            int rcount = jTable1.getRowCount();
            String to_ledger = "";
            if (purch.equals("Cash")) {
                to_ledger = "CASH";
            } else if (purch.equals("Credit")) {
                to_ledger = jTextField12.getText();
            }
//            ArrayList igst_per = new ArrayList();
//            ArrayList sgst_per = new ArrayList();
//            ArrayList cgst_per = new ArrayList();
            for (int j = 0; j < rcount; j++) {
                check = 1;
                DecimalFormat df_stock = new DecimalFormat("0.00");
                if (isIgstApplicable == true) {
                    String igst_per = "";
                    Statement acc_st = connect.createStatement();
                    Statement acc_st_pur = connect.createStatement();
                    ResultSet acc_pur_rs = acc_st_pur.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(j, 1) + "") + "'");
                    while (acc_pur_rs.next()) {
                        String percentage = acc_pur_rs.getString("percent");
                        double per = Double.parseDouble(percentage);
                        DecimalFormat df = new DecimalFormat("0.#");
                        ResultSet acc_rs = acc_st.executeQuery("Select * from ledger where Type = 'IGST' and percent = '" + df.format(per) + "'");
                        while (acc_rs.next()) {
                            igst_per = acc_rs.getString("ledger_name");
//                         igst_per.add(j,acc_rs.getString("ledger_name"));
                        }
                        stmt.executeUpdate("insert into stock1 values ('" + jTextField22.getText() + "','" + jTextField12.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField2.getText() + "','" + partypurchasedate + "','" + purch + "','" + jTextField4.getText() + "','" + jTable1.getValueAt(j, 0) + "','" + jTable1.getValueAt(j, 3) + "','" + jTable1.getValueAt(j, 4) + "','" + jTable1.getValueAt(j, 5) + "','" + jTable1.getValueAt(j, 7) + "','" + jTextField13.getText() + "','" + jTextField23.getText() + "','" + jTextField15.getText() + "','" + other_charges + "','" + jTextField14.getText() + "','" + igst_per + "','" + df_stock.format(i_value.get(j)) + "','SGST 0%','0','CGST 0%','0','" + jTextField16.getText() + "','" + jTable1.getValueAt(j, 6) + "','" + jTable1.getValueAt(j, 8) + "','" + jTable1.getValueAt(j, 2) + "','" + jTable1.getValueAt(j, 9) + "','" + jTable1.getValueAt(j, 10) + "','" + to_ledger + "','" + jTable1.getValueAt(j, 1) + "','" + jTable1.getValueAt(j, 11) + "','0','" + jTextField1.getText() + "','" + jTextField27.getText() + "','" + jTextField24.getText() + "','" + jTextField26.getText() + "','" + jTextField17.getText() + "','" + jTextField25.getText() + "','" + freight_gst + "','" + jTextField40.getText() + "')");
                    }
                } else if (isIgstApplicable == false) {

                    String sgst_per = "";
                    String cgst_per = "";
                    Statement acc_st = connect.createStatement();
                    Statement acc_st_pur = connect.createStatement();
                    ResultSet acc_pur_rs = acc_st_pur.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(j, 1) + "") + "'");
                    while (acc_pur_rs.next()) {
                        String percentage = acc_pur_rs.getString("percent");
                        double per = Double.parseDouble(percentage) / 2;
                        DecimalFormat df = new DecimalFormat("0.#");
                        ResultSet acc_rs = acc_st.executeQuery("Select * from ledger where Type = 'SGST' and percent = '" + df.format(per) + "'");
                        while (acc_rs.next()) {
                            sgst_per = acc_rs.getString("ledger_name");
//                            sgst_per.add(j,acc_rs.getString("ledger_name"));
                        }
                        Statement acc_st1 = connect.createStatement();
                        ResultSet acc_rs1 = acc_st1.executeQuery("Select * from ledger where Type = 'CGST' and percent = '" + df.format(per) + "'");
                        while (acc_rs1.next()) {
                            cgst_per = acc_rs1.getString("ledger_name");
//                            cgst_per.add(j,acc_rs1.getString("ledger_name"));
                        }
                        System.out.println("SGST Percentage >>>>>>" + sgst_per);
                        System.out.println("CGST Percentage >>>>>>" + cgst_per);
                        stmt.executeUpdate("insert into stock1 values ('" + jTextField22.getText() + "','" + jTextField12.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField2.getText() + "','" + partypurchasedate + "','" + purch + "','" + jTextField4.getText() + "','" + jTable1.getValueAt(j, 0) + "','" + jTable1.getValueAt(j, 3) + "','" + jTable1.getValueAt(j, 4) + "','" + jTable1.getValueAt(j, 5) + "','" + jTable1.getValueAt(j, 7) + "','" + jTextField13.getText() + "','" + jTextField23.getText() + "','" + jTextField15.getText() + "','" + other_charges + "','" + jTextField14.getText() + "','IGST 0%','0','" + sgst_per + "','" + df_stock.format(s_value.get(j)) + "','" + cgst_per + "','" + df_stock.format(c_value.get(j)) + "','" + jTextField16.getText() + "','" + jTable1.getValueAt(j, 6) + "','" + jTable1.getValueAt(j, 8) + "','" + jTable1.getValueAt(j, 2) + "','" + jTable1.getValueAt(j, 9) + "','" + jTable1.getValueAt(j, 10) + "','" + to_ledger + "','" + jTable1.getValueAt(j, 1) + "','" + jTable1.getValueAt(j, 11) + "','0','" + jTextField1.getText() + "','" + jTextField27.getText() + "','" + jTextField24.getText() + "','" + jTextField26.getText() + "','" + jTextField17.getText() + "','" + jTextField25.getText() + "','" + freight_gst + "','" + jTextField40.getText() + "')");
                    }
                }
            }

            if (SerialNoDataVectorMap != null && SerialNoDataVectorMap.size() > 0) {
                String aSNo = "";
                Vector aDataVector = null;
                Iterator aSerialNoIterator = SerialNoDataVectorMap.keySet().iterator();
                while (aSerialNoIterator.hasNext()) {
                    aSNo = (String) aSerialNoIterator.next();
                    aDataVector = (Vector) SerialNoDataVectorMap.get(aSNo);
                    if (aDataVector != null) {

                        int rowcount = aDataVector.size();
                        Vector aRowVector = null;

                        for (int rowIndex = 0; rowIndex < rowcount; rowIndex++) {
                            aRowVector = (Vector) aDataVector.get(rowIndex);
                            String stooock = (aRowVector.get(1)) + "";
                            String purrate = (String) aRowVector.get(2);
                            //  Prate.add(i, purrate);
                            String retail1 = (String) aRowVector.get(3);
                            //Retail.add(i, retail1);
                            String Rdesc = (String) aRowVector.get(4);
                            //rdesc.add(i, Rdesc);
                            String biforgation = (String) aRowVector.get(5);
                            //byforgation.add(i, biforgation);
                            String Product1 = (String) aRowVector.get(6);
                            String article = (String) aRowVector.get(7);
                            int ismeter = Integer.parseInt(aRowVector.get(8) + "");
                            double qnty = Double.parseDouble(aRowVector.get(9) + "");
                            String nooftag = (String) (aRowVector.get(10) + "");
                            String Desc = (String) (aRowVector.get(11) + "");
                            //product.add(i, Product1);

                            try {
                                stmt.executeUpdate("insert into Stockid values('" + jTextField2.getText() + "','" + stooock + "','" + purrate + "','" + retail1 + "','" + Rdesc + "','" + Desc + "','" + biforgation + "','" + Product1 + "','" + jTextField22.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + article + "','" + aSNo + "','0'," + ismeter + "," + qnty + ",'" + nooftag + "')");
                                stmt.executeUpdate("insert into Stockid2 values('" + stooock + "','" + purrate + "','" + retail1 + "','" + Rdesc + "','" + Desc + "','" + biforgation + "','" + Product1 + "','" + article + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField22.getText() + "','0'," + ismeter + "," + qnty + ")");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            if (check == 1) {
                JOptionPane.showMessageDialog(null, "Inserted Sucessfully.");
            } else {
                JOptionPane.showMessageDialog(null, "DATA IS NOT SAVED.");
            }

            try {

                String against = "insert into againstpayment values('" + jTextField2.getText() + "','" + jTextField4.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField12.getText() + "','" + Purchaseaccount + "','" + jTextField16.getText() + "','purchase','')";
                stmt.executeUpdate(against);

            } catch (Exception e) {
                e.printStackTrace();
            }

            f = 0;
            s = 1;
            stoke = null;

            first = 0;
            second = 0;
            famount = 0;
            mount = 0;
            fvat = 0;
            ffvat = 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //for data insertion 
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i == 0 && issave == false) {
            save();
            if (Barcode.equals("Enable")) {
            printbarcode pb = new printbarcode();
            pb.jTextField4.setText(""+jTextField2.getText()); 
            pb.jTextField4.setEnabled(true);
            pb.jRadioButton3.setSelected(true);
            pb.setVisible(true);
             }
            this.dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed

    }//GEN-LAST:event_jButton1KeyPressed

    private void jTextField16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField16KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField14.requestFocus();
        }

    }//GEN-LAST:event_jTextField16KeyPressed

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        System.exit(0);
    }//GEN-LAST:event_jButton2KeyPressed
    int stok[] = new int[10000];
    static int counter = 0;
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
//        s = 0;
//        {
//            try {
//
//                connection c = new connection();
//                Connection connect = c.cone();
//
//                Statement ts = connect.createStatement();
//                ResultSet rs = ts.executeQuery("select * from Stockid where invoice_number='" + jTextField2.getText() + "'");
//
//                while (rs.next()) {
//                    stok[counter] = rs.getInt(2);
//                    ++counter;
//                }
//                for (int i = 0; i < counter; i++) {
//                    Statement stt = connect.createStatement();
//                    stt.executeUpdate("DELETE * from Stockid2 where Stock_No=" + stok[i] + "");
//                }
//                Statement st = connect.createStatement();
//                st.executeUpdate("DELETE * from Stockid where invoice_number='" + jTextField2.getText() + "'");
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            dispose();
//        }
        System.exit(0);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        insert();
    }//GEN-LAST:event_jButton3KeyPressed

    private void jTextField14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField14KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField16.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField26.requestFocus();
        }
    }//GEN-LAST:event_jTextField14KeyPressed

    private void jTextField14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField14ActionPerformed

    private void jTextField13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField13KeyPressed
        // TODO add your handling code here:

        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
//            jComboBox3.requestFocus();
        }
    }//GEN-LAST:event_jTextField13KeyPressed

    private void jTextField11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField11KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            amount();
            reCalculate();
            jTextField23.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }
    }//GEN-LAST:event_jTextField11KeyPressed

    private void jTextField15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField15KeyPressed

        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField17.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField15KeyPressed

    private void jTextField17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField17KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
//            cstbefore = Double.parseDouble(jTextField17.getText());
//
//            if (Double.parseDouble(jTextField17.getText()) != 0) {
//                jDialog5.setVisible(true);
//                list3.removeAll();
//                list3.addItem("ADD NEW");
//                try {
//                    connection c = new connection();
//                    Connection connect = c.cone();
//
//                    Statement st = connect.createStatement();
//                    Statement st1 = connect.createStatement();
//                    String duties = null;
//                    String Duteis = "DUTIES AND TAXES";
//                    //    String typeoftax = "OTHER";
//                    ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
//                    while (rs1.next()) {
//                        duties = rs1.getString(1);
//                    }
//                    ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = 'CGST' ");
//                    while (rs.next()) {
//                        list3.add(rs.getString(1));
//                    }
//                } catch (SQLException sqe) {
//                    System.out.println("SQl error");
//                    sqe.printStackTrace();
//                }
//                hide();
//            }
            calculationall();
            jTextField25.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField15.requestFocus();
        }
    }//GEN-LAST:event_jTextField17KeyPressed

    private void jComboBox4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox4KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox1.requestFocus();

        }
        if (key == evt.VK_ESCAPE) {
            jTextField10.requestFocus();
        }
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_F2) {
            master.color.byforgation byf = new master.color.byforgation();
            byf.setVisible(true);

        }

    }//GEN-LAST:event_jComboBox4KeyPressed

    private void jTextField19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField19ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField19ActionPerformed

    private void jTextField18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField18ActionPerformed

    private void jTextField19KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField19KeyPressed

    }//GEN-LAST:event_jTextField19KeyPressed

    private void jTextField18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField18KeyPressed
        int key = evt.getKeyCode();
        if (key == evt.VK_DOWN) {
            jTable3.requestFocus();
        }
    }//GEN-LAST:event_jTextField18KeyPressed

    private void jTextField20KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField20KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField20KeyPressed

    private void jTextField21KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField21KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField21KeyPressed

    private void jComboBox4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox4ItemStateChanged

    }//GEN-LAST:event_jComboBox4ItemStateChanged

    private void jTextField16FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField16FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField16FocusLost

    private void jTextField5FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField5FocusGained

    }//GEN-LAST:event_jTextField5FocusGained

    private void jTextField22FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField22FocusLost
        // TODO add your handling code here: ResultSet rs;
    }//GEN-LAST:event_jTextField22FocusLost
    public int markup = 0;

    public void partyselect() {
        String GST = "True";
        DefaultTableModel dtm = null;
        dtm = (DefaultTableModel) jTable3.getModel();
        row = jTable3.getSelectedRow();
        int coloum = jTable3.getSelectedColumn();
        jTextField22.setText("" + dtm.getValueAt(row, 0));
        jTextField12.setText("" + dtm.getValueAt(row, 1));
        Purchaseaccount = "" + dtm.getValueAt(row, 7);
        jLabel48.setText("" + dtm.getValueAt(row, 7));
        if ("".equals("" + dtm.getValueAt(row, 6)) || "" + dtm.getValueAt(row, 6) == null || "null".equals("" + dtm.getValueAt(row, 6))) {
            markup = 0;
        } else {
            markup = Integer.parseInt("" + dtm.getValueAt(row, 6));
        }

        jDialog2.setVisible(false);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + (String) jTable3.getValueAt(row, 1) + "'");
            while (rs.next()) {
                jLabel29.setText(rs.getString(10));
                jLabel30.setText(rs.getString(11));
            }

            ResultSet rs1 = st1.executeQuery("Select * from ledger where ledger_name='" + Purchaseaccount + "' ");
            while (rs1.next()) {

                jLabel31.setText("" + rs1.getString(10));
                jLabel32.setText("" + rs1.getString(11));

            }

            if (GST.equalsIgnoreCase((String) jTable3.getValueAt(row, 8))) {
                isIgstApplicable = true;
                jTextField17.setEnabled(false);
                jTextField25.setEnabled(false);
                jTextField24.setEnabled(false);
                jTextField26.setEnabled(false);

            } else if (!GST.equalsIgnoreCase((String) jTable3.getValueAt(row, 8))) {
                isIgstApplicable = false;
                jTextField1.setEnabled(false);
                jTextField27.setEnabled(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!(jTextField22.getText().isEmpty())) {
            jRadioButton3.setEnabled(true);
            jRadioButton4.setEnabled(true);
            jRadioButton4.setSelected(true);
            if (Purchaseaccount == null) {
                JOptionPane.showMessageDialog(rootPane, "please select Purchase account");
            } else {
                jDateChooser2.setEnabled(true);
                jTextField2.setEnabled(true);
                jTextField10.setEnabled(true);
                jTextField3.setEnabled(true);
                jTextField4.setEnabled(true);
                jTextField5.setEnabled(true);
                jTextField6.setEnabled(true);
                jTextField7.setEnabled(true);
                jTextField8.setEnabled(true);
                jTextField9.setEnabled(true);
                jTextField11.setEnabled(true);
                jTextField12.setEnabled(true);
                jTextField13.setEnabled(true);
                jTextField14.setEnabled(true);
                jTextField15.setEnabled(true);
                jTextField16.setEnabled(true);
                //     jTextField17.setEnabled(true);
                jTextField23.setEnabled(true);
                jComboBox3.setEnabled(true);
                jComboBox4.setEnabled(true);
                jComboBox1.setEnabled(true);
            }

        }
    }


    private void jTextField22KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField22KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDateChooser2.requestFocus();
        }

    }//GEN-LAST:event_jTextField22KeyPressed
    static int row = 0;
    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        partyselect();
        if (isUpdate == true) {
            party_update();
        }
        jDateChooser1.requestFocus();
    }//GEN-LAST:event_jTable3MouseClicked

    private void jTextField3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField3FocusLost
        // TODO add your handling code here:

        String rate = jTextField3.getText();

        if (!rate.matches("[0-9]+$") || jTextField3.getText().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Number only");
            jTextField3.requestFocus();
        } else {
            double qun_tity = Double.parseDouble(jTextField6.getText());
            double p = Double.parseDouble(jTextField3.getText());
            double q = qun_tity;
            double amou_nt = (p * q);
            DecimalFormat df = new DecimalFormat("0.00");
            String amount = df.format(amou_nt);
            jTextField7.setText(amount);
            double discountpercent = Double.parseDouble(jTextField32.getText());
            double purchaseamnt = Double.parseDouble(jTextField3.getText());
            double purchaseamntafterdiscount = purchaseamnt - ((discountpercent / 100) * purchaseamnt);
            double taxpercent = 0;
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("select percent from ledger where ledger_name='" + jComboBox5.getSelectedItem() + "" + "'");
                if (rs.next()) {
                    taxpercent = rs.getDouble(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            double taxamnt = (taxpercent / 100) * purchaseamntafterdiscount;

            System.out.println("purchase amont after discount is" + purchaseamntafterdiscount);
            System.out.println("tax amount is" + taxamnt);
            double purchasenetamnt = taxamnt + purchaseamntafterdiscount;
            System.out.println("purchase net amnt is" + purchasenetamnt);
            purchasenetamnt = Math.round(purchasenetamnt);
            jTextField8.setText(purchasenetamnt + "");
            //	String t = Integer.toString(amou_nt);
        }
        markupmethod();
    }//GEN-LAST:event_jTextField3FocusLost
    public void markupmethod() {
        double rate = Double.parseDouble(jTextField3.getText());
        double percentamount = ((double) markup / 100) * rate;
        double intialretailamount = rate + percentamount;
        double finalretailamount = 10 * (Math.ceil(Math.abs(intialretailamount / 10)));
        jTextField9.setText("" + (int) finalretailamount);
        jTextField8.requestFocus();
    }


    private void jTextField15FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField15FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField15FocusGained
    double afterdisc = 0;
    private void jTextField11FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField11FocusLost
        // TODO add your handling code here:
//        afterdisc = Double.parseDouble(jTextField11.getText());
//        if (beforedisc != afterdisc) {
//            calculationall();
//        } else {
//
//        }
//
//        if (jTextField11.getText().equals("")) {
//            jTextField23.setEnabled(true);
//
//        }
//        double amount = Double.parseDouble(jTextField13.getText());
//        Double discount = new Double(jTextField11.getText());
//        // float discount=Float.parseFloat(jTextField11.getText());
//        System.out.println(discount);
//        double discountafter = ((amount * discount) / 100);
//        double result = amount - discountafter;
//        DecimalFormat df=new DecimalFormat("0.00");
//        String result1 = df.format(result);
//        String resultconverttostring = Double.toString(discountafter);
//        jTextField23.setText("" + resultconverttostring);
//        String resultconverttostring1 = result1;
//        jTextField23.setText("" + resultconverttostring);
//        jTextField15.setText("" + resultconverttostring1);
//        jTextField16.setText("" + resultconverttostring1);
//     

    }//GEN-LAST:event_jTextField11FocusLost

    private void jTextField23FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField23FocusLost
        // TODO add your handling code here:
//       double amount = Double.parseDouble(jTextField13.getText());
//       double discount =Double.parseDouble(jTextField23.getText());
//       double result = amount-discount;
//       DecimalFormat df=new DecimalFormat("0.00");
//        String result1 = df.format(result);
//        String resultconverttostring = result1;
//        jTextField15.setText("" + resultconverttostring);
//       jTextField16.setText("" + resultconverttostring);
//        rigidcalculation();
    }//GEN-LAST:event_jTextField23FocusLost

    private void jTextField16FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField16FocusGained
        // TODO add your handling code here:
//          double dam = Double.parseDouble(jTextField15.getText());
//            double tax = Double.parseDouble(jTextField17.getText());
//            double oth = Double.parseDouble(jTextField14.getText());
//            double tot = ((dam * tax) / 100) + (dam + oth);
//            double Vat = Double.parseDouble(jTextField24.getText());
//            double vatamt= (Vat*dam)/100;
//            jTextField24.setText(""+vatamt);
//            tot=tot+vatamt;
//            
        //           jTextField16.setText("" + tot);
    }//GEN-LAST:event_jTextField16FocusGained

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            partyselect();
        }

        if (key == evt.VK_BACK_SPACE) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_LEFT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_RIGHT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_SPACE) {
            jTextField18.requestFocus();
        }

    }//GEN-LAST:event_jTable3KeyPressed

    private void jTextField18CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField18CaretUpdate
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String partyName = jTextField18.getText();
        if (partyName.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party where party_name LIKE'%" + partyName + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }
                    Object o[] = {rs.getString("party_Code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), rs.getString("Mark_up"), rs.getString("whatsapp_no"), igst};
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField18CaretUpdate

    private void jTextField19CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField19CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();

        String party = jTextField19.getText();
        if (party.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party where party_code  LIKE '%" + party + "%'");

                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_Code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), rs.getString("Mark_up"), rs.getString("whatsapp_no"), igst};

                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);
                sqe.printStackTrace();

            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField19CaretUpdate

    private void jTextField20CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField20CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String city = jTextField20.getText();
        if (city.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party where city LIKE '%" + city + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString("party_Code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), rs.getString("Mark_up"), rs.getString("whatsapp_no"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);
                sqe.printStackTrace();

            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField20CaretUpdate

    private void jTextField21CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField21CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String state = jTextField21.getText();
        if (state.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM party where state LIKE '%" + state + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }
                    Object o[] = {rs.getString("party_Code"), rs.getString("party_name"), rs.getString("city"), rs.getString("state"), rs.getString("opening_bal"), rs.getString("bal_type"), rs.getString("Mark_up"), rs.getString("whatsapp_no"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }
            } catch (Exception sqe) {
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField21CaretUpdate

    private void jTextField23KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField23KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            reCalculate();
            jTextField15.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField23KeyPressed

    private void jTextField22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField22ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField22ActionPerformed

    private void jTextField24KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField24KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
//
//            if (Double.parseDouble(jTextField24.getText()) != 0) {
//                jDialog4.setVisible(true);
//                list1.removeAll();
//                list1.addItem("ADD NEW");
//                try {
//                    connection c = new connection();
//                    Connection connect = c.cone();
//
//                    Statement st = connect.createStatement();
//                    Statement st1 = connect.createStatement();
//                    String duties = null;
////                    String vatinput = "VAT INPUT";
////                    String vatoutput = "VAT OUTPUT";
//                    String Duteis = "DUTIES AND TAXES";
//                    ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
//                    while (rs1.next()) {
//                        duties = rs1.getString(1);
//                    }
//                    ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = 'SGST' ");
//                    while (rs.next()) {
//                        list1.add(rs.getString(1));
//                    }
//                } catch (SQLException sqe) {
//                    System.out.println("SQl error");
//                    sqe.printStackTrace();
//                }
//                hide();
//
//            } else {
            calculationall();
            jTextField26.requestFocus();
        }
        //       }
        if (key == evt.VK_ESCAPE) {
            jTextField25.requestFocus();
        }
    }//GEN-LAST:event_jTextField24KeyPressed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        master.customer.Add_Purchase_Party addnew = new master.customer.Add_Purchase_Party();
        addnew.setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jTextField25KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField25KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
//
//            if (Double.parseDouble(jTextField25.getText()) != 0 && Party_cst == null) {
//                jDialog5.setVisible(true);
//                //           if(Double.parseDouble(jTextField25.getText()) != 0)
//                {
//                    jDialog5.setVisible(true);
//                    list3.removeAll();
//                    list3.addItem("ADD NEW");
//                    try {
//                        connection c = new connection();
//                        Connection connect = c.cone();
//
//                        Statement st = connect.createStatement();
//                        Statement st1 = connect.createStatement();
//                        String duties = null;
//                        String Duteis = "DUTIES AND TAXES";
//                        String typeoftax = "OTHER";
//                        ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
//                        while (rs1.next()) {
//                            duties = rs1.getString(1);
//                        }
//                        ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = CGST ");
//                        while (rs.next()) {
//                            list3.add(rs.getString(1));
//                        }
//                    } catch (SQLException sqe) {
//                        System.out.println("SQl error");
//                        sqe.printStackTrace();
//                    }
//                    hide();
//                }
//                //jTextField24.requestFocus();
//            } else {
            calculationall();
            jTextField24.requestFocus();
            //           }
            //           rigidcalculation();

        }
        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
    }//GEN-LAST:event_jTextField25KeyPressed

    private void jTextField26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField26KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            calculationall();
            jTextField14.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField24.requestFocus();
        }
    }//GEN-LAST:event_jTextField26KeyPressed
    double vatafter = 0;
    private void jTextField24FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField24FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField24FocusLost
    double cstafter = 0;
    private void jTextField17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField17FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField17FocusLost
    public String Party_vat = null;
    private void list1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list1ActionPerformed
        // TODO add your handling code here:
        jDialog4.dispose();
        jTextField26.requestFocus();
        String currbal = null;
        String currbaltype = null;
        String vat_percent = null;
        show();
        if (list1.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger create = new reporting.accounts.createLedger();
            create.setVisible(true);
            create.jComboBox1.setSelectedItem("DUTIES AND TAXES");
        } else {
            Party_vat = list1.getSelectedItem();
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                System.out.println("vatacc." + Party_vat);
                ResultSet rs = st.executeQuery("SELECT * from ledger where ledger_name='" + Party_vat + "'");
                while (rs.next()) {
                    currbal = rs.getString(10);
                    System.out.println("" + currbal);
                    currbaltype = rs.getString(11);
                    vat_percent = rs.getString(6);
                }
                jLabel38.setText("" + currbal);
                jLabel39.setText("" + currbaltype);
                jTextField24.setText("" + vat_percent);
            } catch (SQLException sqe) {
                System.out.println("SQl error");
                sqe.printStackTrace();
            }

            calculationall();
        }
    }//GEN-LAST:event_list1ActionPerformed
    public String createdgroup = null;
    public String Purchaseaccount = null;
    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        // TODO add your handling code here:
        list2.removeAll();
        list2.addItem("ADD NEW");
        jDialog6.setVisible(true);
        cashisselected = true;
        String c = "Cash";
        try {
            connection c1 = new connection();
            Connection connect = c1.cone();

            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT curr_bal,currbal_type from ledger where ledger_name='" + c + "'");
            while (rs.next()) {
                jLabel29.setText("" + rs.getString(1));
                jLabel30.setText("" + rs.getString(2));
            }
            String purchaseacc = "PURCHASE ACCOUNTS";
            ResultSet rs1 = st1.executeQuery("SELECT * from Group_create where under='" + purchaseacc + "'");
            while (rs1.next()) {
                createdgroup = rs1.getString(1);
            }
            ResultSet rs2 = st2.executeQuery("Select * from ledger where groups='" + purchaseacc + "'or groups='" + createdgroup + "'");
            while (rs2.next()) {
                list2.add(rs2.getString(1));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        if (isUpdate == true) {
            party_update();
        }
    }//GEN-LAST:event_jRadioButton3ActionPerformed
    String Purchase_party = null;
    private void list2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list2ActionPerformed
        // TODO add your handling code here:
        jDialog6.dispose();
        if (list2.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger cr = new reporting.accounts.createLedger();
            cr.setVisible(true);
            cr.jComboBox1.setSelectedItem("PURCHASE ACCOUNTS");
        } else {
            Purchaseaccount = list2.getSelectedItem();
            jLabel48.setText("" + Purchaseaccount);
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from ledger where ledger_name='" + Purchaseaccount + "' ");
                while (rs.next()) {
                    if (cashisselected) {
                        jLabel31.setText("" + rs.getString(10));
                        jLabel32.setText("" + rs.getString(11));

                    } else {
                        jLabel31.setText("" + rs.getString(10));
                        jLabel32.setText("" + rs.getString(11));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            jDateChooser1.requestFocus();
        }
    }//GEN-LAST:event_list2ActionPerformed
    boolean cashisselected = true;
    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        // TODO add your handling code here:
        jDialog6.setVisible(true);
        list2.removeAll();
        list2.addItem("ADD NEW");
        cashisselected = false;
        try {
            connection c = new connection();
            Connection connect = c.cone();

            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st3 = connect.createStatement();
            String purchaseacc = "PURCHASE ACCOUNTS";
            ResultSet rs1 = st1.executeQuery("SELECT * from Group_create where under='" + purchaseacc + "'");
            while (rs1.next()) {
                createdgroup = rs1.getString(1);
            }
            ResultSet rs2 = st2.executeQuery("Select * from ledger where groups='" + purchaseacc + "'or groups='" + createdgroup + "'");
            while (rs2.next()) {
                list2.add(rs2.getString(1));
            }
            ResultSet rs3 = st2.executeQuery("Select * from ledger where ledger_name='" + jTextField12.getText() + "'");
            while (rs3.next()) {
                jLabel29.setText(rs3.getString(10));
                jLabel30.setText(rs3.getString(11));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_F2) {
            jDialog10.setVisible(true);
            DecimalFormat df = new DecimalFormat("0");
            int Sel_row = jTable1.getSelectedRow();
            jComboBox6.setSelectedItem((String) jTable1.getValueAt(Sel_row, 1) + "");
            jTextField29.setText(jTable1.getValueAt(Sel_row, 3) + "");
            jTextField30.setText(jTable1.getValueAt(Sel_row, 4) + "");
            jTextField31.setText(jTable1.getValueAt(Sel_row, 5) + "");
            jTextField41.setText(jTextField40.getText() + "");
            if (isIgstApplicable == true) {
                jComboBox12.setSelectedItem(old_freight_gst);
            } else if (isIgstApplicable == false) {
                jComboBox12.setSelectedItem(df.format(Double.parseDouble(old_freight_gst) * 2));
            }
        } else if (key == evt.VK_F3) {
            jDialog11.setVisible(true);
            int Sel_row1 = jTable1.getSelectedRow();
            jComboBox7.setSelectedItem((String) jTable1.getValueAt(Sel_row1, 1) + "");
            jComboBox8.setSelectedItem((String) jTable1.getValueAt(Sel_row1, 2) + "");
            jTextField33.setText(jTable1.getValueAt(Sel_row1, 3) + "");
            jTextField34.setText(jTable1.getValueAt(Sel_row1, 4) + "");
            jTextField35.setText(jTable1.getValueAt(Sel_row1, 5) + "");
            jTextField36.setText(jTable1.getValueAt(Sel_row1, 6) + "");
            jTextField37.setText(jTable1.getValueAt(Sel_row1, 7) + "");
            jTextField38.setText(jTable1.getValueAt(Sel_row1, 8) + "");
            jComboBox9.setSelectedItem((String) jTable1.getValueAt(Sel_row1, 9) + "");
            jComboBox10.setSelectedItem((String) jTable1.getValueAt(Sel_row1, 10) + "");
            jComboBox7.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jDialog1.setVisible(false);
        }
        if (key == evt.VK_DELETE) {
        }
    }//GEN-LAST:event_jTable1KeyPressed
    double totalqnty = 0;
    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox2.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox3.requestFocus();

        }
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_F2) {
            master.article.addArticle addar = new master.article.addArticle();
            addar.setVisible(true);
        }
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jComboBox1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusLost
        // TODO add your handling code here:
        jButton3.requestFocus();
    }//GEN-LAST:event_jComboBox1FocusLost

    private void jComboBox4FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox4FocusGained
        // TODO add your handling code here:
        dynamicbyforgation();
        jComboBox4.setSelectedItem(null);
    }//GEN-LAST:event_jComboBox4FocusGained
    public void dynamicbyforgation() {
        jComboBox4.removeAllItems();

        try {
            connection c = new connection();
            Connection connect = c.cone();

            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("Select * from byforgation ");
            while (rs.next()) {

                jComboBox4.addItem(rs.getString(1));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //jComboBox4.setSelectedItem(null);
    }
    private void jComboBox1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1FocusGained

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ItemStateChanged
    int row1 = 0;
    String sno1 = null;
    public boolean isUpdate = false;
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {

            if (isUpdate = false) {
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                row1 = jTable1.getSelectedRow();
                sno1 = (String) jTable1.getValueAt(row1, 0);
                jDialog1.setVisible(true);
                jButton3.setVisible(false);
                //jButton7.setVisible(true);
                System.out.println("SerialNoDataVectorMap " + SerialNoDataVectorMap);
                Vector aDataVector = (Vector) SerialNoDataVectorMap.get(sno1);
                dtm = (DefaultTableModel) jTable2.getModel();
                dtm.getDataVector().removeAllElements();
                if (aDataVector != null) {
                    for (int i = 0; i < aDataVector.size(); i++) {
                        dtm.addRow((Vector) aDataVector.get(i));
                    }
                }
            } else if (isUpdate = true) {
                jDialog1.setVisible(true);
                jButton3.setVisible(false);
                jButton7.setVisible(false);
                DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
                dtm.getDataVector().removeAllElements();
                dtm.fireTableDataChanged();
                try {
                    int row_no = jTable1.getSelectedRow();
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    ResultSet rs = st.executeQuery("Select * from stockid where sno = '" + jTable1.getValueAt(row_no, 0) + "" + "' and invoice_number='" + jTextField2.getText() + "'");
                    while (rs.next()) {
                        String byforgation = rs.getString("Byforgation");
                        if (!byforgation.equals(null)) {
                            byforgation = "";
                        }
                        String articleno = rs.getString("articleno");
                        if (!articleno.equals(null)) {
                            articleno = "";
                        }

                        Object o[] = {rs.getString("sno"), rs.getString("Stock_No"), rs.getString("Ra_te"), rs.getString("Re_tail"), rs.getString("De_sc"), byforgation, rs.getString("Pro_duct"), articleno, rs.getString("ismeter"), rs.getString("qnt"), rs.getString("nooftag"), rs.getString("P_Desc")};
                        dtm.addRow(o);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        double billamount = Double.parseDouble(jTextField13.getText());
        String sno = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 1);
        double ratebefore = Double.parseDouble((String) jTable1.getValueAt(jTable1.getSelectedRow(), 3));
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        Vector aDataVector = new Vector(dtm.getDataVector());
        SerialNoDataVectorMap.put(sno, aDataVector);
        jDialog1.setVisible(false);
        int i = 0;
        int rowcount = jTable2.getRowCount();
        double rate = 0;
        for (i = 0; i < rowcount; i++) {
            double rate1 = Double.parseDouble((String) jTable2.getValueAt(i, 1));
            rate = rate1 + rate;
        }
        jTable1.setValueAt(rate, jTable1.getSelectedRow(), 3);
        double diff = 0;
        double billamount1 = 0;
        if (rate < ratebefore) {
            diff = ratebefore - rate;
            System.out.println("diffsub" + diff);
            billamount1 = billamount - diff;
            System.out.println("billamount1sub" + billamount1);
        }
        if (rate == ratebefore) {
            billamount1 = billamount;
        }
        if (rate > ratebefore) {
            diff = rate - ratebefore;
            System.out.println("diffadd" + diff);
            billamount1 = billamount + diff;
            System.out.println("billamount1add" + billamount1);
        }
        jTextField13.setText("" + billamount1);
        calculationall();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jComboBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox4ActionPerformed

    }//GEN-LAST:event_jComboBox4ActionPerformed

    private void jComboBox4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBox4MouseClicked

    }//GEN-LAST:event_jComboBox4MouseClicked

    private void jTextField6FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField6FocusLost
        // TODO add your handling code here:


    }//GEN-LAST:event_jTextField6FocusLost

    private void jTextField9FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField9FocusLost
        // TODO add your handling code here:
        String retail = jTextField9.getText();

        if (!retail.matches(("[0-9]+$")) || jTextField9.getText().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Number only");
            jTextField9.requestFocus();
        }
    }//GEN-LAST:event_jTextField9FocusLost
    ArrayList i_value = new ArrayList();
    ArrayList s_value = new ArrayList();
    ArrayList c_value = new ArrayList();
    ArrayList percent = new ArrayList();
    private void jComboBox2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox2KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (!jTextField6.getText().isEmpty() && !jTextField3.getText().isEmpty() && !jTextField7.getText().isEmpty() && !jTextField9.getText().isEmpty()) {

                jButton7.setVisible(false);
                jButton3.setVisible(true);
                sn = jTextField5.getText();

                qnt = jTextField6.getText();
                rate = String.valueOf(jTextField3.getText());
                amount = jTextField7.getText();
                pds = jTextField8.getText();
                rt = jTextField9.getText();
                ds = jTextField10.getText();
                pd = (String) jComboBox3.getSelectedItem();
                fr = (String) jComboBox4.getSelectedItem();
                articleno = (String) jComboBox1.getSelectedItem();
                samebarcode = (String) jComboBox2.getSelectedItem();
                sup = jTextField22.getText();
                supname = jTextField12.getText();
                System.out.println("invoice date is " + formatter.format(jDateChooser2.getDate()));
                invod = formatter.format(jDateChooser2.getDate());
                invon = jTextField2.getText();
                if (jRadioButton3.isSelected()) {
                    purch = "Cash";
                }
                if (jRadioButton4.isSelected()) {
                    purch = "Credit";
                }
                ref = jTextField4.getText();

                bil = jTextField13.getText();
                otherc = jTextField14.getText();
                va = jTextField15.getText();

                namoun = jTextField16.getText();

                supid[incr] = sup;
                supnameid[incr] = supname;
                invoiced[incr] = invod;
                invoiceno[incr] = (invon);
                purchast[incr] = purch;
                refrence[incr] = ref;

                sno[incr] = sn;
                qty[incr] = qnt;
                rat[incr] = rate;
                amoun[incr] = amount;
                pdes[incr] = pds;
                retail[incr] = rt;
                desc[incr] = ds;
                prod[incr] = pd;
                ftr[incr] = fr;
                articlenum[incr] = articleno;
                samebarcode1[incr] = samebarcode;
                bill[incr] = bil;
                ocharge[incr] = otherc;
                amountafter[incr] = va;
                discount[incr] = jTextField11.getText();
                tax[incr] = jTextField17.getText();
                namount[incr] = namoun;

                mount = (mount + Double.parseDouble(amount));
                double tax = Double.parseDouble(jTextField17.getText());
                double taxamount = mount * tax / 100;
                Double amountaftertax = mount + taxamount;
                DecimalFormat df = new DecimalFormat("0.00");
                String mountr = df.format(mount);

                totalqnty = Double.parseDouble(qnt) + totalqnty;
                jTextField28.setText("" + totalqnty);

                String amountaftertax1 = df.format(amountaftertax);
                jTextField13.setText("" + mountr);
                jTextField15.setText("" + mountr);
                jTextField16.setText("" + amountaftertax1);

                try {
                    connection c = new connection();
                    Connection connect = c.cone();

                    Statement st = connect.createStatement();
                    Statement ts = connect.createStatement();
                    ResultSet sr = ts.executeQuery("select * from product where product_code='" + pd + "' ");
                    String a = null;

                } catch (Exception e) {
                    e.printStackTrace();
                }

// Calculating GST percantage value
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement igst_st = connect.createStatement();
                    Statement scgst_st = connect.createStatement();
                    if (isIgstApplicable == true) {
                        ResultSet igst_rs = igst_st.executeQuery("Select * from ledger where ledger_name = '" + jComboBox5.getSelectedItem() + "" + "'");
                        while (igst_rs.next()) {
                            igst_percent = igst_rs.getDouble("percent");
                            double value = Double.parseDouble(jTextField7.getText());
//                            amntbeforetax = (value / (100 + gst_percent)) * 100;
                            iv = (value * igst_percent) / 100;
                        }
                        i_value.add(iv);
                        percent.add(igst_percent);
                        if (igst_percent == 5) {

                            tiv = tiv + iv;
                            jTextField1.setText("" + df.format(tiv));
                        } else if (igst_percent == 12) {
                            tiv1 = tiv1 + iv;
                            jTextField27.setText("" + df.format(tiv1));
                        }
                    } else if (isIgstApplicable == false) {
                        ResultSet scgst_rs = scgst_st.executeQuery("Select * from ledger where ledger_name = '" + jComboBox5.getSelectedItem() + "" + "'");
                        while (scgst_rs.next()) {
                            scgst_percent = scgst_rs.getDouble("percent");
                            double value = Double.parseDouble(jTextField7.getText());
//                            amntbeforetax = (value / (100 + gst_percent)) * 100;
                            sv = (value * (scgst_percent) / 2) / 100;
                            cv = (value * (scgst_percent) / 2) / 100;
                        }
                        s_value.add(sv);
                        c_value.add(cv);
                        percent.add((scgst_percent / 2));
                        if (scgst_percent == 5) {
                            tsv = tsv + sv;
                            tcv = tcv + cv;
                            jTextField17.setText("" + df.format(tcv));
                            jTextField24.setText("" + df.format(tsv));
                        } else if (scgst_percent == 12) {
                            tsv1 = tsv1 + sv;
                            tcv1 = tcv1 + cv;
                            jTextField25.setText("" + df.format(tcv1));
                            jTextField26.setText("" + df.format(tsv1));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Object o[] = {sn, jComboBox5.getSelectedItem() + "", pd, qnt, rate, amount, pds, rt, ds, fr, articleno, samebarcode};
                DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                dtm.addRow(o);
                if (isquantity == true) {
                    stocknumbers();
                }
                calculationall();

                ++s;

                jTextField5.setText("" + s);

                jTextField6.setText(null);
                jTextField7.setText(null);
                jTextField8.setText(null);
                jTextField9.setText(null);
                jTextField10.setText(null);
                jTextField3.setText(null);
                jComboBox3.setSelectedItem(pd);
                jComboBox4.setSelectedItem(null);
                jComboBox1.setSelectedItem(articleno);
                ++incr;
                if (!jDialog1.isVisible()) {
                    jComboBox5.requestFocus();
                } else {
                    jButton3.requestFocus();
                }
            }
        }

    }//GEN-LAST:event_jComboBox2KeyPressed

    private void jTextField25FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField25FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField25FocusLost

    private void jTextField26FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField26FocusLost
        // TODO add your handling code here:
        //       rigidcalculation();
    }//GEN-LAST:event_jTextField26FocusLost

    private void jTextField14FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField14FocusLost
        // TODO add your handling code here:
//        rigidcalculation();
        if (jTextField14.getText().equals("0")) {

        } else {
            jDialog7.setVisible(true);
            list4.removeAll();

            list4.addItem("ADD NEW");
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                Statement st1 = connect.createStatement();
                String duties = null;
                String Duteis = "DIRECT EXPENSES";
                ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
                while (rs1.next()) {
                    duties = rs1.getString(1);
                }
                ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "'");
                while (rs.next()) {
                    list4.add(rs.getString(1));
                }
            } catch (SQLException sqe) {
                sqe.printStackTrace();
            }
        }

    }//GEN-LAST:event_jTextField14FocusLost
    double beforedisc = 0;
    private void jTextField11FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField11FocusGained
        // TODO add your handling code here:
//        beforedisc = Double.parseDouble(jTextField11.getText());

    }//GEN-LAST:event_jTextField11FocusGained
    double cstbefore = 0;
    private void jTextField17FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField17FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField17FocusGained
    double vatbefore = 0;
    private void jTextField24FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField24FocusGained
        // TODO add your handling code here:
        vatbefore = Double.parseDouble(jTextField24.getText());
    }//GEN-LAST:event_jTextField24FocusGained

    private void jTextField28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField28KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }

    }//GEN-LAST:event_jTextField28KeyPressed

    private void jComboBox2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox2FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox2FocusLost

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6ActionPerformed

    private void list3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list3ActionPerformed
        // TODO add your handling code here:

        jDialog5.dispose();
        show();
        if (list3.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger cr = new reporting.accounts.createLedger();
            cr.setVisible(true);
            cr.jComboBox1.setSelectedItem("DUTIES AND TAXES");
            cr.jComboBox5.setSelectedItem("OTHER");
        } else {

            Party_cst = list3.getSelectedItem();
            String cst_percent = null;
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from ledger where ledger_name='" + Party_cst + "' ");
                while (rs.next()) {
                    cst_percent = rs.getString(6);
                    if (cashisselected) {
                        jLabel43.setText("" + rs.getString(10));
                        jLabel44.setText("" + rs.getString(11));
                    } else {
                        jLabel43.setText("" + rs.getString(10));
                        jLabel44.setText("" + rs.getString(11));

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            jTextField17.setText("" + cst_percent);
        }
        jTextField25.requestFocus();
        calculationall();
    }//GEN-LAST:event_list3ActionPerformed
    public String other_charges = "";
    private void list4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list4ActionPerformed
        // TODO add your handling code here:
        jDialog7.dispose();
        String currbal = null;
        String currbaltype = null;
        if (list4.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger create = new reporting.accounts.createLedger();
            create.setVisible(true);
            create.jComboBox1.setSelectedItem("DIRECT EXPENSES");
        } else {
            other_charges = list4.getSelectedItem();
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT * from ledger where ledger_name='" + other_charges + "'");
                while (rs.next()) {
                    currbal = rs.getString(10);
                    System.out.println("" + currbal);
                    currbaltype = rs.getString(11);
                }
                jLabel45.setText("" + currbal);
                jLabel46.setText("" + currbaltype);
                calculationall();

            } catch (SQLException sqe) {
                sqe.printStackTrace();
            }
        }

    }//GEN-LAST:event_list4ActionPerformed

    private void jTextField26FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField26FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField26FocusGained

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        jDialog1.dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jTextField12.requestFocus();
    }//GEN-LAST:event_formWindowOpened
//    String oldsupplier = "";
//    String oldpurchase = "";
//    String partyvatold = "";
//    String sundrycreditorold = "";
//    String sundrycreditorcurrbalold = "";
//    String sundrycreditorcurrbaltypeold = "";
//    String vatold = "";
//    String vatcurrbalold = "";
//    String vatcurrbaltypeold = "";
//    String cstold = "";
//    String igstold = "";
//    String igstcurrbalold = "";
//    String igstcurrbaltypeold = "";
//    String cstcurrbalold = "";
//    String cstcurrbaltypeold = "";
//    String otherchargesold = "";
//    String otherchargescurrbalold = "";
//    String otherchargescurrbaltypeold = "";
//    String discountold = "";
//    String discountcurrbalold = "";
//    String discountcurrbaltypeold = "";
//    ArrayList purchaseaccountold = new ArrayList();
//    ArrayList purchaseaccountcurrbalold = new ArrayList();
//    ArrayList purchaseaccountcurrbaltypeold = new ArrayList();
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public String sundrycreditorold = "";
    double sundrycreditorcurrbalold = 0;
    String sundrycreditorcurrbaltypeold = "";
    String rDiscountaccount = null;
    String rupdateddisccurrbal = null;
    String rupdateddisccurrbaltyoe = null;
    double rigst_value = 0;
    String rigst_type = "";
    double rsgst_value = 0;
    String rsgst_type = "";
    double rcgst_value = 0;
    String rcgst_type = "";
    double rigst_value1 = 0;
    String rigst_type1 = "";
    double rsgst_value1 = 0;
    String rsgst_type1 = "";
    double rcgst_value1 = 0;
    String rcgst_type1 = "";
    double rupdate_igst_value = 0;
    String rupdate_igst_type = "";
    double rupdate_sgst_value = 0;
    String rupdate_sgst_type = "";
    double rupdate_cgst_value = 0;
    String rupdate_cgst_type = "";
    double rupdate_igst_value1 = 0;
    String rupdate_igst_type1 = "";
    double rupdate_sgst_value1 = 0;
    String rupdate_sgst_type1 = "";
    double rupdate_cgst_value1 = 0;
    String rupdate_cgst_type1 = "";
    double rupdate_other_value = 0;
    String rupdate_other_type = "";
    double rupdate_freight_value = 0;
    String rupdate_freight_type = "";
    double rupdate_cash_value = 0;
    String rupdate_cash_type = "";
    public ArrayList rpurc_account = new ArrayList();
    ArrayList rpurc_currbal = new ArrayList();
    ArrayList rpurc_amnt = new ArrayList();
    ArrayList rpurc_type = new ArrayList();
    ArrayList rupdate_purc_amnt = new ArrayList();
    ArrayList rupdate_purc_type = new ArrayList();
//----------------------------------------------------------------------------------------------------------------------------------------------------

    public String old_igst_5 = "";
    public String old_igst_12 = "";
    public String old_sgst_25 = "";
    public String old_sgst_6 = "";
    public String old_cgst_25 = "";
    public String old_cgst_6 = "";
    public String old_freight_gst = "";

    public void updatereversebalances() {
//---------------For Igst-----------------------------------------------------------------------------------
        if (!old_igst_5.equals("")) {
            try {
                double rigst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rigst_st = connect.createStatement();
                ResultSet rigst_rs = rigst_st.executeQuery("Select * from ledger where ledger_name = 'IGST 5%'");
                while (rigst_rs.next()) {
                    rigst_value = rigst_rs.getDouble("curr_bal");
                    rigst_type = rigst_rs.getString("currbal_type");
                }

                ResultSet rigst_rs1 = rigst_st.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rigst_rs1.next()) {
                    rigst_amnt = rigst_rs1.getDouble("total_igst_amnt_5");
                }

//                double rigst_amnt = Double.parseDouble(jTextField1.getText());
                double riamnt;

                if (rigst_type.equals("DR")) {
                    riamnt = rigst_amnt - rigst_value;
                    if (riamnt <= 0) {
                        rupdate_igst_type = "DR";
                        rupdate_igst_value = Math.abs(riamnt);
                    } else {
                        rupdate_igst_type = "CR";
                        rupdate_igst_value = Math.abs(riamnt);
                    }
                } else {
                    if (rigst_type.equals("CR")) {
                        riamnt = rigst_amnt + rigst_value;
                        if (riamnt >= 0) {
                            rupdate_igst_type = "CR";
                            rupdate_igst_value = Math.abs(riamnt);
                        }
                    }
                }
                rigst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_igst_value + "',currbal_type='" + rupdate_igst_type + "' where ledger_name = 'IGST 5%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!old_igst_12.equals("")) {
            try {
                double rigst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rigst_st1 = connect.createStatement();
                ResultSet rigst_rs1 = rigst_st1.executeQuery("Select * from ledger where ledger_name = 'IGST 12%'");
                while (rigst_rs1.next()) {
                    rigst_value1 = rigst_rs1.getDouble("curr_bal");
                    rigst_type1 = rigst_rs1.getString("currbal_type");
                }

                ResultSet rigst_rs = rigst_st1.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rigst_rs.next()) {
                    rigst_amnt = rigst_rs.getDouble("total_igst_amnt_12");
                }
//                double rigst_amnt = Double.parseDouble(jTextField27.getText());
                double riamnt;

                if (rigst_type1.equals("DR")) {
                    riamnt = rigst_amnt - rigst_value1;
                    if (riamnt <= 0) {
                        rupdate_igst_type1 = "DR";
                        rupdate_igst_value1 = Math.abs(riamnt);
                    } else {
                        rupdate_igst_type1 = "CR";
                        rupdate_igst_value1 = Math.abs(riamnt);
                    }
                } else {
                    if (rigst_type1.equals("CR")) {
                        riamnt = rigst_amnt + rigst_value1;
                        if (riamnt >= 0) {
                            rupdate_igst_type1 = "CR";
                            rupdate_igst_value1 = Math.abs(riamnt);
                        }
                    }
                }
                rigst_st1.executeUpdate("Update ledger SET curr_bal= '" + rupdate_igst_value1 + "',currbal_type='" + rupdate_igst_type1 + "' where ledger_name = 'IGST 12%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   

//---------------For Sgst-----------------------------------------------------------------------------------------------
        if (!old_sgst_25.equals("")) {
            try {
                double rsgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rsgst_st = connect.createStatement();
                ResultSet rsgst_rs = rsgst_st.executeQuery("Select * from ledger where ledger_name = 'SGST 2.5%'");
                while (rsgst_rs.next()) {
                    rsgst_value = rsgst_rs.getDouble("curr_bal");
                    rsgst_type = rsgst_rs.getString("currbal_type");
                }

//                double rsgst_amnt = Double.parseDouble(jTextField24.getText());
                ResultSet rsgst_rs1 = rsgst_st.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rsgst_rs1.next()) {
                    rsgst_amnt = rsgst_rs1.getDouble("total_sgst_amnt_25");
                }
                double rsamnt;

                if (rsgst_type.equals("DR")) {
                    rsamnt = rsgst_amnt - rsgst_value;
                    if (rsamnt <= 0) {
                        rupdate_sgst_type = "DR";
                        rupdate_sgst_value = Math.abs(rsamnt);
                    } else {
                        rupdate_sgst_type = "CR";
                        rupdate_sgst_value = Math.abs(rsamnt);
                    }
                } else {
                    if (rsgst_type.equals("CR")) {
                        rsamnt = rsgst_amnt + rsgst_value;
                        if (rsamnt >= 0) {
                            rupdate_sgst_type = "CR";
                            rupdate_sgst_value = Math.abs(rsamnt);
                        }
                    }
                }
                rsgst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_sgst_value + "',currbal_type='" + rupdate_sgst_type + "' where ledger_name = 'SGST 2.5%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!old_sgst_6.equals("")) {
            try {
                double rsgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rsgst_st1 = connect.createStatement();
                ResultSet rsgst_rs1 = rsgst_st1.executeQuery("Select * from ledger where ledger_name = 'SGST 6%'");
                while (rsgst_rs1.next()) {
                    rsgst_value1 = rsgst_rs1.getDouble("curr_bal");
                    rsgst_type1 = rsgst_rs1.getString("currbal_type");
                }

//                double rsgst_amnt = Double.parseDouble(jTextField26.getText());
                ResultSet rsgst_rs = rsgst_st1.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rsgst_rs.next()) {
                    rsgst_amnt = rsgst_rs.getDouble("total_sgst_amnt_6");
                }

                double rsamnt;

                if (rsgst_type1.equals("DR")) {
                    rsamnt = rsgst_amnt - rsgst_value1;
                    if (rsamnt <= 0) {
                        rupdate_sgst_type1 = "DR";
                        rupdate_sgst_value1 = Math.abs(rsamnt);
                    } else {
                        rupdate_sgst_type1 = "CR";
                        rupdate_sgst_value1 = Math.abs(rsamnt);
                    }
                } else {
                    if (rsgst_type1.equals("CR")) {
                        rsamnt = rsgst_amnt + rsgst_value1;
                        if (rsamnt >= 0) {
                            rupdate_sgst_type1 = "CR";
                            rupdate_sgst_value1 = Math.abs(rsamnt);
                        }
                    }
                }
                rsgst_st1.executeUpdate("Update ledger SET curr_bal= '" + rupdate_sgst_value1 + "',currbal_type='" + rupdate_sgst_type1 + "' where ledger_name = 'SGST 6%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   
//---------------For Cgst-----------------------------------------------------------------------------------------------
        if (!old_cgst_25.equals("")) {
            try {
                double rcgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcgst_st = connect.createStatement();
                ResultSet rcgst_rs = rcgst_st.executeQuery("Select * from ledger where ledger_name = 'CGST 2.5%'");
                while (rcgst_rs.next()) {
                    rcgst_value = rcgst_rs.getDouble("curr_bal");
                    rcgst_type = rcgst_rs.getString("currbal_type");
                }

//                double rcgst_amnt = Double.parseDouble(jTextField17.getText());
                ResultSet rcgst_rs1 = rcgst_st.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rcgst_rs1.next()) {
                    rcgst_amnt = rcgst_rs1.getDouble("total_cgst_amnt_25");
                }

                double rcamnt;

                if (rcgst_type.equals("DR")) {
                    rcamnt = rcgst_amnt - rcgst_value;
                    if (rcamnt <= 0) {
                        rupdate_cgst_type = "DR";
                        rupdate_cgst_value = Math.abs(rcamnt);
                    } else {
                        rupdate_cgst_type = "CR";
                        rupdate_cgst_value = Math.abs(rcamnt);
                    }
                } else {
                    if (rcgst_type.equals("CR")) {
                        rcamnt = rcgst_amnt + rcgst_value;
                        if (rcamnt >= 0) {
                            rupdate_cgst_type = "CR";
                            rupdate_cgst_value = Math.abs(rcamnt);
                        }
                    }
                }
                rcgst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_cgst_value + "',currbal_type='" + rupdate_cgst_type + "' where ledger_name = 'CGST 2.5%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!old_cgst_6.equals("")) {
            try {
                double rcgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcgst_st1 = connect.createStatement();
                ResultSet rcgst_rs1 = rcgst_st1.executeQuery("Select * from ledger where ledger_name = 'CGST 6%'");
                while (rcgst_rs1.next()) {
                    rcgst_value1 = rcgst_rs1.getDouble("curr_bal");
                    rcgst_type1 = rcgst_rs1.getString("currbal_type");
                }

//                double rcgst_amnt = Double.parseDouble(jTextField25.getText());
                ResultSet rcgst_rs = rcgst_st1.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rcgst_rs.next()) {
                    rcgst_amnt = rcgst_rs.getDouble("total_cgst_amnt_6");
                }

                double rcamnt;

                if (rcgst_type1.equals("DR")) {
                    rcamnt = rcgst_amnt - rcgst_value1;
                    if (rcamnt <= 0) {
                        rupdate_cgst_type1 = "DR";
                        rupdate_cgst_value1 = Math.abs(rcamnt);
                    } else {
                        rupdate_cgst_type1 = "CR";
                        rupdate_cgst_value1 = Math.abs(rcamnt);
                    }
                } else {
                    if (rcgst_type1.equals("CR")) {
                        rcamnt = rcgst_amnt + rcgst_value1;
                        if (rcamnt >= 0) {
                            rupdate_cgst_type1 = "CR";
                            rupdate_cgst_value1 = Math.abs(rcamnt);
                        }
                    }
                }
                rcgst_st1.executeUpdate("Update ledger SET curr_bal= '" + rupdate_cgst_value1 + "',currbal_type='" + rupdate_cgst_type1 + "' where ledger_name = 'CGST 6%'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------   
//------------------------For Other Charges--------------------------------------------------------------------------
        if ((Double.parseDouble(jTextField14.getText()) == 0) && (other_charges == null)) {

        } else if ((Double.parseDouble(jTextField14.getText()) != 0) && (other_charges == null)) {
            JOptionPane.showMessageDialog(rootPane, "Please choose Direct expenses account");
            jTextField14.requestFocus();
        } else {
            try {
                double rotheramount = 0;
                double rothercurrbal = 0;
                String rCurbaltypeother = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement rother_st = connect.createStatement();
                ResultSet rother_rs = rother_st.executeQuery("Select * from ledger where ledger_name = '" + other_charges + "'");

                while (rother_rs.next()) {
                    rothercurrbal = rother_rs.getDouble("curr_bal");
                    rCurbaltypeother = rother_rs.getString("currbal_type");
                }
//                double rotheramount = Double.parseDouble(jTextField14.getText());

                ResultSet rother_rs1 = rother_st.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rother_rs1.next()) {
                    rotheramount = rother_rs1.getDouble("Other_charges");
                }

                double raother;

                if (rCurbaltypeother.equals("DR")) {
                    raother = rotheramount - rothercurrbal;
                    if (raother < 0) {
                        rupdate_other_type = "DR";
                        rupdate_other_value = Math.abs(raother);

                    } else {
                        rupdate_other_type = "CR";
                        rupdate_other_value = Math.abs(raother);
                    }

                } else {
                    if (rCurbaltypeother.equals("CR")) {
                        raother = rotheramount + rothercurrbal;
                        if (raother > 0) {
                            rupdate_other_type = "CR";
                            rupdate_other_value = Math.abs(raother);
                        }
                    }
                }

                rother_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_other_value + "',currbal_type='" + rupdate_other_type + "' where ledger_name='" + other_charges + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------
//--------------For Creaditor and Cash----------------------------------------------------------------------------------------------------------
        if (jRadioButton3.isSelected()) {
            try {
                double rcash_amount = 0;
                double rcashcurrbal = 0;
                String rCurbaltypecash = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcash_st = connect.createStatement();
                ResultSet rcash_rs = rcash_st.executeQuery("Select * from ledger where ledger_name = 'CASH'");

                while (rcash_rs.next()) {
                    rcashcurrbal = rcash_rs.getDouble("curr_bal");
                    rCurbaltypecash = rcash_rs.getString("currbal_type");
                }
//                double rcash_amount = Double.parseDouble(jTextField16.getText());

                ResultSet rcash_rs1 = rcash_st.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rcash_rs1.next()) {
                    rcash_amount = rcash_rs1.getDouble("Net_amount");
                }

                double rcash_amnt = 0;

                if (rCurbaltypecash.equals("DR")) {
                    rcash_amnt = rcashcurrbal + rcash_amount;
                    rupdate_cash_value = Math.abs(rcash_amnt);
                } else if (rCurbaltypecash.equals("CR")) {
                    if (rcash_amnt > rcash_amount) {
                        rcash_amnt = rcashcurrbal - rcash_amount;
                        rupdate_cash_value = Math.abs(rcash_amnt);
                    }
                    if (rcash_amnt < rcash_amount) {
                        rcash_amnt = rcash_amount - rcashcurrbal;
                        rupdate_cash_value = Math.abs(rcash_amnt);
                        rupdate_cash_type = "DR";
                    }
                }

                System.out.println("Reverse cash balance -- " + rupdate_cash_value);
                rcash_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_cash_value + "',currbal_type='" + rupdate_cash_type + "' where ledger_name='CASH'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (jRadioButton4.isSelected()) {
            try {
                System.out.println("jradio button 4 selected h");
                double totalamount = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcraditor_st = connect.createStatement();
                ResultSet rs4 = rcraditor_st.executeQuery("select * from ledger where ledger_name='" + sundrycreditorold + "'");
                while (rs4.next()) {
                    sundrycreditorcurrbalold = rs4.getDouble("curr_bal");
                    sundrycreditorcurrbaltypeold = rs4.getString("currbal_type");
                }

                double creditoramount = sundrycreditorcurrbalold;
//            double totalamount = Double.parseDouble(jTextField16.getText());

                ResultSet rs5 = rcraditor_st.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rs5.next()) {
                    totalamount = rs5.getDouble("Net_amount");
                }

                double creditoramount1 = creditoramount;
                if (sundrycreditorcurrbaltypeold.equals("DR")) {
                    creditoramount1 = creditoramount1 + totalamount;
                    sundrycreditorcurrbalold = (creditoramount1);
                } else if (sundrycreditorcurrbaltypeold.equals("CR")) {
                    if (creditoramount1 > totalamount) {
                        creditoramount1 = creditoramount1 - totalamount;
                        sundrycreditorcurrbalold = (creditoramount1);

                    } else if (creditoramount1 <= totalamount) {
                        creditoramount1 = totalamount - creditoramount1;
                        sundrycreditorcurrbalold = (creditoramount1);
                        sundrycreditorcurrbaltypeold = ("DR");
                    }
                }
                rcraditor_st.executeUpdate("Update ledger SET curr_bal= '" + sundrycreditorcurrbalold + "',currbal_type='" + sundrycreditorcurrbaltypeold + "' where ledger_name='" + sundrycreditorold + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--------------For Purchase account----------------------------------------------------------------------------------------------------------------
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement rpur_st = connect.createStatement();
            Statement rpur_st1 = connect.createStatement();
            ResultSet rpur_rs1 = rpur_st1.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
            while (rpur_rs1.next()) {
                rpurc_currbal.add(0, rpur_rs1.getString("Amount"));
            }
            int rowcount = jTable1.getRowCount();
            for (int i = 0; i < rowcount; i++) {

                ResultSet rpur_rs = rpur_st.executeQuery("Select * from ledger where ledger_name = '" + (rpurc_account.get(i) + "") + "'");
                while (rpur_rs.next()) {
                    rpurc_type.add(i, rpur_rs.getString("currbal_type"));
                    rpurc_amnt.add(i, rpur_rs.getString("curr_bal"));
                }

                double rpurchase_amnt = Double.parseDouble(rpurc_amnt.get(i) + "");
                double rpurchase_currbal = Double.parseDouble(rpurc_currbal.get(i) + "");
                double rpur_bal = 0;

                if (rpurc_type.get(i).equals("DR")) {
                    rpur_bal = rpurchase_currbal - rpurchase_amnt;
                    if (rpur_bal <= 0) {
                        rupdate_purc_type.add(i, "DR");
                        rupdate_purc_amnt.add(i, Math.abs(rpur_bal));
                    } else {
                        rupdate_purc_type.add(i, "CR");
                        rupdate_purc_amnt.add(i, Math.abs(rpur_bal));
                    }

                } else {
                    if (rpurc_type.get(i).equals("CR")) {
                        rpur_bal = rpurchase_currbal + rpurchase_amnt;
                        if (rpur_bal >= 0) {
                            rupdate_purc_type.add(i, "CR");
                            rupdate_purc_amnt.add(i, Math.abs(rpur_bal));
                        }
                    }
                }

                System.out.println("reverse purchase amount" + rupdate_purc_amnt.get(i));
                System.out.println("reverse purchase account" + rpurc_account.get(i));

                rpur_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_purc_amnt.get(i) + "',currbal_type='" + rupdate_purc_type.get(i) + "' where ledger_name='" + rpurc_account.get(i) + "'");
            }
            rupdate_purc_amnt.clear();
            rupdate_purc_type.clear();
            rpurc_account.clear();
            rpurc_account.clear();

        } catch (Exception e) {
            e.printStackTrace();
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------For Freight with Gst----------------------------------------------------------------------
        if (!old_freight_gst.equals("")) {
            try {
                double rfreightamount = 0;
                double rfreightcurrbal = 0;
                String rCurbaltypefreight = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement rfreight_st = connect.createStatement();
                ResultSet rfreight_rs = rfreight_st.executeQuery("Select * from ledger where ledger_name = 'Freight Charges'");

                while (rfreight_rs.next()) {
                    rfreightcurrbal = rfreight_rs.getDouble("curr_bal");
                    rCurbaltypefreight = rfreight_rs.getString("currbal_type");
                }

                ResultSet rfreight_rs1 = rfreight_st.executeQuery("Select * from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                while (rfreight_rs1.next()) {
                    rfreightamount = rfreight_rs1.getDouble("Freight_Gst_Amount");
                }

                double raf;

                if (rCurbaltypefreight.equals("DR")) {
                    raf = rfreightamount - rfreightcurrbal;
                    if (raf < 0) {
                        rupdate_freight_type = "DR";
                        rupdate_freight_value = Math.abs(raf);

                    } else {
                        rupdate_freight_type = "CR";
                        rupdate_freight_value = Math.abs(raf);
                    }

                } else {
                    if (rCurbaltypefreight.equals("CR")) {
                        raf = rfreightamount + rfreightcurrbal;
                        if (raf > 0) {
                            rupdate_freight_type = "CR";
                            rupdate_freight_value = Math.abs(raf);
                        }
                    }
                }

                rfreight_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_freight_value + "',currbal_type='" + rupdate_freight_type + "' where ledger_name='Freight Charges'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//---------------------------------------------------------------------------------------------------------------------------
    }
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Update");
        if (i == 0) {
            try {
                String partypurchasedate = "";
                if (jDateChooser1.getDate() == null) {

                } else {
                    partypurchasedate = formatter.format(jDateChooser1.getDate());
                }
                connection c = new connection();
                Connection connect = c.cone();
                Statement stmt = connect.createStatement();
                Statement stmt1 = connect.createStatement();
                if (i_value.isEmpty() && s_value.isEmpty() && c_value.isEmpty()) {
                    System.out.println("First If Condition");
                    updatereversebalances();
                    updatebalances();
                    stmt1.executeUpdate("update stock1 set supplier_id='" + jTextField22.getText() + "',supplier_name='" + jTextField12.getText() + "',invoice_date='" + formatter.format(jDateChooser2.getDate()) + "',Refrence_number='" + jTextField4.getText() + "',Partypurchase_date='" + partypurchasedate + "' where Invoice_number='" + jTextField2.getText() + "' ");
                    JOptionPane.showMessageDialog(rootPane, "Data updated successfully !!!");
//                    this.dispose();
                } else {
                    System.out.println("Second else part");
                    updatereversebalances();
                    stmt.executeUpdate("Delete from stock1 where Invoice_number = '" + jTextField2.getText() + "'");
                    save();
                    JOptionPane.showMessageDialog(rootPane, "Data updated successfully !!!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.dispose();
//-----------------------------------------------------------------------------------------------------------                
            //       updatebalances();
//-----------------------------------------------------------------------------------------------------------
//
//                String partypurchasedate = "";
//                if (jDateChooser1.getDate() == null) {
//
//                } else {
//                    partypurchasedate = formatter.format(jDateChooser1.getDate());
//                }
//
//                int row_count = jTable1.getRowCount();
//                for (int r = 0; r <row_count; r++) {
//                    System.out.println("row count"+row_count);
//                    if (i_value.isEmpty() && s_value.isEmpty() && c_value.isEmpty()) {
//                        stmt.executeUpdate("update stock1 set supplier_id='" + jTextField22.getText() + "',supplier_name='" + jTextField12.getText() + "',invoice_date='" + formatter.format(jDateChooser2.getDate()) + "',Refrence_number='" + jTextField4.getText() + "',Purchase_type='" + purch + "',Rate='" + jTable1.getValueAt(r, 4) + "',Amount='" + jTable1.getValueAt(r, 5) + "',Bill_amount='" + jTextField13.getText() + "',discount='" + jTextField11.getText() + "',amount_after_discount='" + jTextField15.getText() + "',other_charges='" + jTextField14.getText() + "',net_amount='" + jTextField16.getText() + "',total_Sgst_amnt_25='" + jTextField24.getText() + "',total_sgst_amnt_6='" + jTextField26.getText() + "',total_cgst_amnt_25='" + jTextField17.getText() + "',total_cgst_amnt_6='" + jTextField25.getText() + "',to_ledger='" + jTextField12.getText() + "',by_ledger='" + jTable1.getValueAt(r, 1) + "',Partypurchase_date='" + partypurchasedate + "',otherchargesaccount='" + other_charges + "',total_igst_amnt_5='" + jTextField1.getText() + "',total_igst_amnt_12='" + jTextField27.getText() + "' where Invoice_number='" + jTextField2.getText() + "' ");
//                    } else {
//
//                        if (isIgstApplicable == true) {
////                          String igst = "IGST".concat(percent.get(j) + "").concat("%");
//                            String igst_per = "";
//                            Statement acc_st = connect.createStatement();
//                            Statement acc_st_pur = connect.createStatement();
//                            ResultSet acc_pur_rs = acc_st_pur.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(r, 1) + "") + "'");
//                            while (acc_pur_rs.next()) {
//                                String percentage = acc_pur_rs.getString("percent");
//                                double per = Double.parseDouble(percentage);
//                                DecimalFormat df = new DecimalFormat("0.#");
//                                ResultSet acc_rs = acc_st.executeQuery("Select * from ledger where Type = 'IGST' and percent = '" + df.format(per) + "'");
//                                while (acc_rs.next()) {
//                                    igst_per = acc_rs.getString("ledger_name");
//                                }
//                                stmt.executeUpdate("update stock1 set supplier_id='" + jTextField22.getText() + "',supplier_name='" + jTextField12.getText() + "',invoice_date='" + formatter.format(jDateChooser2.getDate()) + "',Refrence_number='" + jTextField4.getText() + "',Purchase_type='" + purch + "',Rate='" + (jTable1.getValueAt(r, 4)+"") + "',Amount='" + (jTable1.getValueAt(r, 5)+"") + "',Bill_amount='" + jTextField13.getText() + "',discount='" + jTextField11.getText() + "',amount_after_discount='" + jTextField15.getText() + "',other_charges='" + jTextField14.getText() + "',net_amount='" + jTextField16.getText() + "',igst_amount='" + i_value.get(r) + "',sgst_amount='" + s_value.get(r) + "',cgst_amount='" + c_value.get(r) + "',total_Sgst_amnt_25='" + jTextField24.getText() + "',total_sgst_amnt_6='" + jTextField26.getText() + "',total_cgst_amnt_25='" + jTextField17.getText() + "',total_cgst_amnt_6='" + jTextField25.getText() + "',to_ledger='" + jTextField12.getText() + "',by_ledger='" + (jTable1.getValueAt(r, 1)+"") + "',Partypurchase_date='" + partypurchasedate + "',otherchargesaccount='" + other_charges + "',total_igst_amnt_5='" + jTextField1.getText() + "',total_igst_amnt_12='" + jTextField27.getText() + "',igst_account='" + igst_per + "' where Invoice_number='" + jTextField2.getText() + "' ");
//                            }
//                        } else if (isIgstApplicable == false) {
//                            String sgst_per = "";
//                            String cgst_per = "";
//                            Statement acc_st = connect.createStatement();
//                            Statement acc_st_pur = connect.createStatement();
//                            ResultSet acc_pur_rs = acc_st_pur.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(r, 1) + "") + "'");
//                            while (acc_pur_rs.next()) {
//                                String percentage = acc_pur_rs.getString("percent");
//                                double per = Double.parseDouble(percentage) / 2;
//                                DecimalFormat df = new DecimalFormat("0.#");
//                                System.out.println("percentage"+df.format(per));
//                                ResultSet acc_rs = acc_st.executeQuery("Select * from ledger where Type = 'SGST' and percent = '" + df.format(per) + "'");
//                                while (acc_rs.next()) {
//                                    sgst_per = acc_rs.getString("ledger_name");
//                                    System.out.println("sgst_per"+sgst_per);
//                                }
//                                Statement acc_st1 = connect.createStatement();
//                                ResultSet acc_rs1 = acc_st1.executeQuery("Select * from ledger where Type = 'CGST' and percent = '" + df.format(per) + "'");
//                                while (acc_rs1.next()) {
//                                    cgst_per = acc_rs1.getString("ledger_name");
//                                     System.out.println("cgst_per"+cgst_per);
//                                }
//                            
//                                stmt.executeUpdate("update stock1 set supplier_id='" + jTextField22.getText() + "',supplier_name='" + jTextField12.getText() + "',invoice_date='" + formatter.format(jDateChooser2.getDate()) + "',Refrence_number='" + jTextField4.getText() + "',Purchase_type='" + purch + "',Rate='" + (jTable1.getValueAt(r, 4)+"") + "',Amount='" + (jTable1.getValueAt(r, 5)+"") + "',Bill_amount='" + jTextField13.getText() + "',discount='" + jTextField11.getText() + "',amount_after_discount='" + jTextField15.getText() + "',other_charges='" + jTextField14.getText() + "',net_amount='" + jTextField16.getText() + "',igst_amount='" + i_value.get(r) + "',sgst_amount='" + s_value.get(r) + "',cgst_amount='" + c_value.get(r) + "',total_Sgst_amnt_25='" + jTextField24.getText() + "',total_sgst_amnt_6='" + jTextField26.getText() + "',total_cgst_amnt_25='" + jTextField17.getText() + "',total_cgst_amnt_6='" + jTextField25.getText() + "',sgst_account='" + sgst_per + "',to_ledger='" + jTextField12.getText() + "',by_ledger='" + (jTable1.getValueAt(r, 1)+"") + "',Partypurchase_date='" + partypurchasedate + "',cgst_account='" + cgst_per + "',otherchargesaccount='" + other_charges + "',total_igst_amnt_5='" + jTextField1.getText() + "',total_igst_amnt_12='" + jTextField27.getText() + "' where Invoice_number='" + jTextField2.getText() + "' ");
//                        }
//                      }
//                    }
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            JOptionPane.showMessageDialog(rootPane, "Your Purchase Entry is Updated Successfully");
//
//            dispose();
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        f = 0;
        s = 1;

        famount = 0;
        mount = 0;
        fvat = 0;
        ffvat = 0;

        obj = null;


    }//GEN-LAST:event_formWindowClosed

    private void jTextField25FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField25FocusGained
        // TODO add your handling code here:
        //     cstbefore = Double.parseDouble(jTextField25.getText());
    }//GEN-LAST:event_jTextField25FocusGained

    private void list5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_list5ActionPerformed

    private void jComboBox3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox3KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField6.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jComboBox5.requestFocus();
        }
    }//GEN-LAST:event_jComboBox3KeyPressed
    public String Party_Igst = null;
    private void list6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list6ActionPerformed
        jDialog9.dispose();
        show();
        if (list6.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger cr = new reporting.accounts.createLedger();
            cr.setVisible(true);
            cr.jComboBox1.setSelectedItem("DUTIES AND TAXES");
            cr.jComboBox5.setSelectedItem("OTHER");
        } else {

            Party_Igst = list6.getSelectedItem();
            String Igst_percent = null;
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from ledger where ledger_name='" + Party_Igst + "' ");
                while (rs.next()) {
                    Igst_percent = rs.getString(6);
                    if (cashisselected) {
                        jLabel51.setText("" + rs.getString(10));
                        jLabel52.setText("" + rs.getString(11));
                    } else {
                        jLabel51.setText("" + rs.getString(10));
                        jLabel52.setText("" + rs.getString(11));

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            jTextField1.setText("" + Igst_percent);
        }
        jTextField27.requestFocus();
        calculationall();
    }//GEN-LAST:event_list6ActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
//
//            if (Double.parseDouble(jTextField1.getText()) != 0) {
//                jDialog9.setVisible(true);
//                list6.removeAll();
//                list6.addItem("ADD NEW");
//                try {
//                    connection c = new connection();
//                    Connection connect = c.cone();
//
//                    Statement st = connect.createStatement();
//                    Statement st1 = connect.createStatement();
//                    String duties = null;
//                    String Duteis = "DUTIES AND TAXES";
//                    ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
//                    while (rs1.next()) {
//                        duties = rs1.getString(1);
//                    }
//                    ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = 'IGST' ");
//                    while (rs.next()) {
//                        list6.add(rs.getString(1));
//                    }
//                } catch (SQLException sqe) {
//                    System.out.println("SQl error");
//                    sqe.printStackTrace();
//                }
//                hide();
//
//            } else {
            calculationall();
            jTextField27.requestFocus();
        }
//        }
        if (key == evt.VK_ESCAPE) {
            jTextField25.requestFocus();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField27KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField27KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            calculationall();
            jTextField1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField24.requestFocus();
        }
    }//GEN-LAST:event_jTextField27KeyPressed
    double Igst_before = 0;
    double Igst_after = 0;
    private void jTextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField1FocusLost
//        Igst_after = Double.parseDouble(jTextField1.getText());
//        if (Igst_after != Igst_before) {
//            calculationall();
//        } else {
//
//        }
    }//GEN-LAST:event_jTextField1FocusLost

    private void jTextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField1FocusGained
//        Igst_before = Double.parseDouble(jTextField1.getText());
    }//GEN-LAST:event_jTextField1FocusGained

    private void jTextField26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField26ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField26ActionPerformed

    private void jComboBox5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox5KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jComboBox5KeyPressed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jComboBox6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox6KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField30.requestFocus();
        }
    }//GEN-LAST:event_jComboBox6KeyPressed

    private void jTextField30KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField30KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            String rate = jTextField30.getText();
            if (!rate.matches("[0-9]+$") || jTextField30.getText().isEmpty()) {
                JOptionPane.showMessageDialog(rootPane, "Please Enter Number only");
                jTextField30.requestFocus();
            } else {
                double qun_tity = Double.parseDouble(jTextField29.getText());
                double p = Double.parseDouble(jTextField30.getText());
                double q = qun_tity;
                double amou_nt = (p * q);
                if (!jTextField11.getText().equals("0")) {
                    double discou = (Double.parseDouble(jTextField11.getText()) / 100) * amou_nt;
                    double final_amnt = amou_nt - discou;
                    DecimalFormat df = new DecimalFormat("0.00");
                    jTextField31.setText(df.format(final_amnt));
                    jComboBox12.requestFocus();
                } else {
                    DecimalFormat df = new DecimalFormat("0.00");
                    String amount1 = df.format(amou_nt);
                    jTextField31.setText(df.format(amount1));
                    jComboBox12.requestFocus();
                }
            }
        }
    }//GEN-LAST:event_jTextField30KeyPressed
    public void party_update() {
        int row_count = jTable1.getRowCount();
        double totalamnt = 0;
        double amnt = 0;
        tiv = 0;
        tiv1 = 0;
        tsv = 0;
        tsv1 = 0;
        tcv = 0;
        tcv1 = 0;
        iv = 0;
        sv = 0;
        cv = 0;
        jTextField1.setText("0");
        jTextField27.setText("0");
        jTextField17.setText("0");
        jTextField24.setText("0");
        jTextField25.setText("0");
        jTextField26.setText("0");
        for (int i = 0; i < row_count; i++) {
            DecimalFormat df = new DecimalFormat("0.00");
// Calculating GST percantage value
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement igst_st = connect.createStatement();
                Statement scgst_st = connect.createStatement();
                if (isIgstApplicable == true) {
                    ResultSet igst_rs = igst_st.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(i, 1) + "") + "'");
                    while (igst_rs.next()) {
                        igst_percent = igst_rs.getDouble("percent");
                        double value = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
//                            amntbeforetax = (value / (100 + gst_percent)) * 100;
                        iv = (value * igst_percent) / 100;
                    }
                    i_value.add(iv);
                    s_value.add(0);
                    c_value.add(0);
                    percent.add(igst_percent);
                    if (igst_percent == 5) {
                        tiv = tiv + iv;
                        jTextField1.setText("" + df.format(tiv));
                    } else if (igst_percent == 12) {
                        tiv1 = tiv1 + iv;
                        jTextField27.setText("" + df.format(tiv1));
                    }
                    jTextField17.setEnabled(false);
                    jTextField24.setEnabled(false);
                    jTextField25.setEnabled(false);
                    jTextField26.setEnabled(false);
                    jTextField1.setEnabled(true);
                    jTextField27.setEnabled(true);
                } else if (isIgstApplicable == false) {
                    ResultSet scgst_rs = scgst_st.executeQuery("Select * from ledger where ledger_name = '" + (jTable1.getValueAt(i, 1) + "") + "'");
                    while (scgst_rs.next()) {
                        scgst_percent = scgst_rs.getDouble("percent");
                        double value = Double.parseDouble(jTable1.getValueAt(i, 5) + "");
//                            amntbeforetax = (value / (100 + gst_percent)) * 100;
                        sv = (value * (scgst_percent) / 2) / 100;
                        cv = (value * (scgst_percent) / 2) / 100;
                    }
                    i_value.add(0);
                    s_value.add(sv);
                    c_value.add(cv);
                    percent.add((scgst_percent / 2));

                    if (scgst_percent == 5) {
                        tsv = tsv + sv;
                        tcv = tcv + cv;
                        jTextField17.setText("" + df.format(tcv));
                        jTextField24.setText("" + df.format(tsv));
                    } else if (scgst_percent == 12) {
                        tsv1 = tsv1 + sv;
                        tcv1 = tcv1 + cv;
                        jTextField25.setText("" + df.format(tcv1));
                        jTextField26.setText("" + df.format(tsv1));
                    }

                    jTextField1.setEnabled(false);
                    jTextField27.setEnabled(false);

                    jTextField17.setEnabled(true);
                    jTextField24.setEnabled(true);
                    jTextField25.setEnabled(true);
                    jTextField26.setEnabled(true);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            amnt = Double.parseDouble(jTable1.getValueAt(i, 3) + "") * Double.parseDouble(jTable1.getValueAt(i, 4) + "");
            totalamnt = amnt + totalamnt;
        }

        jTextField13.setText("" + totalamnt);
        calculationall();

        if (Double.parseDouble(jTextField40.getText()) != 0) {
            freightGst();
        }

    }
    private void jButton8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton8KeyPressed
        jDialog10.setVisible(false);
        int sel_row = jTable1.getSelectedRow();
        jTable1.setValueAt("" + jComboBox6.getSelectedItem(), sel_row, 1);
        jTable1.setValueAt("" + jTextField30.getText(), sel_row, 4);
        jTable1.setValueAt("" + jTextField31.getText(), sel_row, 5);
        jTextField40.setText("" + Double.parseDouble(jTextField41.getText()));
        String fre_gst = (String) jComboBox12.getSelectedItem();
        jComboBox11.setSelectedItem(fre_gst);
        party_update();
    }//GEN-LAST:event_jButton8KeyPressed

    private void jDateChooser1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jDateChooser1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();
        if (key == evt.VK_ENTER) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jDateChooser1KeyPressed

    private void jTextField32KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField32KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jComboBox5.requestFocus();

        }
        if (key == evt.VK_ESCAPE) {
            jTextField4.requestFocus();
        }

    }//GEN-LAST:event_jTextField32KeyPressed

    private void jComboBox7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox7KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox8.requestFocus();
        }
    }//GEN-LAST:event_jComboBox7KeyPressed

    private void jComboBox8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox8KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField34.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jComboBox7.requestFocus();
        }
    }//GEN-LAST:event_jComboBox8KeyPressed

    private void jTextField34KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField34KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            String rate = jTextField34.getText();
            if (!rate.matches("[0-9]+$") || jTextField34.getText().isEmpty()) {
                JOptionPane.showMessageDialog(rootPane, "Please Enter Number only");
                jTextField34.requestFocus();
            } else {
                double qun_tity = Double.parseDouble(jTextField33.getText());
                double p = Double.parseDouble(jTextField34.getText());
                double q = qun_tity;
                double amou_nt = (p * q);
                if (!jTextField11.getText().equals("0")) {
                    double discou = (Double.parseDouble(jTextField11.getText()) / 100) * amou_nt;
                    double final_amnt = amou_nt - discou;
                    DecimalFormat df = new DecimalFormat("0.00");
                    jTextField35.setText(df.format(final_amnt));
                    jTextField36.requestFocus();
                } else {
                    DecimalFormat df = new DecimalFormat("0.00");
                    String amount1 = df.format(amou_nt);
                    jTextField35.setText("" + amount1);
                    jTextField36.requestFocus();
                }
                // For Product Description
                double discountpercent = Double.parseDouble(jTextField32.getText());
                double purchaseamnt = Double.parseDouble(jTextField34.getText());
                double purchaseamntafterdiscount = purchaseamnt - ((discountpercent / 100) * purchaseamnt);
                double taxpercent = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement st = connect.createStatement();
                    ResultSet rs = st.executeQuery("select percent from ledger where ledger_name='" + jComboBox7.getSelectedItem() + "" + "'");
                    if (rs.next()) {
                        taxpercent = rs.getDouble(1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // For Retail
                double taxamnt = (taxpercent / 100) * purchaseamntafterdiscount;
                double purchasenetamnt = taxamnt + purchaseamntafterdiscount;
                purchasenetamnt = Math.round(purchasenetamnt);
                jTextField36.setText(purchasenetamnt + "");

                double rate1 = Double.parseDouble(jTextField34.getText());
                double percentamount = ((double) markup / 100) * rate1;
                double intialretailamount = rate1 + percentamount;
                double finalretailamount = 10 * (Math.ceil(Math.abs(intialretailamount / 10)));
                jTextField37.setText("" + (int) finalretailamount);
                jTextField36.requestFocus();
            }
        } else if (key == evt.VK_ESCAPE) {
            jComboBox8.requestFocus();
        }
    }//GEN-LAST:event_jTextField34KeyPressed

    private void jTextField36KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField36KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField37.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField34.requestFocus();
        }
    }//GEN-LAST:event_jTextField36KeyPressed

    private void jTextField37KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField37KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField38.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField36.requestFocus();
        }
    }//GEN-LAST:event_jTextField37KeyPressed

    private void jTextField38KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField38KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox9.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField37.requestFocus();
        }
    }//GEN-LAST:event_jTextField38KeyPressed

    private void jComboBox9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox9KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox10.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jTextField38.requestFocus();
        }
    }//GEN-LAST:event_jComboBox9KeyPressed

    private void jComboBox10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox10KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton9.requestFocus();
        } else if (key == evt.VK_ESCAPE) {
            jComboBox9.requestFocus();
        }
    }//GEN-LAST:event_jComboBox10KeyPressed
    public void changeData() {
        int sel_row = jTable1.getSelectedRow();
        row1 = jTable1.getSelectedRow();
// Getting Data vetcor/ Linked has map data         
        sno1 = (String) jTable1.getValueAt(sel_row, 0);
        Vector aDataVector = (Vector) SerialNoDataVectorMap.get(sno1);
        DefaultTableModel dtm1 = (DefaultTableModel) jTable2.getModel();
        dtm1.getDataVector().removeAllElements();
        if (aDataVector != null) {
            for (int i = 0; i < aDataVector.size(); i++) {
                dtm1.addRow((Vector) aDataVector.get(i));
                jTable2.setValueAt("" + jTextField34.getText(), i, 2);
                jTable2.setValueAt("" + jTextField37.getText(), i, 3);
                jTable2.setValueAt("" + jTextField36.getText(), i, 4);
                jTable2.setValueAt("" + jComboBox8.getSelectedItem(), i, 6);
                jTable2.setValueAt("" + jComboBox9.getSelectedItem(), i, 5);
                jTable2.setValueAt("" + jComboBox10.getSelectedItem(), i, 7);
                jTable2.setValueAt("" + jTextField38.getText(), i, 11);
            }
        }
// Setting New Data in vector / linked has map                
        String sno = (String) jTable1.getValueAt(sel_row, 0);

        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        aDataVector = new Vector(dtm.getDataVector());

        System.out.println(" a datavector is " + aDataVector);
        SerialNoDataVectorMap.put(sno, aDataVector);

        jTable1.setValueAt("" + jComboBox7.getSelectedItem(), sel_row, 1);
        jTable1.setValueAt("" + jComboBox8.getSelectedItem(), sel_row, 2);
        jTable1.setValueAt("" + jTextField34.getText(), sel_row, 4);
        jTable1.setValueAt("" + jTextField35.getText(), sel_row, 5);
        jTable1.setValueAt("" + jTextField36.getText(), sel_row, 6);
        jTable1.setValueAt("" + jTextField37.getText(), sel_row, 7);
        jTable1.setValueAt("" + jTextField38.getText(), sel_row, 8);
        jTable1.setValueAt("" + jComboBox9.getSelectedItem(), sel_row, 9);
        jTable1.setValueAt("" + jComboBox10.getSelectedItem(), sel_row, 10);

        party_update();
        jDialog11.setVisible(false);
    }
    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
//        changeData();
        int sel_row = jTable1.getSelectedRow();
        try {
            boolean old_meter = false;
            boolean new_meter = false;
            connection c = new connection();
            Connection connect = c.cone();
            Statement old_st = connect.createStatement();
            Statement new_st = connect.createStatement();
            ResultSet old_prod = old_st.executeQuery("Select ismeter from product where product_code = '" + jTable1.getValueAt(sel_row, 2) + "'");
            while (old_prod.next()) {
                old_meter = old_prod.getBoolean("ismeter");
            }
            ResultSet new_prod = new_st.executeQuery("Select ismeter from product where product_code = '" + (String) jComboBox8.getSelectedItem() + "'");
            while (new_prod.next()) {
                new_meter = new_prod.getBoolean("ismeter");
            }
            if (old_meter == new_meter) {
                JOptionPane.showMessageDialog(rootPane, "Product change successfully !!!");
                changeData();
            } else if (old_meter != new_meter) {
                JOptionPane.showMessageDialog(rootPane, "Can not change product please chose right product");
                jComboBox8.requestFocus();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton9KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            int sel_row = jTable1.getSelectedRow();
            try {
                boolean old_meter = false;
                boolean new_meter = false;
                connection c = new connection();
                Connection connect = c.cone();
                Statement old_st = connect.createStatement();
                Statement new_st = connect.createStatement();
                ResultSet old_prod = old_st.executeQuery("Select ismeter from product where product_code = '" + jTable1.getValueAt(sel_row, 2) + "'");
                while (old_prod.next()) {
                    old_meter = old_prod.getBoolean("ismeter");
                }
                ResultSet new_prod = new_st.executeQuery("Select ismeter from product where product_code = '" + (String) jComboBox8.getSelectedItem() + "'");
                while (new_prod.next()) {
                    new_meter = new_prod.getBoolean("ismeter");
                }
                if (old_meter == new_meter) {
                    JOptionPane.showMessageDialog(rootPane, "Product change successfully !!!");
                    changeData();
                } else if (old_meter != new_meter) {
                    JOptionPane.showMessageDialog(rootPane, "Can not change product please chose right product");
                    jComboBox8.requestFocus();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (key == evt.VK_ESCAPE) {
            jComboBox10.requestFocus();
        }
    }//GEN-LAST:event_jButton9KeyPressed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable1KeyReleased

    private void jTextField40KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField40KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDialog12.setVisible(true);
        }
    }//GEN-LAST:event_jTextField40KeyPressed

    private void jButton10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton10KeyPressed
        freightGst();
        jDialog12.dispose();
    }//GEN-LAST:event_jButton10KeyPressed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        freightGst();
        jDialog12.dispose();
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jComboBox11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox11KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton10.requestFocus();
        }
    }//GEN-LAST:event_jComboBox11KeyPressed

    private void jTextField41KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField41KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton8.requestFocus();
        }
    }//GEN-LAST:event_jTextField41KeyPressed

    private void jComboBox12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox12KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField41.requestFocus();
        }
    }//GEN-LAST:event_jComboBox12KeyPressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pur_trans_dualGst.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pur_trans_dualGst.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pur_trans_dualGst.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pur_trans_dualGst.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Pur_trans_dualGst().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    public javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    public javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox<String> jComboBox10;
    private javax.swing.JComboBox<String> jComboBox11;
    private javax.swing.JComboBox<String> jComboBox12;
    public javax.swing.JComboBox jComboBox2;
    public javax.swing.JComboBox jComboBox3;
    public javax.swing.JComboBox jComboBox4;
    private javax.swing.JComboBox<String> jComboBox5;
    private javax.swing.JComboBox<String> jComboBox6;
    private javax.swing.JComboBox<String> jComboBox7;
    private javax.swing.JComboBox<String> jComboBox8;
    private javax.swing.JComboBox<String> jComboBox9;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    public com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog10;
    private javax.swing.JDialog jDialog11;
    private javax.swing.JDialog jDialog12;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JDialog jDialog4;
    private javax.swing.JDialog jDialog5;
    private javax.swing.JDialog jDialog6;
    private javax.swing.JDialog jDialog7;
    private javax.swing.JDialog jDialog8;
    private javax.swing.JDialog jDialog9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    public javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    public javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel30;
    public javax.swing.JLabel jLabel31;
    public javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    public javax.swing.JLabel jLabel38;
    public javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    public javax.swing.JLabel jLabel43;
    public javax.swing.JLabel jLabel44;
    public javax.swing.JLabel jLabel45;
    public javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    public javax.swing.JLabel jLabel48;
    public javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel50;
    public javax.swing.JLabel jLabel51;
    public javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    public javax.swing.JRadioButton jRadioButton3;
    public javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    public javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    public javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    public javax.swing.JTextField jTextField22;
    public javax.swing.JTextField jTextField23;
    public javax.swing.JTextField jTextField24;
    public javax.swing.JTextField jTextField25;
    public javax.swing.JTextField jTextField26;
    public javax.swing.JTextField jTextField27;
    public javax.swing.JTextField jTextField28;
    private javax.swing.JTextField jTextField29;
    public javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField30;
    private javax.swing.JTextField jTextField31;
    private javax.swing.JTextField jTextField32;
    private javax.swing.JTextField jTextField33;
    private javax.swing.JTextField jTextField34;
    private javax.swing.JTextField jTextField35;
    private javax.swing.JTextField jTextField36;
    private javax.swing.JTextField jTextField37;
    private javax.swing.JTextField jTextField38;
    private javax.swing.JTextField jTextField39;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField40;
    private javax.swing.JTextField jTextField41;
    public javax.swing.JTextField jTextField5;
    public javax.swing.JTextField jTextField6;
    public javax.swing.JTextField jTextField7;
    public javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    private java.awt.List list1;
    private java.awt.List list2;
    private java.awt.List list3;
    private java.awt.List list4;
    private java.awt.List list5;
    private java.awt.List list6;
    // End of variables declaration//GEN-END:variables
}
