package transaction;

import connection.connection;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

/**
 *
 * @author My Pc
 */
public class Purchasetransaction extends javax.swing.JFrame {

    public String choice = null;
    public String xz[];
    public String x[];
    public String y[];
    public String z[];
    static int count = 0;
    private LinkedHashMap SerialNoDataVectorMap = new LinkedHashMap();

    /**
     * Creates new form Purchasetransaction
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());
    Calendar currentDate1 = Calendar.getInstance();
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
    String dateNow1 = formatter1.format(currentDate1.getTime());
    Calendar currentDate2 = Calendar.getInstance();
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyy");
    String dateNow2 = formatter2.format(currentDate2.getTime());
    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);

    public String Party_cgst = null;
    public boolean isIgstApplicable = false;
    public boolean isUpdate = false;

    public Purchasetransaction() {
        combo();
        initComponents();
        //dynamicbyforgation();

        try {
            jDateChooser2.setDate(formatter.parse(dateNow));
            java.util.Date dateafter = formatter.parse("20" + getyear + "-03-31");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        jDateChooser2.setEnabled(false);
        jTextField2.setEnabled(false);
        jTextField3.setEnabled(false);
        jTextField4.setEnabled(false);
        jTextField5.setEnabled(false);
        jTextField6.setEnabled(false);
        jTextField7.setEnabled(false);
        jTextField8.setEnabled(false);
        jTextField9.setEnabled(false);
        jTextField10.setEnabled(false);
        jTextField11.setEnabled(false);
        jTextField12.setEnabled(false);
        jTextField13.setEnabled(false);
        jTextField14.setEnabled(false);
        jTextField15.setEnabled(false);
        jTextField16.setEnabled(false);
//        jTextField17.setEnabled(false);
        jTextField23.setEnabled(false);
        jRadioButton3.setEnabled(false);
        jRadioButton4.setEnabled(false);
        jComboBox3.setEnabled(false);
        jComboBox4.setEnabled(false);
        jComboBox1.setEnabled(false);
        jLabel29.setVisible(false);
        jLabel30.setVisible(false);
        jLabel31.setVisible(false);
        jLabel32.setVisible(false);
        jLabel43.setVisible(false);
        jLabel44.setVisible(false);
        jLabel45.setVisible(false);
        jLabel46.setVisible(false);
        jLabel38.setVisible(false);
        jLabel39.setVisible(false);
        jLabel15.setVisible(false);
        jTextField22.setEnabled(false);
        jTextField12.setEnabled(true);
        jButton5.setVisible(false);

        jLabel51.setVisible(false);
        jLabel52.setVisible(false);

        jComboBox4.addItem("ADD NEW");
        list1.addItem("ADD NEW");
        list2.addItem("ADD NEW");
        jLabel48.setText("" + Purchaseaccount);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from article_no ");
            while (rs.next()) {
                jComboBox1.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from product ");
            while (rs.next()) {
                jComboBox3.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        jComboBox1.addItem(null);

        jComboBox1.setSelectedItem(null);
        {
            jTextField3.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {

                }
            });
            jDialog4.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent e) {
                    jDialog4.dispose();
                    show();
                    jTextField26.requestFocus();

                }
            });
            jDialog5.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jDialog5.dispose();
                    show();
                    jTextField25.requestFocus();

                }
            });

            jDialog6.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    if (Purchaseaccount == null) {
                        JOptionPane.showMessageDialog(rootPane, "Please select Purchase Account");

                    }

                }
            });
            String purchase = "PR/";
            String slash = "/";
            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String finalconcat = (purchase.concat(yearwise));
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT Invoice_number FROM Stock where branchid='0'");
                String no = null;
                while (rs.next()) {
                    no = rs.getString(1);
                }
                if (no == null) {
                    String zero = "0";
                    no = finalconcat.concat(zero);
                }
                String[] Part1 = no.split("/");
                int a1 = Integer.parseInt(Part1[2]);
                String s = finalconcat.concat(Integer.toString(a1 + 1));
                jTextField2.setText("" + s);

            } catch (SQLException sqe) {
                sqe.printStackTrace();
            }

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGap(0, 643, Short.MAX_VALUE));
            layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGap(0, 517, Short.MAX_VALUE));

            pack();
        }// </editor-fold>     

    }

    private Object makeObj(final String item) {
        return new Object() {
            @Override
            public String toString() {
                return item;
            }
        };
    }
    //      rs = st.executeQuery("SELECT party_code from Party");

    public void combo() {

        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT party_code from Party");
            int a = 0;
            while (rs.next()) {
                a++;
            }
            x = new String[a];
            rs = st.executeQuery("SELECT party_code from Party");
            a = 0;
            while (rs.next()) {

                x[a] = rs.getString("party_code");
                a++;
            }

            Statement st1 = connect.createStatement();
            ResultSet rs1 = st1.executeQuery("SELECT product_code from product");
            int a1 = 0;
            while (rs1.next()) {
                a1++;
            }
            xz = new String[a1];
            rs1 = st1.executeQuery("SELECT product_code from product");
            a1 = 0;
            while (rs1.next()) {

                xz[a1] = rs1.getString("product_code");
                a1++;
            }

            Statement st12 = connect.createStatement();
            ResultSet rs12 = st12.executeQuery("SELECT * from byforgation");
            int a12 = 0;
            while (rs12.next()) {
                //jComboBox4.addItem(rs12.getString(1));
                a12++;
            }
            y = new String[a12];
            rs12 = st12.executeQuery("SELECT * from byforgation");
            a12 = 0;
            while (rs12.next()) {

                y[a12] = rs12.getString(1);
                a12++;
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        //jComboBox4.addItem("ADD NEW");
    }

//----------------After Discount Calculation--------------------------------------------------------
        public void reCalculate() {
            double discount_amount = Double.parseDouble(jTextField23.getText());
            DecimalFormat df = new DecimalFormat("0.00");
            double after_disc = 0;
            double totalcgstamnt = 0;
            double totaligstamnt = 0;
            double totalamountbeforetax = 0;
            double amountbeforetax = 0;
            int rowcount = jTable1.getRowCount();
            for (int i = 0; i < rowcount; i++) {

            double totalamntafterbill = Double.parseDouble(jTextField13.getText());
            double discountpercentage = (discount_amount / totalamntafterbill) * 100;
            double amount = Double.parseDouble(jTable1.getValueAt(i, 2) + "") * Double.parseDouble(jTable1.getValueAt(i, 3) + "");
            double totalafterdiscountpercentage = (discountpercentage / 100) * amount;
            double totalfinal = amount - totalafterdiscountpercentage;
            jTable1.setValueAt(df.format(totalfinal), i, 4);

                String igstaccount = "";
                String sgstaccount = "";
                String cgstaccount = "";
                double gstperc = 0;
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stmt1 = connect.createStatement();

                    if (isIgstApplicable == true) {
                        ResultSet rsigst = stmt1.executeQuery("select IGST from product where product_code='" + jTable1.getValueAt(i, 1) + "" + "'");
                        while (rsigst.next()) {
                            igstaccount = rsigst.getString(1);
                        }
                        ResultSet rsIGSTperc = stmt1.executeQuery("select percent from ledger where ledger_name ='" + igstaccount + "'");
                        while (rsIGSTperc.next()) {
                            gstperc = rsIGSTperc.getDouble(1);
                        }

                        amountbeforetax = (totalfinal / (100 + gstperc)) * 100;
                        double igstamnt = (gstperc * amountbeforetax) / 100;

                        totaligstamnt = igstamnt + totaligstamnt;

                        totalamountbeforetax = amountbeforetax + totalamountbeforetax;

                    } else if (isIgstApplicable == false) {
                        ResultSet rssgst = stmt1.executeQuery("select SGST from product where product_code='" + jTable1.getValueAt(i, 1) + "" + "'");
                        while (rssgst.next()) {
                            sgstaccount = rssgst.getString(1);
                        }
                        ResultSet rscgst = stmt1.executeQuery("select CGST from product where product_code='" + jTable1.getValueAt(i, 1) + "" + "'");
                        while (rscgst.next()) {
                            cgstaccount = rscgst.getString(1);
                        }
                        ResultSet rsSGSTperc = stmt1.executeQuery("select percent from ledger where ledger_name ='" + sgstaccount + "'");
                        while (rsSGSTperc.next()) {
                            gstperc = rsSGSTperc.getDouble(1);
                        }
                        
                        amountbeforetax = (totalfinal / (100 + gstperc + gstperc)) * 100;
                        double cgstamnt = (gstperc * amountbeforetax) / 100;

                        totalcgstamnt = cgstamnt + totalcgstamnt;

                        totalamountbeforetax = amountbeforetax + totalamountbeforetax;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                
                   after_disc = after_disc + Double.parseDouble(jTable1.getValueAt(i, 4) + "");
                   jTextField15.setText("" + df.format(after_disc));
            }

            jTextField25.setText(df.format(totalcgstamnt) + "");
            jTextField26.setText(df.format(totalcgstamnt) + "");
            jTextField27.setText(df.format(totaligstamnt) + "");
    
        percentage();    
        calculationall();
    }
//--------------------------------------------------------------------------------------------------   
//-------------Get Discount in Percentage from Discount value------------------------

    public double percentage() {
        float dis_per;
        double dis_amnt = 0;
        double total_amnt = 0;

        dis_amnt = Double.parseDouble(jTextField23.getText());
        total_amnt = Double.parseDouble(jTextField13.getText());

        dis_per = (float) ((dis_amnt / total_amnt) * 100);

        DecimalFormat df1 = new DecimalFormat("0.00");
        String per = df1.format(dis_per);

        jTextField11.setText(per + "%");
        return Double.parseDouble(per);
    }
//-----------------------------------------------------------------------------------         
    
    /**
     * This 00 is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jTextField19 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jTextField18 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jTextField20 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jDialog3 = new javax.swing.JDialog();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jCheckBox5 = new javax.swing.JCheckBox();
        jLabel35 = new javax.swing.JLabel();
        jDialog4 = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        list1 = new java.awt.List();
        jDialog6 = new javax.swing.JDialog();
        list2 = new java.awt.List();
        jDialog5 = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        list3 = new java.awt.List();
        jDialog7 = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        list4 = new java.awt.List();
        jDialog8 = new javax.swing.JDialog();
        list5 = new java.awt.List();
        jDialog9 = new javax.swing.JDialog();
        jPanel5 = new javax.swing.JPanel();
        list6 = new java.awt.List();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel16 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jComboBox4 = new javax.swing.JComboBox(y);
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel28 = new javax.swing.JLabel();
        jTextField24 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jTextField25 = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jTextField26 = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jTextField28 = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel34 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jComboBox3 = new javax.swing.JComboBox();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel49 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jTextField27 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();

        jDialog1.setMinimumSize(new java.awt.Dimension(800, 600));
        jDialog1.setUndecorated(true);

        jScrollPane3.setAutoscrolls(true);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sno.", "Stock No", "PRate", "Retail", "Net PRate", "byforgation", "Product", "Article no.", "ismeter", "qty", "tag print no.", "Desc"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, true, true, true, true, true, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(8).setResizable(false);
            jTable2.getColumnModel().getColumn(9).setResizable(false);
            jTable2.getColumnModel().getColumn(11).setResizable(false);
        }

        jButton3.setText("OK");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });

        jButton7.setText("update");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton4.setText("Cancel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(506, Short.MAX_VALUE)
                .addComponent(jButton7)
                .addGap(45, 45, 45)
                .addComponent(jButton3)
                .addGap(18, 18, 18)
                .addComponent(jButton4)
                .addGap(38, 38, 38))
            .addComponent(jScrollPane3)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton7)
                    .addComponent(jButton4))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jDialog2.setMinimumSize(new java.awt.Dimension(682, 580));

        jTextField19.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField19CaretUpdate(evt);
            }
        });
        jTextField19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField19ActionPerformed(evt);
            }
        });
        jTextField19.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField19KeyPressed(evt);
            }
        });

        jLabel24.setText("Enter Party code :");

        jTextField18.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField18CaretUpdate(evt);
            }
        });
        jTextField18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField18ActionPerformed(evt);
            }
        });
        jTextField18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField18KeyPressed(evt);
            }
        });

        jLabel25.setText("Enter Party Name :");

        jTextField20.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField20CaretUpdate(evt);
            }
        });
        jTextField20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField20KeyPressed(evt);
            }
        });

        jLabel26.setText("Enter city :");

        jLabel27.setText("Enter State :");

        jTextField21.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField21CaretUpdate(evt);
            }
        });
        jTextField21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField21KeyPressed(evt);
            }
        });

        jScrollPane5.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "party_code", "party_name", "city", "state", "opening_bal", "Balance Type", "Mark UP", "Purchase A/c", "IGST Applicable"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jTable3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable3KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTable3);

        jScrollPane5.setViewportView(jScrollPane4);

        jButton6.setText("ADD NEW PARTY");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 699, Short.MAX_VALUE)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addGap(44, 44, 44)
                        .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jDialog2Layout.createSequentialGroup()
                            .addComponent(jLabel26)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog2Layout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDialog2Layout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addGap(23, 23, 23)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addGap(22, 22, 22)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jCheckBox1.setText("MRP");

        jCheckBox2.setText("Desc");

        jCheckBox3.setText("PDesc");

        jCheckBox4.setText("Byforgation");

        jCheckBox5.setText("Product");

        jLabel35.setText("Select for Printing ?");

        javax.swing.GroupLayout jDialog3Layout = new javax.swing.GroupLayout(jDialog3.getContentPane());
        jDialog3.getContentPane().setLayout(jDialog3Layout);
        jDialog3Layout.setHorizontalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox4)
                    .addComponent(jCheckBox5)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(164, Short.MAX_VALUE))
        );
        jDialog3Layout.setVerticalGroup(
            jDialog3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel35)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox5)
                .addContainerGap(153, Short.MAX_VALUE))
        );

        jDialog4.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog4.setMinimumSize(new java.awt.Dimension(200, 400));

        list1.setMaximumSize(new java.awt.Dimension(200, 400));
        list1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(list1, javax.swing.GroupLayout.PREFERRED_SIZE, 521, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog4Layout = new javax.swing.GroupLayout(jDialog4.getContentPane());
        jDialog4.getContentPane().setLayout(jDialog4Layout);
        jDialog4Layout.setHorizontalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog4Layout.setVerticalGroup(
            jDialog4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog6.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog6.setTitle("SELECT PURCHASE ACCOUNT");
        jDialog6.setMinimumSize(new java.awt.Dimension(200, 400));

        list2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog6Layout = new javax.swing.GroupLayout(jDialog6.getContentPane());
        jDialog6.getContentPane().setLayout(jDialog6Layout);
        jDialog6Layout.setHorizontalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
        );
        jDialog6Layout.setVerticalGroup(
            jDialog6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog6Layout.createSequentialGroup()
                .addComponent(list2, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jDialog5.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog5.setMinimumSize(new java.awt.Dimension(200, 400));

        list3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(list3, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list3, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog5Layout = new javax.swing.GroupLayout(jDialog5.getContentPane());
        jDialog5.getContentPane().setLayout(jDialog5Layout);
        jDialog5Layout.setHorizontalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog5Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );
        jDialog5Layout.setVerticalGroup(
            jDialog5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog7.setMinimumSize(new java.awt.Dimension(200, 400));

        list4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list4, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list4, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog7Layout = new javax.swing.GroupLayout(jDialog7.getContentPane());
        jDialog7.getContentPane().setLayout(jDialog7Layout);
        jDialog7Layout.setHorizontalGroup(
            jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog7Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jDialog7Layout.setVerticalGroup(
            jDialog7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jDialog8.setMinimumSize(new java.awt.Dimension(200, 431));

        list5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog8Layout = new javax.swing.GroupLayout(jDialog8.getContentPane());
        jDialog8.getContentPane().setLayout(jDialog8Layout);
        jDialog8Layout.setHorizontalGroup(
            jDialog8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list5, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
        );
        jDialog8Layout.setVerticalGroup(
            jDialog8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list5, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
        );

        jDialog9.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog9.setMinimumSize(new java.awt.Dimension(200, 400));

        list6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(list6, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(list6, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog9Layout = new javax.swing.GroupLayout(jDialog9.getContentPane());
        jDialog9.getContentPane().setLayout(jDialog9Layout);
        jDialog9Layout.setHorizontalGroup(
            jDialog9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog9Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );
        jDialog9Layout.setVerticalGroup(
            jDialog9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("APPSON : PURCHASE");
        setMinimumSize(new java.awt.Dimension(1300, 900));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jLabel1.setText("Supplier ID :");

        jLabel2.setText("Invoice Date :");

        jLabel3.setText("Invoice No. :");

        jTextField2.setText("1");
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel4.setText("Purchase Type :");

        jLabel5.setText("Purchase Reference No. :");

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel6.setText("S No.");

        jTextField5.setEditable(false);
        jTextField5.setText("1");
        jTextField5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField5FocusGained(evt);
            }
        });
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jLabel7.setText("Qty.");

        jTextField6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField6FocusLost(evt);
            }
        });
        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });
        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel9.setText("Amount");

        jTextField7.setEditable(false);
        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });

        jLabel10.setText("Net PRate");

        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField8KeyPressed(evt);
            }
        });

        jLabel11.setText("Retail");

        jTextField9.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField9FocusLost(evt);
            }
        });
        jTextField9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField9KeyPressed(evt);
            }
        });

        jLabel12.setText("Desc.");

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });
        jTextField10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField10KeyPressed(evt);
            }
        });

        jLabel13.setText("Product");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SNO.", "PRODUCT", "QTY.", "Rate", "AMOUNT", "NET PRATE", "RETAIL", "DESC.", "BYFORGATION", "ARTICLE NO.", "SAME BARCODE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, true, true, true, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
        }

        jLabel14.setText("Supplier Name :");

        jTextField12.setEditable(false);
        jTextField12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField12KeyPressed(evt);
            }
        });

        jLabel16.setText("Bill Amount :");

        jTextField13.setEditable(false);
        jTextField13.setText("0");
        jTextField13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField13KeyPressed(evt);
            }
        });

        jLabel17.setText("Other Charges :");

        jTextField14.setText("0");
        jTextField14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField14FocusLost(evt);
            }
        });
        jTextField14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField14ActionPerformed(evt);
            }
        });
        jTextField14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField14KeyPressed(evt);
            }
        });

        jLabel19.setText("Net Amount :");

        jTextField16.setEditable(false);
        jTextField16.setText("0");
        jTextField16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField16FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField16FocusLost(evt);
            }
        });
        jTextField16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField16KeyPressed(evt);
            }
        });

        jLabel20.setText("Rate");

        jTextField3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField3FocusLost(evt);
            }
        });
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        jLabel21.setText("Discount (%) :");

        jTextField11.setText("0");
        jTextField11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField11ActionPerformed(evt);
            }
        });
        jTextField11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField11FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField11FocusLost(evt);
            }
        });
        jTextField11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField11KeyPressed(evt);
            }
        });

        jLabel18.setText("Amount After Dis :");

        jTextField15.setEditable(false);
        jTextField15.setText("0");
        jTextField15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField15FocusGained(evt);
            }
        });
        jTextField15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField15KeyPressed(evt);
            }
        });

        jLabel22.setText("CGST (%) :");

        jTextField17.setText("0");
        jTextField17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField17FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField17FocusLost(evt);
            }
        });
        jTextField17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField17KeyPressed(evt);
            }
        });

        jLabel23.setText("Byforgation");

        jComboBox4.setSelectedItem(null);
        jComboBox4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jComboBox4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox4ItemStateChanged(evt);
            }
        });
        jComboBox4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBox4FocusGained(evt);
            }
        });
        jComboBox4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboBox4MouseClicked(evt);
            }
        });
        jComboBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox4ActionPerformed(evt);
            }
        });
        jComboBox4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox4KeyPressed(evt);
            }
        });

        jLabel29.setText("jLabel29");

        jLabel30.setText("jLabel30");

        jLabel31.setText("jLabel31");

        jLabel32.setText("jLabel32");

        jTextField22.setEditable(false);
        jTextField22.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField22FocusLost(evt);
            }
        });
        jTextField22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField22ActionPerformed(evt);
            }
        });
        jTextField22.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField22KeyPressed(evt);
            }
        });

        jLabel33.setText("Discount Rs. :");

        jTextField23.setText("0");
        jTextField23.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField23FocusLost(evt);
            }
        });
        jTextField23.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField23KeyPressed(evt);
            }
        });

        buttonGroup1.add(jRadioButton3);
        jRadioButton3.setText("Cash");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton4);
        jRadioButton4.setText("Credit");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });

        jLabel28.setText("SGST (%) :");

        jTextField24.setText("0");
        jTextField24.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField24FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField24FocusLost(evt);
            }
        });
        jTextField24.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField24KeyPressed(evt);
            }
        });

        jLabel36.setText("CGST  Rs. :");

        jTextField25.setText("0");
        jTextField25.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField25FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField25FocusLost(evt);
            }
        });
        jTextField25.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField25KeyPressed(evt);
            }
        });

        jLabel37.setText("SGST Rs. :");

        jTextField26.setText("0");
        jTextField26.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField26FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField26FocusLost(evt);
            }
        });
        jTextField26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField26ActionPerformed(evt);
            }
        });
        jTextField26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField26KeyPressed(evt);
            }
        });

        jLabel38.setText("jLabel38");

        jLabel15.setText("jLabel15");

        jLabel39.setText("jLabel39");

        jLabel40.setText("Party Purchase Date :");

        jLabel41.setText("Article No.");

        jComboBox1.setSelectedItem(null);
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBox1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jComboBox1FocusLost(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jTextField28.setText("0");
        jTextField28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField28KeyPressed(evt);
            }
        });

        jLabel42.setText("Total quantity :");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NO", "YES" }));
        jComboBox2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jComboBox2FocusLost(evt);
            }
        });
        jComboBox2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox2KeyPressed(evt);
            }
        });

        jLabel34.setText("Same Barcode");

        jLabel43.setText("jLabel43");

        jLabel44.setText("jLabel44");

        jLabel45.setText("jLabel45");

        jLabel46.setText("jLabel46");

        jLabel47.setText("Purchase account :");

        jLabel48.setText("jLabel48");

        jButton5.setText("UPDATE");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jComboBox3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox3KeyPressed(evt);
            }
        });

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jDateChooser2.setDateFormatString("dd-MM-yyy");

        jLabel49.setText("IGST (%) :");

        jTextField1.setText("0");
        jTextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField1FocusLost(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel50.setText("IGST Rs. :");

        jTextField27.setText("0");
        jTextField27.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField27FocusLost(evt);
            }
        });
        jTextField27.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField27KeyPressed(evt);
            }
        });

        jLabel51.setText("jLabel51");

        jLabel52.setText("jLabel52");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel4)
                        .addGap(19, 19, 19)
                        .addComponent(jRadioButton3)
                        .addGap(30, 30, 30)
                        .addComponent(jRadioButton4)
                        .addGap(74, 74, 74)
                        .addComponent(jLabel47)
                        .addGap(202, 202, 202)
                        .addComponent(jLabel21)
                        .addGap(71, 71, 71)
                        .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36)
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(68, 68, 68)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(85, 85, 85)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(73, 73, 73)
                        .addComponent(jLabel23)
                        .addGap(62, 62, 62)
                        .addComponent(jLabel41)
                        .addGap(59, 59, 59)
                        .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 1267, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel16)
                        .addGap(70, 70, 70)
                        .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel22)
                        .addGap(38, 38, 38)
                        .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(jLabel49)
                        .addGap(50, 50, 50)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(jLabel43)
                        .addGap(12, 12, 12)
                        .addComponent(jLabel51)
                        .addGap(12, 12, 12)
                        .addComponent(jLabel15)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(14, 14, 14)
                                .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(62, 62, 62)
                                .addComponent(jLabel29)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel30)
                                .addGap(96, 96, 96)
                                .addComponent(jLabel2)
                                .addGap(58, 58, 58)
                                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel14)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(117, 117, 117)
                                        .addComponent(jLabel40)))
                                .addGap(58, 58, 58)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(70, 70, 70)
                                .addComponent(jLabel3)
                                .addGap(24, 24, 24)
                                .addComponent(jTextField2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(24, 24, 24)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel42)
                                    .addComponent(jLabel33))))
                        .addGap(42, 42, 42)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel28)
                            .addComponent(jLabel37))
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField25, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel50)
                            .addComponent(jLabel17)
                            .addComponent(jLabel19))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel44)
                            .addComponent(jLabel31)
                            .addComponent(jLabel32)
                            .addComponent(jLabel45))
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel52)
                                    .addComponent(jLabel46)
                                    .addComponent(jLabel38))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel39)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton5)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1)))
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel29)
                                    .addComponent(jLabel30)
                                    .addComponent(jLabel2))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel40)))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel3))
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jLabel5))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jRadioButton3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jRadioButton4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel47)
                            .addComponent(jLabel48)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel21))
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel13)
                    .addComponent(jLabel7)
                    .addComponent(jLabel20)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel23)
                    .addComponent(jLabel41)
                    .addComponent(jLabel34))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel22)
                            .addComponent(jLabel49)
                            .addComponent(jLabel43)
                            .addComponent(jLabel51)
                            .addComponent(jLabel15))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel42)
                        .addGap(21, 21, 21)
                        .addComponent(jLabel33)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel18))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel36)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel28)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel37))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTextField25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel50)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel17)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel19))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel44)
                        .addGap(12, 12, 12)
                        .addComponent(jLabel31)
                        .addGap(12, 12, 12)
                        .addComponent(jLabel32)
                        .addGap(11, 11, 11)
                        .addComponent(jLabel45))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel52)
                        .addGap(12, 12, 12)
                        .addComponent(jLabel46)
                        .addGap(12, 12, 12)
                        .addComponent(jLabel38)
                        .addGap(11, 11, 11)
                        .addComponent(jLabel39))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton2)
                            .addComponent(jButton5))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    }//GEN-LAST:event_formKeyPressed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
    }//GEN-LAST:event_formKeyReleased

    private void jTextField12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField12KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jRadioButton3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jDateChooser2.requestFocus();
        }
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_F2) {
            jDialog2.setVisible(true);
        }
    }//GEN-LAST:event_jTextField12KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed

        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jTextField11.requestFocus();

        }
        if (key == evt.VK_ESCAPE) {
            jRadioButton3.requestFocus();
        }
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField6.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jTextField5KeyPressed

    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            if (jTextField6.getText().isEmpty()) {
                jTextField23.requestFocus();
            } else {
                jTextField3.requestFocus();
            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField6KeyPressed

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jTextField8.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField6.requestFocus();
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField9.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField3.requestFocus();
        }
    }//GEN-LAST:event_jTextField8KeyPressed

    private void jTextField9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField9KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField10.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField9.requestFocus();
        }
    }//GEN-LAST:event_jTextField9KeyPressed

    private void jTextField10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField10KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox4.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField9.requestFocus();
        }

    }//GEN-LAST:event_jTextField10KeyPressed
    static int f = 0;
    static int s = 1;
    String stoke = null;
    static String sn;
    static String qnt;
    static String rate, amount, pds, rt, ds, pd, fr, articleno, samebarcode;
    static String sup, invod, invon, purch, ref;
    static String bil, otherc, va, namoun;
    static String zero = "000000";
    static String date1;
    static int first, second = 0;
    int gg = 0;
    int ggg = 0;
    int incr = 0;
    String supid[] = new String[100];
    String supnameid[] = new String[100];
    String invoiced[] = new String[100];
    String invoiceno[] = new String[100];
    String purchast[] = new String[100];
    String refrence[] = new String[100];
    String sno[] = new String[100];
    String qty[] = new String[100];
    String rat[] = new String[100];
    String amoun[] = new String[100];
    String pdes[] = new String[100];
    String retail[] = new String[100];
    String desc[] = new String[100];
    String prod[] = new String[100];
    String ftr[] = new String[100];
    String bill[] = new String[100];
    String ocharge[] = new String[100];
    String tax[] = new String[100];
    String discount[] = new String[100];
    String amountafter[] = new String[100];
    String namount[] = new String[100];
    String articlenum[] = new String[100];
    String samebarcode1[] = new String[100];
    static double famount = 0;
    static double mount = 0;
    double fvat = 0;
    static double ffvat = 0;
    String supname = null;
    ArrayList stockno = new ArrayList();

    public void stocknumbers() {
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yy");
        String year = getyear;
        SimpleDateFormat formatter1 = new SimpleDateFormat("MM");
        String month = formatter1.format(currentDate.getTime());
        int sto = 0;
        String totalbarcode = null;
        String sto1 = null;
        String start = "";
//        date1 = year.concat(month);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();

            ResultSet rs = st.executeQuery("select max(Stock_No) from Stockid where branchid = '0' and Da_te>'2017-03-2017' ");
            while (rs.next()) {
                sto1 = (rs.getString(1));
            }

            if (sto1 == null) {

                stoke = year.concat(zero);
                System.out.println("code is running for null" + stoke);
                sto = Integer.parseInt(stoke);
            } else {
                sto = Integer.parseInt(sto1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        jDialog1.setVisible(true);
        int j = 1;
        double q = Double.parseDouble(qnt);

        System.out.println("date" + date1);
        System.out.println("stockno.date" + ggg);

        if (ismeter == 0) {
            if (SerialNoDataVectorMap != null && SerialNoDataVectorMap.size() > 0) {
                Vector aDataVector = null;
                Iterator aIterator = SerialNoDataVectorMap.keySet().iterator();
                while (aIterator.hasNext()) {
                    aDataVector = (Vector) SerialNoDataVectorMap.get(aIterator.next());
                    if (aDataVector != null && aDataVector.size() > 0) {
                        sto = sto + aDataVector.size();
                    }
                }

            }

            DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            for (int i = 0; i < q; i++) {
                sto = sto + 1;
                Object d[] = {1, sto, rate, rt, pds, fr, pd, articleno, ismeter, 1, 1, ds};
                dtm = (DefaultTableModel) jTable2.getModel();
                dtm.addRow(d);
            }

        } else if (ismeter == 1) {

            if (SerialNoDataVectorMap != null && SerialNoDataVectorMap.size() > 0) {
                Vector aDataVector = null;
                Iterator aIterator = SerialNoDataVectorMap.keySet().iterator();
                while (aIterator.hasNext()) {
                    aDataVector = (Vector) SerialNoDataVectorMap.get(aIterator.next());
                    if (aDataVector != null && aDataVector.size() > 0) {
                        sto = sto + aDataVector.size();
                    }
                }

            }

            sto = sto + 1;
            DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            Object d[] = {1, sto, rate, rt, pds, fr, pd, articleno, ismeter, qnt, 1, ds};
            dtm = (DefaultTableModel) jTable2.getModel();
            dtm.addRow(d);
        }

//            while (j <= q) {
//                
//                String bb = Integer.toString((gg + j));
//                stoke = date1.concat(bb);
//              
//                
//                
//                {
//                Object d[] = {stoke, rate, rt, pds,fr, pd};
//                dtm = (DefaultTableModel) jTable2.getModel();
//                dtm.addRow(d);
//              
//                j++;
//                 
//                }
//              
//              
//            }
    }

    public void rigidcalculation() {
        DecimalFormat df_re = new DecimalFormat("0.00");
        double tot = 0;
        double amnt = Double.parseDouble(jTextField13.getText());
        double disc = Double.parseDouble(jTextField23.getText());
        double dam = amnt - disc;
        jTextField15.setText("" + dam);
        double cgst_amnt = Double.parseDouble(jTextField25.getText());
        double oth = Double.parseDouble(jTextField14.getText());
        double sgst_amnt = Double.parseDouble(jTextField26.getText());
        double igstamnt = Double.parseDouble(jTextField27.getText());

        tot = sgst_amnt + dam + cgst_amnt + oth + igstamnt;
        jTextField16.setText("" + df_re.format(tot));
    }

    public void calculationall() {
        DecimalFormat df_cl = new DecimalFormat("0.00");
        double igst_amnt = 0;
        double sgst_amnt = 0;
        double cgst_amnt = 0;
        double tot = 0;
        
        
        double amnt = Double.parseDouble(jTextField13.getText());
        double discamnt = Double.parseDouble(jTextField23.getText());
//        double discpercc = Double.parseDouble(jTextField11.getText());
//        if (discpercc == 0) {
//            discamnt = Double.parseDouble(jTextField23.getText());
//        } else {
//            discamnt = (discpercc * amnt) / 100;
//        }

        double dam = amnt - discamnt;
        double oth = Double.parseDouble(jTextField14.getText());
        
        double cgst_per = Double.parseDouble(jTextField17.getText());
        cgst_amnt = (dam * cgst_per) / 100;
                
        double sgst_per = Double.parseDouble(jTextField24.getText());
        sgst_amnt = (sgst_per * (dam)) / 100;

        double igst_tax = Double.parseDouble(jTextField1.getText());
        igst_amnt = (dam * igst_tax) / 100;
        
        tot = dam + igst_amnt  + sgst_amnt + cgst_amnt + oth;

        jTextField27.setText("" + df_cl.format(igst_amnt));
//        jTextField23.setText("" + discamnt);
        jTextField15.setText("" + df_cl.format(dam));
        jTextField25.setText("" + df_cl.format(cgst_amnt));
        jTextField26.setText("" + df_cl.format(sgst_amnt));
        jTextField16.setText("" + df_cl.format(tot));

    }


    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed
    ArrayList Prate = new ArrayList();
    ArrayList Retail = new ArrayList();
    ArrayList rdesc = new ArrayList();
    ArrayList byforgation = new ArrayList();
    ArrayList product = new ArrayList();

    public void insert() {
        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
        Vector aDataVector = new Vector(dtm.getDataVector());
        int aNo = Integer.parseInt(jTextField5.getText());
        aNo--;

        SerialNoDataVectorMap.put(aNo + "", aDataVector);

        // Purchasetransaction pt=new Purchasetransaction();
        jDialog1.setVisible(false);
        jComboBox1.requestFocus();
    }
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        insert();

    }//GEN-LAST:event_jButton3ActionPerformed
    int p = 0;

    String Discountaccount = null;
    String updateddisccurrbal = null;
    String updateddisccurrbaltyoe = null;
    double disc_value = 0;
    String disc_type = "";
    double igst_value = 0;
    String igst_type = "";
    double sgst_value = 0;
    String sgst_type = "";
    double cgst_value = 0;
    String cgst_type = "";
    double purc_amnt = 0;
    String purc_type = "";
    double update_igst_value = 0;
    String update_igst_type = "";
    double update_sgst_value = 0;
    String update_sgst_type = "";
    double update_cgst_value = 0;
    String update_cgst_type = "";
    double update_other_value = 0;
    String update_other_type = "";
    double update_creditor_value = 0;
    String update_creditor_type = "";
    double update_cash_value = 0;
    String update_cash_type = "";
    double update_purc_amnt = 0;
    String update_purc_type = "";

// Updateing ledger balances    
    public void updatebalances() {
//---------------For Igst Ledger--------------------------------------------------------------------------
        if (Double.parseDouble(jTextField27.getText()) != 0 && Party_Igst != null) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement igst_st = connect.createStatement();
                ResultSet igst_rs = igst_st.executeQuery("Select * from ledger where ledger_name = '" + Party_Igst + "'");
                while (igst_rs.next()) {
                    igst_value = igst_rs.getDouble("curr_bal");
                    igst_type = igst_rs.getString("currbal_type");
                }

                double igst_amnt = Double.parseDouble(jTextField27.getText());
                double iamnt;

                if (igst_type.equals("CR")) {
                    iamnt = igst_amnt - igst_value;
                    if (iamnt < 0) {
                        update_igst_type = "CR";
                        update_igst_value = Math.abs(iamnt);
                    } else {
                        update_igst_type = "DR";
                        update_igst_value = Math.abs(iamnt);
                    }
                } else {
                    if (igst_type.equals("DR")) {
                        iamnt = igst_amnt + igst_value;
                        if (iamnt >= 0) {
                            update_igst_type = "DR";
                            update_igst_value = Math.abs(iamnt);
                        }
                    }
                }
                igst_st.executeUpdate("Update ledger SET curr_bal= '" + update_igst_value + "',currbal_type='" + update_igst_type + "' where ledger_name = '" + Party_Igst + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//--------------------------------------------------------------------------------------------------------
//-----------------For Sgst ledger------------------------------------------------------------------------
        if (Double.parseDouble(jTextField26.getText()) != 0 && Party_Sgst != null) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement sgst_st = connect.createStatement();
                ResultSet sgst_rs = sgst_st.executeQuery("Select * from ledger where ledger_name = '" + Party_Sgst + "'");
                while (sgst_rs.next()) {
                    sgst_value = sgst_rs.getDouble("curr_bal");
                    sgst_type = sgst_rs.getString("currbal_type");
                }

                double sgst_amnt = Double.parseDouble(jTextField26.getText());
                double samnt;

                if (sgst_type.equals("CR")) {
                    samnt = sgst_amnt - sgst_value;
                    if (samnt < 0) {
                        update_sgst_type = "CR";
                        update_sgst_value = Math.abs(samnt);
                    } else {
                        update_sgst_type = "DR";
                        update_sgst_value = Math.abs(samnt);
                    }
                } else {
                    if (sgst_type.equals("DR")) {
                        samnt = sgst_amnt + sgst_value;
                        if (samnt >= 0) {
                            update_sgst_type = "DR";
                            update_sgst_value = Math.abs(samnt);
                        }
                    }
                }
                sgst_st.executeUpdate("Update ledger SET curr_bal= '" + update_sgst_value + "',currbal_type='" + update_sgst_type + "' where ledger_name = '" + Party_Sgst + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//--------------------------------------------------------------------------------------------------------
//-----------------------For Cgst ledger------------------------------------------------------------------
        if (Double.parseDouble(jTextField25.getText()) != 0 && Party_cgst != null) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement cgst_st = connect.createStatement();
                ResultSet cgst_rs = cgst_st.executeQuery("Select * from ledger where ledger_name = '" + Party_cgst + "'");
                while (cgst_rs.next()) {
                    cgst_value = cgst_rs.getDouble("curr_bal");
                    cgst_type = cgst_rs.getString("currbal_type");
                }

                double cgst_amnt = Double.parseDouble(jTextField25.getText());
                double camnt;

                if (cgst_type.equals("CR")) {
                    camnt = cgst_amnt - cgst_value;
                    if (camnt < 0) {
                        update_cgst_type = "CR";
                        update_cgst_value = Math.abs(camnt);
                    } else {
                        update_cgst_type = "DR";
                        update_cgst_value = Math.abs(camnt);
                    }
                } else {
                    if (cgst_type.equals("DR")) {
                        camnt = cgst_amnt + cgst_value;
                        if (camnt >= 0) {
                            update_cgst_type = "DR";
                            update_cgst_value = Math.abs(camnt);
                        }
                    }
                }
                cgst_st.executeUpdate("Update ledger SET curr_bal= '" + update_cgst_value + "',currbal_type='" + update_cgst_type + "' where ledger_name = '" + Party_cgst + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//--------------------------------------------------------------------------------------------------------
//-----------------For Other Charges Ledger---------------------------------------------------------------
        if ((Double.parseDouble(jTextField14.getText()) == 0) && (other_charges == null)) {

        } else if ((Double.parseDouble(jTextField14.getText()) != 0) && (other_charges == null)) {
            JOptionPane.showMessageDialog(rootPane, "Please choose Direct expenses account");
            jTextField14.requestFocus();
        } else {
            try {
                double othercurrbal = 0;
                String Curbaltypeother = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement other_st = connect.createStatement();
                ResultSet other_rs = other_st.executeQuery("Select * from ledger where ledger_name = '" + other_charges + "'");

                while (other_rs.next()) {
                    othercurrbal = other_rs.getDouble("curr_bal");
                    Curbaltypeother = other_rs.getString("currbal_type");
                }
                double otheramount = Double.parseDouble(jTextField14.getText());
                double aother;

                if (Curbaltypeother.equals("CR")) {
                    aother = otheramount - othercurrbal;
                    if (aother < 0) {
                        update_other_type = "CR";
                        update_other_value = Math.abs(aother);

                    } else {
                        update_other_type = "DR";
                        update_other_value = Math.abs(aother);
                    }

                } else {
                    if (Curbaltypeother.equals("DR")) {
                        aother = otheramount + othercurrbal;
                        if (aother >= 0) {
                            update_other_type = "DR";
                            update_other_value = Math.abs(aother);
                        }
                    }
                }

                other_st.executeUpdate("Update ledger SET curr_bal= '" + update_other_value + "',currbal_type='" + update_other_type + "' where ledger_name='" + other_charges + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//--------------------------------------------------------------------------------------------------------
//---------------For Creditor and cash ledger-------------------------------------------------------------
        if (jRadioButton3.isSelected()) {
            try {
                double cashcurrbal = 0;
                String Curbaltypecash = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement cash_st = connect.createStatement();
                ResultSet cash_rs = cash_st.executeQuery("Select * from ledger where ledger_name = 'CASH'");

                while (cash_rs.next()) {
                    cashcurrbal = cash_rs.getDouble("curr_bal");
                    Curbaltypecash = cash_rs.getString("currbal_type");
                }
                double cash_amount = Double.parseDouble(jTextField16.getText());
                double cash_amnt = 0;

                if (Curbaltypecash.equals("CR")) {
                    cash_amnt = cashcurrbal + cash_amount;
                    update_cash_value = Math.abs(cash_amnt);
                } else if (Curbaltypecash.equals("DR")) {
                    if (cash_amnt > cash_amount) {
                        cash_amnt = cashcurrbal - cash_amount;
                        update_cash_value = Math.abs(cash_amnt);
                    }
                    if (cash_amnt < cash_amount) {
                        cash_amnt = cash_amount - cashcurrbal;
                        update_cash_value = Math.abs(cash_amnt);
                        update_cash_type = "CR";
                    }
                }

                cash_st.executeUpdate("Update ledger SET curr_bal= '" + update_cash_value + "',currbal_type='" + update_cash_type + "' where ledger_name='CASH'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (jRadioButton4.isSelected()) {
            try {
                double creditcurrbal = 0;
                String Curbaltypecradit = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement craditor_st = connect.createStatement();
                ResultSet cred_rs = craditor_st.executeQuery("Select * from ledger where ledger_name = '" + jTextField12.getText() + "'");

                while (cred_rs.next()) {
                    creditcurrbal = cred_rs.getDouble("curr_bal");
                    Curbaltypecradit = cred_rs.getString("currbal_type");
                }
                double creditamount = Double.parseDouble(jTextField16.getText());
                double acredit;

                if (Curbaltypecradit.equals("DR")) {
                    acredit = creditamount - creditcurrbal;
                    if (acredit <= 0) {
                        update_creditor_type = "DR";
                        update_creditor_value = Math.abs(acredit);
                    } else {
                        update_creditor_type = "CR";
                        update_creditor_value = Math.abs(acredit);
                    }

                } else {
                    if (Curbaltypecradit.equals("CR")) {
                        acredit = creditamount + creditcurrbal;
                        if (acredit >= 0) {
                            update_creditor_type = "CR";
                            update_creditor_value = Math.abs(acredit);
                        }
                    }
                }
                craditor_st.executeUpdate("Update ledger SET curr_bal= '" + update_creditor_value + "',currbal_type='" + update_creditor_type + "' where ledger_name='" + jTextField12.getText() + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//--------------------------------------------------------------------------------------------------------
//-----------------For Purchase account ledger------------------------------------------------------------
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement pur_st = connect.createStatement();

            String purc_account = jLabel48.getText();
            double purc_currbal = Double.parseDouble(jTextField15.getText());

            ResultSet pur_rs = pur_st.executeQuery("Select * from ledger where ledger_name = '" + purc_account + "'");
            while (pur_rs.next()) {
                purc_type = pur_rs.getString("currbal_type");
                purc_amnt = pur_rs.getDouble("curr_bal");
            }

            double purchase_amnt = purc_amnt;
            double purchase_currbal = purc_currbal;
            double pur_bal = 0;

            if (purc_type.equals("CR")) {
                pur_bal = purchase_currbal - purchase_amnt;
                if (pur_bal <= 0) {
                    update_purc_type = "CR";
                    update_purc_amnt = Math.abs(pur_bal);
                } else {
                    update_purc_type = "DR";
                    update_purc_amnt = Math.abs(pur_bal);
                }

            } else {
                if (purc_type.equals("DR")) {
                    pur_bal = purchase_currbal + purchase_amnt;
                    if (pur_bal >= 0) {
                        update_purc_type = "DR";
                        update_purc_amnt = Math.abs(pur_bal);
                    }
                }
            }
            pur_st.executeUpdate("Update ledger SET curr_bal= '" + update_purc_amnt + "',currbal_type='" + update_purc_type + "' where ledger_name='" + purc_account + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }
//--------------------------------------------------------------------------------------------------------
    }

//    public void updatebalances() {
//        if ((Double.parseDouble(jTextField26.getText()) == 0) && (Party_vat == null)) {
//
//        } else if ((Double.parseDouble(jTextField26.getText()) != 0) && (Party_vat == null)) {
//            JOptionPane.showMessageDialog(rootPane, "Please choose vat account");
//        } else {
////for sgst 
//            String vatcurrbal = jLabel38.getText();
//            String Curbaltypevat = jLabel39.getText();
//            double vatcurrbal1 = Double.parseDouble(vatcurrbal);
//            double vatamount = Double.parseDouble(jTextField26.getText());
//            double avat;
//            if (Curbaltypevat.equals("CR")) {
//                avat = vatamount - vatcurrbal1;
//                if (avat < 0) {
//                    jLabel39.setText("CR");
//                    jLabel38.setText("" + String.valueOf(Math.abs(avat)));
//                } else {
//                    jLabel39.setText("DR");
//                    jLabel38.setText("" + String.valueOf(Math.abs(avat)));
//                }
//            } else {
//                if (Curbaltypevat.equals("DR")) {
//                    avat = vatamount + vatcurrbal1;
//                    if (avat > 0) {
//                        jLabel39.setText("DR");
//                        jLabel38.setText("" + String.valueOf(Math.abs(avat)));
//                    }
//                }
//            }
//        }
//        if ((Double.parseDouble(jTextField25.getText()) == 0) && (Party_cst == null)) {
//        } else if ((Double.parseDouble(jTextField25.getText()) != 0) && (Party_cst == null)) {
//            JOptionPane.showMessageDialog(rootPane, "Please choose cst account");
//        } else {
////for cgst
//            String cstcurrbal = jLabel43.getText();
//            String Curbaltypecst = jLabel44.getText();
//            double cstcurrbal1 = Double.parseDouble(cstcurrbal);
//            double cstamount = Double.parseDouble(jTextField25.getText());
//            double acst;
//            if (Curbaltypecst.equals("CR")) {
//                acst = cstamount - cstcurrbal1;
//                if (acst < 0) {
//                    jLabel44.setText("CR");
//                    jLabel43.setText("" + String.valueOf(Math.abs(acst)));
//                } else {
//                    jLabel44.setText("DR");
//                    jLabel43.setText("" + String.valueOf(Math.abs(acst)));
//                }
//            } else {
//                if (Curbaltypecst.equals("DR")) {
//                    acst = cstamount + cstcurrbal1;
//                    if (acst > 0) {
//                        jLabel44.setText("DR");
//                        jLabel43.setText("" + String.valueOf(Math.abs(acst)));
//                    }
//                }
//            }
//        }
//        
//        //for igst        
//        
//            if ((Double.parseDouble(jTextField27.getText()) == 0) && (Party_Igst == null)) {
//        } else if ((Double.parseDouble(jTextField27.getText()) != 0) && (Party_Igst == null)) {
//            JOptionPane.showMessageDialog(rootPane, "Please choose IGST account");
//        } else {
////for igst 
//            String vatcurrbal = jLabel51.getText();
//            String Curbaltypevat = jLabel52.getText();
//            double vatcurrbal1 = Double.parseDouble(vatcurrbal);
//            double vatamount = Double.parseDouble(jTextField27.getText());
//            double avat;
//            if (Curbaltypevat.equals("CR")) {
//                avat = vatamount - vatcurrbal1;
//                if (avat < 0) {
//                    jLabel52.setText("CR");
//                    jLabel51.setText("" + String.valueOf(Math.abs(avat)));
//                } else {
//                    jLabel52.setText("DR");
//                    jLabel51.setText("" + String.valueOf(Math.abs(avat)));
//                }
//            } else {
//                if (Curbaltypevat.equals("DR")) {
//                    avat = vatamount + vatcurrbal1;
//                    if (avat > 0) {
//                        jLabel52.setText("DR");
//                        jLabel51.setText("" + String.valueOf(Math.abs(avat)));
//                    }
//                }
//            }
//        }
//
////for discount
//        String disc_currbal = null;
//        String disc_currbaltype = null;
//        try {
//            connection c = new connection();
//            Connection connect = c.cone();
//            Statement st = connect.createStatement();
//            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='Discount Account'");
//            while (rs.next()) {
//                Discountaccount = rs.getString(1);
//                disc_currbal = rs.getString(10);
//                disc_currbaltype = rs.getString(11);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////for discount
//
//        if ((Double.parseDouble(jTextField23.getText()) != 0) && (Discountaccount == null)) {
//            JOptionPane.showMessageDialog(rootPane, "Please choose Discount account");
//        } else {
//
//            String disccurrbal = disc_currbal;
//            String Curbaltypedisc = disc_currbaltype;
//            double othercurrbal1 = Double.parseDouble(disccurrbal);
//            double discamount = Double.parseDouble(jTextField23.getText());
//            double adisc;
//
//            if (Curbaltypedisc.equals("DR")) {
//                adisc = discamount - othercurrbal1;
//                if (adisc <= 0) {
//                    updateddisccurrbaltyoe = "DR";
//                    updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
//
//                } else {
//                    updateddisccurrbaltyoe = ("CR");
//                    updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
//                }
//
//            } else {
//                if (Curbaltypedisc.equals("CR")) {
//                    adisc = discamount + othercurrbal1;
//                    if (adisc >= 0) {
//                        updateddisccurrbaltyoe = ("CR");
//                        updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
//                    }
//                }
//
//            }
//        }
//
////for other charges
//        if ((Double.parseDouble(jTextField14.getText()) == 0) && (other_charges == null)) {
//
//        } else if ((Double.parseDouble(jTextField14.getText()) != 0) && (other_charges == null)) {
//            JOptionPane.showMessageDialog(rootPane, "Please choose Direct expenses account");
//            jTextField14.requestFocus();
//        } else {
//
//            String othercurrbal = jLabel45.getText();
//            String Curbaltypeother = jLabel46.getText();
//            double othercurrbal1 = Double.parseDouble(othercurrbal);
//            double otheramount = Double.parseDouble(jTextField14.getText());
//            double aother;
//
//            if (Curbaltypeother.equals("CR")) {
//                aother = otheramount - othercurrbal1;
//                if (aother < 0) {
//                    jLabel46.setText("CR");
//                    jLabel45.setText("" + String.valueOf(Math.abs(aother)));
//
//                } else {
//                    jLabel46.setText("DR");
//                    jLabel45.setText("" + String.valueOf(Math.abs(aother)));
//                }
//
//            } else {
//                if (Curbaltypeother.equals("DR")) {
//                    aother = otheramount + othercurrbal1;
//                    if (aother > 0) {
//                        jLabel46.setText("DR");
//                        jLabel45.setText("" + String.valueOf(Math.abs(aother)));
//                    }
//                }
//
//            }
//        }
//
//        if (jRadioButton4.isSelected()) {
////purchase and creditor         
//
//            System.out.println("code is running1");
//            String purchaseamount = jLabel31.getText();
//            String creditoramount = jLabel29.getText();
//            double totalamount = Double.parseDouble(jTextField16.getText());
//            double purchaseamount1 = Double.parseDouble(purchaseamount);
//            double creditoramount1 = Double.parseDouble(creditoramount);
//            double totalamountaftervat = totalamount - Double.parseDouble(jTextField26.getText());
//            double bc = 0;
//            if (jLabel32.getText().equals("DR")) {
//
//                purchaseamount1 = purchaseamount1 + Double.parseDouble(jTextField13.getText());
//                jLabel31.setText("" + purchaseamount1);
//            } else if (jLabel32.getText().equals("CR")) {
//                if (purchaseamount1 > Double.parseDouble(jTextField13.getText())) {
//                    bc = (purchaseamount1 - Double.parseDouble(jTextField13.getText()));
//                    jLabel31.setText("" + bc);
//                }
//                if (Double.parseDouble(jTextField13.getText()) > purchaseamount1) {
//                    bc = (Double.parseDouble(jTextField13.getText()) - purchaseamount1);
//                    jLabel31.setText("" + bc);
//                    jLabel32.setText("DR");
//                }
//            }
//
//            if (jLabel30.getText().equals("CR")) {
//                creditoramount1 = creditoramount1 + totalamount;
//                jLabel29.setText("" + creditoramount1);
//            } else if (jLabel30.getText().equals("DR")) {
//                if (creditoramount1 > totalamount) {
//                    creditoramount1 = creditoramount1 - totalamount;
//                    jLabel29.setText("" + creditoramount1);
//                } else if (creditoramount1 <= totalamount) {
//                    creditoramount1 = totalamount - creditoramount1;
//                    jLabel29.setText("" + creditoramount1);
//                    jLabel30.setText("CR");
//                }
//            }
//
//        }
//
//        if (jRadioButton3.isSelected()) {
////purchase and cash
//
//            System.out.println("code is running2");
//            String purchaseamount = jLabel31.getText();
//            String cashamount = jLabel29.getText();
//            double totalamount = Double.parseDouble(jTextField16.getText());
//            double purchaseamount1 = Double.parseDouble(purchaseamount);
//            double cashamount1 = Double.parseDouble(cashamount);
//            double totalamountaftervat = totalamount - Double.parseDouble(jTextField26.getText());
//            double bc = 0;
//            if (jLabel32.getText().equals("DR")) {
//                purchaseamount1 = purchaseamount1 + Double.parseDouble(jTextField13.getText());
//                jLabel31.setText("" + purchaseamount1);
//            } else if (jLabel32.getText().equals("CR")) {
//                if (purchaseamount1 > Double.parseDouble(jTextField13.getText())) {
//                    bc = (purchaseamount1 - Double.parseDouble(jTextField13.getText()));
//                    jLabel31.setText("" + bc);
//                }
//                if (Double.parseDouble(jTextField13.getText()) > purchaseamount1) {
//                    bc = (Double.parseDouble(jTextField13.getText()) - purchaseamount1);
//                    jLabel31.setText("" + bc);
//                    jLabel32.setText("DR");
//                }
//            }
//
//            if (jLabel30.getText().equals("CR")) {
//                cashamount1 = cashamount1 + totalamount;
//                jLabel29.setText("" + cashamount1);
//            } else if (jLabel30.getText().equals("DR")) {
//                if (cashamount1 > totalamount) {
//                    cashamount1 = cashamount1 - totalamount;
//                    jLabel29.setText("" + cashamount1);
//                }
//                if (cashamount1 < totalamount) {
//                    cashamount1 = totalamount - cashamount1;
//                    jLabel29.setText("" + cashamount1);
//                    jLabel30.setText("CR");
//                }
//            }
//        }
//
//    }
    boolean issave = false;

    public void save() {
        updatebalances();
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            String partypurchasedate = "";
            int j = 0;
            int check = 0;
            if (jDateChooser1.getDate() == null) {

            } else {
                partypurchasedate = formatter.format(jDateChooser1.getDate());
            }

            int rcount = jTable1.getRowCount();
            String to_ledger = "";
            if (purch.equals("Cash")) {
                to_ledger = "CASH";
            } else if (purch.equals("Credit")) {
                to_ledger = jTextField12.getText();
            }
            while (j < incr) {
                if (jRadioButton4.isSelected()) {
                    String sql = "insert into stock values('" + jTextField22.getText() + "','" + jTextField12.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + (jTextField2.getText()) + "','" + purch + "','" + jTextField4.getText() + "','" + sno[j] + "','" + qty[j] + "','" + rat[j] + "','" + amoun[j] + "','" + pdes[j] + "','" + retail[j] + "','" + desc[j] + "','" + ftr[j] + "','" + prod[j] + "','" + jTextField13.getText() + "','" + jTextField11.getText() + "','" + jTextField15.getText() + "','" + jTextField17.getText() + "','" + jTextField14.getText() + "','" + jTextField16.getText() + "','" + jTextField24.getText() + "','" + jTextField26.getText() + "','" + jTextField25.getText() + "','" + Party_Sgst + "','" + jTextField12.getText() + "','" + Purchaseaccount + "','" + partypurchasedate + "','" + articlenum[j] + "','" + jTextField28.getText() + "','" + samebarcode1[j] + "','" + Party_cgst + "','" + other_charges + "','0','" + jTextField27.getText() + "','" + Party_Igst + "')";
                    check = stmt.executeUpdate(sql);
                    ++j;
                }
            }

            if (SerialNoDataVectorMap != null && SerialNoDataVectorMap.size() > 0) {
                String aSNo = "";
                Vector aDataVector = null;
                Iterator aSerialNoIterator = SerialNoDataVectorMap.keySet().iterator();
                while (aSerialNoIterator.hasNext()) {
                    aSNo = (String) aSerialNoIterator.next();
                    aDataVector = (Vector) SerialNoDataVectorMap.get(aSNo);
                    if (aDataVector != null) {

                        int rowcount = aDataVector.size();
                        Vector aRowVector = null;

                        for (int rowIndex = 0; rowIndex < rowcount; rowIndex++) {
                            aRowVector = (Vector) aDataVector.get(rowIndex);
                            String stooock = (aRowVector.get(1)) + "";
                            String purrate = (String) aRowVector.get(2);
                            //  Prate.add(i, purrate);
                            String retail1 = (String) aRowVector.get(3);
                            //Retail.add(i, retail1);
                            String Rdesc = (String) aRowVector.get(4);
                            //rdesc.add(i, Rdesc);
                            String biforgation = (String) aRowVector.get(5);
                            //byforgation.add(i, biforgation);
                            String Product1 = (String) aRowVector.get(6);
                            String article = (String) aRowVector.get(7);
                            int ismeter = Integer.parseInt(aRowVector.get(8) + "");
                            double qnty = Double.parseDouble(aRowVector.get(9) + "");
                            String nooftag = (String) (aRowVector.get(10) + "");
                            String Desc = (String) (aRowVector.get(11) + "");
                            //product.add(i, Product1);

                            try {
                                stmt.executeUpdate("insert into Stockid values('" + jTextField2.getText() + "','" + stooock + "','" + purrate + "','" + retail1 + "','" + Rdesc + "','" + Desc + "','" + biforgation + "','" + Product1 + "','" + jTextField22.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + article + "','" + aSNo + "','0'," + ismeter + "," + qnty + ",'" + nooftag + "')");
                                stmt.executeUpdate("insert into Stockid2 values('" + stooock + "','" + purrate + "','" + retail1 + "','" + Rdesc + "','" + Desc + "','" + biforgation + "','" + Product1 + "','" + article + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField22.getText() + "','0'," + ismeter + "," + qnty + ")");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            if (check == 1) {
                JOptionPane.showMessageDialog(null, "Inserted Sucessfully.");
            } else {
                JOptionPane.showMessageDialog(null, "DATA IS NOT SAVED.");
            }

            issave = true;
            f = 0;
            s = 1;
            stoke = null;

            first = 0;
            second = 0;
            famount = 0;
            mount = 0;
            fvat = 0;
            ffvat = 0;

            dispose();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //for data insertion 
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i == 0 && issave == false) {
            save();
        }
//        if (i == 0 && issave == false) {          
//            updatebalances();
//            s = 0;
//            try {
//
//                Statement st = null;
//                Statement st1 = null;
//                ResultSet rs = null;
//                PreparedStatement pstm = null;
//                int j = 0;
//                int check = 0;
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement stmt = connect.createStatement();
//                String partypurchasedate = "";
//                if (jDateChooser1.getDate() == null) {
//
//                } else {
//                    partypurchasedate = formatter.format(jDateChooser1.getDate());
//                }
//
//                while (j < incr) {
//                    try {
////                   
//                        if (jRadioButton4.isSelected()) {
//                            String sql = "insert into stock values('" + jTextField22.getText() + "','" + jTextField12.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + (jTextField2.getText()) + "','" + purch + "','" + jTextField4.getText() + "','" + sno[j] + "','" + qty[j] + "','" + rat[j] + "','" + amoun[j] + "','" + pdes[j] + "','" + retail[j] + "','" + desc[j] + "','" + ftr[j] + "','" + prod[j] + "','" + jTextField13.getText() + "','" + jTextField11.getText() + "','" + jTextField15.getText() + "','" + jTextField17.getText() + "','" + jTextField14.getText() + "','" + jTextField16.getText() + "','" + jTextField24.getText() + "','" + jTextField26.getText() + "','" + jTextField25.getText() + "','" + Party_vat + "','" + jTextField12.getText() + "','" + Purchaseaccount + "','" + partypurchasedate + "','" + articlenum[j] + "','" + jTextField28.getText() + "','" + samebarcode1[j] + "','" + Party_cst + "','" + other_charges + "','0','" + jTextField27.getText() + "','" + Party_Igst + "')";
//                            check = stmt.executeUpdate(sql);
//                            ++j;
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (SerialNoDataVectorMap != null && SerialNoDataVectorMap.size() > 0) {
//                    String aSNo = "";
//                    Vector aDataVector = null;
//                    Iterator aSerialNoIterator = SerialNoDataVectorMap.keySet().iterator();
//                    while (aSerialNoIterator.hasNext()) {
//                        aSNo = (String) aSerialNoIterator.next();
//                        aDataVector = (Vector) SerialNoDataVectorMap.get(aSNo);
//                        if (aDataVector != null) {
//
//                            int rowcount = aDataVector.size();
//                            Vector aRowVector = null;
//
//                            for (int rowIndex = 0; rowIndex < rowcount; rowIndex++) {
//                                aRowVector = (Vector) aDataVector.get(rowIndex);
//                                String stooock = (aRowVector.get(1)) + "";
//                                String purrate = (String) aRowVector.get(2);
//                                //  Prate.add(i, purrate);
//                                String retail1 = (String) aRowVector.get(3);
//                                //Retail.add(i, retail1);
//                                String Rdesc = (String) aRowVector.get(4);
//                                //rdesc.add(i, Rdesc);
//                                String biforgation = (String) aRowVector.get(5);
//                                //byforgation.add(i, biforgation);
//                                String Product1 = (String) aRowVector.get(6);
//                                String article = (String) aRowVector.get(7);
//                                int ismeter = Integer.parseInt(aRowVector.get(8) + "");
//                                double qnty = Double.parseDouble(aRowVector.get(9) + "");
//                                String nooftag = (String) (aRowVector.get(10) + "");
//                                String Desc = (String) (aRowVector.get(11) + "");
//                                //product.add(i, Product1);
//
//                                try {
//                                    stmt.executeUpdate("insert into Stockid values('" + jTextField2.getText() + "','" + stooock + "','" + purrate + "','" + retail1 + "','" + Rdesc + "','" + Desc + "','" + biforgation + "','" + Product1 + "','" + jTextField22.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + article + "','" + aSNo + "','0'," + ismeter + "," + qnty + ",'" + nooftag + "')");
//                                    stmt.executeUpdate("insert into Stockid2 values('" + stooock + "','" + purrate + "','" + retail1 + "','" + Rdesc + "','" + Desc + "','" + biforgation + "','" + Product1 + "','" + article + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField22.getText() + "','0'," + ismeter + "," + qnty + ")");
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                    }
//                }
//
//                if (check == 1) {
//                    JOptionPane.showMessageDialog(null, "Inserted Sucessfully.");
//                } else {
//                    JOptionPane.showMessageDialog(null, "DATA IS NOT SAVED.");
//                }
//
//                if (jRadioButton4.isSelected()) {
//                    try {
//                        System.out.println("code is running4 ledger update");
//                        String qdd = "Update ledger SET curr_bal= '" + jLabel29.getText() + "',currbal_type='" + jLabel30.getText() + "' where ledger_name='" + jTextField12.getText() + "'";
//                        stmt.executeUpdate(qdd);
//
//                        String qd = "Update ledger SET curr_bal= '" + jLabel31.getText() + "',currbal_type='" + jLabel32.getText() + "' where ledger_name='" + Purchaseaccount + "'";
//                        stmt.executeUpdate(qd);
//                    } catch (Exception e) {
//                        System.out.println("" + e);
//                        e.printStackTrace();
//                    }
//                }
//
//                if (jRadioButton3.isSelected()) {
//                    try {
//                        String a = "Cash";
//
//                        String qd = "Update ledger SET curr_bal= '" + jLabel31.getText() + "',currbal_type='" + jLabel32.getText() + "' where ledger_name='" + a + "'";
//                        stmt.executeUpdate(qd);
//
//                        String qdd = "Update ledger SET curr_bal= '" + jLabel29.getText() + "',currbal_type='" + jLabel30.getText() + "' where ledger_name='" + Purchaseaccount + "'";
//                        stmt.executeUpdate(qdd);
//                    } catch (Exception e) {
//                        System.out.println("" + e);
//                        e.printStackTrace();
//                    }
//
//                }
//                try {
//
//                    String against = "insert into againstpayment values('" + jTextField2.getText() + "','" + jTextField4.getText() + "','" + formatter.format(jDateChooser2.getDate()) + "','" + jTextField12.getText() + "','" + Purchaseaccount + "','" + jTextField16.getText() + "','purchase','')";
//                    stmt.executeUpdate(against);
//
//                    if ((Party_vat == null) && (Double.parseDouble(jTextField26.getText()) == 0)) {
//
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + jLabel38.getText() + "',currbal_type='" + jLabel39.getText() + "' where ledger_name='" + Party_vat + "'");
//                        stmt.executeUpdate(qddvat);
//
//                    }
//                } catch (Exception e) {
//                    System.out.println("" + e);
//                    e.printStackTrace();
//                }
//                try {
//                    if ((Double.parseDouble(jTextField23.getText()) != 0) && (Discountaccount == null)) {
//                        JOptionPane.showMessageDialog(rootPane, "Please choose Disount account");
//                    } else if ((Double.parseDouble(jTextField23.getText()) == 0) && (Discountaccount != null)) {
//
//                    } else {
//                        String qdddisc = ("Update ledger SET curr_bal= '" + updateddisccurrbal + "',currbal_type='" + updateddisccurrbaltyoe + "' where ledger_name='" + Discountaccount + "'");
//                        stmt.executeUpdate(qdddisc);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                try {
//                    if ((Party_cst == null) && (Double.parseDouble(jTextField25.getText()) == 0)) {
//
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + jLabel43.getText() + "',currbal_type='" + jLabel44.getText() + "' where ledger_name='" + Party_cst + "'");
//                        stmt.executeUpdate(qddvat);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                try {
//                    if ((Party_Igst == null) && (Double.parseDouble(jTextField27.getText()) == 0)) {
//
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + jLabel51.getText() + "',currbal_type='" + jLabel52.getText() + "' where ledger_name='" + Party_Igst + "'");
//                        stmt.executeUpdate(qddvat);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                try {
//                    if ((Double.parseDouble(jTextField14.getText()) == 0) && (other_charges == null)) {
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + jLabel45.getText() + "',currbal_type='" + jLabel46.getText() + "' where ledger_name='" + other_charges + "'");
//                        stmt.executeUpdate(qddvat);
//                    }
//                } catch (Exception e) {
//                    System.out.println("" + e);
//                    e.printStackTrace();
//                }
//                  issave = true;
//                f = 0;
//                s = 1;
//                stoke = null;
//
//                first = 0;
//                second = 0;
//                famount = 0;
//                mount = 0;
//                fvat = 0;
//                ffvat = 0;
//
//                dispose();
//
////new Purchasetransaction().setVisible(true);
//                //           jDialog5.setVisible(true);
//            } catch (SQLException ex) {
//                ex.printStackTrace();
//            }
////            
//        }
//  

//           jDialog5.setVisible(true);

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed

    }//GEN-LAST:event_jButton1KeyPressed

    private void jTextField16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField16KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField14.requestFocus();
        }

    }//GEN-LAST:event_jTextField16KeyPressed

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2KeyPressed
    int stok[] = new int[10000];
    static int counter = 0;
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        insert();
    }//GEN-LAST:event_jButton3KeyPressed

    private void jTextField14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField14KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jTextField16.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField26.requestFocus();
        }

    }//GEN-LAST:event_jTextField14KeyPressed

    private void jTextField14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField14ActionPerformed

    private void jTextField13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField13KeyPressed
        // TODO add your handling code here:

        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
//            jComboBox3.requestFocus();
        }
    }//GEN-LAST:event_jTextField13KeyPressed

    private void jTextField11KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField11KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            jComboBox3.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }
    }//GEN-LAST:event_jTextField11KeyPressed

    private void jTextField15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField15KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField17.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField15KeyPressed

    private void jTextField17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField17KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
            cstbefore = Double.parseDouble(jTextField17.getText());

            if (Double.parseDouble(jTextField17.getText()) != 0) {
                jDialog5.setVisible(true);
                list3.requestFocus();
                list3.removeAll();
                list3.addItem("ADD NEW");
                try {
                    connection c = new connection();
                    Connection connect = c.cone();

                    Statement st = connect.createStatement();
                    Statement st1 = connect.createStatement();
                    String duties = null;
                    String Duteis = "DUTIES AND TAXES";
                    //    String typeoftax = "OTHER";
                    ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
                    while (rs1.next()) {
                        duties = rs1.getString(1);
                    }
                    ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = 'CGST' ");
                    while (rs.next()) {
                        list3.add(rs.getString(1));
                    }
                } catch (SQLException sqe) {
                    System.out.println("SQl error");
                    sqe.printStackTrace();
                }

//                hide();
            }
//             jTextField25.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField15.requestFocus();
        }
    }//GEN-LAST:event_jTextField17KeyPressed

    private void jComboBox4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox4KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox1.requestFocus();

        }
        if (key == evt.VK_ESCAPE) {
            jTextField10.requestFocus();
        }
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_F2) {
            master.color.byforgation byf = new master.color.byforgation();
            byf.setVisible(true);

        }

    }//GEN-LAST:event_jComboBox4KeyPressed

    private void jTextField19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField19ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField19ActionPerformed

    private void jTextField18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField18ActionPerformed

    private void jTextField19KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField19KeyPressed


    }//GEN-LAST:event_jTextField19KeyPressed

    private void jTextField18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField18KeyPressed

        int key = evt.getKeyCode();

        if (key == evt.VK_DOWN) {
            jTable3.requestFocus();

        }
    }//GEN-LAST:event_jTextField18KeyPressed

    private void jTextField20KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField20KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField20KeyPressed

    private void jTextField21KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField21KeyPressed
        //jButton4.requestFocus();
    }//GEN-LAST:event_jTextField21KeyPressed

    private void jComboBox4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox4ItemStateChanged

    }//GEN-LAST:event_jComboBox4ItemStateChanged

    private void jTextField16FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField16FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField16FocusLost

    private void jTextField5FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField5FocusGained

    }//GEN-LAST:event_jTextField5FocusGained

    private void jTextField22FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField22FocusLost
        // TODO add your handling code here: ResultSet rs;
    }//GEN-LAST:event_jTextField22FocusLost
    public int markup = 0;

    public void partyselect() {
        String GST = "True";
        DefaultTableModel dtm = null;
        dtm = (DefaultTableModel) jTable3.getModel();
        row = jTable3.getSelectedRow();
        int coloum = jTable3.getSelectedColumn();
        jTextField22.setText("" + dtm.getValueAt(row, 0));
        jTextField12.setText("" + dtm.getValueAt(row, 1));
        Purchaseaccount = "" + dtm.getValueAt(row, 7);
        jLabel48.setText("" + dtm.getValueAt(row, 7));
        if ("".equals("" + dtm.getValueAt(row, 6)) || "" + dtm.getValueAt(row, 6) == null || "null".equals("" + dtm.getValueAt(row, 6))) {
            markup = 0;
        } else {
            markup = Integer.parseInt("" + dtm.getValueAt(row, 6));
        }

        jDialog2.setVisible(false);
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='" + (String) jTable3.getValueAt(row, 1) + "'");
            while (rs.next()) {
                jLabel29.setText(rs.getString(10));
                jLabel30.setText(rs.getString(11));
            }

            ResultSet rs1 = st1.executeQuery("Select * from ledger where ledger_name='" + Purchaseaccount + "' ");
            while (rs1.next()) {

                jLabel31.setText("" + rs1.getString(10));
                jLabel32.setText("" + rs1.getString(11));

            }

            if (GST.equalsIgnoreCase((String) jTable3.getValueAt(row, 8))) {
                jTextField17.setEnabled(false);
                jTextField25.setEnabled(false);
                jTextField24.setEnabled(false);
                jTextField26.setEnabled(false);
                jTextField1.setEnabled(true);
                jTextField27.setEnabled(true);
                
                jTextField17.setText("0");
                jTextField25.setText("0");
                jTextField24.setText("0");
                jTextField26.setText("0");
                Party_Sgst = null;
                Party_cgst = null;
                
                
            } else if (!GST.equalsIgnoreCase((String) jTable3.getValueAt(row, 8))) {
                jTextField1.setEnabled(false);
                jTextField27.setEnabled(false);

                jTextField17.setEnabled(true);
                jTextField25.setEnabled(true);
                jTextField24.setEnabled(true);
                jTextField26.setEnabled(true);
                
                jTextField1.setText("0");
                jTextField27.setText("0");
                Party_Igst = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!(jTextField22.getText().isEmpty())) {
            jRadioButton3.setEnabled(true);
            jRadioButton4.setEnabled(true);
            jRadioButton4.setSelected(true);
            if (Purchaseaccount == null) {
                JOptionPane.showMessageDialog(rootPane, "please select Purchase account");
            } else {
                jDateChooser2.setEnabled(true);
                jTextField2.setEnabled(true);
                jTextField3.setEnabled(true);
                jTextField4.setEnabled(true);
                jTextField5.setEnabled(true);
                jTextField6.setEnabled(true);
                jTextField7.setEnabled(true);
                jTextField8.setEnabled(true);
                jTextField9.setEnabled(true);
                jTextField11.setEnabled(true);
                jTextField12.setEnabled(true);
                jTextField13.setEnabled(true);
                jTextField14.setEnabled(true);
                jTextField15.setEnabled(true);
                jTextField16.setEnabled(true);
                jTextField10.setEnabled(true);
                // jTextField17.setEnabled(true);
                jTextField23.setEnabled(true);
                jComboBox3.setEnabled(true);
                jComboBox4.setEnabled(true);
                jComboBox1.setEnabled(true);
            }

        }
    }


    private void jTextField22KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField22KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jDateChooser2.requestFocus();
        }

    }//GEN-LAST:event_jTextField22KeyPressed
    static int row = 0;
    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        partyselect();
    }//GEN-LAST:event_jTable3MouseClicked
    String IGSTaccount = "";
    String SGSTaccount = "";
    String CGSTaccount = "";
    private void jTextField3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField3FocusLost
        // TODO add your handling code here:

        String rate = jTextField3.getText();

        if (!rate.matches("[0-9]+$") || jTextField3.getText().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Number only");
            jTextField3.requestFocus();
        } else {
            double qun_tity = Double.parseDouble(jTextField6.getText());
            double p = Double.parseDouble(jTextField3.getText());
            double q = qun_tity;
            double amou_nt = (p * q);
            DecimalFormat df = new DecimalFormat("0.00");
            String amount = df.format(amou_nt);
            jTextField7.setText(amount);
            double discountpercent = Double.parseDouble(jTextField11.getText());
            double purchaseamnt = Double.parseDouble(jTextField3.getText());
            double purchaseamntafterdiscount = purchaseamnt - ((discountpercent / 100) * purchaseamnt);

            double taxpercent = 5;
            double taxamnt = (taxpercent / 100) * purchaseamntafterdiscount;

            System.out.println("purchase amont after discount is" + purchaseamntafterdiscount);
            System.out.println("tax amount is" + taxamnt);
            double purchasenetamnt = taxamnt + purchaseamntafterdiscount;
            System.out.println("purchase net amnt is" + purchasenetamnt);
            purchasenetamnt = Math.round(purchasenetamnt);
            jTextField8.setText(purchasenetamnt + "");
            //	String t = Integer.toString(amou_nt);
        }
        markupmethod();
    }//GEN-LAST:event_jTextField3FocusLost
    public void markupmethod() {
        double rate = Double.parseDouble(jTextField3.getText());
        double percentamount = ((double) markup / 100) * rate;
        double intialretailamount = rate + percentamount;
        double finalretailamount = 10 * (Math.ceil(Math.abs(intialretailamount / 10)));
        jTextField9.setText("" + (int) finalretailamount);
        jTextField9.requestFocus();
    }


    private void jTextField15FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField15FocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_jTextField15FocusGained
    double afterdisc = 0;
    private void jTextField11FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField11FocusLost
        // TODO add your handling code here:
        afterdisc = Double.parseDouble(jTextField11.getText());
        if (beforedisc != afterdisc) {
            calculationall();
        } else {

        }

        if (jTextField11.getText().equals("")) {
            jTextField23.setEnabled(true);

        }
//        double amount = Double.parseDouble(jTextField13.getText());
//        Double discount = new Double(jTextField11.getText());
//        // float discount=Float.parseFloat(jTextField11.getText());
//        System.out.println(discount);
//        double discountafter = ((amount * discount) / 100);
//        double result = amount - discountafter;
//        DecimalFormat df=new DecimalFormat("0.00");
//        String result1 = df.format(result);
//        String resultconverttostring = Double.toString(discountafter);
//        jTextField23.setText("" + resultconverttostring);
//        String resultconverttostring1 = result1;
//        jTextField23.setText("" + resultconverttostring);
//        jTextField15.setText("" + resultconverttostring1);
//        jTextField16.setText("" + resultconverttostring1);
//     

    }//GEN-LAST:event_jTextField11FocusLost

    private void jTextField23FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField23FocusLost
        // TODO add your handling code here:
//       double amount = Double.parseDouble(jTextField13.getText());
//       double discount =Double.parseDouble(jTextField23.getText());
//       double result = amount-discount;
//       DecimalFormat df=new DecimalFormat("0.00");
//        String result1 = df.format(result);
//        String resultconverttostring = result1;
//        jTextField15.setText("" + resultconverttostring);
//       jTextField16.setText("" + resultconverttostring);
//        rigidcalculation();
    }//GEN-LAST:event_jTextField23FocusLost

    private void jTextField16FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField16FocusGained
        // TODO add your handling code here:
//          double dam = Double.parseDouble(jTextField15.getText());
//            double tax = Double.parseDouble(jTextField17.getText());
//            double oth = Double.parseDouble(jTextField14.getText());
//            double tot = ((dam * tax) / 100) + (dam + oth);
//            double Vat = Double.parseDouble(jTextField24.getText());
//            double vatamt= (Vat*dam)/100;
//            jTextField24.setText(""+vatamt);
//            tot=tot+vatamt;
//            
        //           jTextField16.setText("" + tot);
    }//GEN-LAST:event_jTextField16FocusGained

    private void jTable3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable3KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyChar();

        if (key == evt.VK_ENTER) {
            partyselect();
        }

        if (key == evt.VK_BACK_SPACE) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_LEFT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_RIGHT_PARENTHESIS) {
            jTextField18.requestFocus();
        }

        if (key == evt.VK_SPACE) {
            jTextField18.requestFocus();
        }

    }//GEN-LAST:event_jTable3KeyPressed

    private void jTextField18CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField18CaretUpdate

        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String partyName = jTextField18.getText();
        if (partyName.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party where party_name LIKE'%" + partyName + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(10), rs.getString(11), rs.getString("Mark_up"), rs.getString("Purchase_account"), igst};
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);
                sqe.printStackTrace();

            }

        }
        dtm.fireTableDataChanged();


    }//GEN-LAST:event_jTextField18CaretUpdate

    private void jTextField19CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField19CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();

        String party = jTextField19.getText();
        if (party.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party where party_code  LIKE '%" + party + "%'");

                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(10), rs.getString(11), rs.getString("Mark_up"), rs.getString("Purchase_account"), igst};

                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);
                sqe.printStackTrace();

            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField19CaretUpdate

    private void jTextField20CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField20CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String city = jTextField20.getText();
        if (city.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party where city LIKE '%" + city + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(10), rs.getString(11), rs.getString("Mark_up"), rs.getString("Purchase_account"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }

            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);
                sqe.printStackTrace();

            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField20CaretUpdate

    private void jTextField21CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField21CaretUpdate
        // TODO add your handling code here:
        ResultSet rs = null;
        DefaultTableModel dtm;
        dtm = (DefaultTableModel) jTable3.getModel();
        dtm.getDataVector().removeAllElements();
        String state = jTextField21.getText();
        if (state.trim().length() > 0) {
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                rs = st.executeQuery("SELECT  * FROM Party where state LIKE '%" + state + "%'");
                while (rs.next()) {

                    String igst;
                    if (rs.getBoolean("IGST") == true) {
                        igst = "True";
                    } else {
                        igst = "False";
                    }

                    Object o[] = {rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(10), rs.getString(11), rs.getString("Mark_up"), rs.getString("Purchase_account"), igst};
                    dtm = (DefaultTableModel) jTable3.getModel();
                    dtm.addRow(o);
                }
            } catch (Exception sqe) {
                System.out.println("SQl error" + sqe);
                sqe.printStackTrace();
            }
        }
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_jTextField21CaretUpdate

    private void jTextField23KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField23KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            reCalculate();
            jTextField15.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField11.requestFocus();
        }
    }//GEN-LAST:event_jTextField23KeyPressed

    private void jTextField22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField22ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField22ActionPerformed

    private void jTextField24KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField24KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            if (Double.parseDouble(jTextField24.getText()) != 0) {
                jDialog4.setVisible(true);
                list1.requestFocus();
                list1.removeAll();
                list1.addItem("ADD NEW");
                try {
                    connection c = new connection();
                    Connection connect = c.cone();

                    Statement st = connect.createStatement();
                    Statement st1 = connect.createStatement();
                    String duties = null;
//                    String vatinput = "VAT INPUT";
//                    String vatoutput = "VAT OUTPUT";
                    String Duteis = "DUTIES AND TAXES";
                    ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
                    while (rs1.next()) {
                        duties = rs1.getString(1);
                    }
                    ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = 'SGST' ");
                    while (rs.next()) {
                        list1.add(rs.getString(1));
                    }
                } catch (SQLException sqe) {
                    System.out.println("SQl error");
                    sqe.printStackTrace();
                }
//                jTextField26.requestFocus();
//                hide();
            }
//            } else {
//                jTextField26.requestFocus();
//            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField25.requestFocus();
        }
    }//GEN-LAST:event_jTextField24KeyPressed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        master.customer.Add_Purchase_Party addnew = new master.customer.Add_Purchase_Party();
        addnew.setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jTextField25KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField25KeyPressed
        // TODO add your handling code here:
     char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField24.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField17.requestFocus();
        }
//        char key = evt.getKeyChar();
//        if (key == evt.VK_ENTER || key == evt.VK_TAB) {
//
//            if (Double.parseDouble(jTextField25.getText()) != 0 && Party_cgst == null) {
//                jDialog5.setVisible(true);
//
//                //           if(Double.parseDouble(jTextField25.getText()) != 0)
//                {
//                    jDialog5.setVisible(true);
//                    list3.removeAll();
//                    list3.addItem("ADD NEW");
//                    try {
//                        connection c = new connection();
//                        Connection connect = c.cone();
//
//                        Statement st = connect.createStatement();
//                        Statement st1 = connect.createStatement();
//                        String duties = null;
//                        String Duteis = "DUTIES AND TAXES";
//                        String typeoftax = "OTHER";
//                        ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
//                        while (rs1.next()) {
//                            duties = rs1.getString(1);
//                        }
//                        ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = CGST ");
//                        while (rs.next()) {
//                            list3.add(rs.getString(1));
//                        }
//                    } catch (SQLException sqe) {
//                        System.out.println("SQl error");
//                        sqe.printStackTrace();
//                    }
//                    hide();
//                }
//
//                //jTextField24.requestFocus();
//            } else {
//                jTextField24.requestFocus();
//            }
////            rigidcalculation();
//
//        }
//        if (key == evt.VK_ESCAPE) {
//            jTextField17.requestFocus();
//        }
    }//GEN-LAST:event_jTextField25KeyPressed

    private void jTextField26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField26KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField14.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField24.requestFocus();
        }
    }//GEN-LAST:event_jTextField26KeyPressed
    double vatafter = 0;
    private void jTextField24FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField24FocusLost
//        vatafter = Double.parseDouble(jTextField24.getText());
//        if (vatafter != vatbefore) {
//            calculationall();
//        } else {
//
//        }
    }//GEN-LAST:event_jTextField24FocusLost
    double cstafter = 0;
    private void jTextField17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField17FocusLost
        // TODO add your handling code here:
//        cstafter = Double.parseDouble(jTextField17.getText());
//        if (cstbefore != cstafter) {
//            calculationall();
//        } else {
//
//        }
    }//GEN-LAST:event_jTextField17FocusLost
    public String Party_Sgst = null;
    private void list1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list1ActionPerformed
        // TODO add your handling code here:
        jDialog4.dispose();
        jTextField26.requestFocus();
        String currbal = null;
        String currbaltype = null;
        String vat_percent = null;
        show();
        if (list1.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger create = new reporting.accounts.createLedger();
            create.setVisible(true);
            create.jComboBox1.setSelectedItem("DUTIES AND TAXES");
        } else {
            Party_Sgst = list1.getSelectedItem();
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                System.out.println("sgst acc." + Party_Sgst);
                ResultSet rs = st.executeQuery("SELECT * from ledger where ledger_name='" + Party_Sgst + "'");
                while (rs.next()) {
                    currbal = rs.getString(10);
                    System.out.println("" + currbal);
                    currbaltype = rs.getString(11);
                    vat_percent = rs.getString(6);
                }
                jLabel38.setText("" + currbal);
                jLabel39.setText("" + currbaltype);
                jTextField24.setText("" + vat_percent);
            } catch (SQLException sqe) {
                System.out.println("SQl error");
                sqe.printStackTrace();
            }

            calculationall();
        }
    }//GEN-LAST:event_list1ActionPerformed
    public String createdgroup = null;
    public String Purchaseaccount = null;
    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        // TODO add your handling code here:
        list2.removeAll();
        list2.addItem("ADD NEW");
        jDialog6.setVisible(true);
        cashisselected = true;
        String c = "Cash";
        try {
            connection c1 = new connection();
            Connection connect = c1.cone();

            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            ResultSet rs = st.executeQuery("SELECT curr_bal,currbal_type from ledger where ledger_name='" + c + "'");
            while (rs.next()) {
                jLabel29.setText("" + rs.getString(1));
                jLabel30.setText("" + rs.getString(2));
            }
            String purchaseacc = "PURCHASE ACCOUNTS";
            ResultSet rs1 = st1.executeQuery("SELECT * from Group_create where under='" + purchaseacc + "'");
            while (rs1.next()) {
                createdgroup = rs1.getString(1);
            }
            ResultSet rs2 = st2.executeQuery("Select * from ledger where groups='" + purchaseacc + "'or groups='" + createdgroup + "'");
            while (rs2.next()) {
                list2.add(rs2.getString(1));
            }
            
            
           if (!jTextField1.getText().equals("0") && !jTextField27.getText().equals("0")) {
                jTextField1.setEnabled(false);
                jTextField27.setEnabled(false);

                jTextField17.setEnabled(true);
                jTextField25.setEnabled(true);
                jTextField24.setEnabled(true);
                jTextField26.setEnabled(true);

                jTextField1.setText("0");
                jTextField27.setText("0");
                Party_Igst = null;

                jTextField17.requestFocus();

            }
            
        } catch (SQLException sqe) {
            System.out.println("SQl error");
            sqe.printStackTrace();
        }
    }//GEN-LAST:event_jRadioButton3ActionPerformed
    String Purchase_party = null;
    private void list2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list2ActionPerformed
        // TODO add your handling code here:
        jDialog6.dispose();

        if (list2.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger cr = new reporting.accounts.createLedger();
            cr.setVisible(true);
            cr.jComboBox1.setSelectedItem("PURCHASE ACCOUNTS");
        } else {
            Purchaseaccount = list2.getSelectedItem();
            jLabel48.setText("" + Purchaseaccount);
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from ledger where ledger_name='" + Purchaseaccount + "' ");
                while (rs.next()) {
                    if (cashisselected) {
                        jLabel31.setText("" + rs.getString(10));
                        jLabel32.setText("" + rs.getString(11));

                    } else {
                        jLabel31.setText("" + rs.getString(10));
                        jLabel32.setText("" + rs.getString(11));

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            jDateChooser1.requestFocus();
        }


    }//GEN-LAST:event_list2ActionPerformed
    boolean cashisselected = true;
    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        // TODO add your handling code here:
        jDialog6.setVisible(true);
        list2.removeAll();
        list2.addItem("ADD NEW");
        cashisselected = false;
        try {
            connection c = new connection();
            Connection connect = c.cone();

            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            Statement st2 = connect.createStatement();
            Statement st3 = connect.createStatement();
            String purchaseacc = "PURCHASE ACCOUNTS";
            ResultSet rs1 = st1.executeQuery("SELECT * from Group_create where under='" + purchaseacc + "'");
            while (rs1.next()) {
                createdgroup = rs1.getString(1);
            }
            ResultSet rs2 = st2.executeQuery("Select * from ledger where groups='" + purchaseacc + "'or groups='" + createdgroup + "'");
            while (rs2.next()) {
                list2.add(rs2.getString(1));
            }
            ResultSet rs3 = st2.executeQuery("Select * from ledger where ledger_name='" + jTextField12.getText() + "'");
            while (rs3.next()) {
                jLabel29.setText(rs3.getString(10));
                jLabel30.setText(rs3.getString(11));
            }
        } catch (SQLException sqe) {
            System.out.println("SQl error");
            sqe.printStackTrace();
        }
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ESCAPE) {
            jDialog1.setVisible(false);
        }

        if (key == evt.VK_DELETE) {
            double totalamount = 0, totalamountaftertax = 0;
            DefaultTableModel dtm = null;
            dtm = (DefaultTableModel) jTable1.getModel();
            row = jTable1.getSelectedRow();
            double totalamt1 = Double.parseDouble((String) jTable1.getValueAt(row, 3));
            double nettotal = Double.parseDouble(jTextField13.getText());
            double nettotal1 = nettotal - totalamt1;
            jTextField13.setText("" + nettotal1);
            jTextField15.setText("" + nettotal1);
            jTextField16.setText("" + nettotal1);

            int rowcount = dtm.getRowCount();
            for (int i = row + 1; i < rowcount; i++) {
                int row1 = row + 2;
                jTable1.getModel().setValueAt(--row1, i, 0);
                row++;
            }
            int a = Integer.parseInt(jTextField5.getText());
            a = a - 1;
            jTextField5.setText("" + a);
            row = jTable1.getSelectedRow();
            dtm.removeRow(row);
        }


    }//GEN-LAST:event_jTable1KeyPressed
    double totalqnty = 0;
    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jComboBox2.requestFocus();

        }
        if (key == evt.VK_ESCAPE) {
            jComboBox3.requestFocus();

        }
        int key1 = evt.getKeyCode();
        if (key1 == evt.VK_F2) {
            master.article.addArticle addar = new master.article.addArticle();
            addar.setVisible(true);

        }

    }//GEN-LAST:event_jComboBox1KeyPressed

    private void jComboBox1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusLost
        // TODO add your handling code here:
        jButton3.requestFocus();
    }//GEN-LAST:event_jComboBox1FocusLost

    private void jComboBox4FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox4FocusGained
        // TODO add your handling code here:
        dynamicbyforgation();
        jComboBox4.setSelectedItem(null);
    }//GEN-LAST:event_jComboBox4FocusGained
    public void dynamicbyforgation() {
        jComboBox4.removeAllItems();

        try {
            connection c = new connection();
            Connection connect = c.cone();

            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("Select * from byforgation ");
            while (rs.next()) {

                jComboBox4.addItem(rs.getString(1));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //jComboBox4.setSelectedItem(null);
    }


    private void jComboBox1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1FocusGained

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ItemStateChanged
    int row1 = 0;
    String sno1 = null;
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        if (isUpdate = false) {
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            row1 = jTable1.getSelectedRow();
            sno1 = (String) jTable1.getValueAt(row1, 0);
            jDialog1.setVisible(true);
            jButton3.setVisible(false);
            //jButton7.setVisible(true);
            System.out.println("SerialNoDataVectorMap " + SerialNoDataVectorMap);
            Vector aDataVector = (Vector) SerialNoDataVectorMap.get(sno1);
            dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            if (aDataVector != null) {
                for (int i = 0; i < aDataVector.size(); i++) {
                    dtm.addRow((Vector) aDataVector.get(i));
                }
            }
        } else if (isUpdate = true) {
            jDialog1.setVisible(true);
            jButton3.setVisible(false);
            jButton7.setVisible(false);
            DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
            dtm.getDataVector().removeAllElements();
            dtm.fireTableDataChanged();
            try {
                int row_no = jTable1.getSelectedRow();
                connection c = new connection();
                Connection connect = c.cone();
                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from stockid where sno = '" + jTable1.getValueAt(row_no, 0) + "" + "'");
                while (rs.next()) {
                    String byforgation = rs.getString("Byforgation");
                    if (!byforgation.equals(null)) {
                        byforgation = "";
                    }
                    String articleno = rs.getString("articleno");
                    if (!articleno.equals(null)) {
                        articleno = "";
                    }

                    Object o[] = {rs.getString("sno"), rs.getString("Stock_No"), rs.getString("Ra_te"), rs.getString("Re_tail"), rs.getString("De_sc"), byforgation, rs.getString("Pro_duct"), articleno, rs.getString("ismeter"), rs.getString("qnt"), rs.getString("nooftag"), rs.getString("P_Desc")};
                    dtm.addRow(o);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
//        double billamount = Double.parseDouble(jTextField13.getText());
//        String sno = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 1);
//        double ratebefore = Double.parseDouble((String) jTable1.getValueAt(jTable1.getSelectedRow(), 3));
//        DefaultTableModel dtm = (DefaultTableModel) jTable2.getModel();
//        Vector aDataVector = new Vector(dtm.getDataVector());
//        SerialNoDataVectorMap.put(sno, aDataVector);
//        jDialog1.setVisible(false);
//        int i = 0;
//        int rowcount = jTable2.getRowCount();
//        double rate = 0;
//        for (i = 0; i < rowcount; i++) {
//            double rate1 = Double.parseDouble((String) jTable2.getValueAt(i, 1));
//            rate = rate1 + rate;
//        }
//        jTable1.setValueAt(rate, jTable1.getSelectedRow(), 3);
//        double diff = 0;
//        double billamount1 = 0;
//        if (rate < ratebefore) {
//            diff = ratebefore - rate;
//            System.out.println("diffsub" + diff);
//            billamount1 = billamount - diff;
//            System.out.println("billamount1sub" + billamount1);
//        }
//        if (rate == ratebefore) {
//            billamount1 = billamount;
//        }
//        if (rate > ratebefore) {
//            diff = rate - ratebefore;
//            System.out.println("diffadd" + diff);
//            billamount1 = billamount + diff;
//            System.out.println("billamount1add" + billamount1);
//        }
//        jTextField13.setText("" + billamount1);
//        calculationall();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jComboBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox4ActionPerformed

    }//GEN-LAST:event_jComboBox4ActionPerformed

    private void jComboBox4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBox4MouseClicked

    }//GEN-LAST:event_jComboBox4MouseClicked

    private void jTextField6FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField6FocusLost
        // TODO add your handling code here:
        String qty = jTextField6.getText();
//
//        if (!qty.matches(("[0-9]+$")) && !jTextField6.getText().isEmpty()) {
//            JOptionPane.showMessageDialog(rootPane, "Please Enter Number only");
//            jTextField6.requestFocus();
//        }
    }//GEN-LAST:event_jTextField6FocusLost

    private void jTextField9FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField9FocusLost
        // TODO add your handling code here:
        String retail = jTextField9.getText();

        if (!retail.matches(("[0-9]+$")) || jTextField9.getText().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Number only");
            jTextField9.requestFocus();
        }
    }//GEN-LAST:event_jTextField9FocusLost
    int ismeter = 0;
    private void jComboBox2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox2KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jButton7.setVisible(false);
            jButton3.setVisible(true);
            sn = jTextField5.getText();
            qnt = jTextField6.getText();
            rate = String.valueOf(jTextField3.getText());
            amount = jTextField7.getText();
            pds = jTextField8.getText();
            rt = jTextField9.getText();
            ds = jTextField10.getText();
            pd = (String) jComboBox3.getSelectedItem();
            fr = (String) jComboBox4.getSelectedItem();
            articleno = (String) jComboBox1.getSelectedItem();
            samebarcode = (String) jComboBox2.getSelectedItem();
            sup = jTextField22.getText();
            supname = jTextField12.getText();
            System.out.println("invoice date is " + formatter.format(jDateChooser2.getDate()));
            invod = formatter.format(jDateChooser2.getDate());
            invon = jTextField2.getText();
            if (jRadioButton3.isSelected()) {
                purch = "Cash";
            }
            if (jRadioButton4.isSelected()) {
                purch = "Credit";
            }
            ref = jTextField4.getText();

            bil = jTextField13.getText();
            otherc = jTextField14.getText();
            va = jTextField15.getText();

            namoun = jTextField16.getText();

            supid[incr] = sup;
            supnameid[incr] = supname;
            invoiced[incr] = invod;
            invoiceno[incr] = (invon);
            purchast[incr] = purch;
            refrence[incr] = ref;

            sno[incr] = sn;
            qty[incr] = qnt;
            rat[incr] = rate;
            amoun[incr] = amount;
            pdes[incr] = pds;
            retail[incr] = rt;
            desc[incr] = ds;
            prod[incr] = pd;
            ftr[incr] = fr;
            articlenum[incr] = articleno;
            samebarcode1[incr] = samebarcode;
            bill[incr] = bil;
            ocharge[incr] = otherc;
            amountafter[incr] = va;
            discount[incr] = jTextField11.getText();
            tax[incr] = jTextField17.getText();
            namount[incr] = namoun;

            mount = (mount + Double.parseDouble(amount));
            double tax = Double.parseDouble(jTextField17.getText());
            double taxamount = mount * tax / 100;
            Double amountaftertax = mount + taxamount;
            DecimalFormat df = new DecimalFormat("0.00");
            String mountr = df.format(mount);

            totalqnty = Double.parseDouble(qnt) + totalqnty;
            jTextField28.setText("" + totalqnty);

            String amountaftertax1 = df.format(amountaftertax);
            jTextField13.setText("" + mountr);
            jTextField15.setText("" + mountr);
            jTextField16.setText("" + amountaftertax1);

            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                Statement ts = connect.createStatement();

                ResultSet sr = ts.executeQuery("select ismeter from product where product_code='" + pd + "' ");
                while (sr.next()) {
                    ismeter = sr.getInt(1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            Object o[] = {sn, pd, qnt, rate, amount, pds, rt, ds, fr, articleno, samebarcode};
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.addRow(o);

            stocknumbers();

            ++s;
            jTextField5.setText("" + s);

            jTextField6.setText(null);
            jTextField7.setText(null);
            jTextField8.setText(null);
            jTextField9.setText(null);
            jTextField10.setText(null);
            jTextField3.setText(null);
            jComboBox3.setSelectedItem(pd);
            jComboBox4.setSelectedItem(null);
            jComboBox1.setSelectedItem(articleno);
            ++incr;
            jButton3.requestFocus();
        }


    }//GEN-LAST:event_jComboBox2KeyPressed

    private void jTextField25FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField25FocusLost
        rigidcalculation() ;
    }//GEN-LAST:event_jTextField25FocusLost

    private void jTextField26FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField26FocusLost
        // TODO add your handling code here:
         rigidcalculation();
    }//GEN-LAST:event_jTextField26FocusLost

    private void jTextField14FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField14FocusLost
        // TODO add your handling code here:
//        rigidcalculation();
        jDialog7.setVisible(true);
        list4.removeAll();
        list4.addItem("ADD NEW");
        try {
            connection c = new connection();
            Connection connect = c.cone();

            Statement st = connect.createStatement();
            Statement st1 = connect.createStatement();
            String duties = null;
            String Duteis = "DIRECT EXPENSES";
            ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
            while (rs1.next()) {
                duties = rs1.getString(1);
            }
            ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "'");
            while (rs.next()) {
                list4.add(rs.getString(1));
            }
        } catch (SQLException sqe) {
            System.out.println("SQl error");
            sqe.printStackTrace();
        }


    }//GEN-LAST:event_jTextField14FocusLost
    double beforedisc = 0;
    private void jTextField11FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField11FocusGained
        // TODO add your handling code here:
        beforedisc = Double.parseDouble(jTextField11.getText());

    }//GEN-LAST:event_jTextField11FocusGained
    double cstbefore = 0;
    private void jTextField17FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField17FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField17FocusGained
    double vatbefore = 0;
    private void jTextField24FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField24FocusGained
        // TODO add your handling code here:
  //      vatbefore = Double.parseDouble(jTextField24.getText());
    }//GEN-LAST:event_jTextField24FocusGained

    private void jTextField28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField28KeyPressed
        // TODO add your handling code here:
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {

            jTextField11.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField13.requestFocus();
        }

    }//GEN-LAST:event_jTextField28KeyPressed

    private void jComboBox2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox2FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox2FocusLost

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6ActionPerformed

    private void list3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list3ActionPerformed
        // TODO add your handling code here:
        jDialog5.dispose();
        show();
        if (list3.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger cr = new reporting.accounts.createLedger();
            cr.setVisible(true);
            cr.jComboBox1.setSelectedItem("DUTIES AND TAXES");
            cr.jComboBox5.setSelectedItem("OTHER");
        } else {

            Party_cgst = list3.getSelectedItem();
            String cst_percent = null;
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from ledger where ledger_name='" + Party_cgst + "' ");
                while (rs.next()) {
                    cst_percent = rs.getString(6);
                    if (cashisselected) {
                        jLabel43.setText("" + rs.getString(10));
                        jLabel44.setText("" + rs.getString(11));
                    } else {
                        jLabel43.setText("" + rs.getString(10));
                        jLabel44.setText("" + rs.getString(11));

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            jTextField17.setText("" + cst_percent);
        }
        jTextField25.requestFocus();
        calculationall();
    }//GEN-LAST:event_list3ActionPerformed
    public String other_charges = null;
    private void list4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list4ActionPerformed
        // TODO add your handling code here:
        jDialog7.dispose();
        String currbal = null;
        String currbaltype = null;
        if (list4.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger create = new reporting.accounts.createLedger();
            create.setVisible(true);
            create.jComboBox1.setSelectedItem("DIRECT EXPENSES");
        } else {
            other_charges = list4.getSelectedItem();
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("SELECT * from ledger where ledger_name='" + other_charges + "'");
                while (rs.next()) {
                    currbal = rs.getString(10);
                    System.out.println("" + currbal);
                    currbaltype = rs.getString(11);
                }
                jLabel45.setText("" + currbal);
                jLabel46.setText("" + currbaltype);

            } catch (SQLException sqe) {
                System.out.println("SQl error");
                sqe.printStackTrace();
            }
        }

    }//GEN-LAST:event_list4ActionPerformed

    private void jTextField26FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField26FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField26FocusGained

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        jDialog1.dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        jTextField12.requestFocus();
    }//GEN-LAST:event_formWindowOpened
//    String oldsupplier = "";
//    String oldpurchase = "";
//    String partyvatold = "";
//    String purchaseaccountold = "";
//    String purchaseaccountcurrbalold = "";
//    String purchaseaccountcurrbaltypeold = "";
//    String sundrycreditorold = "";
//    String sundrycreditorcurrbalold = "";
//    String sundrycreditorcurrbaltypeold = "";
//    String vatold = "";
//    String vatcurrbalold = "";
//    String vatcurrbaltypeold = "";
//    String cstold = "";
//    String igstold = "";
//    String igstcurrbalold = "";
//    String igstcurrbaltypeold = "";
//    String cstcurrbalold = "";
//    String cstcurrbaltypeold = "";
//    String otherchargesold = "";
//    String otherchargescurrbalold = "";
//    String otherchargescurrbaltypeold = "";
//    String discountold = "";
//    String discountcurrbalold = "";
//    String discountcurrbaltypeold = "";

    public String sundrycreditorold = "";
    double sundrycreditorcurrbalold = 0;
    String sundrycreditorcurrbaltypeold = "";
    String rDiscountaccount = null;
    String rupdateddisccurrbal = null;
    String rupdateddisccurrbaltyoe = null;
    double rigst_value = 0;
    String rigst_type = "";
    double rsgst_value = 0;
    String rsgst_type = "";
    double rcgst_value = 0;
    String rcgst_type = "";
    double rupdate_igst_value = 0;
    String rupdate_igst_type = "";
    double rupdate_sgst_value = 0;
    String rupdate_sgst_type = "";
    double rupdate_cgst_value = 0;
    String rupdate_cgst_type = "";
    double rupdate_other_value = 0;
    String rupdate_other_type = "";
    double rupdate_cash_value = 0;
    String rupdate_cash_type = "";
    public String rpurc_account = "";
    double rpurc_currbal = 0;
    double rpurc_amnt = 0;
    String rpurc_type = "";
    double rupdate_purc_amnt = 0;
    String rupdate_purc_type = "";

    public String old_igst_amnt = "";
    public String old_igst_party = "";
    public String old_sgst_amnt = "";
    public String old_sgst_party = "";
    public String old_cgst_amnt = "";
    public String old_cgst_party = "";

    public void updatereversebalances() {
//---------For Old Igst ledger-----------------------------------------------------------------------
        if (!old_igst_amnt.equals("") && !old_igst_party.equals("")) {
            try {
                double rigst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rigst_st = connect.createStatement();
                ResultSet rigst_rs = rigst_st.executeQuery("Select * from ledger where ledger_name = '" + old_igst_party + "'");
                while (rigst_rs.next()) {
                    rigst_value = rigst_rs.getDouble("curr_bal");
                    rigst_type = rigst_rs.getString("currbal_type");
                }

                ResultSet rigst_rs1 = rigst_st.executeQuery("Select * from stock where Invoice_number = '" + jTextField2.getText() + "'");
                while (rigst_rs1.next()) {
                    rigst_amnt = rigst_rs1.getDouble("IGST_Amount");
                }

//                double rigst_amnt = Double.parseDouble(jTextField1.getText());
                double riamnt;

                if (rigst_type.equals("DR")) {
                    riamnt = rigst_amnt - rigst_value;
                    if (riamnt <= 0) {
                        rupdate_igst_type = "DR";
                        rupdate_igst_value = Math.abs(riamnt);
                    } else {
                        rupdate_igst_type = "CR";
                        rupdate_igst_value = Math.abs(riamnt);
                    }
                } else {
                    if (rigst_type.equals("CR")) {
                        riamnt = rigst_amnt + rigst_value;
                        if (riamnt >= 0) {
                            rupdate_igst_type = "CR";
                            rupdate_igst_value = Math.abs(riamnt);
                        }
                    }
                }
                rigst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_igst_value + "',currbal_type='" + rupdate_igst_type + "' where ledger_name = '" + old_igst_party + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//----------------------------------------------------------------------------------------------------------------
//----------------For Old Sgst Ledger-----------------------------------------------------------------------------
        if (!old_sgst_amnt.equals("") && !old_sgst_party.equals("")) {
            try {
                double rsgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rsgst_st = connect.createStatement();
                ResultSet rsgst_rs = rsgst_st.executeQuery("Select * from ledger where ledger_name = '" + old_sgst_party + "'");
                while (rsgst_rs.next()) {
                    rsgst_value = rsgst_rs.getDouble("curr_bal");
                    rsgst_type = rsgst_rs.getString("currbal_type");
                }

//                double rsgst_amnt = Double.parseDouble(jTextField24.getText());
                ResultSet rsgst_rs1 = rsgst_st.executeQuery("Select * from stock where Invoice_number = '" + jTextField2.getText() + "'");
                while (rsgst_rs1.next()) {
                    rsgst_amnt = rsgst_rs1.getDouble("sgstamount");
                }
                double rsamnt;

                if (rsgst_type.equals("DR")) {
                    rsamnt = rsgst_amnt - rsgst_value;
                    if (rsamnt <= 0) {
                        rupdate_sgst_type = "DR";
                        rupdate_sgst_value = Math.abs(rsamnt);
                    } else {
                        rupdate_sgst_type = "CR";
                        rupdate_sgst_value = Math.abs(rsamnt);
                    }
                } else {
                    if (rsgst_type.equals("CR")) {
                        rsamnt = rsgst_amnt + rsgst_value;
                        if (rsamnt >= 0) {
                            rupdate_sgst_type = "CR";
                            rupdate_sgst_value = Math.abs(rsamnt);
                        }
                    }
                }
                rsgst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_sgst_value + "',currbal_type='" + rupdate_sgst_type + "' where ledger_name = '" + old_sgst_party + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//----------------------------------------------------------------------------------------------------------------
//----------------For Old Cgst Ledger-----------------------------------------------------------------------------
        if (!old_cgst_amnt.equals("") && !old_cgst_party.equals("")) {
            try {
                double rcgst_amnt = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcgst_st = connect.createStatement();
                ResultSet rcgst_rs = rcgst_st.executeQuery("Select * from ledger where ledger_name = '" + old_cgst_party + "'");
                while (rcgst_rs.next()) {
                    rcgst_value = rcgst_rs.getDouble("curr_bal");
                    rcgst_type = rcgst_rs.getString("currbal_type");
                }

//                double rcgst_amnt = Double.parseDouble(jTextField17.getText());
                ResultSet rcgst_rs1 = rcgst_st.executeQuery("Select * from stock where Invoice_number = '" + jTextField2.getText() + "'");
                while (rcgst_rs1.next()) {
                    rcgst_amnt = rcgst_rs1.getDouble("cgsttaxamount");
                }

                double rcamnt;

                if (rcgst_type.equals("DR")) {
                    rcamnt = rcgst_amnt - rcgst_value;
                    if (rcamnt <= 0) {
                        rupdate_cgst_type = "DR";
                        rupdate_cgst_value = Math.abs(rcamnt);
                    } else {
                        rupdate_cgst_type = "CR";
                        rupdate_cgst_value = Math.abs(rcamnt);
                    }
                } else {
                    if (rcgst_type.equals("CR")) {
                        rcamnt = rcgst_amnt + rcgst_value;
                        if (rcamnt >= 0) {
                            rupdate_cgst_type = "CR";
                            rupdate_cgst_value = Math.abs(rcamnt);
                        }
                    }
                }
                rcgst_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_cgst_value + "',currbal_type='" + rupdate_cgst_type + "' where ledger_name = '" + old_cgst_party + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------
//-------------------For Old other Charges-------------------------------------------------------------------------
        if ((Double.parseDouble(jTextField14.getText()) == 0) && (other_charges == null)) {

        } else if ((Double.parseDouble(jTextField14.getText()) != 0) && (other_charges == null)) {
            JOptionPane.showMessageDialog(rootPane, "Please choose Direct expenses account");
            jTextField14.requestFocus();
        } else {
            try {
                double rotheramount = 0;
                double rothercurrbal = 0;
                String rCurbaltypeother = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement rother_st = connect.createStatement();
                ResultSet rother_rs = rother_st.executeQuery("Select * from ledger where ledger_name = '" + other_charges + "'");

                while (rother_rs.next()) {
                    rothercurrbal = rother_rs.getDouble("curr_bal");
                    rCurbaltypeother = rother_rs.getString("currbal_type");
                }
//                double rotheramount = Double.parseDouble(jTextField14.getText());

                ResultSet rother_rs1 = rother_st.executeQuery("Select * from stock where Invoice_number = '" + jTextField2.getText() + "'");
                while (rother_rs1.next()) {
                    rotheramount = rother_rs1.getDouble("Other_charges");
                }

                double raother;

                if (rCurbaltypeother.equals("DR")) {
                    raother = rotheramount - rothercurrbal;
                    if (raother < 0) {
                        rupdate_other_type = "DR";
                        rupdate_other_value = Math.abs(raother);

                    } else {
                        rupdate_other_type = "CR";
                        rupdate_other_value = Math.abs(raother);
                    }

                } else {
                    if (rCurbaltypeother.equals("CR")) {
                        raother = rotheramount + rothercurrbal;
                        if (raother > 0) {
                            rupdate_other_type = "CR";
                            rupdate_other_value = Math.abs(raother);
                        }
                    }
                }

                rother_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_other_value + "',currbal_type='" + rupdate_other_type + "' where ledger_name='" + other_charges + "'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------
//-----------For Old Creditor and cash Ledger----------------------------------------------------------------------
        if (jRadioButton3.isSelected()) {
            try {
                double rcash_amount = 0;
                double rcashcurrbal = 0;
                String rCurbaltypecash = "";
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcash_st = connect.createStatement();
                ResultSet rcash_rs = rcash_st.executeQuery("Select * from ledger where ledger_name = 'CASH'");

                while (rcash_rs.next()) {
                    rcashcurrbal = rcash_rs.getDouble("curr_bal");
                    rCurbaltypecash = rcash_rs.getString("currbal_type");
                }
//                double rcash_amount = Double.parseDouble(jTextField16.getText());

                ResultSet rcash_rs1 = rcash_st.executeQuery("Select * from stock where Invoice_number = '" + jTextField2.getText() + "'");
                while (rcash_rs1.next()) {
                    rcash_amount = rcash_rs1.getDouble("Net_amount");
                }

                double rcash_amnt = 0;

                if (rCurbaltypecash.equals("DR")) {
                    rcash_amnt = rcashcurrbal + rcash_amount;
                    rupdate_cash_value = Math.abs(rcash_amnt);
                } else if (rCurbaltypecash.equals("CR")) {
                    if (rcash_amnt > rcash_amount) {
                        rcash_amnt = rcashcurrbal - rcash_amount;
                        rupdate_cash_value = Math.abs(rcash_amnt);
                    }
                    if (rcash_amnt < rcash_amount) {
                        rcash_amnt = rcash_amount - rcashcurrbal;
                        rupdate_cash_value = Math.abs(rcash_amnt);
                        rupdate_cash_type = "DR";
                    }
                }

                rcash_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_cash_value + "',currbal_type='" + rupdate_cash_type + "' where ledger_name='CASH'");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (jRadioButton4.isSelected()) {
            try {
                double totalamount = 0;
                connection c = new connection();
                Connection connect = c.cone();
                Statement rcraditor_st = connect.createStatement();
                ResultSet rs4 = rcraditor_st.executeQuery("select * from ledger where ledger_name='" + sundrycreditorold + "'");
                while (rs4.next()) {
                    sundrycreditorcurrbalold = rs4.getDouble("curr_bal");
                    sundrycreditorcurrbaltypeold = rs4.getString("currbal_type");
                }

                double creditoramount = sundrycreditorcurrbalold;
//            double totalamount = Double.parseDouble(jTextField16.getText());

                ResultSet rs5 = rcraditor_st.executeQuery("Select * from stock where Invoice_number = '" + jTextField2.getText() + "'");
                while (rs5.next()) {
                    totalamount = rs5.getDouble("Net_amount");
                }

                double creditoramount1 = creditoramount;
                if (sundrycreditorcurrbaltypeold.equals("DR")) {
                    creditoramount1 = creditoramount1 + totalamount;
                    sundrycreditorcurrbalold = (creditoramount1);
                } else if (sundrycreditorcurrbaltypeold.equals("CR")) {
                    if (creditoramount1 > totalamount) {
                        creditoramount1 = creditoramount1 - totalamount;
                        sundrycreditorcurrbalold = (creditoramount1);

                    } else if (creditoramount1 <= totalamount) {
                        creditoramount1 = totalamount - creditoramount1;
                        sundrycreditorcurrbalold = (creditoramount1);
                        sundrycreditorcurrbaltypeold = ("DR");
                    }
                }
                rcraditor_st.executeUpdate("Update ledger SET curr_bal= '" + sundrycreditorcurrbalold + "',currbal_type='" + sundrycreditorcurrbaltypeold + "' where ledger_name='" + sundrycreditorold + "'");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//-----------------------------------------------------------------------------------------------------------------
//--------------------For Old Purchase Ledger----------------------------------------------------------------------
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement rpur_st = connect.createStatement();
            Statement rpur_st1 = connect.createStatement();
            ResultSet rpur_rs1 = rpur_st1.executeQuery("Select * from stock where Invoice_number = '" + jTextField2.getText() + "'");
            while (rpur_rs1.next()) {
                rpurc_currbal = rpur_rs1.getDouble("amount_after_discount");
            }

            ResultSet rpur_rs = rpur_st.executeQuery("Select * from ledger where ledger_name = '" + rpurc_account + "'");
            while (rpur_rs.next()) {
                rpurc_type = rpur_rs.getString("currbal_type");
                rpurc_amnt = rpur_rs.getDouble("curr_bal");
            }

            double rpurchase_amnt = rpurc_amnt;
            double rpurchase_currbal = rpurc_currbal;
            double rpur_bal = 0;

            if (rpurc_type.equals("DR")) {
                rpur_bal = rpurchase_currbal - rpurchase_amnt;
                if (rpur_bal <= 0) {
                    rupdate_purc_type = "DR";
                    rupdate_purc_amnt = Math.abs(rpur_bal);
                } else {
                    rupdate_purc_type = "CR";
                    rupdate_purc_amnt = Math.abs(rpur_bal);
                }

            } else {
                if (rpurc_type.equals("CR")) {
                    rpur_bal = rpurchase_currbal + rpurchase_amnt;
                    if (rpur_bal >= 0) {
                        rupdate_purc_type = "CR";
                        rupdate_purc_amnt = Math.abs(rpur_bal);
                    }
                }
            }
            System.out.println("rupdate_purc_amnt"+rupdate_purc_amnt);
            rpur_st.executeUpdate("Update ledger SET curr_bal= '" + rupdate_purc_amnt + "',currbal_type='" + rupdate_purc_type + "' where ledger_name='" + rpurc_account + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }
//-----------------------------------------------------------------------------------------------------------------
    }

//    public void updatereversebalances() {
//        try {
//            connection c = new connection();
//            Connection connect = c.cone();
//            Statement stmt = connect.createStatement();
//
//            ResultSet rs = stmt.executeQuery("select * from stock where Invoice_number='" + jTextField2.getText() + "' ");
//            while (rs.next()) {
//                sundrycreditorold = rs.getString("Supplier_name");
//                purchaseaccountold = rs.getString("by_ledger");
//                vatold = rs.getString("sgstaccount");
//                cstold = rs.getString("cgsttaxaccount");
//                igstold = rs.getString("IGST_Tax_Account");
//                otherchargesold = rs.getString("otherchargesaccount");
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if ((Double.parseDouble(jTextField26.getText()) == 0) || (vatold == null) || vatold.equals("null")) {
//
//        } else {
////for sgst 
//            try {
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement stmtvat = connect.createStatement();
//
//                ResultSet rs1 = stmtvat.executeQuery("select * from ledger where ledger_name='" + vatold + "'");
//                while (rs1.next()) {
//                    vatcurrbalold = rs1.getString(10);
//                    vatcurrbaltypeold = rs1.getString(11);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            String vatcurrbal = vatcurrbalold;
//            String Curbaltypevat = vatcurrbaltypeold;
//            double vatcurrbal1 = Double.parseDouble(vatcurrbal);
//            double vatamount = Double.parseDouble(jTextField26.getText());
//            double avat;
//            if (Curbaltypevat.equals("DR")) {
//                avat = vatamount - vatcurrbal1;
//                if (avat < 0) {
//                    vatcurrbaltypeold = "DR";
//                    vatcurrbalold = ("" + String.valueOf(Math.abs(avat)));
//                } else {
//                    vatcurrbaltypeold = ("CR");
//                    vatcurrbalold = ("" + String.valueOf(Math.abs(avat)));
//                }
//            } else {
//                if (Curbaltypevat.equals("CR")) {
//                    avat = vatamount + vatcurrbal1;
//                    if (avat > 0) {
//                        vatcurrbaltypeold = ("CR");
//                        vatcurrbalold = ("" + String.valueOf(Math.abs(avat)));
//                    }
//                }
//            }
//        }
//        if ((Double.parseDouble(jTextField25.getText()) == 0) && (cstold == null) || (cstold.equals("null"))) {
//        } else {
//            try {
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement stmtcst = connect.createStatement();
//
//                ResultSet rs2 = stmtcst.executeQuery("select * from ledger where ledger_name='" + cstold + "'");
//                while (rs2.next()) {
//                    cstcurrbalold = rs2.getString(10);
//                    cstcurrbaltypeold = rs2.getString(11);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
////for cgst
//            String cstcurrbal = cstcurrbalold;
//            String Curbaltypecst = cstcurrbaltypeold;
//            double cstcurrbal1 = Double.parseDouble(cstcurrbal);
//            double cstamount = Double.parseDouble(jTextField25.getText());
//            double acst;
//            if (Curbaltypecst.equals("DR")) {
//                acst = cstamount - cstcurrbal1;
//                if (acst < 0) {
//                    cstcurrbaltypeold = ("DR");
//                    cstcurrbalold = ("" + String.valueOf(Math.abs(acst)));
//                } else {
//                    cstcurrbaltypeold = ("CR");
//                    cstcurrbalold = ("" + String.valueOf(Math.abs(acst)));
//                }
//            } else {
//                if (Curbaltypecst.equals("CR")) {
//                    acst = cstamount + cstcurrbal1;
//                    if (acst > 0) {
//                        cstcurrbaltypeold = ("CR");
//                        cstcurrbalold = ("" + String.valueOf(Math.abs(acst)));
//                    }
//                }
//            }
//        }
//
//// For Igst updation
//        if ((Double.parseDouble(jTextField27.getText()) == 0) && (igstold == null) || (igstold.equals("null"))) {
//        } else {
//            try {
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement stmtcst = connect.createStatement();
//
//                ResultSet rs2 = stmtcst.executeQuery("select * from ledger where ledger_name='" + igstold + "'");
//                while (rs2.next()) {
//                    igstcurrbalold = rs2.getString(10);
//                    igstcurrbaltypeold = rs2.getString(11);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
////for Igst
//            String igstcurrbal = igstcurrbalold;
//            String Curbaltypeigst = igstcurrbaltypeold;
//            double igstcurrbal1 = Double.parseDouble(igstcurrbal);
//            double igstamount = Double.parseDouble(jTextField27.getText());
//            double aigst;
//            if (Curbaltypeigst.equals("DR")) {
//                aigst = igstamount - igstcurrbal1;
//                if (aigst < 0) {
//                    igstcurrbaltypeold = ("DR");
//                    igstcurrbalold = ("" + String.valueOf(Math.abs(aigst)));
//                } else {
//                    igstcurrbaltypeold = ("CR");
//                    igstcurrbalold = ("" + String.valueOf(Math.abs(aigst)));
//                }
//            } else {
//                if (Curbaltypeigst.equals("CR")) {
//                    aigst = igstamount + igstcurrbal1;
//                    if (aigst > 0) {
//                        igstcurrbaltypeold = ("CR");
//                        igstcurrbalold = ("" + String.valueOf(Math.abs(aigst)));
//                    }
//                }
//            }
//        }
//
////for discount
//        String disc_currbal = null;
//        String disc_currbaltype = null;
//        try {
//            connection c = new connection();
//            Connection connect = c.cone();
//            Statement st = connect.createStatement();
//            ResultSet rs = st.executeQuery("select * from ledger where ledger_name='Discount Account'");
//            while (rs.next()) {
//                Discountaccount = rs.getString(1);
//                disc_currbal = rs.getString(10);
//                disc_currbaltype = rs.getString(11);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////for discount
//
//        if ((Double.parseDouble(jTextField23.getText()) != 0) && (Discountaccount == null)) {
//            JOptionPane.showMessageDialog(rootPane, "Please choose Discount account");
//        } else {
//
//            String disccurrbal = disc_currbal;
//            String Curbaltypedisc = disc_currbaltype;
//            double othercurrbal1 = Double.parseDouble(disccurrbal);
//            double discamount = Double.parseDouble(jTextField23.getText());
//            double adisc;
//
//            if (Curbaltypedisc.equals("CR")) {
//                adisc = discamount - othercurrbal1;
//                if (adisc <= 0) {
//                    updateddisccurrbaltyoe = "CR";
//                    updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
//
//                } else {
//                    updateddisccurrbaltyoe = ("DR");
//                    updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
//                }
//
//            } else {
//                if (Curbaltypedisc.equals("DR")) {
//                    adisc = discamount + othercurrbal1;
//                    if (adisc >= 0) {
//                        updateddisccurrbaltyoe = ("DR");
//                        updateddisccurrbal = ("" + String.valueOf(Math.abs(adisc)));
//                    }
//                }
//
//            }
//        }
//
////for other charges
//        if ((otherchargesold == null) || (otherchargesold.equals("null"))) {
//
//        } else {
//
//            try {
//
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement stmtother = connect.createStatement();
//
//                ResultSet rs3 = stmtother.executeQuery("select * from ledger where ledger_name='" + otherchargesold + "'");
//                while (rs3.next()) {
//                    otherchargescurrbalold = rs3.getString(10);
//                    otherchargescurrbaltypeold = rs3.getString(11);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            String othercurrbal = otherchargescurrbalold;
//            String Curbaltypeother = otherchargescurrbaltypeold;
//            double othercurrbal1 = Double.parseDouble(othercurrbal);
//            double otheramount = Double.parseDouble(jTextField14.getText());
//            double aother;
//
//            if (Curbaltypeother.equals("DR")) {
//                aother = otheramount - othercurrbal1;
//                if (aother < 0) {
//                    otherchargescurrbaltypeold = ("DR");
//                    otherchargescurrbalold = ("" + String.valueOf(Math.abs(aother)));
//
//                } else {
//                    otherchargescurrbaltypeold = ("CR");
//                    otherchargescurrbalold = ("" + String.valueOf(Math.abs(aother)));
//                }
//
//            } else {
//                if (Curbaltypeother.equals("CR")) {
//                    aother = otheramount + othercurrbal1;
//                    if (aother > 0) {
//                        otherchargescurrbaltypeold = ("CR");
//                        otherchargescurrbalold = ("" + String.valueOf(Math.abs(aother)));
//                    }
//                }
//
//            }
//        }
//
//        if (jRadioButton4.isSelected()) {
////purchase and creditor         
//
//            try {
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement stmtcreditor = connect.createStatement();
//
//                ResultSet rs4 = stmtcreditor.executeQuery("select * from ledger where ledger_name='" + sundrycreditorold + "'");
//                while (rs4.next()) {
//                    sundrycreditorcurrbalold = rs4.getString(10);
//                    sundrycreditorcurrbaltypeold = rs4.getString(11);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            try {
//                connection c = new connection();
//                Connection connect = c.cone();
//                Statement stmtpurchase = connect.createStatement();
//
//                ResultSet rs5 = stmtpurchase.executeQuery("select * from ledger where ledger_name='" + purchaseaccountold + "'");
//                while (rs5.next()) {
//                    purchaseaccountcurrbalold = rs5.getString(10);
//                    purchaseaccountcurrbaltypeold = rs5.getString(11);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            String purchaseamount = purchaseaccountcurrbalold;
//            String creditoramount = sundrycreditorcurrbalold;
//            double totalamount = Double.parseDouble(jTextField16.getText());
//            double purchaseamount1 = Double.parseDouble(purchaseamount);
//            double creditoramount1 = Double.parseDouble(creditoramount);
//            double totalamountaftervat = totalamount - Double.parseDouble(jTextField26.getText());
//            double bc = 0;
//            if (purchaseaccountcurrbaltypeold.equals("CR")) {
//                //                    purchaseamount1=purchaseamount1+totalamountaftervat;
//                purchaseamount1 = purchaseamount1 + Double.parseDouble(jTextField13.getText());
//                purchaseaccountcurrbalold = ("" + purchaseamount1);
//            } else if (purchaseaccountcurrbaltypeold.equals("DR")) {
//                if (purchaseamount1 > Double.parseDouble(jTextField13.getText())) {
//                    bc = (purchaseamount1 - Double.parseDouble(jTextField13.getText()));
//                    purchaseaccountcurrbalold = ("" + bc);
//                }
//                if (Double.parseDouble(jTextField13.getText()) > purchaseamount1) {
//                    bc = (Double.parseDouble(jTextField13.getText()) - purchaseamount1);
//                    purchaseaccountcurrbalold = ("" + bc);
//                    purchaseaccountcurrbaltypeold = ("CR");
//                }
//            }
//
//            if (sundrycreditorcurrbaltypeold.equals("DR")) {
//                creditoramount1 = creditoramount1 + totalamount;
//                sundrycreditorcurrbalold = ("" + creditoramount1);
//            } else if (sundrycreditorcurrbaltypeold.equals("CR")) {
//                if (creditoramount1 > totalamount) {
//                    creditoramount1 = creditoramount1 - totalamount;
//                    sundrycreditorcurrbalold = ("" + creditoramount1);
//
//                } else if (creditoramount1 <= totalamount) {
//                    creditoramount1 = totalamount - creditoramount1;
//                    sundrycreditorcurrbalold = ("" + creditoramount1);
//                    sundrycreditorcurrbaltypeold = ("DR");
//                }
//            }
//
//        }
//
//        if (jRadioButton3.isSelected()) {
////purchase and cash
//
//            System.out.println("code is running2");
//            String purchaseamount = jLabel31.getText();
//            String cashamount = jLabel29.getText();
//            double totalamount = Double.parseDouble(jTextField16.getText());
//            double purchaseamount1 = Double.parseDouble(purchaseamount);
//            double cashamount1 = Double.parseDouble(cashamount);
//            double totalamountaftervat = totalamount - Double.parseDouble(jTextField26.getText());
//            double bc = 0;
//            if (jLabel32.getText().equals("CR")) {
//                purchaseamount1 = purchaseamount1 + Double.parseDouble(jTextField13.getText());
//                jLabel31.setText("" + purchaseamount1);
//            } else if (jLabel32.getText().equals("DR")) {
//                if (purchaseamount1 > Double.parseDouble(jTextField13.getText())) {
//                    bc = (purchaseamount1 - Double.parseDouble(jTextField13.getText()));
//                    jLabel31.setText("" + bc);
//                }
//                if (Double.parseDouble(jTextField13.getText()) > purchaseamount1) {
//                    bc = (Double.parseDouble(jTextField13.getText()) - purchaseamount1);
//                    jLabel31.setText("" + bc);
//                    jLabel32.setText("CR");
//                }
//            }
//
//            if (jLabel30.getText().equals("DR")) {
//                cashamount1 = cashamount1 + totalamount;
//                jLabel29.setText("" + cashamount1);
//            } else if (jLabel30.getText().equals("CR")) {
//                if (cashamount1 > totalamount) {
//                    cashamount1 = cashamount1 - totalamount;
//                    jLabel29.setText("" + cashamount1);
//                }
//                if (cashamount1 < totalamount) {
//                    cashamount1 = totalamount - cashamount1;
//                    jLabel29.setText("" + cashamount1);
//                    jLabel30.setText("DR");
//                }
//            }
//        }
//
//    }

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to Update");
        if (i == 0) {
            try {
                String partypurchasedate = "";
                if (jDateChooser1.getDate() == null) {

                } else {
                    partypurchasedate = formatter.format(jDateChooser1.getDate());
                }
                connection c = new connection();
                Connection connect = c.cone();
                Statement stmt = connect.createStatement();
                if (!Party_Igst.equals("") || !Party_Sgst.equals("") && !Party_cgst.equals("")) {
                    System.out.println("Please Wait Account is Updating...");
                    updatereversebalances();
                    updatebalances();
                    stmt.executeUpdate("Update stock set Supplier_id = '" + jTextField22.getText() + "', Supplier_name = '" + jTextField12.getText() + "', Invoice_date = '" + formatter.format(jDateChooser2.getDate()) + "', Refrence_number = '" + jTextField4.getText() + "', Partypurchase_date = '" + partypurchasedate + "', IGST_Tax_Account = '" + Party_Igst + "', Igst_Amount = '" + jTextField27.getText() + "', cgsttaxaccount = '" + Party_cgst + "' , cgsttaxamount = '" + jTextField25.getText() + "', sgstaccount = '" + Party_Sgst + "', sgstamount = '" + jTextField26.getText() + "', otherchargesaccount = '" + other_charges + "', Other_charges = '" + jTextField14.getText() + "', to_ledger = '" + jTextField12.getText() + "', by_ledger = '" + jLabel48.getText() + "',discount = '" + jTextField23.getText() + "', amount_after_discount = '" + jTextField15.getText() + "', Net_amount = '"+jTextField16.getText()+"' where Invoice_number='" + jTextField2.getText() + "' ");
                    JOptionPane.showMessageDialog(rootPane, "Data updated successfully !!!");
                }
//                else if (Party_Igst.equals("") || Party_Sgst.equals("") && Party_cgst.equals("")) {
//                    System.out.println("Please Wait while Account is Updating...");
//                    updatereversebalances();
//                    updatebalances();
//                    stmt.executeUpdate("Update stock set Supplier_id = '" + jTextField22.getText() + "', Supplier_name = '" + jTextField12.getText() + "', Invoice_date = '" + formatter.format(jDateChooser2.getDate()) + "', Refrence_number = '" + jTextField4.getText() + "', Partypurchase_date = '" + partypurchasedate + "', otherchargesaccount = '" + other_charges + "', Other_charges = '" + jTextField14.getText() + "', to_ledger = '" + jTextField12.getText() + "', by_ledger = '" + jLabel48.getText() + "',discount = '" + jTextField23.getText() + "', amount_after_discount = '" + jTextField15.getText() + "' where Invoice_number='" + jTextField2.getText() + "' ");
//                    JOptionPane.showMessageDialog(rootPane, "Data updated successfully !!!");
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.dispose();
//        if (i == 0) {
//            updatereversebalances();
//            try {
//                connection c = new connection();
//                Connection connect = c.cone();
//
//                Statement stmt = connect.createStatement();
//                if (jRadioButton4.isSelected()) {
//                    try {
//                        System.out.println("code is running4 ledger update");
//                        String qdd = "Update ledger SET curr_bal= '" + sundrycreditorcurrbalold + "',currbal_type='" + sundrycreditorcurrbaltypeold + "' where ledger_name='" + sundrycreditorold + "'";
//                        stmt.executeUpdate(qdd);
//
//                        String qd = "Update ledger SET curr_bal= '" + purchaseaccountcurrbalold + "',currbal_type='" + purchaseaccountcurrbaltypeold + "' where ledger_name='" + purchaseaccountold + "'";
//                        stmt.executeUpdate(qd);
//                    } catch (Exception e) {
//                        System.out.println("" + e);
//                        e.printStackTrace();
//                    }
//                }
//
//                if (jRadioButton3.isSelected()) {
//                    try {
//                        String a = "Cash";
//
//                        String qd = "Update ledger SET curr_bal= '" + jLabel31.getText() + "',currbal_type='" + jLabel32.getText() + "' where ledger_name='" + a + "'";
//                        stmt.executeUpdate(qd);
//
//                        String qdd = "Update ledger SET curr_bal= '" + jLabel29.getText() + "',currbal_type='" + jLabel30.getText() + "' where ledger_name='" + Purchaseaccount + "'";
//                        stmt.executeUpdate(qdd);
//                    } catch (Exception e) {
//                        System.out.println("" + e);
//                        e.printStackTrace();
//                    }
//                }
//
//// For Sgst 
//                try {
//                    if ((vatold == null) || (vatold.equals("null"))) {
//
//                    } else {
//                        String qddsgst = ("Update ledger SET curr_bal= '" + vatcurrbalold + "',currbal_type='" + vatcurrbaltypeold + "' where ledger_name='" + vatold + "'");
//                        stmt.executeUpdate(qddsgst);
//
//                    }
//                } catch (Exception e) {
//                    System.out.println("" + e);
//                    e.printStackTrace();
//                }
//
//// For Cgst                
//                try {
//                    if ((cstold == null) || (cstold.equals(null))) {
//
//                    } else {
//                        String qddcgst = ("Update ledger SET curr_bal= '" + cstcurrbalold + "',currbal_type='" + cstcurrbaltypeold + "' where ledger_name='" + cstold + "'");
//                        stmt.executeUpdate(qddcgst);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//// For Igst
//                try {
//                    if ((igstold == null) || (igstold.equals(null))) {
//
//                    } else {
//                        String qddigst = ("Update ledger SET curr_bal= '" + igstcurrbalold + "',currbal_type='" + igstcurrbaltypeold + "' where ledger_name='" + igstold + "'");
//                        stmt.executeUpdate(qddigst);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
////------------------------------------------------------------------------------                    
//                try {
//                    if ((Double.parseDouble(jTextField23.getText()) != 0) && (Discountaccount == null)) {
//                        JOptionPane.showMessageDialog(rootPane, "Please choose Disount account");
//                    } else if ((Double.parseDouble(jTextField23.getText()) == 0) && (Discountaccount != null)) {
//
//                    } else {
//                        String qdddisc = ("Update ledger SET curr_bal= '" + updateddisccurrbal + "',currbal_type='" + updateddisccurrbaltyoe + "' where ledger_name='" + Discountaccount + "'");
//                        stmt.executeUpdate(qdddisc);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                try {
//                    if ((otherchargesold == null) || (otherchargesold.equals("null"))) {
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + otherchargescurrbalold + "',currbal_type='" + otherchargescurrbaltypeold + "' where ledger_name='" + otherchargesold + "'");
//                        stmt.executeUpdate(qddvat);
//                    }
//                } catch (Exception e) {
//                    System.out.println("" + e);
//                    e.printStackTrace();
//                }
//
//                ResultSet rss4 = stmt.executeQuery("select * from ledger where ledger_name='" + jTextField12.getText() + "'");
//                while (rss4.next()) {
//                    jLabel29.setText("" + rss4.getString(10));
//                    jLabel30.setText("" + rss4.getString(11));
//                }
//
//                ResultSet rss5 = stmt.executeQuery("select * from ledger where ledger_name='" + Purchaseaccount + "'");
//                while (rss5.next()) {
//                    jLabel31.setText("" + rss5.getString(10));
//                    jLabel32.setText("" + rss5.getString(11));
//                }
//
//                if (Party_vat == null) {
//
//                } else {
//                    ResultSet rs2 = stmt.executeQuery("select * from ledger where ledger_name='" + Party_vat + "'");
//                    while (rs2.next()) {
//                        jLabel38.setText(rs2.getString(10));
//                        jLabel39.setText(rs2.getString(11));
//                    }
//                }
//
//                if (Party_cst != null) {
//
//                } else {
//                    ResultSet rs3 = stmt.executeQuery("select * from ledger where ledger_name='" + Party_cst + "'");
//                    while (rs3.next()) {
//                        jLabel43.setText(rs3.getString(10));
//                        jLabel44.setText(rs3.getString(11));
//                    }
//                }
//
//                if (other_charges != null) {
//
//                } else {
//                    ResultSet rs4 = stmt.executeQuery("select * from ledger where ledger_name='" + other_charges + "'");
//                    while (rs4.next()) {
//                        jLabel45.setText(rs4.getString(10));
//                        jLabel46.setText(rs4.getString(11));
//                    }
//                }
//
//                updatebalances();
//
//                if (jRadioButton4.isSelected()) {
//                    try {
//                        System.out.println("code is running4 ledger update");
//                        String qdd = "Update ledger SET curr_bal= '" + jLabel29.getText() + "',currbal_type='" + jLabel30.getText() + "' where ledger_name='" + jTextField12.getText() + "'";
//                        stmt.executeUpdate(qdd);
//
//                        String qd = "Update ledger SET curr_bal= '" + jLabel31.getText() + "',currbal_type='" + jLabel32.getText() + "' where ledger_name='" + Purchaseaccount + "'";
//                        stmt.executeUpdate(qd);
//                    } catch (Exception e) {
//                        System.out.println("" + e);
//                        e.printStackTrace();
//                    }
//                }
//
//                if (jRadioButton3.isSelected()) {
//                    try {
//                        String a = "Cash";
//
//                        String qd = "Update ledger SET curr_bal= '" + jLabel31.getText() + "',currbal_type='" + jLabel32.getText() + "' where ledger_name='" + a + "'";
//                        stmt.executeUpdate(qd);
//
//                        String qdd = "Update ledger SET curr_bal= '" + jLabel29.getText() + "',currbal_type='" + jLabel30.getText() + "' where ledger_name='" + Purchaseaccount + "'";
//                        stmt.executeUpdate(qdd);
//                    } catch (Exception e) {
//                        System.out.println("" + e);
//                        e.printStackTrace();
//                    }
//                }
//
//                try {
//                    if ((Party_vat == null) && (Double.parseDouble(jTextField26.getText()) == 0)) {
//
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + jLabel38.getText() + "',currbal_type='" + jLabel39.getText() + "' where ledger_name='" + Party_vat + "'");
//                        stmt.executeUpdate(qddvat);
//
//                    }
//                } catch (Exception e) {
//                    System.out.println("" + e);
//                    e.printStackTrace();
//                }
//
//                try {
//                    if ((Double.parseDouble(jTextField23.getText()) != 0) && (Discountaccount == null)) {
//                        JOptionPane.showMessageDialog(rootPane, "Please choose Disount account");
//                    } else if ((Double.parseDouble(jTextField23.getText()) == 0) && (Discountaccount != null)) {
//
//                    } else {
//                        String qdddisc = ("Update ledger SET curr_bal= '" + updateddisccurrbal + "',currbal_type='" + updateddisccurrbaltyoe + "' where ledger_name='" + Discountaccount + "'");
//                        stmt.executeUpdate(qdddisc);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                try {
//                    if ((Party_cst == null) && (Double.parseDouble(jTextField25.getText()) == 0)) {
//
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + jLabel43.getText() + "',currbal_type='" + jLabel44.getText() + "' where ledger_name='" + Party_cst + "'");
//                        stmt.executeUpdate(qddvat);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    if ((Double.parseDouble(jTextField14.getText()) == 0) && (other_charges == null)) {
//                    } else {
//                        String qddvat = ("Update ledger SET curr_bal= '" + jLabel45.getText() + "',currbal_type='" + jLabel46.getText() + "' where ledger_name='" + other_charges + "'");
//                        stmt.executeUpdate(qddvat);
//                    }
//                } catch (Exception e) {
//                    System.out.println("" + e);
//                    e.printStackTrace();
//                }
//
//                stmt.executeUpdate("update stock set supplier_id='" + jTextField22.getText() + "',supplier_name='" + jTextField12.getText() + "',invoice_date='" + formatter.format(jDateChooser2.getDate()) + "',Refrence_number='" + jTextField4.getText() + "',bill_amount='" + jTextField13.getText() + "',discount='" + jTextField11.getText() + "',amount_after_discount='" + jTextField15.getText() + "',tax='" + jTextField17.getText() + "',other_charges='" + jTextField14.getText() + "',net_amount='" + jTextField16.getText() + "',vat='" + jTextField24.getText() + "',vatamount='" + jTextField26.getText() + "',taxamount='" + jTextField25.getText() + "',vataccount='" + Party_vat + "',to_ledger='" + jTextField12.getText() + "',by_ledger='" + Purchaseaccount + "',Partypurchase_date='" + formatter.format(jDateChooser1.getDate()) + "',taxaccount='" + Party_cst + "',otherchargesaccount='" + other_charges + "' where Invoice_number='" + jTextField2.getText() + "' ");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            JOptionPane.showMessageDialog(rootPane, "Your Purchase Entry is Updated Successfully");
//
//            dispose();
//        }


    }//GEN-LAST:event_jButton5ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        f = 0;
        s = 1;

        famount = 0;
        mount = 0;
        fvat = 0;
        ffvat = 0;


    }//GEN-LAST:event_formWindowClosed

    private void jTextField25FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField25FocusGained
        // TODO add your handling code here:
 //       cstbefore = Double.parseDouble(jTextField25.getText());
    }//GEN-LAST:event_jTextField25FocusGained

    private void list5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_list5ActionPerformed

    private void jComboBox3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox3KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jTextField6.requestFocus();
        }
    }//GEN-LAST:event_jComboBox3KeyPressed
    public String Party_Igst = null;
    private void list6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list6ActionPerformed
        jDialog9.dispose();
        show();
        if (list6.getSelectedItem().equals("ADD NEW")) {
            reporting.accounts.createLedger cr = new reporting.accounts.createLedger();
            cr.setVisible(true);
            cr.jComboBox1.setSelectedItem("DUTIES AND TAXES");
            cr.jComboBox5.setSelectedItem("OTHER");
        } else {

            Party_Igst = list6.getSelectedItem();
            String Igst_percent = null;
            try {
                connection c = new connection();
                Connection connect = c.cone();

                Statement st = connect.createStatement();
                ResultSet rs = st.executeQuery("Select * from ledger where ledger_name='" + Party_Igst + "' ");
                while (rs.next()) {
                    Igst_percent = rs.getString(6);
                    if (cashisselected) {
                        jLabel51.setText("" + rs.getString(10));
                        jLabel52.setText("" + rs.getString(11));
                    } else {
                        jLabel51.setText("" + rs.getString(10));
                        jLabel52.setText("" + rs.getString(11));

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            jTextField1.setText("" + Igst_percent);
        }
        jTextField27.requestFocus();
        calculationall();
    }//GEN-LAST:event_list6ActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            if (Double.parseDouble(jTextField1.getText()) != 0) {
                jDialog9.setVisible(true);
                list6.requestFocus();
                list6.removeAll();
                list6.addItem("ADD NEW");
                try {
                    connection c = new connection();
                    Connection connect = c.cone();

                    Statement st = connect.createStatement();
                    Statement st1 = connect.createStatement();
                    String duties = null;
                    String Duteis = "DUTIES AND TAXES";
                    ResultSet rs1 = st1.executeQuery("SELECT * FROM Group_create where Under='" + Duteis + "'");
                    while (rs1.next()) {
                        duties = rs1.getString(1);
                    }
                    ResultSet rs = st.executeQuery("SELECT * FROM ledger where groups='" + duties + "' or groups='" + Duteis + "' and type = 'IGST' ");
                    while (rs.next()) {
                        list6.add(rs.getString(1));
                    }
                } catch (SQLException sqe) {
                    System.out.println("SQl error");
                    sqe.printStackTrace();
                }
//                jTextField27.requestFocus();
//                hide();
            }
//            } else {
//                jTextField27.requestFocus();
//            }
        }
        if (key == evt.VK_ESCAPE) {
            jTextField25.requestFocus();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField27KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField27KeyPressed
        char key = evt.getKeyChar();
        if (key == evt.VK_ENTER) {
            jTextField1.requestFocus();
        }
        if (key == evt.VK_ESCAPE) {
            jTextField24.requestFocus();
        }
    }//GEN-LAST:event_jTextField27KeyPressed
    double Igst_before = 0;
    double Igst_after = 0;
    private void jTextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField1FocusLost
//        Igst_after = Double.parseDouble(jTextField1.getText());
//        if (Igst_after != Igst_before) {
//            calculationall();
//        } else {
//
//        }
    }//GEN-LAST:event_jTextField1FocusLost

    private void jTextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField1FocusGained
//        Igst_before = Double.parseDouble(jTextField1.getText());
    }//GEN-LAST:event_jTextField1FocusGained

    private void jTextField26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField26ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField26ActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField11ActionPerformed

    private void jTextField27FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField27FocusLost
        rigidcalculation(); 
    }//GEN-LAST:event_jTextField27FocusLost

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Purchasetransaction.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Purchasetransaction.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Purchasetransaction.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Purchasetransaction.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Purchasetransaction().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    public javax.swing.JComboBox jComboBox1;
    public javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    public javax.swing.JComboBox jComboBox4;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    public com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JDialog jDialog3;
    private javax.swing.JDialog jDialog4;
    private javax.swing.JDialog jDialog5;
    private javax.swing.JDialog jDialog6;
    private javax.swing.JDialog jDialog7;
    private javax.swing.JDialog jDialog8;
    private javax.swing.JDialog jDialog9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    public javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel30;
    public javax.swing.JLabel jLabel31;
    public javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    public javax.swing.JLabel jLabel38;
    public javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    public javax.swing.JLabel jLabel43;
    public javax.swing.JLabel jLabel44;
    public javax.swing.JLabel jLabel45;
    public javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    public javax.swing.JLabel jLabel48;
    public javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel50;
    public javax.swing.JLabel jLabel51;
    public javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    public javax.swing.JRadioButton jRadioButton3;
    public javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField10;
    public javax.swing.JTextField jTextField11;
    public javax.swing.JTextField jTextField12;
    public javax.swing.JTextField jTextField13;
    public javax.swing.JTextField jTextField14;
    public javax.swing.JTextField jTextField15;
    public javax.swing.JTextField jTextField16;
    public javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    public javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    public javax.swing.JTextField jTextField22;
    public javax.swing.JTextField jTextField23;
    public javax.swing.JTextField jTextField24;
    public javax.swing.JTextField jTextField25;
    public javax.swing.JTextField jTextField26;
    public javax.swing.JTextField jTextField27;
    public javax.swing.JTextField jTextField28;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    public javax.swing.JTextField jTextField6;
    public javax.swing.JTextField jTextField7;
    public javax.swing.JTextField jTextField8;
    public javax.swing.JTextField jTextField9;
    private java.awt.List list1;
    private java.awt.List list2;
    private java.awt.List list3;
    private java.awt.List list4;
    private java.awt.List list5;
    private java.awt.List list6;
    // End of variables declaration//GEN-END:variables
}
