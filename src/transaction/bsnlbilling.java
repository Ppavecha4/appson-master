package transaction;

import connection.connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Prateek^
 */
public class bsnlbilling extends javax.swing.JFrame {

    /**
     * Creates new form bsnlbilling
     */
    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String dateNow = formatter.format(currentDate.getTime());

    SimpleDateFormat yearnow = new SimpleDateFormat("yy");
    String getyear = yearnow.format(currentDate.getTime());
    String nextyear = Integer.toString(Integer.parseInt(getyear) + 1);

    public bsnlbilling() {
        initComponents();
        jButton3.setVisible(false);
        try {
            System.out.println("date now is" + formatter.parse(dateNow));
            System.out.println("date after is" + formatter.parse("31-03-20" + getyear + ""));
            java.util.Date dateafter = formatter.parse("31-03-20" + getyear + "");
            java.util.Date datenow = formatter.parse(dateNow);
            if (datenow.before(dateafter)) {
                getyear = Integer.toString(Integer.parseInt(getyear) - 1);
                nextyear = Integer.toString(Integer.parseInt(getyear) + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            connection c = new connection();
            Connection connect = c.cone();

            Statement stmt = connect.createStatement();
            String sales = "BSL/";
            String slash = "/";
            //    String Branchno = "1/";

            String yearwise = (getyear.concat(nextyear)).concat(slash);
            String finalconcat = ((sales.concat(yearwise)));

            ResultSet rs4 = stmt.executeQuery("SELECT s_no FROM bsnlbilling ");
            int a = 0, b, d = 0;
            int i1 = 0;
            while (rs4.next()) {
                if (i1 == 0) {
                    d = rs4.getString(1).lastIndexOf("/");
                    i1++;
                }
                b = Integer.parseInt(rs4.getString(1).substring(d + 1));
                if (a < b) {
                    a = b;
                }
            }
            String s = finalconcat.concat(Integer.toString(a + 1));
            jTextField1.setText(s);
            jDateChooser1.setDate(formatter.parse(dateNow));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Updating bsnlbilling table and ledger table
    public void updateEntry() {
        int response = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to update ?");
        String cust_name = "";
        double amnt = 0;
        double disc_amnt = 0;
        double net_amnt = 0;

        double bsnlcashcurrbal = 0;
        String bsnlcashcurrbaltype = "";
        double bsnlaccountupdateamount = 0;

        double bsnldisccurrbal = 0;
        String bsnldisccurrbaltype = "";
        double bsnldiscaccountupdateamount = 0;

        double bsnllapucurrbal = 0;
        String bsnllapucurrbaltype = "";
        double bsnllapuaccountupdateamount = 0;
        switch (response) {
            case JOptionPane.YES_OPTION:
                try {
                    connection c = new connection();
                    Connection connect = c.cone();
                    Statement stmt = connect.createStatement();
                    Statement stmt1 = connect.createStatement();
                    Statement stmt2 = connect.createStatement();
                    Statement stmt3 = connect.createStatement();

                    String date = formatter.format(jDateChooser1.getDate());

                    ResultSet rs = stmt.executeQuery("Select * from bsnlbilling where s_no = '" + jTextField1.getText() + "'");
                    while (rs.next()) {
                        cust_name = rs.getString("customer_name");
                        amnt = Double.parseDouble(rs.getString("amnt"));
                        disc_amnt = Double.parseDouble(rs.getString("disc_amnt"));
                        net_amnt = Double.parseDouble(rs.getString("net_amnt"));
                    }

                    // Restoring ledger table to its previous state    
                    ResultSet rs1 = stmt.executeQuery("select * from ledger where ledger_name='BSNL CASH'");
                    while (rs1.next()) {
                        bsnlcashcurrbal = Double.parseDouble(rs1.getString("curr_bal"));
                        bsnlcashcurrbaltype = rs1.getString("currbal_type");
                    }
                    if (bsnlcashcurrbaltype.equals("CR")) {
                        bsnlaccountupdateamount = bsnlcashcurrbal + net_amnt;
                        if (bsnlaccountupdateamount > 0) {
                            bsnlcashcurrbaltype = "CR";
                            bsnlaccountupdateamount = (Math.abs(bsnlaccountupdateamount));
                        }
                        if (bsnlaccountupdateamount <= 0) {
                            bsnlcashcurrbaltype = "DR";
                            bsnlaccountupdateamount = Math.abs(bsnlaccountupdateamount);
                        }
                    } else if (bsnlcashcurrbaltype.equals("DR")) {
                        bsnlaccountupdateamount = bsnlcashcurrbal - net_amnt;
                        bsnlcashcurrbaltype = "DR";
                        bsnlaccountupdateamount = Math.abs(bsnlaccountupdateamount);
                    }
                    stmt.executeUpdate("Update ledger set curr_bal='" + bsnlaccountupdateamount + "" + "',currbal_type='" + bsnlcashcurrbaltype + "' where ledger_name='BSNL CASH'");

                    ResultSet rs2 = stmt.executeQuery("select * from ledger where ledger_name='BSNL DISCOUNT A/C'");

                    while (rs2.next()) {
                        bsnldisccurrbal = Double.parseDouble(rs2.getString("curr_bal"));
                        bsnldisccurrbaltype = rs2.getString("currbal_type");
                    }
                    if (bsnldisccurrbaltype.equals("CR")) {
                        bsnldiscaccountupdateamount = bsnldisccurrbal + disc_amnt;
                        if (bsnldiscaccountupdateamount > 0) {
                            bsnldisccurrbaltype = "CR";
                            bsnldiscaccountupdateamount = (Math.abs(bsnldiscaccountupdateamount));
                        }
                        if (bsnldiscaccountupdateamount <= 0) {
                            bsnldisccurrbaltype = "DR";
                            bsnldiscaccountupdateamount = Math.abs(bsnldiscaccountupdateamount);
                        }
                    } else if (bsnldisccurrbaltype.equals("DR")) {
                        bsnldiscaccountupdateamount = bsnldisccurrbal - disc_amnt;
                        bsnldisccurrbaltype = "DR";
                        bsnldiscaccountupdateamount = Math.abs(bsnldiscaccountupdateamount);
                    }
                    stmt.executeUpdate("Update ledger set curr_bal='" + bsnldiscaccountupdateamount + "" + "',currbal_type='" + bsnldisccurrbaltype + "' where ledger_name='BSNL DISCOUNT A/C'");

                    ResultSet rs3 = stmt.executeQuery("select * from ledger where ledger_name='BSNL LAPU  BELANCE'");

                    while (rs3.next()) {
                        bsnllapucurrbal = Double.parseDouble(rs3.getString("curr_bal"));
                        bsnllapucurrbaltype = rs3.getString("currbal_type");
                    }
                    if (bsnllapucurrbaltype.equals("DR")) {
                        bsnllapuaccountupdateamount = bsnllapucurrbal + amnt;

                        System.out.println("lapu updated amount is " + bsnllapuaccountupdateamount);
                        if (bsnllapuaccountupdateamount > 0) {
                            bsnllapucurrbaltype = "DR";
                            bsnllapuaccountupdateamount = (Math.abs(bsnllapuaccountupdateamount));
                        }
                        if (bsnllapuaccountupdateamount <= 0) {
                            bsnllapucurrbaltype = "CR";
                            bsnllapuaccountupdateamount = Math.abs(bsnllapuaccountupdateamount);
                        }
                    } else if (bsnllapucurrbaltype.equals("CR")) {
                        bsnllapuaccountupdateamount = bsnllapucurrbal - amnt;
                        bsnllapucurrbaltype = "CR";
                        bsnllapuaccountupdateamount = Math.abs(bsnllapuaccountupdateamount);
                    }
                    stmt.executeUpdate("Update ledger set curr_bal='" + bsnllapuaccountupdateamount + "" + "',currbal_type='" + bsnllapucurrbaltype + "' where ledger_name='BSNL LAPU  BELANCE'");

                    // Updating bsnbilling value and calling updatebalancce method for updating ledger table        
                    stmt.executeUpdate("Update bsnlbilling set date = '" + date + "',customer_name = '" + jTextField3.getText() + "', amnt = '" + jTextField4.getText() + "', disc_amnt = '" + jTextField5.getText() + "', net_amnt = '" + jTextField6.getText() + "', to_ledger = 'BSNL CASH', by_ledger = 'BSNL LAPU  BELANCE', disc_ledger = 'BSNL DISCOUNT A/C', narration = '" + jTextArea1.getText() + "' where s_no = '"+jTextField1.getText()+"' ");

                    updatebalance();

                    stmt1.executeUpdate("Update ledger set curr_bal='" + cashaccountupdateamount + "" + "',currbal_type='" + cashcurrbaltype + "' where ledger_name='BSNL CASH'");
                    stmt2.executeUpdate("Update ledger set curr_bal='" + discaccountupdateamount + "" + "',currbal_type='" + disccurrbaltype + "' where ledger_name='BSNL DISCOUNT A/C'");
                    stmt3.executeUpdate("Update ledger set curr_bal='" + lapuaccountupdateamount + "" + "',currbal_type='" + lapucurrbaltype + "' where ledger_name='BSNL LAPU  BELANCE'");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.dispose();
                break;
            case JOptionPane.NO_OPTION:
                this.dispose();
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Bsnl Billing");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel1.setText("S No. : -");

        jTextField1.setEditable(false);

        jLabel2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel2.setText("Date: -");

        jLabel3.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel3.setText("Customer Name: -");

        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel4.setText("Amount");

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField4KeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel5.setText("Discount");

        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField5KeyPressed(evt);
            }
        });

        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField6KeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel6.setText("Net Amount");

        jButton1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButton2.setText("CANCEL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel7.setText("Narration: -");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setText("LAPU BALANCE");
        jTextArea1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextArea1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextArea1);

        jDateChooser1.setDateFormatString("dd-MM-yyy");

        jButton3.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButton3.setText("Update");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel7)
                    .addComponent(jLabel1))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jButton3))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                                .addComponent(jLabel2)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jButton2))))
                    .addComponent(jScrollPane1)
                    .addComponent(jTextField3))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGap(123, 123, 123))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButton2)
                                .addComponent(jButton3)))
                        .addGap(27, 27, 27))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:

        jDateChooser1.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jTextField4.requestFocus();
        }
    }//GEN-LAST:event_jTextField3KeyPressed

    private void jTextField4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jTextField5.requestFocus();
        }
    }//GEN-LAST:event_jTextField4KeyPressed

    private void jTextField5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyPressed
        // TODO add your handling code here:

        double netamnt = 0;
        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jTextField6.requestFocus();

            netamnt = Double.parseDouble(jTextField4.getText()) - Double.parseDouble(jTextField5.getText());
            jTextField6.setText((int) netamnt + "");
        }
    }//GEN-LAST:event_jTextField5KeyPressed
    public boolean issave = false;

    public void save() {
        int i = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to save");
        if (i == 0 && issave == false) {
            issave = true;
            updatebalance();
            try {
                connection c = new connection();
                Connection connect = c.cone();
                Statement stmt = connect.createStatement();
                Statement stmt1 = connect.createStatement();
                Statement stmt2 = connect.createStatement();
                Statement stmt3 = connect.createStatement();

                stmt.executeUpdate("insert into bsnlbilling values ('" + jTextField1.getText() + "','" + formatter.format(jDateChooser1.getDate()) + "','" + jTextField3.getText() + "','" + jTextField4.getText() + "','" + jTextField5.getText() + "','" + jTextField6.getText() + "','BSNL CASH','BSNL LAPU  BELANCE','BSNL DISCOUNT A/C','" + jTextArea1.getText() + "')");
                stmt1.executeUpdate("Update ledger set curr_bal='" + cashaccountupdateamount + "" + "',currbal_type='" + cashcurrbaltype + "' where ledger_name='BSNL CASH'");
                stmt2.executeUpdate("Update ledger set curr_bal='" + discaccountupdateamount + "" + "',currbal_type='" + disccurrbaltype + "' where ledger_name='BSNL DISCOUNT A/C'");
                stmt3.executeUpdate("Update ledger set curr_bal='" + lapuaccountupdateamount + "" + "',currbal_type='" + lapucurrbaltype + "' where ledger_name='BSNL LAPU  BELANCE'");

            } catch (Exception e) {
                e.printStackTrace();
            }
            dispose();
            cashcurrbal = 0;
            cashcurrbaltype = "";
            cashaccountupdateamount = 0;
            lapucurrbal = 0;
            lapucurrbaltype = "";
            lapuaccountupdateamount = 0;
            disccurrbal = 0;
            disccurrbaltype = "";
            discaccountupdateamount = 0;
            transaction.bsnlbilling bs = new bsnlbilling();
            bs.setVisible(true);
            bs.jDateChooser1.setDate(jDateChooser1.getDate());

        }
    }

    double cashcurrbal = 0;
    String cashcurrbaltype = "";
    double cashaccountupdateamount = 0;

    double lapucurrbal = 0;
    String lapucurrbaltype = "";
    double lapuaccountupdateamount = 0;

    double disccurrbal = 0;
    String disccurrbaltype = "";
    double discaccountupdateamount = 0;

    public void updatebalance() {
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement stmt = connect.createStatement();
            Statement stmt1 = connect.createStatement();
            Statement stmt2 = connect.createStatement();

            ResultSet rs = stmt.executeQuery("select * from ledger where ledger_name='BSNL CASH'");
            while (rs.next()) {
                cashcurrbal = Double.parseDouble(rs.getString(10));
                cashcurrbaltype = rs.getString(11);
            }
            double totalamnt = Double.parseDouble(jTextField6.getText());
            if (cashcurrbaltype.equals("CR")) {
                cashaccountupdateamount = cashcurrbal - totalamnt;
                if (cashaccountupdateamount > 0) {
                    cashcurrbaltype = "CR";
                    cashaccountupdateamount = (Math.abs(cashaccountupdateamount));
                }
                if (cashaccountupdateamount <= 0) {
                    cashcurrbaltype = "DR";
                    cashaccountupdateamount = Math.abs(cashaccountupdateamount);
                }
            } else if (cashcurrbaltype.equals("DR")) {
                cashaccountupdateamount = cashcurrbal + totalamnt;
                cashcurrbaltype = "DR";
                cashaccountupdateamount = Math.abs(cashaccountupdateamount);
            }

            ResultSet rs1 = stmt.executeQuery("select * from ledger where ledger_name='BSNL DISCOUNT A/C'");

            while (rs1.next()) {
                disccurrbal = Double.parseDouble(rs1.getString(10));
                disccurrbaltype = rs1.getString(11);
            }

            double discamnt = Double.parseDouble(jTextField5.getText());

            if (disccurrbaltype.equals("CR")) {
                discaccountupdateamount = disccurrbal - discamnt;
                if (discaccountupdateamount > 0) {
                    disccurrbaltype = "CR";
                    discaccountupdateamount = (Math.abs(discaccountupdateamount));
                }
                if (discaccountupdateamount <= 0) {
                    disccurrbaltype = "DR";
                    discaccountupdateamount = Math.abs(discaccountupdateamount);
                }
            } else if (disccurrbaltype.equals("DR")) {
                discaccountupdateamount = disccurrbal + discamnt;
                disccurrbaltype = "DR";
                discaccountupdateamount = Math.abs(discaccountupdateamount);
            }
            ResultSet rs2 = stmt2.executeQuery("select * from ledger where ledger_name='BSNL LAPU  BELANCE'");

            while (rs2.next()) {
                lapucurrbal = Double.parseDouble(rs2.getString(10));
                lapucurrbaltype = rs2.getString(11);
            }
            double amnt = Double.parseDouble(jTextField4.getText());

            if (lapucurrbaltype.equals("DR")) {
                lapuaccountupdateamount = lapucurrbal - amnt;

                System.out.println("lapu updated amount is " + lapuaccountupdateamount);
                if (lapuaccountupdateamount > 0) {
                    lapucurrbaltype = "DR";
                    lapuaccountupdateamount = (Math.abs(lapuaccountupdateamount));
                }
                if (lapuaccountupdateamount <= 0) {
                    lapucurrbaltype = "CR";
                    lapuaccountupdateamount = Math.abs(lapuaccountupdateamount);
                }
            } else if (lapucurrbaltype.equals("CR")) {
                lapuaccountupdateamount = lapucurrbal + amnt;
                lapucurrbaltype = "CR";
                lapuaccountupdateamount = Math.abs(lapuaccountupdateamount);
            }
        } catch (Exception e) {

        }
    }


    private void jTextField6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jTextArea1.requestFocus();
        }
    }//GEN-LAST:event_jTextField6KeyPressed


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        save();

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextArea1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            jButton1.requestFocus();
        }
    }//GEN-LAST:event_jTextArea1KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:

        int key = evt.getKeyCode();

        if (key == evt.VK_ENTER) {
            save();
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        updateEntry();
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(bsnlbilling.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(bsnlbilling.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(bsnlbilling.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(bsnlbilling.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new bsnlbilling().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTextArea jTextArea1;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    public javax.swing.JTextField jTextField5;
    public javax.swing.JTextField jTextField6;
    // End of variables declaration//GEN-END:variables
}
