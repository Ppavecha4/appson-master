package transaction;

import connection.connection;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.JFileChooser;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFPrintSetup;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author PRATEEK
 */
public class cashbook extends javax.swing.JFrame {
    /**
     * Creates new form daybook
     */
    Vector heads = new Vector();

    DefaultTableModel dtm;

    Calendar currentDate = Calendar.getInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
    String DateNow = formatter.format(currentDate.getTime());
    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyy");
    String datenow = formatter1.format(currentDate.getTime());
    XSSFWorkbook workbook = new XSSFWorkbook();
    private static cashbook obj = null;

    public cashbook() {

        initComponents();
        this.setLocationRelativeTo(null);

        dtm = (DefaultTableModel) jTable1.getModel();
        jLabel2.setText(datenow);
        try {
            connection c = new connection();
            Connection conne = c.cone();
            Statement stmt = conne.createStatement();

            ResultSet rs = stmt.executeQuery("select * from ledger where groups='CASH IN HAND' or groups='SALES ACCOUNTS' ");

            while (rs.next()) {
                daybookmethod(rs.getString(1), DateNow);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static cashbook getObj() {
        if (obj == null) {
            obj = new cashbook();
        }
        return obj;
    }

    public void daybookmethod(String choice, String DateNow) {

        try {
            connection c = new connection();
            Connection conne = c.cone();
            Statement stmt1 = conne.createStatement();

            Vector data = new Vector();
            Vector rowvector = new Vector();
            int i = 0;

            ResultSet rs = stmt1.executeQuery("Select open_bal,bal_type from ledger where ledger_name='" + choice + "'");

            while (rs.next()) {
                rowvector = new Vector();
                String openbaltype = rs.getString(2);
                if (openbaltype.equals("CR")) {
                    rowvector.add("2015-03-31");
                    rowvector.add("");
                    rowvector.add("");
                    rowvector.add("OPENING BALANCE");
                    rowvector.add(rs.getString(1));
                    rowvector.add("");

                    data.add(rowvector);
                } else {
                    rowvector.add("2016-03-31");
                    rowvector.add("");
                    rowvector.add("");
                    rowvector.add("OPENING BALANCE");
                    rowvector.add("");
                    rowvector.add(rs.getString(1));

                    data.add(rowvector);
                }

            }

            ResultSet rs2 = stmt1.executeQuery("select * from Contra where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(v_date as date) <'" + DateNow + "' ");

            String matchsno4 = "test";
            while (rs2.next()) {
                rowvector = new Vector();
                String matchsno1 = rs2.getString(1);

                String abyledger = rs2.getString(3);
                String atoledger = rs2.getString(4);

                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(abyledger)) {

                        rowvector.add(rs2.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add(rs2.getString(5));
                        rowvector.add("");
                        data.add(rowvector);

                    } else if (choice.equals(atoledger)) {
                        rowvector.add(rs2.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add("");
                        rowvector.add(rs2.getString(5));
                        data.add(rowvector);

                    }
                    matchsno4 = matchsno1;
                }

            }

            ResultSet rs3 = stmt1.executeQuery("select * from interestentry where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(date as date) <'" + DateNow + "'");

            matchsno4 = "test";
            while (rs3.next()) {
                rowvector = new Vector();
                String matchsno1 = rs3.getString(1);

                String abyledger = rs3.getString("by_ledger");
                String atoledger = rs3.getString("to_ledger");

                {
                    if (choice.equals(abyledger)) {

                        rowvector.add(rs3.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add(rs3.getString(7));
                        rowvector.add(atoledger);
                        rowvector.add(rs3.getString(4));
                        rowvector.add("");
                        data.add(rowvector);

                    } else if (choice.equals(atoledger)) {
                        rowvector.add(rs3.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add(rs3.getString(7));
                        rowvector.add(abyledger);
                        rowvector.add("");
                        rowvector.add(rs3.getString(4));
                        data.add(rowvector);

                    }
                    //     matchsno4 = matchsno1;
                }

            }

            Statement stmtbsnl = conne.createStatement();
            ResultSet rsbsnl = stmtbsnl.executeQuery("select * from bsnlbilling where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "'  or disc_ledger='" + choice + "') and CAST(date as date)<'" + DateNow + "'");

            while (rsbsnl.next()) {
                rowvector = new Vector();
                String matchsno1 = rsbsnl.getString(1);

                String abyledger = rsbsnl.getString(8);
                String atoledger = rsbsnl.getString(7);
                String adiscledger = rsbsnl.getString(9);

                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(abyledger)) {

                        rowvector.add(rsbsnl.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add(rsbsnl.getString(4));
                        rowvector.add("");
                        data.add(rowvector);

                    } else if (choice.equals(atoledger)) {
                        rowvector.add(rsbsnl.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(abyledger);
                        rowvector.add("");
                        rowvector.add(rsbsnl.getString(6));
                        data.add(rowvector);

                    } else if (choice.equals(adiscledger)) {
                        rowvector.add(rsbsnl.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(abyledger);
                        rowvector.add("");
                        rowvector.add(rsbsnl.getString(5));
                        data.add(rowvector);

                    }
                    matchsno4 = matchsno1;
                }

            }

            Statement stmtjourn = conne.createStatement();
            ResultSet rsjourn = stmtjourn.executeQuery("select * from journal where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)<'" + DateNow + "' ");

            matchsno4 = "test";
            while (rsjourn.next()) {
                rowvector = new Vector();
                String matchsno1 = rsjourn.getString(2);
                String atoledger = rsjourn.getString(5);
                String abyledger = rsjourn.getString(4);
                if (choice.equals(atoledger)) {
                    if (rsjourn.getString(8).equals("") || rsjourn.getString(8).equals(null)) {
                    } else {
                        rowvector.add(rsjourn.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(abyledger);
                        rowvector.add(rsjourn.getString(8));
                        rowvector.add("");
                        //   matchsno4 = matchsno1;
                        data.add(rowvector);
                    }
                } else if (choice.equals(abyledger)) {
                    if (rsjourn.getString(7).equals("") || rsjourn.getString(7).equals(null)) {
                    } else {

                        rowvector.add(rsjourn.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add("");
                        rowvector.add(rsjourn.getString(7));

                        //    matchsno4 = matchsno1;
                        data.add(rowvector);
                    }
                }

            }
            Statement stmtpayment = conne.createStatement();
            ResultSet rspayment = stmtpayment.executeQuery("select * from payment where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)<'" + DateNow + "' ");

            matchsno4 = "test";
            while (rspayment.next()) {
                rowvector = new Vector();
                String matchsno1 = rspayment.getString(1);
                String byledger = rspayment.getString(3);
                String toledger = rspayment.getString(4);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(byledger)) {
                        rowvector.add(rspayment.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(toledger);
                        rowvector.add(rspayment.getString(5));
                        rowvector.add("");
                    } else if (choice.equals(toledger)) {
                        rowvector.add(rspayment.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(byledger);
                        rowvector.add("");
                        rowvector.add(rspayment.getString(5));
                    }
                    matchsno4 = matchsno1;
                    data.add(rowvector);
                }

            }

//cash reciept
            Statement stmtreciept = conne.createStatement();
            ResultSet rsreciept = stmtreciept.executeQuery("select * from receipt where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)<'" + DateNow + "' ");

            matchsno4 = "test";
            while (rsreciept.next()) {
                rowvector = new Vector();
                String matchsno1 = rsreciept.getString(1);
                String byledger = rsreciept.getString(3);
                String toledger = rsreciept.getString(4);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(byledger)) {
                        rowvector.add(rsreciept.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(toledger);
                        rowvector.add("");
                        rowvector.add(rsreciept.getString(5));
                    } else if (choice.equals(toledger)) {
                        rowvector.add(rsreciept.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(byledger);
                        rowvector.add(rsreciept.getString(5));
                        rowvector.add("");
                    }
                    matchsno4 = matchsno1;
                    data.add(rowvector);
                }

            }

            Statement stmtbankpayment = conne.createStatement();
            ResultSet rsbankpayment = stmtbankpayment.executeQuery("select * from bankpayment where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date) < '" + DateNow + "' ");

            matchsno4 = "test";
            while (rsbankpayment.next()) {
                rowvector = new Vector();
                String matchsno1 = rsbankpayment.getString(1);
                String abyledger = rsbankpayment.getString(3);
                String atoledger = rsbankpayment.getString(4);
                if (choice.equals(abyledger)) {
                    rowvector.add(rsbankpayment.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankpayment.getString(10));
                    rowvector.add(atoledger);
                    rowvector.add(rsbankpayment.getString(5));
                    rowvector.add("");
                    matchsno4 = matchsno1;
                    data.add(rowvector);
                } else if (choice.equals(atoledger)) {
//                 if (matchsno4.equals(matchsno1)) {
//                } 
                    //             else {
                    rowvector.add(rsbankpayment.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankpayment.getString(10));
                    rowvector.add(abyledger);
                    rowvector.add("");
                    rowvector.add(rsbankpayment.getString(5));

                    //      matchsno4 = matchsno1;
                    data.add(rowvector);
                    //   }           
                }

            }

            Statement stmtbankreciept = conne.createStatement();
            ResultSet rsbankreciept = stmtbankreciept.executeQuery("select * from bankreceipt where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)<'" + DateNow + "' ");

            matchsno4 = "test";
            while (rsbankreciept.next()) {
                rowvector = new Vector();
                String matchsno1 = rsbankreciept.getString(1);
                String abyledger = rsbankreciept.getString(3);
                String atoledger = rsbankreciept.getString(4);
                if (choice.equals(abyledger)) {
                    rowvector.add(rsbankreciept.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankreciept.getString(10));
                    rowvector.add(atoledger);
                    rowvector.add("");
                    rowvector.add(rsbankreciept.getString(5));
                    matchsno4 = matchsno1;
                    data.add(rowvector);
                } else if (choice.equals(atoledger)) {
//                 if (matchsno4.equals(matchsno1)) {
//                } 
                    //             else {

                    rowvector.add(rsbankreciept.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankreciept.getString(10));
                    rowvector.add(abyledger);
                    rowvector.add(rsbankreciept.getString(5));
                    rowvector.add("");

                    //      matchsno4 = matchsno1;
                    data.add(rowvector);
                    //   }           
                }

            }

            Statement stmtpur = conne.createStatement();

            if (choice.equals("DISCOUNT ACCOUNT")) {
                ResultSet rspur = stmtpur.executeQuery("select * from Stock where  discount !=0 ");
                //parti = null;
                matchsno4 = "test";
                while (rspur.next()) {
                    rowvector = new Vector();

                    String matchsno1 = rspur.getString(4);
                    if (matchsno4.equals(matchsno1)) {

                    } else {
                        Double discountamount = (Double.parseDouble(rspur.getString(16))) - (Double.parseDouble(rspur.getString(18)));
                        rowvector.add(rspur.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add("Discount Entry");
                        rowvector.add(rspur.getString(2));
                        rowvector.add(discountamount);
                        rowvector.add("");
                        matchsno4 = matchsno1;
                        data.add(rowvector);
                    }
                }

            } else {
                ResultSet rspur = stmtpur.executeQuery("select * from Stock where  to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or sgstaccount ='" + choice + "'  or cgsttaxaccount='" + choice + "' or igst_tax_account ='" + choice + "' or otherchargesaccount='" + choice + "' and CAST(invoice_date as date)<'" + DateNow + "'");
                //  parti = null;
                matchsno4 = "test";
                while (rspur.next()) {
                    rowvector = new Vector();
                    String matchsno1 = rspur.getString(4);
                    String asgstaccount = rspur.getString("sgstaccount");
                    String acgstaccount = rspur.getString("cgsttaxaccount");
                    String aigstaccount = rspur.getString("Igst_tax_Account");
                    String aothercharges = rspur.getString("otherchargesaccount");
                    String netamount = "", atoledger = "", netamount1 = "";
                    String abyledger = rspur.getString("by_ledger");
                    if (choice.equals(asgstaccount)) {
                        netamount1 = rspur.getString("sgstamount");
                        atoledger = rspur.getString("to_ledger");
                    } else if (choice.equals(aigstaccount)) {
                        netamount1 = rspur.getString("igst_amount");
                        atoledger = rspur.getString("to_ledger");
                    } else if (choice.equals(aothercharges)) {
                        netamount1 = rspur.getString("Other_charges");
                        atoledger = rspur.getString("to_ledger");
                        netamount = "";
                    } else if (choice.equals(acgstaccount)) {
                        netamount1 = rspur.getString("cgsttaxamount");
                        atoledger = rspur.getString("to_ledger");
                    } else if (choice.equals(abyledger)) {
                        atoledger = rspur.getString("to_ledger");
                        netamount1 = rspur.getString("Bill_amount");
                    } else {

                        netamount = rspur.getString("Net_amount");
                        atoledger = rspur.getString("by_ledger");
                    }
                    if (matchsno4.equals(matchsno1)) {
                    } else {

                        rowvector.add(rspur.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add(rspur.getString(6));
                        rowvector.add(atoledger);
                        rowvector.add(netamount);
                        rowvector.add(netamount1);

                        matchsno4 = matchsno1;
                        data.add(rowvector);
                    }
                }
            }

            Statement stmtsales = conne.createStatement();
            ResultSet rssales = stmtsales.executeQuery("select * from billing  where (cashaccount= '" + choice + "' or sundrydebtorsaccount='" + choice + "' or cardaccount='" + choice + "' or by_ledger = '" + choice + "' or vataccount='" + choice + "' or Sgstaccount='" + choice + "' or Cgstaccount='" + choice + "' or '" + choice + "'='DISCOUNT ACCOUNT') and CAST(date as date)<'" + DateNow + "'");
            // System.out.println(""+"select * from Stock where Supplier_name = '" + choice + "' or Purchase_type = '" + choice + "' or vataccount='"+choice+"'");
            // parti = null;
            matchsno4 = "test";
            while (rssales.next()) {
                rowvector = new Vector();
                String matchsno1 = rssales.getString("bill_refrence");
                double discamount = Double.parseDouble(rssales.getString("total_disc_amnt"));
                String avatacc = rssales.getString("vataccount");
                String asgstacc = rssales.getString("sgstaccount");
                String acgstacc = rssales.getString("cgstaccount");
                String acashaccount = rssales.getString("cashaccount");
                String asundrydebtorsaccount = rssales.getString("sundrydebtorsaccount");
                String acardaccount = rssales.getString("cardaccount");
                String netamount = "", atoledger = rssales.getString("to_ledger"), abyledger = rssales.getString("by_ledger");
                String netamount1 = "";
                String Description = rssales.getString("customer_name");
                ResultSet rscalc = null;
                Statement stm = conne.createStatement();
                if ((choice.equals(avatacc)) || (choice.equals(asgstacc)) || (choice.equals(acgstacc))) {

                    if (choice.equals(avatacc)) {
                        rscalc = stm.executeQuery("select sum(vatamnt) from billing where bill_refrence ='" + matchsno1 + "'  ");
                    } else if (choice.equals(asgstacc)) {
                        rscalc = stm.executeQuery("select sum(sgstamnt) from billing where bill_refrence ='" + matchsno1 + "'  ");
                    } else if (choice.equals(acgstacc)) {
                        rscalc = stm.executeQuery("select sum(cgstamnt) from billing where bill_refrence ='" + matchsno1 + "'  ");
                    }
                    double calculation = 0;

                    while (rscalc.next()) {
                        calculation = rscalc.getDouble(1);
                    }
                    netamount = "" + calculation;
                    atoledger = rssales.getString(acashaccount + asundrydebtorsaccount + acardaccount);
                    netamount1 = "";
                } else if (choice.equals("Discount Account")) {
                    if (discamount == 0) {

                    } else {
                        netamount1 = rssales.getString("total_disc_amnt");;
                        netamount = "";
                        atoledger = rssales.getString("by_ledger");
                    }
                } else if (choice.equals(acashaccount)) {
                    netamount = "";
                    netamount1 = rssales.getString("cashamount");
                    atoledger = rssales.getString("by_ledger");
                } else if (choice.equals(asundrydebtorsaccount)) {
                    netamount = "";
                    netamount1 = rssales.getString("sundrydebtorsamount");
                    atoledger = rssales.getString("by_ledger");
                } else if (choice.equals(acardaccount)) {
                    netamount = "";
                    netamount1 = rssales.getString("cardamount");
                    atoledger = rssales.getString("by_ledger");
                } else if (choice.equals(abyledger)) {

                    ResultSet rscalcsales = stm.executeQuery("select sum(total),sum(vatamnt),sum(sgstamnt),sum(cgstamnt) from billing where bill_refrence ='" + matchsno1 + "' and by_ledger='" + choice + "' ");
                    double calculation = 0;
                    while (rscalcsales.next()) {
                        calculation = (rscalcsales.getDouble(1)) - (rscalcsales.getDouble(2) + rscalcsales.getDouble(3) + rscalcsales.getDouble(4));
                    }

                    netamount = "" + calculation;
                    if (acashaccount != null || !acashaccount.equals("null")) {
                        atoledger = acashaccount;
                    } else if (asundrydebtorsaccount != null || !asundrydebtorsaccount.equals("null")) {
                        atoledger = asundrydebtorsaccount;
                    } else if (acardaccount != null || !acardaccount.equals("null")) {
                        atoledger = acardaccount;
                    }

                    netamount1 = "";
                }

                if (matchsno4.equals(matchsno1)) {

                } else {

                    rowvector.add(rssales.getString("date"));
                    rowvector.add(matchsno1);
                    rowvector.add(Description);
                    rowvector.add(atoledger);
                    rowvector.add(netamount);
                    rowvector.add(netamount1);
                    matchsno4 = matchsno1;
                    data.add(rowvector);

                }
            }
            Statement stmtsalesreturn = conne.createStatement();
            ResultSet rssalesreturn = stmtsalesreturn.executeQuery("select * from salesreturn  where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or vataccount='" + choice + "') and CAST(date as date) <'" + DateNow + "'");

            matchsno4 = "test";
            while (rssalesreturn.next()) {
                rowvector = new Vector();
                String matchsno1 = rssalesreturn.getString("bill_refrence");
                String avatacc = rssalesreturn.getString("vataccount");
                String netamount = "", atoledger = rssalesreturn.getString("to_ledger"), abyledger = rssalesreturn.getString("by_ledger");
                String netamount1 = "";
                //System.out.println(""+matchsno1);
                if (choice.equals(avatacc)) {
                    Statement stm = conne.createStatement();
                    ResultSet rscalc = stm.executeQuery("select sum(vatamnt) from salesreturn where bill_refrence ='" + matchsno1 + "'  ");
                    int calculation = 0;

                    while (rscalc.next()) {
                        calculation = rscalc.getInt(1);
                    }
                    netamount1 = "" + calculation;
                    netamount = "";
                    atoledger = rssalesreturn.getString("to_ledger");
                    //                   netamount1=rssalesreturn.getString("vatamnt");
                }
                if (choice.equals(atoledger)) {
                    netamount1 = "";
                    netamount = rssalesreturn.getString("total_value_after_tax");
                    atoledger = rssalesreturn.getString("by_ledger");
                } else if (choice.equals(abyledger)) {
                    Statement stm = conne.createStatement();
                    ResultSet rscalc = stm.executeQuery("select sum(total),sum(vatamnt) from salesreturn where bill_refrence ='" + matchsno1 + "' and by_ledger='" + choice + "' ");
                    Integer calculation = 0;

                    while (rscalc.next()) {
                        calculation = (int) (Double.parseDouble(rscalc.getString(1)) - Double.parseDouble(rscalc.getString(2)));
                    }

                    netamount1 = "" + calculation;
                    atoledger = rssalesreturn.getString("to_ledger");
                    netamount = "";
                }
                if (matchsno4.equals(matchsno1)) {

                } else {
                    rowvector.add(rssalesreturn.getString("date"));
                    rowvector.add(matchsno1);
                    rowvector.add("");
                    rowvector.add(atoledger);
                    rowvector.add(netamount);
                    rowvector.add(netamount1);

                    matchsno4 = matchsno1;
                    data.add(rowvector);
                }
            }
            Statement stmttobank = conne.createStatement();
            ResultSet rstobank = stmtjourn.executeQuery("select * from tobank where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(Da_te as date)<'" + DateNow + "'");

            matchsno4 = "test";
            while (rstobank.next()) {
                rowvector = new Vector();
                String matchsno1 = rstobank.getString(1);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    String byledger = rstobank.getString(5);
                    String toledger = rstobank.getString(6);

                    if (choice.equals(byledger)) {
                        rowvector.add(rstobank.getString(7));
                        rowvector.add(rstobank.getString(1));
                        rowvector.add("");
                        rowvector.add(toledger);
                        rowvector.add(rstobank.getString(3));
                        rowvector.add("");

                        data.add(rowvector);
                    } else if (choice.equals(toledger)) {
                        rowvector.add(rstobank.getString(7));
                        rowvector.add(rstobank.getString(1));
                        rowvector.add("");
                        rowvector.add(byledger);
                        rowvector.add("");
                        rowvector.add(rstobank.getString(3));

                        data.add(rowvector);
                    }
                    matchsno4 = matchsno1;
                }
            }

//            stmtreceipt.close();
//            stmtpayment.close();
//            stmtjourn.close();
//            stmt1.close();
//            conne.close();
//
//     
            long credit = 0, debit = 0, totalcredit = 0, totaldebit = 0;
            int rowcount = data.size();

            for (i = 0; i < rowcount; i++) {

                String a[] = data.get(i).toString().split(",");
                String[] b = a[5].split("]");

                if (!a[4].isEmpty()) {
                    if ((a[4] == null) || (a[4].trim().equals(""))) {
                        credit = 0;
                    } else {
                        credit = (long) Double.parseDouble(a[4] + "");
                    }
                } else {
                    credit = 0;
                }
                totalcredit = credit + totalcredit;

                if (!b[0].trim().isEmpty()) {
                    if ((b[0].trim() == null) || (b[0].trim() == "") || "0".equals(b[0].trim())) {
                        debit = 0;
                    } else {
                        debit = (long) Float.parseFloat(b[0].trim());
                    }
                } else {
                    debit = 0;
                }
                totaldebit = debit + totaldebit;

            }

            Vector data1 = new Vector();

            Vector rowvector11 = new Vector();
            int i1 = 0;

            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");

            data1.add(rowvector11);
            dtm.addRow(rowvector11);

            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");
            rowvector11.add("");

            data1.add(rowvector11);
            dtm.addRow(rowvector11);

            Vector rowvector1 = new Vector();

            rowvector1.add("");
            rowvector1.add(choice);
            rowvector1.add("");
            rowvector1.add("");
            rowvector1.add("");
            rowvector1.add("");

            data1.add(rowvector1);
            dtm.addRow(rowvector1);

            double total = 0;

            if (totalcredit >= totaldebit) {
                rowvector = new Vector();

                double result = totalcredit - totaldebit;
                rowvector.add("");
                rowvector.add("");
                rowvector.add("OPENING BALANCE");
                rowvector.add("");
                rowvector.add(Math.abs(result));
                rowvector.add("");
                total = totalcredit;
                data1.add(rowvector);
                dtm.addRow(rowvector);
            } else if (totaldebit > totalcredit) {
                rowvector = new Vector();

                double result = totaldebit - totalcredit;
                rowvector.add("");
                rowvector.add("");
                rowvector.add("OPENING BALANCE");
                rowvector.add("");
                rowvector.add("");
                rowvector.add(Math.abs(result));

                total = totaldebit;
                data1.add(rowvector);
                dtm.addRow(rowvector);
            }

            rs2 = stmt1.executeQuery("select * from Contra where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(v_date as date) ='" + DateNow + "' ");

            matchsno4 = "test";
            while (rs2.next()) {
                rowvector = new Vector();
                String matchsno1 = rs2.getString(1);

                String abyledger = rs2.getString(3);
                String atoledger = rs2.getString(4);

                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(abyledger)) {

                        rowvector.add(rs2.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add(rs2.getString(5));
                        rowvector.add("");
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    } else if (choice.equals(atoledger)) {
                        rowvector.add(rs2.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add("");
                        rowvector.add(rs2.getString(5));
                        data.add(rowvector);

                    }
                    matchsno4 = matchsno1;
                }

            }

            rs3 = stmt1.executeQuery("select * from interestentry where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(date as date) ='" + DateNow + "'");

            matchsno4 = "test";
            while (rs3.next()) {
                rowvector = new Vector();
                String matchsno1 = rs3.getString(1);

                String abyledger = rs3.getString("by_ledger");
                String atoledger = rs3.getString("to_ledger");

                {
                    if (choice.equals(abyledger)) {

                        rowvector.add(rs3.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add(rs3.getString(7));
                        rowvector.add(atoledger);
                        rowvector.add(rs3.getString(4));
                        rowvector.add("");
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    } else if (choice.equals(atoledger)) {
                        rowvector.add(rs3.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add(rs3.getString(7));
                        rowvector.add(abyledger);
                        rowvector.add("");
                        rowvector.add(rs3.getString(4));
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    }
                    //     matchsno4 = matchsno1;
                }

            }

            rsbsnl = stmtbsnl.executeQuery("select * from bsnlbilling where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "'  or disc_ledger='" + choice + "') and CAST(date as date)='" + DateNow + "'");

            while (rsbsnl.next()) {
                rowvector = new Vector();
                String matchsno1 = rsbsnl.getString(1);

                String abyledger = rsbsnl.getString(8);
                String atoledger = rsbsnl.getString(7);
                String adiscledger = rsbsnl.getString(9);

                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(abyledger)) {

                        rowvector.add(rsbsnl.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add(rsbsnl.getString(4));
                        rowvector.add("");
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    } else if (choice.equals(atoledger)) {
                        rowvector.add(rsbsnl.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(abyledger);
                        rowvector.add("");
                        rowvector.add(rsbsnl.getString(6));
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    } else if (choice.equals(adiscledger)) {
                        rowvector.add(rsbsnl.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(abyledger);
                        rowvector.add("");
                        rowvector.add(rsbsnl.getString(5));
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    }
                    matchsno4 = matchsno1;
                }

            }

            rsjourn = stmtjourn.executeQuery("select * from journal where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)= '" + DateNow + "' ");

            matchsno4 = "test";
            while (rsjourn.next()) {
                rowvector = new Vector();
                String matchsno1 = rsjourn.getString(2);
                String atoledger = rsjourn.getString(5);
                String abyledger = rsjourn.getString(4);
                if (choice.equals(atoledger)) {
                    if (rsjourn.getString(8).equals("") || rsjourn.getString(8).equals(null)) {
                    } else {
                        rowvector.add(rsjourn.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(abyledger);
                        rowvector.add(rsjourn.getString(8));
                        rowvector.add("");
                        //   matchsno4 = matchsno1;
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    }
                } else if (choice.equals(abyledger)) {
                    if (rsjourn.getString(7).equals("") || rsjourn.getString(7).equals(null)) {
                    } else {

                        rowvector.add(rsjourn.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(atoledger);
                        rowvector.add("");
                        rowvector.add(rsjourn.getString(7));

                        //    matchsno4 = matchsno1;
                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    }
                }

            }
            rspayment = stmtpayment.executeQuery("select * from payment where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)='" + DateNow + "' ");

            matchsno4 = "test";
            while (rspayment.next()) {
                rowvector = new Vector();
                String matchsno1 = rspayment.getString(1);
                String byledger = rspayment.getString(3);
                String toledger = rspayment.getString(4);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(byledger)) {
                        rowvector.add(rspayment.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(toledger);
                        rowvector.add(rspayment.getString(5));
                        rowvector.add("");
                    } else if (choice.equals(toledger)) {
                        rowvector.add(rspayment.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(byledger);
                        rowvector.add("");
                        rowvector.add(rspayment.getString(5));
                    }
                    matchsno4 = matchsno1;
                    data1.add(rowvector);
                    dtm.addRow(rowvector);
                }

            }

//cash reciept
            rsreciept = stmtreciept.executeQuery("select * from receipt where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)='" + DateNow + "' ");

            matchsno4 = "test";
            while (rsreciept.next()) {
                rowvector = new Vector();
                String matchsno1 = rsreciept.getString(1);
                String byledger = rsreciept.getString(3);
                String toledger = rsreciept.getString(4);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    if (choice.equals(byledger)) {
                        rowvector.add(rsreciept.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(toledger);
                        rowvector.add("");
                        rowvector.add(rsreciept.getString(5));
                    } else if (choice.equals(toledger)) {
                        rowvector.add(rsreciept.getString(2));
                        rowvector.add(matchsno1);
                        rowvector.add("");
                        rowvector.add(byledger);
                        rowvector.add(rsreciept.getString(5));
                        rowvector.add("");
                    }
                    matchsno4 = matchsno1;
                    data1.add(rowvector);
                    dtm.addRow(rowvector);
                }

            }

            rsbankpayment = stmtbankpayment.executeQuery("select * from bankpayment where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date) = '" + DateNow + "' ");

            matchsno4 = "test";
            while (rsbankpayment.next()) {
                rowvector = new Vector();
                String matchsno1 = rsbankpayment.getString(1);
                String abyledger = rsbankpayment.getString(3);
                String atoledger = rsbankpayment.getString(4);
                if (choice.equals(abyledger)) {
                    rowvector.add(rsbankpayment.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankpayment.getString(10));
                    rowvector.add(atoledger);
                    rowvector.add(rsbankpayment.getString(5));
                    rowvector.add("");
                    matchsno4 = matchsno1;
                    data1.add(rowvector);
                    dtm.addRow(rowvector);
                } else if (choice.equals(atoledger)) {
//                 if (matchsno4.equals(matchsno1)) {
//                } 
                    //             else {
                    rowvector.add(rsbankpayment.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankpayment.getString(10));
                    rowvector.add(abyledger);
                    rowvector.add("");
                    rowvector.add(rsbankpayment.getString(5));

                    //      matchsno4 = matchsno1;
                    data1.add(rowvector);
                    //   }           
                    dtm.addRow(rowvector);
                }

            }

            rsbankreciept = stmtbankreciept.executeQuery("select * from bankreceipt where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(da_te as date)='" + DateNow + "' ");

            matchsno4 = "test";
            while (rsbankreciept.next()) {
                rowvector = new Vector();
                String matchsno1 = rsbankreciept.getString(1);
                String abyledger = rsbankreciept.getString(3);
                String atoledger = rsbankreciept.getString(4);
                if (choice.equals(abyledger)) {
                    rowvector.add(rsbankreciept.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankreciept.getString(10));
                    rowvector.add(atoledger);
                    rowvector.add("");
                    rowvector.add(rsbankreciept.getString(5));
                    matchsno4 = matchsno1;
                    data1.add(rowvector);
                    dtm.addRow(rowvector);
                } else if (choice.equals(atoledger)) {
//                 if (matchsno4.equals(matchsno1)) {
//                } 
                    //             else {

                    rowvector.add(rsbankreciept.getString(2));
                    rowvector.add(matchsno1);
                    rowvector.add(rsbankreciept.getString(10));
                    rowvector.add(abyledger);
                    rowvector.add(rsbankreciept.getString(5));
                    rowvector.add("");

                    //      matchsno4 = matchsno1;
                    data1.add(rowvector);
                    //   }           
                    dtm.addRow(rowvector);
                }

            }

            if (choice.equals("DISCOUNT ACCOUNT")) {
                ResultSet rspur = stmtpur.executeQuery("select * from Stock where  discount !=0 ");
//                parti = null;
                matchsno4 = "test";
                while (rspur.next()) {
                    rowvector = new Vector();

                    String matchsno1 = rspur.getString(4);
                    if (matchsno4.equals(matchsno1)) {

                    } else {
                        Double discountamount = (Double.parseDouble(rspur.getString(16))) - (Double.parseDouble(rspur.getString(18)));
                        rowvector.add(rspur.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add("Discount Entry");
                        rowvector.add(rspur.getString(2));
                        rowvector.add(discountamount);
                        rowvector.add("");
                        matchsno4 = matchsno1;
                        data.add(rowvector);
                    }
                }

            } else {
                ResultSet rspur = stmtpur.executeQuery("select * from Stock where ( to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or sgstaccount ='" + choice + "'  or cgsttaxaccount='" + choice + "' or igst_tax_account ='" + choice + "' or otherchargesaccount='" + choice + "') and cast(Invoice_date as date)< '" + DateNow + "'");
                //              parti = null;
                matchsno4 = "test";
                while (rspur.next()) {
                    rowvector = new Vector();
                    String matchsno1 = rspur.getString(4);
                    String asgstaccount = rspur.getString("sgstaccount");
                    String acgstaccount = rspur.getString("cgsttaxaccount");
                    String aigstaccount = rspur.getString("Igst_tax_Account");
                    String aothercharges = rspur.getString("otherchargesaccount");
                    String netamount = "", atoledger = "", netamount1 = "";
                    String abyledger = rspur.getString("by_ledger");
                    if (choice.equals(asgstaccount)) {
                        netamount1 = rspur.getString("sgstamount");
                        atoledger = rspur.getString("to_ledger");
                    } else if (choice.equals(aigstaccount)) {
                        netamount1 = rspur.getString("igst_amount");
                        atoledger = rspur.getString("to_ledger");
                    } else if (choice.equals(aothercharges)) {
                        netamount1 = rspur.getString("Other_charges");
                        atoledger = rspur.getString("to_ledger");
                        netamount = "";
                    } else if (choice.equals(acgstaccount)) {
                        netamount1 = rspur.getString("cgsttaxamount");
                        atoledger = rspur.getString("to_ledger");
                    } else if (choice.equals(abyledger)) {
                        atoledger = rspur.getString("to_ledger");
                        netamount1 = rspur.getString("Bill_amount");
                    } else {

                        netamount = rspur.getString("Net_amount");
                        atoledger = rspur.getString("by_ledger");
                    }
                    if (matchsno4.equals(matchsno1)) {
                    } else {

                        rowvector.add(rspur.getString(3));
                        rowvector.add(matchsno1);
                        rowvector.add(rspur.getString(6));
                        rowvector.add(atoledger);
                        rowvector.add(netamount);
                        rowvector.add(netamount1);

                        matchsno4 = matchsno1;
                        data1.add(rowvector1);
                        dtm.addRow(rowvector1);
                    }
                }
            }

            Statement stmtpurreturn = conne.createStatement();
            ResultSet rspurreturn = stmtpurreturn.executeQuery("select * from PurchaseReturn  where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or vataccount='" + choice + "' or taxaccount='" + choice + "' or otherchargesaccount='" + choice + "') and cast(Invoice_date as date )='" + DateNow + "'  ");
            // System.out.println(""+"select * from Stock where Supplier_name = '" + choice + "' or Purchase_type = '" + choice + "' or vataccount='"+choice+"'");

            matchsno4 = "test";
            while (rspurreturn.next()) {
                rowvector = new Vector();
                String matchsno1 = rspurreturn.getString("Invoice_no");
                String avatacc = rspurreturn.getString("vataccount");
                String ataxaccount = rspurreturn.getString("taxaccount");
                String aotherchargesaccount = rspurreturn.getString("otherchargesaccount");
                String netamount = "", atoledger = rspurreturn.getString("to_ledger"), abyledger = rspurreturn.getString("by_ledger");

                String netamount1 = "";
                //System.out.println(""+matchsno1);
                if (choice.equals(avatacc)) {
                    netamount = rspurreturn.getString("vatamount");

                    netamount1 = "";
                } else if (choice.equals(ataxaccount)) {
                    netamount = rspurreturn.getString("taxamt");
                    atoledger = rspurreturn.getString("to_ledger");
                    netamount1 = "";

                } else if (choice.equals(aotherchargesaccount)) {
                    netamount = rspurreturn.getString("Other_charges");
                    atoledger = rspurreturn.getString("to_ledger");
                    netamount1 = "";

                } else if (choice.equals(abyledger)) {
                    netamount1 = rspurreturn.getString("Net_amt");
                    atoledger = rspurreturn.getString("to_ledger");
                    netamount = "";
                } else {
                    netamount = rspurreturn.getString("Net_amt");
                    netamount1 = "";
                    atoledger = rspurreturn.getString("by_ledger");
                }

                if (matchsno4.equals(matchsno1)) {

                } else {

                    rowvector.add(rspurreturn.getString("Invoice_date"));
                    rowvector.add(matchsno1);
                    rowvector.add(rspurreturn.getString(5));
                    rowvector.add(atoledger);
                    rowvector.add(netamount);
                    rowvector.add(netamount1);

//                    data[i][0] = ;
//                    data[i][1] = atoledger;
//                    data[i][2] = matchsno1;
//                    data[i][3] = rspurreturn.getString("Purchase_type");
//                    data[i][4] = netamount;
//                    data[i][5] = netamount1;
//                    i++;
                    matchsno4 = matchsno1;
                    data1.add(rowvector);
                    dtm.addRow(rowvector);
                }
            }

            rssales = stmtsales.executeQuery("select * from billing  where (cashaccount= '" + choice + "' or sundrydebtorsaccount='" + choice + "' or cardaccount='" + choice + "' or by_ledger = '" + choice + "' or vataccount='" + choice + "' or Sgstaccount='" + choice + "' or Cgstaccount='" + choice + "' or '" + choice + "'='DISCOUNT ACCOUNT') and cast(date as date)='" + DateNow + "' ");
            // System.out.println(""+"select * from Stock where Supplier_name = '" + choice + "' or Purchase_type = '" + choice + "' or vataccount='"+choice+"'");
            //           parti = null;
            matchsno4 = "test";
            while (rssales.next()) {
                rowvector = new Vector();
                String matchsno1 = rssales.getString("bill_refrence");
                double discamount = Double.parseDouble(rssales.getString("total_disc_amnt"));
                String avatacc = rssales.getString("vataccount");
                String asgstacc = rssales.getString("sgstaccount");
                String acgstacc = rssales.getString("cgstaccount");
                String acashaccount = rssales.getString("cashaccount");
                String asundrydebtorsaccount = rssales.getString("sundrydebtorsaccount");
                String acardaccount = rssales.getString("cardaccount");
                String netamount = "", atoledger = rssales.getString("to_ledger"), abyledger = rssales.getString("by_ledger");
                String netamount1 = "";
                String Description = rssales.getString("customer_name");
                ResultSet rscalc = null;
                Statement stm = conne.createStatement();
                if ((choice.equals(avatacc)) || (choice.equals(asgstacc)) || (choice.equals(acgstacc))) {

                    if (choice.equals(avatacc)) {
                        rscalc = stm.executeQuery("select sum(vatamnt) from billing where bill_refrence ='" + matchsno1 + "'  ");
                    } else if (choice.equals(asgstacc)) {
                        rscalc = stm.executeQuery("select sum(sgstamnt) from billing where bill_refrence ='" + matchsno1 + "'  ");
                    } else if (choice.equals(acgstacc)) {
                        rscalc = stm.executeQuery("select sum(cgstamnt) from billing where bill_refrence ='" + matchsno1 + "'  ");
                    }
                    double calculation = 0;

                    while (rscalc.next()) {
                        calculation = rscalc.getDouble(1);
                    }
                    netamount = "" + calculation;
                    atoledger = rssales.getString(acashaccount + asundrydebtorsaccount + acardaccount);
                    netamount1 = "";
                } else if (choice.equals("Discount Account")) {
                    if (discamount == 0) {

                    } else {
                        netamount1 = rssales.getString("total_disc_amnt");;
                        netamount = "";
                        atoledger = rssales.getString("by_ledger");
                    }
                } else if (choice.equals(acashaccount)) {
                    netamount = "";
                    netamount1 = rssales.getString("cashamount");
                    atoledger = rssales.getString("by_ledger");
                } else if (choice.equals(asundrydebtorsaccount)) {
                    netamount = "";
                    netamount1 = rssales.getString("sundrydebtorsamount");
                    atoledger = rssales.getString("by_ledger");
                } else if (choice.equals(acardaccount)) {
                    netamount = "";
                    netamount1 = rssales.getString("cardamount");
                    atoledger = rssales.getString("by_ledger");
                } else if (choice.equals(abyledger)) {

                    ResultSet rscalcsales = stm.executeQuery("select sum(total),sum(vatamnt),sum(sgstamnt),sum(cgstamnt) from billing where bill_refrence ='" + matchsno1 + "' and by_ledger='" + choice + "' ");
                    double calculation = 0;
                    while (rscalcsales.next()) {
                        calculation = rscalcsales.getDouble(1) - (rscalcsales.getDouble(2) + rscalcsales.getDouble(3) + rscalcsales.getDouble(4));
                    }

                    netamount = "" + calculation;
                    if (acashaccount != null || !acashaccount.equals("null")) {
                        acashaccount = acashaccount;
                    } else {
                        acashaccount = "";
                    }
                    if (asundrydebtorsaccount != null || !asundrydebtorsaccount.equals("null")) {
                        asundrydebtorsaccount = asundrydebtorsaccount;
                    } else {
                        asundrydebtorsaccount = "";
                    }
                    if (acardaccount != null || !acardaccount.equals("null")) {
                        acardaccount = acardaccount;
                    } else {
                        acardaccount = "";
                    }
                    atoledger = acashaccount + acardaccount + asundrydebtorsaccount;
                    netamount1 = "";
                }

                if (matchsno4.equals(matchsno1)) {

                } else {

                    rowvector.add(rssales.getString("date"));
                    rowvector.add(matchsno1);
                    rowvector.add(Description);
                    rowvector.add(atoledger);
                    rowvector.add(netamount);
                    rowvector.add(netamount1);
                    matchsno4 = matchsno1;
                    data1.add(rowvector);
                    dtm.addRow(rowvector);
                }
            }
            rssalesreturn = stmtsalesreturn.executeQuery("select * from salesreturn  where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "' or vataccount='" + choice + "') and CAST(date as date) = '" + DateNow + "'");
            // System.out.println(""+"select * from Stock where Supplier_name = '" + choice + "' or Purchase_type = '" + choice + "' or vataccount='"+choice+"'");

            matchsno4 = "test";
            while (rssalesreturn.next()) {
                rowvector = new Vector();
                String matchsno1 = rssalesreturn.getString("bill_refrence");
                String avatacc = rssalesreturn.getString("vataccount");
                String netamount = "", atoledger = rssalesreturn.getString("to_ledger"), abyledger = rssalesreturn.getString("by_ledger");
                String netamount1 = "";
                //System.out.println(""+matchsno1);
                if (choice.equals(avatacc)) {
                    Statement stm = conne.createStatement();
                    ResultSet rscalc = stm.executeQuery("select sum(vatamnt) from salesreturn where bill_refrence ='" + matchsno1 + "'  ");
                    int calculation = 0;

                    while (rscalc.next()) {
                        calculation = rscalc.getInt(1);
                    }
                    netamount1 = "" + calculation;
                    netamount = "";
                    atoledger = rssalesreturn.getString("to_ledger");
                    //                   netamount1=rssalesreturn.getString("vatamnt");
                }
                if (choice.equals(atoledger)) {
                    netamount1 = "";
                    netamount = rssalesreturn.getString("total_value_after_tax");
                    atoledger = rssalesreturn.getString("by_ledger");
                } else if (choice.equals(abyledger)) {
                    Statement stm = conne.createStatement();
                    ResultSet rscalc = stm.executeQuery("select sum(total),sum(vatamnt) from salesreturn where bill_refrence ='" + matchsno1 + "' and by_ledger='" + choice + "' ");
                    Integer calculation = 0;

                    while (rscalc.next()) {
                        calculation = (int) (Double.parseDouble(rscalc.getString(1)) - Double.parseDouble(rscalc.getString(2)));
                    }

                    netamount1 = "" + calculation;
                    atoledger = rssalesreturn.getString("to_ledger");
                    netamount = "";
                }
                if (matchsno4.equals(matchsno1)) {

                } else {
                    rowvector.add(rssalesreturn.getString("date"));
                    rowvector.add(matchsno1);
                    rowvector.add("");
                    rowvector.add(atoledger);
                    rowvector.add(netamount);
                    rowvector.add(netamount1);

                    matchsno4 = matchsno1;
                    data1.add(rowvector);
                    dtm.addRow(rowvector);
                }
            }

            rstobank = stmtjourn.executeQuery("select * from tobank where (to_ledger = '" + choice + "' or by_ledger = '" + choice + "') and CAST(Da_te as date)='" + DateNow + "'");

            matchsno4 = "test";
            while (rstobank.next()) {
                rowvector = new Vector();
                String matchsno1 = rstobank.getString(1);
                if (matchsno4.equals(matchsno1)) {
                } else {
                    String byledger = rstobank.getString(5);
                    String toledger = rstobank.getString(6);

                    if (choice.equals(byledger)) {
                        rowvector.add(rstobank.getString(7));
                        rowvector.add(rstobank.getString(1));
                        rowvector.add("");
                        rowvector.add(toledger);
                        rowvector.add(rstobank.getString(3));
                        rowvector.add("");

                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    } else if (choice.equals(toledger)) {
                        rowvector.add(rstobank.getString(7));
                        rowvector.add(rstobank.getString(1));
                        rowvector.add("");
                        rowvector.add(byledger);
                        rowvector.add("");
                        rowvector.add(rstobank.getString(3));

                        data1.add(rowvector);
                        dtm.addRow(rowvector);
                    }
                    matchsno4 = matchsno1;
                }
            }

            long credit1 = 0;
            long debit1 = 0;
            long totalcredit1 = 0;
            long totaldebit1 = 0;
            int rowcount1 = data1.size();

            for (int i2 = 0; i2 < rowcount1; i2++) {

                String a[] = data1.get(i2).toString().split(",");
                String[] b = a[5].split("]");
                System.out.println("a 4 is " + a[4]);

                if (!a[4].isEmpty()) {
                    if ((a[4].trim() == null) || (a[4].trim().equals(""))) {
                        credit1 = 0;
                    } else {
                        credit1 = (long) Double.parseDouble(a[4]);
                    }
                } else {
                    credit1 = 0;
                }
                totalcredit1 = credit1 + totalcredit1;

                if (!b[0].trim().isEmpty()) {
                    if ((b[0].trim() == null) || (b[0].trim().equals("")) || "0".equals(b[0].trim())) {
                        debit1 = 0;
                    } else {
                        debit1 = (long) Float.parseFloat(b[0].trim());
                    }
                } else {
                    debit1 = 0;
                }
                totaldebit1 = debit1 + totaldebit1;

            }

            if (totalcredit1 >= totaldebit1) {
                rowvector = new Vector();

                Long result = totalcredit1 - totaldebit1;
                rowvector.add("");
                rowvector.add("");
                rowvector.add("CLOSING BALANCE");
                rowvector.add("");
                rowvector.add("");
                rowvector.add(Math.abs(result));

                total = totalcredit1;
                data1.add(rowvector);
                dtm.addRow(rowvector);
            } else if (totaldebit1 > totalcredit1) {
                rowvector = new Vector();

                Long result = totaldebit1 - totalcredit1;
                rowvector.add("");
                rowvector.add("");
                rowvector.add("CLOSING BALANCE");
                rowvector.add("");
                rowvector.add(Math.abs(result));
                rowvector.add("");

                total = totaldebit1;
                data1.add(rowvector);
                dtm.addRow(rowvector);
            }

            rowvector = new Vector();

            rowvector.add("");
            rowvector.add("");
            rowvector.add("TOTAL");
            rowvector.add("");
            rowvector.add(Math.abs(total));
            rowvector.add(Math.abs(total));

            data1.add(rowvector);
            dtm.addRow(rowvector);

            stmtpayment.close();
            stmtjourn.close();
            stmt1.close();
            conne.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
    
//---------Function to write export data into excel------------------------------------------------------------
    public void toExcel() throws FileNotFoundException, IOException {
        String Company_Name = "";
        String Company_No = "";
        try {
            connection c = new connection();
            Connection connect = c.cone();
            Statement st = connect.createStatement();
            ResultSet rs = st.executeQuery("Select * from cmp_detail");
            while(rs.next()){
            Company_Name = rs.getString("cmp_name");
            Company_No = rs.getString("phone_no");        
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        TableModel model = jTable1.getModel();
// Creating Sheet        
        XSSFSheet spreadsheet = workbook.createSheet();
// Creating Rows
        XSSFRow row = spreadsheet.createRow((short) 0);
        XSSFRow row1 = spreadsheet.createRow((short) 1);
        XSSFRow row2;
// Creating Cell        
        XSSFCell cell1 = (XSSFCell) row.createCell((short) 0);
// Creating Font        
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Verdana");
        font.setItalic(false);
        font.setBold(true);
        font.setColor(HSSFColor.AUTOMATIC.index);
// Creating Style        
        XSSFCellStyle style = workbook.createCellStyle();
        XSSFCellStyle style1 = workbook.createCellStyle();
        style1.setAlignment(HorizontalAlignment.RIGHT);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        style.setFont(font);
        style.setWrapText(true);
        row.setHeight((short) 700);
        cell1.setCellValue(Company_Name+" - "+Company_No + "\n" + "Cash Book -" + jLabel2.getText());
//        cell1.setCellValue("Khabiya Cloth Store - 07412-492939" + "\n" + "Cash Book -" + jLabel2.getText());
        cell1.setCellStyle(style);
// Marging the rows        
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));
// Setting Row header        
        for (int i = 0; i < model.getColumnCount(); i++) {
            XSSFCell cell = (XSSFCell) row1.createCell((short) i);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
// Getting Value for Each Column        
        for (int i = 0; i < model.getRowCount(); i++) {
// Creating row after header and row header            
            row = spreadsheet.createRow(i + 2);
            for (int j = 0; j < model.getColumnCount(); j++) {
// Creating Cell for puting values                
                XSSFCell cell2 = (XSSFCell) row.createCell((short) j);
                cell2.setCellValue((model.getValueAt(i, j)+""));
// Condition to check numeric value then right align                 
                if (j >= 3) {
                    cell2.setCellStyle(style1);
                }
    /*            
// Condition to put table total after all rows                
                if (i == (model.getRowCount() - 1)) {
                    row2 = spreadsheet.createRow(i + 4);
                    XSSFCell c1 = (XSSFCell) row2.createCell((short) 2);
                    c1.setCellValue("Total");
                    c1.setCellStyle(style);
//                    spreadsheet.addMergedRegion(new CellRangeAddress(i + 4, i + 4, 0, 0));
                    c1 = (XSSFCell) row2.createCell((short) 4);
                    c1.setCellValue(jTextField2.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 5);
                    c1.setCellValue(jTextField3.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 6);
                    c1.setCellValue(jTextField4.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 7);
                    c1.setCellValue(jTextField5.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 8);
                    c1.setCellValue(jTextField6.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 9);
                    c1.setCellValue(jTextField7.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 10);
                    c1.setCellValue(jTextField8.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 11);
                    c1.setCellValue(jTextField9.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 12);
                    c1.setCellValue(jTextField10.getText());
                    c1.setCellStyle(style1);
                    c1 = (XSSFCell) row2.createCell((short) 13);
                    c1.setCellValue(jTextField11.getText());
                    c1.setCellStyle(style1);
                } 
      */
// Setting excel sheet area for printing                
                workbook.setPrintArea(0, 0, j, 0, i);
            }
        }
// Resizing cloumn
        for (int i = 0; i < model.getColumnCount(); i++) {
            spreadsheet.autoSizeColumn(i);
        }
// Setup for printing
        XSSFPrintSetup ps = (XSSFPrintSetup) spreadsheet.getPrintSetup();
//        ps.setLandscape(true);
        spreadsheet.setAutobreaks(true);
        spreadsheet.setFitToPage(true);
        ps.setFitWidth((short) 1);
        ps.setFitHeight((short) 0);
    }
//-------------------------------------------------------------------------------------------------------------

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog2 = new javax.swing.JDialog();
        jLabel3 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        jDialog2.setMinimumSize(new java.awt.Dimension(250, 100));

        jLabel3.setText("Enter Date : ");

        jDateChooser1.setDateFormatString("yyy-MM-dd");

        jButton2.setText("GO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "DATE", "VCH NO", "DESCRIPTION", "PARTICULARS", "PAID", "RECIVED"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(75);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("CASH AND SALES BOOK");

        jButton1.setForeground(new java.awt.Color(57, 57, 58));
        jButton1.setText("Export");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 945, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButton1)
                .addGap(287, 287, 287)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jLabel1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        jDialog2.dispose();
        String date1 = formatter.format(jDateChooser1.getDate());
        String date11 = formatter1.format(jDateChooser1.getDate());
        jLabel2.setText(date11);

        dtm.getDataVector().removeAllElements();
        dtm.fireTableDataChanged();

        try {
            connection c = new connection();
            Connection conne = c.cone();
            Statement stmt = conne.createStatement();

            ResultSet rs = stmt.executeQuery("select * from ledger where groups='CASH IN HAND' or groups='SALES ACCOUNTS' ");

            while (rs.next()) {
                daybookmethod(rs.getString(1), date1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        int key = evt.getKeyCode();

        if (key == evt.VK_F3) {
            jDialog2.setVisible(true);
        }

    }//GEN-LAST:event_jTable1KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        obj = null;
    }//GEN-LAST:event_formWindowClosed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser fc = new JFileChooser();
        int option = fc.showSaveDialog(cashbook.this);
        if (option == JFileChooser.APPROVE_OPTION) {
            String filename = fc.getSelectedFile().getName();
            String path = fc.getSelectedFile().getParentFile().getPath();

            int len = filename.length();
            String ext = "";
            String file = "";

            if (len > 4) {
                ext = filename.substring(len - 4, len);
            }

            if (ext.equals(".xlsx")) {
                file = path + "\\" + filename;
            } else {
                file = path + "\\" + filename + ".xlsx";
            }
            try {
                toExcel();
                FileOutputStream out = new FileOutputStream(new File(file));
                //write operation workbook using file out object 
                workbook.write(out);

//                Desktop desktop = Desktop.getDesktop();
//                try {
//                    desktop.print(new File(file));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                System.out.println("createworkbook.xlsx written successfully");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(cashbook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(cashbook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(cashbook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(cashbook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new cashbook().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
